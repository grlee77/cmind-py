#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_feat_prep(output_dir,func_file,TR,smooth=5,paradigm_hp=128,brain_thresh=10, use_OTSU=False, usan_vol=None, susan_bt_usan = None, perfusion_subtract=False, slice_timing_file=None, verbose=False, logger=None):
    
    """run additional Feat preprocessing steps
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    func_file : str
        filename of the 4D NIFTI/NIFTI-GZ fMRI timeseries
    TR : float
        repetition time, TR (seconds)
    smooth : float, optional
       spatial smoothing (mm)
    paradigm_hp : float, optional
        high pass filter cutoff (seconds)
    brain_thresh : float, optional
        brain threshold (percent). 
    use_OTSU : bool, optional
        NOTE: if skimage.filter.threshold_otsu is found, brain_thresh is 
        ignored and OTSU threshold is used instead
    usan_vol : str, optional
        filename of alternative usan_vol for susan smoothing.  if not specified
        mean_func is used as in Feat
    susan_bt_usan : float, optional
        brightness threshold for susan_smooth
    perfusion_subtract : bool, optional
        if True, perform ASL surround subtraction
    slice_timing_file : str, optional
        slice timing file
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
                  
    Returns
    -------
    filtered_func_data : str
        filename of the filtered 4D functional data
    noise_est_file : str
        filename of the noise estimation parameters
    preproc_tarfile : str
        filename of the .tar.gz archive containing the preprocessing results

        
    See Also
    --------
    cmind_feat_preprocess2
    
    Notes
    -----
    Carries out spatial smoothing, bandpass temporal filtering, etc.
    Called by cmind_feat_preprocess2
    
    """
    import os, glob, warnings
    from os.path import join as pjoin
    
    import numpy as np

    from cmind.utils.file_utils import imexist
    from cmind.utils.logging_utils import log_cmd
    from cmind.globals import fsl_version
    
    if use_OTSU:  #use OTSU brain threshold instead
        try:
            from skimage.filter import threshold_otsu as otsu
            use_OTSU=True
        except:
            warnings.warn("skimage not found.  unable to use OTSU threshold")
            use_OTSU=False
    
    #smooth=float(smooth)
    #paradigm_hp=float(paradigm_hp)
    #brain_thresh=float(brain_thresh)
    
    o=output_dir
    normmean = 10000;
    
    #TODO: could add automated cutoff calculation.  For now I ran it and stored the recommended value in feat_params.csv
    #if not paradigm_hp:
    #    paradigm_hp=float(log_cmd('$FSLDIR/bin/cutoffcalc --tr=4.0 --in=design.mat'))
    
    #eval(sprintf('!$FSLDIR/bin/fslmaths %s prefiltered_func_data_mcf -odt float',func_file))
    log_cmd('$FSLDIR/bin/fslmaths "%s" "%s/prefiltered_func_data_mcf" -odt float' % (func_file,o), verbose=verbose, logger=logger)
    
    #fslinfo prefiltered_func_data_mcf | grep "dim4" | head -n 1 | sed 's/ \+/\t/g' | cut -f 2 > dim.txt
    Ntime = log_cmd("$FSLDIR/bin/fslinfo \"%s/prefiltered_func_data_mcf\" | grep dim4 | head -n 1 | sed 's/ \+/\t/g' | cut -f 2" % o, verbose=verbose, logger=logger)
    Ntime = int(Ntime)
    Rvol = np.round(Ntime/2.0)
    log_cmd('$FSLDIR/bin/fslroi "%s/prefiltered_func_data_mcf" "%s/example_func" %d 1' % (o,o,Rvol), verbose=verbose, logger=logger)
    
    if slice_timing_file: #optional slice timing correction
        #Note:  FSL 4.1.9+ use t=0 as the center of the TR.  prior versions used 0.5.  
        if not os.path.exists(slice_timing_file):
            raise IOError("Slice timing file, %s, doesn't exist!" % slice_timing_file)
        log_cmd('$FSLDIR/bin/slicetimer -i "%s/prefiltered_func_data_mcf" --out="%s/prefiltered_func_data_mcf_st" -r %0.11g --tcustom="%s"' % (o, o, TR, slice_timing_file), verbose=verbose, logger=logger)
        log_cmd('$FSLDIR/bin/immv "%s/prefiltered_func_data_mcf_st" "%s/prefiltered_func_data_mcf"' % (o,o), verbose=verbose, logger=logger) #keep same filename with or without slice timing correction...
        
    log_cmd('$FSLDIR/bin/fslmaths "%s/prefiltered_func_data_mcf" -Tmean "%s/mean_func"' % (o,o) , verbose=verbose, logger=logger)
    log_cmd('$FSLDIR/bin/bet2 "%s/mean_func" "%s/mask" -f 0.3 -n -m' % (o,o), verbose=verbose, logger=logger)
    log_cmd('$FSLDIR/bin/immv "%s/mask_mask" "%s/mask"' % (o,o), verbose=verbose, logger=logger)
    #log_cmd('$FSLDIR/bin/immv "%s/mask_mask" "%s/mask"' % (o,o), verbose=verbose, logger=logger)
    
    log_cmd('$FSLDIR/bin/fslmaths "%s/prefiltered_func_data_mcf" -mas "%s/mask" "%s/prefiltered_func_data_bet"' % (o,o,o), verbose=verbose, logger=logger)
    pct=log_cmd('$FSLDIR/bin/fslstats "%s/prefiltered_func_data_bet" -p 2 -p 98' % o, verbose=verbose, logger=logger)
    (p2,p98)=pct.strip().split(' ')
    p2=abs(float(p2))
    p98=float(p98)
    
    if use_OTSU:
        imbool,mean_func=imexist(pjoin(o,'mean_func'))[0:2]
        if not imbool:
            raise ValueError("mean_func does not exist at expected location")
        thresh=otsu(mean_func)
    else:
        thresh=p2+brain_thresh*(p98-p2)/100.0
    
    log_cmd('$FSLDIR/bin/fslmaths "%s/prefiltered_func_data_bet" -thr %f -Tmin -bin "%s/mask" -odt char' % (o,thresh,o), verbose=verbose, logger=logger)
    p50=log_cmd('$FSLDIR/bin/fslstats "%s/prefiltered_func_data_mcf" -k "%s/mask" -p 50' % (o,o), verbose=verbose, logger=logger)
    p50=float(p50.strip())

    log_cmd('$FSLDIR/bin/fslmaths "%s/mask" -dilF "%s/mask"' % (o,o), verbose=verbose, logger=logger)
    log_cmd('$FSLDIR/bin/fslmaths "%s/prefiltered_func_data_mcf" -mas "%s/mask" "%s/prefiltered_func_data_thresh"' % (o,o,o), verbose=verbose, logger=logger)


    
        
    if smooth>0.01: #optional spatial smoothing
        if usan_vol is None:
            usan_vol=pjoin(o,'mean_func')  #FSL Feat default
        
        
        susan_bt = 0.75*(p50-p2)  #FSL Feat default
        
        #Note: susan_bt must be larger than noise level, but should be less 
        #      than the contrast of the edges we want to preserve
        if susan_bt_usan is None:
            susan_bt_usan = susan_bt
            
        susan_kern = smooth / 2.355
        log_cmd('$FSLDIR/bin/fslmaths "%s/prefiltered_func_data_thresh" -Tmean "%s/mean_func"' % (o,o), verbose=verbose, logger=logger)
        log_cmd('$FSLDIR/bin/susan "%s/prefiltered_func_data_thresh" %f %f 3 1 1 "%s" %f "%s/prefiltered_func_data_smooth"' % (o,susan_bt,susan_kern,usan_vol,susan_bt_usan,o), verbose=verbose, logger=logger)
        log_cmd('$FSLDIR/bin/fslmaths "%s/prefiltered_func_data_smooth" -mas "%s/mask" "%s/prefiltered_func_data_smooth"' % (o,o,o), verbose=verbose, logger=logger)
    else:  #no smoothing needs to be done
        log_cmd('$FSLDIR/bin/immv "%s/prefiltered_func_data_thresh" "%s/prefiltered_func_data_smooth"' % (o,o), verbose=verbose, logger=logger)        
    
    mult_fact = normmean/p50
    log_cmd('$FSLDIR/bin/fslmaths "%s/prefiltered_func_data_smooth" -mul %f "%s/prefiltered_func_data_intnorm"' % (o,mult_fact,o), verbose=verbose, logger=logger)
    
    if perfusion_subtract: #TODO: SHOULD NOT USE FILM PREWHITENING IF THIS IS SELECTED
        #Note: use -c flag if first volume is control
        #TODO: UCLA and CCHMC data have opposite control/tag orders, so would have to pass a flag to choose whether to include -c or not
        log_cmd('$FSLDIR/bin/perfusion_subtract "%s/prefiltered_func_data_intnorm" "%s/prefiltered_func_data_perfsub" -c' % (o,o), verbose=verbose, logger=logger)


    if perfusion_subtract:
        funcdata = pjoin(o,'prefiltered_func_data_perfsub')
    else:
        funcdata = pjoin(o,'prefiltered_func_data_intnorm')
    
    if paradigm_hp and (paradigm_hp>0) and (paradigm_hp<np.Inf):  #optional bandpass temporal filtering
        if fsl_version >= '5.0.7':  #as of version 5.0.7, fslmaths highpass filter removes the mean.  we need to add it back
            log_cmd('$FSLDIR/bin/fslmaths "%s" -Tmean "%s"' % (funcdata,pjoin(o,'tempMean')), verbose=verbose, logger=logger)
            restore_mean_cmd = '-add "%s"' % pjoin(o,'tempMean')
        else:
            restore_mean_cmd = ""            
        bptf_vols = paradigm_hp/float(TR)/2.0
        log_cmd('$FSLDIR/bin/fslmaths "%s" -bptf %f -1 %s "%s/prefiltered_func_data_tempfilt"' % (funcdata,bptf_vols,restore_mean_cmd, o), verbose=verbose, logger=logger)
        log_cmd('$FSLDIR/bin/fslmaths "%s/prefiltered_func_data_tempfilt" "%s/filtered_func_data"' % (o,o), verbose=verbose, logger=logger)
        if fsl_version >= '5.0.7': 
            log_cmd('$FSLDIR/bin/imrm "%s/tempMean"' % (o), verbose=verbose, logger=logger)            
    else:
        log_cmd('$FSLDIR/bin/fslmaths "%s" "%s/filtered_func_data"' % (funcdata,o), verbose=verbose, logger=logger)
        
            
    if True:  #not required.  just running out of curiosity
        #estnoise <4d_input_data> [spatial_sigma temp_hp_sigma temp_lp_sigma]
        #can run on already filtered data instead of having estnoise reapply spatial and temporal smoothing
        log_cmd('$FSLDIR/bin/estnoise "%s/filtered_func_data" > "%s/noise_est.txt"' % (o,o), verbose=verbose, logger=logger)
    
    #Put the correct TR into the header
    log_cmd("$FSLDIR/bin/fslhd -x \"%s/filtered_func_data\" | sed 's/  dt = .*/  dt = '%0.3f'/g' > \"%s/tmpHeader\"" % (o,TR,o), verbose=verbose, logger=logger)
    log_cmd('$FSLDIR/bin/fslcreatehd "%s/tmpHeader" "%s/filtered_func_data"' % (o,o), verbose=verbose, logger=logger)
    os.remove(pjoin(o,'tmpHeader'))

    log_cmd('$FSLDIR/bin/fslmaths "%s/filtered_func_data" -Tmean "%s/mean_func"' % (o,o), verbose=verbose, logger=logger)

    prefilt_files=glob.glob(pjoin(o,'prefiltered_func_data*'))
    for f in prefilt_files:
        os.remove(f)
    
    #log_cmd('/bin/rm stat_p2_p98.txt stats_p50.txt dim.txt', verbose=verbose, logger=logger)
    
    #melodic -i "%s/filtered_func_data" -o "%s/filtered_func_data.ica" -v --nobet --bgthreshold=3 --tr=4.0 --report --guireport=../../report.html -d 0 --mmthresh=0.5 --Ostats --Tdes=/home/lee8rx/Data/Akila/IRC04H_07M005_F_1/BOLD.feat/design.mat --Tcon=/home/lee8rx/Data/Akila/IRC04H_07M005_F_1/BOLD.feat/design.con
    #feat design.fsf -D MELODIC.ica -stop

    
def cmind_fMRI_preprocess2(output_dir, func_file,feat_params=None,TR=None,smooth=None,paradigm_hp=None,brain_thresh=None, perfusion_subtract=False, preprocess_case=None, slice_timing_file=None, usan_vol=None, output_tar=False, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """ FMRI Preprocessing: Stage 2
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    func_file : str
        filename of the 4D NIFTI/NIFTI-GZ fMRI timeseries
    feat_params : str, optional
        filename of a text (.csv) file containing parameters of the FEAT preprocessing
    TR : float, optional
        repetition time, TR (seconds).  If not specified will be read from feat_params
    smooth : float, optional
       spatial smoothing (mm).  If not specified will be read from feat_params
    paradigm_hp : float, optional
        high pass filter cutoff (seconds).  If not specified will be read from feat_params
    brain_thresh : float, optional
        brain threshold (percent).  If not specified will be read from feat_params
    perfusion_subtract : bool, optional
        if True, perform ASL surround subtraction.  If not specified will be read from feat_params
    preprocess_case : str or None, optional
        placeholder that may be used in the future to apply different processing 
        to different cases
    slice_timing_file : str or None, optional
        slice timing file
    usan_vol : str or None, optional
        filename of alternative usan_vol for susan smoothing.  if not specified
        mean_func is used as in Feat
    output_tar : bool, optional
        compress the feat preprocessing folder into a .tar.gz archive
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
        
    Returns
    -------
    filtered_func_data : str
        filename corresponding to the filtered functional data
    noise_est_file : str
        noise_est.txt file corresponding to filtered_func_data
    preproc_tarfile : str or None
        .tar.gz archive where results were stored
    feat_params_out : str
        filename of a .csv file containing the preprocessing options that were used
    prep_dir : str
        directory where the preprocessed results were stored        
        
    Notes
    -----
    Carries out spatial smoothing, bandpass temporal filtering, etc.
    
    """
    
    import os
    from os.path import join as pjoin
    
    import numpy as np
    import nibabel as nib

    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict, dict2csv, export_tarfile
    from cmind.utils.logging_utils import cmind_init_logging, cmind_func_info, log_cmd

    from cmind.pipeline.cmind_fMRI_preprocess2 import cmind_feat_prep

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, output_tar, perfusion_subtract = input2bool([generate_figures, ForceUpdate, verbose, output_tar, perfusion_subtract])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting fMRI Preprocessing: Stage 2")      
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    prep_dir = output_dir

    # Optional for LONI - When ASL input does not exist
    if func_file == "None":
        print("filtered_func_data:None")
        print("noise_est_file:None")
        print("preproc_tarfile:None")
        print("feat_params_out:None")
        print("prep_dir:{}".format(prep_dir))     
        return  

    if feat_params:
        if isinstance(feat_params,str):
            if os.path.exists(feat_params):
                #[f p ext]=fileparts(feat_params);
                #if(streq(ext,'.mat'))
                #    load(feat_params,'feat_params')
                #elif(streq(ext,'.txt'))
                #    feat_params_file=feat_params; clear feat_params
                feat_params=csv2dict(feat_params);
            else:
                raise IOError("feat_params must either be the feat parameter structure, or a path to the .txt file containing it")
        elif not isinstance(feat_params,dict):
            raise IOError("feat_params must either be the feat parameter structure, or a path to the .txt file containing it")

    #Initialize to any parameters found in feat_params.csv
    if feat_params:
        preproc_options=feat_params
    else:
        preproc_options={} #dictionary to store a copy of the preprocessing options that were used
    
    #If requested, override values in feat_params.csv or set a default if not feat_params.csv was provided
    if TR is None:
        if (feat_params and 'tr' in feat_params):
            TR=float(feat_params['tr'])
        else:
            TR=4.0;  
    preproc_options['tr']=TR
    
    if smooth is None:
        if (feat_params and 'smooth' in feat_params):
            smooth=float(feat_params['smooth'])
        else:
            smooth=6.0;  
    preproc_options['smooth']=smooth
    
    if paradigm_hp is None:
        if(feat_params and 'paradigm_hp' in feat_params):
            paradigm_hp=float(feat_params['paradigm_hp'])
        else:
            paradigm_hp=128.0;  
    preproc_options['paradigm_hp']=paradigm_hp
    if paradigm_hp<0 or paradigm_hp==np.Inf:
        preproc_options['temphp_yn']=0
    else:
        preproc_options['temphp_yn']=1
            
    if brain_thresh is None:
        if(feat_params and 'brain_thresh' in feat_params):
            brain_thresh=float(feat_params['brain_thresh'])
        else:
            brain_thresh=7.0;  
    preproc_options['brain_thresh']=brain_thresh
            
    if perfusion_subtract is None:
        if(feat_params and 'perfsub_yn' in feat_params):
            perfusion_subtract=input2bool(feat_params['perfsub_yn'])
        else:
            perfusion_subtract=False;    
    if perfusion_subtract:
        preproc_options['perfsub_yn']=1
    else:
        preproc_options['perfsub_yn']=0
        
#    if paradigm_hp < np.Inf:
#        prep_dir = 'prep_s%02d' % (int(smooth)) + '_bptf%04d' % (int(paradigm_hp)) 
#    else:
#        prep_dir = 'prep_s%02d' % (int(smooth))
#        
#    prep_dir = pjoin(output_dir,prep_dir)
#    if not os.path.exists(prep_dir):
#        os.makedirs(prep_dir)  
#    elif ForceUpdate: #remove old stats folder
#        shutil.rmtree(prep_dir)
#        os.makedirs(prep_dir)
            
    if output_tar:
        #preproc_tarfile = pjoin(output_dir,prep_dir+'.tar.gz')
        preproc_tarfile = pjoin(output_dir,'prep.tar.gz')
    else:
        preproc_tarfile = None
    
    #preprocess_case_substr = preprocess_case.replace('Feat_','')
    
    #combined_flag = ~isempty(strfind(preprocess_case_substr,'EchoCombined'));
    #preprocess_case_filestr=preprocess_case_substr.replace('DE2','DE').replace('SE2','SE')
    fexists=imexist(func_file,'nifti-1')[0]
    
    if not fexists:  
        raise IOError("specified func_file, %s, doesn't exist" % (func_file))    
    else:
        fvol=nib.load(func_file)
        if len(fvol.shape)!=4:
            raise ValueError("Specified func_vol data was not 4D")
        else:
            npts=fvol.shape[-1]
            
    preproc_options['npts']=npts  #store number of timepoints into the feat parameters
    preproc_options['level']=1    #1st-level analysis
    preproc_options['inmelodic']=0  #not in melodic
    preproc_options['melodic_yn']=0 # not running melodic
    
    if slice_timing_file: #optional slice timing correction
        #Note:  FSL 4.1.9+ use t=0 as the center of the TR.  prior versions used 0.5.  
        if not os.path.exists(slice_timing_file):
            raise IOError("Slice timing file, %s, doesn't exist!" % slice_timing_file) 
        preproc_options['st']=4
        preproc_options['st_file']='"'+slice_timing_file+'"'
    else:
        preproc_options['st']=0
        preproc_options['st_file']='""'
        
    feat_params_out = pjoin(prep_dir,'cmind_fMRI_preprocess2_options.csv')
    dict2csv(preproc_options,feat_params_out)
                          
    if usan_vol is not None:
        if not imexist(usan_vol)[0]:
            raise IOError("USAN file, %s, doesn't exist!" % usan_vol) 
        pct=log_cmd('$FSLDIR/bin/fslstats "%s" -p 99' % usan_vol, verbose=verbose, logger=module_logger)
        susan_bt_usan=0.33*float(pct.strip())
    else:
        susan_bt_usan=None
            
    if (not imexist(pjoin(prep_dir,'filtered_func_data'),'nifti-1')[0]) or ForceUpdate:
        cmind_feat_prep(prep_dir,func_file,TR=TR,smooth=smooth,paradigm_hp=paradigm_hp, 
                        brain_thresh=brain_thresh,use_OTSU=False,
                        perfusion_subtract=perfusion_subtract,
                        slice_timing_file=slice_timing_file,
                        verbose=verbose,logger=module_logger,
                        usan_vol=usan_vol, susan_bt_usan=susan_bt_usan)
        np.savetxt(pjoin(prep_dir,'smooth_mm.txt'),np.asarray([smooth,]),fmt='%0.7g')
        np.savetxt(pjoin(prep_dir,'TR.txt'),np.asarray([TR,]),fmt='%0.7g')
        np.savetxt(pjoin(prep_dir,'paradigm_hp.txt'),np.asarray([paradigm_hp,]),fmt='%0.7g')
        np.savetxt(pjoin(prep_dir,'brain_thresh.txt'),np.asarray([brain_thresh,]),fmt='%0.7g')
    
#    #copy (or symlink) all files from preprocessing dir to the main FEAT directory
#    try:
#        cmind_copy_all(prep_dir, output_dir,match_str='*',use_symlinks=True, overwrite_existing=True)
#    except:
#        warnings.warn("Unable to generate symbolic links.  copying files instead...")
#        cmind_copy_all(prep_dir, output_dir,match_str='*',use_symlinks=False, overwrite_existing=True)
    
    if preproc_tarfile:
        if (not os.path.exists(preproc_tarfile)) or ForceUpdate:
            export_tarfile(preproc_tarfile, prep_dir, arcnames='') #tar prep_dir into output_tar.  using arcnames='' to remove the absolute path!

    filtered_func_data=pjoin(prep_dir,'filtered_func_data.nii.gz')
    noise_est_file=pjoin(prep_dir,'noise_est.txt')
    
    module_logger.info("Finished fMRI Preprocessing: Stage 2")      
    
    #print any filename outputs for capture by LONI
    print("filtered_func_data:{}".format(filtered_func_data))
    print("noise_est_file:{}".format(noise_est_file))
    print("preproc_tarfile:{}".format(preproc_tarfile))
    print("feat_params_out:{}".format(feat_params_out))
    print("prep_dir:{}".format(prep_dir))
    return (filtered_func_data, 
            noise_est_file, 
            preproc_tarfile, 
            feat_params_out, 
            prep_dir)


def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_ASLBOLD_dir
    subj_dir=pjoin(cmind_example_output_dir,'F')

    
    feat_params=pjoin(cmind_ASLBOLD_dir,'feat_params.csv')
    slice_timing_file=[] #'/media/Data1/src_repositories/svn_stuff/cmind-matlab/trunk/ASLBOLD/slice_timing_ASL.txt'
    output_tar=True
    generate_figures=True
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
       
    if verbose:
        func=cmind_timer(cmind_fMRI_preprocess2,logger=logger)
    else:
        func=cmind_fMRI_preprocess2
    
    paradigms=['Sentences','Stories']
    acq_types=['ASL','BOLD']
    for paradigm in paradigms:
        for acq_type in acq_types:
            output_dir=pjoin(subj_dir,'ASLBOLD_%s' % paradigm,'Feat_%s' % acq_type)
            usan_vol=pjoin(subj_dir,'ASLBOLD_%s' % paradigm,'MeanSub_ASL_'+paradigm+'_mcf_N4.nii.gz')
            #preprocess_case='Feat_%s_%s' % (acq_type,paradigm) 
            preprocess_case=None
            func_file=pjoin(subj_dir,'ASLBOLD_%s' % paradigm,'%s_%s_mcf.nii.gz' % (acq_type,paradigm))
            func(output_dir, func_file, feat_params, 
                 preprocess_case=preprocess_case, 
                 slice_timing_file=slice_timing_file, usan_vol=usan_vol, 
                 output_tar=output_tar, generate_figures=generate_figures, 
                 ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)

 
        
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="FMRI Preprocessing: Stage 2", epilog="")  #-h, --help exist by default
        #example of a positional argument
        
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-i","--func_file", required=True, help="filename of the 4D NIFTI/NIFTI-GZ fMRI timeseries")
        parser.add_argument("-params","--feat_params", type=str, help="filename of a text file containing parameters of the FEAT preprocessing")
        parser.add_argument("--TR", type=str, default="None", help="repetition time, TR (seconds).  If not specified will be read from feat_params")
        parser.add_argument("--smooth", type=str, default="None", help="spatial smoothing (mm).  If not specified will be read from feat_params")
        parser.add_argument("--paradigm_hp", type=str, default="None", help="high pass filter cutoff (seconds).  If not specified will be read from feat_params")
        parser.add_argument("--brain_thresh", type=str, default="None", help="brain threshold (percent).  If not specified will be read from feat_params")
        parser.add_argument("--perfusion_subtract", type=str, default="False", help="if True, perform ASL surround subtraction.  If not specified will be read from feat_params")
        parser.add_argument("--preprocess_case", help="[Feat_BOLD_Sentences,Feat_BOLD_Stories,Feat_ASL_Sentences,Feat_ASL_Stories]; may be used in future to apply different processing to different cases")
        parser.add_argument("--slice_timing_file", type=str, default="None", help="slice timing file")
        parser.add_argument("-usan", "--usan_vol", type=str, default='', help="alternate usan_vol for susan smooth")
        parser.add_argument("--output_tar", type=str, default="true", help="compress the feat preprocessing folder into a .tar.gz archive")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")        
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
     
    
        output_dir=args.output_dir
        func_file=args.func_file
        feat_params=args.feat_params
        TR=str2none(args.TR)
        smooth=str2none(args.smooth)
        paradigm_hp=str2none(args.paradigm_hp)
        brain_thresh=str2none(args.brain_thresh)
        perfusion_subtract=args.perfusion_subtract
        preprocess_case=args.preprocess_case
        output_tar=args.output_tar
        generate_figures=args.generate_figures
        slice_timing_file=str2none(args.slice_timing_file,none_strings=['none','default','[]'])
        usan_vol=str2none(args.usan_vol)
        ForceUpdate=args.ForceUpdate
        verbose=args.verbose
        logger=args.logger
        
        if verbose:
            cmind_fMRI_preprocess2=cmind_timer(cmind_fMRI_preprocess2,logger=logger)
    
        cmind_fMRI_preprocess2(output_dir, func_file, feat_params, TR=TR, 
                               smooth=smooth, paradigm_hp=paradigm_hp,
                               brain_thresh=brain_thresh, 
                               perfusion_subtract=perfusion_subtract, 
                               preprocess_case=preprocess_case, 
                               slice_timing_file=slice_timing_file, 
                               usan_vol=usan_vol, output_tar=output_tar, 
                               generate_figures=generate_figures,
                               ForceUpdate=ForceUpdate, verbose=verbose, 
                               logger=logger)
#TODO:  add option for antsMotionCorr in place of mcflirt
        #see example at:  https://github.com/stnava/ANTs/blob/master/Scripts/antsMotionCorrExample
        