#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

#TODO:  could just take a list of any lowres volumes as input instead of T1/CBF explicitly

def cmind_T1_tissue_averages(output_dir,T1_map_file, NRMSE_file, GMpve,WMpve,CSFpve, CBF_quant_file=None, CBF_percent_file=None, HRtoCBF_affine=None,HRtoCBF_warp=None, PVEthresh_pct=75.0, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """calculate average T1 value for each tissue type (GM, WM, CSF)
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    T1_map_file : str
        image file containing the T1map fitting results
    NRMSE_file : str
        normalized root-mean-square error (NRMSE) image from T1_map fit
    GMpve : str
        GM partial volume estimate from structural processing
    WMpve : str
        WM partial volume estimate from structural processing
    CSFpve : str
        CSF partial volume estimate from structural processing
    CBF_quant_file : str or None, optional
        quantitative ASL image
    CBF_percent_file : str or None, optional
        ASL control-tag signal percentage image
    HRtoCBF_affine : str or None, optional
        affine transform:  structural->BaselineCBF
    HRtoCBF_warp : str or None, optional
        warp:  structural->BaselineCBF
    PVEthresh_pct : float, optional
        threshold PVE maps by this percentage.  Valid range: (0 100]
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    T1_avg_file : str
    CBF_avg_file : str or None
    
    """
    
    import os
    from os.path import join as pjoin
    
    import nibabel as nib
    import numpy as np
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, dict2csv
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting T1 tissue averaging")      

    if (PVEthresh_pct>100) or (PVEthresh_pct<=0):
        raise ValueError("PVE threshold must be between 0 and 100")
            
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    o=output_dir

    #T1_map_file=pjoin(CBF_dir,'T1_map_reg2CBF.nii.gz');                              
    (imbool, T1_map_file)=imexist(T1_map_file,'nifti-1')[0:2];  
    if not imbool:
        raise IOError("T1_map_file, %s, missing" % T1_map_file)
        
    if HRtoCBF_affine and (not os.path.exists(HRtoCBF_affine)):
        raise IOError("HRtoCBF_affine, %s, missing" % HRtoCBF_affine)
        
    if HRtoCBF_warp and (not imexist(HRtoCBF_warp)[0]):  #TODO: use warp instead of affine if present
        raise IOError("HRtoCBF_warp, %s, missing" % HRtoCBF_warp)

    if (not HRtoCBF_affine) and (not HRtoCBF_warp):
        raise IOError("Either HRtoCBF_affine or HRtoCBF_warp must be specified")
    
    #HRtoCBF_affine=pjoin(CBF_reg2T1_dir,'highres2lowres.mat');                
    #if (not os.path.exists(HRtoCBF_affine)): 
    #    HRtoCBF_affine=pjoin(CBF_reg2T1_dir,'lowres2highres_inv.mat');              
    #    if (not os.path.exists(HRtoCBF_affine)): 
    #        raise IOError("HRtoCBF_affine, %s, missing" % HRtoCBF_affine)
    #    
    #T1_brainmask_file=pjoin(T1segment_dir,'T1W_Structural_N4_brain_mask.nii.gz');    
    #(imbool, T1_brainmask_file)=imexist(T1_brainmask_file,'nifti-1')[0:2];  
    
    #CSFpve=pjoin(T1segment_dir,'T1Segment_CSF.nii.gz');                              
    (imbool, CSFpve)=imexist(CSFpve,'nifti-1')[0:2]; 
    if not imbool:
        raise IOError("CSFpve, %s, missing" % CSFpve)
        
    #GMpve=pjoin(T1segment_dir,'T1Segment_GM.nii.gz');                                
    (imbool, GMpve)=imexist(GMpve,'nifti-1')[0:2];  
    if not imbool:
        raise IOError("GMpve, %s, missing" % GMpve)

    #WMpve=pjoin(T1segment_dir,'T1Segment_WM.nii.gz');                                
    (imbool, WMpve)=imexist(WMpve,'nifti-1')[0:2]; 
    if not imbool:
        raise IOError("WMpve, %s, missing" % WMpve)
        raise
        
    #NRMSE_file=pjoin(T1map_dir,'T1_NRMSE_map.nii.gz');                               
    (imbool, NRMSE_file)=imexist(NRMSE_file,'nifti-1')[0:2];
    if not imbool:
        raise IOError("NRMSE_file, %s, missing" % NRMSE_file)
        
    if CBF_quant_file:
        (imbool, CBF_quant_file)=imexist(CBF_quant_file,'nifti-1')[0:2];  
        if not imbool:
            raise IOError("CBF_quant_file not found!" % CBF_quant_file)

    if (not os.path.exists(pjoin(output_dir,'mean_T1s_NRMSE_masked.txt'))) or ForceUpdate:
        
        if HRtoCBF_warp:  #with fieldmap correction
            log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/GMpve2CBF" -w "%s" --interp=trilinear --super --superlevel=a' % (GMpve,T1_map_file,o,HRtoCBF_warp), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/WMpve2CBF" -w "%s" --interp=trilinear --super --superlevel=a' % (WMpve,T1_map_file,o,HRtoCBF_warp), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/CSFpve2CBF" -w "%s" --interp=trilinear --super --superlevel=a' % (CSFpve,T1_map_file,o,HRtoCBF_warp), verbose=verbose, logger=module_logger)
        else:  #without fieldmap correction
            log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/GMpve2CBF" --postmat="%s"  --interp=trilinear --super --superlevel=a' % (GMpve,T1_map_file,o,HRtoCBF_affine), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/WMpve2CBF" --postmat="%s"  --interp=trilinear --super --superlevel=a' % (WMpve,T1_map_file,o,HRtoCBF_affine), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/CSFpve2CBF" --postmat="%s"  --interp=trilinear --super --superlevel=a' % (CSFpve,T1_map_file,o,HRtoCBF_affine), verbose=verbose, logger=module_logger)
            
        log_cmd('$FSLDIR/bin/fslmaths "%s/GMpve2CBF" -thr %f "%s/GMmask"' % (o,PVEthresh_pct/100.0,o), verbose=verbose, logger=module_logger);
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mas "%s/GMmask" "%s/T1_map_reg2CBF_GMmask"' % (T1_map_file,o,o), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/fslstats "%s/T1_map_reg2CBF_GMmask" -M > "%s/GM_avgT1.txt"' % (o,o), verbose=verbose, logger=module_logger);
            
        log_cmd('$FSLDIR/bin/fslmaths "%s/WMpve2CBF" -thr %f "%s/WMmask"' % (o,PVEthresh_pct/100.0,o), verbose=verbose, logger=module_logger);
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mas "%s/WMmask" "%s/T1_map_reg2CBF_WMmask"' % (T1_map_file,o,o), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/fslstats "%s/T1_map_reg2CBF_WMmask" -M > "%s/WM_avgT1.txt"' % (o,o), verbose=verbose, logger=module_logger);
        
        log_cmd('$FSLDIR/bin/fslmaths "%s/CSFpve2CBF" -thr %f "%s/CSFmask"' % (o,PVEthresh_pct/100.0,o), verbose=verbose, logger=module_logger);
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mas "%s/CSFmask" "%s/T1_map_reg2CBF_CSFmask"' % (T1_map_file,o,o), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/fslstats "%s/T1_map_reg2CBF_CSFmask" -M > "%s/CSF_avgT1.txt"' % (o,o), verbose=verbose, logger=module_logger);
        
        T1vox_GM_nii=nib.load(pjoin(o,'T1_map_reg2CBF_GMmask.nii.gz'))
        T1vox_GM=T1vox_GM_nii.get_data()
        T1vox_WM=nib.load(pjoin(o,'T1_map_reg2CBF_WMmask.nii.gz'))
        T1vox_WM=T1vox_WM.get_data()
        T1vox_CSF=nib.load(pjoin(o,'T1_map_reg2CBF_CSFmask.nii.gz'))
        T1vox_CSF=T1vox_CSF.get_data()
        
        if generate_figures and CBF_percent_file:
            #CBF_percent_file=pjoin(CBF_dir,'BaselineCBF_mcf_percent_change.nii.gz');  
            (imbool, CBF_percent_file)=imexist(CBF_percent_file,'nifti-1')[0:2];  
            if not imbool:
                raise IOError("CBF_percent_file, %s, missing!" % CBF_percent_file)

            #make color overlays of the voxel masks
            log_cmd('$FSLDIR/bin/overlay 0 0 %s -a "%s/GMmask" 0.1 1 "%s/ov"' % (CBF_percent_file,o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/ov" -s 2 -a "%s/overlay_GM.png"' % (o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/overlay 0 0 %s -a "%s/WMmask" 0.1 1 "%s/ov"' % (CBF_percent_file,o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/ov" -s 2 -a "%s/overlay_WM.png"' % (o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/overlay 0 0 %s -a "%s/CSFmask" 0.1 1 "%s/ov"' % (CBF_percent_file,o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/ov" -s 2 -a "%s/overlay_CSF.png"' % (o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/pngappend "%s/overlay_WM.png" - "%s/overlay_GM.png" - "%s/overlay_CSF.png" "%s/overlays.png"' % (o,o,o,o), verbose=verbose, logger=module_logger)
            os.remove(pjoin(o,'overlay_WM.png'))
            os.remove(pjoin(o,'overlay_GM.png'))
            os.remove(pjoin(o,'overlay_CSF.png'))
            log_cmd('$FSLDIR/bin/imrm "%s/ov"' % o)

            #cmind_reg_report_img(pjoin(pwd,'BaselineCBF_mcf_percent_change.nii.gz'),pjoin(pwd,'GMmask.nii.gz'), 'CBF_GM_mask_reg.png', '/tmp/reg',3, '-i -0.5 2')

        #if BaselineCBF_mean_file and NRMSE_file:        
        #    log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/T1_NRMSE_map_reg2CBF" --premat="%s"  --interp=spline' % (NRMSE_file,BaselineCBF_mean_file,o,T1concat2CBF_affine), verbose=verbose, logger=module_logger)
        #    NRMSE_map=nib.load(pjoin(o,'T1_NRMSE_map_reg2CBF.nii.gz')); 
        #    NRMSE_map=NRMSE_map.get_data()
        #else:
        #Assume NRMSE map is already registered to the T1 and I0 maps
        NRMSE_map=nib.load(NRMSE_file);             
        NRMSE_map=NRMSE_map.get_data()
        #NRMSE_map=single(NRMSE_map.img);
        
        
            #figure,subplot(311),hist(T1vox_WM(T1vox_WM~=0),100); axis([0 4 0 max(n)])%h=findobj(gca,'Type','patch'),set(h,'FaceColor','r','EdgeColor','k')
            #subplot(312),[n,x]=hist(T1vox_GM(T1vox_GM~=0),100);
            #subplot(313),[n,x]=hist(T1vox_CSF(T1vox_CSF~=0),100);
            
        
        WMmask=T1vox_WM>0;
        NRMSE_WM=NRMSE_map[WMmask];
        WMmask2=(NRMSE_map<=np.percentile(NRMSE_WM,q=PVEthresh_pct)) * (T1vox_WM>0)
        WMmask_nii=nib.load(pjoin(o,'WMmask.nii.gz')); 
        mask_affine=WMmask_nii.get_affine()
        mask_hdr=WMmask_nii.get_header()
        WMmask2_nii=nib.Nifti1Image(WMmask2,affine=mask_affine,header=mask_hdr)
        WMmask2_nii.to_filename(pjoin(o,'WMmask2.nii.gz'))
        
        #WMvec=T1vox_WM[WMmask];
        WMvec2=T1vox_WM[WMmask2];
        GMmask=T1vox_GM>0;
        NRMSE_GM=NRMSE_map[GMmask];
        GMmask2=(NRMSE_map<=np.percentile(NRMSE_GM,q=PVEthresh_pct)) * (T1vox_GM>0);
        GMmask2_nii=nib.Nifti1Image(GMmask2,affine=mask_affine,header=mask_hdr)
        GMmask2_nii.to_filename(pjoin(o,'GMmask2.nii.gz'))

        #GMvec=T1vox_GM[GMmask];
        GMvec2=T1vox_GM[GMmask2];
        CSFmask=T1vox_CSF>0;
        NRMSE_CSF=NRMSE_map[CSFmask];
        CSFmask2=(NRMSE_map<=np.percentile(NRMSE_CSF,q=PVEthresh_pct)) * (T1vox_CSF>0);
        CSFmask2_nii=nib.Nifti1Image(CSFmask2,affine=mask_affine,header=mask_hdr)
        CSFmask2_nii.to_filename(pjoin(o,'CSFmask2.nii.gz'))
        
        #CSFvec=T1vox_CSF[CSFmask];
        CSFvec2=T1vox_CSF[CSFmask2];
        #mean_vals=np.array([np.mean(WMvec), np.mean(GMvec), np.mean(CSFvec)])
        mean_vals2=np.array([np.mean(WMvec2), np.mean(GMvec2), np.mean(CSFvec2)])
        std_vals2=np.array([np.std(WMvec2,ddof=1), np.std(GMvec2,ddof=1), np.std(CSFvec2,ddof=1)])
        T1_avg_file=pjoin(o,'mean_T1s_NRMSE_masked.txt')
        np.savetxt(T1_avg_file,mean_vals2,delimiter='\t',fmt='%0.7g')

        if generate_figures and CBF_percent_file:
            #CBF_percent_file=pjoin(CBF_dir,'BaselineCBF_mcf_percent_change.nii.gz');  
            (imbool, CBF_percent_file)=imexist(CBF_percent_file,'nifti-1')[0:2];  
            if not imbool:
                raise IOError("CBF_percent_file, %s, missing!" % CBF_percent_file)

            #make color overlays of the voxel masks
            log_cmd('$FSLDIR/bin/overlay 0 0 %s -a "%s/GMmask2" 0.1 1 "%s/ov"' % (CBF_percent_file,o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/ov" -s 2 -a "%s/overlay_GM.png"' % (o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/overlay 0 0 %s -a "%s/WMmask2" 0.1 1 "%s/ov"' % (CBF_percent_file,o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/ov" -s 2 -a "%s/overlay_WM.png"' % (o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/overlay 0 0 %s -a "%s/CSFmask2" 0.1 1 "%s/ov"' % (CBF_percent_file,o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/ov" -s 2 -a "%s/overlay_CSF.png"' % (o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/pngappend "%s/overlay_WM.png" - "%s/overlay_GM.png" - "%s/overlay_CSF.png" "%s/overlays_NRMSEthresh.png"' % (o,o,o,o), verbose=verbose, logger=module_logger)
            os.remove(pjoin(o,'overlay_WM.png'))
            os.remove(pjoin(o,'overlay_GM.png'))
            os.remove(pjoin(o,'overlay_CSF.png'))
            log_cmd('$FSLDIR/bin/imrm "%s/ov"' % o)
            
        if generate_figures:
            import matplotlib as mpl
            mpl.use('agg') #don't plot to screen
            import matplotlib.pyplot as plt
            #h=figure('Visible','off');
            use_LaTeX=False
            if use_LaTeX:
                mpl.rc('text', usetex=True)  #enable LaTeX usage in text
            f=plt.figure()
            T1_min=0.5
            T1_max=3;
            #histtype='bar'
            #(n,bins,patches)=plt.hist([T1vox_WM[np.nonzero(T1vox_WM)].ravel(),T1vox_GM[np.nonzero(T1vox_GM)].ravel(),T1vox_CSF[np.nonzero(T1vox_CSF)].ravel()],48,range=(T1_min,T1_max),histtype=histtype)
            histtype='step'
            (n,bins,patches)=plt.hist([T1vox_WM[np.nonzero(T1vox_WM)].ravel(),T1vox_GM[np.nonzero(T1vox_GM)].ravel(),T1vox_CSF[np.nonzero(T1vox_CSF)].ravel()],128,range=(T1_min,T1_max),histtype=histtype)
            plt.axis((T1_min, T1_max, 0, np.max(n)))
            plt.legend(('CSF (%0.3g +/- %0.3g)' % (mean_vals2[2],std_vals2[2]),'GM (%0.3g +/- %0.3g)' % (mean_vals2[1],std_vals2[1]),'WM (%0.3g +/- %0.3g)' % (mean_vals2[0],std_vals2[0])),loc='upper right')
            f.savefig(pjoin(o,'T1histograms.png'))
            
                            
        #TODO: move the CBF_averages stuff below here into cmind_baselineCBF_quantitative                 
        #log_cmd('$FSLDIR/bin/flirt -in "%s" -ref "%s" -out CSF_HRtoLR -applyxfm -init "%s" -interp sinc' % (CSF_input,T1_map_file,HRtoCBF_affine), verbose=verbose, logger=module_logger)
        #log_cmd('$FSLDIR/bin/flirt -in "%s" -ref "%s" -out GM_HRtoLR -applyxfm -init "%s" -interp sinc' % (GMpve,T1_map_file,HRtoCBF_affine), verbose=verbose, logger=module_logger)
        #log_cmd('$FSLDIR/bin/flirt -in "%s" -ref "%s" -out WM_HRtoLR -applyxfm -init "%s" -interp sinc' % (WMpve,T1_map_file,HRtoCBF_affine), verbose=verbose, logger=module_logger)
            
        log_cmd('$FSLDIR/bin/fslmaths "%s/GMpve2CBF" -thr 0.75 -bin "%s/GM_HRtoLR_75pct"' % (o,o), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/fslmaths "%s/WMpve2CBF" -thr 0.75 -bin "%s/WM_HRtoLR_75pct"' % (o,o), verbose=verbose, logger=module_logger)
        GM_mask=nib.load(pjoin(o,'GM_HRtoLR_75pct.nii.gz'))
        GM_mask=GM_mask.get_data()>0
        WM_mask=nib.load(pjoin(o,'WM_HRtoLR_75pct.nii.gz'))
        WM_mask=WM_mask.get_data()>0
        
        #mask_img=WMmask2+2*GMmask2+3*CSFmask2
        
        if CBF_quant_file:
            CBF_nii=nib.load(CBF_quant_file)
            CBF_nii=CBF_nii.get_data();
            
            CBF_averages={}
            CBF_averages['GM_CBF']=np.mean(CBF_nii[GM_mask]);
            CBF_averages['GM_CBF_std']=np.std(CBF_nii[GM_mask]);
            CBF_averages['WM_CBF']=np.mean(CBF_nii[WM_mask]);
            CBF_averages['WM_CBF_std']=np.std(CBF_nii[WM_mask]);
            CBF_averages['ratio_CBF']=CBF_averages['GM_CBF']/CBF_averages['WM_CBF'];
            CBF_avg_file=pjoin(o,'CBF_averages.csv')
            dict2csv(CBF_averages,CBF_avg_file)
        
            save_averages=True
            if save_averages:
                import pickle as pickle
                with open(pjoin(o,"CBF_averages.pickle"), "wb") as f:   
                    pickle.dump(CBF_averages,f)
        else:
            CBF_avg_file=None
                  
        writeHTML=True  
        if writeHTML:
            from cmind.utils.cmind_HTML_report_gen import cmind_HTML_reports
            cmind_HTML_reports('T1histogram',output_dir,report_dir=None,ForceUpdate=ForceUpdate);
            
        return (T1_avg_file, CBF_avg_file)

def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    output_dir=pjoin(cmind_example_output_dir,'P','T1histogram')
    T1_map_file=pjoin(cmind_example_output_dir,'P','BaselineCBF','T1_map_reg2CBF.nii.gz')
    NRMSE_file=pjoin(cmind_example_output_dir,'P','BaselineCBF','T1_NRMSE_map_reg2CBF.nii.gz')
    CBF_percent_file=pjoin(cmind_example_output_dir,'P','BaselineCBF','BaselineCBF_mcf_percent_change.nii.gz')
    HRtoCBF_affine=pjoin(cmind_example_output_dir,'P','BaselineCBF','reg_BBR_fmap','lowres2highres_inv.mat')
    HRtoCBF_warp=pjoin(cmind_example_output_dir,'P','BaselineCBF','reg_BBR_fmap','lowres2highres_warp_inv.nii.gz')
    GMpve=pjoin(cmind_example_output_dir,'P','T1_proc','T1Segment_GM.nii.gz')
    WMpve=pjoin(cmind_example_output_dir,'P','T1_proc','T1Segment_WM.nii.gz')
    CSFpve=pjoin(cmind_example_output_dir,'P','T1_proc','T1Segment_CSF.nii.gz')
    CBF_quant_file=pjoin(cmind_example_output_dir,'P','BaselineCBF','CBF_Wang2002.nii.gz')
    PVEthresh_pct=75.0;
    ForceUpdate=True;
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
    
    if verbose:
        func=cmind_timer(cmind_T1_tissue_averages,logger=logger)
    else:
        func=cmind_T1_tissue_averages

    func(output_dir,T1_map_file,NRMSE_file,GMpve,WMpve,CSFpve,
         CBF_quant_file=CBF_quant_file, CBF_percent_file=CBF_percent_file, 
         HRtoCBF_affine=HRtoCBF_affine, HRtoCBF_warp=HRtoCBF_warp, 
         PVEthresh_pct=PVEthresh_pct, ForceUpdate=ForceUpdate, verbose=verbose, 
         logger=logger)

        
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Calculate average T1 values for each tissue type (GM, WM, CSF)", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("--T1_map_file", help="image file containing the T1map fitting results")
        parser.add_argument("--NRMSE_file", help="normalized root-mean-square error (NRMSE) image from T1_map fit")
        parser.add_argument("--GMpve", help="GM partial volume estimate from structural processing")
        parser.add_argument("--WMpve", help="WM partial volume estimate from structural processing")
        parser.add_argument("--CSFpve", help="CSF partial volume estimate from structural processing")
        parser.add_argument("--CBF_quant_file", type=str, default='none', help="quantitative ASL image")
        parser.add_argument("--CBF_percent_file", type=str, default='none', help="ASL control-tag signal percentage image")
        parser.add_argument("--HRtoCBF_affine", type=str, default='none', help="affine transform:  structural->BaselineCBF")
        parser.add_argument("--HRtoCBF_warp", type=str, default='none', help="warp:  structural->BaselineCBF")
        parser.add_argument("--PVEthresh_pct", type=float, default=75.0, help="threshold PVE maps by this percentage.  Valid range: (0 100]")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
          
        
        args=parser.parse_args()
        
        output_dir=args.output_dir
        T1_map_file=args.T1_map_file
        NRMSE_file=args.NRMSE_file
        GMpve=args.GMpve
        WMpve=args.WMpve
        CSFpve=args.CSFpve
        CBF_quant_file=str2none(args.CBF_quant_file)
        CBF_percent_file=str2none(args.CBF_percent_file)
        HRtoCBF_affine=str2none(args.HRtoCBF_affine)
        HRtoCBF_warp=str2none(args.HRtoCBF_warp)
        PVEthresh_pct=args.PVEthresh_pct
        generate_figures=args.generate_figures
        ForceUpdate=args.ForceUpdate
        verbose=args.verbose
        logger=args.logger
        
        if verbose:
            cmind_T1_tissue_averages=cmind_timer(cmind_T1_tissue_averages,logger=logger)

        cmind_T1_tissue_averages(output_dir,T1_map_file,NRMSE_file,GMpve,WMpve,
                                 CSFpve,CBF_quant_file=CBF_quant_file, 
                                 CBF_percent_file=CBF_percent_file, 
                                 HRtoCBF_affine=HRtoCBF_affine, 
                                 HRtoCBF_warp=HRtoCBF_warp, 
                                 PVEthresh_pct=PVEthresh_pct, 
                                 ForceUpdate=ForceUpdate, verbose=verbose, 
                                 logger=logger)
