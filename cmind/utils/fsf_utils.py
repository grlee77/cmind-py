"""
**FSL .fsf file creation/modification utilities**

create_fsf
create_con
create_ev
write_fsf
fsf_substitute
"""
from __future__ import division, print_function, absolute_import
import os
#from collections import OrderedDict
import numpy as np


def create_fsf(vers=6.0):
    """ Populates FEAT fsf structure with default values

        
    Parameters
    ----------
    vers : float
        Feat version to create the .fsf file for
    
    Returns
    -------
    fsf : dict
        fsf structure
    
    See Also
    --------
    write_fsf
    
    Notes
    -----
    04/22/05 PJ
    Modified by GRL to update to feat v6.0
    
    """
    #if kwargs.has_key('version'):
    #    vers=kwargs['version']
    
    if (vers!=5.4) and (vers!=5.98) and (vers!=6.0):
        print('warning: unknown version, %f, version set to 6.0' % vers)
        vers = 6.0;
    
    #fsf=OrderedDict()
    fsf={}
    
    # FEAT version number
    fsf['version']=vers;
    
    # Are we in MELODIC?
    fsf['inmelodic']=0;  #GRL
    
    # Analysis level
    # 1 : First-level analysis
    # 2 : Higher-level analysis
    fsf['level']=1;
    
    # Which stages to run
    # 0 : No first-level analysis (registration and/or group stats only)
    # 7 : Full first-level analysis
    # 1 : Pre-Stats
    # 3 : Pre-Stats + Stats
    # 2 :             Stats
    # # # # version 5.4
    # 6 :             Stats + Contrasts, Thresholding, Rendering
    # 4 :                     Contrasts, Thresholding, Rendering
    # # # # version 5.98
    # 6:              Stats + Post-Stats
    # 4:                      Post-Stats
    fsf['analysis']=6;
    
    # Use relative filenames
    fsf['relative_yn']=0;
    
    # Balloon help
    fsf['help_yn']=1;
    
    # Run Featwatcher
    fsf['featwatcher_yn']=1;
    
    # Cleanup first-level standard-space images
    fsf['sscleanup_yn']=0;
    
    # Output directory
    fsf['outputdir']='';
    
    # TR(s)
    fsf['tr']=[];
    
    # Total volumes
    fsf['npts']=[];
    
    # Delete volumes
    fsf['ndelete']=0;
    
    # Number of first-level analyses
    fsf['multiple']=1;  # Not a flag - actual num of analyses
    
    # Higher-level input type
    # 1 : Inputs are lower-level FEAT directories
    # 2 : Inputs are cope images from FEAT directories
    fsf['inputtype']=1;
    
    # Carry out pre-stats processing?
    fsf['filtering_yn']=1;
    
    # Brain/background threshold, #
    fsf['brain_thresh']=10;
    
    # Post-stats-only directory copying
    # 0 : Overwrite original post-stats results
    # 1 : Copy original FEAT directory for new Contrasts, Thresholding, Rendering
    fsf['newdir_yn']=0;
    
    # Slice timing correction
    # 0 : None
    # 1 : Regular up (0, 1, 2, 3, ...)
    # 2 : Regular down
    # 3 : Use slice order file
    # 4 : Use slice timings file
    # 5 : Interleaved (0, 2, 4 ... 1, 3, 5 ... )
    fsf['st']=0;
    
    # Slice timings file
    fsf['st_file']='';
    
    # Motion correction
    # 0 : None
    # 1 : MCFLIRT
    fsf['mc']=1;
    
    # Spin-history (currently obsolete)
    fsf['sh_yn']=0;
    
    # BET brain extraction
    fsf['bet_yn']=1;
    
    # Spatial smoothing FWHM (mm)
    fsf['smooth']=5;
    
    # Intensity normalization
    fsf['norm_yn']=0;
    
    # Highpass temporal filtering
    fsf['temphp_yn']=0;
    
    # Lowpass temporal filtering
    fsf['templp_yn']=0;
    
    # MELODIC ICA data exploration
    fsf['melodic_yn']=0;
    
    # Carry out main stats?
    fsf['stats_yn']=1;
    
    # Carry out prewhitening?
    fsf['prewhiten_yn']=1;
    
    # Higher-level modelling
    # 3 : Fixed effects
    # 0 : Mixed Effects: Simple OLS
    # 2 : Mixed Effects: FLAME (stage 1 only)
    # 1 : Mixed Effects: FLAME (full)
    fsf['mixed_yn']=1;
    
    # Number of EVs
    fsf['evs_orig']=0;
    fsf['evs_real']=0;
    
    # Number of contrasts
    fsf['ncon_orig']=0;
    fsf['ncon_real']=0;
    
    # Number of F-tests
    fsf['nftests_orig']=0;
    fsf['nftests_real']=0;
    
    # Add constant column to design matrix? (obsolete)
    fsf['constcol']=0;
    
    # Carry out post-stats steps?
    fsf['poststats_yn']=1;
    
    # Pre-threshold masking?
    fsf['threshmask']='';
    
    # Thresholding
    # 0 : None
    # 1 : Uncorrected
    # 2 : Voxel
    # 3 : Cluster
    fsf['thresh']=2;
    
    # P threshold
    fsf['prob_thresh']=0.05;
    
    # Z threshold
    fsf['z_thresh']=2.3;
    
    # Z min/max for colour rendering
    # 0 : Use actual Z min/max
    # 1 : Use preset Z min/max
    fsf['zdisplay']=0;
    
    # Z min in colour rendering
    fsf['zmin']=2;
    
    # Z max in colour rendering
    fsf['zmax']=8;
    
    # Colour rendering type
    # 0 : Solid blobs
    # 1 : Transparent blobs
    fsf['rendertype']=1;
    
    # Background image for higher-level stats overlays
    # 1 : Mean highres
    # 2 : First highres
    # 3 : Mean functional
    # 4 : First functional
    # 5 : Standard space template
    fsf['bgimage']=1;
    
    # Registration?
    fsf['reg_yn']=1;
    
    # B0 fieldmap unwarping?
    fsf['regunwarp_yn']=0;
    
    # Registration to initial structural
    fsf['reginitial_highres_yn']=0;
    
    # Search space for registration to initial structural
    # 0   : No search
    # 90  : Normal search
    # 180 : Full search
    fsf['reginitial_highres_search']=90;
    
    # Degrees of Freedom for registration to initial structural
    fsf['reginitial_highres_dof']=12;
    
    # Registration to main structural
    fsf['reghighres_yn']=0;
    
    # Search space for registration to main structural
    # 0   : No search
    # 90  : Normal search
    # 180 : Full search
    fsf['reghighres_search']=90;
    
    # Degrees of Freedom for registration to main structural
    fsf['reghighres_dof']=12;
    
    # Registration to standard image?
    fsf['regstandard_yn']=1;
    
    # Standard image
    fsf['regstandard']='/usr/share/data/fsl-mni152-templates/MNI152_T1_2mm_brain.nii.gz';
    
    # Search space for registration to standard space
    # 0   : No search
    # 90  : Normal search
    # 180 : Full search
    fsf['regstandard_search']=90;
    
    # Degrees of Freedom for registration to standard space
    fsf['regstandard_dof']=12;
    
    # Do nonlinear registration to standard space?
    fsf['regstandard_nonlinear_yn']=0;
    
    # High pass filter cutoff
    fsf['paradigm_hp']=-1;
    
    # Number of lower-level copes feeding into higher-level analysis
    fsf['ncopeinputs']=0;
    fsf['copeinputs']=[];
    
    # 4D AVW data or FEAT directory (1)
    fsf['feat_files']=[];
    
    # Insert EV structure creation here
    fsf['ev']= create_ev();
    
    fsf['con']= {};  # create contrasts with create_con
    fsf['Ftest']= [];  #
    
    # Contrast & F-tests mode
    # real : control real EVs
    # orig : control original EVs
    fsf['con_mode_old']='orig';
    fsf['con_mode']='orig';
    
    # Read in default contrast structures if desired
    
    # Contrast masking - use >0 instead of thresholding?
    fsf['conmask_zerothresh_yn']=0;
    
    # This is an EV X EV matrix of what contrast should be masked with what.
    # Mask contrast index in row with contrast index in column
    # e.g. Mask real contrast/F-test 1 with real contrast/F-test 2?
    fsf['conmask_mtx']= [];
    
    # Do contrast masking at all?
    fsf['conmask1_1']=0;
    
    # Setup Orthogonalisation at higher level? 
    fsf['level2orth']= 0; 
    
    # Group membership for input #d
    fsf['groupmem']= [];
        
    if vers==5.4:
        
        # Delay before starting (hours)
        fsf['delay']=0;
        
        # Dwell/Asymmetry ratio 
        fsf['dwellasym']=-1;
    
        # Do nonlinear registration to initial structural?
        fsf['reginitial_highres_nonlinear_yn']=0;
    
        # Do nonlinear registration to main structural?
        fsf['reghighres_nonlinear_yn']=0;
    
    elif vers==5.98:
        
        # Perfusion tag/control order
        fsf['tagfirst']=1;
        
        # Critical z for design efficiency calculation
        fsf['critical_z']=5.3;
        
        # Noise level
        fsf['noise']= 0.66;
        
        # Noise AR(1) (temporal smoothness
        fsf['noisear']= 0.34;
        
        # EPI dwell time (ms)
        fsf['dwell']= 0.7;
        
        # EPI TE (ms)
        fsf['te']= 35;
        
        # Signal loss threshold
        fsf['signallossthresh']= 10;
        
        # Unwarp direction
        fsf['unwarp_dir']='y-';
        
        # Perfusion subtraction
        fsf['perfsub_yn']= 0;
        
        # Add motion parameters to model
        # 0 : No
        # 1 : Yes
        fsf['motionevs']= 0;
        
        # Robust outlier detection in FLAME?
        fsf['robust_yn']= 0;
        
        # additional parameter for Number of EVs
        fsf['evs_vox']= 0;
        
        # Create time series plots
        fsf['tsplot_yn']= 1;
    
        # Control nonlinear warp field resolution
        fsf['regstandard_nonlinear_warpres']= 10;
        
        # Add confound EVs text file
        fsf['confoundevs']= 0;
        
        # Alternative example_func image (not derive from input 4D dataset)
        fsf['alternative_example_func']='""'
        
        # Alternative (to BETting) mask image
        fsf['alternative_mask']='""';
        
        # Initial structural space registration initialisation transform
        fsf['init_initial_highres']='""';
        
        # Structural space reigstration initialisation transform
        fsf['init_highres']='""';
        
        # Standard space reigstration initialisation transform
        fsf['init_standard']='""';
        
        # For full FEAT analysis: overwrite existing .feat output dir?
        fsf['overwrite_yn']= 0;
        
    elif vers==6.00:
        
        # Perfusion tag/control order
        fsf['tagfirst']=1;
        
        # Critical z for design efficiency calculation
        fsf['critical_z']= 5.3;
        
        # Noise level
        fsf['noise']= 66;
        
        # Noise AR(1) (temporal smoothness
        fsf['noisear']= 0.34;
        
        # EPI dwell time (ms)
        fsf['dwell']= 0.7;
        
        # EPI TE (ms)
        fsf['te']= 35;
        
        # Signal loss threshold
        fsf['signallossthresh']= 10;
        
        # Unwarp direction
        fsf['unwarp_dir']='y-';
        
        # Perfusion subtraction
        fsf['perfsub_yn']= 0;
        
        # Add motion parameters to model
        # 0 : No
        # 1 : Yes
        fsf['motionevs']= 0;
        fsf['motionevsbeta']='""';
        fsf['scriptevsbeta']='""';
        
        # Use alternate reference images?
        fsf['alternateReference_yn']= 0;
        
        # Total voxels
        fsf['totalVoxels']= 0;
    
        # Robust outlier detection in FLAME?
        fsf['robust_yn']= 0;
        
        # additional parameter for Number of EVs
        fsf['evs_vox']= 0;
        
        # Create time series plots
        fsf['tsplot_yn']= 1;
    
        # Control nonlinear warp field resolution
        fsf['regstandard_nonlinear_warpres']= 10;
        
        # Degrees of Freedom for registration to main structural
        fsf['reghighres_dof']='BBR';
    
        # Add confound EVs text file
        fsf['confoundevs']= 0;
        
        # Alternative example_func image (not derive from input 4D dataset)
        fsf['alternative_example_func']='""'
        
        # Alternative (to BETting) mask image
        fsf['alternative_mask']='""';
        
        # Initial structural space registration initialisation transform
        fsf['init_initial_highres']='""';
        
        # Structural space reigstration initialisation transform
        fsf['init_highres']='""';
        
        # Standard space reigstration initialisation transform
        fsf['init_standard']='""';
        
        # For full FEAT analysis: overwrite existing .feat output dir?
        fsf['overwrite_yn']= 0;

    return fsf


def create_con(mode='orig'):
    """ Creates a contrast structure for specification of FEAT model
    
    Parameters
    ----------
    mode : {'orig','real'}
        contrast mode
        
    Returns
    -------
    con : dict
        dictionary with a default ev structure
    
    """
    #con=OrderedDict()
    con={}
    con['type'] = mode;
    
    # Display images for contrast_real 1
    con['conpic']=1;
    
    # Title for contrast_real 1
    con['conname']='';
    
    # Real contrast_real vector 1
    # Ultimately has EV elements
    con['con_vect']=[];
    
    # F-test 1 element 1
    con['ftest']=[];
    return con

def create_ev():
    """ ev=create_ev;
    Creates a regressor for use in FEAT model
    
    Returns
    -------
    ev : dict
    dictionary with a default ev structure
    
    """
    #ev=OrderedDict()
    ev={}    
    # Basic waveform shape (EV 1)
    # 0 : Square
    # 1 : Sinusoid
    # 2 : Custom (1 entry per volume)
    # 3 : Custom (3 column format)
    # 4 : Interaction
    # 10 : Empty (all zeros)
    ev['shape']=0;
    
    # Convolution (EV 1)
    # 0 : None
    # 1 : Gaussian
    # 2 : Gamma
    # 3 : Double-Gamma HRF
    # 4 : Gamma basis functions
    # 5 : Sine basis functions
    # 6 : FIR basis functions
    ev['convolve']=0;
    
    # Convolve phase (EV 1)
    ev['convolve_phase']=0;
    
    # Apply temporal filtering (EV 1)
    ev['tempfilt_yn']=0;
    
    # Add temporal derivative (EV 1)
    ev['deriv_yn']=0;
    
    #
    # Different EV shapes take different sets of parameters
    #
    
    #
    # Parameters for square waves
    # 
    
    # Skip (EV 1)
    ev['skip']=0;
    
    # Off (EV 1)
    ev['off']=[];
    
    # On (EV 1)
    ev['on']=[];
    
    # Phase (EV 1)
    ev['phase']=[];
    
    # Stop (EV 1)
    ev['stop']=[];
    
    #
    # Parameters for custom EVs
    #
    ev['title'] = '""';
    ev['fname']='"dummy"';
    
    # The orthogonalization vector has to obviously be set once the EVs are all known
    # Orthogonalise EV 1 wrt EV 0
    ev['ortho'] = [];
    
    #
    # Convolution parameters
    #
    ev['gausssigma'] = 2.8;
    ev['gaussdelay'] = 3;
    return ev
    

def write_fsf(fsf,output_name=None):
    """
    Parameters
    ----------
    fsf : dict
        fsf dictionary to be written
    output_name : str
        string for fsf file output.  If unspecified it will be fsf['fsldir']/design.fsf
    
    Returns
    -------
    output_name : string
        filename of the fsf file that was generated
    
    Notes
    -----
    write_fsf(fsf);
   
    Writes a FEAT structure file (.fsf) for use with FSL's FEAT analysis package
   
    fsf is structure with all of the parameters necessary to write out the .fsf
    file. The default field values are set in create_fsf.m
   
    NOTE: This is still a work in progress. 
    Fortunately, the FEAT GUI will read a partially completed setup file.  
    In effect, it should be possible to create a template for any given analysis
    and then fill a few fields with pointers to the subject specific files.
    
    # based on a Matlab script by Petr Janata
    # writes the .fsf file based on information in the structure
    modification by Gregory Lee for CMIND project
   """
    
    if not output_name:
        output_name=os.path.join(fsf['fsldir'],'design.fsf')

    if os.path.exists(output_name):
        print("Warning: overwriting existing fsf file")
    
    fsf_file = open(output_name,'w')
    
    fsf_file.write(\
        '\n# FEAT version number'\
        '\nset fmri(version) %1.2f\n' % fsf['version'])
        
    if fsf['version'] >= 6.00:
        fsf_file.write(\
            '\n# Are we in MELODIC?'\
            '\nset fmri(inmelodic) %d\n' % fsf['inmelodic'])
    
    fsf_file.write(\
        '\n# Analysis level'\
        '\n# 1 : First-level analysis'\
        '\n# 2 : Higher-level analysis'\
        '\nset fmri(level) %d\n' % fsf['level'])

    
    fsf_file.write(\
        '\n# Which stages to run'\
        '\n# 0 : No first-level analysis (registration and/or group stats only)'\
        '\n# 7 : Full first-level analysis'\
        '\n# 1 : Pre-Stats'\
        '\n# 3 : Pre-Stats + Stats'\
        '\n# 2 :             Stats'\
        '\n# 6 :             Stats + Post-stats'\
        '\n# 4 :                     Post-stats'\
        '\nset fmri(analysis) %d\n' % fsf['analysis'])

    
    if fsf['version'] == 5.4:
        fsf_file.write(\
                '\n# Delay before starting (hours)'\
                '\nset fmri(delay) %d\n' % fsf['delay'])
    
    fsf_file.write(\
        '\n# Use relative filenames'\
        '\nset fmri(relative_yn) %d\n' % fsf['relative_yn'])

    
    fsf_file.write(\
        '\n# Balloon help'\
        '\nset fmri(help_yn) %d\n' % fsf['help_yn'])

    
    fsf_file.write(\
        '\n# Run Featwatcher'\
        '\nset fmri(featwatcher_yn) %d\n' % fsf['featwatcher_yn'])

    
    fsf_file.write(\
        '\n# Cleanup first-level standard-space images'\
        '\nset fmri(sscleanup_yn) %d\n' % fsf['sscleanup_yn'])

    
    fsf_file.write(\
        '\n# Output directory'\
        '\nset fmri(outputdir) "%s"\n' % fsf['outputdir'])

    
    fsf_file.write(\
        '\n# TR(s)'\
        '\nset fmri(tr) %1.2f\n' % fsf['tr'])

    
    fsf_file.write(\
        '\n# Total volumes'\
        '\nset fmri(npts) %d\n' % fsf['npts'])

    
    fsf_file.write(\
        '\n# Delete volumes'\
        '\nset fmri(ndelete) %d\n' % fsf['ndelete'])

    
    if fsf['version'] >= 5.98:
        fsf_file.write(\
            '\n# Perfusion tag/control order'\
            '\nset fmri(tagfirst) %d\n' % fsf['tagfirst'])
    
    fsf_file.write(\
        '\n# Number of first-level analyses'\
        '\nset fmri(multiple) %d\n' % fsf['multiple'])

    
    fsf_file.write(\
        '\n# Higher-level input type'\
        '\n# 1 : Inputs are lower-level FEAT directories'\
        '\n# 2 : Inputs are cope images from FEAT directories'\
        '\nset fmri(inputtype) %d\n' % fsf['inputtype'])

    
    fsf_file.write(\
        '\n# Carry out pre-stats processing?'\
        '\nset fmri(filtering_yn) %d\n' % fsf['filtering_yn'])

    
    fsf_file.write(\
        '\n# Brain/background threshold, %%'\
        '\nset fmri(brain_thresh) %d\n' % fsf['brain_thresh'])

    
    if fsf['version'] >= 5.98:
        fsf_file.write(\
            '\n# Critical z for design efficiency calculation'\
            '\nset fmri(critical_z) %1.2f\n' % fsf['critical_z'])
        
        fsf_file.write(\
            '\n# Noise level'\
            '\nset fmri(noise) %1.6f\n' % fsf['noise'])
        
        fsf_file.write(\
            '\n# Noise AR(1)'\
            '\nset fmri(noisear) %1.6f\n' % fsf['noisear'])
    
    fsf_file.write(\
        '\n# Post-stats-only directory copying'\
        '\n# 0 : Overwrite original post-stats results'\
        '\n# 1 : Copy original FEAT directory for new Contrasts, Thresholding, Rendering'\
        '\nset fmri(newdir_yn) %d\n' % fsf['newdir_yn'])

    
    fsf_file.write(\
        '\n# Motion correction'\
        '\n# 0 : None'\
        '\n# 1 : MCFLIRT'\
        '\nset fmri(mc) %d\n' % fsf['mc'])

    
    fsf_file.write('\n# Spin-history (currently obsolete)'\
                   '\nset fmri(sh_yn) %d\n' % fsf['sh_yn'])

    
    fsf_file.write('\n# B0 fieldmap unwarping?'\
                   '\nset fmri(regunwarp_yn) %d\n' % fsf['regunwarp_yn'])

    
    if fsf['version'] >= 5.98:
        fsf_file.write('\n# EPI dwell time (ms)'\
                       '\nset fmri(dwell) %f\n' % fsf['dwell'])
        
        fsf_file.write('\n# EPI TE (ms)'\
                       '\nset fmri(te) %d\n' % fsf['te'])
        
        fsf_file.write('\n# %% Signal loss threshold'\
                       '\nset fmri(signallossthresh) %d\n' % fsf['signallossthresh'])
        
        fsf_file.write('\n# Unwarp direction'\
                       '\nset fmri(unwarp_dir) %s\n' % fsf['unwarp_dir'])
    
    fsf_file.write('\n# Slice timing correction'\
        '\n# 0 : None'\
        '\n# 1 : Regular up (0, 1, 2, 3, ...)'\
        '\n# 2 : Regular down'\
        '\n# 3 : Use slice order file'\
        '\n# 4 : Use slice timings file'\
        '\n# 5 : Interleaved (0, 2, 4 ... 1, 3, 5 ... )'\
        '\nset fmri(st) %d\n' % fsf['st'])

    
    fsf_file.write('\n# Slice timings file'\
        '\nset fmri(st_file) "%s"\n' % fsf['st_file'])

    
    fsf_file.write('\n# BET brain extraction'\
        '\nset fmri(bet_yn) %d\n' % fsf['bet_yn'])

    
    fsf_file.write('\n# Spatial smoothing FWHM (mm)'\
        '\nset fmri(smooth) %d\n' % fsf['smooth'])

    
    fsf_file.write('\n# Intensity normalization'\
        '\nset fmri(norm_yn) %d\n' % fsf['norm_yn'])

    
    if fsf['version'] >= 5.98:
        fsf_file.write('\n# Perfusion subtraction'\
            '\nset fmri(perfsub_yn) %d\n' % fsf['perfsub_yn'])

    fsf_file.write('\n# Highpass temporal filtering'\
        '\nset fmri(temphp_yn) %d\n' % fsf['temphp_yn'])

    
    fsf_file.write('\n# Lowpass temporal filtering'\
        '\nset fmri(templp_yn) %d\n' % fsf['templp_yn'])

    
    fsf_file.write('\n# MELODIC ICA data exploration'\
        '\nset fmri(melodic_yn) %d\n' % fsf['melodic_yn'])

    
    fsf_file.write('\n# Carry out main stats?'\
        '\nset fmri(stats_yn) %d\n' % fsf['stats_yn'])

    
    fsf_file.write('\n# Carry out prewhitening?'\
        '\nset fmri(prewhiten_yn) %d\n' % fsf['prewhiten_yn'])

    
    if fsf['version'] >= 5.98:
        fsf_file.write('\n# Add motion parameters to model'\
            '\n# 0 : No'\
            '\n# 1 : Yes'\
            '\nset fmri(motionevs) %d'\
            '\nset fmri(motionevsbeta) %s'\
            '\nset fmri(scriptevsbeta) %s\n' % (fsf['motionevs'], fsf['motionevsbeta'], fsf['scriptevsbeta']))
        
        fsf_file.write('\n# Robust outlier detection in FLAME?'\
            '\nset fmri(robust_yn) %d\n' % fsf['robust_yn'])
    
    fsf_file.write('\n# Higher-level modelling'\
        '\n# 3 : Fixed effects'\
        '\n# 0 : Mixed Effects: Simple OLS'\
        '\n# 2 : Mixed Effects: FLAME 1'\
        '\n# 1 : Mixed Effects: FLAME 1+2'\
        '\nset fmri(mixed_yn) %d\n' % fsf['mixed_yn'])

    
    fsf_file.write('\n# Number of EVs'\
        '\nset fmri(evs_orig) %d'\
        '\nset fmri(evs_real) %d\n' % (fsf['evs_orig'],fsf['evs_real']))

    if fsf['version'] >= 5.98:
        fsf_file.write('set fmri(evs_vox) %d\n' % (fsf['evs_vox']))
    
    fsf_file.write('\n# Number of contrasts'\
        '\nset fmri(ncon_orig) %d'\
        '\nset fmri(ncon_real) %d\n' % (fsf['ncon_orig'], fsf['ncon_real']))
    
    fsf_file.write('\n# Number of F-tests'\
        '\nset fmri(nftests_orig) %d'\
        '\nset fmri(nftests_real) %d\n' % (fsf['nftests_orig'], fsf['nftests_real']))
    
    fsf_file.write('\n# Add constant column to design matrix? (obsolete)'\
        '\nset fmri(constcol) %d\n' % fsf['constcol'])

    
    fsf_file.write('\n# Carry out post-stats steps?'\
        '\nset fmri(poststats_yn) %d\n' % fsf['poststats_yn'])

    
    fsf_file.write('\n# Pre-threshold masking?'\
        '\nset fmri(threshmask) "%s"\n' % fsf['threshmask'])

    
    fsf_file.write('\n# Thresholding'\
        '\n# 0 : None'\
        '\n# 1 : Uncorrected'\
        '\n# 2 : Voxel'\
        '\n# 3 : Cluster'\
        '\nset fmri(thresh) %d\n' % fsf['thresh'])

    
    fsf_file.write('\n# P threshold'\
        '\nset fmri(prob_thresh) %1.4f\n' % fsf['prob_thresh'])

    
    fsf_file.write('\n# Z threshold'\
        '\nset fmri(z_thresh) %1.2f\n' % fsf['z_thresh'])

    
    fsf_file.write('\n# Z min/max for colour rendering'\
        '\n# 0 : Use actual Z min/max'\
        '\n# 1 : Use preset Z min/max'\
        '\nset fmri(zdisplay) %d\n' % fsf['zdisplay'])

    
    fsf_file.write('\n# Z min in colour rendering'\
        '\nset fmri(zmin) %d\n' % fsf['zmin'])

    
    fsf_file.write('\n# Z max in colour rendering'\
        '\nset fmri(zmax) %d\n' % fsf['zmax'])

    
    fsf_file.write('\n# Colour rendering type'\
        '\n# 0 : Solid blobs'\
        '\n# 1 : Transparent blobs'\
        '\nset fmri(rendertype) %d\n' % fsf['rendertype'])

    
    fsf_file.write('\n# Background image for higher-level stats overlays'\
        '\n# 1 : Mean highres'\
        '\n# 2 : First highres'\
        '\n# 3 : Mean functional'\
        '\n# 4 : First functional'\
        '\n# 5 : Standard space template'\
        '\nset fmri(bgimage) %d\n' % fsf['bgimage'])

    
    if fsf['version'] >= 5.98:
        fsf_file.write('\n# Create time series plots'\
            '\nset fmri(tsplot_yn) %d\n' % fsf['tsplot_yn'])
    
    fsf_file.write('\n# Registration?'\
        '\nset fmri(reg_yn) %d\n' % fsf['reg_yn'])
    
    if fsf['version'] == 5.4:
        fsf_file.write('\n# Dwell/Asymmetry ratio '\
                '\nset fmri(dwellasym) %d\n' % fsf['dwellasym'])
    
    fsf_file.write('\n# Registration to initial structural'\
        '\nset fmri(reginitial_highres_yn) %d\n' % fsf['reginitial_highres_yn'])
    
    fsf_file.write('\n# Search space for registration to initial structural'\
        '\n# 0   : No search'\
        '\n# 90  : Normal search'\
        '\n# 180 : Full search'\
        '\nset fmri(reginitial_highres_search) %d\n' % fsf['reginitial_highres_search'])
    
    fsf_file.write('\n# Degrees of Freedom for registration to initial structural'\
        '\nset fmri(reginitial_highres_dof) %d\n' % fsf['reginitial_highres_dof'])

    
    if fsf['version'] == 5.4:
        fsf_file.write('\n# Do nonlinear registration from structural to standard space?'\
            '\nset fmri(reginitial_highres_nonlinear_yn) %d\n' % fsf['reginitial_highres_nonlinear_yn'])

    
    fsf_file.write('\n# Registration to main structural'\
        '\nset fmri(reghighres_yn) %d\n' % fsf['reghighres_yn'])

    
    fsf_file.write('\n# Search space for registration to main structural'\
        '\n# 0   : No search'\
        '\n# 90  : Normal search'\
        '\n# 180 : Full search'\
        '\nset fmri(reghighres_search) %d\n' % fsf['reghighres_search'])

    
    if fsf['version'] >= 6.00:
        fsf_file.write('\n# Degrees of Freedom for registration to main structural'\
            '\nset fmri(reghighres_dof) %s\n' % fsf['reghighres_dof'])
    
    else:
        fsf_file.write('\n# Degrees of Freedom for registration to main structural'\
            '\nset fmri(reghighres_dof) %d\n' % fsf['reghighres_dof'])
    
    if fsf['version'] == 5.4:
        fsf_file.write('\n# Do nonlinear registration to main structural?'\
            '\nset fmri(reghighres_nonlinear_yn) %d\n' % fsf['reghighres_nonlinear_yn'])

    fsf_file.write('\n# Registration to standard image?'\
        '\nset fmri(regstandard_yn) %d\n' % fsf['regstandard_yn'])

    
    if fsf['version'] >= 5.98:
        fsf_file.write('\n# Use alternate reference images?'\
            '\nset fmri(alternateReference_yn) %d\n' % fsf['alternateReference_yn'])
    
    fsf_file.write('\n# Standard image'\
        '\nset fmri(regstandard) "%s"\n' % fsf['regstandard'].replace('.nii.gz',''))

    
    fsf_file.write('\n# Search space for registration to standard space'\
        '\n# 0   : No search'\
        '\n# 90  : Normal search'\
        '\n# 180 : Full search'\
        '\nset fmri(regstandard_search) %d\n' % fsf['regstandard_search'])

    
    fsf_file.write('\n# Degrees of Freedom for registration to standard space'\
        '\nset fmri(regstandard_dof) %d\n' % fsf['regstandard_dof'])

    
    fsf_file.write('\n# Do nonlinear registration from structural to standard space?'\
        '\nset fmri(regstandard_nonlinear_yn) %d\n' % fsf['regstandard_nonlinear_yn'])

    
    if fsf['version'] >= 5.98:
        fsf_file.write('\n# Control nonlinear warp field resolution'\
            '\nset fmri(regstandard_nonlinear_warpres) %d \n' % fsf['regstandard_nonlinear_warpres'])

    
    fsf_file.write('\n# High pass filter cutoff'\
        '\nset fmri(paradigm_hp) %d\n' % fsf['paradigm_hp'])

    
    fsf_file.write('\n# Number of lower-level copes feeding into higher-level analysis'\
        '\nset fmri(ncopeinputs) %d\n' % fsf['ncopeinputs'])

    
    # Here we loop over all copes feeding into the higher level analysis
    for icope in range(fsf['ncopeinputs']):
        use_cope = fsf['copeinputs'][icope]
        if fsf['version'] >= 5.98:
            fsf_file.write('\n# Use lower-level cope %d for higher-level analysis'\
                '\nset fmri(copeinput.%d) %d\n' % (icope+1, icope+1, use_cope))
        
    
    
    nfiles = len(fsf['feat_files'])
    for ifile in range(nfiles):
        feat_fname=fsf['feat_files'][ifile]
        if os.path.isdir(feat_fname):
            if feat_fname[-1]!=os.path.sep:
                feat_fname=feat_fname+os.path.sep

        fsf_file.write('\n# 4D AVW data or FEAT directory (%d)'\
       	'\nset feat_files(%d) "%s"\n' % (ifile+1, ifile+1, feat_fname))
    
    if fsf['version'] >= 5.98:
        fsf_file.write('\n# Add confound EVs text file'\
            '\nset fmri(confoundevs) %d\n' % fsf['confoundevs'])
    
    if fsf['reginitial_highres_yn']:
        nfiles = len(fsf['initial_highres_files'])
        for ifile in range(nfiles):
            fsf_file.write('\n# Session''s structural image for analysis %d'\
     	  '\nset initial_highres_files(%d) "%s"\n' % (ifile+1, ifile+1, fsf['initial_highres_files'][ifile].replace('.nii.gz','') + ''))
    
    if fsf['reghighres_yn']:
        nfiles = len(fsf['highres_files'])
        for ifile in range(nfiles):
            fsf_file.write('\n# Subject\'s structural image for analysis %d'\
     	  '\nset highres_files(%d) "%s"\n' % (ifile+1, ifile+1,fsf['highres_files'][ifile].replace('.nii.gz','') + ''))
    
    # Here we loop over all of the variables specified in the model
    nev = len(fsf['ev'])
    for iev in range(nev):
        ev = fsf['ev'][iev]
        
        if fsf['version'] >= 5.98:
            fsf_file.write('\n# EV %d title'\
                '\nset fmri(evtitle%d) %s\n' % (iev+1, iev+1, ev['title']))
        
        fsf_file.write('\n# Basic waveform shape (EV %d)'\
       	'\n# 0 : Square'\
       	'\n# 1 : Sinusoid'\
       	'\n# 2 : Custom (1 entry per volume)'\
       	'\n# 3 : Custom (3 column format)'\
       	'\n# 4 : Interaction'\
        '\n# 10 : Empty (all zeros)'\
       	'\nset fmri(shape%d) %d\n' % (iev+1, iev+1, ev['shape']))
    
        
        fsf_file.write('\n# Convolution (EV %d)'\
       	'\n# 0 : None'\
       	'\n# 1 : Gaussian'\
       	'\n# 2 : Gamma'\
       	'\n# 3 : Double-Gamma HRF'\
       	'\n# 4 : Gamma basis functions'\
       	'\n# 5 : Sine basis functions'\
       	'\n# 6 : FIR basis functions'\
       	'\nset fmri(convolve%d) %d\n' % (iev+1, iev+1, ev['convolve']))
    
        
        fsf_file.write('\n# Convolve phase (EV %d)'\
       	'\nset fmri(convolve_phase%d) %d\n' % (iev+1, iev+1, ev['convolve']))
    
        
        fsf_file.write('\n# Apply temporal filtering (EV %d)'\
       	'\nset fmri(tempfilt_yn%d) %d\n' % (iev+1, iev+1, ev['tempfilt_yn']))
    
        
        fsf_file.write('\n# Add temporal derivative (EV %d)'\
       	'\nset fmri(deriv_yn%d) %d\n' % (iev+1, iev+1, ev['deriv_yn']))
    
        
        if ev['shape']==0:
            fsf_file.write('\n# Skip (EV %d)'\
       	    '\nset fmri(skip%d) %d\n' % (iev+1, iev+1, ev['skip']))
        
            
            fsf_file.write('\n# Off (EV %d)'\
       	    '\nset fmri(off%d) %1.2f\n' % (iev+1, iev+1, ev['off']))
        
            
            fsf_file.write('\n# On (EV %d)'\
       	    '\nset fmri(on%d) %1.2f\n' % (iev+1, iev+1, ev['on']))
        
            
            fsf_file.write('\n# Phase (EV %d)'\
       	    '\nset fmri(phase%d) %d\n' % (iev+1, iev+1, ev['phase']))
        
            
            fsf_file.write('\n# Stop (EV %d)'\
       	    '\nset fmri(stop%d) %d\n' % (iev+1, iev+1, ev['stop']))
        
        elif (ev['shape']==2) or (ev['shape']==3):
            fsf_file.write('\n# Custom EV file (EV %d)'\
            '\nset fmri(custom%d) "%s"\n' % (iev+1, iev+1, 'dummy'))
       	    #'\nset fmri(custom%d) "%s"\n' % (iev+1, iev+1, ev['fname']))
        else:
            raise Exception('Not set up to handle this EV shape yet\n')
        
        
        # Add convolution info
        if ev['convolve']==1: # gaussian
            fsf_file.write('\n# Gauss sigma (EV %d)'\
       	    '\nset fmri(gausssigma%d) %1.2f\n' % (iev+1, iev+1, ev['gausssigma']))
        
            fsf_file.write('\n# Gauss delay (EV %d)'\
       	    '\nset fmri(gaussdelay%d) %1.2f\n' % (iev+1, iev+1, ev['gaussdelay']))
        
        # Add the orthogonalization info
        if not ev['ortho']: #default is no orthogonalization
            ev['ortho'] = [0]*nev
        
        if ev['ortho']:
            ortho_vect = [0] + ev['ortho'];
            for jev in range(len(ortho_vect)):
                fsf_file.write('\n# Orthogonalise EV %d wrt EV %d'\
           	    '\nset fmri(ortho%d.%d) %d\n' % (iev+1, jev, iev+1, jev, ortho_vect[jev]))
        
        if(fsf['level']==2):  #GRL: trick to write EVs into the .fsf file for group analysis
            if ev['fname'] and os.path.exists(ev['fname']):
                ev_regressor=np.genfromtxt(ev['fname'])
                for ireg in range(ev_regressor.shape[0]):
                    fsf_file.write('\n# Higher-level EV value for EV %d and input %d'\
                        '\nset fmri(evg%d.%d) %g\n' % (iev+1, ireg+1, ireg+1, iev+1,  ev_regressor[ireg]))
                
    
    fsf_file.write('\n# Setup Orthogonalisation at higher level? '\
        '\nset fmri(level2orth) %d\n' % fsf['level2orth'])

    
    if not fsf['groupmem']: #if not specified, assume all subjects belong to group 1
        fsf['groupmem']=[1]*len(fsf['feat_files'])
    
    for igroup in range(len(fsf['groupmem'])):
        fsf_file.write('\n# Group membership for input %d'\
        '\nset fmri(groupmem.%d) %d\n' % (igroup+1, igroup+1, fsf['groupmem'][igroup]))
    
    # Add contrast information from fsf['con'] structure
    fsf_file.write('\n# Contrast & F-tests mode'\
        '\n# real : control real EVs'\
        '\n# orig : control original EVs'\
        '\nset fmri(con_mode_old) %s'\
        '\nset fmri(con_mode) %s\n' % (fsf['con_mode_old'], fsf['con_mode']))
    
    # Loop over each of the real contrasts
    ncon = len(fsf['con'])
    nFtest = len(fsf['Ftest'])
    
    for icon in range(ncon):
        con = fsf['con'][icon]
    
        mode = con['type']
        
        fsf_file.write('\n# Display images for contrast_%s %d'\
       	'\nset fmri(conpic_%s.%d) %d\n' % ( mode, icon+1, mode, icon+1, con['conpic']))
    
        
        fsf_file.write('\n# Title for contrast_%s %d'\
       	'\nset fmri(conname_%s.%d) "%s"\n' % ( mode, icon+1, mode, icon+1, con['conname']))
    
        
        for iev in range(nev):
            if len(con['con_vect']) < iev:
                continue
            fsf_file.write('\n# Real contrast_%s vector %d element %d'\
     	  '\nset fmri(con_%s%d.%d) %d\n' % ( mode, icon+1, iev+1, mode, icon+1, iev+1, con['con_vect'][iev]))
        
        if con['ftest']:
            for iftest in range(con['ftest']):
                fsf_file.write('\n# F-test %d element %d'\
         	  '\nset fmri(ftest_%s%d.%d) %d\n' % ( iftest+1, icon+1, mode, iftest+1, icon+1, con['ftest'][iftest]))
        
        
    # Contrast masking
    fsf_file.write('\n# Contrast masking - use >0 instead of thresholding?'\
        '\nset fmri(conmask_zerothresh_yn) %d\n' % fsf['conmask_zerothresh_yn'])

    
    for icon in range(ncon):  #TODO: implement contrast masking properly
        for jcon in range(ncon):
            domask=0
            if(icon!=jcon):
                fsf_file.write('\n# Mask real contrast/F-test %d with real contrast/F-test %d?'\
                '\nset fmri(conmask%d_%d) %d\n' % ( icon+1, jcon+1, icon+1, jcon+1, domask))
    
    fsf_file.write('\n# Do contrast masking at all?'\
        '\nset fmri(conmask1_1) %d\n' % fsf['conmask1_1'])
    
    if fsf['version'] >= 6.0:
        fsf_file.write('\n##########################################################'\
            '\n# Now options that don\'t appear in the GUI\n'\
            '\n# Alternative (to BETting) mask image'\
            '\nset fmri(alternative_mask) %s\n'\
            '\n# Initial structural space registration initialisation transform'\
            '\nset fmri(init_initial_highres) %s\n'\
            '\n# Structural space registration initialisation transform'\
            '\nset fmri(init_highres) %s\n'\
            '\n# Standard space registration initialisation transform'\
            '\nset fmri(init_standard) %s\n'\
            '\n# For full FEAT analysis: overwrite existing .feat output dir?'\
            '\nset fmri(overwrite_yn) %d\n' % (fsf['alternative_mask'], fsf['init_initial_highres'], fsf['init_highres'], fsf['init_standard'], fsf['overwrite_yn']))
  
    elif fsf['version'] >= 5.98:
        # Extra non-GUI options
        fsf_file.write('\n##########################################################'\
            '\n# Now options that don''t appear in the GUI\n'\
            '\n# Alternative example_func image (not derived from input 4D dataset'\
            '\nset fmri(alternative_example_func) %s\n'\
            '\n# Alternative (to BETting) mask image'\
            '\nset fmri(alternative_mask) %s\n'\
            '\n# Initial structural space registration initialisation transform'\
            '\nset fmri(init_initial_highres) %s\n'\
            '\n# Structural space registration initialisation transform'\
            '\nset fmri(init_highres) %s\n'\
            '\n# Standard space registration initialisation transform'\
            '\nset fmri(init_standard) %s\n'\
            '\n# For full FEAT analysis: overwrite existing .feat output dir?'\
            '\nset fmri(overwrite_yn) %d\n' % (fsf['alternative_example_func'],fsf['alternative_mask'],fsf['init_initial_highres'],fsf['init_highres'],fsf['init_standard'],fsf['overwrite_yn']))

    fsf_file.close()
    return (output_name)
    
    #%TODO: conmask%d_%d entries
    #'\n# Mask real contrast/F-test 1 with real contrast/F-test 2?'
    #'set fmri(conmask1_2) 0'
    #
    #'\n# Mask real contrast/F-test 1 with real contrast/F-test 3?'
    #'set fmri(conmask1_3) 0'
    #
    #'\n# Mask real contrast/F-test 1 with real contrast/F-test 4?'
    #'set fmri(conmask1_4) 0'
    #
    #'\n# Mask real contrast/F-test 2 with real contrast/F-test 1?'
    #'set fmri(conmask2_1) 0'
    #
    #'\n# Mask real contrast/F-test 2 with real contrast/F-test 3?'
    #'set fmri(conmask2_3) 0'
    #
    #'\n# Mask real contrast/F-test 2 with real contrast/F-test 4?'
    #'set fmri(conmask2_4) 0'
    #
    #'\n# Mask real contrast/F-test 3 with real contrast/F-test 1?'
    #'set fmri(conmask3_1) 0'
    #
    #'\n# Mask real contrast/F-test 3 with real contrast/F-test 2?'
    #'set fmri(conmask3_2) 0'
    #
    #'\n# Mask real contrast/F-test 3 with real contrast/F-test 4?'
    #'set fmri(conmask3_4) 0'
    #
    #'\n# Mask real contrast/F-test 4 with real contrast/F-test 1?'
    #'set fmri(conmask4_1) 0'
    #
    #'\n# Mask real contrast/F-test 4 with real contrast/F-test 2?'
    #'set fmri(conmask4_2) 0'
    #
    #'\n# Mask real contrast/F-test 4 with real contrast/F-test 3?'
    #'set fmri(conmask4_3) 0'
    
def fsf_substitute(FSF_file,valdict):
    import warnings
    
    f=open(FSF_file,'r');
    fsf=f.readlines()  #orginal fsf file
    f.close()
   
    modified_lines=[]
    for key in valdict:
        key_found=False
        for lineno,line in enumerate(fsf):
            if line.find('fmri('+key+')')!=-1:
                key_found=True
                keystr=valdict[key]
                if keystr=='': #safeguard to make sure no empty values are written!
                    keystr='""'
                newline='set fmri({}) {}\n'.format(key,keystr)
                modified_lines.append((lineno,newline))
                break
            else:
                newline=line
        if not key_found:
            warnings.warn("key, %s, not found in %s" % (key,FSF_file))
    for mline in modified_lines:
        fsf[mline[0]]=mline[1]            
    f=open(FSF_file,'w');
    f.writelines(fsf)  #orginal fsf file
    f.close    