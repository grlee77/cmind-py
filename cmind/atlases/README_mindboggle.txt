
Thank you for your interest in the Mindboggle-101 dataset (http://mindboggle.info/data/)!
This first release includes manually labeled anatomical brain regions for 101 healthy subjects,
following the Desikan-Killiany-Tourville protocol.

Please cite the following article and website when making use of these data or the labeling protocol:

"101 labeled brain images and a consistent human cortical labeling protocol"
Arno Klein, Jason Tourville. Frontiers in Brain Imaging Methods. 6:171. DOI: 10.3389/fnins.2012.00171
http://www.frontiersin.org/Brain_Imaging_Methods/10.3389/fnins.2012.00171/full

Copyright 2012,  Mindboggle team (http://mindboggle.info), Creative Commons License 
(http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US)

For questions, please contact Arno Klein (arno@mindboggle.info).

Mindboggle101, M/F, R/L, Age, Source
------------------------------------
Colin27-1, M, ?, ?, colin27_t1_tal_lin
HLN-12-1, M, R, 29, plos_CJ_700_3_1
HLN-12-2, M, R, 39, plos_Ding_756_3_1
HLN-12-3, F, R, 24, plos_Ding_1009_3_1
HLN-12-4, F, R, 25, plos_Ding_1035_3_1
HLN-12-5, M, R, 29, plos_Ding_1042_3_1
HLN-12-6, F, R, 26, plos_Ding_1050_3_1
HLN-12-7, F, R, 29, plos_Ding_1052_3_1
HLN-12-8, M, R, 23, plos_Language_587_3_1
HLN-12-9, M, R, 23, plos_Language_795_3_1
HLN-12-10, F, R, 24, plos_Language_820_3_1
HLN-12-11, F, R, 29, plos_Language_886_3_1
HLN-12-12, M, R, 34, plos_Language_888_3_1
MMRR-21-1, M, -, 25, KKI2009-11
MMRR-21-2, M, -, 38, KKI2009-14
MMRR-21-3, M, R, 34, KKI2009-15
MMRR-21-4, F, -, 42, KKI2009-16
MMRR-21-5, M, R, 26, KKI2009-18
MMRR-21-6, F, R, 26, KKI2009-19
MMRR-21-7, M, -, 28, KKI2009-20
MMRR-21-8, F, R, 38, KKI2009-21
MMRR-21-9, F, R, 30, KKI2009-22
MMRR-21-10, M, R, 30, KKI2009-24
MMRR-21-11, M, R, 25, KKI2009-25
MMRR-21-12, F, R, 29, KKI2009-27
MMRR-21-13, F, R, 49, KKI2009-29
MMRR-21-14, M, R, 25, KKI2009-31
MMRR-21-15, F, R, 28, KKI2009-33
MMRR-21-16, M, R, 30, KKI2009-34
MMRR-21-17, F, L, 23, KKI2009-36
MMRR-21-18, F, R, 61, KKI2009-37
MMRR-21-19, M, R, 32, KKI2009-40
MMRR-21-20, F, R, 22, KKI2009-41
MMRR-21-21, M, R, 26, KKI2009-42
MMRR-3T7T-2-1, M, R, 24, MMR2011A_3T
MMRR-3T7T-2-2, M, R, 22, MMR2011B_3T
NKI-RS-22-1, F, R, 29, NKI_Rockland_1097782
NKI-RS-22-2, F, R, 29, NKI_Rockland_1351931
NKI-RS-22-3, F, R, 27, NKI_Rockland_1427581
NKI-RS-22-4, M, R, 23, NKI_Rockland_1476437
NKI-RS-22-5, F, R, 33, NKI_Rockland_1523112
NKI-RS-22-6, M, R, 24, NKI_Rockland_2071045
NKI-RS-22-7, M, R, 34, NKI_Rockland_2221971
NKI-RS-22-8, M, R, 20, NKI_Rockland_2483617
NKI-RS-22-9, F, R, 20, NKI_Rockland_2788776
NKI-RS-22-10, M, R, 21, NKI_Rockland_3366302
NKI-RS-22-11, F, R, 25, NKI_Rockland_3466763
NKI-RS-22-12, M, L, 23, NKI_Rockland_3611212
NKI-RS-22-13, M, R, 34, NKI_Rockland_3701005
NKI-RS-22-14, M, R, 25, NKI_Rockland_3808535
NKI-RS-22-15, M, R, 22, NKI_Rockland_3815065
NKI-RS-22-16, F, R, 21, NKI_Rockland_3927656
NKI-RS-22-17, M, R, 23, NKI_Rockland_4119751
NKI-RS-22-18, M, R, 40, NKI_Rockland_4277600
NKI-RS-22-19, F, R, 26, NKI_Rockland_4290056
NKI-RS-22-20, M, R, 21, NKI_Rockland_9006154
NKI-RS-22-21, F, R, 26, NKI_Rockland_9100911
NKI-RS-22-22, F, R, 26, NKI_Rockland_9645370
NKI-TRT-20-1, M, R, 52, 0021002
NKI-TRT-20-2, M, R, 32, 0021006
NKI-TRT-20-3, M, R, 36, 0021018
NKI-TRT-20-4, M, R, 22, 0021024
NKI-TRT-20-5, F, R, 27, 1427581
NKI-TRT-20-6, M, R, 60, 1793622
NKI-TRT-20-7, F, R, 21, 1961098
NKI-TRT-20-8, M, L, 21, 2475376
NKI-TRT-20-9, M, R, 30, 2799329
NKI-TRT-20-10, M, A, 27, 2842950
NKI-TRT-20-11, M, A, 48, 3201815
NKI-TRT-20-12, F, R, 22, 3313349
NKI-TRT-20-13, M, R, 19, 3315657
NKI-TRT-20-14, M, R, 25, 3808535
NKI-TRT-20-15, M, R, 22, 4288245
NKI-TRT-20-16, M, L, 32, 6471972
NKI-TRT-20-17, F, R, 22, 7055197
NKI-TRT-20-18, M, R, 42, 8574662
NKI-TRT-20-19, F, L, 31, 8735778
NKI-TRT-20-20, F, R, 36, 9630905
OASIS-TRT-20-1, F, R, 20, OAS1_0061_MR1
OASIS-TRT-20-2, F, R, 25, OAS1_0080_MR1
OASIS-TRT-20-3, M, R, 22, OAS1_0092_MR1
OASIS-TRT-20-4, M, R, 23, OAS1_0111_MR1
OASIS-TRT-20-5, M, R, 25, OAS1_0117_MR1
OASIS-TRT-20-6, M, R, 34, OAS1_0145_MR1
OASIS-TRT-20-7, F, R, 20, OAS1_0150_MR1
OASIS-TRT-20-8, F, R, 20, OAS1_0156_MR1
OASIS-TRT-20-9, F, R, 21, OAS1_0191_MR1
OASIS-TRT-20-10, F, R, 23, OAS1_0202_MR1
OASIS-TRT-20-11, F, R, 19, OAS1_0230_MR1
OASIS-TRT-20-12, F, R, 20, OAS1_0236_MR1
OASIS-TRT-20-13, F, R, 29, OAS1_0239_MR1
OASIS-TRT-20-14, F, R, 28, OAS1_0249_MR1
OASIS-TRT-20-15, M, R, 20, OAS1_0285_MR1
OASIS-TRT-20-16, M, R, 22, OAS1_0368_MR1
OASIS-TRT-20-17, F, R, 20, OAS1_0379_MR1
OASIS-TRT-20-18, F, R, 26, OAS1_0395_MR1
OASIS-TRT-20-19, M, R, 29, OAS1_0101_MR2
OASIS-TRT-20-20, M, R, 22, OAS1_0353_MR2
Twins-2-1, M, R, 41, Arno
Twins-2-2, M, R, 41, Barrett
Afterthought-1, M, R, ?, Satrajit
