#!/bin/sh

if [ $# -lt 6 ]
then
  echo "Usage: `basename $0` feat_dir pos_cope# neg_cope# zstat# ztitle background_image outname"
  exit 0
fi  

feat_dir=$1 #"ASL_Sentences_FLAME1_3719.gfeat"
cope_pos=$2 #"1"
cope_neg=$3 #"4"
ztitle=$4 #"BOLD (TE=11)"
background=$5
outname=$6 #"Figures/3719/Sentences_FLAME1_TE11_BOLD.png"


#~/src_repositories/svn_stuff/cmind-matlab/trunk/lib/annotate_img_glm1.sh ~/smb_shares/rds6/cmind/Processed_Data/F/IRC04H_18F012_F_1/Feat_BOLD_Sentences 1 2 "BOLD (TE=35)" "Sentences_TE35_BOLD.png"
#echo "`basename $0` $feat_dir $cope_pos $cope_neg $ztitle $outname"

#feat_dir="ASL_Sentences_FLAME1_3719.gfeat"
#cope_pos="1"
#cope_neg="4"
#zstat_number="3"
#ztitle="BOLD (TE=11)"
#outname="Figures/3719/Sentences_FLAME1_TE11_BOLD.png"

poscope="${feat_dir}/thresh_zstat${cope_pos}"
negcope="${feat_dir}/thresh_zstat${cope_neg}"
overlay_vol=${feat_dir}/rendered_zstat1_2
overlay_img=${overlay_vol}.png

#Set up some appropriate sizes for the labels
cbar_scale="1.25"
title_scale="2.5"
pointsize1=`echo "$cbar_scale*11" | bc`
pointsize2=`echo "$title_scale*10" | bc`
resize=`echo "$cbar_scale*100" | bc`
border_sz="8"

#background="$feat_dir/../example_func"
if [ ! -f ${background} ]; then  #allow alternate example_func image location
    echo "background image not found"
    exit 1
fi
dotransparent="0"
slice_skip="1"
image_width="700"
slicer_scale="2"

zmin_pos=`fslstats $poscope -l 0.0001 -R | awk '{print $1}' | cut -c1-5`
zmax_pos=`fslstats $poscope -l 0.0001 -R | awk '{print $2}' | cut -c1-5`
zmin_neg=`fslstats $negcope -l 0.0001 -R | awk '{print $1}' | cut -c1-5`
zmax_neg=`fslstats $negcope -l 0.0001 -R | awk '{print $2}' | cut -c1-5`

#if [ `echo "$zmax_pos>0" | bc` -gt 0 ]
#then
#  echo "zmax_pos > 0"
#  #composite -gravity NorthWest zbar_pos.png $overlay_img ztmp.png
#else
#  echo "zmax_pos <= 0"
#  cp $overlay_img ztmp.png
#fi#

#if [ `echo "$zmax_neg>0" | bc` -gt 0 ]
#then
#  echo "zmax_neg > 0"
#  #composite -gravity NorthEast zbar_neg.png ztmp.png ztmp.png
#else
#  echo "zmax_neg <= 0"
#fi


if [ `echo "$zmax_pos>0" | bc` -gt 0 ] && [ `echo "$zmax_neg>0" | bc` -gt 0 ]
then
   echo "both > 0"
   $FSLDIR/bin/overlay $dotransparent 0 $background -a $poscope $zmin_pos $zmax_pos $negcope $zmin_neg $zmax_neg $overlay_vol
   $FSLDIR/bin/slicer $overlay_vol -s $slicer_scale -S $slice_skip  $image_width $overlay_img
   overlay_flag="3"
else
   if [ `echo "$zmax_pos>0" | bc` -gt 0 ]
   then
       echo "pos > 0"
       $FSLDIR/bin/overlay $dotransparent 0 $background -a $poscope $zmin_pos $zmax_pos $overlay_vol
       $FSLDIR/bin/slicer $overlay_vol -s $slicer_scale -S $slice_skip $image_width $overlay_img
       overlay_flag="2"
   elif [ `echo "$zmax_neg>0" | bc` -gt 0 ]
   then
       echo "neg > 0"
#       overlay $dotransparent 0 $background -a $poscope -1 -0.1 $negcope $zmin_neg $zmax_neg $overlay_vol
       $FSLDIR/bin/overlay $dotransparent 0 $background -a $poscope 0.1 1 $negcope $zmin_neg $zmax_neg $overlay_vol
       $FSLDIR/bin/slicer $overlay_vol -s $slicer_scale -S $slice_skip $image_width $overlay_img
       overlay_flag="1"
   else
       echo "neither > 0"
       $FSLDIR/bin/slicer $background -s $slicer_scale -S $slice_skip $image_width $overlay_img
       overlay_flag="0"
   fi
fi

#



convert $FSLDIR/etc/luts/ramp.gif -resize ${resize}% ${feat_dir}/rampLG.png
convert $FSLDIR/etc/luts/ramp2.gif -resize ${resize}% -flop ${feat_dir}/ramp2LG.png

#convert -background none -fill black -pointsize 20 label:$zmin zmin.png
#convert -background none -fill black -pointsize 20 label:$zmax zmax.png
convert -background black -fill white -pointsize $pointsize1 label:"  $zmin_pos " ${feat_dir}/zmin.png
convert -background black -fill white -pointsize $pointsize1 label:" $zmax_pos  " ${feat_dir}/zmax.png
convert ${feat_dir}/zmin.png ${feat_dir}/rampLG.png ${feat_dir}/zmax.png +append ${feat_dir}/zbar_pos.png
#convert -background none -fill black -pointsize 20 label:$zmin zmin.png
#convert -background none -fill black -pointsize 20 label:$zmax zmax.png
convert -background black -fill white -pointsize $pointsize1 label:" -$zmin_neg " ${feat_dir}/zmin.png
convert -background black -fill white -pointsize $pointsize1 label:" -$zmax_neg " ${feat_dir}/zmax.png
convert ${feat_dir}/zmin.png ${feat_dir}/ramp2LG.png ${feat_dir}/zmax.png +append ${feat_dir}/zbar_neg.png
#convert zbar_pos.png zbar_neg.png +append zbar_both.png
#convert zbar_both.png Figures/3719/Sentences_FLAME1_TE11_BOLD.png -append ztest.png

echo "$zmax_pos $zmax_neg"
if [ `echo "$zmax_pos>0" | bc` -gt 0 ]
then
#  echo "writing1"
  composite -gravity NorthWest ${feat_dir}/zbar_pos.png $overlay_img ${feat_dir}/ztmp.png
else
  echo "No positive activation"
  cp $overlay_img ${feat_dir}/ztmp.png
fi

if [ `echo "$zmax_neg>0" | bc` -gt 0 ]
then
  echo "writing2"
  composite -gravity NorthEast ${feat_dir}/zbar_neg.png ${feat_dir}/ztmp.png ${feat_dir}/ztmp.png
else
  echo "No negative activation"
fi

convert -background black -fill white -pointsize $pointsize2 -border $border_sz -bordercolor black label:"$ztitle" ${feat_dir}/ztitle.png
convert -gravity center ${feat_dir}/ztitle.png ${feat_dir}/ztmp.png -append $outname
rm ${feat_dir}/ztitle.png ${feat_dir}/ztmp.png ${feat_dir}/zbar_neg.png ${feat_dir}/zbar_pos.png ${feat_dir}/zmin.png ${feat_dir}/zmax.png ${feat_dir}/rampLG.png ${feat_dir}/ramp2LG.png $overlay_img
if [ -f ${overlay_vol}.nii.gz ]
then
  rm ${overlay_vol}.nii.gz 
fi
