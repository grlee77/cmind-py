"""
**Logging Utilities**
Various functionality related to logging outputs for the C-MIND processing pipelines

cmind_func_info
cmind_logger
cmind_init_logging
log_cmd

"""
from __future__ import division, print_function, absolute_import
import errno
import os
import logging
import inspect
import subprocess
import tempfile
import warnings

from cmind.globals import cmind_verbosity_level

def _lvl2number(level_string): 
    """convert a logging level string into it's corresponding numerical code
    
    Parameters
    ----------
    level_string : {'DEBUG','INFO','WARNING','ERROR','CRITICAL'}
        string describing the desired logging level
        
    Returns
    -------
    numeric_level : int
        numeric code corresponding to the log level string
        
    """
    numeric_level=getattr(logging,level_string.upper(),None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % level_string)
    return numeric_level

def cmind_logger(log_level_console='INFO',logger_name='CMIND',logfile=None,log_level_file='DEBUG',file_mode='a',console_format_string=None,file_format_string=None, capture_warnings=True, datefmt='%m-%d-%y %H:%M'):
    """configures a logger
    
    Parameters
    ----------
    log_level_console : optional, {None,'DEBUG','INFO','WARNING','ERROR','CRITICAL'}
        desired logging level for output to the console
    logfile : optional, str
        file to log to
    log_level_file : optional, {'DEBUG','INFO','WARNING','ERROR','CRITICAL'}
        desired logging level for output to the file
    file_mode : optional, {'w','a'}
        overwrite or append to `logfile`   
    console_format_string : optional, string or None
        console log formatting string to be passed on to logging.Formatter()   
    file_format_string : optional, string or None
        file log formatting string to be passed on to logging.Formatter()   
    capture_warnings : bool
        control whether logging captures warnings
    datefmt : str, optional
        optional custom date formatting to be passed on to logging.Formatter()
    Returns
    -------
    logger : logger
        logger for use in logging by cmind applications
        
    """
    
    # create logger 
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    
    for h in logger.handlers:
        logger.removeHandler(h)

    if capture_warnings:
        logging.captureWarnings(capture_warnings)
        pyw_logger=logging.getLogger('py.warnings')

    if log_level_console:    
        # create console handler
        ch = logging.StreamHandler()
        
        # set the desired logging level
        ch.setLevel(_lvl2number(log_level_console))
    
        # create formatter 
        if not console_format_string:
            #datefmt='%m-%d-%y %H:%M'
            ch_formatter = logging.Formatter('%(name)s | %(levelname)s | %(message)s',datefmt=datefmt)  # - %(module)s
        else:
            ch_formatter = logging.Formatter(console_format_string,datefmt=datefmt)
        #add the formatter to the handler
        ch.setFormatter(ch_formatter)
        
        # add the handler to the logger
        logger.addHandler(ch) 
        if capture_warnings:
            pyw_logger.addHandler(ch)  
    
    # create file handler with a potentially different log level
    if logfile:
        if isinstance(logfile,str) and (logfile.lower()=='default'):  
            logfile=os.path.join(tempfile.mkdtemp(),'cmind_log.txt')
        fh = logging.FileHandler(logfile,mode=file_mode)
        fh.setLevel(_lvl2number(log_level_file))
        if not file_format_string:
            fh_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',datefmt=datefmt)    # - %(module)s
        else:
            fh_formatter = logging.Formatter(file_format_string,datefmt=datefmt)
        fh.setFormatter(fh_formatter)
        logger.addHandler(fh)
        if capture_warnings:
            pyw_logger.addHandler(fh)  
          
    return logger
    

def cmind_init_logging(verbose=True, logger=None, child_name=None):    
    """Initialize CMIND module logging
    
    Parameters
    ----------
    verbose : str, optional
        if True the module_logger level will be set to DEBUG, otherwise ERROR
    logger : logging.Logger or str
        if this is a string, it should be the name of the desired logfile
    child_name : str, optional
        if present, the logger will have the name: logger.name + child_name.
        child_name defaults to the calling function name.  If logger=None, the
        generated logger will have the name "child_name if it was specified"
        
    Returns
    -------
    module_logger : logging.Logger
        logger to be used within the CMIND module
    """
    
    if not child_name:
        child_name=inspect.stack()[1][3]  #gets the name of the calling function from the stack
        
    if isinstance(logger,logging.Logger):
        module_logger_name = logger.name
        module_logger_name += '.'+child_name
    else:
        module_logger_name = child_name        
        
    if isinstance(logger,logging.Logger):  #initialize from existing logger 
        module_logger=logging.getLogger(module_logger_name)
    else:  #initialize using logging
        if logger and isinstance(logger,str):
            module_logger=cmind_logger(logfile=logger,logger_name=module_logger_name)
        else: #no logger requested
            module_logger=cmind_logger(logger_name=module_logger_name,log_level_console='DEBUG') #only log critical errors to console and nothing to file
            
    if verbose:
        module_logger.setLevel('DEBUG')
    else:
        module_logger.setLevel('ERROR')    
        
    return module_logger
    

def log_cmd(shell_cmd,verbose=False,logger=None,print_stdout=None,stderr_to_stdout=False,print_cmd_header=False,env_dict=None,auto_retry=False,log_error=True):
    """ wrapper around subprocess.check_output
    
    Parameters
    ----------
    shell_cmd : str
        string representing the shell command to be run
    verbose : boolean, optional
        if True, print the shell_cmd to standard out
    logfile : str or file handle
        if logfile: print the shell_cmd to the logfile as well
    print_stdout : boolean, optional
        if True, also print out the standard output of the shell commands
    stderr_to_stdout : boolean, optional
        if True, redirect stderr to stdout  (recommend setting to False)
    print_cmd_header : boolean, optional
        if True, print "Executing shell command" header 
    env_dict : dict, optional
        if present, append/override the specified environment variables
    auto_retry : bool, optional
        if True, will retry the command once after failure
        
    Returns
    -------
    stdout : str
        Standard output of the command
        
    """
    
    def log_msg(logger,message):
        if logger:
            if isinstance(logger,str):
                try:
                    logger=open(logger,'a')
                    logger.write(message)
                    logger.close()
                except:
                    warnings.warn("failed to write to requested log file, {}".format(logger))
            else:
                logger.debug(message)
        else:
            print(message)
            
            
    if print_cmd_header:
        header_str="Executing shell command:\n"
    else:
        header_str=""
        
    if verbose:
        if logger:
            if isinstance(logger,str):  #manually log to file
                try:
                    print('Logging to:', logger)
                    logger=open(logger,'a')
                    logger.write("{}{}\n".format(header_str,shell_cmd))
                except:
                    warnings.warn("failed to print to requested log file, {}".format(logger))
            else:  #use logging object
                logger.info("{}{}".format(header_str,shell_cmd))
        else:  #just print to screen
            print("{}{}\n".format(header_str,shell_cmd))

    if not print_stdout:
        if verbose and cmind_verbosity_level>1:
            print_stdout=True
        else:
            print_stdout=False
            
    kwargs={}
    if env_dict:
        from copy import copy
        env=copy(os.environ)
        for k,v in env_dict.items(): #add or replace existing value
            env[k]=v
        kwargs['env']=env
    if stderr_to_stdout:
        kwargs['stderr']=subprocess.STDOUT
 
    try:
        stdout=subprocess.check_output(shell_cmd, shell=True, **kwargs)  #run in shell, redirecting stderr to stdout
    except subprocess.CalledProcessError as e:
        
        if auto_retry and e.errno == errno.EINTR:  #
            warnings.warn("EINTR sytem interrupt error encountered.  automatically retrying once")
            try:
                stdout=subprocess.check_output(shell_cmd, shell=True, **kwargs)  #run in shell, redirecting stderr to stdout
            except subprocess.CalledProcessError as e:
                pass
                
        if print_stdout or stderr_to_stdout:
            log_message = e.output
        else:
            log_message = ''
            
        if not stderr_to_stdout:
            log_message = log_message + '\n\nError occured in shell command: see stderr\n'
        else:
            log_message = log_message + '\n\nError occured in shell command, but was redirected to stdout (see above)\n'
        
        if log_error:
            log_msg(logger,log_message)    
        
        raise e

        
    if print_stdout:
        log_msg(logger,stdout)
        
    return stdout    
       

def cmind_func_info(logger=None,multiline_output=True,remove_functions_from_list=True):
    """Print the calling function name and its arguments
    
    Parameters
    ----------
    logger : logging.Logger, optional
        if unspecified print to console rather than logger
    multiline_output : bool, optional
        also print with one argument per line for easier reading
    remove_functions_from_list : bool, optional
        if True, do not print the functions and modules imported by the current frame    
    """
    
    #get the frame of the calling function
    frame,filename,line_number,func_name,lines,index=inspect.getouterframes(inspect.currentframe())[1]
    
    args, _, _, values = inspect.getargvalues(frame)
    #func_name=inspect.getframeinfo(frame)[2]  
    #func_name=inspect.stack()[1][3]  #gets the name of the calling function from the stack
    
    val_items=list(values.items()) #convert tuple to list so we can modify it

    if remove_functions_from_list:  #TODO: should probably change to a test to print only arguments matching the function signature
        def keep_test(pair):  #only keep the element if it is not a function or module
            pname,pval=pair
            pval=str(pval)
            is_function=str(pval).find('<function')!=-1
            is_module=str(pval).find('<module')!=-1
            return not (is_function or is_module)
        val_items = [pair for pair in val_items if keep_test(pair)] #list comprehension to prune val_items based on keep_test

    #print the function as called:
    params = ', '.join('{}={}'.format(*pair) for pair in val_items)
    info_string='{}({})'.format(func_name,params)
    
    if multiline_output: #print again, but with argument,value pairs split onto separate lines for easier reading       
        params = '\n     '.join('{} = {}'.format(*pair) for pair in val_items) #zip(args, [values[arg] for arg in args]))
        info_string=info_string+'\nfunction {} called with arguments:\n     {}'.format(func_name,params)
    
    info_string += '\n' 
    
    #either log it or just print to console
    if isinstance(logger,logging.Logger):  
        logger.info(info_string)
    else:
        print(info_string)   
        
        
        
def logger_info(logger):
    """Print a logger's name, handlers, and levels
    
    Parameters
    ----------
    logger : logging.Logger
        logger to print details about.  
        
    """
    if not logger:
        print("logger is: None")
    elif isinstance(logger,str):
        print("logger is: {}".format(logger))
    elif isinstance(logger,logging.Logger):
        print("logger name: {}".format(logger.name))
        print("logger level: {}".format(logger.level))
        print("logger handlers: ")
        for handler in logger.handlers:
            print("\thandler: {},  level: {}".format(handler,handler.level))
    else:
        raise ValueError("Unknown logger type")
          