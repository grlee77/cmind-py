"""
Utilities to find various external packages
"""

from __future__ import division, print_function, absolute_import
import os 
import sys
import subprocess
from os.path import join as pjoin
from cmind.utils.file_utils import which

if sys.platform == 'darwin':
    env_lib = 'DYLD_LIBRARY_PATH'
else:
    env_lib = 'LD_LIBRARY_PATH'

def find_FSL():
    """locate FSL via FSLDIR environment variable and determine its version

    Returns
    -------
    FSL_dir : $FSLDIR
    fsl_version : FSL version number

    """
    FSL_dir = os.getenv('FSLDIR')
    if not FSL_dir or (not os.path.exists(FSL_dir)):
        fsl_version = None
    else:
        try:
            fsl_version = str(
                subprocess.check_output(
                    'cat %s' %
                    (pjoin(FSL_dir, 'etc', 'fslversion')),
                    shell=True)).strip()
        except subprocess.CalledProcessError:
            fsl_version = None
    return (FSL_dir, fsl_version)


def find_shellscripts():
    """ Find the cmind shell scripts such as transpose_dirs.sh
    """
    import sys

    # manually specified path to cmind-py shell scripts
    cmind_shellscript_dir = os.getenv('CMIND_SHELLSCRIPT_DIR')

    # try relative path to the file within the source tree
    if not cmind_shellscript_dir:
        cmind_shellscript_dir = os.path.realpath(
            pjoin(
                os.path.dirname(
                    os.path.realpath(__file__)),
                '..',
                'bin'))
        if not os.path.exists(
                pjoin(cmind_shellscript_dir, 'fsl_motion_outliers_v2')):
            cmind_shellscript_dir = None

    # if not found there, installer should have put it within python's /bin
    if cmind_shellscript_dir is None or not os.path.isdir(
            cmind_shellscript_dir):
        python_bin_dir = os.path.dirname(sys.executable)
        if os.path.exists(pjoin(python_bin_dir, 'fsl_motion_outliers_v2')):
            cmind_shellscript_dir = python_bin_dir
        else:
            cmind_shellscript_dir = None

    # fallback: try to find it on the system path
    if cmind_shellscript_dir is None or not os.path.isdir(
            cmind_shellscript_dir):
        fname = which('fsl_motion_outliers_v2')
        if fname is None:
            cmind_shellscript_dir = None
        else:
            cmind_shellscript_dir = os.path.dirname(fname)

    return cmind_shellscript_dir


def find_ANTS():
    """Find ANTs by checking the following locations:
        1)  environment variable ANTSPATH
        2)  searching for antsRegistration on the path

    Returns
    -------
    ANTs_dir : dir
        directory containing the ANTs binaries
    """

    ANTs_dir = os.getenv('ANTSPATH')  # path to ANTs
    if ANTs_dir is None:
        # check if ANTs binaries are already on system path
        fname = which('antsRegistration')
        if fname is not None:
            ANTs_dir = os.path.dirname(fname)
        else:
            ANTs_dir = None
    elif not os.path.exists(ANTs_dir):
        ANTs_dir = None
    # not sure how to get version info for ANTs
    ANTs_version = 'unknown'
    return (ANTs_dir, ANTs_version)


def find_DTIPrep():
    """Find DTIPrep binary by checking the following locations:
        1)  environment variable DTIPREP
        2)  searching for DTIPrep on the path

    Returns
    -------
    DTIPrep : file
        filename of the DTIPrep binary
    DTIPrep_version : str
        DTIPrep version number
    """
    DTIPrep = os.getenv('CMIND_DTIPREP')
    if DTIPrep is None:
        # check if ANTs binaries are already on system path
        DTIPrep = which('DTIPrep')
    elif not os.path.exists(DTIPrep):
        DTIPrep = None
    DTIPrep_version = None
    if DTIPrep:
        try:
            DTIPrep_version = str(
                subprocess.check_output(
                    '{} --version'.format(DTIPrep),
                    shell=True)).strip()
            DTIPrep_version = DTIPrep_version.split(':')[-1].strip()
        except subprocess.CalledProcessError:
            DTIPrep_version = None
    return (DTIPrep, DTIPrep_version)


def find_Slicer3D():
    import warnings
    import os
    import sys
    import numpy as np
    from os.path import join as pjoin
    """Find 3D Slicer binary by checking the following locations:
        1)  environment variable $CMIND_3DSLICER
        2)  trying $SLICER4_HOME/Slicer
        3)  searching for Slicer on the path

    Returns
    -------
    Slicer3D : file
        filename of the 3D Slicer binary
    """
    Slicer3D = os.getenv('CMIND_3DSLICER')
    if not Slicer3D:
        if os.getenv('SLICER4_HOME'):
            Slicer3D = pjoin(os.getenv('SLICER4_HOME'), 'Slicer')
    if not Slicer3D:
        # check if Slicer binaries are already on system path
        Slicer3D = which('Slicer')
    elif not os.path.exists(Slicer3D):
        Slicer3D = None
    Slicer3D_lib_path = os.getenv('CMIND_3DSLICER_LIB')
    if Slicer3D_lib_path and os.path.isdir(Slicer3D_lib_path):
        Slicer3D_cli_path = pjoin(Slicer3D_lib_path, 'cli-modules')
        """
        The following paths should be added to the environment before calling
        3D Slicer
        """
        Slicer3D_Libs = [Slicer3D_lib_path, Slicer3D_cli_path]
    else:
        Slicer3D_lib_path = None
        Slicer3D_cli_path = None
        Slicer3D_Libs = None
    # create dictionary of environment variables to pass in for 3DSlicer
    Slicer3_env_dict = {}
       
    if (Slicer3D_Libs is not None) and np.any(
            [l not in os.environ[env_lib] for l in Slicer3D_Libs]):
        Slicer3_env_dict[env_lib] = os.environ[env_lib]
        for lib in Slicer3D_Libs:
            if lib not in Slicer3_env_dict[env_lib]:
                Slicer3_env_dict[env_lib] = Slicer3_env_dict[
                    env_lib] + os.pathsep + lib
    return Slicer3D, Slicer3D_Libs, Slicer3D_cli_path, Slicer3_env_dict


def get_Slicer3D_version(Slicer3D):  # slow, so don't run unless asked for
    """Find 3D Slicer version information

    Returns
    -------
    Slicer3D_version : str
        3D Slicer version number
    """
    Slicer3D_version = None
    if Slicer3D:
        try:
            Slicer3D_version = str(subprocess.check_output(
                '{} --version'.format(Slicer3D),
                shell=True)).strip().split(' ')[-1]
        except subprocess.CalledProcessError:
            Slicer3D_version = None
    return Slicer3D_version


def find_AFNI():
    """Find AFNI binary folder by checking the following locations:
        1)  environment variable AFNIPATH
        2)  searching for afni on the path

    Returns
    -------
    AFNI_dir : dir
        directory containing the AFNI binaries
    AFNI_version_info : str
        AFNI version number
    """
    # path to AFNI
    AFNI_dir = os.getenv('AFNIPATH')
    if not AFNI_dir:
        # check if AFNI binaries are already on system path
        fname = which('afni')
        if fname is not None:
            # redirect any errors about not finding it to stdout
            AFNI_dir = os.path.dirname(fname)
        else:
            AFNI_dir = None
    elif not os.path.exists(AFNI_dir):
        AFNI_dir = None
    AFNI_version_info = None
    if AFNI_dir:
        try:
            AFNI_version_info = str(subprocess.check_output(
                '{} -ver'.format(pjoin(AFNI_dir, 'afni')), shell=True)).strip()
        except subprocess.CalledProcessError:
            AFNI_version_info = None

    return (AFNI_dir, AFNI_version_info)


def find_GraphicsMagick():
    """Find GraphicsMagick by checking the following locations:
        1)  searching for gm on the path

    Returns
    -------
    has_GraphicsMagick : bool
        True if ImageMagick was found
    GraphicsMagick_version_info : str
        GraphicsMagick version information
    """
    if not which('gm'):
        has_GraphicsMagick = False
    else:
        has_GraphicsMagick = True

    GraphicsMagick_version_info = None
    if has_GraphicsMagick:
        try:
            GraphicsMagick_version_info = str(
                subprocess.check_output(
                    'gm version',
                    shell=True)).strip()
        except subprocess.CalledProcessError:
            GraphicsMagick_version_info = None
    return (has_GraphicsMagick, GraphicsMagick_version_info)


def find_ImageMagick():
    """Find ImageMagick by checking the following locations:
        1)  searching for convert on the path

    Returns
    -------
    has_ImageMagick : bool
        True if ImageMagick was found
    ImageMagick_version_info : str
        ImageMagick version information
    """
    if not which('convert'):
        has_ImageMagick = False
    else:
        has_ImageMagick = True
    ImageMagick_version_info = None
    if has_ImageMagick:
        try:
            ImageMagick_version_info = str(
                subprocess.check_output(
                    'convert --version',
                    shell=True)).strip()
        except subprocess.CalledProcessError:
            ImageMagick_version_info = None
    return (has_ImageMagick, ImageMagick_version_info)


def find_MNI_templates(cmind_FSL_dir=None):
    """Find MNI standard brains via searching in the following locations
        1) environment variable MNIDIR
        2) if FSL is installed, try $FSLDIR/data/standard

    Returns
    -------
    cmind_MNI_dir : dir
        directory containing the MNI standard brains
    """
    cmind_MNI_dir = os.getenv('MNIDIR')
    if not cmind_MNI_dir:
        if not cmind_FSL_dir:
            cmind_FSL_dir = find_FSL()[0]
        if cmind_FSL_dir:
            cmind_MNI_dir = pjoin(cmind_FSL_dir, 'data', 'standard')
    if cmind_MNI_dir and (
            not os.path.exists(pjoin(
                cmind_MNI_dir, 'MNI152_T1_2mm_brain.nii.gz'))):
        cmind_MNI_dir = None
    return cmind_MNI_dir


def check_exist(progs):
    """utility to check existance of various external programs

    Parameters
    ----------
    progs : str or list of str
        list of programs to find (case insensitive)

    Notes
    -----
    Can check for the following programs:
    FSL, ANTs, AFNI, DTIPrep, MNI Templates, 3DSlicer, ImageMagick
    """
    if isinstance(progs, str):
        progs = [progs, ]
    elif not isinstance(progs, (list, tuple)):
        raise ValueError(
            "check_exist() requires string or list of strings as input")
    for p in progs:
        if p.lower() == 'fsl':
            v = find_FSL()
        elif p.lower() == 'ants':
            v = find_ANTS()
        elif p.lower() == 'afni':
            v = find_AFNI()
        elif p.lower() == 'dtiprep':
            v = find_DTIPrep()
        elif p.lower() == 'mni':
            v = find_MNI_templates()
        elif (p.lower() == 'slicer' or p.lower() == 'slicer3d' or
              p.lower() == '3dslicer'):
            v = find_Slicer3D()
        elif p.lower() == 'imagemagick':
            v = find_ImageMagick()
        if not v:
            raise EnvironmentError("{} must be install to run this script" % p)
    return True
