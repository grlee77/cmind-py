#!/bin/bash

#Need to move into the directory containing the bash source file
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

echo "CMD: $*"


usage()
{
echo "Basic usage: cmind_normalize.py -o <output_dir> -t1 <T1_volume_nii> -rsf <reg_struct>  [options]

usage: cmind_normalize.py [-h] -o OUTPUT_DIR -t1 T1_VOLUME_NII -rsf REG_STRUCT
                          [-direct DIRECT_TO_MNI] [-rf REG_FLAG]
                          [-a ANTS_PATH] [-mni MNI_PATH] [-areg ANTS_REG_CASE]
                          [-prec ANTS_PRECISION] [-MNIres MNI_RESOLUTION]
                          [-gf GENERATE_FIGURES] [-u FORCEUPDATE] [-v]
                          [-log LOGGER]

Choose a pediatric template based on age and then normalize a structural image to that study template.  A precomputed transform to MNI space will then be applied

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which structural processing results are
                        stored
  -age AGE_MONTHS, --age_months AGE_MONTHS
             age of subject in months
  -t1 T1_VOLUME_NII, -t1_vol T1_VOLUME_NII, --T1_volume_nii T1_VOLUME_NII
                        T1W image (after brain extraction). This is the image
                        to register to standard space
  -rsf REG_STRUCT, --reg_struct REG_STRUCT
                        filename of the .csv file containing the registration
                        dictionary

optional arguments:
  -h, --help            show this help message and exit
  -direct DIRECT_TO_MNI, --direct_to_MNI DIRECT_TO_MNI
                        bypass study template and register directly to MNI
                        space
  -rf REG_FLAG, --reg_flag REG_FLAG
                        string of 3 characters controlling whether FLIRT,
                        FNIRT and/or ANTS registrations of T1 to standard are
                        to be run; doFLIRT = 0 or 1 (1 = do the regisration
                        using FLIRT),doFNIRT = 0 or 1 (1 = do the regisration
                        using FNIRT) (NOT CURRENTLY IMPLEMENTED),doANTS = 0, 1
                        or 2 (0 = skip, 1 = do using Exp, 2 = do using SyN),
                        default = '101' does FLIRT and ANTs (Exp)
  -a ANTS_PATH, -ants ANTS_PATH, --ANTS_path ANTS_PATH
                        path to the ANTs binaries
  -res MNI_PATH, --resolution_string MNI_PATH
                        path containing the MNI standard brains
  -areg ANTS_REG_CASE, --ANTS_reg_case ANTS_REG_CASE
                        {'Aff','SyN','AffSyn','SynAff'}; Type of ANTS
                        registration
  -prec ANTS_PRECISION, --ANTS_precision ANTS_PRECISION
                        {'lenient','ants_defaults','strict'}, choose from
                        preset precision levels for the registrations (default
                        = 'lenient')
  -MNIres MNI_RESOLUTION, --MNI_resolution MNI_RESOLUTION
                        {'2mm','1mm'}, resolution of MNI standard space image
                        to register
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        filename to log to
  -np
                        number of threads
"
}


if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=2
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi


source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/timer.sh

#this version of structural processing requires Python 2.7+
source $cwd/load_python_env.sh



#defaults for arguments:
age_months=200  #use older child template by default
ANTS_bias_cmd=N4BiasFieldCorrection
BET_threshold=0.40
ForceUpdate=true
generate_figures=true
verbose_str="--verbose"  #non-verbose by default

#more defaults for cmind_age_to_template.py
ped_template_path=$CMIND_TEMPLATE_DIR
resolution="2mm"

#more defaults for the normalization part
reg_flag='101' #[1 0 1] = 5 after conversion to decimal
ANTS_reg_case='SyNAff'  #GRL: changed from SyN to SyNAff on 1/22/14
reg_struct='Default'
ANTS_path='Default'
direct_to_MNI="False"


verbose_string=""
all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
	-np )
	    shift
	    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1;
	    ;;
    -age|--age_months )
        shift
        age_months=$1
        ;;
    -tdir|-template_dir|--ped_template_path )
        shift
        ped_template_path=$1
        ;;
    -t1|-t1_vol|--T1_volume_nii )
        shift
        T1_volume_nii=$1
        ;;
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -rsf|--reg_struct )
        shift
        reg_struct=$1
        ;;
    -direct|--direct_to_MNI )
        shift
        direct_to_MNI=$1
        ;;
    -rf|--reg_flag )
        shift
        reg_flag=$1
        ;;
    -a|-ants|--ANTS_path )
        shift
        ANTS_path=$1
        ;;
    -mni|--MNI_path )
        shift
        MNI_path=$1
        ;;
    -areg|--ANTS_reg_case )
        shift
        ANTS_reg_case=$1
        ;;
    -prec|--ANTS_precision )
        shift
        ANTS_precision=$1
        ;;
    -res|-MNIres|--resolution_string )
        shift
        resolution_string=$1
        MNI_resolution=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
	esac
    shift
done


if [ ! -z $verbose_string ]; then
    debug=true
else
    debug=false
fi

#output_dir=${output_dir}

echo $output_dir
echo $ANTS_bias_cmd
echo $BET_threshold
echo $ForceUpdate


#DON'T CHECK FOR THIS ONE HERE.  It will be created by cmind_age_to_template.py


if [ -z "${T1_brain_volume_nii+xxx}" ] || [ ! -f ${T1_brain_volume_nii} ]; then
    T1_brain_volume_nii=${output_dir}/T1W_Structural_N4_brain.nii.gz
    T1_volume_nii=${output_dir}/T1W_Structural_N4_brain.nii.gz
fi

if [ ! -f ${T1_volume_nii} ] || [ -z ${T1_volume_nii} ]; then
    echo "ERROR: T1 volume: ${T1_volume_nii} doesn't exist"
    exit 1
fi 

if [ $reg_struct = Default ]; then
    reg_struct=${output_dir}/reg_struct.csv
fi

if [ ! -d ${output_dir} ]; then
    echo "Creating T1 normalization output directory: ${output_dir}"
    mkdir -p "${output_dir}"
fi 

#Check that the ANTs executable is on the path
if [ $ANTS_path = Default ]; then
    command -v antsRegistration >/dev/null 2>&1 || { echo "The ANTs tool antsRegistration is required, but was not found. Aborting." >&2; exit 1; }
    ANTS_path=`command -v antsRegistration`
    ANTS_path=`dirname $ANTS_path`
fi

MNI_path=$FSLDIR/data/standard/
#if [ ! -d $MNI_path ]; then  #try alternate location
#    MNI_path=$CMIND_FSL_TEMPLATE_DIR
#fi

if [ ! -d $MNI_path ]; then
    echo "ERROR: MNI Template directory: $MNI_path doesn't exist"
    exit 1
fi 

# #the following lines replace the previous cmind_age_to_template.sh script
# t=$(timer)
# echo "python ${cmind_python_dir}/cmind_age_to_template.py ${age_months} ${ped_template_path} ${output_dir} --resolution_str ${resolution} --ForceUpdate $ForceUpdate $verbose_str"
# python ${cmind_python_dir}/cmind_age_to_template.py ${age_months} ${ped_template_path} ${output_dir} --resolution_str ${resolution} --ForceUpdate $ForceUpdate $verbose_str
# printf 'cmind_age_to_template:  Elapsed time: %s\n' $(timer $t)



#existence checks for all required conditional arguments
if [ -z "$age_months" ]; then
   echo "missing required argument: age_months"
fi
if [ -z "$ped_template_path" ]; then
   echo "missing required argument: ped_template_path"
fi
if [ -z "$output_dir" ]; then
   echo "missing required argument: output_dir"
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( age_months ped_template_path output_dir resolution_string ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
all_optional_args=''
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_age_to_template.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_age_to_template.py ${verbose_string} ${all_optional_args}
printf 'cmind_age_to_template.py:  Elapsed time: %s\n' $(timer $t)

reg_struct=$output_dir/reg_struct.csv
echo "reg_struct = $reg_struct"
if [ ! -f $reg_struct ] ; then
        echo "ERROR: Failed to finish T1 Age to Template"
        exit 1
fi 


#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    reg_struct
#    reg_struct_file


#the following lines replace the previous cmind_normalize.sh script
# t=$(timer)
# echo "python ${cmind_python_dir}/cmind_normalize.py ${output_dir} $MNI_path ${age_months} ${ANTS_path} ${T1_brain_volume_nii} ${reg_struct} --reg_flag ${reg_flag} --ANTS_reg_case ${ANTS_reg_case} --MNI_resolution $resolution --generate_figures ${generate_figures} --ForceUpdate $ForceUpdate"
# python ${cmind_python_dir}/cmind_normalize.py ${output_dir} $MNI_path ${age_months} ${ANTS_path} ${T1_brain_volume_nii} ${reg_struct} --reg_flag ${reg_flag} --ANTS_reg_case ${ANTS_reg_case} --MNI_resolution $resolution --generate_figures ${generate_figures} --ForceUpdate $ForceUpdate
# printf 'Normalization:  Elapsed time: %s\n' $(timer $t)


if [ -z "$T1_volume_nii" ]; then
   echo "missing required argument: T1_volume_nii"
fi
if [ -z "$reg_struct" ]; then
   echo "missing required argument: reg_struct"
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir T1_volume_nii reg_struct direct_to_MNI reg_flag ANTS_path MNI_path ANTS_reg_case ANTS_precision MNI_resolution generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
all_optional_args=''
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_normalize.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_normalize.py ${verbose_string} ${all_optional_args}
printf 'cmind_normalize.py:  Elapsed time: %s\n' $(timer $t)

#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    reg_struct


#check that at least one of the possible normalized outputs exists
output_file1_test=$output_dir/T1_to_MNI_lin.nii.gz
output_file1_test2=$output_dir/T1_to_MNI_nonlin.nii.gz
output_file1_test3=$output_dir/T1_to_MNI_ANTS_SyN_Deformed.nii.gz
echo "$output_file_test"
if [ ! -f $output_file_test ] || [ ! -f $output_file_test2 ] || [ ! -f $output_file_test3 ]; then
    echo "ERROR: Failed to finish normalization to standard space"
    exit 1
fi 

echo "Finished `basename $0`"
exit 0
