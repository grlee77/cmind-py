#!/bin/bash
if [ -z "$1" ]; then
    echo "Usage: cmind_tar.sh <input_file>"
    exit 1
fi

tar=${1/.nii.gz/.tar.gz}
dir=$(dirname ${1})
base=$(basename ${tar})
wild=${base/.tar.gz/*}

if [ -f $tar ]; then
    rm $tar
fi

echo "cd $dir"
cd $dir
echo "tar -zcf $(basename ${tar}) $wild"
/bin/tar -zcf $(basename ${tar}) $wild

#clean up the files that we tar'd up
rm $wild.txt
rm $wild.xml
rm $wild.nii.gz

if [ $? -ne 0 ];then
    echo "ERROR:  Could not create tar file"
    exit 1
fi

