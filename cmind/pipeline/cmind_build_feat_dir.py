#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_build_feat_dir(output_dir, preproc_tarfile, stats_tarfile, reg_tarfile, reg_standard_tarfile, reduce_file_level=1, poststats_tarfile=None, output_tar=None, verbose=False, ForceUpdate=False, logger=None):
    """ Build a first-level feat directory from separate first level .tar.gz files
    
    Parameters
    ----------
    output_dir : str
        desired output location for the first-level feat directory structure.
        The feat folder will be: output_dir/Feat_Level1
    preproc_tarfile : str
        tar file of preprocessing results
    stats_tarfile : str
        tar file of stats results
    reg_tarfile : str
        tar file of registration to anatomical/structural space
    reg_standard_tarfile : str
        tar file of registration to standard space
    reduce_file_level : int
        if 0 include all files from the source .tar.gz.  If 1, minimize the 
        resulting .tar.gz size be removing some files unnecessary for 2nd level
        analysis.  If >1, remove unneccessary files more aggressively.
    poststats_tarfile : str, optional
        tar file of first level poststats results (not required for 2nd level analysis)
    output_tar : str, optional
        output name for .tar.gz (will remove output_dir after compression).  If 
        output_tar is a filename rather than a full path, it will be placed 
        within output_dir.  If output_tar=='auto', the default filename will be 
        os.path.join(output_dir,'feat_level1.tar.gz').  
    ForceUpdate : bool, optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    
    output_name : str or None, optional
        will be the name of the generated directory or tar file
        
    
    """
    import os, glob, shutil
    from os.path import join as pjoin

    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import export_tarfile, imexist
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

        
    #convert various strings to boolean
    verbose = input2bool(verbose)
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting to Build Feat Directory from tar files")        
    
    
    #convert any relative paths to absolute paths
    output_dir=os.path.abspath(output_dir)
    
    output_subdir=pjoin(output_dir,'Feat_Level1') #
    
    preproc_tarfile=os.path.abspath(preproc_tarfile)
    stats_tarfile=os.path.abspath(stats_tarfile)
    reg_tarfile=os.path.abspath(reg_tarfile)
    reg_standard_tarfile=os.path.abspath(reg_standard_tarfile)
    if poststats_tarfile:
        poststats_tarfile=os.path.abspath(poststats_tarfile)
        
    if output_tar=='auto':
        output_tar=pjoin(output_dir,'feat_level1.tar.gz')
    elif output_tar:
        if os.path.dirname(output_tar)=='':
            output_tar = pjoin(output_dir,output_tar)
        output_tar=os.path.abspath(output_tar)
    
    if not os.path.isdir(output_subdir):
        os.makedirs(output_subdir)

    if not os.path.exists(preproc_tarfile):
        raise IOError("preproc_tarfile: %s, not found" % preproc_tarfile)
    if not os.path.exists(stats_tarfile):
        raise IOError("stats_tarfile: %s, not found" % stats_tarfile)
    if not os.path.exists(reg_tarfile):
        raise IOError("reg_tarfile: %s, not found" % reg_tarfile)
    if not os.path.exists(reg_standard_tarfile):
        raise IOError("reg_standard_tarfile: %s, not found" % reg_standard_tarfile)     
    
    if os.listdir(output_subdir):
        if ForceUpdate:
            shutil.rmtree(output_subdir)
        else:
            return output_subdir

    if ForceUpdate and os.path.exists(output_tar):
        os.remove(output_tar)
        
    tar_list=[preproc_tarfile, stats_tarfile, reg_tarfile, reg_standard_tarfile]  #tar files to extract directly into root of output_subdir
    
    if poststats_tarfile and (reduce_file_level<2):
        if not os.path.exists(poststats_tarfile):
            raise IOError("poststats_tarfile: %s, not found" % poststats_tarfile)
        tar_list.append(poststats_tarfile)
        
    for tarf in tar_list:
        log_cmd("tar zxvfh %s -C %s" % (tarf,output_subdir), verbose=verbose, logger=module_logger)  #GRL: may have to add -m option to avoid unable to modify utime errors
    
    if reduce_file_level>0:
        reg_files=glob.glob(pjoin(output_subdir,'reg','*'))
        reg_files = [f for f in reg_files if not os.path.isdir(f)]
        reg_files_keep = ['example_func2standard.mat','standard.nii.gz','standard.nii'] #only these are checked by feat at the 2nd level
        for f in reg_files:
            if not os.path.basename(f) in reg_files_keep:
                if os.path.islink(f):
                    os.unlink(f)
                else:
                    os.remove(f)

        if reduce_file_level==1: #only remove some of the largest files not needed at 2nd level

            to_remove = [pjoin(output_subdir,'stats','corrections'),
                         pjoin(output_subdir,'stats','res4d'),
                         pjoin(output_subdir,'stats','threshac1'),
                         ]
            for f in to_remove:
                imbool,f=imexist(f)[0:2]
                if imbool:
                    if os.path.islink(f):
                        os.unlink(f)
                    else:
                        os.remove(f)
        elif reduce_file_level>1:  #more comprehensive cleanup of first-level files unneeded at 2nd level
            stats_files=glob.glob(pjoin(output_subdir,'stats','*'))
            stats_files = [f for f in stats_files if not os.path.isdir(f)]

            for f in stats_files:
                bf = os.path.basename(f)
                iscope = 'cope' in bf
                isdof = 'dof' in bf
                if (not iscope) and (not isdof) and (not bf in ['smoothness','ratios','probs','logfile','cmlogfile']):
                    if os.path.islink(f):
                        os.unlink(f)
                    else:
                        os.remove(f)
    
            base_files=glob.glob(pjoin(output_subdir,'*'))
            base_files = [f for f in base_files if not os.path.isdir(f)]
            
            #also cleanup the base directory except for design, DLH, RESELS files
            for f in base_files:
                bf = os.path.basename(f)
                if (not 'design' in bf):
                    if os.path.islink(f):
                        os.unlink(f)
                    else:
                        os.remove(f)
    
    # copy the mean absolute and relative motion values to buildfeat dir, to import into database
    mcf_files=glob.glob(pjoin(output_dir,'*.rms'))
    mcf_files=glob.glob(os.path.join(output_dir,'..', '*.rms'))
    for f in mcf_files:
        shutil.copy(f,pjoin(output_subdir,os.path.basename(f)))
                
    if output_tar:
        export_tarfile(output_tar, output_subdir,arcnames='Feat_Level1')
        module_logger.info("Created Feat Level 1 archive: {}".format(output_tar))        
        shutil.rmtree(output_subdir)
        output_name=output_tar
    else:
        module_logger.info("Created Feat Level 1 folder in: {}".format(output_dir))        
        output_name=output_subdir
        
    #print any filename outputs for capture by LONI
    print("output_name:{}".format(output_name))
    return output_name
    

def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    verbose=True
    ForceUpdate=False
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
             
    if verbose:
        func=cmind_timer(cmind_build_feat_dir,logger=logger)
    else:
        func=cmind_build_feat_dir
      
    reduce_file_level=1
    
    paradigms=['Sentences','Stories']
    acq_types=['ASL','BOLD']
    for paradigm in paradigms:
        for acq_type in acq_types:

            source_feat_dir=os.path.join(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s' % acq_type)
            
            #basedir='/home/lee8rx/smb_shares/bmi/LONI/Greg/06M008/P/test/'
            output_dir=os.path.join(source_feat_dir,'level1_out')
            pre=os.path.join(source_feat_dir,'prep.tar.gz')
            stats=os.path.join(source_feat_dir+'_stats','stats.tar.gz')
            reg=os.path.join(source_feat_dir+'_reg','reg.tar.gz')
            regstd=os.path.join(source_feat_dir+'_reg','reg_standard_ANTS.tar.gz')
            poststats=os.path.join(source_feat_dir+'_poststats','poststats.tar.gz')
            otar=os.path.join(source_feat_dir,'..','..','{}_{}_glm1.tar.gz'.format(paradigm,acq_type))
            
            func(output_dir,pre,stats,reg,regstd,poststats_tarfile=poststats,
                 output_tar=otar, reduce_file_level=reduce_file_level,
                 verbose=verbose, ForceUpdate=ForceUpdate, logger=logger)
      
                                
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer
    
    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Build a first-level feat directory from separate first level .tar.gz files", epilog="")  #-h, --help exist by default
        #example of a positional argument
        
        parser.add_argument("-o","--output_dir",required=True, type=str, help="desired output location for the first-level feat directory structure")
        parser.add_argument("-pre","--preproc_tarfile", dest='pre', required=True, type=str, help="tar file of preprocessing results")
        parser.add_argument("-stats","--stats_tarfile", dest='stats', required=True, type=str, help="tar file of stats results")
        parser.add_argument("-reg","--reg_tarfile",dest='reg', type=str, required=True, help="tar file of registration to anatomical/structural space")
        parser.add_argument("-regstd","--reg_standard_tarfile",dest='regstd', required=True, type=str, help="tar file of registration to standard space")
        parser.add_argument("-poststats","--poststats_tarfile",dest='poststats', type=str, default="None", help="tar file of first level poststats results (not required for 2nd level analysis)")
        parser.add_argument("-otar","--output_tar",dest='otar', type=str, default="True", help="output name for .tar.gz (will remove output_dir after compression)")
        parser.add_argument("-reduce","--reduce_file_level",dest='reduce_file_level', type=int, default=1, help="0=keep all files from first level.  1=remove some unneccessary files.  2=more aggressive removal")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        #print args
        output_dir=args.output_dir   
        pre=args.pre
        stats=args.stats   
        reg=args.reg   
        regstd=args.regstd   
        poststats=str2none(args.poststats, none_strings=['none','default'])   
        otar=str2none(args.otar, none_strings=['none','default'])
        reduce_file_level = args.reduce_file_level
        ForceUpdate=args.ForceUpdate
        verbose=args.verbose   
        logger=args.logger   
            
        if verbose:
            cmind_build_feat_dir=cmind_timer(cmind_build_feat_dir,logger=logger)
    
        cmind_build_feat_dir(output_dir,pre,stats,reg,regstd,
                             poststats_tarfile=poststats,output_tar=otar, 
                             reduce_file_level=reduce_file_level, 
                             verbose=verbose, ForceUpdate=ForceUpdate,
                             logger=logger)