#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CMIND_ASLBOLD_DIR=$cwd/../ASLBOLD/
#CMIND_FSL_TEMPLATE_DIR='/cluster/loni/code/fsl_templates/' #$cwd/../../../../fsl_templates/
CMIND_DTIHARDI_DIR=$cwd/../DTIPrep_protocols/
CMIND_TEMPLATE_DIR='/cluster/loni/code/cmind_templates_new/' #TODO: fix hardcode
CMIND_SHELLSCRIPT_DIR=$cwd/../../bin/
#CMIND_EXAMPLE_DIR=$cwd/../../cmind/data/
#CMIND_EXAMPLE_OUTPUT_DIR=/scratch/LONI/


#echo "CMIND_ASLBOLD_DIR=$CMIND_ASLBOLD_DIR"
#echo "CMIND_DTIHARDI_DIR=$CMIND_DTIHARDI_DIR"
#echo "CMIND_TEMPLATE_DIR=$CMIND_TEMPLATE_DIR"
#echo "CMIND_SHELLSCRIPT_DIR=$CMIND_SHELLSCRIPT_DIR"
#echo "CMIND_EXAMPLE_DIR=$CMIND_EXAMPLE_DIR"
