#various decorators for potential used by the C-MIND project
"""
**Function decorators**
"""
from __future__ import division, print_function, absolute_import
import time, functools, logging

def cmind_timer(func,print_time=True,logger=None):
    """decorator to print elapsed time to run a function
    
    Parameters
    ----------
    func : function to time
    print_time : bool, optional
        if true print out the elapsed time with either logger or print
    logger : logging.Logger, optional
        logger to print timing information to
        
    Returns
    -------
    wrapper : function wrapped with timing printouts

    """
    @functools.wraps(func)
    def wrapper(*args,**kwargs):
        t1 = time.time()
        res = func(*args,**kwargs)
        if print_time:
            t2 = time.time()
            #time_str='func:%r took: %2.4f sec' % (func.__name__, t2-t1)
            time_str='TIMING:{} took {} seconds'.format(func.__name__, t2-t1)
            if logger and isinstance(logger,logging.Logger):
                logger.info(time_str)
            else:
                print(time_str)
        return res
    return wrapper
