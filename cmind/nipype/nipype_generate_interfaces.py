#!/usr/bin/env python
""" automated generation of nipype interfaces from cmind.pipeline docstrings  

    Warning:  The generated interfaces rely on the docstrings, so these must be 
    accurate for the interfaces to be valid.
"""

import os, inspect, tempfile
try:
    from numpydoc.docscrape import NumpyDocString
except:
    raise ImportError("the numpydoc package is required in order to autogenerate the cmind nipype interfaces")
    
from cmind.utils.utils import confirm  

#define dictionary to convert numpydoc docstring type information to traits types
type_dict={}
type_dict['bool']='traits.Bool'
type_dict['int']='traits.Int'
type_dict['float']='traits.Float'
type_dict['list']='traits.List'
type_dict['list of str']='traits.List(traits.String)'
type_dict['list of tuple']='traits.List(traits.Tuple)'
type_dict['list of bool']='traits.List(traits.Bool)'
type_dict['list of float']='traits.List(traits.Float)'
type_dict['list of int']='traits.List(traits.Int)'
type_dict['tuple']='traits.List'
type_dict['str']='traits.String'
type_dict['str (filename)']='traits.File'
type_dict['file']='traits.File'
type_dict['filename']='traits.File'
type_dict['directory']='traits.Directory'
type_dict['dir']='traits.Directory'
type_dict['dict']='traits.DictStrAny'
type_dict['enum']='traits.Enum'
type_dict['array']='traits.Array'
type_dict['ndarray']='traits.Array'
type_dict['logging.Logger']='traits.String'
return_type_dict=type_dict
#    return_type_dict['str']='File'
    
def generate_specs(func,type_dict = type_dict):
    """ generates input and output specs by parsing the docstrings using
        numpydoc.docscrape.NumpyDocString, so the docstrings must be in numpydoc 
        format.
        
        Notes:
            
            For inputs:
                for sets {'item1','item2','item3'}, all items must be on a single
                line within the docstring
            
                traits.Either will be used if multiple types are separated by commas
                or the string ' or ' in the docstring
            
                if 'optional' was not specified the trait will be mandatory
                
            For outputs:
                if 'optional' was not specified exists=True will be set
        
    """
    #if a set of options , e.g. {'a','b','c'} are given, then use type enum
    #    warning:  the full set must be on a single line in the docstring for this to work!
    def _parse_enum(ptype, mstr):
        if ptype.find('{')!=-1:  #if found set notation, this is an enumerated type
            ptype_str='traits.Enum('                    
            enum_list=list(eval(ptype.strip('"')))
            enum_str='"{}",'.format(enum_list[0])
            for e in enum_list[1::]:
                enum_str=enum_str+'"{}",'.format(e)
            mstr=enum_str+mstr
        else:
            ptype_str=None
           
        return (ptype_str,mstr)
        
    def _parse_types(ptype):
        #just use first ptype in list for the auto-conversion
        orlist=ptype.split(' or ')   #bugfix: need spaces around 'or' here to 
                                     #avoid spliting directory, etc.
        ptypes=[]
        for o in orlist:
            ptypes = ptypes + o.split(',')
        ptypes = [p.strip() for p in ptypes] 

        if 'None' in ptypes:
            allow_none=True #TODO
            ptypes = [p for p in ptypes if p!='None'] #remove None from the list of types
        else:
            allow_none=False
        
        if len(ptypes)>1:  #allow multiple trait types via traits.Either
            ptype_str='traits.Either('  
            tlist=[] #keep a list of already added types to avoid duplicates
            for p in ptypes:
                if p in type_dict:
                    t=type_dict[p]
                    if not t in tlist:
                        tlist.append(t)
                        ptype_str=ptype_str+t+', ';
                else:
                    raise Exception('type: {} not found in type_dict  (pname={}, func={})'.format(p, pname,func))
            if len(tlist)==1: #if there was only one non-duplicate type, don't use traits.Either
                if t.find('(')==-1:
                    ptype_str=t+'('
                ptype_str=ptype_str.replace(')','')

        else:
            if ptypes[0] in type_dict:
                ptype_str=type_dict[ptypes[0].strip()]+'('
            else:
                print("Exception in _parse_types() for ptype={}\n".format(ptype))
                raise Exception('type: {} not found in type_dict'.format(p))
        if allow_none:
            #ptype_str='traits.Trait(None,'+ptype_str
            #ptype_str=ptype_str.rstrip('(')+','
            ptype_str='traits.Any('  #Kludge.  should fix so allows only None or the specified type
            
        return (ptype_str, ptypes)
                        
    npdocstr=NumpyDocString(inspect.getdoc(func))
    #print npdocstr._parsed_data['Parameters']
    #print npdocstr._parsed_data['Returns']
    
    input_spec=[]
    for idx,param in enumerate(npdocstr._parsed_data['Parameters']):
        pname,ptype,pdesc=param
    
        if ptype.find('optional')==-1:
            mstr='mandatory=True, position=%d, ' % idx
        else:
            mstr='mandatory=False, '
            
        if pname=='output_dir':
            mstr='mandatory=False, '
            
        ptype=ptype.replace(', optional','').replace(',optional','')
        
        (ptype_str,mstr)=_parse_enum(ptype, mstr) 
        
        if ptype_str != 'traits.Enum(':
            (ptype_str, ptypes)=_parse_types(ptype)
                
        desc=pdesc[0]
        desc=desc.replace('"',"'") #replace any double quotes in the description with single quotes
        input_spec_line=['%s = %s%sdesc="%s")\n' % (pname,ptype_str,mstr,desc)]
        input_spec=input_spec+input_spec_line
    
    output_spec=[]
    for idx,param in enumerate(npdocstr._parsed_data['Returns']):
        pname,ptype,pdesc=param
        
        if ptype.find('optional')==-1: 
            exist_str="exists=True"
        else:
            exist_str="exists=False"

        ptype=ptype.replace(', optional','').replace(',optional','')
        
        (ptype_str,mstr)=_parse_enum(ptype, mstr) 
        
        if ptype_str != 'traits.Enum(':
            (ptype_str, ptypes)=_parse_types(ptype)
            
        desc=pdesc[0]
        desc=desc.replace('"',"'") #replace any double quotes in description with single quote
        output_spec_line=['%s = %s%s,desc="%s")\n' % (pname, ptype_str, exist_str, desc)]
        output_spec=output_spec+output_spec_line
        
    return (input_spec, output_spec, npdocstr)
     
def generate_interface_code(func,input_spec,output_spec,npdocstr=None):    
    """ takes an input_spec and output_spec from generate_specs and generates
    the nipype interface code for the function
    """
    #get a list of returned variables from the numpy docstring
    if npdocstr:
        return_variables = [n for (n,t,d) in npdocstr._parsed_data['Returns']]
    else:
        return_variables = []
        
    #build a list of returned variables to be used in generating 
    #_run_interface() below
    rvars=""
    for v in return_variables:
        if not rvars:
            rvars='('
        rvars=rvars+'self._{},'.format(v)
    if rvars:
        rvars=rvars.rstrip(',')+') = '
    
    #generate the InputSpec
    interface_code=[]
    interface_code.append('class {}InputSpec(BaseInterfaceInputSpec):\n'.format(func.__name__))
    for line in input_spec:
        interface_code.append('    '+line)
    interface_code.append('\n\n')

    #generate the OutputSpec    
    interface_code.append('class {}OutputSpec(TraitedSpec):\n'.format(func.__name__))
    for line in output_spec:
        interface_code.append('    '+line)
    interface_code.append('\n\n')
    
    #generate the Interface
    interface_code.append('class {}(BaseInterface):\n'.format(func.__name__))
    interface_code.append('    input_spec={}InputSpec\n'.format(func.__name__))
    interface_code.append('    output_spec={}OutputSpec\n\n'.format(func.__name__))

    #generate the _run_interface() function
    interface_code.append('    def _run_interface(self, runtime):\n')
    interface_code.append('        from cmind.pipeline import {} as {}\n'.format(func.__name__,func.__name__.replace('cmind_','')))
    interface_code.append('        args = {}\n')
    interface_code.append('        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:\n')
    interface_code.append('            value = getattr(self.inputs, key)\n')
    interface_code.append('            if isdefined(value):\n')
    interface_code.append('                args[key] = value\n')
    interface_code.append('        {}{}(**args)\n'.format(rvars,func.__name__.replace('cmind_','')))
    interface_code.append('        return runtime\n\n')

    #generate the _list_outputs() function
    interface_code.append('    def _list_outputs(self):\n')
    interface_code.append('        outputs=self._outputs().get()\n')
    for v in return_variables:
        interface_code.append('        outputs["{}"]=self._{}\n'.format(v,v))
    interface_code.append('        return outputs\n')
    interface_code.append('\n\n')
    
    return interface_code

                
def generate_interface_template(func,out_file=None,append=False):
    """ generates the interface code for function func and write it to out_file
    """
    (input_spec, output_spec, npdocstr)=generate_specs(func, type_dict = type_dict)
    interface_code=generate_interface_code(func,input_spec,output_spec, npdocstr)
    
    if not out_file:
        out_file=os.path.join(tempfile.gettempdir(),"{}_ift.py".format(func.__name__))
    #if os.path.exists(out_file):
    #    raise ValueError("out_file already exists!.  remove it first")
    if append:
        f=open(out_file,'a')        
    else:
        f=open(out_file,'w')
    f.writelines(interface_code)
    f.close()
                
def generate_all_cmind_interfaces(out_file=None):
    import cmind.pipeline
    all_funcs=dir(cmind.pipeline)
    
    #all pipeline functions start with cmind_*, so filter out any other symbols
    all_funcs = [item for item in all_funcs if item.find('cmind_')==0]

    if not out_file:
        import cmind.nipype.nipype_generate_interfaces
        tmp=os.path.dirname(cmind.nipype.nipype_generate_interfaces.__file__)
        out_dir=os.path.join(tmp,'interfaces')
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        out_file=os.path.join(out_dir,'cmind_interfaces.py')  #'/tmp/nipype_interfaces_generated.py'   
        
    init_out_file=os.path.join(os.path.dirname(out_file),'__init__.py')
        
    if os.path.isfile(init_out_file) or os.path.isfile(out_file):
        prompt = "\nWarning:  at least one of the following files already exists:\n"
        prompt += "{}\n{}\n".format(init_out_file,out_file)
        prompt += "Do you want to proceed, overwriting these files?"
        overwrite=confirm(prompt, resp=False)

    if overwrite:
        for idx,func_name in enumerate(all_funcs):
            print(func_name)
            if idx==0:
                f=open(out_file,'w')
                f.write('import os\n')
                f.write('from nipype.interfaces.base import (traits,BaseInterface, BaseInterfaceInputSpec, TraitedSpec, isdefined) #, File\n\n\n')
                f.close()
                f2=open(init_out_file,'w')
            
            append=True
            func=eval("cmind.pipeline.{}".format(func_name))
            generate_interface_template(func,out_file=out_file,append=append)
            f2.write('from .cmind_interfaces import {}\n'.format(func_name))
        f2.close()
        print("autogenerated interface files:\n{}\n{}".format(init_out_file,
                                                                out_file))
    else:
        print("interface generation aborted by user")
    
if __name__ == '__main__':
    generate_all_cmind_interfaces()