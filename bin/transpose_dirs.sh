#!/bin/bash
#solution from:  http://stackoverflow.com/questions/1729824/transpose-a-file-in-bash

if [ $# -gt 1 ]; then
    output_name=$2
else 
    fileroot=`basename $1 .txt`
    output_name=${fileroot}_transpose.txt
fi

#echo $fileroot
#echo "${fileroot}_transpose.txt"
awk '
{ 
    for (i=1; i<=NF; i++)  {
        a[NR,i] = $i
    }
}
NF>p { p = NF }
END {    
    for(j=1; j<=p; j++) {
        str=a[1,j]
        for(i=2; i<=NR; i++){
            str=str" "a[i,j];
        }
        print str
    }
}' $1 > $output_name


