.. _pipelines_struct:

Structural Processing
=====================

A common processing stream for the T1-weighted anatomical scans was used for
both the physiological and functional scanning sessions.  

Defacing Structural Volumes
---------------------------

Prior to making the "raw" structural data available, defacing was performed to further
anonymize the data.  First, defacing of each pediatric template was performed manually to 
create a mask preserving the brain and scalp, but without facial regions.  The defacing 
procedure was then performed as follows:

- A copy of the subjects anatomical T1 or T2-weighted volume was made and bias corrected using the N4 algorithm [Tustison2010]_.  
- The defaced age-appropriate pediatric head template was then warped into the subject's anatomical space using the SyN algorithm [Avants2008]_.
- The computed transform was used to warp the non-face mask into subject space and then the mask was multiplied by the original (pre bias-correction) anatomical to give a "raw" volume that has been defaced, but has not yet had any other processing applied.


Structural Preprocessing
------------------------

The structural volume was first bias corrected using the N4 algoithm 
[Tustison2010]_ as implemented in ANTs_.  The brain was then extracted using 
FSL's brain extraction tool [Smith2002]_.  The brain mask was applied to the 
original structural volume and N4 bias correction was repeated on the 
brain-extracted volume. 

.. Note:: Brain extraction did not perform well on a handful of the youngest
    subjects (e.g. less than 6 months old).  For these subjects, we will need to
    implement a revised approach)

Segmentation
------------

A 3-class tissue segmentation was performed using FSL's FIRST [Zhang2001]_.  No
priors were used during segmentation.

.. Note:: Segmentation results are currently poor for very young subjects (e.g. 
    less than 6 months old).  For these subjects, we will need a revised approach)


.. _Normalization:

Registration to the Study Template
----------------------------------

Study templates based on T1-weighted anatomical data processed using the 
``buildtemplateparallel.sh`` script from ANTs_ were created for the following 
age groups.

- 0-6 months
- 6 months to 2 years
- 2 year to 4 years
- 4 years to 18 years

Based on the subject's age at scan, a nonlinear symmetric diffeomorphic image 
registration was used to transform the structural volume into the space of the 
study template.  This algorithm is referred to as SyN [Avants2008]_ as 
implemented in ANTs_ under *antsRegistration*.

.. Note:: The cmind_normalize.py module also has options to use other registration 
    approaches such as FLIRT, but the data in the C-MIND database was registered
    using SyN.

Normalization to MNI space
--------------------------

Affine transformations from the study templates to MNI space were  precomputed
and then used across all subjects.  These affine transformations are
provided ``TODO:  LINK HERE``.  The template folders are labeled by the age
ranges in months.  The affine transformation from a 2mm study template to 2mm 
MNI space appropriate for a 5 year old would be the one in:  
``/48to216/SyN/48to216_to_MNI_brain_2mm_ANTS_SyN_0GenericAffine.mat``

The affine transformation is concatenated with the nonlinear transformation 
from anatomical to study template space computed above to generate a 
transformation directly from anatomical to MNI space without intermediate 
interpolation stages.  See the help text for the ANTs_ tool 
*antsApplyTransforms* for more information on concatenating transforms.

.. include:: links_names.txt