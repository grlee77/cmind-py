
.. _Database_files:

Database File Variables
=======================

Raw
---

These are theraw image data immediately after conversion to gzipped NIFTI format.  These data were used as the starting point for the various processing pipelines that generated the processed image types described in the subsequent sections.

The following raw datatypes are present in the database

All data are stored in Left-Anterior-Superior (LAS) orientation.  

- **Alpha**: ASL inversion efficiency estimation scan
- **Functional**:  Simultaneous ASL/BOLD data
- **DTI**: DTI data
- **HARDI**: HARDI data
- **RestingState**:  Resting state BOLD data
- **Anatomical**: T1-weighted anatomicals
- **T2**:  T2-weighted anatomicals
- **BaselineCBF**:  Resting state CBF-weighted images acquired via a pCASL pulse sequence
- **T1EST_TI100, ... T1EST_TI3000**:  These are EPI volumes acquired at a range of inversion times.


Structural Processing
---------------------

T1-weighted structural data was acquired in each scanning session.  Most subjects have this data from both the physiological and functional scanning sessions.

- **Processed Structural Image**:  This is the T1 anatomical after intensity normalization and brain extraction.  (in subject space)
- **Linear (FLIRT) Normalized T1**:  This is the Processed Structural Image, affine transformed to MNI standard space (via `FSL's FLIRT <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT>`_)
- **Nonlinear (ANTS) Normalized T1**:  This is the Processed Structural Image, after nonlinear deformation to MNI standard space (via `ANTs SyN <http://http://stnava.github.io/ANTs/>`_)
- **T1 CSF Partial Volume Estimate**:  partial volume estimate from FAST-based segmentation for the cerebrospinal fluid
- **Linear (FLIRT) T1 CSF Partial Volume Estimate**:  CSF partial volume estimate, affine transformed to MNI standard space (via `FSL's FLIRT <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT>`_)
- **Nonlinear (ANTS) Normalized T1 CSF Partial Volume Estimate**:  CSF partial volume estimate, after nonlinear deformation to MNI standard space (via `ANTs SyN <http://http://stnava.github.io/ANTs/>`_)
- **T1 Grey Matter Partial Volume Estimate**:  partial volume estimate from FAST-based segmentation for the grey matter
- **Linear (FLIRT) T1 Grey Matter Partial Volume Estimate**:  Grey Matter partial volume estimate, affine transformed to MNI standard space (via `FSL's FLIRT <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT>`_)
- **Nonlinear (ANTS) Normalized T1 Grey Matter Partial Volume Estimate**:  Grey Matter partial volume estimate, after nonlinear deformation to MNI standard space (via `ANTs SyN <http://http://stnava.github.io/ANTs/>`_)
- **T1 White Matter Partial Volume Estimate**:  artial volume estimate from FAST-based segmentation for the white matter
- **Linear (FLIRT) T1 White Matter Partial Volume Estimate**:  white matter partial volume estimate, affine transformed to MNI standard space (via `FSL's FLIRT <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT>`_)
- **Nonlinear (ANTS) Normalized T1 White Matter Partial Volume Estimate**:  white matter partial volume estimate, after nonlinear deformation to MNI standard space (via `ANTs SyN <http://http://stnava.github.io/ANTs/>`_)

All registrations were performed first to an age-appropriate study template.  A previously computed transformation for the study template space to MNI space was then applied.  There are currently two versions of the registered results:

1. Using `FSL's FLIRT <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT>`_ at each stage: Anatomical->Study Template->MNI
2. Using `ANTs SyN <http://http://stnava.github.io/ANTs/>`_ nonlinear registration at each stage:  Anatomical->Study Template->MNI


Pediatric Template Generation
------------------------------

A series of four 2mm T1-Weighted age-specific templates in MNI coordinate space were generated using repeated non-rigid registration of inidividual T1W volumes as implemented in ANTs buildtemplateparallel.sh script.
The four templates correspond to the following age ranges:

- 0-6 months
- 6 months - 24 months (2 years)
- 24 - 48 months (2-4 years)
- 48 - 216 months (4-18 years)

These templates can be downloaded at: `CMIND_Templates <https://bitbucket.org/grlee77/cmind-py/downloads>`_


DTI/HARDI Processing
--------------------

For DTI/HARDI, both raw data and data after automatated quality control / coregistration are available.  Quality control (QC) was performed via the `DTIPrep <https://www.nitrc.org/projects/dtiprep/>`_ software package .  This software discards "bad" directions corresponding to slice dropouts, interlace artifacts, or excessive motion.  Coregistration/eddy correction was then performed on the directions that were retained.  The QC'd datasets will have a variable number of directions kept per subject.  This number of "good" directions (not counting b=0 volumes) can be queried via the properties variable "Directions Kept".  The maximum possible value is 61 directions, corresponding to all directions kept.  The corresponding b-value and b-vector arrays for use are also available as part of the DTI QC or HARDI QC downloads.

- **DTI QC**:  The is the DTI data after automated quality control
- **HARDI QC**:  The is the HARDI data after automated quality control

In both cases the QC download is a .tar.gz file containing the following outputs from preprocessing:

- **{BASENAME}_QCed.nii.gz**:  This is the gzipped NIFTI volume corresponding to the output from DTIPrep
- **{BASENAME}_bvals.txt**:  This is a single column text file containing the b-values for the volumes in {BASENAME}_QCed.nii.gz
- **{BASENAME}_bvecs.txt**:  This is a three-column text file containing the b-vectors for the volumes in {BASENAME}_QCed.nii.gz
- **{BASENAME}_dirs_kept.txt**:  This text file just contains the total number of directions that were kept (not counting B0.  Max possible is 61)
- **{BASENAME}_QCReport.txt**:  The full text format output of the DTIPrep preprocessing run
- **{BASENAME}_QCReport.xml**:  The full XML format output of the DTIPrep preprocessing run


Query Example:

.. figure::  img/HARDI_directions_query_example.png
   :align:   center

   Example query for any subjects with HARDI data and >45 directions retained after DTIPrep preprocessing


Physiological Processing
------------------------

- **Alpha Value**:  This is a number corresponding to the estimated pCASL inversion efficiency.  
- **T1 Map**: This is the estimated T1 relaxation time map, in the low-resolution subject space
- **BaselineCBF Map**:  This is the estimated quantitative CBF map, in the low-resolution subject space
- **Linear (FLIRT) Normalized BaselineCBF Map**:  This is the BaselineCBF Map, affine transformed to MNI standard space (via `FSL's FLIRT <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT>`_)
- **Nonlinear (ANTS) Normalized BaselineCBF Map**:  This is the BaselineCBF Map, after nonlinear deformation to MNI standard space (via `ANTs SyN <http://stnava.github.io/ANTs/>`_)

Functional Processing
---------------------

For a given subject and longitudinal session, there are up to four sets of first-level outputs from the functional analysis.  These are:

- **ASL Sentences Level 1**
- **BOLD Sentences Level 1**
- **ASL Stories Level 1**
- **BOLD Stories Level 1**

As the names indicate, these correspond to either the ASL or BOLD timeseries for either the Stories or Sentences paradigm.  ASL and BOLD timeseries were acquired simultaneously as described in the following publication:  

	*Schmithorst VJ, Hernandez-Garcia L, Vannest J, Rajagopal A, Lee G, Holland SK.  Optimized simultaneous ASL and BOLD functional imaging of the whole brain. J Magn Reson Imaging. 2013;  In Press*:  `http://www.ncbi.nlm.nih.gov/pubmed/24115454 <http://www.ncbi.nlm.nih.gov/pubmed/24115454>`_

The picture matching paradigm was only performed in subjects age >=7 years.  Subject responses were recorded and can be determined from the query interface via the variables under the following category name:


Contrasts
^^^^^^^^^

	**For BOLD (either paradigm)**:

	- *contrast 1*:  positive BOLD activation
	- *contrast 2*:  negative BOLD activation (deactivation)

	**For ASL (either paradigm)**:

	- *contrast 1*:  positive BOLD activation
	- *contrast 2*:  positive ASL activation
	- *contrast 3*:  positive ASL baseline
	- *contrast 4*:  negative BOLD activation (deactivation)
	- *contrast 5*:  negative ASL activation (deactivation)

	Outputs of the first level fMRI analysis are stored in a .tar.gz file containing the full first level FEAT folder from FSL.  After extracting these folders for a set of subjects they are ready for use in a second level analysis.  Registration to standard space has already been performed via `ANTs SyN <http://http://stnava.github.io/ANTs/>`_.  No registration should be run at the second level.

	The structure of the various files and subfolders present are described in detail at the FSL wiki: `FEAT Output Details <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FEAT/UserGuide#FEAT_Output>`_

Behavioral Responses
^^^^^^^^^^^^^^^^^^^^

	The behavioral responses recorded during the Sentence/Picture matching task can be queried via the following variables in the database:

	Catagory:  *Behavioral Responses from Picture Tasks*

		- *Percentage Correct Active*
		- *Percentage Correct Passive*
		- *Percentage Correct Words*
  









