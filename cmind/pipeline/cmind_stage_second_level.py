#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_stage_second_level(output_dir, tarfile_list, output_tar=None, preserve_subdir_names=True, verbose=False, logger=None):
    """ Build a first-level feat directory from separate first level .tar.gz files
    
    Parameters
    ----------
    output_dir : str
        desired directory in which to extract the first level results for further processing
    tarfile_list : list of str or str
        list of .tar or .tar.gz files corresponding to first level analyses
        If string, this is assumed to be the path to a text file containing one tar file name per line
    output_tar : str, optional
        If provided, all first level subjects can be combined into a single .tar.gz
    preserve_subdir_names : bool, optional
        If provided, the subject directory names will match the basename of the 
        input tar files (minus the .tar.gz extension)
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    Returns
    -------
    level1_dir_list : str
        list of the first-level feat analysis folders.  
        Stored as output_dir/level1_dir_list.txt
        
    """
    import os, warnings
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import export_tarfile,split_multiple_ext
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    
    #convert various strings to boolean
    verbose = input2bool(verbose)
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Staging Files for 2nd-level analysis")      
    
    #convert any relative paths to absolute paths
    output_dir=os.path.abspath(output_dir)
    
    tar_list = []
    #read in the first level feat directories and make sure they all exist
    if isinstance(tarfile_list,(list,tuple)):
        tar_list=tarfile_list
    else: #can read from a file as well
        if not os.path.exists(tarfile_list):
            raise IOError("Specified tarfile_list file, %s, not found!" % tarfile_list)
        for line in open(tarfile_list,'r'):
            line=line.strip()
            if line: #skip empty lines
                if line[0]=="#": #skip commented lines
                    continue
                else:
                    tar_list.append(os.path.abspath(line))
    for tarf in tar_list:
        if not os.path.exists(tarf):
            raise IOError("Specified tar file, %s, not found!" % tarf)
    Ntar=len(tar_list)
    print(Ntar)
    
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    subj_dirs=[]
    for idx,tarf in enumerate(tar_list):
        if preserve_subdir_names:
            subjdir_name=split_multiple_ext(os.path.basename(tarf))[0]
            subj_dir=os.path.join(output_dir,subjdir_name)
        else:
            subj_dir=os.path.join(output_dir,'lvl1_subj%04d' % idx)
        if os.path.exists(subj_dir):
            if os.listdir(subj_dir):
                #raise IOError("non-empty subject directory, %s, already exists!",subj_dir)
                warnings.warn("non-empty subject directory, {}, already exists!  Skipping this input file...".format(subj_dir))
                continue
        else:
            os.makedirs(subj_dir)
        subj_dirs.append(subj_dir)  
        
        ext=os.path.splitext(tarf)[1]
        if ext=='.gz':
            if os.path.splitext(tarf[:-3])[1]=='.tar':
                log_cmd('tar zxvf "%s" --strip 1 -C "%s"' % (tarf,subj_dir), verbose=verbose, logger=module_logger)  #GRL: may have to add -m option to avoid unable to modify utime errors
            else:
                raise IOError("filetype must be .tar or .tar.gz")
        elif ext=='.tar':
            log_cmd('tar xvf "%s" --strip 1 -C "%s"' % (tarf,subj_dir), verbose=verbose, logger=module_logger)  #GRL: may have to add -m option to avoid unable to modify utime errors
        else:
                raise IOError("filetype must be .tar or .tar.gz")
    
    subj_dirs=[]
    for idx,tarf in enumerate(tar_list):
        subj_dir=os.path.join(output_dir,'lvl1_subj%04d' % idx)
        subj_dirs.append(subj_dir)

    subj_dir_file=os.path.join(output_dir,'level1_dir_list.txt')
    f=open(subj_dir_file,'w')
    for d in subj_dirs:
        f.write(d+'\n')
    f.close()        
                        
    if output_tar:
        export_tarfile(output_tar, output_dir,arcnames='All_Level1_Folders')
        #shutil.rmtree(output_dir)  #don't remove the staged files!
    
    level1_dir_list = subj_dirs
    return level1_dir_list
    
def _testme():
    tarfile_list='/home/lee8rx/src_repositories/svn_stuff/cmind-matlab/trunk/lib/Python/lvl2_tarlist_3subj.txt'
    output_dir='/home/lee8rx/smb_shares/rds6/cmind/Processed_Data/Lvl2_Stage_Test'
    output_tar=None
    preserve_subdir_names=False
    verbose=True
    logger=None
    if verbose:
        func=cmind_timer(cmind_stage_second_level,logger=logger)
    else:
        func=cmind_stage_second_level
    func(output_dir,tarfile_list,output_tar=output_tar, 
         preserve_subdir_names=preserve_subdir_names,verbose=verbose, 
         logger=logger)               
    
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer
    
    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Recenter coordinates of .nii.gz volumes to the Center-of-mass of the volume", epilog="")  #-h, --help exist by default
        #example of a positional argument
        
        parser.add_argument("-o","--output_dir",dest='output_dir',required=True, type=str, help="desired directory in which to extract the first level results for further processing")
        parser.add_argument("-list","--tarfile_list", dest='tarfile_list', required=True, type=str, help="list of .tar or .tar.gz files corresponding to first level analyses. If string, this is assumed to be the path to a text file containing one tar file name per line")
        parser.add_argument("-otar","--output_tar", dest='output_tar', type=str, default='none', help="If provided, all first level subjects can be combined into a single .tar.gz")
        parser.add_argument("-psub","--preserve_subdir_names", dest='preserve_subdir_names', type=str, default='none', help="If provided, the subject directory names will match the basename of the input tar files (minus the .tar.gz extension)")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        output_dir=args.output_dir   
        tarfile_list=args.tarfile_list
        output_tar=str2none(args.output_tar, none_strings=['default','none'])
        preserve_subdir_names=str2none(args.preserve_subdir_names, none_strings=['default','none'])
        verbose=args.verbose   
        logger=args.logger   
                
        if verbose:
            cmind_stage_second_level=cmind_timer(cmind_stage_second_level,logger=logger)
    
        cmind_stage_second_level(output_dir,tarfile_list,output_tar=output_tar, preserve_subdir_names=preserve_subdir_names, verbose=verbose, logger=logger)