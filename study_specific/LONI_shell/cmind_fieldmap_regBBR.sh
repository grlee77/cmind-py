#!/bin/bash


usage()
{
echo "Basic usage: cmind_fieldmap_regBBR.py -o <output_dir> -lrvol <lowres_vol> --reg_struct_file <reg_struct_file> -head <T1_vol> -brain <T1_brain_vol> -wm <WMseg_vol>  [options]

usage: cmind_fieldmap_regBBR.py [-h] -o OUTPUT_DIR -lrvol LOWRES_VOL
                                --reg_struct_file REG_STRUCT_FILE -head T1_VOL
                                -brain T1_BRAIN_VOL -wm WMSEG_VOL
                                [--bbrf BBRF] [-fvolp FIELDMAP_VOL_PROCESSED]
                                [-fvolu FIELDMAP_VOL_UNMASKED]
                                [-fmapaff FIELDMAP2STRUCT_AFFINE]
                                [-unw UNWARPDIR] [-epies EPI_ECHO_SPACING]
                                [-sense SENSE_FACTOR] [-gf GENERATE_FIGURES]
                                [-u FORCEUPDATE] [-v] [-log LOGGER]

coregister functional images to a structural image, optionally using a
fieldmap to correct distortions along the phase encoding direction

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -lrvol LOWRES_VOL, --lowres_vol LOWRES_VOL
                        image filename for the "moving" image
  -rsf REG_STRUCT_FILE, --reg_struct_file REG_STRUCT_FILE
                        filename of the existing .csv registration structure


  -t1proc_dir DIRECTORY
  						directory containing structural images
  			OR
  -head T1_VOL, -t1vol T1_VOL, --T1_vol T1_VOL
                        filename of the 'fixed' head image (before brain
                        extraction)
  -brain T1_BRAIN_VOL, -t1bvol T1_BRAIN_VOL, --T1_brain_vol T1_BRAIN_VOL
                        filename of the 'fixed' brain image
  -wm WMSEG_VOL, -wmvol WMSEG_VOL, --WMseg_vol WMSEG_VOL
                        filename of the white matter partial volume estimate
                        image


optional arguments:
  -h, --help            show this help message and exit
  --bbrf BBRF, --bbr_schedule_file BBRF
                        filename of flirt schedule to use (e.g.
                        $FSLDIR/etc/flirtsch/bbr.sch)



  -fieldmap_dir DIRECTORY
  						directory containing the processed fieldmap
				OR
  -fvolp FIELDMAP_VOL_PROCESSED, --Fieldmap_vol_processed FIELDMAP_VOL_PROCESSED
                        filename of the fieldmap volume
  -fvolu FIELDMAP_VOL_UNMASKED, --Fieldmap_vol_unmasked FIELDMAP_VOL_UNMASKED
                        filename of the unmasked fieldmap volume
  -fmapaff FIELDMAP2STRUCT_AFFINE, --fieldmap2struct_affine FIELDMAP2STRUCT_AFFINE
                        filename of the affine transform of the fieldmap to
                        structural space


  -unw UNWARPDIR, --unwarpdir UNWARPDIR
                        {'x','y','z','x-','y-','z-'} phase encoding direction
                        during the readout
  -epies EPI_ECHO_SPACING, --EPI_echo_spacing EPI_ECHO_SPACING
                        EPI echo spacing (seconds)
  -sense SENSE_FACTOR, --SENSE_factor SENSE_FACTOR
                        SENSE acceleration factor
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate overlay images summarizing the
                        registration
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        filename to log to
  -use_fm use_fieldmap
   						if True, use the fieldmap during registration
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi


REQUIRED_ARGS=12
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi


cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh
source $cwd/load_python_env.sh

make_overlay_image="true"
ForceUpdate="true"
bbr_schedule_file=$FSLDIR/etc/flirtsch/bbr.sch
unwarpdir='y'
EPI_echo_spacing='0.00055'
SENSE_factor='2'
use_fieldmap=true

verbose_string="True"
all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
	-np )
	    shift
	    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1;
	    ;;
	-t1proc_dir|--T1proc_dir )
	    shift
	    t1proc_dir=$1
	    ;;
	-fieldmap_dir|--fieldmap_dir )
	    shift
	    fieldmap_dir=$1
	    ;;
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -lrvol|--lowres_vol )
        shift
        lowres_vol=$1
        ;;
    -rsf|--reg_struct_file )
        shift
        reg_struct_file=$1
        ;;
    -head|-t1vol|--T1_vol )
        shift
        T1_vol=$1
        ;;
    -brain|-t1bvol|--T1_brain_vol )
        shift
        T1_brain_vol=$1
        ;;
    -wm|-wmvol|--WMseg_vol )
        shift
        WMseg_vol=$1
        ;;
    --bbrf|--bbr_schedule_file )
        shift
        bbrf=$1
        ;;
    -fvolp|--Fieldmap_vol_processed )
        shift
        Fieldmap_vol_processed=$1
        ;;
    -fvolu|--Fieldmap_vol_unmasked )
        shift
        Fieldmap_vol_unmasked=$1
        ;;
    -fmapaff|--fieldmap2struct_affine )
        shift
        fieldmap2struct_affine=$1
        ;;
    -unw|--unwarpdir )
        shift
        unwarpdir=$1
        ;;
    -epies|--EPI_echo_spacing )
        shift
        EPI_echo_spacing=$1
        ;;
    -sense|--SENSE_factor )
        shift
        SENSE_factor=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
	-use_fm )
	    shift
	    use_fieldmap=$1
	    ;;
    esac
    shift
done


if [ ! -z $verbose_string ]; then
	debug=true
else
	debug=false
fi


if $debug
then
    echo Fieldmap volume processed: $Fieldmap_vol_processed
    echo Fieldmap volume unmasked: $Fieldmap_vol_unmasked
    echo Fieldmap2struct affine: $fieldmap2struct_affine
fi

if [ -z "${output_dir+xxx}" ]
then
    echo "ERROR: need output folder specified via -o flag."
    exit 1
fi

if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi 

if [ ! -f $lowres_vol ]; then
    echo "ERROR: lowres_vol file: $lowres_vol doesn't exist"
    exit 1
fi 
if [ ! -f $reg_struct_file ]; then
    echo "ERROR: reg_struct_file: $reg_struct_file doesn't exist"
    exit 1
fi


if [ ! -z ${t1proc_dir} ]
then
	if [ -d ${t1proc_dir} ]
	then
	    if [ -z "${T1_vol+xxx}" ] || [ ! -f $T1_vol ]
	    then
		T1_vol=${t1proc_dir}/T1W_Structural_N4.nii.gz
	    fi

	    if [ -z "${T1_brain_vol+xxx}" ] || [ ! -f T1_brain_vol ]
	    then
		T1_brain_vol=${t1proc_dir}/T1W_Structural_N4_brain.nii.gz
	    fi

	    if [ -z "${WMseg_vol+xxx}" ] || [ ! -f $WMseg_vol ]
	    then
		WMseg_vol=${t1proc_dir}/T1Segment_WM.nii.gz
	    fi
	fi
fi

if [ ! -f $T1_vol ]; then
    echo "ERROR: T1_vol: $T1_vol doesn't exist"
    exit 1
fi 
if [ ! -f $T1_brain_vol ]; then
    echo "ERROR: T1_brain_vol: $T1_brain_vol doesn't exist"
    exit 1
fi 
if [ ! -f $WMseg_vol ]; then
    echo "ERROR: WMseg_vol file: $WMseg_vols doesn't exist"
    exit 1
fi 


if [ ! -z ${fieldmap_dir} ]
then
	if [ -d ${fieldmap_dir} ] && $use_fieldmap
	then
	    if [ -z "${Fieldmap_vol_processed+xxx}" ] || [ ! -f $Fieldmap_vol_processed ]
	    then
		Fieldmap_vol_processed=${fieldmap_dir}/Fieldmap_medianfilt.nii.gz
	    fi

	    if [ -z "${Fieldmap_vol_unmasked+xxx}" ] || [ ! -f $Fieldmap_vol_unmasked ]
	    then
		Fieldmap_vol_unmasked=${fieldmap_dir}/fieldmaprads_unmasked.nii.gz
	    fi
	    
	    if [ -z "${fieldmap2struct_affine+xxx}" ] || [ ! -f $fieldmap2struct_affine ]
	    then
		fieldmap2struct_affine=${fieldmap_dir}/fieldmap2struct.mat
	    fi
	else
	    echo "not using fieldmap information."
	    use_fieldmap=false
	fi
fi

if [ ! -f $Fieldmap_vol_processed ]; then
    echo "Fieldmap not found, proceeding without fieldmap"
    use_fieldmap=false
fi 

if $use_fieldmap; then
    if [ ! -f $Fieldmap_vol_unmasked ]; then
	echo "ERROR: Fieldmap_vol_unmasked file: $Fieldmap_vol_unmasked doesn't exist"
	exit 1
    fi 
    if [ ! -f $fieldmap2struct_affine ]; then
	echo "ERROR: fieldmap2struct_affine file: $fieldmap2struct_affine doesn't exist"
	exit 1
    fi 
    
    fieldmap_args="--Fieldmap_vol_processed $Fieldmap_vol_processed --Fieldmap_vol_unmasked $Fieldmap_vol_unmasked --fieldmap2struct_affine $fieldmap2struct_affine --unwarpdir $unwarpdir --EPI_echo_spacing $EPI_echo_spacing --SENSE_factor $SENSE_factor"
else
    fieldmap_args=""
fi


if ${debug}
then
    echo output dir: $output_dir
    echo fieldmap used: $use_fieldmap
fi




#existence checks for all required conditional arguments
if [ -z "$output_dir" ]; then
   echo "missing required argument: output_dir"
fi
if [ -z "$lowres_vol" ]; then
   echo "missing required argument: lowres_vol"
fi
if [ -z "$reg_struct_file" ]; then
   echo "missing required argument: reg_struct_file"
fi
if [ -z "$T1_vol" ]; then
   echo "missing required argument: T1_vol"
fi
if [ -z "$T1_brain_vol" ]; then
   echo "missing required argument: T1_brain_vol"
fi
if [ -z "$WMseg_vol" ]; then
   echo "missing required argument: WMseg_vol"
fi

use_bbr="true"
if [[ `basename $lowres_vol` == "BOLD_S"* ]]; then
    use_bbr="false"
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir lowres_vol reg_struct_file T1_vol T1_brain_vol WMseg_vol use_bbr bbrf Fieldmap_vol_processed Fieldmap_vol_unmasked fieldmap2struct_affine unwarpdir EPI_echo_spacing SENSE_factor generate_figures ForceUpdate logger  )


#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_fieldmap_regBBR.py ${all_optional_args} -v"
python ${cmind_python_dir}/cmind_fieldmap_regBBR.py ${all_optional_args} -v
printf 'cmind_fieldmap_regBBR.py:  Elapsed time: %s\n' $(timer $t)



# t=$(timer)

# if ${debug}
# then
#     python ${cmind_python_dir}/cmind_fieldmap_regBBR.py ${output_dir} ${lowres_vol} ${reg_struct_file} ${T1_vol} ${T1_brain_vol} ${WMseg_vol} --bbr_schedule_file ${bbr_schedule_file} --generate_figures ${generate_figures} --ForceUpdate $ForceUpdate $fieldmap_args --verbose
# else
#     python ${cmind_python_dir}/cmind_fieldmap_regBBR.py ${output_dir} ${lowres_vol} ${reg_struct_file} ${T1_vol} ${T1_brain_vol} ${WMseg_vol} --bbr_schedule_file ${bbr_schedule_file} --generate_figures ${generate_figures} --ForceUpdate $ForceUpdate $fieldmap_args
    
# fi    

# printf 'cmind_fieldmap_regBBR:  Elapsed time: %s\n' $(timer $t)

#check that lowres2highres.nii.gz exists

LRtoHR_Affine=$output_dir/lowres2highres.mat 
HRtoLR_Affine=$output_dir/lowres2highres_inv.mat 
if $use_fieldmap; then 
    LRtoHR_Warp=$output_dir/lowres2highres_warp.nii.gz
    HRtoLR_Warp=$output_dir/lowres2highres_warp_inv.nii.gz
    if [ ! -f $LRtoHR_Affine ]; then
    	echo "ERROR: Failed to finish BBR registration"
    	exit 1
    fi
else
    LRtoHR_Warp="None"
    HRtoLR_Warp="None"
    if [ ! -f $LRtoHR_Affine ]; then
    	echo "ERROR: Failed to finish BBR registration"
    	exit 1
    fi
fi

if ${debug} 
then
    echo "LRtoHR_Affine:$LRtoHR_Affine"
    echo "LRtoHR_Warp:$LRtoHR_Warp"
    echo "HRtoLR_Affine:$HRtoLR_Affine"
    echo "HRtoLR_Warp:$HRtoLR_Warp"
    echo "Finished cmind_fieldmap_regBBR"
fi

