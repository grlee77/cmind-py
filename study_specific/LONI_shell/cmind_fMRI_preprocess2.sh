#!/bin/bash
echo "CMD: $*"

usage()
{
echo "Basic usage: cmind_fMRI_preprocess2.py <output_dir> <func_file>  [options]

usage: cmind_fMRI_preprocess2.py [-h] [--feat_params FEAT_PARAMS] [--TR TR]
                                 [--smooth SMOOTH] [--paradigm_hp PARADIGM_HP]
                                 [--brain_thresh BRAIN_THRESH]
                                 [--perfusion_subtract PERFUSION_SUBTRACT]
                                 [--preprocess_case PREPROCESS_CASE]
                                 [--slice_timing_file USAN_VOL]
                                 [--usan_vol SLICE_TIMING_FILE]
                                 [--output_tar OUTPUT_TAR]
                                 [-gf GENERATE_FIGURES] [-u FORCEUPDATE] [-v]
                                 [-log LOGGER]
                                 output_dir func_file

FMRI Preprocessing: Stage 2

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -i FUNC_FILE, --func_file FUNC_FILE
                       filename of the 4D NIFTI/NIFTI-GZ fMRI timeseries

optional arguments:
  -h, --help            show this help message and exit
  --feat_params FEAT_PARAMS
                        filename of a text file containing parameters of the
                        FEAT preprocessing
  --TR TR               repetition time, TR (seconds). If not specified will
                        be read from feat_params
  --smooth SMOOTH       spatial smoothing (mm). If not specified will be read
                        from feat_params
  --paradigm_hp PARADIGM_HP
                        high pass filter cutoff (seconds). If not specified
                        will be read from feat_params
  --brain_thresh BRAIN_THRESH
                        brain threshold (percent). If not specified will be
                        read from feat_params
  --perfusion_subtract PERFUSION_SUBTRACT
                        if True, perform ASL surround subtraction. If not
                        specified will be read from feat_params
  --preprocess_case PREPROCESS_CASE
                        [Feat_BOLD_Sentences,Feat_BOLD_Stories,Feat_ASL_Senten
                        ces,Feat_ASL_Stories]; may be used in future to apply
                        different processing to different cases
  --slice_timing_file SLICE_TIMING_FILE
                        slice timing file
  --usan_vol USAN_VOL
                        alternate usan_vol for susan smooth
  --output_tar OUTPUT_TAR
                        compress the feat preprocessing folder into a .tar.gz
                        archive
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=4
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

######################################################
###  Begin site-specific module load commands here ###


cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh


ForceUpdate=true
generate_figures=true
output_dir="default"
feat_params="default"
slice_timing_file="default"
output_tar=true

###  End site-specific module load commands here   ###
######################################################


verbose_string=""
all_optional_args=""
usan_vol="none"
while [ "$1" != "" ]; do
  case $1 in
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -i|--func_file )
        shift
        func_file=$1
        ;;
    -params|--feat_params )
        shift
        feat_params=$1
        ;;
    --TR )
        shift
        TR=$1
        ;;
    --smooth )
        shift
        smooth=$1
        ;;
    --paradigm_hp )
        shift
        paradigm_hp=$1
        ;;
    --brain_thresh )
        shift
        brain_thresh=$1
        ;;
    --perfusion_subtract )
        shift
        perfusion_subtract=$1
        ;;
    --preprocess_case )
        shift
        preprocess_case=$1
        ;;
    --slice_timing_file )
        shift
        slice_timing_file=$1
        ;;
    --usan_vol )
        shift
        usan_vol=$1
        ;;
    --output_tar )
        shift
        output_tar=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done


if [ ! -z $verbose_string ]; then
  debug=true
else
  debug=false
fi


#existence checks for all required conditional arguments

######################################################
###  Begin custom argument processing here         ###

#ASLBOLD_template_path="/scratch/LONI/Greg/cmind-matlab/trunk/ASLBOLD"

if [ "$output_dir" == "default" ]; then
	output_dir=`dirname $func_file`
	#output_dir=$output_dir/ASLBOLD_$preprocess_case
fi

fname=`basename $func_file` 
fname=${fname:0:4}

fp_ext=.csv
if [ "$feat_params" == "default" ]; then	
    feat_params=${CMIND_ASLBOLD_DIR}feat_params.csv
fi

if $debug
then
 	echo "output_dir=$output_dir"
  echo "preprocess_case=$preprocess_case"
	echo "func_file=$func_file"
  echo "feat_params=$feat_params"
  echo "generate_figures=$generate_figures"
  echo "ForceUpdate=$ForceUpdate"
fi

if [ ! -f $feat_params ]; then
    echo "ERROR: feat_params file: $feat_params doesn't exist"
    exit 1
fi 

if [ ! -f $func_file ] && [ "$func_file" != "None" ]; then
    echo "ERROR: func_file file: $func_file doesn't exist"
    exit 1
fi 

if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi



###  End custom argument processing here           ###
######################################################

#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir func_file feat_params TR smooth paradigm_hp brain_thresh perfusion_subtract preprocess_case slice_timing_file usan_vol output_tar generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_fMRI_preprocess2.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_fMRI_preprocess2.py ${verbose_string} ${all_optional_args}
printf 'cmind_fMRI_preprocess2.py:  Elapsed time: %s\n' $(timer $t)


####### Copies the average absolute and relative motion values to Feat_Dir ########
path=`dirname $func_file`
if [[ `basename $func_file` == ASL* ]]; then
    cp $path/ASL*_mean.rms $output_dir/.
fi

if [[ `basename $func_file` == BOLD* ]]; then
    cp $path/BOLD*_mean.rms $output_dir/.
fi

#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    filtered_func_data
#    noise_est_file
#    preproc_tarfile
#    preproc_options_file

#########################################################
###  Custom post-processing output after this point   ###

#check that outputs exist

if [ "$func_file" != "None" ]; then
  output_file_test=${output_dir}/filtered_func_data.nii.gz
  # noise_est_file=${output_dir}/noise_est.txt
  # #feat_params_out=${output_dir}/cmind_fMRI_preprocess2_options.csv
  echo "$output_file_test"
  if [ ! -f $output_file_test ]; then
      echo "ERROR: Failed to finish fMRI pre-processing"
      exit 1
  fi 
fi
#echo "Filtered_func_data:${output_file_test}"
#echo "noise_est_file:${noise_est_file}"
#echo "feat_params_out:${feat_params_out}"
#echo "Finished cmind_fMRI_preprocess2"

