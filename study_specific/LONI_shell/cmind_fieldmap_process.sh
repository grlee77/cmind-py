#!/bin/bash
echo "CMD: $*"

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd


usage()
{
echo "Basic usage: cmind_fieldmap_process.py -o <output_dir> -rads <fieldmap_nii_rads> -mag <fieldmap_nii_mag> -head <T1_head_vol> -brain <T1_brain_vol>  [options]

usage: cmind_fieldmap_process.py [-h] -o OUTPUT_DIR -rads FIELDMAP_NII_RADS
                                 -mag FIELDMAP_NII_MAG -head T1_HEAD_VOL
                                 -brain T1_BRAIN_VOL [-dte DELTATE]
                                 [-unw UNWARPDIR] [-u FORCEUPDATE] [-v]
                                 [-log LOGGER]

coregister functional images to a structural image, optionally using a
fieldmap to correct distortions along the phase encoding direction

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -rads FIELDMAP_NII_RADS, --fieldmap_nii_rads FIELDMAP_NII_RADS
                        fieldmap volume in rad/s
  -mag FIELDMAP_NII_MAG, --fieldmap_nii_mag FIELDMAP_NII_MAG
                        magnitude image corresponding to 'fieldmap_nii_rads'


  -t1proc_dir DIRECTORY
  						directory with T1 structural head and brain
  		OR
  -head T1_HEAD_VOL, --T1_head_vol T1_HEAD_VOL
                        filename of the 'fixed' head image (before brain
                        extraction)
  -brain T1_BRAIN_VOL, --T1_brain_vol T1_BRAIN_VOL
                        filename of the 'fixed' brain image


optional arguments:
  -h, --help            show this help message and exit
  -dte DELTATE, --deltaTE DELTATE
                        echo time difference used to generate
                        'fieldmap_nii_rads'
  -unw UNWARPDIR, --unwarpdir UNWARPDIR
                        {'x','y','z','x-','y-','z-'}, optional phase encoding
                        direction during the readout
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
"
}


if [ $# == 0 ]; then usage;  exit 1; fi
if [ "$1" == "-h" ]; then usage; exit 1; fi
if [ "$1" == "--help" ]; then usage; exit 1; fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi


source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

REQUIRED_ARGS=4
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi


ForceUpdate=true
unwarpdir=Y
deltaTE=0.0023
output_dir="default"
fieldmap_nii_rads="None"
fieldmap_nii_mag="None"

all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
	-t1proc_dir )
	    shift
	    T1proc_dir=$1
	    ;;
	-np )
	    shift
	    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1;
	    ;;
	-u )
	    shift
	    ForceUpdate=$1
	    ;;	
    -rads|--fieldmap_nii_rads )
        shift
        fieldmap_nii_rads=$1
        ;;
    -mag|--fieldmap_nii_mag )
        shift
        fieldmap_nii_mag=$1
        ;;
    -t1|-head|--T1_head_vol )
        shift
        T1_head_vol=$1
        ;;
    -t1brain|-brain|--T1_brain_vol )
        shift
        T1_brain_vol=$1
        ;;
    -dte|--deltaTE )
        shift
        deltaTE=$1
        ;;
    -unw|--unwarpdir )
        shift
        unwarpdir=$1
        ;;
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done

if [ ! -z $verbose_string ]; then
	debug=true
else
	debug=false
fi


if [ "${fieldmap_nii_rads,,}" == "none" ] || [ "${fieldmap_nii_mag,,}" == "none" ];then
    echo "Not using GRE fieldmaps"
    echo "Fieldmap_vol_processed:None"
    echo "Fieldmap_vol_unmasked:None"
    echo "Fieldmap2struct:None"
    exit 0
fi

if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi 
if [ ! -f $fieldmap_nii_rads ]; then
    echo "ERROR: fieldmap_nii_rads file: $fieldmap_nii_rads doesn't exist"
    exit 1
fi 
if [ ! -f $fieldmap_nii_mag ]; then
    echo "ERROR: fieldmap_nii_mag: $fieldmap_nii_mag doesn't exist"
    exit 1
fi

if [ ! -z ${T1proc_dir} ]
then
	if [ -d ${T1proc_dir} ]
	then
	    if [ -z "${T1_head_vol+xxx}" ] || [ ! -f $T1_head_vol ]
	    then
		T1_head_vol=${T1proc_dir}/T1W_Structural_N4.nii.gz
	    fi

	    if [ -z "${T1_brain_vol+xxx}" ] || [ ! -f $T1_brain_vol ]
	    then
		T1_brain_vol=${T1proc_dir}/T1W_Structural_N4_brain.nii.gz
	    fi
	fi
fi

if [ ! -f $T1_head_vol ]; then
    echo "ERROR: T1_head_vol: $T1_head_vol doesn't exist"
    exit 1
fi

if [ ! -f $T1_brain_vol ]; then
    echo "ERROR: T1_brain_vol: $T1_brain_vol doesn't exist"
    exit 1
fi 



#existence checks for all required conditional arguments
if [ -z "$output_dir" ]; then
   echo "missing required argument: output_dir"
fi
if [ -z "$fieldmap_nii_rads" ]; then
   echo "missing required argument: fieldmap_nii_rads"
fi
if [ -z "$fieldmap_nii_mag" ]; then
   echo "missing required argument: fieldmap_nii_mag"
fi
if [ -z "$T1_head_vol" ]; then
   echo "missing required argument: T1_head_vol"
fi
if [ -z "$T1_brain_vol" ]; then
   echo "missing required argument: T1_brain_vol"
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir fieldmap_nii_rads fieldmap_nii_mag T1_head_vol T1_brain_vol deltaTE unwarpdir ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_fieldmap_process.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_fieldmap_process.py ${verbose_string} ${all_optional_args}
printf 'cmind_fieldmap_process.py:  Elapsed time: %s\n' $(timer $t)

#The following options should be printed by the python function above:
#    fieldmap_vol
#    fieldmap_unmasked
#    fieldmap2struct_affine


# t=$(timer)

# if $debug
# then
#     python ${cmind_python_dir}/cmind_fieldmap_process.py ${output_dir} ${fieldmap_nii_rads} ${fieldmap_nii_mag} ${T1_head_vol} ${T1_brain_vol} --deltaTE ${deltaTE} --unwarpdir ${unwarpdir} --ForceUpdate $ForceUpdate --verbose
# else
#     python ${cmind_python_dir}/cmind_fieldmap_process.py ${output_dir} ${fieldmap_nii_rads} ${fieldmap_nii_mag} ${T1_head_vol} ${T1_brain_vol} --deltaTE ${deltaTE} --unwarpdir ${unwarpdir} --ForceUpdate $ForceUpdate
# fi

# printf 'cmind_fieldmap_process:  Elapsed time: %s\n' $(timer $t)

output_file_test=$output_dir/Fieldmap_medianfilt.nii.gz
output_file_unmasked=$output_dir/fieldmaprads_unmasked.nii.gz
file_struct_aff=$output_dir/fieldmap2struct.mat

if [ ! -f $output_file_test ] || [ ! -f $output_file_unmasked ] || [ ! -f $file_struct_aff ]; then
   	echo "ERROR: Failed to finish fieldmap processing"
   	exit 1
fi

# if $debug
# then
#     echo "Fieldmap_vol_processed:$output_file_test"
#     echo "Fieldmap_vol_unmasked:$output_file_unmasked"
#     echo "Fieldmap2struct:$file_struct_aff"
# fi

# echo "Finished cmind_fieldmap_process"

