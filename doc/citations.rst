.. _citations:

Citations
=========

.. FLIRT
.. [Jenkinson2001] M. Jenkinson and S.M. Smith. A global optimisation method for robust affine registration of brain images. Medical Image Analysis, 5(2):143-156, 2001.
.. [Jenkinson2002] Jenkinson, M., Bannister, P., Brady, J. M. and Smith, S. M. Improved Optimisation for the Robust and Accurate Linear Registration and Motion Correction of Brain Images. NeuroImage, 17(2), 825-841, 2002. 
.. [Greve2009] Greve, D.N. (2009), Accurate and robust brain image alignment using boundary-based registration,  NeuroImage, vol. 48, no. 1, pp. 63-72.

.. FAST
.. [Zhang2001] Y. Zhang, M. Brady, and S. Smith.  Segmentation of brain MR images through a hidden Markov random field model and the expectation maximization algorithm. IEEE Trans. on Medical Imaging, 20(1):45-57, 2001. 

.. FSL
.. [Smith2004] Smith. S.M. (2004), Advances in functional and structural MR image analysis and implementation as FSL , NeuroImage, vol. 23, no. S1, pp. 208-19.
.. [Jenkinson2012] M. Jenkinson, C.F. Beckmann, T.E.J. Behrens, M.W. Woolrich, and S.M. Smith. FSL. NeuroImage, 62:782-790, 2012. 

.. FUGUE/PRELUDE
.. [Jenkinson2001b] M. Jenkinson. Improved Unwarping of EPI Volumes using Regularised B0 Maps.  Seventh Int Conf on Functional Mapping of the Human Brain; 2001
.. [Jenkinson2003] M. Jenkinson. A fast, automated, n-dimensional phase unwrapping algorithm. Magnetic Resonance in Medicine, 49(1):193-197, 2003. 
.. [Jenkinson2004] M. Jenkinson. Improving the Registration of B0-distorted EPI Images using Calculated Cost Function Weights.  Tenth Int Conf on Functional Mapping of the Human Brain; 2004;

.. BET
.. [Smith2002] S.M. Smith.  Fast robust automated brain extraction.  Human Brain Mapping, 17(3):143-155, November 2002. 
.. [Pechaud2005] M. Pechaud, M. Jenkinson, S. Smith.  BET2 - MRI-Based Estimation of Brain, Skull and Scalp Surfaces. FMRIB Technical Report TR06MP1
.. [Jenkinson2005] M. Jenkinson, M. Pechaud, and S. Smith. BET2: MR-based estimation of brain, skull and scalp surfaces.  In Eleventh Annual Meeting of the Organization for Human Brain Mapping, 2005.

.. FILM
.. [Woolrich2001] M.W. Woolrich, B.D. Ripley, J.M. Brady, and S.M. Smith.  Temporal autocorrelation in univariate linear modelling of FMRI data.  NeuroImage, 14(6):1370-1386, 2001. 

.. FLAME
.. [Beckmann2003] C.F. Beckmann, M. Jenkinson, and S.M. Smith.  General multi-level linear modelling for group analysis in FMRI.  NeuroImage, 20:1052-1063, 2003. 
.. [Woolrich2008] M. Woolrich.  Robust group analysis using outlier inference.  NeuroImage, 41(2):286-301, 2008. 
.. [Woolrich2004] M.W. Woolrich, T.E.J. Behrens, C.F. Beckmann, M. Jenkinson, and S.M. Smith. Multi-level linear modelling for FMRI group analysis using Bayesian inference. NeuroImage, 21(4):1732-1747, 2004.

.. MELODIC
.. [Beckmann2004] C.F. Beckmann and S.M. Smith.  Probabilistic independent component analysis for functional magnetic resonance imaging.  IEEE Trans. on Medical Imaging, 23(2):137-152, 2004. 
.. [Beckmann2005a] C.F. Beckmann and S.M. Smith.  Tensorial extensions of independent component analysis for multisubject FMRI analysis.  NeuroImage, 25(1):294-311, 2005. 
.. [Beckmann2005b] C.F. Beckmann, M. De Luca, J.T. Devlin, and S.M. Smith.  Investigations into resting-state connectivity using independent component analysis.  Philosophical Transactions of the Royal Society, 360(1457):1001-1013, 2005. 

.. SUSAN
.. [Smith1997] S.M. Smith and J.M. Brady.  SUSAN - a new approach to low level image processing.  International Journal of Computer Vision, 23(1):45-78, May 1997. 

.. ANTs
.. [Avants2008] Avants, B.B. (2008), 'Symmetric diffeomorphic image registration with cross-correlation: evaluating automated labeling of elderly and neurodegenerative brain',  Medical Image Analysis, vol. 12, no. 1, pp. 26-41.

.. ASL GLM
.. [Mumford2006] Mumford JA, Hernandez-Garcia LH, Lee GR, Nichols TE.  Estimation efficiency and statistical power in arterial spin labeling fMRI.  NeuroImage 2006;33:103-114.

.. Others
.. [Smith2007] S.M. Smith, M. Jenkinson, C.F. Beckmann, K.L. Miller, and M. Woolrich.  Meaningful design and contrast estimability in FMRI. NeuroImage, 34(1):127-136, 2007. 
.. [Power2012] Power, J.D. (2012),  'Spurious but systematic correlations in functional connectivity MRI networks arise from subject motion',  NeuroImage, vol. 59, no. 3, pp. 2142-54.
.. [Rex2003] Rex, D.E. (2003), 'The LONI Pipeline Processing Environment', NeuroImage, vol. 19, no. 3, pp. 1033-48.
.. [Gorgolewski2011] Gorgolewski, K. (2011), 'Nipype: a flexible, lightweight and extensible neuroimaging data processing framework in Python',   Frontiers in Neuroinformatics, vol. 5, article 13.
.. [Schmithorst2014] Schmithorst, V.J. (2014), 'Optimized simultaneous ASL and BOLD functional imaging of the whole brain', Journal of Magnetic Resonance Imaging, In Press (doi: 10.1002/jmri.24273).
.. [CMIND_Contract] The Pediatric Functional Neuroimaging Research Network, NICHD HHSN275200900018C.
.. [Wang2002] Wang, JJ. et. al. (2002), Comparison of Quantitative Perfusion Imaging Using Arterial Spin Labeling at 1.5 and 4.0 Tesla.  Magnetic Resonance in Medicine 48:242-254
 