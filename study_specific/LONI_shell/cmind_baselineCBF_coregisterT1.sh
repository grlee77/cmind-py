#!/bin/bash
echo "CMD: $*"

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd


usage()
{
echo "Basic usage: cmind_baselineCBF_coregisterT1.py -o <output_dir> -cbf <CBF_nii> -T1concat <T1concat_N4_brain_nii> -t1map <T1_map_nii> -M0map <I0_map_nii>  [options]
																OR
				   cmind_baselineCBF_coregisterT1.py -o <output_dir> -CBF_dir <CBF_dir> -T1map_dir <T1map_dir> [options]

usage: cmind_baselineCBF_coregisterT1.py [-h] -o OUTPUT_DIR -cbf CBF_NII
                                         -T1concat T1CONCAT_N4_BRAIN_NII
                                         -t1map T1_MAP_NII -M0map I0_MAP_NII
                                         [-nrmse_map T1_NRMSE_MAP_NII]
                                         [-gf GENERATE_FIGURES]
                                         [-u FORCEUPDATE] [-v] [-log LOGGER]

coregister T1map images to CBF

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output

  -CBF_dir DIRECTORY
  						directory containing CBF data
  			OR                        
  -cbf CBF_NII, --CBF_nii CBF_NII
                        filename of Baseline CBF volume to register to


  -T1map_dir DIRECTORY
  						directory containing T1map results
  			OR
  -T1concat T1CONCAT_N4_BRAIN_NII, --T1concat_N4_brain_nii T1CONCAT_N4_BRAIN_NII
                        filename containing the brain extracted 4D TI image
                        series used during T1 fitting
  -t1map T1_MAP_NII, --T1_map_nii T1_MAP_NII
                        filename containing the T1_map volume to register
  -M0map I0_MAP_NII, --I0_map_nii I0_MAP_NII
                        filename containing the I0_map (M0) volume to register

optional arguments:
  -h, --help            show this help message and exit
  -nrmse_map T1_NRMSE_MAP_NII, --T1_NRMSE_map_nii T1_NRMSE_MAP_NII
                        filename containing the T1 fit NRMSE_map volume to
                        register
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=4
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

ForceUpdate=true
all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
	-CBF_dir )
	    shift
	    CBF_dir=$1
	    ;;
	-t1map_dir )
	    shift
	    T1map_dir=$1
	    ;;
	-o|--output_dir )
        shift
        output_dir=$1
        ;;
    -cbf|--CBF_nii )
        shift
        CBF_nii=$1
        ;;
    -T1concat|--T1concat_N4_brain_nii )
        shift
        T1concat_N4_brain_nii=$1
        ;;
    -t1map|--T1_map_nii )
        shift
        T1_map_nii=$1
        ;;
    -M0map|--I0_map_nii )
        shift
        I0_map_nii=$1
        ;;
    -nrmse_map|--T1_NRMSE_map_nii )
        shift
        T1_NRMSE_map_nii=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done


if [ ! -z $verbose_string ]; then
  debug=true
else
  debug=false
fi


if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi 



if [ ! -z ${CBF_dir} ]
then
	if [ -d ${CBF_dir} ]
	then
	    if [ -z "${CBF_nii+xxx}" ] || [ ! -f $CBF_nii ]
	    then
		CBF_nii=${CBF_dir}/BaselineCBF_control_mcf_mean_N4_brain.nii.gz
		echo CBF_nii: ${CBF_nii}
	    fi
	fi
fi

if [ ! -f $CBF_nii ]; then
    echo "ERROR: CBF_nii file: $CBF_nii doesn't exist"
    exit 1
fi 

if [ ! -z ${T1map_dir} ]
then
	if [ -d ${T1map_dir} ]
	then
	    if [ -z "${T1concat_N4_brain_nii+xxx}" ] || [ ! -f $T1concat_N4_brain_nii ]
	    then
		T1concat_N4_brain_nii=${T1map_dir}/T1concat_mcf.nii.gz
	    fi
	    
	    if [ -z "${T1_map_nii+xxx}" ] || [ ! -f $T1_map_nii ]
	    then
		T1_map_nii=${T1map_dir}/T1_map.nii.gz
	    fi
	    
	     if [ -z "${T1_NRMSE_map+xxx}" ] || [ ! -f $T1_NRMSE_map ]
	    then
		T1_NRMSE_map=${T1map_dir}/T1_NRMSE_map.nii.gz
	    fi
	    
	    if [ -z "${I0_map_nii+xxx}" ] || [ ! -f $I0_map_nii ]
	    then
		I0_map_nii=${T1map_dir}/I0_map.nii.gz
	    fi
	fi
fi

if [ ! -f $T1concat_N4_brain_nii ]; then
    echo "ERROR: T1concat_N4_brain_nii file: $T1concat_N4_brain_nii doesn't exist"
    exit 1
fi 
if [ ! -f $T1concat_N4_brain_nii ]; then
    echo "ERROR: T1concat_N4_brain_nii file: $T1concat_N4_brain_nii doesn't exist"
    exit 1
fi 
if [ ! -f $T1_map_nii ]; then
    echo "ERROR: T1_map_nii file: $T1_map_nii doesn't exist"
    exit 1
fi 
if [ ! -f $I0_map_nii ]; then
    echo "ERROR: I0_map_nii file: $I0_map_nii doesn't exist"
    exit 1
fi 

if $debug 
then
    echo CBF_N4_BRAIN: ${CBF_N4_brain_nii}
    echo OTPUT_DIR: ${output_dir}
    echo T1CONCAT_N4: ${T1concat_N4_brain_nii}
    echo FORCE: ${Forceupdate}
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir CBF_nii T1concat_N4_brain_nii T1_map_nii I0_map_nii T1_NRMSE_map_nii generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_baselineCBF_coregisterT1.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_baselineCBF_coregisterT1.py ${verbose_string} ${all_optional_args}
printf 'cmind_baselineCBF_coregisterT1.py:  Elapsed time: %s\n' $(timer $t)
#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    T1map_reg2CBF
#    I0map_reg2CBF
#    T1map2CBF_affine

# t=$(timer)

output_file_test=${output_dir}/T1_map_reg2CBF.nii.gz
T1map2CBF_affine=${output_dir}/flirt_T1concat2CBF.mat

# if $debug  #TODO: generate_figures is missing here
# then
#     python ${cmind_python_dir}/cmind_baselineCBF_coregisterT1.py -o ${output_dir} ${CBF_N4_brain_nii} ${T1concat_N4_brain_nii} $T1_map_nii $I0_map_nii --T1_NRMSE_map_nii ${NRMSE_map} --ForceUpdate ${ForceUpdate} --verbose
# else
#     python ${cmind_python_dir}/cmind_baselineCBF_coregisterT1.py -o ${output_dir} ${CBF_N4_brain_nii} ${T1concat_N4_brain_nii} $T1_map_nii $I0_map_nii --T1_NRMSE_map_nii ${NRMSE_map} --ForceUpdate ${ForceUpdate}
# fi

# printf 'cmind_baselineCBF_coregisterT1:  Elapsed time: %s\n' $(timer $t)

#check that BaselineCBF_mcf_percent_change.nii.gz exists
if [ ! -f $output_file_test ] || [ ! -f $T1map2CBF_affine ]; then
    echo "ERROR: Failed to finish BaselineCBF CoregisterT1"
    exit 1
fi 

# if $debug
# then
#     echo "T1map_reg2CBF:$output_file_test"     
#     echo "T1map2CBF_affine:$T1map2CBF_affine"
# fi

echo "Finished cmind_baselineCBF_coregisterT1"

