#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

"""
The following constants are those found in FSL's asl_calib and appear to be for
3T.

# constants
T1csf=4.3
T2csf=750
T1gm=1.3
T2gm=100
T1wm=1.0
T2wm=50

T2b=150

# partition coeffs
# based on Herscovitch and Raichle 1985
pccsf=1.15 # a blood water density of 0.87
pcwm=0.82
pcgm=0.98

if [ -z $t2star ]; then
# we need to correct for T2* not T2 so change the defaults
# NB these will still be overridden by specific values supplied
    T2csf=400
    T2gm=60 # from Foucher 2011 JMRI 34:785-790
    T2wm=50 # ditto

    T2b=50 #from Petersen 2006 MRM 55(2):219-232 see discussion
fi

"""

def calcCBF_Wang2002(deltaM,maskim,cbf_lambda,R1a,R1app,delta,delta_art,w,t,TR,alpha,M0_blood):
    """
    Parameters
    ----------
    deltaM : ndarray
        average (control-tag) ASL subtraction image
    maskim : ndarray
        boolean mask over which to perform the computation
    cbf_lambda : float, optional
        blood-brain partition coefficient
    R1a : float
        R1 relaxation rate of arterial blood (1/seconds)
    R1app : float
        apparent R1 relaxation rate (1/seconds)
    delta : float
        transit time (to capillary compartment) (seconds)
    delta_art : float
        arterial transit time (seconds)
    w : float
        post-labeling delay (seconds)
    t : float
        label duration (seconds) 
    TR : float
        TR (seconds)
    alpha : float
        inversion efficiency
    M0_blood : float
        M0 of blood (usually estimated from WM or CSF)

    Notes
    -----
    Quantify CBF using the model of Wang et. al.
    
    .. math:: \Delta M_{t} = \\frac{2 \\alpha f M^{0}_b T_{1app}}{\lambda} \; \exp \left( \\frac{-\Delta t}{T_{1a}} \\right) \; \left[ \exp \left(\\frac{min((\Delta t - w),0)}{T_{1app}} \\right) - \exp \left(\\frac{min((\Delta t - \\tau - w),0)}{T_{1app}} \\right) \\right]
 
    """
    
    import numpy as np

    if not isinstance(w,(float,int)):
        if np.prod(w.shape)>1:
            w=w[maskim]
    
    deltaM=deltaM[maskim];
    #deltaM = BaselineCBF.avgdiff(maskim);
    #M0_guess=mean(BaselineCBF.control,4);
    #M0_guess=M0_guess(maskim);
    
    fac1=(-2*alpha)/(cbf_lambda*R1app)*np.exp(-delta*R1a)
    fac1=fac1.flatten()
    term1a = np.exp(R1app*min(delta-w,0));
    term1b = np.exp(R1app*(delta-t-w));
    term1 = fac1*(term1a-term1b);
    
    fac2 = (-2*alpha)/(cbf_lambda*R1a)
    #term2a = exp(-R1a*w)
    term2a = np.exp(R1a*(min(delta_art-w,0)-delta_art))
    #term2b = np.exp(-R1a*delta)*np.exp(R1a*min(0,-w+delta))
    term2b = np.exp(R1a*(min(delta-w,0)-delta))
    term2 = fac2 * (term2a-term2b)
    
    # find baseline signal intensity and difference, correcting for T1 effects
    fac = -1/(term1+term2)
    CBF_init = fac*deltaM/M0_blood
    
    CBFmask = (CBF_init>0) & (CBF_init<.1)  #avoid extreme values: .1 = 600 ml/100g/ms
    CBF_init[~CBFmask]=0;
    
    CBF_cor=np.zeros(maskim.shape,dtype='float32')
    CBF_cor[maskim]=CBF_init.copy()
    CBF_cor=CBF_cor*6000;
    
    #scale_max=prctile(CBF_cor.flatten(),p=(99))
    #pyplot.figure(),pyplot.imshow(CBF_cor,vmin=0,vmax=scale_max)
    
    return CBF_cor


def calcflash(a,TR,T1,err=0.01):
    """
    
    Parameters
    ----------
    a : float
        flip angle
    TR : float
        TR (ms)
    T1 : float
        T1 (ms)  (should be in same time units as TR)
    err : float
        will calculate number of pulses needed to get within this amount of the steady state
    
    Examples
    --------
    .. plot::
    
       import numpy as np
       import matplotlib.pyplot as plt
       from cmind.pipeline.cmind_baselineCBF_quantitative import calcflash
       T1vals=np.array((20, 40, 80, 160, 1e3),dtype=np.float); 
       angles=np.arange(1,91)
       TR=8.6; 
       Mxy_ss=np.zeros((len(angles),len(T1vals)),dtype=np.float);
       for m,T1 in enumerate(T1vals):
           for n,angle in enumerate(angles):
               c,Mxy_ss[n,m]=calcflash(angle,TR,T1)[0:2]
       plt.figure()
       plt.plot(angles,Mxy_ss)
       plt.legend(T1vals,loc='upper left')
     
    """ 
    #TRvals=np.arange(3,101,dtype=np.float) 
    #duty=(TRvals-2)/TRvals  #assume 2 ms for excitation, spoiling etc... so readout duty cycle is (TR-2)/TR
    #T2=20; 
    #exp_decay=np.zeros((len(TRvals),1),dtype=np.float)
    #for n,TR in enumerate(TRvals):
    #    exp_decay[n]=np.mean(np.exp(-np.linspace(1,TRvals[n]-1,100)/T2))
    #cerrs=np.zeros_like(exp_decay)
    #Mxy_ss=np.zeros_like(exp_decay)
    #a_ernst=np.zeros_like(exp_decay)
    #Mxy_ss_ernst=np.zeros_like(exp_decay)
    #for cnt, TR in enumerate(TRvals):
    #  cerrs,Mxy_ss[cnt],a_ernst[cnt],Mxy_ss_ernst[cnt]=calcflash(30,TR,50,.05)[0:4]
    #  Also take into account:  # of lines sampled per unit time (will effect level of noise-like aliasing artifact)
    
    import numpy as np
    
    a=a/180.0*np.pi
    E1=np.exp(-TR/float(T1));
    cerr=1e6;
    
    n=1;
    cerrs=[]
    while (cerr>err and n<1000):
        cerr=( ((np.cos(a)*E1)**n)*E1*(1-np.cos(a)) )/(1-E1);
        cerrs.append(cerr)
        n+=1
    ndummy=n-1
    
    a_ernst = np.arccos(E1);
    Mxy_ss_ernst = (1-E1)/(1-np.cos(a_ernst)*E1)*np.sin(a_ernst)
    Mxy_ss = (1-E1)/(1-np.cos(a)*E1)*np.sin(a)
    Mz_ss = (1-E1)/(1-np.cos(a)*E1)
    #print "%f" % (100*Mxy_ss/Mxy_ss_ernst)
    a_ernst=a_ernst*180.0/np.pi
    return (cerrs, Mxy_ss, a_ernst,Mxy_ss_ernst, Mz_ss, ndummy)

def cmind_baselineCBF_quantitative(output_dir,BaselineCBF_mcf_masked, N4BiasCBF, BaselineCBF_timepoints_kept, alpha_file, WMpve, T1_map_reg2CBF, alpha_const = 0.75, HRtoLR_affine=None, HRtoLR_warp=None, relmot=None, relmot_thresh=1.5, cbf_lambda=0.9,R1a=0.625,delta=1.5,delta_art=1.3,w=1.4,tau=2.0,TR=4.0, T2star_wm=45.0, T2star_arterial_blood=53.0, TE=11.5, generate_figures=True, ForceUpdate=False,verbose=False,logger=None):
    """Quantify CBF
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    BaselineCBF_mcf_masked : str
        masked, motion-corrected BaselineCBF volume
    N4BiasCBF : str
        BaselineCBF N4Bias field
    BaselineCBF_timepoints_kept : str
        Timepoints kept during BaselineCBF preprocessing
    alpha_file : float or str
        inversion efficiency or a text file containing the inversion efficiency
    WMpve : str
        white matter partial volume estimate
    T1_map_reg2CBF : str or float, optional
        T1map that has been registered to BaselineCBF_mcf_masked
    alpha_const : float, optional
        if alpha_file was specified, also repeat the CBF mapping using this 
        value of alpha instead
    HRtoLR_affine : str or None, optional
        affine from highres (structural) to lowres space
    HRtoLR_warp : str or None, optional
        warp from highres (structural) to lowres space
    relmot : str, optional
        relative motion .rms file from mcflirt
    relmot_thresh : float, optional
        relative motion threshold (mm) for discarding timepoints
    cbf_lambda : float, optional
        blood-brain partition coefficient
    R1a : float, optional
        R1 relaxation rate of arterial blood (1/seconds)
    delta : float, optional
        transit time (to capillary compartment) (seconds)
    delta_art : float, optional
        arterial transit time (seconds)
    w : float, optional
        post-labeling delay (seconds)
    tau : float, optional
        label duration (seconds) 
    TR : float, optional
        TR (seconds)
    T2star_wm : float, optional
        transverse relaxation rate of white matter
    T2star_arterial_blood : float, optional
        transverse relaxation rate of arterial blood
    TE : float, optional
        echo time of the ASL acquisition
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
   
    Returns
    -------
    CBF_Wang_file : str
        filename of the quantitative CBF image using alpha from alpha_file
    CBF_Wang_alphaconst_file : str
        filename of the quantitative CBF image using alpha = alpha_const
    
    Notes
    -----
    
    .. figure::  ../img/IRC04H_06M008_P_1_CBF_Wang2002.png
       :align:   center
       :scale:   100%
       
       Quantitative CBF volume
      
    """
    import os
    from os.path import join as pjoin
    import warnings
    
    import numpy as np
    import nibabel as nib
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, dict2csv, csv2dict
    from cmind.utils.nifti_utils import cmind_save_nii_mod_header
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    from cmind.pipeline.cmind_baselineCBF_quantitative import calcflash, calcCBF_Wang2002

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting BaselineCBF Quantification")        
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir);
    
    if isinstance(alpha_file,float):
        alpha=alpha_file
    elif isinstance(alpha_file,str):
        if os.path.exists(alpha_file):
            alpha=np.loadtxt(alpha_file)
        else:
            try:
                alpha=float(alpha_file) #try converting string to float
            except:
                raise IOError("Missing alpha_file, %s" % (alpha_file))
    else:
        raise ValueError("alpha_file must be of type float or str")
            
    if (not HRtoLR_affine) and (not HRtoLR_warp):
        raise Exception("Either HRtoLR_affine or HRtoLR_warp must be specified")
    if HRtoLR_affine and (not os.path.exists(HRtoLR_affine)):
        raise IOError("Missing HRtoLR_affine, %s" % (HRtoLR_affine))
    if not imexist(WMpve,'nifti-1')[0]:
        raise IOError("Missing WMpve, %s" % (WMpve))
    
    if (not isinstance(T1_map_reg2CBF,(int,float))) and (not imexist(T1_map_reg2CBF,'nifti-1')[0]):
        raise IOError("Missing T1_map_reg2CBF, %s" % (T1_map_reg2CBF))
    if not imexist(BaselineCBF_mcf_masked,'nifti-1')[0]:
        raise IOError("Missing BaselineCBF_mcf_masked, %s" % (BaselineCBF_mcf_masked))
    if not imexist(N4BiasCBF,'nifti-1')[0]:
        raise IOError("Missing N4BiasCBF, %s" % (N4BiasCBF))
    if not os.path.exists(BaselineCBF_timepoints_kept):
        raise IOError("Missing BaselineCBF_timepoints_kept, %s" % (BaselineCBF_timepoints_kept))
    
    #convert string or int inputs to float    
    cbf_lambda=float(cbf_lambda)
    R1a=float(R1a)
    delta=float(delta)
    delta_art=float(delta_art)
    w=float(w)
    tau=float(tau)
    TR=float(TR)
    TE=float(TE)
    T2star_wm=float(T2star_wm)
    T2star_arterial_blood=float(T2star_arterial_blood)
    
    #commented out are some defaults from FSL
    #spin_echo=False
    #if spin_echo:
    #    T2gm=100
    #    T2wm=50
    #    T2a=150 #default for spin-echo
    #else:
    #    T2gm=60 # Foucher2011:  JMRI 34:785-790. 2011
    #    T2wm=50 
    #    T2a=50  #50 is reasonable default for gradient-echo
    #    

          
    #[pth,CBF_root,ext]=fileparts(CBF_nii);
    #CBF_root=strrep(CBF_root,'.nii','');  %if it was .nii.gz, still need to remove the .nii
    
    VERBOSE=False;
    
    if isinstance(T1_map_reg2CBF,(int,float)):
         CBF_file_str = 'CBF_Wang2002_constT1'
    else:
         CBF_file_str = 'CBF_Wang2002'
         
    CBF_Wang_file=pjoin(output_dir,CBF_file_str+'.nii.gz')
    if alpha_const:
        CBF_Wang_alphaconst_file=pjoin(output_dir,CBF_file_str+'_alphaconst.nii.gz') 
    else:
        CBF_Wang_alphaconst_file=''
        
    if (not imexist(CBF_Wang_file)[0]) or ForceUpdate:
      
        if False:  #(exist(pjoin('.','BaselineCBF.mat'),'file') and (not os.path.isdir(pjoin('.','BaselineCBF.mat')))):
            pass
            #load BaselineCBF BaselineCBF
        else:
            BaselineCBF_nii=nib.load(BaselineCBF_mcf_masked);
            try:
                timepoints_keep=np.loadtxt(BaselineCBF_timepoints_kept,dtype='int32');
            except:
                timepoints_keep=np.loadtxt(BaselineCBF_timepoints_kept.txt,dtype='int32',delimiter=',');
                
            BaselineCBF={}
            
            if relmot and os.path.exists(relmot):
                relmot=np.loadtxt(relmot); #pjoin(output_dir,'mc','BaselineCBF_mcf_rel.rms')
                high_motion_pairs=np.sort(np.ceil(np.where(relmot>relmot_thresh)[0]))  #TODO: check that this is correct
                kept_high_motion=np.intersect1d(timepoints_keep,high_motion_pairs)
                for mm in range(kept_high_motion.shape[0]): #also remove motion outliers
                    fidx=np.where(timepoints_keep==kept_high_motion[mm])[0]
                    if np.any(fidx):
                        timepoints_keep=np.delete(timepoints_keep,fidx)
                
                if high_motion_pairs.shape[0]:
                    np.savetxt(pjoin(output_dir,'BaselineCBF_high_motion_pairs.txt'),np.asarray(high_motion_pairs.flatten()))
                else:
                    np.savetxt(pjoin(output_dir,'BaselineCBF_high_motion_pairs.txt'),[0,])
                np.savetxt(pjoin(output_dir,'BaselineCBF_timepoints_kept2.txt'),np.asarray(timepoints_keep))
                np.savetxt(pjoin(output_dir,'BaselineCBF_Npairs_kept2.txt'),[timepoints_keep.shape[0],])
                
            BaselineCBF['img']=BaselineCBF_nii.get_data()
            BaselineCBF['control'] = BaselineCBF['img'][:,:,:,0::2];   
            BaselineCBF['control'] = BaselineCBF['control'][:,:,:,timepoints_keep];  #discard outlier pairs
            BaselineCBF['tag'] = BaselineCBF['img'][:,:,:,1::2]; 
            BaselineCBF['tag'] = BaselineCBF['tag'][:,:,:,timepoints_keep];  #discard outlier pairs
            BaselineCBF['avgcontrol'] = np.mean( BaselineCBF['control'],3);
            BaselineCBF['diff'] = BaselineCBF['control']-BaselineCBF['tag'];
            BaselineCBF['avgdiff'] = np.mean(BaselineCBF['diff'],3);
            BaselineCBF['stddiff'] = np.std(BaselineCBF['diff'],axis=3,ddof=1);
        
        if VERBOSE:
            import matplotlib.pyplot as pyplot
            from cmind.utils.montager import montager
            pyplot.figure()
            pyplot.imshow(montager(BaselineCBF['avgdiff']),cmap=pyplot.cm.gray)
        #nii = nib.load(pjoin(CBF_nii_dir,'I0_map_reg2CBF.nii.gz'))
        #I0_map = nii.get_data()
            
            
        if isinstance(T1_map_reg2CBF,(int,float)):  #use same T1 at all voxels
            T1_map=T1_map_reg2CBF*np.ones_like(BaselineCBF['avgcontrol'])
        else:
            imbool, T1_map_reg2CBF = imexist(T1_map_reg2CBF,'nifti-1')[0:2]
            if not imbool:
                raise Exception("T1_map_reg2CBF file, %s, not found")
            nii = nib.load(T1_map_reg2CBF)
            T1_map = nii.get_data()
            if np.sum(T1_map<0)!=0:
                warnings.warn("Negative values found in T1_map...setting them to zero")
                T1_map[T1_map<0]=0
            #truncate any unreasonable T1 values of less than 100 ms 
            tmp1=T1_map[T1_map>0]
            tmp1[tmp1<1e-1]=0;
            T1_map[T1_map>0]=tmp1;
    
        if VERBOSE:
            pyplot.figure()
            pyplot.imshow(montager(T1_map),vmin=0.5,vmax=2.5)
        
        #maskim=abs(BaselineCBF['avgcontrol'])>0.1*max(col(abs(BaselineCBF['avgcontrol'])));
        maskim=(BaselineCBF['avgcontrol']!=0)*(T1_map!=0)
        
        #f_guess = %
        R1app=1/T1_map[maskim] #fix later
        if np.sum(np.isinf(R1app))!=0:
            warnings.warn("Inf found in R1app...setting Inf vals to zero")
            R1app[np.isinf(R1app)]=0;

        if np.sum(np.isnan(R1app))!=0:
            warnings.warn("NaN found in R1app...setting NaNs to zero")
            R1app[np.isnan(R1app)]=0;
        
        #for functional runs, w=0.7, t=1.5, TR=4.0, no crushers
        #for BaselineCBF runs, w=1.4, t=2, TR=4, no crushers
        
        #Simple single-compartment model assuming we know the arterial transit time
        
        nii_N4bias=nib.load(N4BiasCBF); 
        N4bias=nii_N4bias.get_data()
        N4bias[N4bias==0]=1
        BaselineCBF['avgcontrol_N4']=np.mean(BaselineCBF['control'],3)/N4bias
        
        if False: #also do a calculation using a simpler model?
            f_simplest_model = np.zeros(BaselineCBF['avgdiff'].shape,'float32')
            f_simplest_model[maskim] = cbf_lambda*R1app*(BaselineCBF['avgdiff'][maskim])/(2*alpha*np.exp(-R1a*delta_art)*(BaselineCBF['avgcontrol_N4'][maskim]));
            f_simplest_model = f_simplest_model*6000;  #convert from ml/g/s to ml/100g/min
            #f_simplest_model=embed(f_simplest_model,maskim);
            
            f_simplest_model[np.isnan(f_simplest_model)]=0;
            f_simplest_model[np.isinf(f_simplest_model)]=0;
            #scale_max=prctile(f_simplest_model[maskim],p=(95));
            scale_max=np.percentile(f_simplest_model[maskim],q=95.0);
            if VERBOSE:
                pyplot.figure()
                pyplot.imshow(maskim.T*f_simplest_model.T,vmin=0,vmax=scale_max)
                #,colorbar('vert')
            
            cmind_save_nii_mod_header(nii_N4bias,f_simplest_model,pjoin(output_dir,'CBF_simplemodel.nii.gz'),None,[-40, min(240,scale_max)])
                
        #TODO:  update calcCBF_Wang2002 to use M0 from WM or CSF?
            
    
        BaselineCBF['avgdiff_N4']=BaselineCBF['avgdiff']/N4bias
        
        #BaselineCBF_N4=load_niigz('BaselineCBF_control_mcf_mean_N4_brain.nii.gz'); 
        #BaselineCBF_N4=single(BaselineCBF_N4.img);
        #WM_input=pjoin(T1proc_dir,'T1Segment_WM.nii.gz')
        ref=BaselineCBF_mcf_masked #pjoin(cwd,'BaselineCBF_mcf.nii.gz');  #'CBF_Wang2002.nii.gz')
        
        if imexist(ref,'nifti-1')[0]:
            if HRtoLR_warp: #use (inverse) fieldmap warp + affine 
                log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/WM_HRtoLR" -w "%s" --interp=sinc' % (WMpve,ref,output_dir,HRtoLR_warp), verbose=verbose, logger=module_logger)
            else: #use affine only (no fieldmap correction)
                log_cmd('$FSLDIR/bin/flirt -in "%s" -ref "%s" -out "%s/WM_HRtoLR" -applyxfm -init "%s" -interp sinc' % (WMpve,ref,output_dir,HRtoLR_affine), verbose=verbose, logger=module_logger)
        else:
            raise IOError("reference image, %s, doesn't exist" % ref)

        log_cmd('$FSLDIR/bin/fslmaths "%s/WM_HRtoLR" -thr 0.98 -bin "%s/WM_HRtoLR_98pct"' % (output_dir,output_dir), verbose=verbose, logger=module_logger)
        WM98=nib.load(pjoin(output_dir,'WM_HRtoLR_98pct.nii.gz')); 
        WM98=WM98.get_data()>0;
        
        log_cmd('$FSLDIR/bin/fslmaths "%s/WM_HRtoLR" -thr 0.75 -bin "%s/WM_HRtoLR_75pct"' % (output_dir,output_dir), verbose=verbose, logger=module_logger)
        WM75=nib.load(pjoin(output_dir,'WM_HRtoLR_75pct.nii.gz')); 
        WM75=WM75.get_data()>0;
        
        GMpve=WMpve.replace('WM','GM')
        #CSFpve=WMpve.replace('WM','CSF')
        if imexist(GMpve,'nifti-1')[0]:
            if HRtoLR_warp: #use (inverse) fieldmap warp + affine 
                log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s/GM_HRtoLR" -w "%s" --interp=sinc' % (GMpve,ref,output_dir,HRtoLR_warp), verbose=verbose, logger=module_logger) 
            else:
                log_cmd('$FSLDIR/bin/flirt -in "%s" -ref "%s" -out "%s/GM_HRtoLR" -applyxfm -init "%s" -interp sinc' % (GMpve,ref,output_dir,HRtoLR_affine), verbose=verbose, logger=module_logger)
                
            log_cmd('$FSLDIR/bin/fslmaths "%s/GM_HRtoLR" -thr 0.75 -bin "%s/GM_HRtoLR_75pct"' % (output_dir,output_dir), verbose=verbose, logger=module_logger)
            GM75=nib.load(pjoin(output_dir,'GM_HRtoLR_75pct.nii.gz')); 
            GM75=GM75.get_data()>0
        
        #        if(imexist(CSFpve))
        #           system(sprintf('flirt -in %s -ref %s -out CSF_HRtoLR -applyxfm -init %s -interp sinc',CSFpve,ref,HRtoLR_affine))
        #        end
        
        
        M0_WM=np.mean(BaselineCBF['avgcontrol_N4'][WM98]);
        #correction to M0 due to finite TR  (WM T1 is short enough that this is minimal)
        
        if not isinstance(T1_map_reg2CBF,(int,float)): #TODO.  remove?  already was loaded above
            T1_map=nib.load(T1_map_reg2CBF); 
            T1_map=T1_map.get_data()
        mask_T1map=(T1_map!=0)
        
        WM98_mapped=(WM98*mask_T1map)>0
        
        mean_T1_WM=np.mean(T1_map[WM98_mapped]);
        Mxy_ss_WM=calcflash(90,4000,1000*mean_T1_WM)[1] #TODO: calcflash
        M0_WM=M0_WM/Mxy_ss_WM;
        np.savetxt(pjoin(output_dir,'mean_M0_WM.txt'),[M0_WM,]);
    
                
        #water densities of WM=0.73, GM=0.89, Blood=0.87 and CSF=1.00 (see Donahue2006b:  Magn Reson.Med. 2006;56:1261?1273)
        Rwm=0.87/0.73;  #=1.1918,  larger than the value of 1.06 used in Wang1998
        #Rgm=0.87/0.89  %=1.1918,  larger than the value of 1.06 used in Wang1998
        #Wong1998 used:
        #T2wm=80; T2blood=200; TE=11;
        
        #T2star_wm=45 #e.g. ~45 at 3T in Wansapura1999,  67 at 1.5 T in Siemonsen2008
        #T2star_arterial_blood=53  #was previously set to 70.  #~60-70 at 3T in fully oxygenated blood.  for fetal: 80 in Wedegartner2010 at 3T
        
        """
        From Petersen2006b discussion:
        "Silvennoinen et al. (40) investigated the dependence of blood R2 and 
        R2* at different field strengths and showed that blood relaxation 
        parameters relate parabolically to the oxygenation saturation fraction 
        Y:
        R2* = A* + B*(1-Y) +C*(1-Y)**2
        where A*, B*, and C* were measured to be 18, 39, and 119, respectively, 
        at 3T, and for a hematocrit fraction of 0.44 (P.C.M. van Zijl, private 
        communication). In gradient-echo acquisitions, this would therefore lead 
        to a rather large underestimation of M0,a depending on the TE selected. 
        .
        .
        assuming 98% and 65% oxygen saturation for arterial and venous blood 
        respectively, Eq. [13] predicts R2,a* and R2,v* values of 18.8 [s–1] 
        and 46.2 [s–1], respectively. 
        """
        
        
        T2scaling=np.exp((1/T2star_wm-1/T2star_arterial_blood)*TE)
        M0blood=Rwm*T2scaling*M0_WM
        np.savetxt(pjoin(output_dir,'M0blood_est.txt'),[M0blood,]);
    
        CBF_cor = calcCBF_Wang2002(BaselineCBF['avgdiff_N4'],maskim,cbf_lambda,R1a,R1app,delta,delta_art,w,tau,TR,alpha,M0blood)
        scale_max=np.percentile(CBF_cor[maskim],q=95.0);
        cmind_save_nii_mod_header(nii_N4bias,CBF_cor,CBF_Wang_file,None,[-40, min(240,scale_max)]);

        mask_CBFmap=(CBF_cor!=0)
        GM75_mapped=(GM75*mask_CBFmap)>0
        WM75_mapped=(WM75*mask_CBFmap)>0
        mean_CBF_GM=np.mean(CBF_cor[GM75_mapped]);
        mean_CBF_WM=np.mean(CBF_cor[WM75_mapped]);
        std_CBF_GM=np.std(CBF_cor[GM75_mapped],ddof=1);
        std_CBF_WM=np.std(CBF_cor[WM75_mapped],ddof=1);
        module_logger.info("[mean_CBF_GM std_CBF_GM] = %s %s" % (mean_CBF_GM, std_CBF_GM))
        module_logger.info("[mean_CBF_WM std_CBF_WM] = %s %s" % (mean_CBF_WM, std_CBF_WM))
        ratio_CBF=mean_CBF_GM/mean_CBF_WM

        #save CBFstats mean_CBF_* std_CBF_* ratio_CBF alpha alpha_const  #TODO
        
        np.savetxt(pjoin(output_dir,'mean_CBF_GM.txt'),[mean_CBF_GM,]);
        np.savetxt(pjoin(output_dir,'mean_CBF_WM.txt'),[mean_CBF_WM,]);
        np.savetxt(pjoin(output_dir,'ratio_CBF.txt'),[ratio_CBF,]);
        
        
        CBF_Wang_params={}
            
        if alpha_const:  #if a measured alpha was supplied, also repeat using the hardcoded alpha value instead of the measured one
            CBF_cor_const = calcCBF_Wang2002(BaselineCBF['avgdiff_N4'],maskim,cbf_lambda,R1a,R1app,delta,delta_art,w,tau,TR,alpha_const,M0blood)
            cmind_save_nii_mod_header(nii_N4bias,CBF_cor_const,CBF_Wang_alphaconst_file,None,[-40, min(240,scale_max)]);
            CBF_Wang_params['alpha_const']=alpha_const
        
        #Store a record of the model parameters used for the fit
        #TODO: add M0 to the list
        
        CBF_Wang_params['CBF_lambda']=cbf_lambda
        CBF_Wang_params['R1a']=R1a
        CBF_Wang_params['delta']=delta
        CBF_Wang_params['delta_art']=delta_art
        CBF_Wang_params['TR']=TR
        CBF_Wang_params['w']=w
        CBF_Wang_params['tau']=tau
        CBF_Wang_params['alpha']=alpha
        
        if imexist(WMpve,'nifti-1')[0]:
            CBF_Wang_params['M0blood']=M0blood
        dict2csv(CBF_Wang_params,pjoin(output_dir,'CBF_Wang_model_inputs.csv'))
        
        save_params=True
        if save_params:
            import pickle as pickle
            with open(pjoin(output_dir,"CBF_Wang_params.pickle"), "wb") as f:   
                pickle.dump(CBF_Wang_params,f)
        
#        fid=open(pjoin(output_dir,'CBF_Wang_model_inputs.txt'),'w');
#        fid.write('cbf_lambda = %0.3g\n' % cbf_lambda)
#        fid.write('R1a = %0.3g\n' % R1a)
#        fid.write('delta = %0.3g\n' % delta)
#        fid.write('delta_art = %0.3g\n' % delta_art)
#        fid.write('TR = %0.3g\n' % TR)
#        fid.write('w = %0.3g\n' % w)
#        fid.write('tau = %0.3g\n' % tau)
#        fid.write('alpha = %0.3g\n' % alpha)
#        if not isinstance((alpha_file,float)):
#            fid.write('alpha_const = %0.3g\n' % alpha_const)
#        if imexist(WMpve,'nifti-1')[0]:
#             fid.write('M0blood = %0.3g\n' % M0blood)
#        fid.close()
    
        if generate_figures:
            #TODO: add colorbar/units to these figures
            log_cmd('$FSLDIR/bin/slicer "%s" -n -s 4 -i 0 160 -a "%s"' % (pjoin(output_dir,CBF_file_str),pjoin(output_dir,CBF_file_str+'.png')), verbose=verbose, logger=module_logger)
            if alpha_const:
                log_cmd('$FSLDIR/bin/slicer "%s" -n -s 4 -i 0 160 -a "%s"' % (pjoin(output_dir,CBF_file_str+'_alphaconst'),pjoin(output_dir,CBF_file_str+'_alphaconst.png')), verbose=verbose, logger=module_logger)
        
        writeHTML=True
        if writeHTML:
            from cmind.utils.cmind_HTML_report_gen import cmind_HTML_reports
            
            #read in results from computation to use in report
            extra_params={}
            try:    
                extra_params['alpha']=np.loadtxt(alpha_file,dtype=float)
            except:
                extra_params['alpha']=np.NaN
            try:    
                extra_params['mean_CBF_GM']=np.loadtxt(pjoin(output_dir,'mean_CBF_GM.txt'),dtype=float);  
            except: 
                extra_params['mean_CBF_GM']=np.NaN
            try:    
                extra_params['mean_CBF_WM']=np.loadtxt(pjoin(output_dir,'mean_CBF_WM.txt'),dtype=float)
            except: 
                extra_params['mean_CBF_WM']=np.NaN
            try:    
                extra_params['ratio_CBF']=np.loadtxt(pjoin(output_dir,'ratio_CBF.txt'),dtype=float);      
            except: 
                extra_params['ratio_CBF']=np.NaN
            try:
                CBF_Wang_params=csv2dict(pjoin(output_dir,'CBF_Wang_model_inputs.csv'))
                if alpha_const:
                    extra_params['alpha_const']=CBF_Wang_params['alpha_const'];
                else:
                    extra_params['alpha_const']=np.NaN;
            except:
                extra_params['alpha_const']=np.NaN;
        
            cmind_HTML_reports('baselineCBF',output_dir,None,ForceUpdate,extra_params);

        #water densities of WM=0.73, GM=0.89, Blood=0.87 and CSF=1.00 (see Donahue2006b:  Magn Reson.Med. 2006;56:1261?1273)


    #print any filename outputs for capture by LONI
    print("CBF_Wang_file:{}".format(CBF_Wang_file))
    print("CBF_Wang_alphaconst_file:{}".format(CBF_Wang_alphaconst_file))
    return (CBF_Wang_file,CBF_Wang_alphaconst_file)


#TODO: remove cmind_baselineCBF_quantify
cmind_baselineCBF_quantify=cmind_baselineCBF_quantitative  #alternative naming for backwards compatibility

def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    output_dir=pjoin(cmind_example_output_dir,'P','BaselineCBF')
    BaselineCBF_mcf_masked=pjoin(output_dir,'BaselineCBF_mcf_masked.nii.gz')
    N4BiasCBF=pjoin(output_dir,'BaselineCBF_N4Bias.nii.gz')
    BaselineCBF_timepoints_kept=pjoin(output_dir,'BaselineCBF_timepoints_kept.txt')
    alpha_file=pjoin(output_dir,'..','alpha','myalpha.txt')
    WMpve=pjoin(output_dir,'..','T1_proc','T1Segment_WM.nii.gz')
    HRtoLR_affine=pjoin(output_dir,'reg_BBR_fmap','lowres2highres_inv.mat')
    HRtoLR_warp=pjoin(output_dir,'reg_BBR_fmap','lowres2highres_warp_inv.nii.gz')
    T1_map_reg2CBF=pjoin(output_dir,'T1_map_reg2CBF.nii.gz')
    relmot=pjoin(output_dir,'mc','BaselineCBF_mcf_rel.rms')
    relmot_thresh=1.5
    ForceUpdate=True;
    cbf_lambda=0.9
    R1a=0.625
    delta=1.5
    delta_art=1.3
    w=1.4
    tau=2.0
    TR=4.0
    alpha_const = 0.75
    verbose=True
    generate_figures=True
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',
                        file_mode='w')  
    if verbose:
        func=cmind_timer(cmind_baselineCBF_quantitative,logger=logger)
    else:
        func=cmind_baselineCBF_quantitative
        
    func(output_dir,
         BaselineCBF_mcf_masked, 
         N4BiasCBF, 
         BaselineCBF_timepoints_kept,
         alpha_file,
         WMpve,
         T1_map_reg2CBF,
         alpha_const=alpha_const,
         HRtoLR_affine=HRtoLR_affine,
         HRtoLR_warp=HRtoLR_warp,
         relmot=relmot,
         relmot_thresh=relmot_thresh,
         cbf_lambda=cbf_lambda,
         R1a=R1a,
         delta=delta,
         delta_art=delta_art,
         w=w,
         tau=tau,
         generate_figures=generate_figures,
         ForceUpdate=ForceUpdate,
         TR=TR,
         verbose=verbose,
         logger=logger)
                   
if __name__ == '__main__':
    import sys,argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:      
        parser = argparse.ArgumentParser(description="Quantify CBF", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-cbfm","--BaselineCBF_mcf_masked", required=True, help="masked, motion-corrected BaselineCBF volume")
        parser.add_argument("-n4cbf","--N4BiasCBF", required=True, help="BaselineCBF N4Bias field")
        parser.add_argument("-tk","--BaselineCBF_timepoints_kept", required=True, help="Timepoints kept during BaselineCBF preprocessing")
        parser.add_argument("-af","--alpha_file", required=True, help="alpha txt file containing the inversion efficiency")
        parser.add_argument("-wm","-wmvol","--WMpve", required=True, help="white matter partial volume estimate")
        parser.add_argument("-T1map","--T1_map_reg2CBF", required=True, help="T1map that has been registered to BaselineCBF_mcf_masked")
        parser.add_argument("--alpha_const", required=False, type=float, default=0.75, help="also repeat the CBF calculation using this value of alpha")
        parser.add_argument("-aff","-h2l","--HRtoLR_affine",type=str,default="none", help="affine from highres (structural) to lowres space")
        parser.add_argument("-warp","-h2lw","--HRtoLR_warp",type=str,default="none", help="warp from highres (structural) to lowres space")
        parser.add_argument("-relmot","--relmot",type=str,default="none", help="relative motion .rms file from mcflirt")
        parser.add_argument("-relmot_thr","--relmot_thresh",type=float,default=1.5, help="relative motion threshold (mm) for discarding timepoints")
        parser.add_argument("--cbf_lambda",type=float,default=0.9, help="blood-brain partition coefficient")
        parser.add_argument("--R1a",type=float,default=0.625, help="R1 relaxation rate of arterial blood (1/seconds)")
        parser.add_argument("--delta",type=float,default=1.5, help="transit time (to capillary compartment) (seconds)")
        parser.add_argument("--delta_art",type=float,default=1.3, help="arterial transit time (seconds)")
        parser.add_argument("--w, --postlabeling_delay",dest='w',type=float,default=1.4, help="post-labeling delay (seconds)")
        parser.add_argument("--tau",type=float,default=2.0, help="label duration (seconds) ")
        parser.add_argument("--TR",type=float,default=4.0, help="TR (seconds)")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
         
        args=parser.parse_args()
        
        output_dir=args.output_dir;
        BaselineCBF_mcf_masked=args.BaselineCBF_mcf_masked;
        N4BiasCBF=args.N4BiasCBF;
        BaselineCBF_timepoints_kept=args.BaselineCBF_timepoints_kept;
        alpha_file=args.alpha_file;
        WMpve=args.WMpve;
        T1_map_reg2CBF=args.T1_map_reg2CBF;
        alpha_const=args.alpha_const;
        HRtoLR_affine=str2none(args.HRtoLR_affine);
        HRtoLR_warp=str2none(args.HRtoLR_warp);
        relmot=str2none(args.relmot);
        relmot_thresh=args.relmot_thresh;
        cbf_lambda=args.cbf_lambda;
        R1a=args.R1a;
        delta=args.delta;
        delta_art=args.delta_art;
        w=args.w;
        tau=args.tau;
        TR=args.TR;
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger
        
        if (not HRtoLR_affine) and (not HRtoLR_warp):
            raise IOError("either --HRtoLR_affine or --HRtoLR_warp must be specified")
            
        if verbose:
            cmind_baselineCBF_quantitative=cmind_timer(cmind_baselineCBF_quantitative,logger=logger)
    
        cmind_baselineCBF_quantitative(output_dir,
                                        BaselineCBF_mcf_masked, 
                                        N4BiasCBF, 
                                        BaselineCBF_timepoints_kept,
                                        alpha_file,
                                        WMpve,
                                        T1_map_reg2CBF,
                                        alpha_const=alpha_const,
                                        HRtoLR_affine=HRtoLR_affine,
                                        HRtoLR_warp=HRtoLR_warp,
                                        relmot=relmot,
                                        relmot_thresh=relmot_thresh,
                                        cbf_lambda=cbf_lambda,
                                        R1a=R1a,
                                        delta=delta,
                                        delta_art=delta_art,
                                        w=w,
                                        tau=tau,
                                        generate_figures=generate_figures,
                                        ForceUpdate=ForceUpdate,
                                        TR=TR,
                                        verbose=verbose,
                                        logger=logger)
        