.. CMIND pipelines documentation master file, created by
   sphinx-quickstart on Fri Sep 20 12:53:14 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

   #Welcome to the CMIND Pipelines python documentation!
   #====================================================

.. include:: ../README.rst

Documentation
=============

* :ref:`User Documentation <manual>` (manual describing details of the C-MIND processing pipelines)

* :ref:`C-MIND Database File Variables <Database_files>` (descriptions of the raw and processed image volumes available in the C-MIND database)

* :ref:`Pipeline API Documentation <api>` (comprehensive programmer's reference)


Download/Installation
=====================
Please see the installation details in the user's manual: :ref:`installation instructions <installation>`

.. include:: links_names.txt

.. toctree::
   :hidden:

   manual
   api