# -*- coding: utf-8 -*-
"""functions that generate workflows corresponding to the CMIND project"""
import os, warnings
from os.path import join as pjoin

import nipype.pipeline.engine as pe

from cmind.utils.utils import is_string_like
from cmind.globals import (cmind_atlas_mindboggle, 
                           cmind_atlas_labels_mindboggle,
                           cmind_atlas_JHU, 
                           cmind_atlas_labels_JHU,
                           cmind_atlas_AAL,
                           cmind_atlas_labels_AAL)

from nipype.pipeline.engine import Node
from nipype.interfaces.utility import Merge, Split

def _parse_func_files(func_files,reuse_same=False,BOLD_only=False):
    """parse the ASL and/or BOLD filenames from the input file list
    
    if reuse_same=True, the same volume will be fed into both ASL & BOLD processing streams
    """
    if BOLD_only:
        BOLD_vols=[f for f in func_files if 'BOLD.nii' in f]
        if BOLD_vols==[]: #if not _BOLD.nii try _ASL.nii instead
            BOLD_vols=func_files
        ASL_vols=[None,]
    else:
        ASL_vols=[f for f in func_files if 'ASL.nii' in f]
        if reuse_same:  #TODO: remove this case.  should not need to be used
            BOLD_vols=ASL_vols
        else:
            BOLD_vols=[f for f in func_files if 'BOLD.nii' in f]
            
    if len(ASL_vols)>2:
        raise ValueError("More than one matching ASL file found")
    elif len(ASL_vols)==1:
        ASL_vol=ASL_vols[0]
    else:
        ASL_vol=None

    if len(BOLD_vols)>2:
        raise ValueError("More than one matching BOLD file found")
    elif len(BOLD_vols)==1:
        BOLD_vol=BOLD_vols[0]
    else:
        BOLD_vol=None

    if (ASL_vol is None) and (BOLD_vol is None):
        raise ValueError("could not find ASL or BOLD volumes with the expected filenames")
    return ASL_vol, BOLD_vol
    
def generate_structural_workflow(output_base_dir,
                                 age_months,
                                 subj_data,
                                 name='structural_proc_T1andT2',
                                 use_atropos_segmentation=True,
                                 atropos_priors_path=None,
                                 atropos_add_T2_if_present=False):
                                     
    """Build a workflow for the T1 and T2 structural images"""
    from cmind.nipype.cmind_nodes import (cmind_struct_preprocess, 
                                      cmind_age_to_template,
                                      cmind_normalize,
                                      cmind_segment,
                                      cmind_segment_atropos)
                                      
    cmind_struct_preprocess.inputs.bet_thresh=0.3
    cmind_struct_preprocess.inputs.age_months=age_months
    cmind_struct_preprocess.inputs.omit_segmentation=True
    
    
    
    if use_atropos_segmentation: #omit the old FAST-based segmentation during cmind_struct_preprocess
        cmind_struct_preprocess.inputs.omit_segmentation=True
        #cmind_segment_atropos.iterables=[("Nclasses",[3,4,6]),]

        if atropos_add_T2_if_present and subj_data['T2']:
            cmind_segment_atropos.inputs.prior_weight=0.15  #mpirically determined to be reasonable
        else:
            cmind_segment_atropos.inputs.prior_weight=0.10  #mpirically determined to be reasonable
        
        cmind_segment_atropos.inputs.Nclasses = 3
        cmind_segment_atropos = cmind_segment_atropos.clone(name='cmind_segment')  #
        
        if atropos_priors_path:
            if not os.path.isdir(atropos_priors_path):
                raise ValueError("specified atropos_priors_path directory not found")
            #cmind_segment_atropos.inputs.Nclasses = 5
            #cmind_segment_atropos.inputs.Nclasses = 3
            cmind_segment_atropos.inputs.priors_dir = None #atropos_priors_path
            
            #also repeat with 6 classes:  separates brainstem & cerebellum as well
            cmind_segment_atropos_6class = cmind_segment_atropos.clone(name='cmind_segment_atropos_6class')
            cmind_segment_atropos_6class.inputs.Nclasses=6
            cmind_segment_atropos_6class.inputs.priors_dir = atropos_priors_path
            
        
    cmind_struct_preprocess.base_dir=output_base_dir
    
    cmind_age_to_template.inputs.age_months=age_months
    cmind_age_to_template.base_dir=output_base_dir
    
    cmind_normalize.inputs.ANTS_reg_case = 'SynAff'
    cmind_normalize.base_dir=output_base_dir
    
    #This one will work with either T1 only or T1+T2
    structural_proc_T1andT2  = pe.Workflow(name='struct_temp',base_dir=output_base_dir)
    
    #structural_proc_T1andT2 internal connections
    structural_proc_T1andT2.connect([
                      (cmind_struct_preprocess, cmind_normalize, [('fname_brain', 'T1_volume_nii')]),
                      (cmind_age_to_template, cmind_normalize, [('reg_struct_file', 'reg_struct')]),  #TODO: passing reg_struct dict directly fails
                    ])
                    
                    
    if use_atropos_segmentation:
        structural_proc_T1andT2.connect([
                  (cmind_struct_preprocess, cmind_segment_atropos, [('fname_head', 'fname_head')]),
                  (cmind_struct_preprocess, cmind_segment_atropos, [('fname_brain', 'fname_brain')]),
                  (cmind_struct_preprocess, cmind_segment_atropos, [('fname_brain_mask', 'fname_brain_mask')]),
                ])                 

        if atropos_priors_path is not None:
            structural_proc_T1andT2.connect([
                      (cmind_normalize, cmind_segment_atropos, [('reg_struct_file', 'reg_struct')]),  #TODO: passing reg_struct dict directly fails
                      
                      (cmind_struct_preprocess, cmind_segment_atropos_6class, [('fname_head', 'fname_head')]),
                      (cmind_struct_preprocess, cmind_segment_atropos_6class, [('fname_brain', 'fname_brain')]),
                      (cmind_struct_preprocess, cmind_segment_atropos_6class, [('fname_brain_mask', 'fname_brain_mask')]),
                      (cmind_normalize, cmind_segment_atropos_6class, [('reg_struct_file', 'reg_struct')]),  #TODO: passing reg_struct dict directly fails
                      
                    ])                 
    else:
        structural_proc_T1andT2.connect([
                  (cmind_struct_preprocess, cmind_segment, [('fname_brain', 'fname_brain')]),
                ])                 
        
    if subj_data['Anatomical']:  #has a T1 scan?
        cmind_struct_preprocess.inputs.struct_nii=subj_data['Anatomical']['files']
        cmind_struct_preprocess.inputs.oname_root='T1W'
    
        if subj_data['T2']:  #also has a T2 scan?
            cmind_struct_preprocess.inputs.bet_use_T2=False
            cmind_struct_preprocess.inputs.T2_struct_nii=subj_data['T2']['files']
            #The code below, would independently normalize the T2 volume.  Probably want to just apply the T1W normalization here instead
            #cmind_normalize_T2=cmind_normalize.clone('cmind_normalize_T2')
            #structural_proc_T1andT2.connect([
            #          (cmind_struct_preprocess, cmind_normalize_T2, [('fname2_brain', 'T1_volume_nii')]),
            #          (cmind_age_to_template, cmind_normalize_T2, [('reg_struct_file', 'reg_struct')]),  #TODO: passing reg_struct dict directly fails
            #         ])
            if atropos_add_T2_if_present and use_atropos_segmentation:  #incorporate the T2 volume into the segmentation
                structural_proc_T1andT2.connect([
                  (cmind_struct_preprocess, cmind_segment_atropos, [('fname2_head', 'additional_intensity_vols')]),
                ])                 
                if atropos_priors_path:
                    structural_proc_T1andT2.connect([
                      (cmind_struct_preprocess, cmind_segment_atropos_6class, [('fname2_head', 'additional_intensity_vols')]),
                    ])                 
            
    elif subj_data['T2']:  #if no T1, but has T2, use it instead
        cmind_struct_preprocess.inputs.struct_nii=subj_data['T2']['files']
        cmind_struct_preprocess.inputs.oname_root='T2W'
    else:
        raise ValueError("Subject has neither T1 or T2 weighted Anatomical data")
        
    structural_proc_T1andT2.config['execution']['remove_unnecessary_outputs']=False
    structural_proc_T1andT2=structural_proc_T1andT2.clone(name)
    
    return structural_proc_T1andT2


def init_fieldmap_nodes(output_base_dir,
                               subj_data,
                               fieldmap_case='CBF',
                               workflow=None,
                               cmind_fieldmap_process = None,
                               cmind_fieldmap_regBBR = None):
    
    #if no pre-existing cmind_fieldmap_process node was passed in, create one
    if cmind_fieldmap_process is None:  
        from cmind.nipype.cmind_nodes import cmind_fieldmap_process                          
        GREfiles=subj_data['GREMap']['files']
        abs_file=None
        rads_file=None
        for f in GREfiles:
            if ('abs.nii' in f) and abs_file is None:
                abs_file=f
            elif ('rads.nii' in f) and rads_file is None:
                rads_file=f                
            else:
                raise ValueError("couldn't find abs and rads GREMap files")
        if (abs_file is None) or (rads_file is None):
            raise ValueError("couldn't find abs and rads GREMap files")
            
        #cmind_fieldmap_process.inputs.output_dir=output_dirs['Fieldmap']
        cmind_fieldmap_process.base_dir=output_base_dir
        cmind_fieldmap_process.inputs.fieldmap_nii_mag=abs_file
        cmind_fieldmap_process.inputs.fieldmap_nii_rads=rads_file

    #if no pre-existing cmind_fieldmap_regBBR node was passed in, create one
    if cmind_fieldmap_regBBR is None:
        from cmind.nipype.cmind_nodes import (cmind_fieldmap_regBBR_CBF,
                                              cmind_fieldmap_regBBR_ASLBOLD,
                                              cmind_fieldmap_regBBR_DTI,
                                              cmind_fieldmap_regBBR_HARDI,
                                              cmind_fieldmap_regBBR_RestingState)
        if fieldmap_case=='CBF' or fieldmap_case=='T1':
            cmind_fieldmap_regBBR=cmind_fieldmap_regBBR_CBF
        elif fieldmap_case=='ASLBOLD':
            cmind_fieldmap_regBBR=cmind_fieldmap_regBBR_ASLBOLD
        elif fieldmap_case=='DTI':
            cmind_fieldmap_regBBR=cmind_fieldmap_regBBR_DTI
        elif fieldmap_case=='HARDI':
            cmind_fieldmap_regBBR=cmind_fieldmap_regBBR_HARDI
        elif fieldmap_case=='RestingState':
            cmind_fieldmap_regBBR=cmind_fieldmap_regBBR_RestingState
        else:
            raise ValueError("Invalid fieldmap_case")
            
        cmind_fieldmap_regBBR.base_dir=output_base_dir
        cmind_fieldmap_regBBR=cmind_fieldmap_regBBR.clone('cmind_fieldmap_regBBR')
        
    #cmind_fieldmap_process=cmind_fieldmap_process.clone('cmind_fieldmap_process'+fieldmap_case)
    #cmind_fieldmap_regBBR=cmind_fieldmap_regBBR.clone('cmind_fieldmap_regBBR_'+fieldmap_case)
   # cmind_fieldmap_process=cmind_fieldmap_process.clone('cmind_fieldmap_process')
    
    #workflow: (fieldmap-related) internal connections 
    if workflow is not None:
        workflow.connect([
            #fieldmap inputs for boundary-based registration
            (cmind_fieldmap_process, cmind_fieldmap_regBBR, [('fieldmap_vol', 'Fieldmap_vol_processed')]),
            (cmind_fieldmap_process, cmind_fieldmap_regBBR, [('fieldmap_unmasked', 'Fieldmap_vol_unmasked')]),
            (cmind_fieldmap_process, cmind_fieldmap_regBBR, [('fieldmap2struct_affine', 'fieldmap2struct_affine')]),
            
    #        #fieldmap inputs for quantification
    #        (cmind_fieldmap_regBBR, cmind_baselineCBF_quantitative, [('HRtoLR_Warp_fname', 'HRtoLR_warp')]),
        ])  
    
    return cmind_fieldmap_process, cmind_fieldmap_regBBR, workflow   


                            
def generate_T1mapping_workflow(output_base_dir,
                                age_months,
                                subj_data,
                                name='T1mapping',
                                has_BaselineCBF=True):
    import re
    from cmind.nipype.cmind_nodes import (cmind_T1map_preprocess, 
                                          cmind_T1map_fit)
                                      
    #require at least 5 T1Est volumes to attempt T1 fitting
    if subj_data['T1Est'] and (len(subj_data['T1Est']['files'])>4):
        
        
        keep_decimal = re.compile(r'[^\d.]+') #match only decimals
        
        T1EST_filelist=subj_data['T1Est']['files']
        TI_list=[]
        for f in T1EST_filelist:
            TIstr=[item for item in f.split('_') if item.find('TI')!=-1]
            TI_list.append(int(keep_decimal.sub('',TIstr[0])))
        
        cmind_T1map_preprocess.base_dir=output_base_dir
        cmind_T1map_preprocess.inputs.T1EST_nii_folder=zip(T1EST_filelist,TI_list)
        
        T1map_workflow = pe.Workflow(name='T1map_tempname',base_dir=output_base_dir)
        
        cmind_T1map_fit.base_dir=output_base_dir
        
        #T1map internal connections
        T1map_workflow.connect([
                                    (cmind_T1map_preprocess, cmind_T1map_fit, [('T1concat_mcf', 'T1concat_file')]),
                                    (cmind_T1map_preprocess, cmind_T1map_fit, [('TIvals_file', 'TIvals_file')]),
                        ])     
    #    T1map_workflow.run(plugin=plugin)
    
 
    else:
        warnings.warn("Not enough T1Est volumes found to create a T1 mapping workflow")
        T1map_workflow = None

    if T1map_workflow is not None:
        T1map_workflow.config['execution']['remove_unnecessary_outputs']=False            
        T1map_workflow=T1map_workflow.clone(name=name)
#        T1map_workflow.write_graph(dotfilename='T1map_proc.dot',graph2use='flat',format='png') #'flat')                                           
    
    return T1map_workflow
    
def generate_BaselineCBF_workflow(output_base_dir,
                                  age_months,
                                  subj_data,
                                  name='BaselineCBF',
                                  UCLA_flag=False,
                                  omit_norm=False, 
                                  has_T1map=True,
                                  add_nofmap_regBBR=True):
    
    from cmind.nipype.cmind_nodes import (cmind_apply_normalization,
                                      
                                      cmind_baselineCBF_preprocess,
                                      cmind_baselineCBF_coregisterT1, 
                                      cmind_baselineCBF_quantitative,
                                      
                                      cmind_fieldmap_regBBR_CBF,
                                      
                                      cmind_alpha)
                                      
    cbf_workflow = pe.Workflow(name='cbf_tempname',base_dir=output_base_dir)

    if subj_data['Alpha']:    
        cmind_alpha.base_dir=output_base_dir
        cmind_alpha.inputs.alpha_complex_nii=subj_data['Alpha']['files']
    
    cmind_baselineCBF_preprocess.base_dir=output_base_dir
    cmind_baselineCBF_preprocess.inputs.UCLA_flag=UCLA_flag
    cmind_baselineCBF_preprocess.inputs.CBF_nii=subj_data['BaselineCBF']['files']
    
    
    if subj_data['BaselineCBF']['use_fieldmap']:
        cmind_fieldmap_process, cmind_fieldmap_regBBR, cbf_workflow = init_fieldmap_nodes(output_base_dir,
                           subj_data,
                           fieldmap_case='CBF',
                           workflow=cbf_workflow)
                           
        if add_nofmap_regBBR:
            cmind_fieldmap_regBBR_nofieldmap = cmind_fieldmap_regBBR_CBF.clone('cmind_fieldmap_regBBR_nofieldmap')
            cbf_workflow.connect([
                #(cmind_baselineCBF_preprocess, cmind_fieldmap_regBBR_nofieldmap, [('BaselineCBF_Meansub', 'lowres_vol')]),
                (cmind_baselineCBF_preprocess, cmind_fieldmap_regBBR_nofieldmap, [('BaselineCBF_mcf_control_mean_N4', 'lowres_vol')]),
            ])         

    else:
        cmind_fieldmap_regBBR = cmind_fieldmap_regBBR_CBF.clone('cmind_fieldmap_regBBR')


    #to avoid mixing methods, rename the cmind_baselineCBF_quantitative node if there is not a T1map volume
    if not has_T1map:
        cmind_baselineCBF_quantitative=cmind_baselineCBF_quantitative.clone(name="cmind_baselineCBF_quantitative_NoT1map")
    
    #cbf_workflow internal connections
    cbf_workflow.connect([
    
        #fieldmap (optional) boundary-based registration
    
        (cmind_baselineCBF_preprocess, cmind_fieldmap_regBBR, [('BaselineCBF_mcf_control_mean_N4', 'lowres_vol')]),
        #(cmind_baselineCBF_preprocess, cmind_fieldmap_regBBR, [('BaselineCBF_Meansub', 'lowres_vol')]),
        
        #quantification
        (cmind_baselineCBF_preprocess, cmind_baselineCBF_quantitative, [('BaselineCBF_mcf_masked', 'BaselineCBF_mcf_masked')]),
        (cmind_baselineCBF_preprocess, cmind_baselineCBF_quantitative, [('BaselineCBF_N4Bias', 'N4BiasCBF')]),
        (cmind_baselineCBF_preprocess, cmind_baselineCBF_quantitative, [('BaselineCBF_timepoints_kept', 'BaselineCBF_timepoints_kept')]),
        
        (cmind_baselineCBF_preprocess, cmind_baselineCBF_quantitative, [('BaselineCBF_relmot', 'relmot')]),   

        
    ])         
    
    if subj_data['Alpha']:   
         cbf_workflow.connect([
                 (cmind_alpha, cmind_baselineCBF_quantitative, [('alpha_fname', 'alpha_file')]),
        ])
    else:
        warnings.warn("No alpha scan found, using fixed alpha=0.75")
        cmind_baselineCBF_quantitative.inputs.alpha_file=0.75


    #connect T1map inputs
    if has_T1map:
        cmind_baselineCBF_coregisterT1.base_dir=output_base_dir
        cbf_workflow.connect([
            #(cmind_baselineCBF_preprocess, cmind_baselineCBF_coregisterT1, [('BaselineCBF_Meansub', 'CBF_nii')]),   
            (cmind_baselineCBF_preprocess, cmind_baselineCBF_coregisterT1, [('BaselineCBF_mcf_control_mean_N4', 'CBF_nii')]),   
            (cmind_baselineCBF_coregisterT1, cmind_baselineCBF_quantitative, [('T1map_reg2CBF', 'T1_map_reg2CBF')]),
        ])           
    else:  #TODO
        cmind_baselineCBF_quantitative.inputs.T1_map_reg2CBF=1.3
        warnings.warn("No T1 map found.  a constant T1 value will be used instead.In future may implement an age-appropriate T1")
        #raise ValueError("T1map currently required.  In future may implement an age-appropriate T1 if T1 map is missing")        

    if subj_data['BaselineCBF']['use_fieldmap']:
        
        #cbf_workflow internal connections (fieldmap-related)
        cbf_workflow.connect([
            #fieldmap inputs for quantification
            (cmind_fieldmap_regBBR, cmind_baselineCBF_quantitative, [('HRtoLR_Warp_fname', 'HRtoLR_warp')]),
        ]) 
        if add_nofmap_regBBR:
            cbf_workflow.connect([
       
                (cmind_fieldmap_regBBR_nofieldmap, cmind_baselineCBF_quantitative, [('HRtoLR_Affine_fname' , 'HRtoLR_affine')]),
            ])  
    else:
        cbf_workflow.connect([
   
            (cmind_fieldmap_regBBR, cmind_baselineCBF_quantitative, [('HRtoLR_Affine_fname' , 'HRtoLR_affine')]),
        ])         
        
    if not omit_norm:
        cmind_apply_normalization_CBF=cmind_apply_normalization.clone('cmind_apply_normalization_CBF')
        cmind_apply_normalization_CBF.base_dir=output_base_dir
        
        
        if False:  #old way
            cbf_workflow.connect([
                #normalize BaselineCBF
                (cmind_baselineCBF_quantitative, cmind_apply_normalization_CBF, [('CBF_Wang_file', 'input_name')]),
            ])         
        else: #new way:  normalize both outputs
            CBF_Outputs_Merge = Node(name='CBF_Outputs_Merge', interface=Merge(2))   
            cbf_workflow.add_nodes([CBF_Outputs_Merge,])
            cbf_workflow.connect([
                    (cmind_baselineCBF_quantitative, CBF_Outputs_Merge,[('CBF_Wang_file','in1'),
                                                   ('CBF_Wang_alphaconst_file','in2'),
                                                   ]),  #merge CBF volumes into a single list
                     (CBF_Outputs_Merge, cmind_apply_normalization_CBF,[('out','input_name')]),     #connect the PVE list as input
                                        ])

        if subj_data['BaselineCBF']['use_fieldmap']:
            cbf_workflow.connect([
                (cmind_fieldmap_regBBR, cmind_apply_normalization_CBF, [('LRtoHR_Warp_fname', 'LRtoHR_warp')]),                
            ])         
        else:
           cbf_workflow.connect([
                #normalize BaselineCBF
                (cmind_fieldmap_regBBR, cmind_apply_normalization_CBF, [('LRtoHR_Affine_fname', 'LRtoHR_affine')]),
            ])         
            
        
    
    cbf_workflow.config['execution']['remove_unnecessary_outputs']=False    
    cbf_workflow=cbf_workflow.clone(name)
#    cbf_workflow.write_graph(dotfilename='BaselineCBF_proc.dot',graph2use='flat',format='png') #'flat')   
    return cbf_workflow

def generate_Physio_workflow(output_base_dir,
                             age_months,
                             subj_data,
                             name="Physio",
                             UCLA_flag=False,
                             AAL_to_SubjectSpace=True,
                             Mindboggle_to_SubjectSpace=True,
                             JHU_to_SubjectSpace=True,
                             PVE_to_SubjectSpace=True,
                             add_nofmap_regBBR=True,
                             use_atropos_segmentation=True,
                             atropos_priors_path=None,
                             atropos_add_T2_if_present=True,
                             add_diffusion_workflow=False): #move AAL_to_SubjectSpace within CBF_workflow?
    
    from cmind.globals import cmind_shellscript_dir, cmind_ANTs_dir
    from cmind.nipype.cmind_nodes import (cmind_apply_normalization,
                                          cmind_T1_tissue_averages,
                                          cmind_atlas_to_subject,
                                          cmind_fieldmap_regBBR_CBF,
                                          cmind_atlas_ROI_averages)

    cmind_fieldmap_process = None #will be set later
    
    Physio_workflow = pe.Workflow(name='Physio_tempname',base_dir=output_base_dir)


    #create Structural workflow  
    Structural_Physio = generate_structural_workflow(output_base_dir,
                                                     age_months,
                                                     subj_data,
                                                     name='Structural_Physio',
                                                     use_atropos_segmentation=use_atropos_segmentation,
                                                     atropos_priors_path=atropos_priors_path,
                                                     atropos_add_T2_if_present=atropos_add_T2_if_present)    
    
    #create T1mapping workflow
    T1map_workflow = generate_T1mapping_workflow(output_base_dir,
                                                 age_months,
                                                 subj_data,
                                                 name='T1mapping')   
    if T1map_workflow:
        has_T1map=True
        CBF_quant_node_name='cmind_baselineCBF_quantitative'
    else:
        has_T1map=False
        CBF_quant_node_name='cmind_baselineCBF_quantitative_NoT1map'

    
    if subj_data['BaselineCBF'] and subj_data['BaselineCBF']['files']:
        has_BaselineCBF=True
    else:
        has_BaselineCBF=False

    if add_diffusion_workflow:
        if ('DTI' in subj_data) and subj_data['DTI']:
            has_DTI = True
        else:
            has_DTI = False
        if ('HARDI' in subj_data) and subj_data['HARDI']:
            has_HARDI = True
        else:
            has_HARDI = False

    if AAL_to_SubjectSpace and (has_T1map or has_BaselineCBF):    
        AAL_to_subject = cmind_atlas_to_subject.clone("AAL_to_subject")
        AAL_to_subject.base_dir = output_base_dir
        AAL_to_subject.inputs.omit_MNItoHR=False
        AAL_to_subject.inputs.interp_type='NearestNeighbor'
        AAL_to_subject.inputs.split_script=pjoin(cmind_shellscript_dir,'aal_split.sh') #TODO: add to cmind.globals
        AAL_to_subject.inputs.atlas_vol=cmind_atlas_AAL  #TODO: add to cmind.globals
        AAL_to_subject.inputs.output_prefix='AAL'
        AAL_to_subject.inputs.ANTS_path=cmind_ANTs_dir

        AAL_ROI_6seg_averages = cmind_atlas_ROI_averages.clone("AAL_ROI_6seg_averages")        
        AAL_ROI_6seg_averages.inputs.ROI_labels_csv = cmind_atlas_labels_AAL
        
        AAL_ROI_3seg_averages = cmind_atlas_ROI_averages.clone("AAL_ROI_3seg_averages")        
        AAL_ROI_3seg_averages.inputs.ROI_labels_csv = cmind_atlas_labels_AAL
        
        Physio_workflow.add_nodes([AAL_to_subject, AAL_ROI_6seg_averages, AAL_ROI_3seg_averages])

    if Mindboggle_to_SubjectSpace and (has_T1map or has_BaselineCBF):    
        Mindboggle_to_subject = cmind_atlas_to_subject.clone("Mindboggle_to_subject")
        Mindboggle_to_subject.base_dir = output_base_dir
        Mindboggle_to_subject.inputs.omit_MNItoHR=False
        Mindboggle_to_subject.inputs.interp_type='NearestNeighbor'
        Mindboggle_to_subject.inputs.split_script=None #pjoin(cmind_shellscript_dir,'Mindboggle_split.sh') #TODO: add to cmind.globals
        Mindboggle_to_subject.inputs.atlas_vol=cmind_atlas_mindboggle  #TODO: add to cmind.globals
        Mindboggle_to_subject.inputs.output_prefix='Mindboggle'
        Mindboggle_to_subject.inputs.ANTS_path=cmind_ANTs_dir

        Mindboggle_ROI_6seg_averages = cmind_atlas_ROI_averages.clone("Mindboggle_ROI_6seg_averages")        
        Mindboggle_ROI_6seg_averages.inputs.ROI_labels_csv = cmind_atlas_labels_mindboggle

        Mindboggle_ROI_3seg_averages = cmind_atlas_ROI_averages.clone("Mindboggle_ROI_3seg_averages")        
        Mindboggle_ROI_3seg_averages.inputs.ROI_labels_csv = cmind_atlas_labels_mindboggle        
        
        Physio_workflow.add_nodes([Mindboggle_to_subject, Mindboggle_ROI_6seg_averages, Mindboggle_ROI_3seg_averages])
        
    if JHU_to_SubjectSpace and (has_T1map or has_BaselineCBF):    
        JHU_to_subject = cmind_atlas_to_subject.clone("JHU_to_subject")
        JHU_to_subject.base_dir = output_base_dir
        JHU_to_subject.inputs.omit_MNItoHR=False
        JHU_to_subject.inputs.interp_type='NearestNeighbor'
        JHU_to_subject.inputs.split_script=None #pjoin(cmind_shellscript_dir,'JHU_split.sh') #TODO: add to cmind.globals
        JHU_to_subject.inputs.atlas_vol=cmind_atlas_JHU  #TODO: add to cmind.globals
        JHU_to_subject.inputs.output_prefix='JHU'
        JHU_to_subject.inputs.ANTS_path=cmind_ANTs_dir

        JHU_ROI_6seg_averages = cmind_atlas_ROI_averages.clone("JHU_ROI_6seg_averages")        
        JHU_ROI_6seg_averages.inputs.ROI_labels_csv = cmind_atlas_labels_JHU

        JHU_ROI_3seg_averages = cmind_atlas_ROI_averages.clone("JHU_ROI_3seg_averages")        
        JHU_ROI_3seg_averages.inputs.ROI_labels_csv = cmind_atlas_labels_JHU        
        
        Physio_workflow.add_nodes([JHU_to_subject, JHU_ROI_6seg_averages, JHU_ROI_3seg_averages])
        
    if has_BaselineCBF:
        #create BaselineCBF workflow
        CBF_workflow = generate_BaselineCBF_workflow(output_base_dir,
                                                     age_months,
                                                     subj_data,
                                                     name='BaselineCBF',
                                                     omit_norm=False,
                                                     UCLA_flag=UCLA_flag,
                                                     has_T1map=has_T1map)  
    else:  #if no BaselineCBF data, register the T1map data directly
        CBF_workflow = None
    
        if has_T1map: 
            if subj_data['T1Est']['use_fieldmap']:
                cmind_fieldmap_process, cmind_fieldmap_regBBR_T1, T1map_workflow = init_fieldmap_nodes(output_base_dir,
                           subj_data,
                           fieldmap_case='T1',
                           workflow=T1map_workflow)
                           
                Physio_workflow.connect([ 
                    (Structural_Physio,cmind_fieldmap_process,[
                         ('cmind_struct_preprocess.fname_head','T1_head_vol'),
                         ('cmind_struct_preprocess.fname_brain','T1_brain_vol'),
                    ]),
                ])            
                if add_nofmap_regBBR:
                    cmind_fieldmap_regBBR_T1_nofieldmap = cmind_fieldmap_regBBR_CBF.clone("cmind_fieldmap_regBBR_T1_nofieldmap")
                    Physio_workflow.connect([
                        (T1map_workflow, cmind_fieldmap_regBBR_T1_nofieldmap, [('cmind_T1map_fit.T1_map_fname', 'lowres_vol')]),
                        (Structural_Physio,cmind_fieldmap_regBBR_T1_nofieldmap,[
                             ('cmind_normalize.reg_struct_file','reg_struct_file'),
                             ('cmind_struct_preprocess.fname_head','T1_vol'),
                             ('cmind_struct_preprocess.fname_brain','T1_brain_vol'),
                             ('cmind_segment.WM_vol','WMseg_vol'),
                        ]),
                    ])  
            else:
                cmind_fieldmap_regBBR_T1 = cmind_fieldmap_regBBR_CBF.clone('cmind_fieldmap_regBBR_T1')
                
            Physio_workflow.connect([
                (T1map_workflow, cmind_fieldmap_regBBR_T1, [('cmind_T1map_fit.T1_map_fname', 'lowres_vol')]),

                (Structural_Physio,cmind_fieldmap_regBBR_T1,[
                     ('cmind_normalize.reg_struct_file','reg_struct_file'),
                     ('cmind_struct_preprocess.fname_head','T1_vol'),
                     ('cmind_struct_preprocess.fname_brain','T1_brain_vol'),
                     ('cmind_segment.WM_vol','WMseg_vol'),
                ]),
            ])  
        else:
            Physio_workflow.add_nodes([Structural_Physio])
            
    if PVE_to_SubjectSpace and (has_T1map or has_BaselineCBF):   
        PVE_to_subject = cmind_atlas_to_subject.clone("PVE_to_subject")
        PVE_to_subject.base_dir = output_base_dir
        PVE_to_subject.inputs.omit_MNItoHR=True
        PVE_to_subject.inputs.interp_type='BSpline[3]'
        PVE_to_subject.inputs.split_script=None
        PVE_to_subject.inputs.ANTS_path=cmind_ANTs_dir
        
        """
        use Merge/Split to allow all 3 PVEs to be passed/retrieved as lists 
        from PVE_to_subject
        """
        PVE_Split = Node(name='PVE_Split', interface=Split())   
        
        
        Physio_workflow.add_nodes([PVE_to_subject,
                                   PVE_Split])

        if atropos_priors_path:
            seg_Nclasses=Structural_Physio.get_node('cmind_segment').inputs.Nclasses
            PVE_Merge = Node(name='PVE_Merge', interface=Merge(seg_Nclasses))   
            Physio_workflow.add_nodes([PVE_Merge,])
            PVE_Split.inputs.splits=[1,]*seg_Nclasses
            PVE_to_subject.inputs.output_prefix=['WMpve','GMpve','CSFpve'] #,'DeepGMpve','Brainstempve']
            Physio_workflow.connect([
                (Structural_Physio, PVE_Merge,[('cmind_segment.WM_vol','in1'),
                                               ('cmind_segment.GM_vol','in2'),
                                               ('cmind_segment.CSF_vol','in3'),
#                                               ('cmind_segment.DeepGM_vol','in4'),
#                                               ('cmind_segment.Brainstem_vol','in5')
                                               ]),  #merge PVE volumes into a single list
                 (Structural_Physio, PVE_to_subject,[('cmind_normalize.reg_struct_file','reg_struct')]),     #connect the PVE list as input
                                    ])
                                    
            PVE_Merge6 = Node(name='PVE_Merge6', interface=Merge(6))   
            PVE_Split6 = Node(name='PVE_Split6', interface=Split())   
            PVE_Split6.inputs.splits=[1,1,1,1,1,1]
            Physio_workflow.add_nodes([PVE_Merge6,PVE_Split6])
            PVE_to_subject6=PVE_to_subject.clone(name='PVE_to_subject6')
            PVE_to_subject6.inputs.output_prefix=['WMpve','GMpve','CSFpve','DeepGMpve','Brainstempve','Cerebellarpve']
            Physio_workflow.connect([
                (Structural_Physio, PVE_Merge6,[('cmind_segment_atropos_6class.WM_vol','in1'),
                                               ('cmind_segment_atropos_6class.GM_vol','in2'),
                                               ('cmind_segment_atropos_6class.CSF_vol','in3'),
                                               ('cmind_segment_atropos_6class.DeepGM_vol','in4'),
                                               ('cmind_segment_atropos_6class.Brainstem_vol','in5'),
                                               ('cmind_segment_atropos_6class.Cerebellar_vol','in6')]),  #merge PVE volumes into a single list
                (PVE_Merge6, PVE_to_subject6,[('out','atlas_vol')]),     #connect the PVE list as input
                (Structural_Physio, PVE_to_subject6,[('cmind_normalize.reg_struct_file','reg_struct')]),     #connect the PVE list as input
                (PVE_to_subject6, PVE_Split6,[('Atlas_out','inlist')]),  #split the output list back into separate PVEs
            ])                                    
        else:
            seg_Nclasses=Structural_Physio.get_node('cmind_segment').inputs.Nclasses
            PVE_to_subject.inputs.output_prefix=['WMpve','GMpve','CSFpve']
            PVE_Merge = Node(name='PVE_Merge', interface=Merge(seg_Nclasses))   
            Physio_workflow.add_nodes([PVE_Merge,])
            PVE_Split.inputs.splits=[1,]*seg_Nclasses
            Physio_workflow.connect([
                (Structural_Physio, PVE_Merge,[('cmind_segment.WM_vol','in1'),
                                               ('cmind_segment.GM_vol','in2'),
                                               ('cmind_segment.CSF_vol','in3')]),  #merge PVE volumes into a single list
                (Structural_Physio, PVE_to_subject,[('cmind_normalize.reg_struct_file','reg_struct')]),     #connect the PVE list as input                              
                                    ])
            
        Physio_workflow.connect([
            (PVE_Merge, PVE_to_subject,[('out','atlas_vol')]),     #connect the PVE list as input
            (PVE_to_subject, PVE_Split,[('Atlas_out','inlist')]),  #split the output list back into separate PVEs
            ])
        
    #connect Structural and CBF workflows
    if has_BaselineCBF and subj_data['Anatomical']:
        
        Physio_workflow.connect([ 
            (Structural_Physio,CBF_workflow,[
                 ('cmind_normalize.reg_struct_file','cmind_apply_normalization_CBF.reg_struct'),
                 
                 ('cmind_normalize.reg_struct_file','cmind_fieldmap_regBBR.reg_struct_file'),
                 ('cmind_struct_preprocess.fname_head','cmind_fieldmap_regBBR.T1_vol'),
                 ('cmind_struct_preprocess.fname_brain','cmind_fieldmap_regBBR.T1_brain_vol'),
                 ('cmind_segment.WM_vol','cmind_fieldmap_regBBR.WMseg_vol'),
    
                 ('cmind_segment.WM_vol',CBF_quant_node_name+'.WMpve')]),
        ])          
        
        if subj_data['BaselineCBF']['use_fieldmap']:
            Physio_workflow.connect([ 
                (Structural_Physio,CBF_workflow,[
                     ('cmind_struct_preprocess.fname_head','cmind_fieldmap_process.T1_head_vol'),
                     ('cmind_struct_preprocess.fname_brain','cmind_fieldmap_process.T1_brain_vol')]),
            ])            
   
    if add_nofmap_regBBR and has_BaselineCBF and subj_data['BaselineCBF']['use_fieldmap']:
        Physio_workflow.connect([ 
            (Structural_Physio,CBF_workflow,[
                 ('cmind_normalize.reg_struct_file','cmind_fieldmap_regBBR_nofieldmap.reg_struct_file'),
                 ('cmind_struct_preprocess.fname_head','cmind_fieldmap_regBBR_nofieldmap.T1_vol'),
                 ('cmind_struct_preprocess.fname_brain','cmind_fieldmap_regBBR_nofieldmap.T1_brain_vol'),
                 ('cmind_segment.WM_vol','cmind_fieldmap_regBBR_nofieldmap.WMseg_vol'),])
    
        ])       
                 
    #Physio_workflow.add_nodes() 
    #connect Structural and T1mapping normalization workflows
    if has_T1map:
        
        cmind_apply_normalization_T1map=cmind_apply_normalization.clone('cmind_apply_normalization_T1map')
        cmind_apply_normalization_T1map.base_dir=output_base_dir
        
        cmind_T1_tissue_averages=cmind_T1_tissue_averages.clone('TissueAverages')
        cmind_T1_tissue_averages.base_dir=output_base_dir
        cmind_T1_tissue_averages.inputs.PVEthresh_pct = 66.0 #PVE threshold to use

        Physio_workflow.add_nodes([cmind_apply_normalization_T1map,
                                   cmind_T1_tissue_averages])
                                   
        Physio_workflow.connect([ 
            (Structural_Physio,cmind_T1_tissue_averages,[('cmind_segment.GM_vol','GMpve'),
                                                         ('cmind_segment.WM_vol','WMpve'),
                                                         ('cmind_segment.CSF_vol','CSFpve')]),

            (Structural_Physio,cmind_apply_normalization_T1map,[('cmind_normalize.reg_struct_file','reg_struct')]),

        ])     
        
        if atropos_priors_path:
            cmind_T1_tissue_averages_6class=cmind_T1_tissue_averages.clone('TissueAverages_6class')
            Physio_workflow.add_nodes([cmind_T1_tissue_averages_6class,])
                                       
            Physio_workflow.connect([ 
                (Structural_Physio,cmind_T1_tissue_averages_6class,[('cmind_segment_atropos_6class.GM_vol','GMpve'),
                                                             ('cmind_segment_atropos_6class.WM_vol','WMpve'),
                                                             ('cmind_segment_atropos_6class.CSF_vol','CSFpve')]),
    
    
            ])     
                     
            
#        Physio_workflow.add_nodes([cmind_T1_tissue_averages,
#                                   cmind_apply_normalization_T1map])

        #connect structural and T1map workflows
        #T1map_workflow.connect([ 
        #    (Structural_Physio,cmind_apply_normalization_T1map,[('cmind_normalize.reg_struct_file','reg_struct')]),
        #])           
        if not has_BaselineCBF: #If no baselineCBF register directly from T1map without the coregistration to BaselineCBF
            Physio_workflow.connect([                
                (T1map_workflow, cmind_apply_normalization_T1map, [('cmind_T1map_fit.T1_map_fname', 'input_name')]),
                (T1map_workflow, cmind_T1_tissue_averages, [('cmind_T1map_fit.T1_map_fname','T1_map_file'),
                                                            ('cmind_T1map_fit.T1_NRMSE_map_fname', 'NRMSE_file')]),
            ])    
            if atropos_priors_path:
                Physio_workflow.connect([                
                    (T1map_workflow, cmind_T1_tissue_averages_6class, [('cmind_T1map_fit.T1_map_fname','T1_map_file'),
                                                                       ('cmind_T1map_fit.T1_NRMSE_map_fname', 'NRMSE_file')]),
                ])    
            if subj_data['T1Est']['use_fieldmap']:

                 Physio_workflow.connect([                  
                    (cmind_fieldmap_regBBR_T1, cmind_apply_normalization_T1map, [('LRtoHR_Warp_fname', 'LRtoHR_warp')]),
                    (cmind_fieldmap_regBBR_T1, cmind_T1_tissue_averages, [('HRtoLR_Warp_fname', 'HRtoCBF_warp')]),
                 ])    
                 if atropos_priors_path:
                      Physio_workflow.connect([                  
                        (cmind_fieldmap_regBBR_T1, cmind_T1_tissue_averages_6class, [('HRtoLR_Warp_fname', 'HRtoCBF_warp')]),
                     ])    
                 
            else:
                 Physio_workflow.connect([                  
                    (cmind_fieldmap_regBBR_T1, cmind_apply_normalization_T1map, [('LRtoHR_Affine_fname', 'LRtoHR_affine')]),
                    (cmind_fieldmap_regBBR_T1, cmind_T1_tissue_averages, [('HRtoLR_Affine_fname', 'HRtoCBF_affine')]),
                 ])    
                 if atropos_priors_path:
                      Physio_workflow.connect([                  
                        (cmind_fieldmap_regBBR_T1, cmind_T1_tissue_averages_6class, [('HRtoLR_Affine_fname', 'HRtoCBF_affine')]),
                     ])    
                                  
        
        else:
            #connect T1mapping and BaselineCBF workflows
            Physio_workflow.connect([                      
              (T1map_workflow,CBF_workflow,[('cmind_T1map_preprocess.T1concat_mean_N4_brain','cmind_baselineCBF_coregisterT1.T1concat_N4_brain_nii'),
                                            ('cmind_T1map_fit.T1_map_fname','cmind_baselineCBF_coregisterT1.T1_map_nii'),
                                            ('cmind_T1map_fit.I0_map_fname','cmind_baselineCBF_coregisterT1.I0_map_nii'),
                                            ('cmind_T1map_fit.T1_NRMSE_map_fname','cmind_baselineCBF_coregisterT1.T1_NRMSE_map_nii')])
            ])
    
            #normalize T1map
            #TODO: connected from T1map_workflow to CBF_workflow above so cannot now go in the opposite direction or graph is not "acyclic"
            Physio_workflow.connect([                
                (CBF_workflow, cmind_apply_normalization_T1map, [('cmind_baselineCBF_coregisterT1.T1map_reg2CBF', 'input_name')]),
                (CBF_workflow, cmind_T1_tissue_averages, [('cmind_baselineCBF_coregisterT1.T1map_reg2CBF', 'T1_map_file'),
                                                          ('cmind_baselineCBF_coregisterT1.T1_NRMSE_map_reg2CBF', 'NRMSE_file'),
                                                          (CBF_quant_node_name+'.CBF_Wang_file', 'CBF_quant_file'),
                                                          ('cmind_baselineCBF_preprocess.BaselineCBF_mcf_percent_change', 'CBF_percent_file')]),
    
            ])    
            
            if atropos_priors_path:
                Physio_workflow.connect([                
                    (CBF_workflow, cmind_T1_tissue_averages_6class, [('cmind_baselineCBF_coregisterT1.T1map_reg2CBF', 'T1_map_file'),
                                                              ('cmind_baselineCBF_coregisterT1.T1_NRMSE_map_reg2CBF', 'NRMSE_file'),
                                                              (CBF_quant_node_name+'.CBF_Wang_file', 'CBF_quant_file'),
                                                              ('cmind_baselineCBF_preprocess.BaselineCBF_mcf_percent_change', 'CBF_percent_file')]),
        
                ])    
                
            #connect either Affine or fieldmap warp
            if subj_data['BaselineCBF']['use_fieldmap']:
                Physio_workflow.connect([                
                    (CBF_workflow, cmind_apply_normalization_T1map, [('cmind_fieldmap_regBBR.LRtoHR_Warp_fname', 'LRtoHR_warp')]),
                    (CBF_workflow, cmind_T1_tissue_averages, [('cmind_fieldmap_regBBR.HRtoLR_Warp_fname', 'HRtoCBF_warp')]),
                ])    
                if atropos_priors_path:
                    Physio_workflow.connect([                
                        (CBF_workflow, cmind_T1_tissue_averages_6class, [('cmind_fieldmap_regBBR.HRtoLR_Warp_fname', 'HRtoCBF_warp')]),
                    ])    
            else:
                Physio_workflow.connect([                
                    (CBF_workflow, cmind_apply_normalization_T1map, [('cmind_fieldmap_regBBR.LRtoHR_Affine_fname', 'LRtoHR_affine')]),
                    (CBF_workflow, cmind_T1_tissue_averages, [('cmind_fieldmap_regBBR.HRtoLR_Affine_fname', 'HRtoCBF_affine')]),
                ])    
                if atropos_priors_path:
                    Physio_workflow.connect([                
                        (CBF_workflow, cmind_T1_tissue_averages_6class, [('cmind_fieldmap_regBBR.HRtoLR_Affine_fname', 'HRtoCBF_affine')]),
                    ])    


    if AAL_to_SubjectSpace and (has_BaselineCBF or has_T1map):
       
        Physio_workflow.connect([ 
            (Structural_Physio,AAL_to_subject,[
                ('cmind_normalize.reg_struct_file','reg_struct')]),
            ])          
            
        if has_BaselineCBF or has_T1map:
            Physio_workflow.connect([ 
                (PVE_to_subject,AAL_ROI_3seg_averages,[('out1','WMpve'),
                                                  ('out2','GMpve'),
                                                  ('out3','CSFpve')]),
 
                (AAL_to_subject,AAL_ROI_3seg_averages,[('Atlas_out','atlas_vol')]),
 
                (PVE_to_subject6,AAL_ROI_6seg_averages,[('out1','WMpve'),
                                                  ('out2','GMpve'),
                                                  ('out3','CSFpve'),
                                                  ('out4','DeepGMpve')]),

                (AAL_to_subject,AAL_ROI_6seg_averages,[('Atlas_out','atlas_vol')]),

            ])            
            
        if has_BaselineCBF:       #BaselineCBF case will also have T1maps in this same space                 

            Physio_workflow.connect([ 
                (CBF_workflow,AAL_to_subject,[
                    ('cmind_baselineCBF_preprocess.BaselineCBF_Meansub','ref_vol')]),

                #order must match PVE_to_subject6.inputs.output_prefix  
                #   ['WMpve', 'GMpve', 'CSFpve', 'DeepGMpve', 'Brainstempve', 'Cerebellarpve']

                (CBF_workflow,AAL_ROI_3seg_averages,[
                            (CBF_quant_node_name+'.CBF_Wang_alphaconst_file','CBF_quant_vol')]),
                
                (CBF_workflow,AAL_ROI_6seg_averages,[
                            (CBF_quant_node_name+'.CBF_Wang_alphaconst_file','CBF_quant_vol')]),

                ])  
                
            if has_T1map:  #use coregistered T1map during ROI averaging
                Physio_workflow.connect([ 
                    (CBF_workflow,AAL_ROI_3seg_averages,[
                                ('cmind_baselineCBF_coregisterT1.T1map_reg2CBF','T1_quant_vol')]),
                    
                    (CBF_workflow,AAL_ROI_6seg_averages,[
                                ('cmind_baselineCBF_coregisterT1.T1map_reg2CBF','T1_quant_vol')]),
                ]) 
                
            #connect either Affine or fieldmap warp
            if subj_data['BaselineCBF']['use_fieldmap']:
                Physio_workflow.connect([                
                    (CBF_workflow, AAL_to_subject, [
                        ('cmind_fieldmap_regBBR.HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                ])    
            else:
                Physio_workflow.connect([                
                    (CBF_workflow, AAL_to_subject, [
                        ('cmind_fieldmap_regBBR.HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                ])     
        elif has_T1map:
            Physio_workflow.connect([ 
                (T1map_workflow,AAL_to_subject,[
                    ('cmind_T1map_fit.T1_map_fname','ref_vol')]),

                (T1map_workflow,AAL_ROI_3seg_averages,[
                            ('cmind_T1map_fit.T1_map_fname','T1_quant_vol')]),
                
                (T1map_workflow,AAL_ROI_6seg_averages,[
                            ('cmind_T1map_fit.T1_map_fname','T1_quant_vol')]),
               ])    
            #connect either Affine or fieldmap warp
            if subj_data['T1Est'] and subj_data['T1Est']['use_fieldmap']:
                Physio_workflow.connect([                
                    (cmind_fieldmap_regBBR_T1, AAL_to_subject, [
                        ('HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                ])    
            else:
                Physio_workflow.connect([                
                    (cmind_fieldmap_regBBR_T1, AAL_to_subject, [
                        ('HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                ])   
                
    if Mindboggle_to_SubjectSpace and (has_BaselineCBF or has_T1map):
        
        Physio_workflow.connect([ 
            (Structural_Physio,Mindboggle_to_subject,[
                ('cmind_normalize.reg_struct_file','reg_struct')]),
            ])          

        if has_BaselineCBF or has_T1map:
            Physio_workflow.connect([ 
                (PVE_to_subject,Mindboggle_ROI_3seg_averages,[('out1','WMpve'),
                                                  ('out2','GMpve'),
                                                  ('out3','CSFpve')]),

                (Mindboggle_to_subject,Mindboggle_ROI_3seg_averages,[('Atlas_out','atlas_vol')]),

                (PVE_to_subject6,Mindboggle_ROI_6seg_averages,[('out1','WMpve'),
                                                  ('out2','GMpve'),
                                                  ('out3','CSFpve'),
                                                  ('out4','DeepGMpve')]),

                (Mindboggle_to_subject,Mindboggle_ROI_6seg_averages,[('Atlas_out','atlas_vol')]),

            ])            
            
        if subj_data['BaselineCBF']:       #BaselineCBF case will also have T1maps in this same space                 

            Physio_workflow.connect([ 
                (CBF_workflow,Mindboggle_to_subject,[
                    ('cmind_baselineCBF_preprocess.BaselineCBF_Meansub','ref_vol')]),

                #order must match PVE_to_subject6.inputs.output_prefix  
                #   ['WMpve', 'GMpve', 'CSFpve', 'DeepGMpve', 'Brainstempve', 'Cerebellarpve']

                (CBF_workflow,Mindboggle_ROI_3seg_averages,[
                            (CBF_quant_node_name+'.CBF_Wang_alphaconst_file','CBF_quant_vol')]),
                
                (CBF_workflow,Mindboggle_ROI_6seg_averages,[
                            (CBF_quant_node_name+'.CBF_Wang_alphaconst_file','CBF_quant_vol')]),

                ])    
                
            if has_T1map:  #use coregistered T1map during ROI averaging
                Physio_workflow.connect([ 
                    (CBF_workflow,Mindboggle_ROI_3seg_averages,[
                                ('cmind_baselineCBF_coregisterT1.T1map_reg2CBF','T1_quant_vol')]),
                    
                    (CBF_workflow,Mindboggle_ROI_6seg_averages,[
                                ('cmind_baselineCBF_coregisterT1.T1map_reg2CBF','T1_quant_vol')]),
                ]) 

           #connect either Affine or fieldmap warp
            if subj_data['BaselineCBF']['use_fieldmap']:
                Physio_workflow.connect([                
                    (CBF_workflow, Mindboggle_to_subject, [
                        ('cmind_fieldmap_regBBR.HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                ])    
            else:
                Physio_workflow.connect([                
                    (CBF_workflow, Mindboggle_to_subject, [
                        ('cmind_fieldmap_regBBR.HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                ])     
        elif has_T1map:
            Physio_workflow.connect([ 
                (T1map_workflow,Mindboggle_to_subject,[
                    ('cmind_T1map_fit.T1_map_fname','ref_vol')]),
                (T1map_workflow,Mindboggle_ROI_3seg_averages,[
                            ('cmind_T1map_fit.T1_map_fname','T1_quant_vol')]),
                
                (T1map_workflow,Mindboggle_ROI_6seg_averages,[
                            ('cmind_T1map_fit.T1_map_fname','T1_quant_vol')]),
               ])    
            #connect either Affine or fieldmap warp
            if subj_data['T1Est'] and subj_data['T1Est']['use_fieldmap']:
                Physio_workflow.connect([                
                    (cmind_fieldmap_regBBR_T1, Mindboggle_to_subject, [
                        ('HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                ])    
            else:
                Physio_workflow.connect([                
                    (cmind_fieldmap_regBBR_T1, Mindboggle_to_subject, [
                        ('HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                ])   
                
    if JHU_to_SubjectSpace and (has_BaselineCBF or has_T1map):
        
        Physio_workflow.connect([ 
            (Structural_Physio,JHU_to_subject,[
                ('cmind_normalize.reg_struct_file','reg_struct')]),
            ])          

        if has_BaselineCBF or has_T1map:
            Physio_workflow.connect([ 
                (PVE_to_subject,JHU_ROI_3seg_averages,[('out1','WMpve'),
                                                  ('out2','GMpve'),
                                                  ('out3','CSFpve')]),

                (JHU_to_subject,JHU_ROI_3seg_averages,[('Atlas_out','atlas_vol')]),

                (PVE_to_subject6,JHU_ROI_6seg_averages,[('out1','WMpve'),
                                                  ('out2','GMpve'),
                                                  ('out3','CSFpve'),
                                                  ('out4','DeepGMpve')]),

                (JHU_to_subject,JHU_ROI_6seg_averages,[('Atlas_out','atlas_vol')]),

            ])            
            
        if subj_data['BaselineCBF']:       #BaselineCBF case will also have T1maps in this same space                 

            Physio_workflow.connect([ 
                (CBF_workflow,JHU_to_subject,[
                    ('cmind_baselineCBF_preprocess.BaselineCBF_Meansub','ref_vol')]),

                #order must match PVE_to_subject6.inputs.output_prefix  
                #   ['WMpve', 'GMpve', 'CSFpve', 'DeepGMpve', 'Brainstempve', 'Cerebellarpve']

                (CBF_workflow,JHU_ROI_3seg_averages,[
                            (CBF_quant_node_name+'.CBF_Wang_alphaconst_file','CBF_quant_vol')]),
                
                (CBF_workflow,JHU_ROI_6seg_averages,[
                            (CBF_quant_node_name+'.CBF_Wang_alphaconst_file','CBF_quant_vol')]),

                ])    
                
            if has_T1map:  #use coregistered T1map during ROI averaging
                Physio_workflow.connect([ 
                    (CBF_workflow,JHU_ROI_3seg_averages,[
                                ('cmind_baselineCBF_coregisterT1.T1map_reg2CBF','T1_quant_vol')]),
                    
                    (CBF_workflow,JHU_ROI_6seg_averages,[
                                ('cmind_baselineCBF_coregisterT1.T1map_reg2CBF','T1_quant_vol')]),
                ]) 

           #connect either Affine or fieldmap warp
            if subj_data['BaselineCBF']['use_fieldmap']:
                Physio_workflow.connect([                
                    (CBF_workflow, JHU_to_subject, [
                        ('cmind_fieldmap_regBBR.HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                ])    
            else:
                Physio_workflow.connect([                
                    (CBF_workflow, JHU_to_subject, [
                        ('cmind_fieldmap_regBBR.HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                ])     
        elif has_T1map:
            Physio_workflow.connect([ 
                (T1map_workflow,JHU_to_subject,[
                    ('cmind_T1map_fit.T1_map_fname','ref_vol')]),
                (T1map_workflow,JHU_ROI_3seg_averages,[
                            ('cmind_T1map_fit.T1_map_fname','T1_quant_vol')]),
                
                (T1map_workflow,JHU_ROI_6seg_averages,[
                            ('cmind_T1map_fit.T1_map_fname','T1_quant_vol')]),
               ])    
            #connect either Affine or fieldmap warp
            if subj_data['T1Est'] and subj_data['T1Est']['use_fieldmap']:
                Physio_workflow.connect([                
                    (cmind_fieldmap_regBBR_T1, JHU_to_subject, [
                        ('HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                ])    
            else:
                Physio_workflow.connect([                
                    (cmind_fieldmap_regBBR_T1, JHU_to_subject, [
                        ('HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                ])   
                
               
    if PVE_to_SubjectSpace and (has_BaselineCBF or has_T1map):
#        Physio_workflow.connect([ 
#            (Structural_Physio,PVE_to_subject,[
#                ('cmind_normalize.reg_struct_file','reg_struct')]),
#            ])          
        if subj_data['BaselineCBF']:       #BaselineCBF case will also have T1maps in this same space                 

            Physio_workflow.connect([ 
                (CBF_workflow,PVE_to_subject,[
                    ('cmind_baselineCBF_preprocess.BaselineCBF_Meansub','ref_vol')]),
                ])              
            #connect either Affine or fieldmap warp
            if subj_data['BaselineCBF']['use_fieldmap']:
                Physio_workflow.connect([                
                    (CBF_workflow, PVE_to_subject, [
                        ('cmind_fieldmap_regBBR.HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                ])    
            else:
                Physio_workflow.connect([                
                    (CBF_workflow, PVE_to_subject, [
                        ('cmind_fieldmap_regBBR.HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                ])     
                
            if atropos_priors_path:
                Physio_workflow.connect([ 
                    (CBF_workflow,PVE_to_subject6,[
                        ('cmind_baselineCBF_preprocess.BaselineCBF_Meansub','ref_vol')]),
                    ])              
                #connect either Affine or fieldmap warp
                if subj_data['BaselineCBF']['use_fieldmap']:
                    Physio_workflow.connect([                
                        (CBF_workflow, PVE_to_subject6, [
                            ('cmind_fieldmap_regBBR.HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                    ])    
                else:
                    Physio_workflow.connect([                
                        (CBF_workflow, PVE_to_subject6, [
                            ('cmind_fieldmap_regBBR.HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                    ])     
                
        elif has_T1map:
            Physio_workflow.connect([ 
                (T1map_workflow,PVE_to_subject,[
                    ('cmind_T1map_fit.T1_map_fname','ref_vol')]),
                ])    
            #connect either Affine or fieldmap warp
            if subj_data['T1Est']['use_fieldmap']:
                Physio_workflow.connect([                
                    (cmind_fieldmap_regBBR_T1, PVE_to_subject, [
                        ('HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                ])    
            else:
                Physio_workflow.connect([                
                    (cmind_fieldmap_regBBR_T1, PVE_to_subject, [
                        ('HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                ])                   
            if atropos_priors_path:
                Physio_workflow.connect([ 
                    (T1map_workflow,PVE_to_subject6,[
                        ('cmind_T1map_fit.T1_map_fname','ref_vol')]),
                    ])    
                #connect either Affine or fieldmap warp
                if subj_data['T1Est']['use_fieldmap']:
                    Physio_workflow.connect([                
                        (cmind_fieldmap_regBBR_T1, PVE_to_subject6, [
                            ('HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                    ])    
                else:
                    Physio_workflow.connect([                
                        (cmind_fieldmap_regBBR_T1, PVE_to_subject6, [
                            ('HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                    ])                   
    
    if add_diffusion_workflow:
        if has_HARDI:
            Diffusion_workflow = generate_Diffusion_workflow(output_base_dir,
                                        subj_data,
                                        name=None,
                                        UCLA_flag=UCLA_flag,
                                        QA_only = True,
                                        case = 'HARDI',
                                        cmind_fieldmap_process = cmind_fieldmap_process,
                                        )
            Physio_workflow.add_nodes([Diffusion_workflow,])

        if has_DTI:
            Diffusion_workflow2 = generate_Diffusion_workflow(output_base_dir,
                                        subj_data,
                                        name=None,
                                        UCLA_flag=UCLA_flag,
                                        QA_only = True,
                                        case = 'DTI',
                                        cmind_fieldmap_process = cmind_fieldmap_process,
                                        )            
            Physio_workflow.add_nodes([Diffusion_workflow2,])
            
    Physio_workflow.config['execution']['remove_unnecessary_outputs']=False    
    Physio_workflow=Physio_workflow.clone(name=name)
    Physio_workflow.write_graph(dotfilename='Physio_hierarchical.dot',graph2use='hierarchical',format='png') #'flat')   
    Physio_workflow.write_graph(dotfilename='Physio.dot',graph2use='flat',format='png') #'flat')   
    return Physio_workflow                                
                  

def generate_Diffusion_workflow(output_base_dir,
                                subj_data,
                                name=None,
                                UCLA_flag=False,
                                QA_only = True,
                                case = 'HARDI',
                                cmind_fieldmap_process = None,
                                ):
                                    
    """ DTI or HARDI workflow """
#    from cmind.globals import (cmind_DTIPrep, cmind_Slicer3D_cli_dir, 
#                               cmind_Slicer3D_env, cmind_Slicer3D_libs)
                               
    from cmind.nipype.cmind_nodes import (cmind_fieldmap_regBBR_HARDI,
                                          cmind_fieldmap_regBBR_DTI,
                                          cmind_HARDI_QA, cmind_DTI_QA)

    if case == 'HARDI':
        QA_node = cmind_HARDI_QA
        regBBR_node = cmind_fieldmap_regBBR_HARDI
        if not subj_data['HARDI']:
            raise ValueError("HARDI data not found")

    elif case == 'DTI':
        QA_node = cmind_DTI_QA
        regBBR_node = cmind_fieldmap_regBBR_DTI
        if not subj_data['DTI']:
            raise ValueError("DTI data not found")
    else:
        raise ValueError("Unsupported case: {}".format(case))
                       
    Diffusion_workflow = pe.Workflow(name='Diffusion_tempname',
                                     base_dir=output_base_dir)
    if name is None:
        name = case
        
    QA_node.base_dir=output_base_dir
    QA_node.inputs.DTI_input_source = subj_data['HARDI']['files']
    if UCLA_flag:
        QA_node.inputs.isSiemens = True
    else:
        QA_node.inputs.isSiemens = False
    Diffusion_workflow.add_nodes([QA_node,])            
    
    if not QA_only:
        if subj_data[case]['use_fieldmap']:
            cmind_fieldmap_process, regBBR_node, Diffusion_workflow = init_fieldmap_nodes(output_base_dir,
                   subj_data,
                   fieldmap_case='HARDI',
                   cmind_fieldmap_process = cmind_fieldmap_process,  
                   cmind_fieldmap_regBBR = regBBR_node,
                   workflow=Diffusion_workflow,
                   )
        else: 
            #may not be necessary to explicitly set these if default is None
            regBBR_node.inputs.Fieldmap_vol_processed=None
            regBBR_node.inputs.Fieldmap_vol_unmasked=None
            regBBR_node.inputs.fieldmap2struct_affine=None            
        
    Diffusion_workflow.config['execution']['remove_unnecessary_outputs']=False    
    Diffusion_workflow=Diffusion_workflow.clone(name=name)
    Diffusion_workflow.write_graph(dotfilename='Diffusion_hierarchical.dot',graph2use='hierarchical',format='png') #'flat')   
    Diffusion_workflow.write_graph(dotfilename='Diffusion.dot',graph2use='flat',format='png') #'flat')   
    return Diffusion_workflow                    
        

    
def generate_ASLBOLD_workflow(output_base_dir,
                              age_months,
                              subj_data,
                              paradigm,
                              UCLA_flag=False,
                              confounds_to_use='best_confound_file',
                              name=None,
                              run_FWEpoststats=True, 
                              build_feat_dirs=True, 
                              highpass_freqs=[104,],
                              smoothings_mm=[0,8],
                              cluster_z_thresh=2.3,
                              prob_thresh=0.05,
                              reuse_same=False,
                              ASL_FSF_file=None,  
                              BOLD_FSF_file=None,
                              ASL_unfiltered_design_matrix=None,
                              BOLD_unfiltered_design_matrix=None,
                              verify_CMIND_filenames=True,
                              ASL_contrast_of_interest=0,
                              BOLD_contrast_of_interest=1,
                              BOLD_only=False):
                                  
    """Generate an ASLBOLD fMRI workflow.  will need to be connected to a Structural workflow
    
    """
    from cmind.globals import cmind_ASLBOLD_dir
    
    from cmind.nipype.cmind_nodes import (cmind_fieldmap_process,
                                          cmind_fieldmap_regBBR_ASLBOLD,
                                          cmind_fMRI_preprocess,
                                          cmind_fMRI_preprocess2_ASL,
                                          cmind_fMRI_preprocess2_BOLD,
                                          cmind_fMRI_outliers_ASL,
                                          cmind_fMRI_outliers_BOLD,
                                          cmind_fMRI_level1_stats_ASL,
                                          cmind_fMRI_level1_stats_BOLD,
                                          cmind_fMRI_level1_poststats_ASL,
                                          cmind_fMRI_level1_poststats_BOLD,
                                          cmind_fMRI_norm_ASL,
                                          cmind_fMRI_norm_BOLD,
                                          cmind_build_feat_dir) 
    
    #capitalize only the first letter of the paradigm to match filename
    #conventions
    paradigm_cap = paradigm[0].upper()+paradigm[1:].lower()

    if is_string_like(confounds_to_use):
            confounds_to_use = [confounds_to_use,]

    for cidx,confound in enumerate(confounds_to_use):

        #convert alternate shorthand names to the full names
        if confound.lower()=='best':
            confounds_to_use[cidx]='best_confound_file'
        elif confound.lower()=='default':
            confounds_to_use[cidx]='default_confound_file'
        elif confound.lower()=='motion_only':
            confounds_to_use[cidx]='mcf06_confound_file'
        elif confound.lower()=='outliers_only':
            confounds_to_use[cidx]='FSL_outliers_txt'      
    
        confound = confounds_to_use[cidx]
        if not confound in ['default_confound_file',
                                   'best_confound_file',
                                   'mcf06_confound_file',
                                   'FSL_outliers_txt']:
           raise ValueError("Invalid confound_to_use: {}".format(confound))
           
    confound_to_use = confounds_to_use[0] #TODO: support addtional items in list
    if name is None:
        name='CMIND_ASLBOLD_'+paradigm_cap
#        if confound_to_use=='default_confound_file':
#            name += '_default' 
#        elif confound_to_use=='best_confound_file':
#            name += '_best' 
#        elif confound_to_use=='mcf06_confound_file':
#            name += '_mcf06only' 
#        elif confound_to_use=='FSL_outliers_txt':
#            name += '_FSLoutliers' 
    #Find the Feat .fsf files appropriate for this paradigm and subject
    if BOLD_FSF_file is None:
        BOLD_FSF_file=pjoin(cmind_ASLBOLD_dir,'BOLD_level1_'+paradigm_cap+'.fsf')
        if UCLA_flag: #UCLA subjects have opposite tag/control order so use the appropriate .fsf
            BOLD_FSF_file=BOLD_FSF_file.replace('.fsf','_UCLA.fsf')
    if not os.path.exists(BOLD_FSF_file):
        raise ValueError("Could not find the Feat .fsf file for BOLD in the expected location")

    if BOLD_unfiltered_design_matrix is None:
        BOLD_unfiltered_design_matrix=pjoin(cmind_ASLBOLD_dir,'unfilt_BOLD_'+paradigm_cap+'.mat')
        if UCLA_flag: #UCLA subjects have opposite tag/control order so use the appropriate .fsf
            BOLD_unfiltered_design_matrix=BOLD_unfiltered_design_matrix.replace('.mat','_UCLA.mat')

    if not os.path.exists(BOLD_unfiltered_design_matrix):
        raise ValueError("Could not find the unfiltered design matrix file for BOLD in the expected location")

    if not BOLD_only:
        if ASL_FSF_file is None:
            ASL_FSF_file=pjoin(cmind_ASLBOLD_dir,'ASL_level1_'+paradigm_cap+'.fsf')
            if UCLA_flag: #UCLA subjects have opposite tag/control order so use the appropriate .fsf
                ASL_FSF_file=ASL_FSF_file.replace('.fsf','_UCLA.fsf')        
        if not os.path.exists(ASL_FSF_file):
            raise ValueError("Could not find the Feat .fsf file for ASL in the expected location")
    
        if ASL_unfiltered_design_matrix is None:
            ASL_unfiltered_design_matrix=pjoin(cmind_ASLBOLD_dir,'unfilt_ASL_'+paradigm_cap+'.mat')
            if UCLA_flag: #UCLA subjects have opposite tag/control order so use the appropriate .fsf
                ASL_unfiltered_design_matrix=ASL_unfiltered_design_matrix.replace('.mat','_UCLA.mat')
                
        if not os.path.exists(ASL_unfiltered_design_matrix):
            raise ValueError("Could not find the unfiltered design matrix file for ASL in the expected location")

    cmind_fMRI_preprocess.base_dir=output_base_dir
    cmind_fMRI_preprocess.inputs.paradigm_name=paradigm_cap
    
    func_files=subj_data[('Functional',paradigm)]['files']
    
    ASL_vol, BOLD_vol = _parse_func_files(func_files,reuse_same=reuse_same,BOLD_only=BOLD_only)
    
    cmind_fMRI_preprocess.inputs.BOLD_vol=BOLD_vol

    if BOLD_only:
        cmind_fMRI_preprocess.inputs.ASL_vol='' #None        
    else:
        cmind_fMRI_preprocess.inputs.ASL_vol=ASL_vol
#        if not cmind_fMRI_preprocess.inputs.ASL_vol:
#            cmind_fMRI_preprocess.inputs.ASL_vol=None

    if not cmind_fMRI_preprocess.inputs.BOLD_vol:
        cmind_fMRI_preprocess.inputs.BOLD_vol=None
    
    #make sure the filenames match what is expected
    if verify_CMIND_filenames:
        if cmind_fMRI_preprocess.inputs.BOLD_vol.find('BOLD.nii')==-1:
            raise ValueError("Unexpected filename for BOLD_vol")
        if not BOLD_only:
            if cmind_fMRI_preprocess.inputs.ASL_vol.find('ASL.nii')==-1:
                raise ValueError("Unexpected filename for ASL_vol")

    if isinstance(highpass_freqs,(int,float)):
        highpass_freqs=[highpass_freqs,]
        

    cmind_fMRI_preprocess2_BOLD.base_dir=output_base_dir
    cmind_fMRI_preprocess2_BOLD.inputs.preprocess_case = ''  #TODO: remove 
    cmind_fMRI_preprocess2_BOLD.inputs.output_tar = True
    cmind_fMRI_preprocess2_ASL.inputs.brain_thresh = 10
    cmind_fMRI_preprocess2_BOLD.iterables=[("smooth",smoothings_mm),
                                           ('paradigm_hp',highpass_freqs)]

    cmind_fMRI_outliers_BOLD.base_dir=output_base_dir
    cmind_fMRI_outliers_BOLD.inputs.FSF_template=BOLD_FSF_file
    cmind_fMRI_outliers_BOLD.inputs.unfiltered_design_matrix=BOLD_unfiltered_design_matrix
    cmind_fMRI_outliers_BOLD.inputs.contrast_of_interest=BOLD_contrast_of_interest #column of BOLD_unfiltered_design_matrix that is the primary contrast of interest
    cmind_fMRI_outliers_BOLD.inputs.Nproc_max=1 #16
    
    cmind_fMRI_level1_stats_BOLD.base_dir=output_base_dir
    cmind_fMRI_level1_stats_BOLD.inputs.FSF_template=BOLD_FSF_file
    cmind_fMRI_level1_stats_BOLD.inputs.output_tar = True
    cmind_fMRI_level1_stats_BOLD.inputs.lvl1_stats_case='FEAT_BOLD_' + paradigm_cap

    cmind_fMRI_level1_poststats_BOLD.base_dir=output_base_dir
    cmind_fMRI_level1_poststats_BOLD.inputs.lvl1_stats_case='FEAT_BOLD_' + paradigm_cap
    cmind_fMRI_level1_poststats_BOLD.inputs.z_thresh = cluster_z_thresh
    cmind_fMRI_level1_poststats_BOLD.inputs.prob_thresh = prob_thresh
    cmind_fMRI_level1_poststats_BOLD.inputs.thresh = 'cluster'
    cmind_fMRI_level1_poststats_BOLD.inputs.output_tar = True
    
    cmind_fMRI_norm_BOLD.base_dir=output_base_dir
    
    if not BOLD_only:
        cmind_fMRI_preprocess2_ASL.base_dir=output_base_dir
        cmind_fMRI_preprocess2_ASL.inputs.preprocess_case = ''  #TODO: remove 
        cmind_fMRI_preprocess2_ASL.inputs.output_tar = True
        cmind_fMRI_preprocess2_ASL.inputs.brain_thresh = 10
        cmind_fMRI_preprocess2_ASL.iterables=[("smooth",smoothings_mm),
                                              ('paradigm_hp',highpass_freqs)]
        
        cmind_fMRI_outliers_ASL.base_dir=output_base_dir
        cmind_fMRI_outliers_ASL.inputs.FSF_template=ASL_FSF_file
        cmind_fMRI_outliers_ASL.inputs.unfiltered_design_matrix=ASL_unfiltered_design_matrix
        cmind_fMRI_outliers_ASL.inputs.contrast_of_interest=ASL_contrast_of_interest #column of ASL_unfiltered_design_matrix that is the primary contrast of interest
        cmind_fMRI_outliers_ASL.inputs.Nproc_max=1 #16
    #    cmind_fMRI_outliers_ASL.inputs.nuisance_file = confound_to_use
    #    cmind_fMRI_outliers_BOLD.inputs.nuisance_file = confound_to_use
        
        cmind_fMRI_level1_stats_ASL.base_dir=output_base_dir
        cmind_fMRI_level1_stats_ASL.inputs.FSF_template=ASL_FSF_file
        cmind_fMRI_level1_stats_ASL.inputs.lvl1_stats_case='FEAT_ASL_' + paradigm_cap
        cmind_fMRI_level1_stats_ASL.inputs.output_tar = True
        
        cmind_fMRI_level1_poststats_ASL.base_dir=output_base_dir
        cmind_fMRI_level1_poststats_ASL.inputs.lvl1_stats_case='FEAT_ASL_' + paradigm_cap
        cmind_fMRI_level1_poststats_ASL.inputs.z_thresh = cluster_z_thresh
        cmind_fMRI_level1_poststats_ASL.inputs.prob_thresh = prob_thresh
        cmind_fMRI_level1_poststats_ASL.inputs.thresh = 'cluster'
        cmind_fMRI_level1_poststats_ASL.inputs.output_tar = True
        
        cmind_fMRI_norm_ASL.base_dir=output_base_dir
    #use clone to modify the name to include the type of nuisance regressors used
    #cmind_fMRI_level1_stats_ASL=cmind_fMRI_level1_stats_ASL.clone(cmind_fMRI_level1_stats_ASL.name+'_'+confound_to_use)
    
    #for confound in confounds_to_use:
    
    

    #use clone to modify the name to include the type of nuisance regressors used
    #cmind_fMRI_level1_stats_BOLD=cmind_fMRI_level1_stats_BOLD.clone(cmind_fMRI_level1_stats_BOLD.name+'_'+confound_to_use)

    if False: #example call to cmindMapNode version of cmind_fMRI_level1_poststats_ASL
        cmind_fMRI_level1_poststats_ASL.base_dir=output_base_dir
        cmind_fMRI_level1_poststats_ASL.inputs.lvl1_stats_case='FEAT_ASL_' + paradigm_cap
        cmind_fMRI_level1_poststats_ASL.inputs.z_thresh = [2.3, 3.09, 2.3]
        cmind_fMRI_level1_poststats_ASL.inputs.prob_thresh = [0.05, 0.05, 0.05]
        cmind_fMRI_level1_poststats_ASL.inputs.thresh = ['cluster','cluster','voxel']
        #use clone to modify the name to include the type of nuisance regressors used
        cmind_fMRI_level1_poststats_ASL = cmind_fMRI_level1_poststats_ASL.clone(cmind_fMRI_level1_poststats_ASL.name+'_'+confound_to_use)
    
    
    

    #use clone to modify the name to include the type of nuisance regressors used
    #cmind_fMRI_level1_poststats_ASL = cmind_fMRI_level1_poststats_ASL.clone(cmind_fMRI_level1_poststats_ASL.name+'_'+confound_to_use + '_cluster')
    

    #use clone to modify the name to include the type of nuisance regressors used
    #cmind_fMRI_level1_poststats_BOLD = cmind_fMRI_level1_poststats_BOLD.clone(cmind_fMRI_level1_poststats_BOLD.name+'_'+confound_to_use + '_cluster')
    

    
    
#    cmind_fMRI_norm_ASL.name=cmind_fMRI_norm_ASL.name+'_'+confound_to_use    
    #cmind_fMRI_norm_ASL = cmind_fMRI_norm_ASL.clone(cmind_fMRI_norm_ASL.name+'_'+confound_to_use)
    
    
#    cmind_fMRI_norm_BOLD.name=cmind_fMRI_norm_BOLD.name+'_'+confound_to_use
    #cmind_fMRI_norm_BOLD = cmind_fMRI_norm_BOLD.clone(cmind_fMRI_norm_BOLD.name+'_'+confound_to_use)

    cmind_fieldmap_regBBR_ASLBOLD.base_dir=output_base_dir


    #confound_nodes=[]
    #confound_node_dict['BOLD_stats']=[]
    #confound_node_dict['ASL_stats']=[]
    
#    BOLD_stats_nodes=[]
#    ASL_stats_nodes=[]
#    BOLD_poststats_nodes=[]
#    ASL_poststats_nodes=[]  
#    BOLD_norm_nodes=[]
#    ASL_norm_nodes=[]
#    for confound in confounds_to_use:
#        ASL_stats_nodes.append(cmind_fMRI_level1_stats_ASL.clone(cmind_fMRI_level1_stats_ASL.name+'_'+confound_to_use))
#        BOLD_stats_nodes.append(cmind_fMRI_level1_stats_BOLD.clone(cmind_fMRI_level1_stats_BOLD.name+'_'+confound_to_use))
#        ASL_poststats_nodes.append(cmind_fMRI_level1_poststats_ASL.clone(cmind_fMRI_level1_poststats_ASL.name+'_'+confound_to_use + '_cluster'))
#        BOLD_poststats_nodes.append(cmind_fMRI_level1_poststats_BOLD.clone(cmind_fMRI_level1_poststats_BOLD.name+'_'+confound_to_use + '_cluster'))
#        ASL_norm_nodes.append(cmind_fMRI_norm_ASL.clone(cmind_fMRI_norm_ASL.name+'_'+confound_to_use))
#        BOLD_norm_nodes.append(cmind_fMRI_norm_BOLD.clone(cmind_fMRI_norm_BOLD.name+'_'+confound_to_use))

    ASLBOLD_workflow = pe.Workflow(name='ASLBOLD_tempname',base_dir=output_base_dir)

#    #cmind_norm has no other 
#    ASLBOLD_workflow.add_nodes([cmind_fMRI_norm_ASL,
#                                cmind_fMRI_norm_BOLD])

    if subj_data[('Functional',paradigm)]['use_fieldmap']:
        
        GREfiles=subj_data['GREMap']['files']
        abs_file=None
        rads_file=None
        for f in GREfiles:
            if ('abs.nii' in f) and abs_file is None:
                abs_file=f
            elif ('rads.nii' in f) and rads_file is None:
                rads_file=f                  
            else:
                raise ValueError("couldn't find abs and rads GREMap files")
        if (abs_file is None) or (rads_file is None):
            raise ValueError("couldn't find abs and rads GREMap files")
            
        #cmind_fieldmap_process.inputs.output_dir=output_dirs['Fieldmap']
        #cmind_fieldmap_process=cmind_fieldmap_process.clone("cmind_fieldmap_process")
        cmind_fieldmap_process.base_dir=output_base_dir        
        cmind_fieldmap_process.inputs.fieldmap_nii_mag=abs_file
        cmind_fieldmap_process.inputs.fieldmap_nii_rads=rads_file
        ASLBOLD_workflow.add_nodes([cmind_fieldmap_process])
    
        #cbf_workflow internal connections (fieldmap-related)
        ASLBOLD_workflow.connect([
                #fieldmap inputs for boundary-based registration
                (cmind_fieldmap_process, cmind_fieldmap_regBBR_ASLBOLD, [('fieldmap_vol', 'Fieldmap_vol_processed')]),
                (cmind_fieldmap_process, cmind_fieldmap_regBBR_ASLBOLD, [('fieldmap_unmasked', 'Fieldmap_vol_unmasked')]),
                (cmind_fieldmap_process, cmind_fieldmap_regBBR_ASLBOLD, [('fieldmap2struct_affine', 'fieldmap2struct_affine')]),
                
                #fieldmap inputs for cmind_fMRI_outliers_BOLD
                (cmind_fieldmap_regBBR_ASLBOLD, cmind_fMRI_outliers_BOLD, [('HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                
        ])     
        
        if not BOLD_only:
            ASLBOLD_workflow.connect([
                    #fieldmap inputs for cmind_fMRI_outliers_ASL
                    (cmind_fieldmap_regBBR_ASLBOLD, cmind_fMRI_outliers_ASL, [('HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                    
            ])     
            
    else:
        cmind_fieldmap_regBBR_ASLBOLD.inputs.Fieldmap_vol_processed=None
        cmind_fieldmap_regBBR_ASLBOLD.inputs.Fieldmap_vol_unmasked=None
        cmind_fieldmap_regBBR_ASLBOLD.inputs.fieldmap2struct_affine=None
        #cbf_workflow internal connections (fieldmap-related)
        ASLBOLD_workflow.connect([

                #fieldmap inputs for cmind_fMRI_outliers_BOLD
                (cmind_fieldmap_regBBR_ASLBOLD, cmind_fMRI_outliers_BOLD, [('HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
               
        ])             
        if not BOLD_only:
            ASLBOLD_workflow.connect([

                    #fieldmap inputs for cmind_fMRI_outliers_ASL
                    (cmind_fieldmap_regBBR_ASLBOLD, cmind_fMRI_outliers_ASL, [('HRtoLR_Affine_fname', 'HRtoLR_Affine')]),
                    
            ])             

    if BOLD_only:  #which output of cmind_fMRI_preprocess to use as the registration input?
        regBBR_reference_vol = 'BOLD_mcf_vol'
    else:
        regBBR_reference_vol = 'meanCBF_N4_vol'

      
    ASLBOLD_workflow.connect([
                            
            #fieldmap (optional) boundary-based registration for Stories
            (cmind_fMRI_preprocess, cmind_fieldmap_regBBR_ASLBOLD, [(regBBR_reference_vol, 'lowres_vol')]),
            
            (cmind_fMRI_preprocess, cmind_fMRI_preprocess2_BOLD, [('BOLD_mcf_vol', 'func_file')]),
            
            (cmind_fMRI_preprocess, cmind_fMRI_outliers_BOLD, [(regBBR_reference_vol, 'regBBR_ref')]),
            (cmind_fMRI_preprocess, cmind_fMRI_outliers_BOLD, [('motion_parfile_BOLD','motion_parfile')]),
            (cmind_fMRI_preprocess, cmind_fMRI_outliers_BOLD, [('BOLD_mcf_vol','vol_file_niigz')]), 
            (cmind_fMRI_preprocess2_BOLD, cmind_fMRI_outliers_BOLD, [('noise_est_file','noise_est_file')]), 
            (cmind_fMRI_preprocess2_BOLD, cmind_fMRI_outliers_BOLD, [('filtered_func_data','filtered_func_data')]), 
    ])
    
    if not BOLD_only:
        ASLBOLD_workflow.connect([        
                (cmind_fMRI_preprocess, cmind_fMRI_preprocess2_ASL, [('ASL_mcf_vol', 'func_file')]),
                
                (cmind_fMRI_preprocess, cmind_fMRI_outliers_ASL, [(regBBR_reference_vol, 'regBBR_ref')]),
                (cmind_fMRI_preprocess, cmind_fMRI_outliers_ASL, [('motion_parfile_ASL','motion_parfile')]),
                (cmind_fMRI_preprocess, cmind_fMRI_outliers_ASL, [('ASL_mcf_vol','vol_file_niigz')]), 
                (cmind_fMRI_preprocess2_ASL, cmind_fMRI_outliers_ASL, [('noise_est_file','noise_est_file')]), 
                (cmind_fMRI_preprocess2_ASL, cmind_fMRI_outliers_ASL, [('filtered_func_data','filtered_func_data')]), 
        ])
        
    for confound_to_use in confounds_to_use:
        
        #clone stats, poststats, and norm nodes for each confound case
        cmind_fMRI_level1_stats_BOLD=cmind_fMRI_level1_stats_BOLD.clone('cmind_fMRI_level1_stats_BOLD_'+confound_to_use)
        cmind_fMRI_level1_poststats_BOLD=cmind_fMRI_level1_poststats_BOLD.clone('cmind_fMRI_level1_poststats_BOLD_'+confound_to_use + '_cluster')
        cmind_fMRI_norm_BOLD=cmind_fMRI_norm_BOLD.clone('cmind_fMRI_norm_BOLD_'+confound_to_use)

        
        ASLBOLD_workflow.connect([
        
                (cmind_fMRI_outliers_BOLD, cmind_fMRI_level1_stats_BOLD,  [(confound_to_use, 'nuisance_file')]),
                (cmind_fMRI_preprocess2_BOLD, cmind_fMRI_level1_stats_BOLD,  [('feat_params_out', 'feat_params')]), 
                (cmind_fMRI_preprocess2_BOLD, cmind_fMRI_level1_stats_BOLD,  [('prep_dir', 'prep_dir')]), 
                (cmind_fMRI_preprocess2_BOLD, cmind_fMRI_level1_poststats_BOLD,  [('prep_dir', 'prep_dir')]), 
                (cmind_fMRI_level1_stats_BOLD, cmind_fMRI_level1_poststats_BOLD,  [('output_dir', 'stats_dir')]), 
                
                #TODO:  make some connection from fMRI_level1_stats to fMRI_norm so it won't start prematurely
                (cmind_fieldmap_regBBR_ASLBOLD, cmind_fMRI_norm_BOLD,  [('regBBR_dir', 'regBBR_dir')]),  #could also use a function to extract the directory from one of the returned filenames
                #(cmind_fMRI_level1_stats_BOLD, cmind_fMRI_norm_BOLD, [('output_dir', 'output_dir')]), #remove?
                (cmind_fMRI_level1_stats_BOLD, cmind_fMRI_norm_BOLD,  [('stats_dir', 'stats_dir')]), 
                (cmind_fMRI_preprocess2_BOLD, cmind_fMRI_norm_BOLD,  [('prep_dir', 'prep_dir')]), 
                            
        ])  

        if not BOLD_only:
            cmind_fMRI_level1_stats_ASL=cmind_fMRI_level1_stats_ASL.clone('cmind_fMRI_level1_stats_ASL_'+confound_to_use)
            cmind_fMRI_level1_poststats_ASL=cmind_fMRI_level1_poststats_ASL.clone('cmind_fMRI_level1_poststats_ASL_'+confound_to_use + '_cluster')
            cmind_fMRI_norm_ASL=cmind_fMRI_norm_ASL.clone('cmind_fMRI_norm_ASL_'+confound_to_use)

            ASLBOLD_workflow.connect([
                    
                    (cmind_fMRI_outliers_ASL, cmind_fMRI_level1_stats_ASL,  [(confound_to_use, 'nuisance_file')]),
                    (cmind_fMRI_preprocess2_ASL, cmind_fMRI_level1_stats_ASL,  [('feat_params_out', 'feat_params')]), 
                    (cmind_fMRI_preprocess2_ASL, cmind_fMRI_level1_stats_ASL,  [('prep_dir', 'prep_dir')]), 
                    (cmind_fMRI_preprocess2_ASL, cmind_fMRI_level1_poststats_ASL,  [('prep_dir', 'prep_dir')]), 
                    (cmind_fMRI_level1_stats_ASL, cmind_fMRI_level1_poststats_ASL,  [('output_dir', 'stats_dir')]), 
                    
                    (cmind_fieldmap_regBBR_ASLBOLD, cmind_fMRI_norm_ASL,  [('regBBR_dir', 'regBBR_dir')]), 
                    #(cmind_fMRI_level1_stats_ASL, cmind_fMRI_norm_ASL, [('output_dir', 'output_dir')]), #?
                    (cmind_fMRI_level1_stats_ASL, cmind_fMRI_norm_ASL,  [('stats_dir', 'stats_dir')]), 
                    (cmind_fMRI_preprocess2_ASL, cmind_fMRI_norm_ASL,  [('prep_dir', 'prep_dir')]), 
                                
            ])  
    
        if run_FWEpoststats: #add FWE poststats as well
                
            cmind_fMRI_level1_poststats_BOLD_FWE = cmind_fMRI_level1_poststats_BOLD.clone(cmind_fMRI_level1_poststats_BOLD.name.replace('_cluster','_FWE'))
            cmind_fMRI_level1_poststats_BOLD_FWE.inputs.thresh='voxel'
        
            ASLBOLD_workflow.connect([
                    (cmind_fMRI_preprocess2_BOLD, cmind_fMRI_level1_poststats_BOLD_FWE,  [('prep_dir', 'prep_dir')]), 
                    (cmind_fMRI_level1_stats_BOLD, cmind_fMRI_level1_poststats_BOLD_FWE,  [('output_dir', 'stats_dir')]), 
            ])  
            
            if not BOLD_only:
                cmind_fMRI_level1_poststats_ASL_FWE = cmind_fMRI_level1_poststats_ASL.clone(cmind_fMRI_level1_poststats_ASL.name.replace('_cluster','_FWE'))
                cmind_fMRI_level1_poststats_ASL_FWE.inputs.thresh='voxel'
                ASLBOLD_workflow.connect([
                        (cmind_fMRI_preprocess2_ASL, cmind_fMRI_level1_poststats_ASL_FWE,  [('prep_dir', 'prep_dir')]), 
                        (cmind_fMRI_level1_stats_ASL, cmind_fMRI_level1_poststats_ASL_FWE,  [('output_dir', 'stats_dir')]), 
                ])                      
                
        
        if build_feat_dirs:
            cmind_build_feat_dir.base_dir=output_base_dir
            cmind_build_feat_dir.inputs.reduce_file_level=1
            cmind_build_feat_dir.inputs.output_tar='auto'  #auto->will be stored within the output_dir as 'feat_level1.tar.gz'
            #cmind_build_feat_dir_ASL = cmind_build_feat_dir.clone(cmind_build_feat_dir.name+'_ASL')
            #cmind_build_feat_dir_BOLD = cmind_build_feat_dir.clone(cmind_build_feat_dir.name+'_BOLD')
            
            #modify the names to include the confound type
            cmind_build_feat_dir_BOLD = cmind_build_feat_dir.clone('cmind_build_feat_dir_BOLD_'+confound_to_use)
            
            ASLBOLD_workflow.connect([
                    (cmind_fMRI_preprocess2_BOLD, cmind_build_feat_dir_BOLD,  [('preproc_tarfile', 'preproc_tarfile')]), 
                    (cmind_fMRI_level1_stats_BOLD, cmind_build_feat_dir_BOLD,  [('stats_tarfile', 'stats_tarfile')]), 
                    (cmind_fMRI_level1_poststats_BOLD, cmind_build_feat_dir_BOLD,  [('poststats_tarfile', 'poststats_tarfile')]), 
                    (cmind_fMRI_norm_BOLD, cmind_build_feat_dir_BOLD,  [('reg_tarfile', 'reg_tarfile'),
                                                                        ('reg_standard_tarfile', 'reg_standard_tarfile')]), 
            ])  
            if not BOLD_only:
                cmind_build_feat_dir_ASL = cmind_build_feat_dir.clone('cmind_build_feat_dir_ASL_'+confound_to_use)
                ASLBOLD_workflow.connect([
                        (cmind_fMRI_preprocess2_ASL, cmind_build_feat_dir_ASL,  [('preproc_tarfile', 'preproc_tarfile')]), 
                        (cmind_fMRI_level1_stats_ASL, cmind_build_feat_dir_ASL,  [('stats_tarfile', 'stats_tarfile')]), 
                        (cmind_fMRI_level1_poststats_ASL, cmind_build_feat_dir_ASL,  [('poststats_tarfile', 'poststats_tarfile')]), 
                        (cmind_fMRI_norm_ASL, cmind_build_feat_dir_ASL,  [('reg_tarfile', 'reg_tarfile'),
                                                                            ('reg_standard_tarfile', 'reg_standard_tarfile')]), 
                ])  
                

    if not BOLD_only:
        #specify the meanCBF_N4_vol as the "edge" volume to be used by SUSAN during spatial smoothing
        #this avoids overly blurring GM/WM as would happen if mean_func was used as in Feat because the
        #contrast in mean_func is very low
        ASLBOLD_workflow.connect([
                (cmind_fMRI_preprocess, cmind_fMRI_preprocess2_BOLD, [('meanCBF_N4_vol', 'usan_vol')]),
        ])  
        
        ASLBOLD_workflow.connect([
                (cmind_fMRI_preprocess, cmind_fMRI_preprocess2_ASL, [('meanCBF_N4_vol', 'usan_vol')]),
        ])          
                       
    #ASLBOLD_workflow = cmind_autoset_output_dirs(ASLBOLD_workflow)   
    #all_output_dirs=print_all_output_dirs(ASLBOLD_workflow)
                       
    #ASLBOLD_workflow.write_graph(dotfilename='ASLBOLD_workflow.dot',graph2use='hierarchical',format='png') #'flat') 
    #ASLBOLD_workflow.write_graph(dotfilename='ASLBOLD_workflow_flat.dot',graph2use='flat',format='png') #'flat') 
    
    ASLBOLD_workflow.config['execution']['remove_unnecessary_outputs']=False
    #res=ASLBOLD_workflow.run(plugin='Linear')
    #res=ASLBOLD_workflow.run(plugin='MultiProc')
    #res=ASLBOLD_workflow.run(plugin='IPython')
    ASLBOLD_workflow=ASLBOLD_workflow.clone(name)
    return ASLBOLD_workflow
    
def generate_Functional_workflow(output_base_dir,
                                 subject_age,
                                 subj_data,
                                 confounds_to_use=['best',],
                                 paradigms_found=None,
                                 UCLA_flag=False,
                                 run_FWEpoststats=True,
                                 build_feat_dirs=True,
                                 name='CMIND_Functional',
                                 smoothings_mm=[0,8],
                                 highpass_freqs=[104,],
                                 cluster_z_thresh=2.3,
                                 prob_thresh=0.05,
                                 BOLD_only=False,
                                 reuse_same=False,
                                 ASL_FSF_file=None,  
                                 BOLD_FSF_file=None,
                                 ASL_unfiltered_design_matrix=None,
                                 BOLD_unfiltered_design_matrix=None,
                                 verify_CMIND_filenames=True,
                                 ASL_contrast_of_interest=0,
                                 BOLD_contrast_of_interest=1,
                                 PVE_to_SubjectSpace=False,):
                                     
    CMIND_workflow = pe.Workflow(name='Func_tempname',base_dir=output_base_dir)
    
    Structural_ASLBOLD = generate_structural_workflow(output_base_dir,subject_age,subj_data,name='Structural_ASLBOLD')    

    if is_string_like(confounds_to_use):
        confounds_to_use = [confounds_to_use,]
        
    #ASLBOLD_workflow_list=[]



        
    if PVE_to_SubjectSpace:   #if requested will warp segmentation PVEs into subject space
        from cmind.nipype.cmind_nodes import cmind_atlas_to_subject
        from cmind.globals import cmind_ANTs_dir
                
    for pidx,paradigm in enumerate(paradigms_found): #paradigms_found:
        
        
        w=generate_ASLBOLD_workflow(output_base_dir,
                                  subject_age,
                                  subj_data,
                                  paradigm,
                                  confounds_to_use=confounds_to_use,
                                  name=None,
                                  UCLA_flag=UCLA_flag,
                                  run_FWEpoststats=run_FWEpoststats, 
                                  build_feat_dirs=build_feat_dirs,
                                  smoothings_mm=smoothings_mm,
                                  highpass_freqs=highpass_freqs,
                                  cluster_z_thresh=cluster_z_thresh,
                                  prob_thresh=prob_thresh,
                                  BOLD_only=BOLD_only,
                                  reuse_same=reuse_same,
                                  ASL_FSF_file=ASL_FSF_file,
                                  BOLD_FSF_file=BOLD_FSF_file,
                                  ASL_unfiltered_design_matrix=ASL_unfiltered_design_matrix,
                                  BOLD_unfiltered_design_matrix=BOLD_unfiltered_design_matrix,
                                  verify_CMIND_filenames=verify_CMIND_filenames,
                                  ASL_contrast_of_interest=ASL_contrast_of_interest,
                                  BOLD_contrast_of_interest=BOLD_contrast_of_interest)
                                  
        #w=w.clone(name='ASLBOLD_'+paradigm) #if more than one paradigm had to use .clone() in order for things to work properly
    
        #connections between Structural and ASLBOLD workflows
        CMIND_workflow.connect([ (Structural_ASLBOLD,w,[('cmind_struct_preprocess.fname_brain','cmind_fieldmap_regBBR_ASLBOLD.T1_brain_vol'),
                                                                 ('cmind_struct_preprocess.fname_head','cmind_fieldmap_regBBR_ASLBOLD.T1_vol'),
                                                                 ('cmind_segment.WM_vol','cmind_fieldmap_regBBR_ASLBOLD.WMseg_vol'),
                                                                 ('cmind_normalize.reg_struct_file','cmind_fieldmap_regBBR_ASLBOLD.reg_struct_file')]),
                                                                 
                                   (Structural_ASLBOLD,w,[('cmind_segment.WM_vol','cmind_fMRI_outliers_BOLD.WM_pve_vol')]),
                                   (Structural_ASLBOLD,w,[('cmind_segment.GM_vol','cmind_fMRI_outliers_BOLD.GM_pve_vol')]),
                                   (Structural_ASLBOLD,w,[('cmind_segment.CSF_vol','cmind_fMRI_outliers_BOLD.CSF_pve_vol')]),
        ]) 

        #connect reg_struct to all fMRI_norm_BOLD nodes
        BOLD_norm_names = [n for n in w.list_node_names() if 'fMRI_norm_BOLD' in n]
        for BOLD_norm_name in BOLD_norm_names:
            CMIND_workflow.connect([
                                       (Structural_ASLBOLD,w,[('cmind_normalize.reg_struct_file',BOLD_norm_name+'.reg_struct')]),
            ])      
            
        if not BOLD_only:
            CMIND_workflow.connect([                           
                                       (Structural_ASLBOLD,w,[('cmind_segment.WM_vol','cmind_fMRI_outliers_ASL.WM_pve_vol')]),
                                       (Structural_ASLBOLD,w,[('cmind_segment.GM_vol','cmind_fMRI_outliers_ASL.GM_pve_vol')]),
                                       (Structural_ASLBOLD,w,[('cmind_segment.CSF_vol','cmind_fMRI_outliers_ASL.CSF_pve_vol')]),
            ]) 

            
            #connect reg_struct to all fMRI_norm_ASL nodes
            ASL_norm_names = [n for n in w.list_node_names() if 'fMRI_norm_ASL' in n]
            for ASL_norm_name in ASL_norm_names:
                CMIND_workflow.connect([
                                           (Structural_ASLBOLD,w,[('cmind_normalize.reg_struct_file',ASL_norm_name+'.reg_struct')]),
                ])
            
  
        
        #structural to fieldmap connections
        if subj_data[('Functional',paradigm)]['use_fieldmap']:
            CMIND_workflow.connect([ (Structural_ASLBOLD,w,[('cmind_struct_preprocess.fname_brain','cmind_fieldmap_process.T1_brain_vol'),
                                                                             ('cmind_struct_preprocess.fname_head','cmind_fieldmap_process.T1_head_vol')]),
            ]) 
            

        if PVE_to_SubjectSpace:   #if requested will warp segmentation PVEs into subject space
            
            PVE_to_subject = cmind_atlas_to_subject.clone("PVE_to_subject_{}".format(paradigm))
            PVE_to_subject.base_dir = output_base_dir
            PVE_to_subject.inputs.omit_MNItoHR=True
            PVE_to_subject.inputs.interp_type='BSpline[3]'
            PVE_to_subject.inputs.split_script=None
            PVE_to_subject.inputs.ANTS_path=cmind_ANTs_dir
            
            """
            use Merge/Split to allow all 3 PVEs to be passed/retrieved as lists 
            from PVE_to_subject
            """
            PVE_Split = Node(name='PVE_Split_{}'.format(paradigm), interface=Split())   
            
            
            
            seg_Nclasses=Structural_ASLBOLD.get_node('cmind_segment').inputs.Nclasses
            PVE_Merge = Node(name='PVE_Merge_{}'.format(paradigm), interface=Merge(seg_Nclasses))   

            CMIND_workflow.add_nodes([PVE_to_subject,
                                      PVE_Split,
                                      PVE_Merge])
                                      
            PVE_Split.inputs.splits=[1,]*seg_Nclasses
            PVE_to_subject.inputs.output_prefix=['WMpve','GMpve','CSFpve'] #,'DeepGMpve','Brainstempve']
            CMIND_workflow.connect([
                (Structural_ASLBOLD, PVE_Merge,[('cmind_segment.WM_vol','in1'),
                                               ('cmind_segment.GM_vol','in2'),
                                               ('cmind_segment.CSF_vol','in3'),
                                               ]),  #merge PVE volumes into a single list
                
                (Structural_ASLBOLD, PVE_to_subject,[('cmind_normalize.reg_struct_file','reg_struct')]),     #connect the PVE list as input
                                        
                (PVE_Merge, PVE_to_subject,[('out','atlas_vol')]),     #connect the PVE list as input
                (PVE_to_subject, PVE_Split,[('Atlas_out','inlist')]),  #split the output list back into separate PVEs

                (w,PVE_to_subject,[('cmind_fMRI_preprocess.meanBOLD_N4_vol','ref_vol')]),
                (w, PVE_to_subject, [('cmind_fieldmap_regBBR_ASLBOLD.HRtoLR_Warp_fname', 'HRtoLR_Warp')]),
                (w, PVE_to_subject, [('cmind_fieldmap_regBBR_ASLBOLD.HRtoLR_Affine_fname', 'HRtoLR_Affine')]),

                ])     
                
                
        w.write_graph(dotfilename=w.name+'.dot',graph2use='flat',format='png')
        #CMIND_workflow.get_node(w.name).write_graph(dotfilename=w.name+'.dot',graph2use='exec',format='png')
#            if False:
#                graph=w._create_flat_graph()
            #from nipype.pipeline.utils import export_graph
            #export_graph(graph,'/tmp',show=True)
    
        
    CMIND_workflow.config['execution']['remove_unnecessary_outputs']=False
    CMIND_workflow=CMIND_workflow.clone(name=name)
    CMIND_workflow.write_graph(dotfilename='Functional_workflow.dot',graph2use='hierarchical',format='png') #'flat') 
    CMIND_workflow.write_graph(dotfilename='Functional_workflow_flat.dot',graph2use='flat',format='png') #'flat') 
    Structural_ASLBOLD.write_graph(dotfilename='Structural_ASLBOLD_flat.dot',graph2use='flat',format='png') #'flat') 
    
    return CMIND_workflow    