#!/bin/bash
echo "CMD: $*"

usage()
{
echo "Recommended Usage:
     cmind_build_feat_dirs.sh -asl asl_dir -bold bold_dir -p paradigm -oasl asl_output_zip -obold bold_output_zip

Required parameters:
    -asl          : asl_dir - directory to read files from
    -bold         : bold_dir - directory to read files from
    -p            : paradigm - the paradigm for the files
    -oasl         : asl_output_zip - file in which to store the output
    -obold        : bold_output_zip - file in which to store the output

Optional parameters:
    -reduce,  --reduce_file_level : 0, 1, or 2
        	                    0 = keep all files
                                    1 = remove some large files not needed at the 2nd level
                                    2 = more aggressive file cleanup
"
}

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_python_env.sh
source $cwd/load_fsl_env.sh

if [ $# == 0 ]; then usage;  exit 0; fi

if [ "$1" == "-h" ]; then usage; exit 0; fi

REQUIRED_ARGS=3
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

verbose_string=""
reduce_file_level=1
while [ "$1" != "" ]; do
    case $1 in
    -asl )
        shift
        asl_dir=$1
        ;;
    -bold )
        shift
        bold_dir=$1
        ;;
    -p )
        shift
        paradigm=$1
        ;;
    -oasl )
        shift
        asl_output_zip=$1
        ;;
    -obold )
        shift
        bold_output_zip=$1
        ;;
    -reduce|--reduce_file_level )
        shift
        reduce_file_level=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    esac
    shift
done

if [ ! -d "$asl_dir" ]; then
    echo "ERROR: Could not read from input directory"
    exit 1
fi

if [ ! -d "$bold_dir" ]; then
    echo "ERROR: Could not read from bold directory"
    exit 1
fi

echo "ASLDir: $asl_dir"
slash=${#asl_dir}-1
if [ ${asl_dir:slash} != "/" ]; then
    asl_dir="$asl_dir/"
fi
scan="ASL"

if [ -z "$paradigm" ]; then
    echo "Paradigm not set"
    exit 1
fi
echo "Paradigm: $paradigm"

if [ "$(ls -A $asl_dir)" ]; then
    if [ -z "$asl_output_zip" ]; then
        asl_output_zip="${asl_dir}${scan}_${paradigm}_Level_1.tar.gz"
    fi
    echo "Output: $asl_output_zip"

    files=( "prep" "stats" "reg" "reg_standard_ANTS" "poststats" )
    for i in "${files[@]}"
    do
        file=`ls ${asl_dir}$i.tar.gz | sed -n 1p`
        eval $i=$file
    done

    if [ -z "$poststats" ]; then
        poststats="none"
    fi

    echo "Prep: $prep"
    echo "Stats: $stats"
    echo "Reg: $reg"
    echo "RegStd: $reg_standard_ANTS"
    echo "Poststats: $poststats"

    python ${cmind_python_dir}/cmind_build_feat_dir.py -o $asl_dir/asl -pre $prep -stats $stats -reg $reg -regstd $reg_standard_ANTS -poststats $poststats -otar $asl_output_zip -reduce $reduce_file_level ${verbose_string}
    echo "$paradigm" > ${asl_dir}paradigm.txt
fi

echo "BOLDDir: $bold_dir"
slash=${#bold_dir}-1
if [ ${bold_dir:slash} != "/" ]; then
    bold_dir="$bold_dir/"
fi
scan="BOLD"

if [ -z "$bold_output_zip" ]; then
    bold_output_zip="${bold_dir}${scan}_${paradigm}_Level_1.tar.gz"
fi
echo "Output: $bold_output_zip"

files=( "prep" "stats" "reg" "reg_standard_ANTS" "poststats" )
for i in "${files[@]}"
do
    file=`ls ${bold_dir}$i.tar.gz | sed -n 1p`
    eval $i=$file
done

echo "Prep: $prep"
echo "Stats: $stats"
echo "Reg: $reg"
echo "RegStd: $reg_standard_ANTS"
echo "Poststats: $poststats"

python ${cmind_python_dir}/cmind_build_feat_dir.py -o $bold_dir/bold -pre $prep -stats $stats -reg $reg -regstd $reg_standard_ANTS -poststats $poststats -otar $bold_output_zip -reduce $reduce_file_level -v

echo "$paradigm" > ${bold_dir}paradigm.txt

echo "Finished cmind_build_feat_dirs"

