"""
**Image Utilities**
A series of utilities for working with images (.gif, .png, etc)
"""
from __future__ import division, print_function, absolute_import
import os
import shutil
import tempfile
import warnings

import numpy as np

from cmind.globals import (has_ImageMagick,
                           has_GraphicsMagick, 
                           cmind_imageconvert_cmd)
                           
from cmind.utils.file_utils import split_multiple_ext, imexist
from cmind.utils.logging_utils import log_cmd


__all__=[   'slicer_3plane_fig',
            'slicer_mid_sag_fig',
            'slicer_tile_slices',
            'ImageMagick_get_size',
            'ImageMagick_build_colorbar',
            'ImageMagick_crop_border',
            'ImageMagick_append_label',
            'random_color_str',
            #_find_source_node_changes,
        ]

def slicer_3plane_fig(input_volume,istr='',output_image=None, verbose=False, logger=None):  #TODO: move to image_utils
    """Use FSL's slicer to generate a center slice in each image plane
    
    Parameters
    ----------
    input_volume : str
        filename of input volume
    istr : str, optional
        optional string containing additional command line arguments
    output_image : str, optional
        override default filename for output image with this one
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    Returns
    ----------
    output_image : str
        filename of the generated image

    """
    if not output_image:
        output_image=split_multiple_ext(input_volume)[0] + '.png'
    log_cmd('$FSLDIR/bin/slicer "{}" -a "{}" {}'.format(input_volume,output_image,istr),verbose=verbose,logger=logger)
    return (output_image)

def slicer_mid_sag_fig(input_volume,istr='',output_image=None, verbose=False, logger=None):  #TODO: move to image_utils
    """Use FSL's slicer to generate a mid-sagittal image
    
    Parameters
    ----------
    input_volume : str
        filename of input volume
    istr : str, optional
        optional string containing additional command line arguments
    output_image : str, optional
        override default filename for output image with this one
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    Returns
    ----------
    output_image : str
        filename of the generated image

    """
    if not output_image:
        output_image=split_multiple_ext(input_volume)[0] + '.png'
    log_cmd('$FSLDIR/bin/slicer "{}" -x 0.5 "{}" {}'.format(input_volume,output_image,istr),verbose=verbose,logger=logger)
    return (output_image)

def slicer_tile_slices(input_volume,optional_args='',slice_skip=1, width=800, output_image=None, verbose=False, logger=None):  #TODO: move to image_utils
    """Use FSL's slicer to create a mosaic of axial images
    
    Parameters
    ----------
    input_volume : str
        filename of input volume
    optional_args : str, optional
        optional string containing additional command line arguments
    slice_skip : int, optional
        slice skip factor
    width : int, optional
        width of the mosaic in pixels
    output_image : str, optional
        override default filename for output image with this one
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    Returns
    ----------
    output_image : str
        filename of the generated image
    """
    
    if not output_image:
        output_image=split_multiple_ext(input_volume)[0] + '.png'
    log_cmd('$FSLDIR/bin/slicer "{}" {} -S {} {} "{}" '.format(input_volume,optional_args, slice_skip, width, output_image),verbose=verbose,logger=logger)
    return (output_image)

def ImageMagick_get_size(image_file):
    """ Returns size of an image as [width,height]
    
    Parameters
    ----------
    image_file : str
        image file to query
    
    Returns
    -------
    size_list : list
        [width, height] as floats
        
    """
    size_list=log_cmd('identify -format "%%[fx:w],%%[fx:h]" "%s"' % image_file).strip().split(',')
    size_list = [float(item) for item in size_list]
    return size_list

def ImageMagick_build_colorbar(output_img,cbar_input_img=None,cbar_label=['low','high'],rotate_cbar_img=0, vertical_cbar=False, cbar_input_resize_pct=200,bg_color='black',text_color='white',pointsize=None, verbose=False, logger=None):
    """Append a label to the top, bottom, right or left of an image
    
    Parameters
    ----------
    output_img : str
        output filename for the colorbar image
    cbar_input_img : str
        filename of the colorbar image (without labels)
    cbar_label : str, optional
        2-element list corresponding to the labels for the (low,high) ends of 
        the colorbar
    rotate_cbar_img : float, optional
        if > 0, the colorbar image will be rotated by this amount
    vertical_cbar : bool, optional
        if true, the colorbar labels will be stacks above/below the colorbar
        instead of left/right
    cbar_input_resize_pct : int, optional
        percentage by which to resize cbar_input_img
    bg_color : str, optional
        color of the label background
    text_color : str, optional
        color of the label text
    pointsize : int, optional
        pointsize of the label text.  If unspecified, will match size of the 
        colorbar
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    """
    
    if not (has_ImageMagick or has_GraphicsMagick):
        raise OSError("ImageMagick not found in path.  Unable to build colorbar")
        
    if not cbar_input_img:
        cbar_input_img="$FSLDIR/etc/luts/ramp.gif"

    if (not isinstance(cbar_label,(list,tuple))) or len(cbar_label)!=2:
        raise ValueError("cbar_label must be a list or tuple of length two")
    
    if cbar_input_resize_pct<0:
        raise ValueError("colorbar resize percentage must be >0")

    if np.mod(rotate_cbar_img,90)!=0:
        warnings.warn("colorbar rotation should generally be a multiple of 90 degrees")
    
    working_dir=tempfile.mkdtemp()
    cbar_tmp_img=os.path.join(working_dir,'cbar.png')
    cmin_img=os.path.join(working_dir,'cmin.png')
    cmax_img=os.path.join(working_dir,'cmax.png')

    if rotate_cbar_img!=0:
        if has_GraphicsMagick:
            distort_cmd='-rotate %d' % int(rotate_cbar_img)  #rotate the text by 270 degrees so it runs vertically
        else:
            distort_cmd='+distort SRT %f' % rotate_cbar_img
    else:
        distort_cmd=''
        
    if cbar_input_resize_pct!=100:
        rotate_cmd = '-resize %d%%' % int(cbar_input_resize_pct)
    else:
        rotate_cmd = ''

    #rotate and/or resize cbar_input_img 
    if rotate_cmd or distort_cmd:
        log_cmd('%s "%s" %s %s "%s"' % (cmind_imageconvert_cmd, cbar_input_img, distort_cmd, rotate_cmd, cbar_tmp_img), verbose=verbose, logger=logger)
    else:
        log_cmd('cp "%s" "%s"' % (cbar_input_img, cbar_tmp_img), verbose=verbose, logger=logger)

    if not pointsize:
        w,h=ImageMagick_get_size(cbar_tmp_img)
        if vertical_cbar:
            pointsize=int(1.3*w)
        else:
            pointsize=int(1.3*h)  #match text height to the colorbar size

    log_cmd('%s -background %s -fill %s -set colorspace sRGB -pointsize %d label:" %s " "%s"' % (cmind_imageconvert_cmd,bg_color,text_color,pointsize,cbar_label[0],cmin_img), verbose=verbose, logger=logger)
    log_cmd('%s -background %s -fill %s -set colorspace sRGB -pointsize %d label:" %s " "%s"' % (cmind_imageconvert_cmd,bg_color,text_color,pointsize,cbar_label[1],cmax_img), verbose=verbose, logger=logger)
    if vertical_cbar:
        log_cmd('%s "%s" "%s" "%s" -gravity center -set colorspace sRGB -append "%s"' % (cmind_imageconvert_cmd,cmax_img, cbar_tmp_img, cmin_img,output_img), verbose=verbose, logger=logger)
    else:
        log_cmd('%s "%s" "%s" "%s" -gravity center -set colorspace sRGB +append "%s"' % (cmind_imageconvert_cmd,cmin_img, cbar_tmp_img, cmax_img,output_img), verbose=verbose, logger=logger)
    shutil.rmtree(working_dir)
         
"""
from wand.image import Image
from wand.color import Color

img=Image(filename='/tmp/cbar.png')

w=img.width
h=img.height
w,h=img.size

#make a copy instead of changing the original
img2=img.clone()
img2.format='gif'  #switch format

#can trim the border using
img2.trim()

#can scale by 200% using
img2.transform('','200%')

#can rotate 45 degrees using
img2.rotate(45)
img2.flop()  #flip LR
img2.flip()  #flip UD

#convert to grayscale
img.type='grayscale'

#add an alpha channel
img.alpha_channel = True

blank_image=Image(width=200, height=100, background=Color('red'))
blank_image.save(filename='/tmp/red.png')

"""

def ImageMagick_crop_border(input_image,output_image=None,verbose=False,logger=None):  #TODO: move to image_utils
    """Trim borders from an image
    
    Parameters
    ----------
    input_image : str
        filename for the input image
    output_image : str, optional
        filename for the output image
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    """
    if not (has_ImageMagick or has_GraphicsMagick):
        raise OSError("ImageMagick not found in path.  Unable to trim image border")
    
    if not os.path.exists(input_image):
        raise OSError("Specified image file does not exist")
        
    if not output_image:  #overwrite input by default
        output_image=input_image
        
    if has_GraphicsMagick:  #had to add +matte to fix a bug in some versions of GraphicsMagick where border wasn't cropped properly
        log_cmd('%s "%s" -set colorspace sRGB +matte -trim "%s"' % (cmind_imageconvert_cmd,input_image,output_image),verbose=verbose,logger=logger) #remove border 
    else:
        log_cmd('%s "%s" -set colorspace sRGB -trim "%s"' % (cmind_imageconvert_cmd,input_image,output_image),verbose=verbose,logger=logger) #remove border 
 
def ImageMagick_resize(input_image,pct,output_image=None,verbose=False,logger=None):  #TODO: move to image_utils
    """Resize an image
    
    Parameters
    ----------
    input_image : str
        filename for the input image
    pct : int
        percentage to resize by
    output_image : str, optional
        filename for the output image
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    """
    if not (has_ImageMagick or has_GraphicsMagick):
        raise OSError("ImageMagick not found in path.  Unable to resize image")
    
    if not os.path.exists(input_image):
        raise OSError("Specified image file does not exist")
        
    if not output_image:  #overwrite input by default
        output_image=input_image
        
    log_cmd("%s %s -resize %d%% %s" % (cmind_imageconvert_cmd,input_image,pct,output_image), verbose=verbose, logger=logger)
    

def ImageMagick_append_label(input_image,title_string,output_image=None,position='top',bg_color='black',text_color='white',pointsize=90, colorspace = 'sRGB', verbose=False,logger=None):
    """Append a label to the top, bottom, right or left of an image
    
    Parameters
    ----------
    input_image : str
        filename for the input image
    label_string : str
        text for the label
    output_image : str, optional
        filename for the output image
    bg_color : str, optional
        color of the label background
    text_color : str, optional
        color of the label text
    position : str, {'top','bottom','left','right'}
        controls the position or the label.  If 'left' or 'right' label text
        will run vertically.
    pointsize : int, optional
        pointsize of the label text
    colorspace : str, optional
        modify colorspace of image.  default = sRGB
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    """
    #TODO: could auto-determine an appropriate text size to fit the image dimensions
    
     
    if not (has_ImageMagick or has_GraphicsMagick):
        raise OSError("ImageMagick not found in path.  Unable to build colorbar")
    
    if not output_image: #by default overwrite the input image
        output_image=input_image
        
    if isinstance(pointsize,str):
        pointsize=int(pointsize)
        
    working_dir=tempfile.mkdtemp()
    title_file=os.path.join(working_dir,'title.png')
    
    if (position.lower()=='left') or (position.lower()=='right'):
        #    use rotate instead of distort.
        distort_string='-rotate 270'  #rotate the text by 270 degrees so it runs vertically
        #distort_string='+distort SRT 270'  #rotate the text by 270 degrees so it runs vertically
    else:
        distort_string=''  #horizontal text is the default
    
    #generate an image containing just the label
    log_cmd('{} -background {} -fill {} -pointsize {} {} label:"{}" {}'.format(cmind_imageconvert_cmd,bg_color,text_color,pointsize,distort_string,title_string,title_file),verbose=verbose, logger=logger);
    
    #append the label to the original image
    if position.lower()=='top':
        log_cmd("{} -background {} -set colorspace {} -gravity North colorspace -append {} {} {}".format(cmind_imageconvert_cmd, bg_color, colorspace, title_file, input_image,output_image),verbose=verbose, logger=logger)
    elif position.lower()=='bottom':
        log_cmd("{} -background {} -set colorspace {} -gravity South -append {} {} {}".format(cmind_imageconvert_cmd,bg_color, colorspace, input_image, title_file, output_image),verbose=verbose, logger=logger)
    elif position.lower()=='left':
        log_cmd("{} -background {} -set colorspace {} -gravity West +append {} {} {}".format(cmind_imageconvert_cmd,bg_color, colorspace, title_file, input_image, output_image),verbose=verbose, logger=logger)
    elif position.lower()=='right':
        log_cmd("{} -background {} -set colorspace {} -gravity East +append {} {} {}".format(cmind_imageconvert_cmd,bg_color, colorspace, input_image, title_file, output_image),verbose=verbose, logger=logger)
    else:
        raise ValueError("Unrecognized position, {}.  Must be 'top' or 'bottom'".format(position))
    shutil.rmtree(working_dir)    

#http://www.graphviz.org/doc/info/colors.html    
def random_color_str(cmin=0x000000,cmax=0xe0e0e0):   
    """return a randomized RGB hex color string "#{RRGGBB}" for use in a graphviz .dot file.  
    
    Parameters
    ----------
    cmin : int, optional
        minimum allowed RGB color:  must be in range [0x00000 0xffffff]
        (R, G, B can each range from 0x00 to 0xff corresponding to the integer range [0 255])
        
    cmax : int, optional
        maximum allowed color:  must be in range [0x00000 0xffffff]
        
    Returns
    -------
    color_code : str
        color code string for use in graphviz .dot files
        
    """
    if cmin>cmax:
        raise ValueError("cmin must be <= cmax")
    if cmin>0xffffff or cmax>0xffffff:
        raise ValueError("cmin, cmax must be <= 0xffffff")
    if cmin<0 or cmax<0:
        raise ValueError("cmin, cmax must be >=0")
    
    #constrain color to be between 0x      
    color_code='"#{:06x}"'.format(np.random.randint(cmin,cmax))
    return color_code    

def _find_source_node_changes(e):
    """
    
    """
    v_prev=e[0].split(':')[0]
    change_idx=[]
    for idx,v in enumerate(e[1::]):
        v1=v.split(':')[0]   
        if v1!=v_prev:
            change_idx.append(idx+1)
        v_prev=v1
    return change_idx
#
#if coloredges_by_node: #GRL
#    change_idx=find_source_node_changes(edges)
#    for idx in change_idx[-1::-1]:
#        edges.insert(idx,"edge [color=" + random_color_str() + "]")    


def set_foregroundcolor(ax, color):  #from: https://gist.github.com/jasonmc/1160951
    '''For the specified axes, sets the color of the frame, major ticks,
    tick labels, axis labels, title and legend
    '''
    for tl in ax.get_xticklines() + ax.get_yticklines():
        tl.set_color(color)
    for spine in ax.spines:
        ax.spines[spine].set_edgecolor(color)
    for tick in ax.xaxis.get_major_ticks():
        tick.label1.set_color(color)
    for tick in ax.yaxis.get_major_ticks():
        tick.label1.set_color(color)
    ax.axes.xaxis.label.set_color(color)
    ax.axes.yaxis.label.set_color(color)
    ax.axes.xaxis.get_offset_text().set_color(color)
    ax.axes.yaxis.get_offset_text().set_color(color)
    ax.axes.title.set_color(color)
    lh = ax.get_legend()
    if lh != None:
        lh.get_title().set_color(color)
        lh.legendPatch.set_edgecolor('none')
        labels = lh.get_texts()
        for lab in labels:
            lab.set_color(color)
    for tl in ax.get_xticklabels():
        tl.set_color(color)
    for tl in ax.get_yticklabels():
        tl.set_color(color)
 
 
def set_backgroundcolor(ax, color):  #from: https://gist.github.com/jasonmc/1160951
    '''Sets the background color of the current axes (and legend).
    Use 'None' (with quotes) for transparent. To get transparent
    background on saved figures, use:
    pp.savefig("fig1.svg", transparent=True)
    '''
    ax.patch.set_facecolor(color)
    lh = ax.get_legend()
    if lh != None:
        lh.legendPatch.set_facecolor(color)

def mpl_white_on_black(f):   
    """
    convert matplotlib figure to white text on black bacground
    if saving to file, will also need f.savefig(filename, facecolor='black')     
    
    Parameters
    ----------
    f : matplotlib.figure.Figure
        matplotlib figure to modify
        
    """
    for ax in f.axes:
        set_foregroundcolor(ax, 'white')
        set_backgroundcolor(ax, 'black')        
        
def designmat_plot(dm, col_width_pixels = 8, vrange = None, normalize_cols = False):
    import matplotlib.pyplot as plt
    from cmind.utils.montager import montager
    
    if dm.shape[1]>dm.shape[0]:
        raise ValueError("Design matrix has more columns than timepoints!  Need to transpose?")
    f=plt.figure()
    
    if normalize_cols:
        dm = dm.copy()
        for col in range(dm.shape[-1]):
            dm[:,col] -= dm[:,col].mean()
            dm_range = dm[:,col].max()-dm[:,col].min()
            if dm_range>0:
                dm[:,col] = dm[:,col]/(2*np.abs(dm[:,col]).max()) #(dm_range)
            
    if vrange is None:
        vmin = dm.min()
        vmax = dm.max()
    else:
        vmin = vrange[0]
        vmax = vrange[1]
        
    plt.imshow(montager(np.kron(dm,np.ones((1,col_width_pixels)))),
               cmap=plt.cm.gray,
               vmin=vmin,vmax=vmax)
    plt.ylabel('Timepoint')
    plt.xlabel('Regressors')
    frame1=plt.gca()
    frame1.axes.get_xaxis().set_ticks([])
    return f    

def _nibabel_zrange(zstat_thresh_vol,percentile_range=[1,99]):
    import nibabel as nib
    img = nib.load(zstat_thresh_vol).get_data()
    img = img[img>0]
    if len(img)>0:
        zrange = np.percentile(img,q=percentile_range)
        zmin=zrange[0]
        zmax=zrange[1]
    else:
        zmin=0
        zmax=0
    return zmin, zmax
    
def cmind_dual_func_overlay(bg_image,pos_con_img,neg_con_img,output_vol,scale=1,slice_skip=2,zpos_img=None,zneg_img=None,zmin_pos=None,zmax_pos=None,zmin_neg=None,zmax_neg=None,title_str=None,overlay_colorbars=True,html_out=None,conname="",do_transparent=False,slicer_width=750,verbose=False,logger=None):
    """utility to overlay both positive and negative activations on a single
    background image
    
    Parameters
    ----------
    bg_image : str
        filename of the background image
    pos_con_image : str
        filename of the positive contrast image
    neg_con_image : str
        filename of the negative contrast image
    zpos_img : str, optional
        filename of the pos. contrast z-stat image to get the min/max range from
    zneg_img : str, optional
        filename of the neg. contrast z-stat image to get the min/max range from        
    zpos_min : float, optional
        if present, clip range with this minimum value
    zpos_max : float, optional
        if present, clip range with this maximum value
    zneg_min : float, optional
        if present, clip range with this minimum value
    zneg_max : float, optional
        if present, clip range with this maximum value
    output_vol : str
        output filename for the overlays
    scale : int
        image scaling factor when generating overlays
    slice_skip : int, optional
        slice skip factor when generating overlays
    overlay_colorbars : bool, optional    
        if True, overlay colorbars onto the stat images    
    title_str : str or None, optional
        title for the figure
    html_out : str, optional
        filename for output html report
    conname : str, optional
        contrast name to list in `html_out` report
    do_transparent : bool, optional
        use transparent overlays?
    slicer_width : int, optional
        maximum allowed width in pixels of the output overlay images
            
    """    
    try:
        import nibabel as nib
        found_nibabel = True
    except:
        found_nibabel = False
        
    output_vol = split_multiple_ext(output_vol)[0]
    output_image = output_vol + '.png'
    
    #(zmin_pos,zmax_pos)=pos_zrange.split('\n')[-2].strip().split(' ')
    #(zmin_neg,zmax_neg)=neg_zrange.split('\n')[-2].strip().split(' ')
    imbool, pos_con_img = imexist(pos_con_img)[0:2]
    if not imbool:
        raise ValueError("Specified positive contrast volume not found")

    imbool, bg_image = imexist(bg_image)[0:2]
    if not imbool:
        raise ValueError("Specified background volume not found")

    if zpos_img is not None:
        if found_nibabel:
            zmin_pos,zmax_pos=_nibabel_zrange(zpos_img)
            pos_zrange="%0.3g %0.3g" % (zmin_pos, zmax_pos)
            zmin_pos=pos_zrange.strip().split(' ')[0]
            zmax_pos=pos_zrange.strip().split(' ')[1]
        else:
            pos_zrange=log_cmd('$FSLDIR/bin/fslstats %s -l 0.0001 -R' % (zpos_img))
    else:
        pos_zrange=log_cmd('$FSLDIR/bin/fslstats %s -l 0.0001 -R' % (pos_con_img))
        
    if pos_zrange.lower().find('empty mask image')!=-1:
        pos_zrange=('0.000000', '0.000000')
    else:
        pos_zrange=pos_zrange.strip().split(' ')
        
    if zmin_pos is None:
        zmin_pos=pos_zrange[0]
    if zmax_pos is None:
        zmax_pos=pos_zrange[1]
             
    if neg_con_img is not None:
        imbool, neg_con_img = imexist(neg_con_img)[0:2]
        if not imbool:
            raise ValueError("Specified negative contrast volume not found")
    
        if zneg_img is not None:
            if found_nibabel:
                zmin_neg,zmax_neg=_nibabel_zrange(zneg_img)
                neg_zrange="%0.3g %0.3g" % (zmin_neg, zmax_neg)
                zmin_neg=neg_zrange.strip().split(' ')[0]
                zmax_neg=neg_zrange.strip().split(' ')[1]                
            else:
                neg_zrange=log_cmd('$FSLDIR/bin/fslstats %s -l 0.0001 -R' % (zneg_img))
        else:
            neg_zrange=log_cmd('$FSLDIR/bin/fslstats %s -l 0.0001 -R' % (neg_con_img))
            
        if neg_zrange.lower().find('empty mask image')!=-1:
            neg_zrange=('0.000000', '0.000000')
        else:
            neg_zrange=neg_zrange.strip().split(' ')
            
        if zmin_neg is None:
            zmin_neg=neg_zrange[0]
        if zmax_neg is None:
            zmax_neg=neg_zrange[1]
             
        
        log_cmd('$FSLDIR/bin/overlay %d 0 "%s" -a "%s" %s %s "%s" %s %s "%s"' % (do_transparent,bg_image,pos_con_img,zmin_pos,zmax_pos,neg_con_img,zmin_neg,zmax_neg,output_vol), verbose, logger)
    else:
        log_cmd('$FSLDIR/bin/overlay %d 0 "%s" -a "%s" %s %s "%s"' % (do_transparent,bg_image,pos_con_img,zmin_pos,zmax_pos,output_vol), verbose, logger)        
    
    #print(slice_skip)
    #print('$FSLDIR/bin/slicer "{}" -s {} -A {} "{}"'.format(output_vol,slice_skip,slicer_width,output_image))
    log_cmd('$FSLDIR/bin/slicer "%s" -s %d -S %d %d "%s"' % (output_vol,scale,slice_skip,slicer_width,output_image), verbose, logger)
    
    if overlay_colorbars and (not has_ImageMagick):
        warnings.warn("overlay_colorbars requested, but ImageMagick not found.  overlays will not be generated")
    
    if overlay_colorbars and has_ImageMagick:
        
        #Set up some appropriate sizes for the labels
        cbar_scale=1.25
        title_scale=2.5
        pointsize1 = cbar_scale*11
        pointsize2 = title_scale*10
        resize = int(cbar_scale*100)
        border_sz = 8
        
        #    if not working_dir:
        working_dir = tempfile.mkdtemp()  #createRandomDirectory()
        rm_working_dir=True
        #else:
        #    rm_working_dir=False

        log_cmd('{} $FSLDIR/etc/luts/ramp.gif -resize {}% {}/rampLG.png'.format(cmind_imageconvert_cmd,resize,working_dir), verbose, logger)
        log_cmd('{} -background black -fill white -set colorspace sRGB -pointsize {} label:"  {} " {}/zmin.png'.format(cmind_imageconvert_cmd,pointsize1,zmin_pos,working_dir), verbose, logger)
        log_cmd('{} -background black -fill white -set colorspace sRGB -pointsize {} label:" {}  " {}/zmax.png'.format(cmind_imageconvert_cmd,pointsize1,zmax_pos,working_dir), verbose, logger)
        log_cmd('{} -background black -set colorspace sRGB {}/zmin.png {}/rampLG.png {}/zmax.png +append {}/zbar_pos.png'.format(cmind_imageconvert_cmd,working_dir,working_dir,working_dir,working_dir), verbose, logger)

        if zmax_pos>0:
            log_cmd('composite -background black -gravity NorthWest {}/zbar_pos.png {} {}/ztmp.png'.format(working_dir,output_image,working_dir), verbose, logger)
        else:
            #print("No positive activation")
            log_cmd('cp {} {}/ztmp.png'.format(output_image,working_dir), verbose, logger)

        if neg_con_img is not None:
            log_cmd('{} $FSLDIR/etc/luts/ramp2.gif -resize {}% -flop {}/ramp2LG.png'.format(cmind_imageconvert_cmd,resize,working_dir), verbose, logger)
            log_cmd('{} -background black -fill white -set colorspace sRGB -pointsize {} label:"  {} " {}/zmin.png'.format(cmind_imageconvert_cmd,pointsize1,zmin_neg,working_dir), verbose, logger)
            log_cmd('{} -background black -fill white -set colorspace sRGB -pointsize {} label:" {}  " {}/zmax.png'.format(cmind_imageconvert_cmd,pointsize1,zmax_neg,working_dir), verbose, logger)
            log_cmd('{} -background black -set colorspace sRGB {}/zmin.png {}/ramp2LG.png {}/zmax.png +append {}/zbar_neg.png'.format(cmind_imageconvert_cmd,working_dir,working_dir,working_dir,working_dir), verbose, logger)
         
            if zmax_neg>0:
                log_cmd('composite -gravity NorthEast -background black {}/zbar_neg.png {}/ztmp.png {}/ztmp.png'.format(working_dir,working_dir,working_dir), verbose, logger)
            else:
                pass
                #print("No negative activation")
        
        if title_str is not None:
            log_cmd('{} -background black -fill white -set colorspace sRGB -pointsize {} -border {} -bordercolor black label:"{}" {}/ztitle.png'.format(cmind_imageconvert_cmd,pointsize2,border_sz,title_str,working_dir), verbose, logger)
        log_cmd('{} -gravity center -set colorspace sRGB -background black {}/ztitle.png {}/ztmp.png -append {}'.format(cmind_imageconvert_cmd,working_dir,working_dir,output_image), verbose, logger)

        if rm_working_dir:
            shutil.rmtree(working_dir)
            
    if html_out:
        pth=os.path.dirname(html_out)
        if pth and (pth!=os.path.dirname(output_image)):
            log_cmd('mv %s %s' % (output_image,os.path.join(pth,os.path.basename(output_image))), verbose, logger)
        
        log_cmd('cp $FSLDIR/etc/luts/ramp.gif "%s"/.ramp.gif' % (pth), verbose, logger)
        
        fid_out=open(html_out,'w')
        fid_out.write('<HTML>')
        
        fid_out.write('<h2>%s</h2><br>\n' % os.getcwd())
        fid_out.write('<hr><b>Thresholded activation images</b><br>\n')
        fid_out.write('<p>%s<br>\n' % conname)
        fid_out.write('<p>\n') #'&nbsp; &nbsp; &nbsp; &nbsp;\n')
        fid_out.write('%s&nbsp;<IMG BORDER=0 SRC=\".ramp.gif\">&nbsp;%s\n' % (zmin_pos,zmax_pos))
        if neg_con_img is not None:
            log_cmd('cp $FSLDIR/etc/luts/ramp2.gif "%s"/.ramp2.gif' % (pth));
            fid_out.write('&nbsp; &nbsp; &nbsp; &nbsp;\n')
            fid_out.write('-%s&nbsp;<IMG BORDER=0 SRC=\".ramp2.gif\">&nbsp;-%s<br>\n' % (zmax_neg,zmin_neg))
            
        if pth:
            fid_out.write('<IMG BORDER=0 SRC=\"%s\">\n' % os.path.basename(output_image))
        else:
            fid_out.write('<IMG BORDER=0 SRC=\"%s\">\n' % output_image)
    
        fid_out.write('</BODY></HTML>\n')
        fid_out.close()
