#!/bin/bash
echo "CMD: $*"
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

usage()
{
echo "Basic usage: cmind_fMRI_preprocess.py -o <output_dir> -asl <ASL_vol> -bold <BOLD_vol>  [options]

usage: cmind_fMRI_preprocess.py [-h] -o OUTPUT_DIR -asl ASL_VOL -bold BOLD_VOL
                                [-p PARADIGM_NAME] [-ants ANTS_BIAS_CMD]
                                [-ucla UCLA_FLAG]
                                [--fsl_motion_outliers_cmd FSL_MOTION_OUTLIERS_CMD]
                                [-gf GENERATE_FIGURES] [-u FORCEUPDATE] [-v]
                                [-log LOGGER]

Run coregistration of ASL and BOLD timeseries

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -asl ASL_VOL, --ASL_vol ASL_VOL
                        filename of the NIFTI/NIFTI-GZ ASL 4D timeseries
  -bold BOLD_VOL, --BOLD_vol BOLD_VOL
                        filename of the NIFTI/NIFTI-GZ BOLD 4D timeseries

optional arguments:
  -h, --help            show this help message and exit
  -p PARADIGM_NAME, --paradigm_name PARADIGM_NAME
                        [Feat_BOLD_Sentences,Feat_BOLD_Stories,Feat_ASL_Senten
                        ces,Feat_ASL_Stories];may be used in future to apply
                        different processing to different cases
  -ants ANTS_BIAS_CMD, --ANTS_bias_cmd ANTS_BIAS_CMD
                        pathname to the ANTs bias field correction binary
  -ucla UCLA_FLAG, --UCLA_flag UCLA_FLAG
                        if True, ASL labeling order is tag, control, tag...;
                        if False, ASL labeling order is control, tag,
                        control...
  --fsl_motion_outliers_cmd FSL_MOTION_OUTLIERS_CMD
                        path to the fsl_motion_outliers_cmd_v2 shell script.
                        If not specified will try to guess it relative to the
                        python script location.
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=6
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

######################################################
###  Begin site-specific module load commands here ###
######################################################

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh


ForceUpdate=true
generate_figures=true
UCLA_flag=false
ANTS_bias_cmd=N4BiasFieldCorrection
output_dir="default"
fsl_motion_outliers_cmd="None"

######################################################
###  End site-specific module load commands here   ###
######################################################


verbose_string=""
all_optional_args=""
while [ "$1" != "" ]; do
  case $1 in
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -asl|--ASL_vol )
        shift
        ASL_vol=$1
        ;;
    -bold|--BOLD_vol )
        shift
        BOLD_vol=$1
        ;;
    -p|--paradigm_name )
        shift
        paradigm_name=$1
        ;;
    -ants|--ANTS_bias_cmd )
        shift
        ANTS_bias_cmd=$1
        ;;
    -ucla|--UCLA_flag )
        shift
        UCLA_flag=$1
        ;;
    --fsl_motion_outliers_cmd )
        shift
        fsl_motion_outliers_cmd=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done


if [ ! -z $verbose_string ]; then
	debug=true
else
	debug=false
fi


echo "python ${cmind_python_dir}/cmind_fMRI_preprocess.py ${verbose_string} ${all_optional_args}"
#existence checks for all required conditional arguments
if [ -z "$output_dir" ]; then
   echo "missing required argument: output_dir"
fi

if [ -z "$ASL_vol" ]; then
   echo "missing required argument: ASL_vol"
fi

if [ -z "$BOLD_vol" ]; then
   echo "missing required argument: BOLD_vol"
fi

######################################################
###  Begin custom argument processing here         ###
######################################################


if [ $output_dir = "default" ]; then
	output_dir=`dirname $ASL_vol`
	output_dir=$output_dir/ASLBOLD_$paradigm_name
fi

if [ ! -f $ASL_vol ]; then
    echo "ERROR: ASL_vol file: $ASL_vol doesn't exist"
    exit 1
fi 

if [ ! -f $BOLD_vol ]; then
    echo "ERROR: BOLD_vol file: $BOLD_vol doesn't exist"
    exit 1
fi 

if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi


#Check that the ANTs executable is on the path
command -v $ANTS_bias_cmd >/dev/null 2>&1 || { echo "The ANTS command $ANTS_bias_cmd is required, but was not found. Aborting." >&2; exit 1; }
ANTS_bias_cmd=`command -v $ANTS_bias_cmd`


######################################################
###  End custom argument processing here           ###
######################################################


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir ASL_vol BOLD_vol paradigm_name ANTS_bias_cmd UCLA_flag fsl_motion_outliers_cmd generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_fMRI_preprocess.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_fMRI_preprocess.py ${verbose_string} ${all_optional_args}
printf 'cmind_fMRI_preprocess.py:  Elapsed time: %s\n' $(timer $t)


#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    ASL_mcf_vol
#    BOLD_mcf_vol
#    meanCBF_N4_vol
#    motion_parfile_ASL
#    motion_parfile_BOLD

#########################################################
###  Custom post-processing output after this point   ###



# ASL_mcf_vol="${output_dir}/ASL_${paradigm_name}_mcf.nii.gz"
# BOLD_mcf_vol="${output_dir}/BOLD_${paradigm_name}_mcf.nii.gz"
# meanCBF_N4_vol="${output_dir}/MeanSub_ASL_${paradigm_name}_mcf_N4.nii.gz"
# motion_parfile_ASL="${output_dir}/ASL_${paradigm_name}_mcf.par"
# motion_parfile_BOLD="${output_dir}/BOLD_${paradigm_name}_mcf.par"

if $debug
then
echo "output_dir=$output_dir"
echo "ASL_vol=$ASL_vol"
echo "BOLD_vol=$BOLD_vol"
echo "paradigm_name=$paradigm_name"
echo "ANTS_bias_cmd=$ANTS_bias_cmd"
echo "UCLA_flag=$UCLA_flag"
echo "generate_figures=$generate_figures"
echo "ForceUpdate=$ForceUpdate"
fi

#check that preprocessed output exists
if [ ! -f $ASL_mcf_vol ] || [ ! -f $BOLD_mcf_vol ] || [ ! -f $meanCBF_N4_vol ]; then
    echo "ERROR: Failed to finish fMRI motion correction preprocessing"
    exit 1
fi 

# #print output filenames for capture by LONI pipeline
# echo "ASL_mcf_vol:${ASL_mcf_vol}"
# echo "BOLD_mcf_vol:${BOLD_mcf_vol}"
# echo "meanCBF_N4_vol:${meanCBF_N4_vol}"
# echo "motion_parfile_ASL:${motion_parfile_ASL}"
# echo "motion_parfile_BOLD:${motion_parfile_BOLD}"
# echo "Finished cmind_fMRI_preprocess"
