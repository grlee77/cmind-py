#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_alpha(output_dir, alpha_complex_nii, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """ASL labeling efficiency estimation
    
    Parameters
    ----------     
    output_dir : str
        directory to store output
    alpha_complex_nii : str
        filename to the complex alpha .nii.gz volume
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
            
    Returns
    -------
    alpha_fname : str
        text file containing the computed alpha value
    alpha : float
        alpha value
            
    Notes
    -----
    primary output is myalpha.txt, containing the computed inversion efficiency
    
    additional output:
        alpha.mat
        alpha_avg_control.nii.gz
        alpha_avg_control_masked.nii.gz
        alpha_avg_sub.nii.gz
        alpha_avg_sub_masked.nii.gz
        alpha_avg_tag.nii.gz
        alpha_control_overlay.nii.gz
        alpha_control_overlay.png
        alpha_details.png
        alpha_map_masked.nii.gz
        alpha_sub_overlay.nii.gz
        cbar.png
        myalpha.txt
        myalpha_old.txt
    """
    import os, warnings
    import cPickle as pickle
    from os.path import join as pjoin
    
    import numpy as np
    import nibabel as nib
    
    from cmind.utils.utils import input2bool
    from cmind.utils.cmind_utils import cmind_outlier_remove
    from cmind.utils.nifti_utils import cmind_save_nii_mod_header
    from cmind.utils.file_utils import imexist, dict2csv
#    from cmind.utils.image_utils import slicer_tile_slices
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting alpha Calculation")

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    ForceUpdate=input2bool(ForceUpdate)
    generate_figures=input2bool(generate_figures)
                    
    (alpha_exist,alpha_complex_nii) = imexist(alpha_complex_nii)[0:2]
    if not alpha_exist:
        raise IOError("alpha_complex_nii, %s, doesn't exist!" % alpha_complex_nii)
    
    alpha_fname=pjoin(output_dir,'myalpha.txt')    
    
    if generate_figures:
        import matplotlib as mpl
        mpl.use('agg') #don't plot to screen
        import matplotlib.pylab as pylab
        import matplotlib.pyplot as plt
        
    if (not os.path.exists(alpha_fname)) or ForceUpdate:
        files_to_delete=['alpha.mat', 'myalpha*.txt', 'alpha*.nii.gz', 'alpha*.png']
        for f in files_to_delete:
            f=pjoin(output_dir,f)
            if os.path.exists(f):
                os.remove(f)
                
        alpha={}
        alpha_nii=nib.load(alpha_complex_nii)
        alpha['img'] = alpha_nii.get_data()
        alpha['control'] = alpha['img'][:,:,0::2];
        alpha['tag'] = alpha['img'][:,:,1::2];
        
        discard_outliers=True
        if discard_outliers:
            thresh=1.0
            cplx_diff_imgs = alpha['control']-alpha['tag'];
            (timepoints_keep, AL)=cmind_outlier_remove(cplx_diff_imgs,thresh);  #TODO: replace with fsl_motion_outliers
            alpha['control']=alpha['control'][:,:,timepoints_keep];
            alpha['tag']=alpha['tag'][:,:,timepoints_keep];
        
        alpha['control_real'] = np.mean(alpha['control'],2);
        alpha['tag_real'] = np.mean(alpha['tag'],2);
        
        #remove phase from control image
        phcorr = np.angle(alpha['control_real']);
        alpha['control_real'] = np.real(alpha['control_real']*np.exp(-1j*phcorr));
        alpha['tag_real'] = np.real(alpha['tag_real']*np.exp(-1j*phcorr));
        
        #now use a real subtraction 
        alpha['subtracted_real'] = alpha['control_real']-alpha['tag_real'];
        
        delay_correct=False
        if delay_correct:
            #PROBLEM: no correction for T1 recovery time before
            #measurement plane -> underestimation of alpha
            #in alpha measurement the plane is ~ 0.5 cm from the labeling
            #site.  assuming avg velocity of 20cm/s this would take 25 ms to transit
            T1_art=1660.0; #ms
            time_to_measurement_plane = 25.0; #ms
            alpha['subtracted_real'] = alpha['subtracted_real']/np.exp(-time_to_measurement_plane/float(T1_art));
        
        #nonzero_indices=np.where(alpha['control_real']!=0)
        foreground_indices=np.where(alpha['control_real']>0.10*np.percentile(alpha['control_real'],q=95))
        alpha['eff_real_unmasked']=np.zeros_like(alpha['subtracted_real']);
        alpha['eff_real_unmasked'][foreground_indices]=alpha['subtracted_real'][foreground_indices]/(2*alpha['control_real'][foreground_indices]);
        
        #GRL: new alpha calculation.  Hopefully more robust by averaging several voxels.  use
        alpha['vmask25']=alpha['subtracted_real']>0.25*np.max(alpha['subtracted_real']);
        #xy coordinates that are within the mask
        
        #using end:-1:1 for figure display purposes
        (xmask,ymask)=np.where(alpha['vmask25'][:,::-1]); 
        #xy_mask=[xmask ymask];
        
        tmp=alpha['eff_real_unmasked'][:,::-1]
        tmp[np.isnan(tmp)]=0
        s0=np.sort(tmp[alpha['vmask25'][:,::-1]])
        si0=np.argsort(tmp[alpha['vmask25'][:,::-1]])
        #xy_mask_s=xy_mask(si0,:);
        xmask_s=xmask[si0]
        ymask_s=ymask[si0]
        alpha['eff_sorted_vmask25']=s0
        
        #clip out any physically implausible values
        s=s0.copy()
        idx_clip=np.where(s>0.95)[0]
        #idx_nonclip=np.where(s<=0.95)[0]
        s=np.delete(s,idx_clip) #s[idx_nonclip]
        xmask_s=np.delete(xmask_s,idx_clip) #xmask_s[idx_nonclip]
        ymask_s=np.delete(ymask_s,idx_clip) #ymask_s[idx_nonclip]

        x = np.arange(np.prod(s.shape))
        #x_keep=xmask_s[x]
        #y_keep=ymask_s[x]
        
        idx_keep=np.arange(np.round(4*x.shape[0]/5.),x.shape[0],dtype=int)  #keep only top 20% brightest voxels within vmask25 to avoid substantial partial voluming
        max_keep=10 #maximum number of voxels to allow in alpha averaging.  if too many, probably substantial partial voluming
        if(idx_keep.shape[0]>max_keep):
            idx_keep=idx_keep[-max_keep::]
        
        x_keep=xmask_s[idx_keep]
        y_keep=ymask_s[idx_keep]
                
        alpha['robust_alpha']=np.mean(s[idx_keep])
        alpha['robust_alpha_mean']=alpha['robust_alpha']
        alpha['robust_alpha_std']=np.std(s[idx_keep],ddof=1)
        alpha['robust_alpha_max']=np.max(s[idx_keep])
        alpha['robust_alpha_min']=np.min(s[idx_keep])
        alpha['robust_alpha_median']=np.median(s[idx_keep])
        alpha['Nvmask25']=alpha['eff_sorted_vmask25'].shape[0]
        alpha['Nclip']=idx_clip.shape[0]
        alpha['Nkeep']=idx_keep.shape[0]
    
        if generate_figures:
            try:
                #h=figure('Visible','off');
                use_LaTeX=False
                if use_LaTeX:
                    mpl.rc('text', usetex=True)  #enable LaTeX usage in text
                            #set(h,'Position',[2 33 1285 1285])
                pylab.figure(figsize=(13,12),dpi=80)
                
                title_fontdict = {'family' : 'serif',
                        'weight' : 'bold',
                        'size'   : 24}                
                        
                pylab.subplot(2,2,1)
                pylab.imshow(alpha['control_real'][:,::-1].T,cmap=mpl.cm.gray,interpolation='nearest')
                pylab.title('Voxel Overlay',fontdict=title_fontdict)
                pylab.axis('off')
                pylab.hold('on')
                pylab.plot(xmask,ymask,'r.')
                pylab.plot(x_keep,y_keep,'g.')
                pylab.axis('image')
                
                pylab.subplot(2,2,2)
                pylab.imshow(alpha['subtracted_real'][:,::-1].T,cmap=mpl.cm.gray,interpolation='nearest'),pylab.axis('off')
                pylab.title('Control-Tag',fontdict=title_fontdict)
                pylab.hold('on')
                pylab.plot(x_keep,y_keep,'gx')
                pylab.axis('image')
                
                pylab.subplot(2,2,3)
                pylab.imshow(alpha['eff_real_unmasked'][:,::-1].T*alpha['vmask25'][:,::-1].T,cmap=mpl.cm.hot,interpolation='nearest'),pylab.axis('off')
                pylab.title('Alpha Map',fontdict=title_fontdict)
                pylab.hold('on')
                pylab.axis('image')
                
                ax4=pylab.subplot(2,2,4)
                for item in ([ax4.xaxis.label, ax4.yaxis.label]):   #[ax4.title, ax4.xaxis.label, ax4.yaxis.label] + ax4.get_xticklabels() + ax4.get_yticklabels()
                    item.set_fontsize(22)
                for item in (ax4.get_xticklabels() + ax4.get_yticklabels()):
                    item.set_fontsize(16)
                pylab.plot(np.arange(s0.shape[0]),s0,'r.-') #,'LineWidth',2,'MarkerSize',15) #,pylab.axis('off')
                pylab.title('sorted alphas',fontdict=title_fontdict)
                pylab.hold('on')
                pylab.plot(x,s,'b.-') #,'LineWidth',2,'MarkerSize',15) #,pylab.axis('off')
                pylab.plot(x[idx_keep],s[idx_keep],'g.-') #,'LineWidth',2,'MarkerSize',15) #,pylab.axis('off')
                if use_LaTeX:
                    pylab.ylabel(r'$\alpha') #LaTeX
                else:
                    pylab.ylabel('alpha') #LaTeX
                pylab.xlabel('voxel #') #LaTeX
                
                #trick to force the plot in ax4 to be square
                x0,x1 = ax4.get_xlim()
                y0,y1 = ax4.get_ylim()
                ax4.set_aspect((x1-x0)/float(y1-y0))
                
                #tighten up the borders of the subplots
                plt.tight_layout()
                
                #save the image to disk
                figfile=open(pjoin(output_dir,'alpha_details.png'),'wb')
                pylab.savefig(figfile,format='png')
                figfile.close()
            except:
                warnings.warn("Failed to generate figures")

        alpha_fname_old = pjoin(output_dir,'myalpha_old.txt')     #old way where just the brightest voxel in the subtracted image was used
       
        #OLD alpha calculation that just used the brightest voxel in the subtraction image
        mi=np.argmax(np.abs(alpha['subtracted_real']));
        alpha_out=alpha['eff_real_unmasked'].flatten()[mi];
        alpha['old_alpha']=alpha_out
        
        if True:  #write some stats on the range of alphas out to a .csv
            alpha_stats={}   
            alpha_stats['robust_alpha_mean']=alpha['robust_alpha_mean']
            alpha_stats['robust_alpha_std']=alpha['robust_alpha_std']
            alpha_stats['robust_alpha_max']=alpha['robust_alpha_max']
            alpha_stats['robust_alpha_min']=alpha['robust_alpha_min']
            alpha_stats['robust_alpha_median']=alpha['robust_alpha_median']
            dict2csv(alpha_stats,pjoin(output_dir,'alpha_stats.csv')) 
        
        if True: #write alpha dictionary out to disk
            try:
                with open(pjoin(output_dir,"alpha.pickle"), "wb") as f:   
                    pickle.dump(alpha,f)
            except:
                warnings.warn("Failed to write alpha.pickle to disk")
                
        np.savetxt(alpha_fname_old,[alpha_out,],fmt='%0.7g')
        np.savetxt(alpha_fname,[alpha['robust_alpha'],],fmt='%0.7g')
        
        if generate_figures:
            from cmind.globals import has_ImageMagick, has_GraphicsMagick, cmind_imageconvert_cmd
            
            nii_type=16; #floating point
            #flag2D=True
            
            cmind_save_nii_mod_header(alpha_nii,alpha['control_real'],pjoin(output_dir,'alpha_avg_control.nii.gz'),nii_type)
            cmind_save_nii_mod_header(alpha_nii,alpha['subtracted_real'],pjoin(output_dir,'alpha_avg_sub.nii.gz'),nii_type)
            cmind_save_nii_mod_header(alpha_nii,alpha['tag_real'],pjoin(output_dir,'alpha_avg_tag.nii.gz'),nii_type)

            amask=np.abs(alpha['control_real'])+np.abs(alpha['tag_real']); amask=amask>(0.08*np.max(amask));
            cmind_save_nii_mod_header(alpha_nii,alpha['eff_real_unmasked']*amask,pjoin(output_dir,'alpha_map_masked.nii.gz'),nii_type,[0, 1])
            ctmp=np.abs(alpha['control_real'])*amask;
            ctmp=ctmp/np.max(ctmp);
            cmind_save_nii_mod_header(alpha_nii,ctmp,pjoin(output_dir,'alpha_avg_control_masked.nii.gz'),nii_type,[0, 1])
            stmp=np.abs(alpha['subtracted_real'])*amask;
            stmp=stmp/np.max(stmp);
            cmind_save_nii_mod_header(alpha_nii,stmp,pjoin(output_dir,'alpha_avg_sub_masked.nii.gz'),nii_type,[0, 1])
            
            #requires ImageMagick & FSL
            o=output_dir
            
            log_cmd('$FSLDIR/bin/overlay 1 0 %s/alpha_avg_sub_masked 0 1 %s/alpha_map_masked 0.3 1.0 %s/alpha_sub_overlay' % (o,o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/overlay 1 0 %s/alpha_avg_control_masked 0 1 %s/alpha_map_masked 0.3 1.0 %s/alpha_control_overlay' % (o,o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer %s/alpha_control_overlay -n -s 2 -A 300 %s/alpha_control_overlay_tmp.png' % (o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer %s/alpha_avg_control_masked -n -i 0 1 -s 2 -A 300 %s/alpha_control_masked.png' % (o,o), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer %s/alpha_avg_sub_masked -i 0 1 -n -s 2 -A 300 %s/alpha_sub_masked.png' % (o,o), verbose=verbose, logger=module_logger)
            #slicer_tile_slices(pjoin(o,'alpha_control_overlay'),optional_args='-n -s 2',width=300,output_image=pjoin(o,'alpha_control_overlay_tmp.png'))
            #slicer_tile_slices(pjoin(o,'alpha_control_masked'),optional_args='-n -i 0 1 -s 2',width=300)
            #slicer_tile_slices(pjoin(o,'alpha_avg_sub_masked'),optional_args='-n -i 0 1 -s 2',width=300)
            
            if has_ImageMagick or has_GraphicsMagick:
                try:
                    log_cmd('%s $FSLDIR/etc/luts/ramp.gif -resize 200%% %s/cbar.png' % (cmind_imageconvert_cmd, o), verbose=verbose, logger=module_logger)
                    log_cmd('%s -background black -fill white -pointsize 28 label:"  0.3  " %s/cmin.png' % (cmind_imageconvert_cmd, o), verbose=verbose, logger=module_logger)
                    log_cmd('%s -background black -fill white -pointsize 28 label:"  1.0  " %s/cmax.png' % (cmind_imageconvert_cmd, o), verbose=verbose, logger=module_logger)
                    log_cmd('%s %s/cmin.png %s/cbar.png %s/cmax.png +append %s/cbar.png' % (cmind_imageconvert_cmd, o,o, o,o), verbose=verbose, logger=module_logger)
                    log_cmd('$FSLDIR/bin/pngappend %s/alpha_control_overlay_tmp.png - %s/cbar.png %s/alpha_control_overlay.png' % (o,o, o), verbose=verbose, logger=module_logger)
                    log_cmd('rm %s/cmin.png %s/cmax.png %s/cbar.png %s/alpha_control_overlay_tmp.png' % (o,o,o,o), verbose=verbose, logger=module_logger)
                except:
                    warnings.warn("Failed to generate colorbar overlay")

        alpha=alpha['robust_alpha']
    else:
        alpha=np.genfromtxt(alpha_fname)
        
#    writeHTML=True;
#    if writeHTML:
#        from cmind.utils.cmind_HTML_report_gen import cmind_HTML_reports
#        cmind_HTML_reports('alpha',output_dir,report_dir=None,ForceUpdate=ForceUpdate);
      
    #print any filename outputs for capture by LONI
    print("alpha_fname:{}".format(alpha_fname))
    return (alpha_fname, alpha)

def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_dir,cmind_example_output_dir
    output_dir=os.path.join(cmind_example_output_dir,'P','alpha')
    alpha_complex_nii=os.path.join(cmind_example_dir,'IRC04H_06M008_P_1_WIP_ALPHA_MEASURMENT_7_1_complex.nii.gz')
    generate_figures=True
    ForceUpdate=True;
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
    if verbose:
        func=cmind_timer(cmind_alpha,logger=logger)
    else:
        func=cmind_alpha
    func(output_dir,alpha_complex_nii,ForceUpdate=ForceUpdate,generate_figures=generate_figures,verbose=verbose,logger=logger)
    
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:    
        parser = argparse.ArgumentParser(description="ASL labeling efficiency estimation", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory to store output")
        parser.add_argument("-fname","--alpha_complex_nii",required=True, help="filename to the complex alpha .nii.gz volume")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        output_dir=args.output_dir;
        alpha_complex_nii=args.alpha_complex_nii;
        ForceUpdate=args.ForceUpdate;
        generate_figures=args.generate_figures;
        verbose=args.verbose;
        logger=args.logger;
        
        if verbose:
            
            cmind_alpha=cmind_timer(cmind_alpha,logger=logger)
    
        cmind_alpha(output_dir,alpha_complex_nii,generate_figures=generate_figures,ForceUpdate=ForceUpdate,verbose=verbose,logger=logger)