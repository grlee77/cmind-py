.. _usage_LONI:

=============
Usage in LONI
=============

There are a set of shell script wrappers for the 
:ref:`command line<usage_command_line>` pipeline modules that were developed 
for running the pipelines in parallel on the computing
`cluster <http://bmi.cchmc.org/resources/clusters>`_ here at CCHMC.  

.. Warning:: Although, these are provided for reference within the source code 
    distribution within the /site_specific/LONI_shell folder, they are not 
    expected to work as is in other cluster environments without some manual 
    editing.  In particular many of the shell scripts call utilities such as 
    load_fsl_env.sh which tries to a module containing FSL in the cluster.  The 
    particulars of loading each software package in load_*.sh would have to be 
    updated to reflect the local software environment. Additionally, some 
    paths and variables may be hardcoded to values appropriate to the C-MIND 
    study, but may not apply to other studies.

Reference LONI_ .pipe files containing the full physiological and functional
session pipelines that were run via the cluster to generate the files in the
database are available at:  TODO

.. include:: links_names.txt