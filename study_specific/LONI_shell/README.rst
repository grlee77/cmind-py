All global paths and program modules should be setup properly for the local LONI cluster by modifying the various load_XXX_env.sh files accordingly.  The cmind_*.sh modules should not need to be modified.

To run the physio & functional .pipe files, only the following four files should need to be modified:

load_ANTS_env.sh
load_cmind_env.sh
load_fsl_env.sh
load_python_env.sh


The following may also be modified, but are not currently used by the provided .pipe files:

load_slicer4_env.sh
load_mricron_env.sh
load_ffmpeg_env.sh
load_DTIPrep_env.sh