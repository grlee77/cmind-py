#!/bin/bash

# split an AAL atlas into many binary masks,
# one for each region in the atlas

# validate command line arguments
if [ $# -ne 1 -a $# -ne 2 ]
then
  # incorrect number of arguments
  # print usage message and exit
  echo "Usage: ./aal_split.sh aal_atlas_file.nii [output_prefix]"
  echo "output_prefix defaults to aal_atlas_file followed by an underscore"
  exit 1
fi

# strip extension from atlas filename using case-insensitive regex
ORIGINAL_NOCASEMATCH=`shopt -p | grep nocasematch`
shopt -s nocasematch
REGEX='(.*)\.nii(.gz)?'
if [[ "$1" =~ ${REGEX} ]]
then
  ATLAS_FILE="${BASH_REMATCH[1]}"
  ${ORIGINAL_NOCASEMATCH}
else
  ${ORIGINAL_NOCASEMATCH}
  echo "$1 does not appear to be name of a NIFTI file"
  exit 1
fi

# select prefix for output files
if [ "$2" ]
then
  # use second commandline argument if provided
  OUTPUT_PREFIX="$2"
else
  # otherwise default to name of atlas file
  OUTPUT_PREFIX="${ATLAS_FILE}_"
fi

# get the number of regions to split
REGEX='[0-9]+\.0* ([0-9]+)\.0*'
if [[ `fslstats "${ATLAS_FILE}" -R` =~ ${REGEX} ]]
then
  NREGIONS=${BASH_REMATCH[1]}
else
  echo "problem calling fslstats on ${ATLAS_FILE}"
  exit 1
fi
echo "${ATLAS_FILE} contains ${NREGIONS} regions"

# how many digit placeholders are needed?
NDIGITS=4 #${#NREGIONS}

# loop & create a mask for each region
i=1
while [ $i -le ${NREGIONS} ]
do
  OUTPUT_FILE="${OUTPUT_PREFIX}`printf %0${NDIGITS}d $i`"
  fslmaths "${ATLAS_FILE}" -sub `expr $i - 1` -thr 1 -uthr 1.5  "${OUTPUT_FILE}"
  if [ $? -ne 0 ]
  then
    echo "problem calling fslmaths on ${ATLAS_FILE}"
    exit 1
  fi
  echo "created ${OUTPUT_FILE} for region $i"
  i=`expr $i + 1`
done
