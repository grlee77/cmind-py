#!/usr/bin/env python
"""utility to autogenerate a list of standard volumes and transforms for the
CMIND study.  Creates a file reg_struct.csv with the following rows

#The following entries correspond to the age-specific template and brain masks:
StudyTemplate,<filename>
StudyTemplate_brain,<filename>
StudyTemplate_brain_mask,<filename>

#additional, optional cerebrum masks priors
StudyTemplate_cerebrum,<filename>
StudyTemplate_cerebrum_mask,<filename>

#optional tissue priors
StudyTemplate_pve1_prior,<filename>
StudyTemplate_pve2_prior,<filename>
StudyTemplate_pve3_prior,<filename>

#Nonlinear affine & warp to MNI space (precomputed)
ANTS_StudyTemplate_to_MNI_Aff,<filename>
ANTS_StudyTemplate_to_MNI_Warp,<filename>

#StudyTemplate warped to MNI space (precomputed)
ANTS_StudyTemplate_to_MNI_img,<filename>

#Linear affine to MNI space (precomputed) 
FLIRT_StudyTemplate_to_MNI_Aff,<filename>

#StudyTemplate affine transformed to MNI space (precomputed)
FLIRT_StudyTemplate_to_MNI_img,<filename>

"""

from __future__ import division, print_function, absolute_import

def cmind_age_to_template(age_months,ped_template_path,output_dir,resolution_string='2mm',ForceUpdate=False, verbose=False, logger=None):
    """choose appropriate pediatric template based on the subject age
    
    Parameters
    ----------     
    age_months : float
        age of subject in months
    ped_template_path : directory
        path to the directory containing the pediatric templates
    output_dir : str
        output directory in which to store reg_struct.csv
    resolution_str : {'1mm','2mm'}, optional
        resolution of the templates to use (default = '2mm')
    ForceUpdate : bool, optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
            
    Returns
    -------
    reg_struct : dict
        dictionary of registration related atlases and transforms
    reg_struct_file : str
        filename of the .csv file containing reg_struct
    
    Notes
    -----
    initialize a registration structure, reg_struct and save it to reg_struct.csv in output_dir.
    
    based on age, a specific study-specific template will be assigned
    
      
    .. figure::  ../img/00to06_brain_2mm.png
       :align:   center
    
       study template for `age_months` < 6 months
    
    .. figure::  ../img/06to24_brain_2mm.png
       :align:   center
    
       study template for 6 < `age_months` < 24

    .. figure::  ../img/24to48_brain_2mm.png
       :align:   center
    
       study template for 24 < `age_months` < 48

    .. figure::  ../img/48to216_brain_2mm.png
       :align:   center
    
       study template for `age_months` > 48

    
    """
    import os
    import warnings
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import dict2csv, csv2dict
    from cmind.utils.logging_utils import cmind_init_logging, cmind_func_info

    #TODO:  enable using XML or JSON in addition to CSV
    
    #convert various strings to boolean
    ForceUpdate, verbose = input2bool([ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    
    #logger_info(logger)  
    #logger_info(module_logger)  
    
    module_logger.info("Choosing Age Appropriate Template")
                                            
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    reg_struct_file=os.path.join(output_dir,'reg_struct.csv')  
        
    if (not os.path.exists(reg_struct_file)) or ForceUpdate:
        #RIGID_template_path=os.path.join(ped_template_path,'RIGID')
        #FLIRT_template_path=os.path.join(ped_template_path,'FLIRT')
        #FNIRT_template_path=os.path.join(ped_template_path,'FNIRT')
        #ANTS_template_path=os.path.join(ped_template_path,'FNIRT')
        
        #Age-dependent paths to the atlases and transforms
        
        age_months=float(age_months)
        
        if age_months<=6.0:
            age_str='00to06'
        elif (age_months>6) and (age_months<=24):
            age_str='06to24'
        elif (age_months>24) and (age_months<=36):
            age_str='24to48'
        elif (age_months>36) and (age_months<=252):
            age_str='48to216'
    
        RIGID_template_path=os.path.join(ped_template_path,age_str,'RIGID')
        FLIRT_template_path=os.path.join(ped_template_path,age_str,'FLIRT')
        
        if os.path.exists(reg_struct_file): #insert values into previously existing dictionary
            reg_struct=csv2dict(reg_struct_file)
        else: #initialize registration dictionary    
            reg_struct={}
        
        ANTS_template_path=os.path.join(ped_template_path,age_str, 'SyN')
        SyN_affine=os.path.join(ANTS_template_path,age_str + '_to_MNI_brain_' + resolution_string + '_ANTS_SyN_0GenericAffine.mat')
        SyN_warp=os.path.join(ANTS_template_path,age_str + '_to_MNI_brain_' + resolution_string + '_ANTS_SyN_1Warp.nii.gz')
        SyN_deformed=os.path.join(ANTS_template_path,age_str + '_to_MNI_brain_' + resolution_string + '_ANTS_SyN_Deformed.nii.gz') 
        if os.path.exists(SyN_warp):
            reg_struct['ANTS_StudyTemplate_to_MNI_Aff']=SyN_affine #Affine transform from ANTS
            reg_struct['ANTS_StudyTemplate_to_MNI_Warp']=SyN_warp #Nonlinear Warp from ANTS
            reg_struct['ANTS_StudyTemplate_to_MNI_img']=SyN_deformed#Nonlinear Warp from ANTS
            
        reg_struct['FLIRT_StudyTemplate_to_MNI_Aff']=os.path.join(FLIRT_template_path,age_str + '_' + resolution_string + '_to_MNI.txt')  #Affine transform from FSL's FLIRT
        reg_struct['FLIRT_StudyTemplate_to_MNI_img']=os.path.join(FLIRT_template_path,age_str + '_template_brain_mask_' + resolution_string + '_to_MNI.nii.gz')  #FSL Affine transformed image
        #reg_struct['nonFLIRT_StudyTemplate_to_MNI_Warp']=os.path.join(FNIRT_template_path,age_str + '_to_std_nonlin_field.nii.gz')
        #reg_struct['nonFLIRT_StudyTemplate_to_MNI_img']=os.path.join(FNIRT_template_path,age_str + '_to_std_nonlin.nii.gz')
        reg_struct['StudyTemplate']=os.path.join(RIGID_template_path,age_str + '_template_' + resolution_string + '.nii.gz')
        reg_struct['StudyTemplate_brain']=os.path.join(RIGID_template_path,age_str + '_template_brain_' + resolution_string + '.nii.gz')
        reg_struct['StudyTemplate_brain_mask']=os.path.join(RIGID_template_path,age_str + '_template_brain_mask_' + resolution_string + '.nii.gz')
        reg_struct['StudyTemplate_cerebrum']=os.path.join(RIGID_template_path,age_str + '_template_cerebrum_' + resolution_string + '.nii.gz')
        reg_struct['StudyTemplate_cerebrum_mask']=os.path.join(RIGID_template_path,age_str + '_template_cerebrum_mask_' + resolution_string + '.nii.gz')
        
        pve1_file=os.path.join(RIGID_template_path,age_str + '_template_pve1_' + resolution_string + '.nii.gz')      
        pve2_file=os.path.join(RIGID_template_path,age_str + '_template_pve2_' + resolution_string + '.nii.gz')      
        pve3_file=os.path.join(RIGID_template_path,age_str + '_template_pve3_' + resolution_string + '.nii.gz')      
        if os.path.exists(pve1_file) and os.path.exists(pve2_file) and os.path.exists(pve3_file):
            reg_struct['StudyTemplate_pve1_prior']=pve1_file
            reg_struct['StudyTemplate_pve2_prior']=pve2_file
            reg_struct['StudyTemplate_pve3_prior']=pve3_file  
            module_logger.info('tissue priors found')
       
        Nwarn=0;    
        for key,val in list(reg_struct.items()): #check existence of files
            if not isinstance(val,(list,tuple)):
                if os.path.splitext(val)[1]:  #if value has an extension, assume it is a file and then check whether it exists
                    if not os.path.exists(val):
                        warnings.warn("Atlas file {}: {} doesn't exist!".format(key, val))
                        Nwarn+=1
    
        if (Nwarn>0):
            raise IOError("some of the specified atlas files do not exist!")

        dict2csv(reg_struct,reg_struct_file)
            
    else:  #reload previously computed output
        reg_struct=csv2dict(reg_struct_file)

    if verbose:
        pass
        #dprint(reg_struct)        
        
    return (reg_struct, reg_struct_file)

def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_template_dir
    age_months=84.7
    ped_template_path=cmind_template_dir
    resolution_string='2mm'
    ForceUpdate=True;
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')        
    
    if verbose:
        func=cmind_timer(cmind_age_to_template,logger=logger)
    else:
        func=cmind_age_to_template

    testcases=['P','F']
    for testcase in testcases:    
        if testcase=='F':
            output_dir=os.path.join(cmind_example_output_dir,'F','T1_proc');
        elif testcase=='P':
            output_dir=os.path.join(cmind_example_output_dir,'P','T1_proc');
            
        func(age_months,ped_template_path,output_dir,resolution_string=resolution_string,ForceUpdate=ForceUpdate,verbose=verbose,logger=logger)
   
    
if __name__ == '__main__':
    import sys,argparse
    from cmind.utils.utils import _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="choose appropriate pediatric template based on the subject age", epilog="")  #-h, --help exist by default
        parser.add_argument("-age","--age_months",required=True,type=float, help="age of subject in months")
        parser.add_argument("-tdir","-template_dir","--ped_template_path",required=True, help="path to the directory containing the pediatric templates")
        parser.add_argument("-o","--output_dir",required=True, help="output directory in which to store reg_struct.csv")
        parser.add_argument("-res","--resolution_string", type=str, default="2mm", help="resolution of the templates to use (default = '2mm')")
        #parser.add_argument("-u","--ForceUpdate", action="store_true", default=False, help="Overwrite any existing fits that may reside in the data directory")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help='if True, rerun and overwrite any previously existing results')
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
          
        args=parser.parse_args()
        
        age_months=args.age_months;
        ped_template_path=args.ped_template_path;
        output_dir=args.output_dir;
        resolution_string=args.resolution_string;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger
    
        if verbose:
            cmind_age_to_template=cmind_timer(cmind_age_to_template,logger=logger)
            
        cmind_age_to_template(age_months,ped_template_path,output_dir,resolution_string=resolution_string,ForceUpdate=ForceUpdate,verbose=verbose,logger=logger)
                    
