#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $cwd/exec_exists.sh

if exec_exists module;
then
	#echo "found module"
	module load ffmpeg
else
	if exec_exists ffmpeg;
	then
		#echo "module not found, but ffmpeg already in path"
		echo ""
	else
		echo "Error:  module not found, and ffmpeg not already on path"
		exit 1
	fi
fi

