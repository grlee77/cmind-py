#!/usr/bin/env python
from __future__ import division, print_function, absolute_import


def _arg_parser():
    import argparse
    parser = argparse.ArgumentParser(description="print package & external software version information", epilog="")  #-h, --help exist by default
    parser.add_argument("-a","--all", dest="extended_info",help="Include additional packges in the output", action="store_true")
    
    return parser
    
if __name__ == '__main__':
    from cmind.globals import print_environment_info
    parser = _arg_parser()
        
    args=parser.parse_args()

    print_environment_info(extended_info=args.extended_info)