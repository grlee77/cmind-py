#!/bin/bash
#cmind_T1map_fit.m\
echo "CMD: $*"

usage()
{
echo "Basic usage: cmind_T1_tissue_averages.py -o <output_dir> -t1map <T1_map_file> -cbfmf <BaselineCBF_mean_file> -t12cbfa <T1concat2CBF_affine> -cbfq <CBF_quant_file> -nrmse <NRMSE_file> -gm <GMpve> -wm <WMpve> -csf <CSFpve>  [options]

usage: cmind_T1_tissue_averages.py [-h] -o OUTPUT_DIR -t1map T1_MAP_FILE
                                   -cbfmf BASELINECBF_MEAN_FILE -t12cbfa
                                   T1CONCAT2CBF_AFFINE -cbfq CBF_QUANT_FILE
                                   -nrmse NRMSE_FILE -gm GMPVE -wm WMPVE -csf
                                   CSFPVE [-cbfpf CBF_PERCENT_FILE]
                                   [-aff HRTOCBF_AFFINE] [-w HRTOCBF_WARP]
                                   [-gf GENERATE_FIGURES] [-u FORCEUPDATE]
                                   [-v] [-log LOGGER]

Calculate average T1 values for each tissue type (GM, WM, CSF)

required_arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -cbfq CBF_QUANT_FILE, --CBF_quant_file CBF_QUANT_FILE
                        quantitative ASL image
  
  
  -fieldmap_reg_dir DIRECTORY
                        directory containing regBBR registration results (default filenames)  
        OR
  -aff HRTOCBF_AFFINE, -hr2cbfa HRTOCBF_AFFINE, --HRtoCBF_affine HRTOCBF_AFFINE
                        affine transform: structural->BaselineCBF

        OR
  -w HRTOCBF_WARP, -hr2cbfw HRTOCBF_WARP, --HRtoCBF_warp HRTOCBF_WARP
                        warp: structural->BaselineCBF


  -t1map_dir DIRECTORY
                        directory containing T1map processed results (default filenames)  
        OR
  -t1map T1_MAP_FILE, --T1_map_file T1_MAP_FILE
                        image file containing the T1map fitting results
  -nrmse NRMSE_FILE, --NRMSE_file NRMSE_FILE
                        normalized root-mean-square error (NRMSE) image from
                        T1_map fit

  -t1proc_dir DIRECTORY
                        directory containing anatomical segmentation results (default filenames)  
        OR
  -gm GMPVE, --GMpve GMPVE
                        GM partial volume estimate from structural processing
  -wm WMPVE, --WMpve WMPVE
                        WM partial volume estimate from structural processing
  -csf CSFPVE, --CSFpve CSFPVE
                        CSF partial volume estimate from structural processing

  -CBF_pre_dir DIRECTORY
                        directory containing baselineCBF results (default filenames)  


optional arguments:
  -h, --help            show this help message and exit
  -cbfpf CBF_PERCENT_FILE, --CBF_percent_file CBF_PERCENT_FILE
                        ASL control-tag signal percentage image
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=12
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh


#Assign input arguments to variables

output_dir="default"
ForceUpdate=true

while [ "$1" != "" ]; do
    case $1 in
	-fieldmap_reg_dir )
	    shift
	    fieldmap_reg_dir=$1
	    ;;
	-t1map_dir )
	    shift
	    t1map_dir=$1
	    ;;
	-t1proc_dir )
	    shift
	    t1proc_dir=$1
	    ;;
	-cbf_pre_dir )
	    shift
	    CBF_pre_dir=$1
	    ;;
	-o|--output_dir )
        shift
        output_dir=$1
        ;;
    -t1map|--T1_map_file )
        shift
        T1_map_file=$1
        ;;
    -cbfq|--CBF_quant_file )
        shift
        CBF_quant_file=$1
        ;;
    -nrmse|--NRMSE_file )
        shift
        NRMSE_file=$1
        ;;
    -gm|--GMpve )
        shift
        GMpve=$1
        ;;
    -wm|--WMpve )
        shift
        WMpve=$1
        ;;
    -csf|--CSFpve )
        shift
        CSFpve=$1
        ;;
    -cbfpf|--CBF_percent_file )
        shift
        CBF_percent_file=$1
        ;;
    -aff|-hr2cbfa|--HRtoCBF_affine )
        shift
        HRtoCBF_affine=$1
        ;;
    -w|-hr2cbfw|--HRtoCBF_warp )
        shift
        HRtoCBF_warp=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
	
    esac
    shift
done

if [ ! -z $verbose_string ]; then
  debug=true
else
  debug=false
fi

if [ ! -z $fieldmap_reg_dir ]
then

    if [ -z "${HRtoCBF_affine+xxx}" ] || [ ! -f $HRtoCBF_affine ]
    then
        HRtoCBF_affine=${fieldmap_reg_dir}/lowres2highres_inv.mat
        if [ ! -f $HRtoCBF_affine ]
        then
             HRtoCBF_affine=None
        fi
    fi

    if [ -z "${HRtoCBF_warp+xxx}" ] || [ ! -f $HRtoCBF_warp ]
    then
        HRtoCBF_warp=${fieldmap_reg_dir}/lowres2highres_warp_inv.nii.gz
        if [ ! -f $HRtoCBF_warp ]
        then
             HRtoCBF_warp=None
        fi
    fi
fi

if [ ! -z $t1map_dir ]
then
    if [ -z "${T1_map_file+xxx}" ] || [ ! -f $T1_map_file ]
    then
        T1_map_file=${t1map_dir}/T1_map.nii.gz
    fi

    if [ -z "${NRMSE_file+xxx}" ] || [ ! -f $NRMSE_file ]
    then
        NRMSE_file=${t1map_dir}/T1_NRMSE_map.nii.gz
    fi
fi


if [ -z "${BaselineCBF_mean_file+xxx}" ] || [ ! -f $BaselineCBF_mean_file ]
then
    BaselineCBF_mean_file=${CBF_pre_dir}/MeanSub_BaselineCBF_N4.nii.gz
fi

if [ -z "${CBF_percent_file+xxx}" ] || [ ! -f $CBF_percent_file ]
then
    CBF_percent_file=${CBF_pre_dir}/BaselineCBF_mcf_percent_change.nii.gz
fi

if [ -z "${GMpve+xxx}" ] || [ ! -f $GMpve ]
then
    GMpve=${t1proc_dir}/T1Segment_GM.nii.gz
fi

if [ -z "${WMpve+xxx}" ] || [! -f $WMpve ]
then
    WMpve=${t1proc_dir}/T1Segment_WM.nii.gz
fi

if [ -z "${CSFpve_affine+xxx}" ] || [ ! -f $CSFpve ]
then
    CSFpve=${t1proc_dir}/T1Segment_CSF.nii.gz
fi



if [ $output_dir = "default" ]; then
    output_dir=`dirname $T1_map_file`
    output_dir=$output_dir/../T1_tissue_averages    
fi

if $debug
then
    echo "output_dir:$output_dir"
    echo "T1_map_file:$T1_map_file"
    echo "BaselineCBF_mean_file:$BaselineCBF_mean_file"
    echo "CBF_quant_file:$CBF_quant_file"
    echo "NRMSE_file:$NRMSE_file"
    echo "GMpve:$GMpve"
    echo "WMpve:$WMpve"
    echo "CSFpve:$CSFpve"
    echo "CBF_percent_file:$CBF_percent_file"
    echo "HRtoCBF_affine:$HRtoCBF_affine"
    echo "HRtoCBF_warp:$HRtoCBF_warp"
    echo "ForceUpdate:$ForceUpdate"
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir T1_map_file CBF_quant_file NRMSE_file GMpve WMpve CSFpve CBF_percent_file HRtoCBF_affine HRtoCBF_warp generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_T1_tissue_averages.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_T1_tissue_averages.py ${verbose_string} ${all_optional_args}
printf 'cmind_T1_tissue_averages.py:  Elapsed time: %s\n' $(timer $t)

#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    T1_avg_file
#    CBF_avg_file


# t=$(timer)

# echo "python ${cmind_python_dir}/cmind_T1_tissue_averages.py $output_dir $T1_map_file $BaselineCBF_mean_file $T1concat2CBF_affine $CBF_quant_file $NRMSE_file $GMpve $WMpve $CSFpve --CBF_percent_file $CBF_percent_file --HRtoCBF_affine $HRtoCBF_affine --HRtoCBF_warp $HRtoCBF_warp --ForceUpdate $ForceUpdate"
# python ${cmind_python_dir}/cmind_T1_tissue_averages.py $output_dir $T1_map_file $BaselineCBF_mean_file $T1concat2CBF_affine $CBF_quant_file $NRMSE_file $GMpve $WMpve $CSFpve --CBF_percent_file $CBF_percent_file --HRtoCBF_affine $HRtoCBF_affine --HRtoCBF_warp $HRtoCBF_warp --ForceUpdate $ForceUpdate --verbose

# printf 'T1_tissue_averages:  Elapsed time: %s\n' $(timer $t)

#check that T1_tissue_averages.nii.gz exists
meanT1=${output_dir}/mean_T1s_NRMSE_masked.txt

echo "$meanT1"
if [ ! -f $meanT1 ]; then
    echo "ERROR: Failed to finish T1_tissue_averages"
    exit 1
fi 

echo "Finished cmind_T1_tissue_averages"

