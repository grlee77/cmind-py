#!/usr/bin/env python
from __future__ import division, print_function, absolute_import
#
#def _move_segment_iter(WM_vol, GM_vol, CSF_vol,Seg_img,iter_no,image_only=True, verbose=False, logger=None):
#    """copy/rename files from a previous segmentation iteration to prepare for the next
#    """
#    
#    import os
#    import warnings
#    
#    from cmind.utils.file_utils import split_multiple_ext
#    from cmind.utils.logging_utils import log_cmd
#
#    iter_no=int(iter_no)
#    
#    if not image_only: #keep copies of segmentation results, wither iteration number appended to names
#        WM_vol_previous_iter=split_multiple_ext(WM_vol)[0]+'_iter%04d' % iter_no
#        GM_vol_previous_iter=split_multiple_ext(GM_vol)[0]+'_iter%04d' % iter_no
#        CSF_vol_previous_iter=split_multiple_ext(CSF_vol)[0]+'_iter%04d' % iter_no
#        
#        log_cmd('$FSLDIR/bin/immv %s %s' % (WM_vol,WM_vol_previous_iter), verbose=verbose, logger=logger)
#        log_cmd('$FSLDIR/bin/immv %s %s' % (GM_vol,GM_vol_previous_iter), verbose=verbose, logger=logger)
#        log_cmd('$FSLDIR/bin/immv %s %s' % (CSF_vol,CSF_vol_previous_iter), verbose=verbose, logger=logger)
#    else:  #no copies of intermediate iterations will be kept
#       WM_vol_previous_iter=WM_vol
#       GM_vol_previous_iter=GM_vol
#       CSF_vol_previous_iter=CSF_vol        
#   
#    #keep a copy of the summary image for each iteration
#    try:
#        if os.path.exists(Seg_img):
#            log_cmd('mv %s %s' % (Seg_img,split_multiple_ext(Seg_img)[0]+'_iter%04d' % iter_no + '.png'), verbose=verbose, logger=logger)
#    except:
#        warnings.warn("failed to move segmentation summary images from previous iteration")
#        
#    return (WM_vol_previous_iter,GM_vol_previous_iter,CSF_vol_previous_iter)
                  
                 
def cmind_segment_atropos(fname_brain, fname_head, fname_brain_mask, additional_intensity_vols=[],output_dir='', reg_struct=None, Nclasses=3, priors_dir=None, prior_weight=0.10, resegment_using_N4mask=False, ANTS_bias_cmd=None, generate_figures=False, ForceUpdate=False, verbose=False, logger=None):
    """ Segmentation using FSL's FAST
    
    Parameters
    ----------
    fname_brain : str
        input T1-weighted brain volume
    fname_head : str
        input T1-weighted head volume
    fname_brain_mask : str
        input T1-weighted brain mask
    additional_intensity_vols : str or list of str, optional
        additional volumes (e.g. T2-weighted) to use during segmentation 
        (additional -a option inputs to Atropos)
    output_dir : str, optional
        output directory for the segmentation
    reg_struct : str or dict, optional
        registration structure
    Nclasses : int, optional
        number of classes to segment.  If priors_dir is specified, there must
        also be at least this many priors
    priors_dir : str or None, optional
        directory containing the priors.  Prior files must be named prior*.nii.gz
    prior_weight : float, optional
        weight applied to the priors during segmentation.  valid range: [0 1]
    resegment_using_N4mask : bool, optional
        rerun N4 bias correction on a mask weighted toward white matter then resegment
    ANTS_bias_cmd : str, optional
        path to ANTs N4BiasFieldCorrection binary (used if resegment_using_N4mask=True)
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    WM_vol : str
        WM partial volume estimate
    GM_vol : str
        GM partial volume estimate
    CSF_vol : str
        CSF partial volume estimate
    DeepGM_vol : str or None
        Deep gray matter partial volume estimate
    Brainstem_vol : str or None
        Brainstem partial volume estimate
    Cerebellar_vol : str or None
        Cerebellar partial volume estimate
        
    Notes
    -----
      
    .. figure::  ../img/Segmentation_Slices_squeezed.png
       :align:   center
    
       Examples of segmentation partial volume estimates (Top: CSF, Middle: GM, Bottom: WM)
        
    """

    import os, glob, shutil, tempfile
    from os.path import join as pjoin
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    #from cmind.pipeline.cmind_segment import _move_segment_iter
    from cmind.pipeline.ants_applywarp import ants_applywarp
    from cmind.globals import cmind_ANTs_dir
        
    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, resegment_using_N4mask = input2bool([generate_figures, ForceUpdate, verbose, resegment_using_N4mask])
    
#    if resegment_using_N4mask:  #TODO: move old outputs before rerunning so that ForceUpdate=True flag can be removed
#        from cmind.pipeline.cmind_bias_correct import cmind_bias_correct
#        
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Segmentation")      
            
    if additional_intensity_vols:
        if isinstance(additional_intensity_vols,str): #convert to list
            additional_intensity_vols=[additional_intensity_vols,]
        for i,vol in enumerate(additional_intensity_vols):
            imbool,vol_full=imexist(vol)[0:2]
            additional_intensity_vols[i]=vol_full #make sure all filenames have the full file extension
            if not imbool:
                raise ValueError("additional intensity volume, {}, not found".format(vol))
                
#    if verbose:
#        from time import time
#        print "Starting Segmentation with FSL's FAST"
#        starttime=time()
    
    (imbool, fname_brain, fname_brain_root, fname_brain_path, fname_brain_ext)=imexist(fname_brain,'nifti-1')
    if not imbool:
        raise IOError("input fname_brain not found!")

    (imbool, fname_head)=imexist(fname_head,'nifti-1')[0:2]
    if not imbool:
        raise IOError("input fname_head not found!")

    (imbool, fname_brain_mask)=imexist(fname_brain_mask,'nifti-1')[0:2]
    if not imbool:
        raise IOError("input fname_brain_mask not found!")


    if priors_dir is not None:
        sn='AtroposSeg'
    else:
        sn='AtroposSegNoPriors'
        
    if not output_dir:
        seg_name=os.path.join(fname_brain_path,'AtroposPriors',sn)
        output_dir=os.path.dirname(seg_name)
    else:
        seg_name=os.path.join(output_dir,sn)
        
#    if not os.path.dirname(seg_name):
#        seg_name=os.path.join(fname_brain_path,'AtroposPriors',seg_name)
        
    prior_workingdir=None
    if priors_dir is not None:
        if not os.path.isdir(priors_dir):
            raise ValueError("Specified priors directory does not exist")
        else:
            prior_filelist=sorted(glob.glob(pjoin(priors_dir,'prior*.nii.gz')))
            Npriors_found=len(prior_filelist)
            if Npriors_found<Nclasses:
                raise ValueError("Expected {} priors, but found {}".format(Nclasses,Npriors_found))
            elif Npriors_found>Nclasses:
                #copy only the first Nclasses of the priors to a temporary folder
                prior_filelist=prior_filelist[:Nclasses]
                prior_workingdir=tempfile.mkdtemp()
                for f in prior_filelist:
                    shutil.copy(f,prior_workingdir)
                priors_dir=prior_workingdir
    
    sn=os.path.basename(seg_name)
     
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    CSF_vol=os.path.join(output_dir,seg_name+'_CSF'+fname_brain_ext)
    GM_vol=os.path.join(output_dir,seg_name+'_GM'+fname_brain_ext)
    WM_vol=os.path.join(output_dir,seg_name+'_WM'+fname_brain_ext)
    
    
    if priors_dir is not None:
        if Nclasses>3:
            DeepGM_vol=os.path.join(output_dir,seg_name+'_DeepGM'+fname_brain_ext)
        else:
            DeepGM_vol=None
        if Nclasses>4:
            Brainstem_vol=os.path.join(output_dir,seg_name+'_Brainstem'+fname_brain_ext)
        else:
            Brainstem_vol=None
        if Nclasses>5:
            Cerebellar_vol=os.path.join(output_dir,seg_name+'_Cerebellum'+fname_brain_ext)
        else:
            Cerebellar_vol=None
    else:
        DeepGM_vol=None
        Brainstem_vol=None
        Cerebellar_vol=None

        

        
    
    if (not imexist(CSF_vol,'nifti-1')[0]) or ForceUpdate:
        # run segmentation
    
        #Warp the StudyTemplate priors into the subject's Structural space
        if priors_dir is not None:
            if isinstance(reg_struct,str):
                if os.path.exists(reg_struct):
                    reg_struct=csv2dict(reg_struct)
                else:
                    raise IOError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
            elif not isinstance(reg_struct,dict):
                raise IOError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
            
            #note: these were 2mm transforms, but still appear to be able to use them to transform from 1mm atlas->1mm subject
    #        transform_list=[HRtoStudyTemplate_Affine,
    #                        HRtoStudyTemplate_Warp]
            transform_list=reg_struct['ANTS_HR_to_StudyTemplate_transform_list']
    
            moving = fname_head
            fixed_list = glob.glob(os.path.join(priors_dir,'prior*.nii.gz'))
            #fixed_list = [item for item in fixed_list if item.find('Warp')==-1]
        
            for fixed in fixed_list:
                output_warped=os.path.join(output_dir,os.path.basename(fixed))
                if not os.path.isfile(output_warped):
                    ants_applywarp(transform_list,fixed,moving,output_warped,ANTS_path=None,dim=3,interp_type="",nopng=False,inverse_flag=True, verbose=True)
        
        #Run Atropos segmentation
        Atropos_cmd=pjoin(cmind_ANTs_dir,'Atropos')

        intensity_images="-a {}".format(fname_head)
        if additional_intensity_vols:
            for vol in additional_intensity_vols:
                intensity_images += " -a {}".format(vol)

        if priors_dir is None:
            log_cmd('%s -d 3 -x %s -c [5,1.e-3] -m [0.10,1x1x1] -u 0 --icm=[1,1] %s -o [%s.nii.gz,%sProb%%02d.nii.gz] -i KMeans[%d] -p Socrates[1]' % (Atropos_cmd,fname_brain_mask,intensity_images,seg_name,seg_name, Nclasses),verbose=verbose,logger=module_logger)
        else:
            log_cmd('%s -d 3 -x %s -c [5,1.e-3] -m [0.10,1x1x1] -u 0 --icm=[1,1] %s -o [%s.nii.gz,%sProb%%02d.nii.gz] -i PriorProbabilityImages[%d,%s/priors%%d.nii.gz,%f] -p Socrates[1]' % (Atropos_cmd,fname_brain_mask,intensity_images,seg_name,seg_name,Nclasses,output_dir,prior_weight),verbose=verbose,logger=module_logger)
            
        #Rename the outputs
        log_cmd('$FSLDIR/bin/immv %sProb01 %s' % (seg_name,CSF_vol), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/immv %sProb02 %s' % (seg_name,GM_vol), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/immv %sProb03 %s' % (seg_name,WM_vol), verbose=verbose, logger=module_logger)
        if DeepGM_vol:
            log_cmd('$FSLDIR/bin/immv %sProb04 %s' % (seg_name,DeepGM_vol), verbose=verbose, logger=module_logger)
        if Brainstem_vol:
            log_cmd('$FSLDIR/bin/immv %sProb05 %s' % (seg_name,Brainstem_vol), verbose=verbose, logger=module_logger)
        if Cerebellar_vol:
            log_cmd('$FSLDIR/bin/immv %sProb06 %s' % (seg_name,Cerebellar_vol), verbose=verbose, logger=module_logger)


        # binary masks where the partial volume of WM is >=0.5
        WM_50pct=os.path.join(output_dir,seg_name+'_WM_50pct')
        log_cmd('$FSLDIR/bin/fslmaths %s -thr 0.5 -bin %s' % (WM_vol, WM_50pct), verbose=verbose, logger=module_logger)
        #fslmaths ${seg_name}_WMorCSF  -thr 0.5 -bin ${seg_name}_WMorCSF_50pct
        
        #create a WM edge mask for overlay in later BBR-based registrations
        
        WM_edge=os.path.join(output_dir,seg_name+'_WMedge')
        log_cmd('$FSLDIR/bin/fslmaths %s -edge -bin -mas %s %s' % (WM_50pct,WM_50pct,WM_edge), verbose=verbose, logger=module_logger)
        
        if generate_figures:
            from cmind.utils.cmind_utils import cmind_reg_report_img

            if priors_dir:
                pstr='' #'_Prior'
            else:
                pstr=''

            use_edge_overlay=False
            CSF_img=os.path.join(output_dir,'CSF%s_slices.png' % pstr)
            GM_img=os.path.join(output_dir,'GM%s_slices.png' % pstr)
            WM_img=os.path.join(output_dir,'WM%s_slices.png' % pstr)
            Seg_img=os.path.join(output_dir,'Segmentation%s_Slices.png' % pstr)
            if use_edge_overlay:
                cmind_reg_report_img(CSF_vol, fname_brain, CSF_img,fewer_flag=3)
                cmind_reg_report_img(GM_vol, fname_brain, GM_img,fewer_flag=3)
                cmind_reg_report_img(WM_vol, fname_brain, WM_img,fewer_flag=3)            
            else:
                log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (CSF_vol,CSF_img), verbose=verbose, logger=module_logger)
                log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (GM_vol,GM_img), verbose=verbose, logger=module_logger)
                log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (WM_vol,WM_img), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/pngappend %s - %s - %s %s' % (CSF_img,GM_img,WM_img,Seg_img), verbose=verbose, logger=module_logger)

            os.remove(CSF_img)
            os.remove(GM_img)
            os.remove(WM_img)
            WMedge_img=os.path.join(output_dir,'WMedge%s_slices.png' % pstr)
            log_cmd('$FSLDIR/bin/slicer %s %s_WM_50pct -a %s' % (fname_brain, seg_name, WMedge_img), verbose=verbose, logger=module_logger)
            
            if priors_dir:
                DeepGM_img=os.path.join(output_dir,'DeepGM%s_slices.png' % pstr)
                Brainstem_img=os.path.join(output_dir,'Brainstem%s_slices.png' % pstr)
                Cerebellar_img=os.path.join(output_dir,'Cerebellar%s_slices.png' % pstr)
                Seg_img2=os.path.join(output_dir,'Segmentation%s_Slices_OtherPVE.png' % pstr)
                if use_edge_overlay:
                    if DeepGM_vol:
                        cmind_reg_report_img(DeepGM_vol, fname_brain, DeepGM_img,fewer_flag=3)
                    if Brainstem_vol:
                        cmind_reg_report_img(Brainstem_vol, fname_brain, Brainstem_img,fewer_flag=3)
                    if Cerebellar_vol:
                        cmind_reg_report_img(Cerebellar_vol, fname_brain, Cerebellar_img,fewer_flag=3)                         
                else:
                    if DeepGM_vol:
                        log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (DeepGM_vol,DeepGM_img), verbose=verbose, logger=module_logger)
                    if Brainstem_vol:
                        log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (Brainstem_vol,Brainstem_img), verbose=verbose, logger=module_logger)
                    
                if Cerebellar_vol:
                    log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (Cerebellar_vol,Cerebellar_img), verbose=verbose, logger=module_logger)
                    log_cmd('$FSLDIR/bin/pngappend %s - %s - %s %s' % (DeepGM_img,Brainstem_img,Cerebellar_img,Seg_img2), verbose=verbose, logger=module_logger)
                    log_cmd('$FSLDIR/bin/pngappend %s + %s %s' % (Seg_img,Seg_img2,Seg_img), verbose=verbose, logger=module_logger)
                    os.remove(DeepGM_img)
                    os.remove(Brainstem_img)
                    os.remove(Cerebellar_img)
                    os.remove(Seg_img2)
                elif Brainstem_vol:
                    log_cmd('$FSLDIR/bin/pngappend %s - %s %s' % (DeepGM_img,Brainstem_img,Seg_img2), verbose=verbose, logger=module_logger)
                    log_cmd('$FSLDIR/bin/pngappend %s + %s %s' % (Seg_img,Seg_img2,Seg_img), verbose=verbose, logger=module_logger)
                    os.remove(Brainstem_img)
                    os.remove(DeepGM_img)                    
                    os.remove(Seg_img2)                    
                elif DeepGM_vol:
                    log_cmd('$FSLDIR/bin/pngappend %s - %s %s' % (Seg_img,DeepGM_img,Seg_img), verbose=verbose, logger=module_logger)
                    os.remove(DeepGM_img)
        
        
        if resegment_using_N4mask:  #TODO: move old outputs before rerunning so that ForceUpdate=True flag can be removed
            raise ValueError("Not Implemented.  See Atropos N4")    
#            #generate a weighting mask for use during bias field correction.
#            #using at least a weight of 0.5 in whole brain, but up to 1 in white matter
#            weight_mask=os.path.join(output_dir,'N4_weight_mask.nii.gz')
#            fname_brain_mask=os.path.join(output_dir,'tmp_brainmask')
#            log_cmd('$FSLDIR/bin/fslmaths %s -thr 0 -bin %s' % (fname_brain, fname_brain_mask),verbose=verbose,logger=logger)
#            log_cmd('$FSLDIR/bin/fslmaths %s -mul 0.5 -add %s -div 1.5 %s' % (fname_brain_mask,WM_vol,weight_mask),verbose=verbose,logger=logger)
#            log_cmd('$FSLDIR/bin/imrm %s' % (fname_brain_mask),verbose=verbose,logger=logger)
#            
#            if (len(sn)>=2) and sn[0:2]=='T2':
#                bias_oname_root='T2W'
#            else:
#                bias_oname_root='T1W'
#            #run bias field correction using this weighting mask
#            fname_brain, bias_field_vol=cmind_bias_correct(fname_brain, fname_brain, ANTS_bias_cmd, oname_root=bias_oname_root, weight_mask=weight_mask, generate_figures=generate_figures, ForceUpdate=True, verbose=verbose, logger=logger) #current brain must be overwritten so ForceUpdate=True
#            
#            #move the results of the previous iteration
#            iter_no=1
#            _move_segment_iter(WM_vol, GM_vol, CSF_vol,Seg_img,iter_no,image_only=True, verbose=verbose, logger=module_logger)
#            
#            #rerun segmentation on the new bias corrected brain
#            (WM_vol, GM_vol, CSF_vol)=cmind_segment(fname_brain, seg_name=seg_name, generate_figures=generate_figures, use_prior=use_prior, use_alt_prior=use_alt_prior, reg_struct=reg_struct, use_prior_throughout=use_prior_throughout, resegment_using_N4mask=False, ANTS_bias_cmd=ANTS_bias_cmd, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
#       
    
            #/media/Data1/BRAINSTools/Scripts/antsAtroposN4.sh -d 3 -a IRC04H_00F011_P_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1.nii.gz -a IRC04H_00F011_P_1_WIP_T2W_3D_FLAIR_1x1x1_SENSE_21_1.nii.gz -c 5 -o T1Segment_Atropos
    else:
        print("Brain segmentation previously performed...skipping")        

    
    #if a working directory was created above then remove it        
    if prior_workingdir:
        shutil.rmtree(prior_workingdir)
        
    return (WM_vol, GM_vol, CSF_vol, DeepGM_vol, Brainstem_vol, Cerebellar_vol)
        
def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    output_dir='' #will be set automatically
    reg_struct=None
    generate_figures=True
    resegment_using_N4mask=True
    ANTS_bias_cmd=None
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
   
    if verbose:
        func=cmind_timer(cmind_segment_atropos,logger=logger)
    else:
        func=cmind_segment_atropos

    testcases=['P','F']
    for testcase in testcases:    
        fname_brain=os.path.join(cmind_example_output_dir,testcase,
                                'T1_proc','T1W_Structural_N4_brain.nii.gz')
        fname_head=os.path.join(cmind_example_output_dir,testcase,
                                'T1_proc','T1W_Structural_N4.nii.gz')
        fname_brain_mask=os.path.join(cmind_example_output_dir,testcase,
                                'T1_proc','T1W_Structural_N4_brain_mask.nii.gz')
        priors_dir='/media/Data1/src_repositories/svn_stuff/cmind-matlab/trunk/cmind_templates_new/48to216/Atropos_Oasis/'  #TODO: autofind instead of hardcode
        additional_intensity_vols = None
                                
        prior_weight=0.10
        reg_struct=os.path.join(cmind_example_output_dir,testcase,
                                'T1_proc','reg_struct.csv')
                        
        func(fname_brain, fname_head, fname_brain_mask, 
             additional_intensity_vols=additional_intensity_vols, seg_name=seg_name, 
             reg_struct=reg_struct, priors_dir=priors_dir, prior_weight=prior_weight,
            resegment_using_N4mask=resegment_using_N4mask, ANTS_bias_cmd=ANTS_bias_cmd, 
            generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, 
            logger=logger)  


 
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()

    else:    
        parser = argparse.ArgumentParser(description="Segmentation using FSL's FAST", epilog="")  #-h, --help exist by default
        #example of a positional argument
        
        parser.add_argument("-i","-t1_vol","--fname_brain",required=True, help="input T1-weighted brain volume")
        parser.add_argument("--fname_head",required=True, help="input T1-weighted head volume")
        parser.add_argument("--fname_brain_mask",required=True, help="input T1-weighted brain mask")
        parser.add_argument("-a","--additional_intensity_vols",required=False, help="additional volumes (e.g. T2-weighted) to use during segmentation (additional -a option inputs to Atropos)")
        parser.add_argument("-o","--output_dir",type=str,default="None", help="directory to use for the segmentation outputs")
        parser.add_argument("-rsf","--reg_struct",type=str,default="None", help="registration structure")
        parser.add_argument("-pdir","--priors_dir",type=str,default="None", help="Directory containin segmentation priors.  Prior filenames must be priors*.nii.gz")
        parser.add_argument("--prior_weight",type=float,default=0.1, help="weight on the priors")
        parser.add_argument("-reseg","--resegment_using_N4mask",type=str,default="False", help="rerun N4 bias correction on a mask weighted toward white matter then resegment")
        parser.add_argument("-ants","--ANTS_bias_cmd",type=str,default="None", help="path to ANTs N4BiasFieldCorrection binary (used if resegment_using_N4mask=True)")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        fname_brain=args.fname_brain
        fname_head=args.fname_head
        fname_brain_mask=args.fname_brain_mask
        additional_intensity_vols=str2none(args.additional_intensity_vols)
        output_dir=str2none(args.output_dir, none_strings=['none','default'])
        reg_struct=str2none(args.reg_struct, none_strings=['none','default'])
        priors_dir=str2none(args.priors_dir, none_strings=['none','default'])
        prior_weight=args.prior_weight
        resegment_using_N4mask=args.resegment_using_N4mask;
        ANTS_bias_cmd=str2none(args.ANTS_bias_cmd, none_strings=['none','default'])
        generate_figures=args.generate_figures;
        #UCLA_flag=args.UCLA_flag;
        #generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger
        
        if verbose:
            cmind_segment_atropos=cmind_timer(cmind_segment_atropos,logger=logger)

        cmind_segment_atropos(fname_brain, fname_head, fname_brain_mask, 
            additional_intensity_vols=additional_intensity_vols, seg_name=seg_name, 
             reg_struct=reg_struct, priors_dir=priors_dir, prior_weight=prior_weight,
            resegment_using_N4mask=resegment_using_N4mask, ANTS_bias_cmd=ANTS_bias_cmd, 
            generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, 
            logger=logger)       
             