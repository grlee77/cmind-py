#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def gauss_bias_corr(im1,sigma=6.0):
    """ Divide gaussian smoothed version of image to correct severe bias fields
        that are not dealt with properly by N4
    """
    import numpy as np
    try:
        from skimage.filter import threshold_otsu as otsu
    except:
        raise ImportError("couldn't import otsu from skimage")
    try:
        import scipy.ndimage
    except:
        raise ImportError("scipy.ndimage")
    
    #im1f=scipy.ndimage.gaussian_filter(im1, sigma=8)
    im1f=scipy.ndimage.gaussian_filter(im1, sigma=2*sigma)
                
    ratio_im = im1/im1f
    ratio_im[np.isnan(ratio_im)]=0
    ratio_im[ratio_im>5] = 5
    #plt.figure(); plt.imshow(montager(ratio_im[...,::8],flipy=True),cmap=plt.cm.gray)
    
    #plt.figure(); plt.imshow(montager(im1f[...,::8],flipy=True),cmap=plt.cm.gray)
    
    mask = ratio_im>(0.85*otsu(ratio_im))
    im1v2=im1.copy()
    im1v2[~mask]=np.mean(im1[mask])
    bias_field = scipy.ndimage.gaussian_filter(im1v2, sigma=sigma)
    bias_field /= np.mean(bias_field[mask])
    ratio_im = im1/bias_field
    ratio_im[np.isnan(ratio_im)]=0
    #ratio_im[ratio_im>5] = 5
    
    #ratio_im *= np.mean(im1[mask])/np.mean(ratio_im[mask])
    
    return ratio_im, bias_field

def cmind_bias_correct(fname_crop,fname_bias,ANTS_bias_cmd=None, oname_root='T1W', weight_mask=None, N4_shrinkFactor=3, use_gaussian_prefilt = False, gaussian_sigma = 6.0, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """N4 Bias Field correction
    
    Parameters
    ----------
    fname_crop : str
        input volume
    fname_bias : str
        filename to use for output volume. if a directory name is given instead, 
        a file named ${oname_root}_Structural_N4.nii.gz will be created in that 
        folder
    ANTS_bias_cmd : str, optional
        location of N4BiasFieldCorrection binary
    oname_root : str, optional
        output filename prefix
    weight_mask : str, optional
        filename of optional weighting mask for ANTs N4BiasFieldCorrection (see ANTs documentation)
    N4_shrinkFactor : int, optional
        resolution reduction factor during N4 computations (default=3)
    use_gaussian_prefilt : bool, optional
        if True, do an initial crude bias field correction based on division by
        a Gaussian smoothed version of the image
    gaussian_sigma : float, optional
        if use_gaussian_prefilt = True: sigma for the gaussian smoothing stage 
        (in voxels)
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    fname_bias : str
        Bias corrected head
    bias_field_vol : str
        The bias field
    
    See Also
    --------
    
    FSL's standard_space_roi shell script is a registration based approach to neck 
    removal.
    
    Notes
    -----
    
    .. figure::  ../img/T1W_Crop.png
       :align:   center
    
       T1-weighted head prior to bias correction
    
    .. figure::  ../img/T1W_N4.png
       :align:   center
    
       T1-weighted head after bias correction


    """
    import os
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, split_multiple_ext
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    from cmind.finders import check_exist
    check_exist('ANTs') #raise exception if required external software not found

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, use_gaussian_prefilt = input2bool([generate_figures, ForceUpdate, verbose, use_gaussian_prefilt])
    
    if generate_figures:
        check_exist('FSL') #raise exception if required external software not found

    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Bias Correction")
            
    #if verbose:
    #    from time import time
    #    starttime=time()

    #determine the Bias correction routine path and type    
    if not ANTS_bias_cmd:
        from cmind.globals import cmind_ANTs_dir
        ANTS_bias_cmd=os.path.join(cmind_ANTs_dir,'N4BiasFieldCorrection')
        
    if not os.path.exists(ANTS_bias_cmd):
        raise IOError("ANTS_bias_cmd, %s, not found!" % ANTS_bias_cmd)
    if ANTS_bias_cmd.find('N4BiasFieldCorrection')!=-1:
        use_N4 = True;
    elif ANTS_bias_cmd.find('N3BiasFieldCorrection')!=-1:    
        use_N4 = False;
    else:
        raise IOError("Unknown bias field correction routine, %s\n   Should be N4BiasFieldCorrection or N3BiasFieldCorrection" % ANTS_bias_cmd)

    (imbool, fname_crop)=imexist(fname_crop,'nifti-1')[0:2]
    if not imbool:
        raise IOError("input image, %s, doesn't exist!" % fname_crop)

    #add appropriate extension to fname_bias if it was not specified
    module_logger.debug("fname_crop = %s" % fname_crop)
    
    bias_ext=split_multiple_ext(fname_bias)[1]
    if not bias_ext: 
        bias_ext=split_multiple_ext(fname_crop)[1]
        fname_bias = fname_bias + bias_ext
    
    module_logger.debug("fname_bias = %s" % fname_bias)
    
    extra_args='-b 200 -c [100x100x100x100, 1e-06]'

    
    if weight_mask:
        (imbool,weight_mask)=imexist(weight_mask)[0:2]
        if not imbool:
            raise IOError("Specified weight mask not found")
        else:
            extra_args = extra_args + ' -w "%s"' % weight_mask

    output_dir=os.path.dirname(fname_crop)
    if not output_dir:
        output_dir=os.getcwd()
    elif not os.path.isdir(output_dir):
        os.makedirs(output_dir)
                    
    if use_gaussian_prefilt:
        import nibabel as nib
        im_crop_nii = nib.load(fname_crop)     
        im_bias_pre, bias_pre = gauss_bias_corr(im_crop_nii.get_data(),
                                                sigma=gaussian_sigma)
        im_bias_pre=im_bias_pre.astype(im_crop_nii.get_data_dtype())
        im_bias_pre_nii = nib.Nifti1Image(im_bias_pre,
                                          affine = im_crop_nii.get_affine(),
                                          header = im_crop_nii.get_header())
        fname_crop = split_multiple_ext(fname_bias)[0] + '_pre' + bias_ext
        im_bias_pre_nii.to_filename(fname_crop)
        del im_bias_pre
        del im_bias_pre_nii
        del im_crop_nii
            
    N4_img=os.path.join(output_dir,oname_root+'_N4.png')
    bias_field_vol=os.path.join(output_dir,oname_root+'_N4Bias.nii.gz')
    
    if fname_bias and os.path.isdir(fname_bias):  #if specified fname_crop was a directory, use a default name
        fname_bias=os.path.join(fname_bias,oname_root+'_Structural_N4.nii.gz')
    
    #TODO: add -x $BRAINMASK -w $WM_PROB_MASK
    
    if (not imexist(fname_bias,'nifti-1')[0]) or ForceUpdate:
        if use_N4:
            #$ANTS_bias_cmd -d 3 -i ${fname_crop}.nii.gz -o [${fname_bias}.nii.gz,N4BiasT1.nii.gz] -s 2 -b 200 -c [50x50x30x20, 1e-04]
            shell_cmd='%s -d 3 -i %s -o ["%s","%s"] -s %d %s' % (ANTS_bias_cmd,fname_crop,fname_bias,bias_field_vol, N4_shrinkFactor, extra_args)
        else:
            if weight_mask:
                (imbool,weight_mask)=imexist(weight_mask)[0:2]
            else:
                weight_mask=''
            shell_cmd='%s 3 "%s" "%s" %d %s' % (ANTS_bias_cmd,fname_crop,fname_bias,N4_shrinkFactor,weight_mask)
        log_cmd(shell_cmd, verbose=verbose, logger=module_logger)
    
        #if verbose:
        #    print 'Bias Correction:  Elapsed time: %s\n' % (time()-starttime)
        if generate_figures:
            log_cmd('$FSLDIR/bin/slicer "%s" -a "%s"' % (fname_bias , N4_img), verbose=verbose, logger=module_logger)
    else:
        module_logger.info("Bias correction previously performed...skipping")


    if use_gaussian_prefilt:            
        #multiply N4 bias field by the gaussian prefilter bias field
        N4field_nii = nib.load(bias_field_vol)
        N4field = N4field_nii.get_data() * bias_pre
        N4field_nii = nib.Nifti1Image(N4field,
                                      affine = N4field_nii.get_affine(),
                                      header = N4field_nii.get_header()) 
        N4field_nii.to_filename(bias_field_vol)                         
        
    return (fname_bias,bias_field_vol)

def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_ANTs_dir
    ANTS_bias_cmd=pjoin(cmind_ANTs_dir,'N4BiasFieldCorrection')
    oname_root='T1W'
    generate_figures=True
    ForceUpdate=True
    verbose=True
    weight_mask=None
    N4_shrinkFactor=3
    use_gaussian_prefilt = True
    gaussian_sigma = 10.0
    #weight_mask=os.path.join(cmind_example_output_dir,'P','T1_proc','T1Segment_WM.nii.gz')
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',file_mode='w')        
    if verbose:    
        func = cmind_timer(cmind_bias_correct,logger=logger)
    else:
        func = cmind_bias_correct
    
    testcases=['P','F']
    for testcase in testcases:
        fname_crop=pjoin(cmind_example_output_dir,testcase,'T1_proc','T1W_Structural_Crop.nii.gz')
        fname_bias=pjoin(cmind_example_output_dir,testcase,'T1_proc','T1W_Structural_N4.nii.gz')
        
        fname_bias,bias_field_vol=func(fname_crop,
                                       fname_bias, 
                                       ANTS_bias_cmd,
                                       oname_root=oname_root, 
                                       weight_mask=weight_mask,
                                       N4_shrinkFactor=N4_shrinkFactor, 
                                       use_gaussian_prefilt = use_gaussian_prefilt,
                                       gaussian_sigma = gaussian_sigma,
                                       generate_figures=generate_figures,
                                       ForceUpdate=ForceUpdate, 
                                       verbose=verbose, 
                                       logger=logger)       
                      
    

if __name__ == '__main__':
    import sys,argparse
    from cmind.utils.utils import _parser_to_loni
    from cmind.utils.decorators import cmind_timer
#    from cmind.utils.logging_utils import cmind_logger

    if (len(sys.argv) == 2 and sys.argv[1]=='test'):
        _testme()
    else:    
        parser = argparse.ArgumentParser(description="N4 Bias Field correction", epilog="")  #-h, --help exist by default
        #example of a positional argument
        
        parser.add_argument("fname_crop", help="input volume")
        parser.add_argument("fname_bias", help="filename to use for output volume. if a directory name is given instead, a file named ${oname_root}_Structural_N4.nii.gz will be created in that folder")
        parser.add_argument("ANTS_bias_cmd", help="location of N4BiasFieldCorrection binary")
        parser.add_argument("--oname_root",type=str,default="T1W", help="output filename prefix")
        parser.add_argument("--weight_mask",type=str,default="T1W", help="filename of optional weighting mask for ANTs N4BiasFieldCorrection (see ANTs documentation)")
        parser.add_argument("--N4_shrinkFactor",type=int,default=3, help="resolution reduction factor during N4 computations (default=3)")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("--use_gaussian_prefilt",type=str,default="False", help="if True, do an initial crude bias field correction based on division by a Gaussian smoothed version of the image")
        parser.add_argument("-sigma","--gaussian_sigma",type=float,default=5.0, help="if use_gaussian_prefilt = True: sigma for the gaussian smoothing stage (in voxels)")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        fname_crop=args.fname_crop;
        fname_bias=args.fname_bias;
        ANTS_bias_cmd=args.ANTS_bias_cmd;
        oname_root=args.oname_root
        weight_mask=args.weight_mask
        N4_shrinkFactor=args.N4_shrinkFactor
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        use_gaussian_prefilt=args.use_gaussian_prefilt;
        gaussian_sigma=args.gaussian_sigma;
        verbose=args.verbose;
        logger=args.logger
    
        if verbose:    
            cmind_bias_correct = cmind_timer(cmind_bias_correct, logger=logger)
            
        cmind_bias_correct(fname_crop,
                           fname_bias, 
                           ANTS_bias_cmd,
                           oname_root=oname_root, 
                           weight_mask=weight_mask,
                           N4_shrinkFactor=N4_shrinkFactor, 
                           use_gaussian_prefilt=use_gaussian_prefilt,
                           gaussian_sigma = gaussian_sigma,
                           generate_figures=generate_figures,
                           ForceUpdate=ForceUpdate, 
                           verbose=verbose, 
                           logger=logger)       
                    