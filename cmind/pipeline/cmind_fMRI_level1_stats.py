#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_feat_stats(prep_dir, stats_dir, FSF_template, smooth=5,
                    brain_thresh=10, perfusion_subtract=False,
                    extra_confounds=None, verbose=False, logger=None):
    """utility to run "stats" portion of FEAT processing

    Parameters
    ----------
    prep_dir : str
        FEAT output directory from preprocessing with cmind_ffMRI_preprocess2
    stats_dir : str
        output directory to store the stats images in
    smooth : str, optional
        spatial smoothing (mm)
    brain_thresh : str, optional
        brain threshold
    FSF_template : bool
        FSF template (e.g. design.fsf)
    perfusion_subtract : bool, optional
        maximum allowed width in pixels of the output overlay images
    extra_confounds : str, optional
        text file containing one confound ev per column
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    """
    import os
    from os.path import join as pjoin
    import shutil
    
    from cmind.utils.cmind_utils import read_fsl_design
    from cmind.utils.logging_utils import log_cmd
    from cmind.globals import fsl_version

    s=stats_dir
    normmean = 10000;
        
    #Note: feat_model will apply the highpass temporal filter to the extra_confounds for us...
    if extra_confounds and os.path.exists(extra_confounds):
        log_cmd('$FSLDIR/bin/feat_model "%s/design" %s' % (s,extra_confounds), verbose=verbose, logger=logger)
    else:
        log_cmd('$FSLDIR/bin/feat_model "%s/design"' % s, verbose=verbose, logger=logger)

    absthresh = brain_thresh * normmean/100.0; 
    
    subd=pjoin(stats_dir,'stats')
    if os.path.exists(subd):
        #log_cmd('rm -rf stats', verbose=verbose, logger=logger)
        shutil.rmtree(subd)
         
    if fsl_version>'5.0.4': #new ways of calling film_gls in FSL 5.0.5+
        
        if perfusion_subtract:
            extra_film_opts=' --noest --mf="%s/mean_func" --mft="%s/design.min"' % (s,s)
        else:
            extra_film_opts=' --sa --ms=5'
            
        if fsl_version>'5.0.6':  #film_gls explicitly passes --con, --fcon as of FSL 5.0.7
            design_con_file=pjoin(s,'design.con')
            design_fts_file=pjoin(s,'design.fts')
            if os.path.exists(design_con_file):
                extra_film_opts+=' --con={}'.format(design_con_file)
            if os.path.exists(design_fts_file):
                extra_film_opts+=' --fcon={}'.format(design_fts_file)
            
        #New way (5.0.5+)
        log_cmd('$FSLDIR/bin/film_gls --rn="%s/stats" %s --in="%s/filtered_func_data" --pd="%s/design.mat" --thr=%f' % (s,extra_film_opts, prep_dir,s,absthresh), verbose=verbose, logger=logger)
    else:
        if perfusion_subtract:
            extra_film_opts=' -noest' % (s,s)
        else:
            extra_film_opts=' -sa -ms 5'
            
        log_cmd('$FSLDIR/bin/film_gls -rn "%s/stats" %s "%s/filtered_func_data" "%s/design.mat" %f' % (s,extra_film_opts,prep_dir,s,absthresh), verbose=verbose, logger=logger)
       
    desmat=read_fsl_design(pjoin(stats_dir,'design.mat'))
        
    Ntime = desmat.shape[0]
    Ncol = desmat.shape[1]
    dof = Ntime-Ncol #TODO:  dof = #rows of design matrix - #columns of design matrix
    log_cmd('$FSLDIR/bin/smoothest -d %d -m "%s"/mask -r "%s/stats/res4d" > "%s/stats/smoothness"' % (dof,prep_dir,s,s), verbose=verbose, logger=logger)
    
    #contrast_mgr no longer used as of FSL 5.0.7  
    #   see --con and --fcon arguments to film_gls instead
    if fsl_version<'5.0.7': 
        fts_file=pjoin(s,'design.fts')
        if os.path.exists(fts_file):
            log_cmd('$FSLDIR/bin/contrast_mgr -f "%s" "%s/stats" "%s/design.con"' % (fts_file,s,s), verbose=verbose, logger=logger)
        else:
            log_cmd('$FSLDIR/bin/contrast_mgr "%s/stats" "%s/design.con"' % (s,s), verbose=verbose, logger=logger)



def cmind_fMRI_level1_stats(output_dir, FSF_template, prep_dir, stats_dir=None, lvl1_stats_case="", nuisance_file="", feat_params=None, z_thresh=None,prob_thresh=None,thresh=None,output_tar=False, CMIND_specific_figures=False, omit_poststats=True, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """Utility to run FEAT stats/poststats processing equivalent
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    FSF_template : str
        design.fsf template filename
    prep_dir : str
        specify location where cmind_fMRI_preprocess2.py output is located
    stats_dir : str, optional
        specify location where stats output should go (or is already located)
    lvl1_stats_case : str, optional
        may be used in future to apply different processing to different cases
    nuisance_file : str, optional
        path to a text file with one nuisance regressor per column
    feat_params : str, optional
        filename to a .csv file containing feat parameters listed as key,value 
        pairs.  If z_thresh, prob_thresh, or thresh are explicitly passed in, 
        those values will override the ones in the feat_params file.  Typically
        feat_params should be the preproc_options_file output by 
        cmind_fMRI_preprocess2
    z_thresh : float, optional
        initial z threshold to use when determining clusters.  
        unused if thresh!=2
    prob_thresh : float, optional
        probability threshold for voxelwise or cluster significance
    thresh : {0,1,2}, optional
        0 = uncorrected, 1 = voxel-based (FWE corrected), 2 = cluster-based
    output_tar : bool, optional
        if true, generate .tar.gz versions of stats and poststats folders
    CMIND_specific_figures : bool, optional
        generate additional figures for the CMIND paradigm results.  set this to False if the data is not from the CMIND project
    omit_poststats : bool, optional
        it True, run stats only without poststats thresholding
    generate_figures : bool,optional
        if true, generate overlay images summarizing the registration
    ForceUpdate : bool, optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    stats_tarfile : str or None
        filename of the .tar.gz file containing the stats output
    poststats_tarfile : str or None
        filename of the .tar.gz file containing the poststats output
    output_dir : str
        Feat directory where the analysis was performed
    stats_dir : str
        stats output folder
    poststats_dir : str or None
        poststsats output folder
                        
    See Also
    --------
    cmind_fMRI_outliers
    
    Notes
    -----
    `cmind_fMRI_outliers` should be used to generate a file for `extra_confounds`
    FEAT stats subdirectory will be created as a softlink to `stats_dir`
    
    For the C-MIND study, CMIND-specific figures will be created for the following
    lvl1_stats_case values:
    Feat_BOLD_Stories
    Feat_BOLD_Sentences    
    Feat_ASL_Stories            
    Feat_ASL_Sentences 
           
    """

    import os, glob
    from os.path import join as pjoin
    import warnings
    
    import numpy as np
    
    from cmind.globals import fsl_version
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict, export_tarfile #, rm_broken_links, cmind_copy_all
    from cmind.utils.fsf_utils import fsf_substitute
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    from cmind.pipeline.cmind_fMRI_level1_stats import cmind_feat_stats
    from cmind.pipeline.cmind_fMRI_level1_poststats import cmind_fMRI_level1_poststats
    
    #TODO: implement, CSV, xml, JSON, or other filetypes for feat_params

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, output_tar, omit_poststats = input2bool([generate_figures, ForceUpdate, verbose, output_tar, omit_poststats])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Feat level-1 stats/postats")        
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if omit_poststats:
        poststats_dir = None
    else:
        poststats_dir = pjoin(output_dir, 'poststats') #if output_dir was used poststats tar file will also contain the stats files (and tar file)
    
    if stats_dir is None:
        stats_dir = pjoin(output_dir,'stats')
 
    # Skip LONI processing if Feat Params is not supplied 
    
    if feat_params == "None":
        print("stats_tarfile:None")
        print("poststats_tarfile:None")
        print("output_dir:{}".format(output_dir))
        print("stats_dir:{}".format(stats_dir))
        print("poststats_dir:{}".format(poststats_dir))
        return

    if feat_params:
        if isinstance(feat_params,str):
            if os.path.exists(feat_params):
                #[f p ext]=fileparts(feat_params);
                #if(streq(ext,'.mat'))
                #    load(feat_params,'feat_params')
                #elif(streq(ext,'.txt'))
                #    feat_params_file=feat_params; clear feat_params
                feat_params=csv2dict(feat_params);
            else:
                raise IOError("feat_params must either be the feat parameter structure, or a path to the .txt file containing it")
        elif not isinstance(feat_params,dict):
            raise IOError("feat_params must either be the feat parameter structure, or a path to the .txt file containing it")

    if not feat_params:
        feat_params={}
        
    if not FSF_template:
        raise ValueError("FSF_template must be specified")
    
    
    smooth=float(feat_params.get('smooth',6.0))
    feat_params['smooth']=smooth
    
    paradigm_hp=float(feat_params.get('paradigm_hp',128.0))
    feat_params['paradigm_hp']=paradigm_hp
        
   
#    if paradigm_hp < np.Inf:
#        prep_subdir = 'prep_s%02d' % (int(smooth)) + '_bptf%04d' % (int(paradigm_hp)) 
#        stats_subdir = 'stats_s%02d' % (smooth) + '_bptf%04d' % (paradigm_hp) 
#    else:
#        prep_subdir = 'prep_s%02d' % (int(smooth))
#        stats_subdir = 'stats_s%02d' % (smooth)
#         
#    if nuisance_file:
#        if nuisance_file.lower()=='none':
#            nuisance_file=[]
#        elif not os.path.exists(nuisance_file):
#            raise IOError("specified nuisance file, %s,  doesn''t exist" % (nuisance_file))
#        if nuisance_file.find('default')!=-1:
#            stats_subdir = stats_subdir + '_mcf06_FSLoutliers'
#        elif nuisance_file.find('best')!=-1:
#            stats_subdir = stats_subdir + '_best'
#        elif  nuisance_file.find('worst')!=-1:
#            stats_subdir = stats_subdir + '_worst'
#        elif  nuisance_file.find('mcf06')!=-1:
#            stats_subdir = stats_subdir + '_mcf06'
#        elif  nuisance_file.find('FSL_outliers')!=-1:
#            stats_subdir = stats_subdir + '_FSLout'
#
#    if not nuisance_file:
#        stats_subdir = stats_subdir + '_none'
#    
#    directory_string=stats_subdir.replace('stats_','') #will save the directory string for later use in naming registration folders
           
    #First priority is the passed value, then the value in feat_params
    #and then a default if no feat_params file was given
    if z_thresh:
        feat_params['z_thresh'] = z_thresh
    else:
        z_thresh = float(feat_params.get('z_thresh', 3.09))
            
    if(z_thresh < 0):
        raise ValueError("Invalid z_thresh<0 specified")

    if prob_thresh:
        feat_params['prob_thresh'] = prob_thresh
    else:
        prob_thresh = float(feat_params.get('prob_thresh', 0.05))
           
    if(prob_thresh < 0):
        raise ValueError("Invalid prob_thresh<0 specified")

    if thresh:
        feat_params['thresh'] = thresh
    else:
        thresh = int(feat_params.get('thresh', 2))   #0=uncorrected, 1=Voxel (FWE), 2=Cluster
   

#    if stats_dir[-1]==os.path.sep: #strip any trailing path seperator
#        stats_dir=stats_dir[:-1]
#    poststats_dir = os.path.dirname(stats_dir) + os.path.basename(stats_dir).replace('stats','poststats')
                                  
    if output_tar:
        stats_tarfile = pjoin(output_dir, 'stats.tar.gz')
        if omit_poststats:
            poststats_tarfile = None
        else:
            poststats_tarfile = pjoin(output_dir, 'poststats.tar.gz')
    else:
        stats_tarfile = None
        poststats_tarfile = None
                                                                
    #case_list=['feat_bold_sentences','feat_bold_stories','feat_asl_sentences','feat_asl_stories','feat_echocombined_sentences','feat_echocombined_stories']
    #if not lvl1_stats_case.lower() in case_list:
    #   raise ValueError("Unrecognized processing case, %s" % lvl1_stats_case)
    
    lvl1_stats_case_substr = lvl1_stats_case.replace('Feat_', '')
    
    #combined_flag = ~isempty(strfind(lvl1_stats_case_substr,'EchoCombined'));
    
    filtfunc_froot = pjoin(prep_dir,'filtered_func_data')
    fexists = imexist(filtfunc_froot, 'nifti-1')[0]
    if not fexists:  # and not os.path.exists('./%s' % lvl1_stats_case]); 
        raise IOError("Did not find filtered_func_data in the feat directory at {}".format(filtfunc_froot))
    
    brain_thresh = float(feat_params.get('brain_thresh', 7.0))
    feat_params['brain_thresh'] = brain_thresh

    perfusion_subtract = input2bool(feat_params.get('perfsub_yn', False))
   
    if perfusion_subtract:    
        feat_params['perfsub_yn'] = 1
    else:
        feat_params['perfsub_yn'] = 0
        
    if not os.path.exists(prep_dir):
        os.makedirs(prep_dir)  
        
#    use_symlinks=True
#    #log_cmd('ln -s -f -t "%s" "%s"/*' % (output_dir,prep_dir), verbose=verbose, logger=module_logger);
#    try:
#        cmind_copy_all(prep_dir, output_dir,match_str='*',use_symlinks=use_symlinks, overwrite_existing=True)                 
#    except OSError as e:
#        if use_symlinks:
#            warnings.warn("Unable to generate symbolic links.  copying files instead...")
#            cmind_copy_all(prep_dir, output_dir,match_str='*',use_symlinks=False, overwrite_existing=True)                 
#        else:
#            print("OSError({0}): {1}".format(e.errno, e.strerror))
# 

    if not os.path.exists(stats_dir):
        os.makedirs(stats_dir)  
#    elif ForceUpdate: #remove old stats folder
#        shutil.rmtree(stats_dir)
#        os.makedirs(stats_dir)
 
    noise_est_file = pjoin(output_dir, 'noise_est.txt')
    if os.path.exists(noise_est_file):
        try:
            noise_val = np.genfromtxt(noise_est_file)
            feat_params['noise'] = noise_val[0]
            feat_params['noisear'] = noise_val[1]
        except:
            warnings.warn("failed to update noise, noisear for design.fsf")

    if (not os.path.exists(pjoin(stats_dir, 'smoothness'))) or ForceUpdate:
        local_fsf_file = pjoin(output_dir, 'design.fsf')
        log_cmd('cp "%s" "%s"' % (FSF_template,local_fsf_file), verbose=verbose, logger=logger)
        fsf_substitute(local_fsf_file,feat_params)  #update local design.fsf with the current values from feat_params
        
        cmind_feat_stats(prep_dir, output_dir,FSF_template,smooth,brain_thresh,perfusion_subtract,nuisance_file, verbose=verbose, logger=logger)
    
#        f=open(pjoin(stats_dir,'dir_string.txt'),mode='w') #save the directory string for later use in naming registration folders
#        f.write(directory_string)
#        f.close()
#        del f
    
#    #copy (or symlink) all files from stats dir to the main FEAT directory
#    try:
#        cmind_copy_all(stats_dir, output_dir,match_str='*',use_symlinks=use_symlinks, overwrite_existing=True)
#    except OSError as e:
#        if use_symlinks:
#            warnings.warn("Unable to generate symbolic links.  copying files instead...")
#            cmind_copy_all(stats_dir, output_dir,match_str='*',use_symlinks=False, overwrite_existing=True)
#        else:
#            print("OSError({0}): {1}".format(e.errno, e.strerror))
#    
        
    if stats_tarfile:
        if (not os.path.exists(stats_tarfile)) or ForceUpdate:
            #explicitly specify the individual files to avoid archiving any other previously existing files as well
            
            #add all design files
            all_stats_files=glob.glob(pjoin(output_dir, 'design*.*')) 
            all_arcnames=[os.path.basename(f) for f in all_stats_files]
            
            #add the stats subfolder
            all_stats_files.append(stats_dir)
            all_arcnames.append('stats')

            export_tarfile(stats_tarfile, all_stats_files, arcnames=all_arcnames) #tar output_dir into stats_tarfile
    
    if not omit_poststats:
        cmind_fMRI_level1_poststats(poststats_dir, 
                                    prep_dir, 
                                    stats_dir=output_dir, 
                                    lvl1_stats_case=lvl1_stats_case_substr, 
                                    feat_params=feat_params, 
                                    z_thresh=z_thresh,
                                    prob_thresh=prob_thresh,
                                    thresh=thresh,
                                    output_tar=output_tar, 
                                    CMIND_specific_figures=CMIND_specific_figures, 
                                    generate_figures=generate_figures, 
                                    ForceUpdate=ForceUpdate, 
                                    verbose=verbose, 
                                    logger=logger)        
        
#        try:
#            cmind_copy_all(poststats_dir, output_dir,match_str='*',use_symlinks=use_symlinks, overwrite_existing=True)
#        except OSError as e:
#            if use_symlinks:
#                warnings.warn("Unable to generate symbolic links.  copying files instead...")
#                cmind_copy_all(poststats_dir, output_dir,match_str='*',use_symlinks=False, overwrite_existing=True)
#            else:
#                print("OSError({0}): {1}".format(e.errno, e.strerror))

#    try:
#        rm_broken_links(output_dir)
#    except:
#        pass
    
    if not omit_poststats:
        if poststats_tarfile:
            if (not os.path.exists(poststats_tarfile)) or ForceUpdate:
                export_tarfile(poststats_tarfile, poststats_dir, arcnames='') #tar poststats_dir into output_tar

    #print any filename outputs for capture by LONI
    print("stats_tarfile:{}".format(stats_tarfile))
    print("poststats_tarfile:{}".format(poststats_tarfile))
    print("output_dir:{}".format(output_dir))
    print("stats_dir:{}".format(stats_dir))
    print("poststats_dir:{}".format(poststats_dir))
    return (stats_tarfile, 
            poststats_tarfile, 
            output_dir,
            stats_dir,
            poststats_dir)

#     %                 reg_resample_mm=4;
#     %                 standard = [output_dir filesep 'T1proc' filesep 'standard']; 
#     %                 highres = [output_dir filesep 'T1proc' filesep 'T1W_3D_IRCstandard32_N3_brain30']; 
#     %                 highres2std = [output_dir filesep 'T1proc' filesep 'T1_to_std_lin.mat'];
#     %                 highres2std_warp = [output_dir filesep 'T1proc' filesep 'T1_to_std_nonlin_field.nii.gz']
#     %                 examplefunc2highres = [output_dir filesep 'regBBR_BOLD_Sentences' filesep 'lowres2highres.mat']; 
#     %                 cmind_melodic_batchprep([output_dir filesep 'Melodic_BOLD_Sentences'],standard,highres,highres2std,examplefunc2highres,reg_resample_mm)
#     %                 cmind_melodic_batchprep([output_dir filesep 'Melodic_BOLD_Sentences'],standard,highres,highres2std_warp,examplefunc2highres,reg_resample_mm)

def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_ASLBOLD_dir
    output_tar=True
    #output_tar=False
    generate_figures=True
    CMIND_specific_figures=True
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',
                        file_mode='w')  
    if verbose:
        func=cmind_timer(cmind_fMRI_level1_stats,logger=logger)
    else:
        func=cmind_fMRI_level1_stats
    
    z_thresh=None
    prob_thresh=None
    thresh=None
#    prep_dir=None   #will be automatically set
    stats_dir=None  #will be automatically set
    omit_poststats=True
    paradigms=['Sentences','Stories']
    acq_types=['BOLD','ASL']
    for paradigm in paradigms:
        for acq_type in acq_types:
            
            prep_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s' % acq_type)
            output_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s_stats' % acq_type)
            lvl1_stats_case='Feat_%s_%s' % (acq_type, paradigm)
            nuisance_file=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'outliers_%s' % acq_type,'mcf06_confounds.txt')
            feat_params=pjoin(prep_dir,'cmind_fMRI_preprocess2_options.csv')

            FSF_template=pjoin(cmind_ASLBOLD_dir,'%s_level1_%s.fsf' % (acq_type, paradigm))
            
            func(output_dir, 
                 FSF_template, 
                 prep_dir=prep_dir, 
                 stats_dir=stats_dir,                 
                 lvl1_stats_case=lvl1_stats_case, 
                 nuisance_file=nuisance_file, 
                 feat_params=feat_params, 
                 z_thresh=z_thresh, prob_thresh=prob_thresh, thresh=thresh, 
                 output_tar=output_tar, 
                 CMIND_specific_figures=CMIND_specific_figures, 
                 omit_poststats=omit_poststats,
                 generate_figures=generate_figures, 
                 ForceUpdate=ForceUpdate,
                 verbose=verbose,logger=logger)
    
    
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Utility to run FEAT stats/poststats processing equivalent", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-fsf","--FSF_template", required=True, help="design.fsf template filename")
        parser.add_argument("--prep_dir", required=False, type=str, default="None", help="specify location where cmind_fMRI_preprocess2.py output is located")
        parser.add_argument("--stats_dir", required=False, type=str, default="None", help="specify location where stats output should go (or is already located)")
        parser.add_argument("-p","--lvl1_stats_case", help="[Feat_BOLD_Sentences,Feat_BOLD_Stories,Feat_ASL_Sentences,Feat_ASL_Stories]; may be used in future to apply different processing to different cases")
        parser.add_argument("-nuis","--nuisance_file", help="path to a text file with one nuisance regressor per column")
        parser.add_argument("-feat","--feat_params", type=str, help="        filename to a .csv file containing feat parameters listed as key,value pairs.  If z_thresh, prob_thresh, or thresh are explicitly passed in, those values will override the ones in the feat_params file.  Typically feat_params should be the preproc_options_file output by cmind_fMRI_preprocess2")
        #parser.add_argument("--preproc_params", type=str, default="None", help="dictionary (or .csv filename) containing keys for 'smooth' and 'paradigm_hp' that will override the values in feat_params.csv if provided")
        parser.add_argument("--z_thresh", type=str, default="None", help="initial z threshold to use when determining clusters. unused if thresh!=2")
        parser.add_argument("--prob_thresh", type=str, default="None", help="probability threshold for voxelwise or cluster significance")
        parser.add_argument("--thresh", type=str, default="None", help="{0,1,2} 0 = uncorrected, 1 = voxel-based (FWE corrected), 2 = cluster-based")
        parser.add_argument("-outtar","--output_tar", type=str, default="false", help="if true, generate .tar.gz versions of stats and poststats folders")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate overlay images summarizing the registration")
        parser.add_argument("--CMIND_specific_figures",type=str,default="False", help="generate additional figures for the CMIND paradigm results.  set this to False if the data is not from the CMIND project")
        parser.add_argument("--omit_poststats",type=str,default="False", help="if True, run stats only without poststats thresholding")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
                
        args=parser.parse_args()
    
        output_dir=args.output_dir
        FSF_template=args.FSF_template
        prep_dir=str2none(args.prep_dir)
        stats_dir=str2none(args.stats_dir)
        lvl1_stats_case=args.lvl1_stats_case
        nuisance_file=args.nuisance_file
        feat_params=args.feat_params
        #preproc_params=str2none(args.preproc_params)
        z_thresh=str2none(args.z_thresh)
        prob_thresh=str2none(args.prob_thresh)
        thresh=str2none(args.thresh)
        output_tar=args.output_tar
        generate_figures=args.generate_figures
        omit_poststats=args.omit_poststats
        CMIND_specific_figures=args.CMIND_specific_figures
        ForceUpdate=args.ForceUpdate
        verbose=args.verbose
        logger=args.logger

        if verbose:
            cmind_fMRI_level1_stats=cmind_timer(cmind_fMRI_level1_stats,logger=logger)
    
        cmind_fMRI_level1_stats(output_dir, 
                                FSF_template, 
                                prep_dir=prep_dir, 
                                stats_dir=stats_dir,
                                lvl1_stats_case=lvl1_stats_case, 
                                nuisance_file=nuisance_file, 
                                feat_params=feat_params, 
                                z_thresh=z_thresh, 
                                prob_thresh=prob_thresh, 
                                thresh=thresh, 
                                output_tar=output_tar, 
                                CMIND_specific_figures=CMIND_specific_figures, 
                                omit_poststats=omit_poststats,
                                generate_figures=generate_figures, 
                                ForceUpdate=ForceUpdate, 
                                verbose=verbose, 
                                logger=logger)
        