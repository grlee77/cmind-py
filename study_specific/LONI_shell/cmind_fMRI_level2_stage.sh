#!/bin/bash
echo "CMD: $*"

usage() 
{
echo "Recommended Usage:      
   cmind_fMRI_level2_stats.sh -o output_dir -fdl Feat_Directory_List
   
Required parameters:
    -o              : output_dir - directory in which to store the output
    -list           : the name of a text file containing one file name per line
    
Optional parameters:
    -otar           : output tarfile
    -debug          : verbose output

Returns:    
    fsf             : the fsf dictionary that was generated "    
}

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/timer.sh

source $cwd/load_cmind_env.sh
source $cwd/load_python_env.sh

if [ $# == 0 ]; then usage;  exit 0; fi

if [ "$1" == "-h" ]; then usage; exit 0; fi

REQUIRED_ARGS=1
if [ $# -lt $REQUIRED_ARGS ] 
then
    usage
    exit 1
fi

output_tarfile="none" #true

all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
    -o )   
		shift
		output_dir=$1
		;;
	-list )
		shift
	    tar_list=$1
	    ;;	
	-otar )
	 	shift
	    output_tarfile=$1
	    ;;	
	-debug )
	    shift
	    debug=$1
	    verbose_string="--verbose"
	    ;;
    esac
    shift
done

echo "output_dir=$output_dir"
echo "tar_list=$tar_list"	
echo "output_tarfile=$output_tarfile"

t=$(timer)
echo "python ${cmind_python_dir}/cmind_stage_second_level.py -o ${output_dir} -list ${tar_list} -otar $output_tarfile "
python ${cmind_python_dir}/cmind_stage_second_level.py -o ${output_dir} -list ${tar_list} -otar $output_tarfile $verbose_string
printf 'cmind_fMRI_level2_stats:  Elapsed time: %s\n' $(timer $t)

#check that QA report exists
output_file_test=${output_dir}/level1_dir_list.txt
echo "$output_file_test"
if [ ! -f $output_file_test ]; then
    echo "ERROR: Failed to finish fMRI Level 2 Staging"
    exit 1
fi 

find "$output_dir" -name "design.fsf" -exec sed -i 's/^set fmri(st_file) $/set fmri(st_file) ""/g' '{}' \;

echo "Finished cmind_fMRI_level2_stats"

