import nipype.interfaces.io as nio
from nipype.interfaces.utility import Function
from os.path import join as pjoin
#setup a datasource node for a given subject and session
def initialize_cmind_datasource2(base_directory=None):
    cmind_datasource = nio.DataGrabber(infields=['subject_id','session_id','paradigm'], outfields=['alpha', 'cbf','fieldmap_abs','fieldmap_rads','T1EST','T1Struct','T2Struct','DTI','HARDI','ASLBOLD_ASL','ASLBOLD_BOLD','RestingState'])
    cmind_datasource.inputs.base_directory = base_directory  #  '/media/Data1/CMIND_Other/Matlab_Output/'
    cmind_datasource.inputs.template = '*'
    cmind_datasource.inputs.sort_filelist = True
    cmind_datasource.inputs.raise_on_empty = False   #Don't raise an exception if files aren't found
    cmind_datasource.inputs.field_template = dict(alpha='%s_%s*ALPHA*complex.nii.gz',
                                            cbf='%s_%s*BaselineCBF*.nii.gz',
                                            fieldmap_abs='%s_%s*Philips_GRE*abs.nii.gz',
                                            fieldmap_rads='%s_%s*Philips_GRE*rads.nii.gz',
                                            T1EST='%s_%s*T1EST_TI*.nii.gz',
                                            T1Struct='%s_%s*T1W*defaced.nii.gz',
                                            T2Struct='%s_%s*T2W*defaced.nii.gz',
                                            DTI='%s_%s*DTI*.nii.gz',
                                            HARDI='%s_%s*HARDI*.nii.gz',
                                            #ASLBOLD_Sentences_ASL='%s_%s_*ASLBOLD_Sentences*ASL.nii.gz',
                                            #ASLBOLD_Sentences_BOLD='%s_%s_*ASLBOLD_Sentences*BOLD.nii.gz',
                                            #ASLBOLD_Stories_ASL='%s_%s_*ASLBOLD_Stories*ASL.nii.gz',
                                            #ASLBOLD_Stories_BOLD='%s_%s_*ASLBOLD_Stories*BOLD.nii.gz',
                                            ASLBOLD_ASL='%s_%s*ASLBOLD_%s*ASL.nii.gz',
                                            ASLBOLD_BOLD='%s_%s*ASLBOLD_%s*BOLD.nii.gz',
                                            RestingState='%s_%s*RESTING_STATE*.nii.gz')
    cmind_datasource.inputs.template_args = dict(alpha=[['subject_id','session_id']],
                                        cbf=[['subject_id','session_id']],
                                        fieldmap_abs=[['subject_id','session_id']],
                                        fieldmap_rads=[['subject_id','session_id']],
                                        T1EST=[['subject_id','session_id']],
                                        T1Struct=[['subject_id','session_id']],
                                        T2Struct=[['subject_id','session_id']],
                                        DTI=[['subject_id','session_id']],
                                        HARDI=[['subject_id','session_id']],
                                        ASLBOLD_ASL=[['subject_id','session_id','paradigm']],
                                        ASLBOLD_BOLD=[['subject_id','session_id','paradigm']],
                                        RestingState=[['subject_id','session_id']])
    #cmind_datasource.inputs.subject_id = None
    #cmind_datasource.inputs.session_id = None
    #cmind_datasource.inputs.paradigm = None
    
    return cmind_datasource             
    
    
def initialize_cmind_datasource(base_directory=None):
    cmind_datasource = nio.DataGrabber(infields=['subject_id','session_id','paradigm'], outfields=['alpha', 'cbf','fieldmap_abs','fieldmap_rads','T1EST','T1Struct','T2Struct','DTI','HARDI','ASLBOLD_ASL','ASLBOLD_BOLD','RestingState'])
    cmind_datasource.inputs.base_directory = base_directory  #  '/media/Data1/CMIND_Other/Matlab_Output/'
    cmind_datasource.inputs.template = '*'
    cmind_datasource.inputs.sort_filelist = True
    cmind_datasource.inputs.raise_on_empty = False   #Don't raise an exception if files aren't found
    cmind_datasource.inputs.field_template = dict(alpha='%s/%s_%s*ALPHA*complex.nii.gz',
                                            cbf='%s/%s_%s*BaselineCBF*.nii.gz',
                                            fieldmap_abs='%s/%s_%s*Philips_GRE*abs.nii.gz',
                                            fieldmap_rads='%s/%s_%s*Philips_GRE*rads.nii.gz',
                                            T1EST='%s/%s_%s*T1EST_TI*.nii.gz',
                                            T1Struct='%s/%s_%s*T1W*defaced.nii.gz',
                                            T2Struct='%s/%s_%s*T2W*defaced.nii.gz',
                                            DTI='%s/%s_%s*DTI*.nii.gz',
                                            HARDI='%s/%s_%s*HARDI*.nii.gz',
                                            #ASLBOLD_Sentences_ASL='%s_%s_*ASLBOLD_Sentences*ASL.nii.gz',
                                            #ASLBOLD_Sentences_BOLD='%s_%s_*ASLBOLD_Sentences*BOLD.nii.gz',
                                            #ASLBOLD_Stories_ASL='%s_%s_*ASLBOLD_Stories*ASL.nii.gz',
                                            #ASLBOLD_Stories_BOLD='%s_%s_*ASLBOLD_Stories*BOLD.nii.gz',
                                            ASLBOLD_ASL='%s/%s_%s*ASLBOLD_%s*ASL.nii.gz',
                                            ASLBOLD_BOLD='%s/%s_%s*ASLBOLD_%s*BOLD.nii.gz',
                                            RestingState='%s/%s_%s*RESTING_STATE*.nii.gz')
    cmind_datasource.inputs.template_args = dict(alpha=[['subject_id','subject_id','session_id']],
                                        cbf=[['subject_id','subject_id','session_id']],
                                        fieldmap_abs=[['subject_id','subject_id','session_id']],
                                        fieldmap_rads=[['subject_id','subject_id','session_id']],
                                        T1EST=[['subject_id','subject_id','session_id']],
                                        T1Struct=[['subject_id','subject_id','session_id']],
                                        T2Struct=[['subject_id','subject_id','session_id']],
                                        DTI=[['subject_id','subject_id','session_id']],
                                        HARDI=[['subject_id','subject_id','session_id']],
                                        ASLBOLD_ASL=[['subject_id','subject_id','session_id','paradigm']],
                                        ASLBOLD_BOLD=[['subject_id','subject_id','session_id','paradigm']],
                                        RestingState=[['subject_id','subject_id','session_id']])
    #cmind_datasource.inputs.subject_id = None
    #cmind_datasource.inputs.session_id = None
    #cmind_datasource.inputs.paradigm = None
    
    return cmind_datasource             
        

def gen_output_names(subject_id,session_id,base_dir='/tmp/nipype_test'): #,paradigm="Stories",acquisition_type='BOLD'):
    import os
    output_dir=os.path.join(base_dir,subject_id + '_' + session_id)  #overwrite value from cmind.globals
    T1Struct_output_dir=os.path.join(output_dir,'T1_proc')
    T2Struct_output_dir=os.path.join(output_dir,'T2_proc')
    #if not os.path.exists(T1Struct_output_dir):
    #    os.makedirs(T1Struct_output_dir)
    T1map_output_dir=os.path.join(output_dir,'T1map')
    fieldmap_output_dir=os.path.join(output_dir,'PhilipsGRE')
    alpha_output_dir=os.path.join(output_dir,'alpha')
    cbf_output_dir=os.path.join(output_dir,'BaselineCBF')
    T1avg_output_dir=os.path.join(output_dir,'T1histogram')
    cbf_reg_output_dir=os.path.join(output_dir,'BaselineCBF','regBBR')
    fname_crop=os.path.join(T1Struct_output_dir,'T1W_Structural_Crop.nii.gz')
    fname_bias=os.path.join(T1Struct_output_dir,'T1W_Structural_N4.nii.gz')
    fname_crop_T2=os.path.join(T2Struct_output_dir,'T2W_Structural_Crop.nii.gz')
    fname_bias_T2=os.path.join(T2Struct_output_dir,'T2W_Structural_N4.nii.gz')
    if subject_id.lower().find('UCLA_')!=-1:
        UCLA_flag=True
    else:
        UCLA_flag=False
        
    return (output_dir,T1Struct_output_dir,T2Struct_output_dir,fname_crop,fname_bias,fname_crop_T2,fname_bias_T2,alpha_output_dir,cbf_output_dir,cbf_reg_output_dir,T1map_output_dir,fieldmap_output_dir,T1avg_output_dir, UCLA_flag)
    
generate_output_names = Function(input_names=["subject_id",
                                              "session_id",
                                              "base_dir"],
                                 output_names=["output_dir",
                                               "T1Struct_output_dir",
                                               "T2Struct_output_dir",
                                               "fname_crop",
                                               "fname_bias",
                                               "fname_crop_T2",
                                               "fname_bias_T2",
                                               "alpha_output_dir",
                                               "cbf_output_dir",
                                               "cbf_reg_output_dir",
                                               "T1map_output_dir",
                                               "fieldmap_output_dir",
                                               "T1avg_output_dir",
                                               "UCLA_flag"],
                                 function=gen_output_names)   

                                  

def gen_ASLBOLD_names(subject_dir,UCLA_flag,paradigm): #,acquisition_type):
    #paradigm="Stories"
    #acquisition_type="BOLD"
    import os
    from cmind.globals import cmind_ASLBOLD_dir
    from cmind.utils.utils import input2bool
    
    UCLA_flag=input2bool(UCLA_flag)
    if UCLA_flag:
        UCLA_string='_UCLA'
    else:
        UCLA_string=''
        
    ASLBOLD_output_dir=os.path.join(subject_dir,'ASLBOLD_'+paradigm)
    #ASLBOLD_Feat_output_dir=os.path.join(ASLBOLD_output_dir,'Feat_'+acquisition_type)
    ASLBOLD_Feat_ASL_output_dir=os.path.join(ASLBOLD_output_dir,'Feat_ASL')
    ASLBOLD_Feat_BOLD_output_dir=os.path.join(ASLBOLD_output_dir,'Feat_BOLD')
    ASLBOLD_reg_output_dir=os.path.join(ASLBOLD_output_dir,'regBBR') #,acquisition_type)
    #ASLBOLD_Outliers_dir=os.path.join(ASLBOLD_output_dir,'Outliers_'+acquisition_type)
    ASLBOLD_Outliers_ASL_dir=os.path.join(ASLBOLD_output_dir,'Outliers_ASL')
    ASLBOLD_Outliers_BOLD_dir=os.path.join(ASLBOLD_output_dir,'Outliers_BOLD')
#    feat_paradigm_str = 'FEAT_'+acquisition_type+paradigm
    feat_paradigm_str_ASL = 'FEAT_ASL_'+paradigm
    feat_paradigm_str_BOLD = 'FEAT_BOLD_'+paradigm
#    FSF_template=os.path.join(cmind_ASLBOLD_dir,'%s_level1_%s.fsf' % (acquisition_type,paradigm))
    FSF_template_ASL=os.path.join(cmind_ASLBOLD_dir,'ASL_level1_%s%s.fsf' % (paradigm, UCLA_string))
    FSF_template_BOLD=os.path.join(cmind_ASLBOLD_dir,'BOLD_level1_%s%s.fsf' % (paradigm, UCLA_string))
#    unfiltered_design_matrix=os.path.join(cmind_ASLBOLD_dir,'unfilt_%s_%s.mat' % (acquisition_type,paradigm))
    unfiltered_design_matrix_ASL=os.path.join(cmind_ASLBOLD_dir,'unfilt_ASL_%s%s.mat' % (paradigm, UCLA_string))
    unfiltered_design_matrix_BOLD=os.path.join(cmind_ASLBOLD_dir,'unfilt_BOLD_%s%s.mat' % (paradigm, UCLA_string))
#    Feat_case='Feat_%s_%s' % (acquisition_type,paradigm)                            
    Feat_case_ASL='Feat_ASL_%s' % (paradigm)                            
    Feat_case_BOLD='Feat_BOLD_%s' % (paradigm)                            
#    outlier_case='Feat_Outliers_%s_%s' % (acquisition_type,paradigm)                            
    outlier_case_ASL='Feat_Outliers_ASL_%s' % (paradigm)                            
    outlier_case_BOLD='Feat_Outliers_BOLD_%s' % (paradigm)                            
    #regBBR_ref=os.path.join(ASLBOLD_reg_output_dir,'EPIvol.nii.gz')
    #LRtoHR_Affine=os.path.join(ASLBOLD_reg_output_dir,'lowres2highres.mat')
    #LRtoHR_Warp=os.path.join(ASLBOLD_reg_output_dir,'lowres2highres_warp.nii.gz')
    #HRtoLR_Affine=os.path.join(ASLBOLD_reg_output_dir,'lowres2highres_inv.mat')
    #HRtoLR_Warp=os.path.join(ASLBOLD_reg_output_dir,'lowres2highres_warp_inv.nii.gz')
                            
    return (ASLBOLD_output_dir,
            ASLBOLD_reg_output_dir, 
            ASLBOLD_Feat_ASL_output_dir, 
            ASLBOLD_Feat_BOLD_output_dir, 
            ASLBOLD_Outliers_ASL_dir, 
            ASLBOLD_Outliers_BOLD_dir, 
            feat_paradigm_str_ASL,
            feat_paradigm_str_BOLD, 
            FSF_template_ASL, 
            FSF_template_BOLD, 
            unfiltered_design_matrix_ASL, 
            unfiltered_design_matrix_BOLD, 
            Feat_case_ASL,
            Feat_case_BOLD,
            outlier_case_ASL,
            outlier_case_BOLD) #, regBBR_ref, LRtoHR_Affine, LRtoHR_Warp, HRtoLR_Affine, HRtoLR_Warp )


generate_ASLBOLD_names = Function(input_names=["subject_dir",
                                              "UCLA_flag",
                                              "paradigm"],
                                 output_names=["ASLBOLD_output_dir",
                                               "ASLBOLD_reg_output_dir",
                                               "ASLBOLD_Feat_ASL_output_dir",
                                               "ASLBOLD_Feat_BOLD_output_dir",
                                               "ASLBOLD_Outliers_ASL_dir",
                                               "ASLBOLD_Outliers_BOLD_dir",
                                               "feat_paradigm_str_ASL",
                                               "feat_paradigm_str_BOLD",
                                               "FSF_template_ASL",
                                               "FSF_template_BOLD",
                                               "unfiltered_design_matrix_ASL",
                                               "unfiltered_design_matrix_BOLD",
                                               "Feat_case_ASL",
                                               "Feat_case_BOLD",
                                               "outlier_case_ASL",
                                               "outlier_case_BOLD"],
                                               #,
                                               #"regBBR_ref",
                                               #"LRtoHR_Affine",
                                               #"LRtoHR_Warp",
                                               #"HRtoLR_Affine",
                                               #"HRtoLR_Warp"],
                                 function=gen_ASLBOLD_names)          