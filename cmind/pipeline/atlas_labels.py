#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def atlas_labels(atlas_vol, split_to_separate_files=False, split_root_name=None):
    """Transform a multi-label atlas from standard MNI standard space to subject space
    
    Parameters
    ----------
    atlas_vol : str
        The atlas should have regions labeled by integers. There can be gaps in
        the label numbering
    split_to_separate_files : bool, optional
        If true, split the atlas into separate masks, one per label value
    split_root_name : str, optional
        If specified the split outputs will be split_root_name+_label%04d.nii.gz
        
    Returns
    -------
    labels : list
        list of the label values found in the atlas
        
        
    """
    
    import numpy as np
    from os.path import join as pjoin
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist
    import nibabel as nib
    
    split_to_separate_files=input2bool(split_to_separate_files)
    
    imbool, atlas_vol,atlas_vol_root, atlas_vol_path, atlas_vol_ext = imexist(atlas_vol)[0:5]
    if not imbool:
        raise IOError("specified atlas_vol not found")

    if split_to_separate_files:
        if split_root_name is None:
            split_root_name=pjoin(atlas_vol_path,atlas_vol_root)
            
        if atlas_vol_ext in ['.nii','.nii.gz']:
            out_ext = atlas_vol_ext
        else: #output NIFTI-GZ for other input types
            out_ext = '.nii.gz'
        
    atlas_nii=nib.load(atlas_vol)            
    atlas=atlas_nii.get_data()
    maxlabel=atlas.max()
    labels=[]
    #mindboggle_labels=csv2dict('/home/lee8rx/src_repositories/my_git/cmind-py-private/cmind/misc/mindboggle_101_labels.csv')

    for n in range(1,int(maxlabel+1)):
        if np.any(atlas==n):
            labels.append(n)
            if split_to_separate_files:
                label_img_nii=nib.Nifti1Image(atlas[atlas==n],atlas_nii.get_affine(),atlas_nii.get_header())
                label_img_nii.to_filename(split_root_name + ('_label%04d%s' % (n,out_ext)))

    return labels

def _testme():
    raise ValueError("TEST NOT YET IMPLEMENTED")    
    
if __name__ == '__main__':
    import sys,argparse
    from cmind.utils.utils import str2none

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()

    else: 
        parser = argparse.ArgumentParser(description="Transform a multi-label atlas from standard MNI standard space to subject space", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("atlas_vol", help="filename of the atlas.  Should work for NIFTI/NIFTIGZ")
        parser.add_argument("-s","--split_to_separate_files", required=False, type=str, default="False", help="If true, split the atlas into separate masks, one per label value")
        parser.add_argument("-oname","--split_root_name", required=False, type=str, default="None", help="If specified the split outputs will be split_root_name+_label%04d.nii.gz")

        args=parser.parse_args()
        
        atlas_vol=args.atlas_vol;
        split_to_separate_files=str2none(args.split_to_separate_files);
        split_root_name=str2none(args.split_root_name);
      
        labels=atlas_labels(atlas_vol,
                     split_to_separate_files=split_to_separate_files,
                     split_root_name=split_root_name)
        print(labels)