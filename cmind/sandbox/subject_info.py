"""misc utilies/examples here"""
import warnings
import pandas as pd
from cmind.utils.utils import is_string_like

if False:
    import os
    import cmind
    from os.path import join as pjoin
    raw_frame_csv=pjoin(os.path.dirname(cmind.__file__),'sandbox','subject_info_query_results.csv')
    
    combined_sessions_csv=pjoin(os.path.dirname(cmind.__file__),'sandbox','combined_sessions_Feb2014.csv')


def get_subject_info(raw_frame,subjID,session,year,all_scan_types=None):
    """ retrieve the age and files corresponding to a given subject ID, scan 
    session and year
    
    Parameters
    ----------
    raw_frame : pandas.core.frame.DataFrame or str
        DataFrame (or .csv) from the cmind database containing at least the
        following columns:
        'Participant ID'
        'Session Type'
        'Year'
        'Scan Type'
        'Participant Age associated with Image File (Days)'
        'File Name'
        'Paradigm'
        
    subjID : str
        the Participant ID.  e.g. 'IRC04H_06M008'
    
    session : {'P','F'}      
        whether the session was physiological or functional
        
    year : int
        longitudinal year
        
    all_scan_types : list, optional
        can be used to restrict the scan types returned to only those types
        contained in the list.  e.g. ['Anatomical',] to retrieve only the info
        for Anatomical scans
        
    Returns
    -------
    age : float
        the subject's age (in Days)
    
    subj_data : dict
        a dictionary where the keys are the scan types and each value is a 
        dictionary with the keys 'files' and/or 'use_fieldmap'
        
        
    """
    

    #If .csv passed in, import as a pandas DataFrame
    if is_string_like(raw_frame):
        if not os.path.exists(raw_frame):
            raise ValueError("Specified raw_frame .csv file not found")
        elif not os.path.splitext(raw_frame)[1]!='.csv.':
            raise ValueError("raw_frame must be either a pandas DataFrame or .csv file")
        raw_frame=pd.DataFrame.from_csv(raw_frame,index_col=False)

    if all_scan_types is None:    
        all_scan_types = list(set(raw_frame['Scan Type']))
        #now replace 'T1Est_*' with a single 'T1Est'
        all_scan_types = sorted([item for item in all_scan_types if item.find('T1Est_')==-1] + ['T1Est'])
        
    #Check that the DataFrame has the required columns from the CMIND database
    required_columns=['Participant ID',
                      'Session Type',
                      'Year',
                      'Scan Type',
                      'Participant Age associated with Image File (Days)',
                      'File Name',
                      'Paradigm']    
    column_list = list(raw_frame.columns)
    for col in required_columns:
        if not col in column_list:
            print("required column {}, not found in raw_frame".format(col))

    #find subset of data matching this scan session
    p_frame=raw_frame[(raw_frame['Participant ID']==subjID)]
    if len(p_frame)==0:
        raise ValueError("subject {} not found in database".format(subjID))
    p_frame=p_frame[p_frame['Year']==int(year)]
    if len(p_frame)==0:
        raise ValueError("longitudinal year {} not found, for subject {}".format(year,subjID))
    p_frame=p_frame[p_frame['Session Type']==session]
    if len(p_frame)==0:
        raise ValueError("session {} not found, for subject {}, year {}".format(session,subjID,year))

    #only include raw data in the query
    if 'File Type' in list(raw_frame.columns): 
        p_frame=p_frame[p_frame['File Type']=='Raw']

    #get the age at scan in days
    age=set(p_frame['Participant Age associated with Image File (Days)'])
    if len(age)==1:
        age=list(age)[0]
    else:
        #ValueError("ages for scan session not unique!")
        warnings.warn("ages for scan session not unique ({},{},{})!.  may be because a 2nd session was required to complete all required scans".format(subjID,session,year))
        
    subj_data={}    
    try:
        #special case to extend the scans across 'P' and 'F' if they were scanned on the same day
        #TODO: If 'P' and 'F' were on the same day, but not in a single session?  Is there a database variable for this?.  For now only assume
        # separate sessions if there are two separate anatomicals
        p_frame2=raw_frame[(raw_frame['Participant ID']==subjID) & (raw_frame['Participant Age associated with Image File (Days)']==age)]
        unique_anatomicals=len(p_frame2[p_frame2['Scan Type']=='Anatomical'])
        if (len(p_frame2)>len(p_frame)) and unique_anatomicals==1:
            print("Assuming a combined physio/functional session for subject for subj {}, session {}, year {}".format(subjID,session,year))
            combined_session=True
            p_frame=p_frame2
        else:
            combined_session=False
        
        GREMap_frame = p_frame[p_frame['Scan Type']=='GREMap']
        if GREMap_frame.empty:
            fieldmap_age=None
        else:
            if(len(GREMap_frame)>2):
                raise ValueError("Multiple {} scans found for subj {}, session {}, year {}".format('GREMap',subjID,session,year))
            elif(len(GREMap_frame)<2):
                raise ValueError("Too few {} scans found for subj {}, session {}, year {}".format('GREMap',subjID,session,year))
            fieldmap_age = GREMap_frame.iloc[0]['Participant Age associated with Image File (Days)']
            subj_data['GREMap']={}
            subj_data['GREMap']['files']=list(GREMap_frame['File Name']) #[0]

        p_anat_frame=p_frame[p_frame['Scan Type']=='Anatomical']       
        if len(p_anat_frame)<1:
            raise ValueError("No T1 anatomical found for subj {}, session {}, year {}".format(subjID,session,year))
        elif len(p_anat_frame)>1:
            raise ValueError("Multiple T1 anatomicals found for subj {}, session {}, year {}".format(subjID,session,year))
        else:
            subj_data['Anatomical']={}
            subj_data['Anatomical']['files']=list(p_anat_frame['File Name'])[0]
            age=float(p_anat_frame['Participant Age associated with Image File (Days)'])
        
        for stype in all_scan_types:
            if stype=='T1Est':
                T1criterion = p_frame['Scan Type'].map(lambda x: x.startswith('T1Est'))
                file_age=p_frame[T1criterion]['Participant Age associated with Image File (Days)'].unique()
                if len(file_age)>1:
                    print(p_frame[T1criterion])
                    raise Exception("T1Est present from multiple sessions?")
                
                if len(file_age)==1:
                    file_age=file_age[0]
                else:
                    file_age=None

                T1est_files=list(p_frame[T1criterion]['File Name'])
                subj_data[stype]={}
                if len(T1est_files)>0:
                    subj_data[stype]={}
                    subj_data[stype]['files']=T1est_files
                    if fieldmap_age and (file_age==fieldmap_age):
                        subj_data[stype]['use_fieldmap']=True
                    else:
                        subj_data[stype]['use_fieldmap']=False
                    
            elif (stype=='Anatomical') or (stype=='GREMap'):
                pass #already did above
            elif stype=='Functional':
                p_scan_frame=p_frame[p_frame['Scan Type']==stype]
                p_scan_frame=p_scan_frame.sort('File Name')
                paradigms=list(set(p_scan_frame['Paradigm']))
                for paradigm in paradigms:
                    p_paradigm_frame = p_scan_frame[p_scan_frame['Paradigm']==paradigm]
                    if len(p_paradigm_frame)>2: #expect two files per paradigm: ASL & BOLD
                        #print("Multiple {},{} scans found for subj {}, session {}".format(stype,paradigm,subjID,sess))
                        #return p_scan_frame
                        raise ValueError("Multiple {},{} scans found for subj {}, session {}, year {}".format(stype,paradigm,subjID,session, year))
                    else:
                        key=(stype,paradigm)
                        subj_data[key]={}
                        subj_data[key]['files']=list(p_paradigm_frame['File Name'])
                        file_age=p_paradigm_frame['Participant Age associated with Image File (Days)'].unique()[0]
                        if fieldmap_age and (file_age==fieldmap_age):
                            subj_data[key]['use_fieldmap']=True
                        else:
                            subj_data[key]['use_fieldmap']=False
            else:
                p_scan_frame=p_frame[p_frame['Scan Type']==stype]    
                p_scan_frame=p_scan_frame.sort('File Name')
                
                #if stype in ['GREMap']: #complex data
                #    Nmax=2
                #else:
                Nmax=1
                #subj_data[stype]={}
                #subj_data[stype]['files']=None
                if len(p_scan_frame)<1:
                    subj_data[stype]={}
                    pass #not found
                elif len(p_scan_frame)>Nmax:
                    raise ValueError("Multiple {} scans found for subj {}, session {}, year {}".format(stype,subjID,session, year))
                else:
                    subj_data[stype]={}
                    subj_data[stype]['files']=list(p_scan_frame['File Name'])
                    if len(subj_data[stype]['files'])==1:
                        subj_data[stype]['files']=subj_data[stype]['files'][0]
                    file_age=p_scan_frame['Participant Age associated with Image File (Days)'].unique()[0]
                    if fieldmap_age and (file_age==fieldmap_age):
                        subj_data[stype]['use_fieldmap']=True
                    else:
                        subj_data[stype]['use_fieldmap']=False
                        
    except Exception as e:
        print(str(e))
        raise Exception("Problem retrieving subject data dictionary")
        #combined_session=None
        subj_data={}
    
    return age, subj_data #, combined_session


def check_combined_sessions(raw_frame, known_combined_sessions_csv=None):
    """finds all subjects with the same file age in both the physio and 
    functional sessions.  This should only happen if the subjects participated 
    in both sessions on the same day.  Also finds any subjects that have 
    multiple ages associated with the same session.  This is usually because a
    return trip was necessary to complete all scan types required.
    """
    
    if combined_sessions_csv is not None:
        combined_sessions_frame = pd.DataFrame.from_csv(combined_sessions_csv,index_col=False)
    
    age_match_list=[]
    multiple_ages_single_session_list=[]
    raw_IDlist = raw_frame['Participant ID'].unique()
    for ID in raw_IDlist:
        id_frame=raw_frame[raw_frame['Participant ID']==ID]
        year_list = id_frame['Year'].unique()
        for year in year_list:
           yr_frame=id_frame[id_frame['Year']==int(year)] 
           session_list = yr_frame['Session Type'].unique()
           if len(session_list)>2:
               raise Exception("Cannot be more than two session types")
           elif len(session_list)==2:
               p_frame = yr_frame[yr_frame['Session Type']=='P']
               age_p = p_frame['Participant Age associated with Image File (Days)'].unique()
               f_frame = yr_frame[yr_frame['Session Type']=='F']
               age_f = f_frame['Participant Age associated with Image File (Days)'].unique()
               if (len(age_p)>1) or (len(age_f)>1):
                   #raise Exception("Cannot be more than one age associated with a single session")
                   multiple_ages_single_session_list.append([ID,year])
               else:
                   if age_p == age_f:
                       if combined_sessions_csv is not None:
                           in_combined_csv=len(combined_sessions_frame[(combined_sessions_frame['ext_subject_id']==ID) & (combined_sessions_frame['year']==year)])>0
                           age_match_list.append([ID,year,in_combined_csv])
                       else:
                           age_match_list.append([ID,year])
                           
    return age_match_list, multiple_ages_single_session_list            
