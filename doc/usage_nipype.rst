.. _usage_nipype:

=================
Using with Nipype
=================

.. Warning:: The Nipype interfaces are still in active development and have not
    be fully tested.  consider this code to be in an alpha state and subject to 
    major revisions going forward.

Nipype_ interfaces to each function are provided within the 
cmind.nipype.interfaces sub-package.  These interfaces can be used to generate
Nipype processing pipelines.  The nipype sub-package is not imported by default 
when ``import cmind`` is run.  Use ``import cmind.nipype.interfaces`` to 
explicitly import it. The interfaces are named identically to their python
function counterparts (e.g. the interface for the function 
``cmind.pipeline.cmind_baselineCBF_preprocess`` would be
``cmind.nipype.interfaces.cmind_baselineCBF_preprocess``).

.. Note:: The interface code contained in cmind.nipype.interfaces.cmind_interfaces
    was entirely automatically generated via parsing of the docstrings for all
    cmind_* pipeline functions via the script 
    cmind.nipype.nipype_generate_interfaces()

.. include:: links_names.txt