.. -*- rest -*-

########
cmind-py
########

cmind-py is a python toolbox for analysis of MRI images.  It was developed for analyzing the datasets 
collected as part of the Cincinnati MR Imaging of NeuroDevelopment (C-MIND) project.

cmind-py is for research only and is not intended for clincal use

Websites
========

The homepage for the C-MIND project is:

`https://research.cchmc.org/c-mind <https://research.cchmc.org/c-mind/>`_

The data collected is publicly available at the following location:

`https://cmind.research.cchmc.org <https://cmind.research.cchmc.org/>`_

Code
====

Source code for the data processing pipelines can be found at:

`https://bitbucket.org/grlee77/cmind-py <https://bitbucket.org/grlee77/cmind-py/>`_

Documentation
=============

An HTML build of the documentation is available at:

https://research.cchmc.org/c-mind-db/py-doc/html/

Pediatric Templates
===================

Download the T1-weighted Pediatric study templates here:

`https://bitbucket.org/grlee77/cmind-py/downloads <https://bitbucket.org/grlee77/cmind-py/downloads/>`_

License
=======

cmind-py is licensed under the terms of the BSD license. Some code included with
cmind-py is also licensed under the BSD license.  Please see the LICENSE file in 
the cmind-py distribution.
