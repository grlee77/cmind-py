#!/bin/bash

while [ "$1" != "" ]; do
    case $1 in
	-gm )
		shift
	    echo "GMfile:$1"
	    ;;	
	-wm )
		shift
	    echo "WMfile:$1"
	    ;;	
	-csf )
		shift
	    echo "CSFfile:$1"
	    ;;	
	-rsf )
		shift
		echo "reg_struct:$1"
	    ;;	
	-head )
		shift
	    echo "T1_vol:$1"
	    ;;	
	-brain )
	 	shift
	    echo "T1_brain:$1"
	    ;;	
    -ppc )   
		shift
		echo "ppc:$1"
		;;
    esac
    shift
done

