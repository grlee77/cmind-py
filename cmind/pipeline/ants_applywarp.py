"""
utility for calling antsApplyTransforms
"""
#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

from cmind.utils.utils import input2bool
from cmind.utils.file_utils import imexist
from cmind.utils.cmind_utils import cmind_reg_report_img
from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

def ants_applywarp(transform_list,fixed,moving,output_warped,ANTS_path=None,dim=3,interp_type="",nopng=False,inverse_flag=False, extra_ANTS_args = '', is_timeseries = False, verbose=False, logger=None):
    """utility for calling antsApplyTransforms
    
    Parameters
    ----------
    transform_list : list
        a list of the transformations to apply
    fixed : str
        image filename for the "fixed" image (registration target)
    moving : str
        image filename for the "moving" image
    output_warped : str
        image filename for the output
    ANTS_path : str
        path to the ants binaries
    dim : {2, 3},optional
        number of image dimensions
    interp_type : str,optional
    nopng : bool,optional
        if False, generate overlay images summarizing the registration
    inverse_flag : bool,optional
        if True, apply the inverse of the transforms in transform_list
    extra_ANTS_args : str, optional
        optional string containing additional ANTs command line arguments
    is_timeseries : bool, optional
        if true, input_name corresponds to timeseries data.  the transformation 
        will be applied to all time frames  (adds ' --input-image-type 3 ' to 
        the antsApplyTransform command line)
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
        
    Notes
    -----
    if `inverse_flag`, then `fixed` is registered to `moving` instead of the
    other way around

    Possible `interp_type` strings are:
        - HammingWindowedSinc
        - NearestNeighbor
        - BSpline[<order=3>]
        - MultiLabel[<sigma=imageSpacing>,<alpha=4.0>]
        - Gaussian[<sigma=imageSpacing>,<alpha=1.0>]
        - CosineWindowedSinc
        - WelchWindowedSinc
        - LanczosWindowedSinc
        - ""
    """
    
    nopng, inverse_flag, verbose = input2bool([nopng, inverse_flag, verbose])
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting ants_applywarp")      
     
    if not isinstance(interp_type,str):
        raise ValueError('interp_type must be either NearestNeighbor, MultiLabel[<sigma=imageSpacing>,<alpha=4.0>], Gaussian[<sigma=imageSpacing>,<alpha=1.0>], BSpline[<order=3>], CosineWindowedSinc, WelchWindowedSinc, HammingWindowedSinc, LanczosWindowedSinc')
    else:
        if not interp_type:
            interp_str=""
        else:
            interp_str=' -n ' + interp_type
    
    (fixed_exists, fixed_full)=imexist(fixed)[0:2];    
    if fixed_exists:
        fixed=fixed_full
    else:
        raise IOError("Fixed image, %s, doesn't exist!" % (fixed))
    
    (moving_exists, moving_full)=imexist(moving)[0:2];
    if moving_exists:
        moving=moving_full
    else:
        raise IOError("Moving image, %s, doesn't exist!" % (moving))
    t_args="";
    
    if inverse_flag:
        transform_order=list(range(len(transform_list)));
        mtmp=moving;
        moving=fixed;
        fixed=mtmp;
    else:
        transform_order=list(range(len(transform_list)-1,-1,-1));
    
    for nn in transform_order:  #last transform in list is applied first, so have to go in reverse order
        tr='"' + transform_list[nn].strip('"') + '"'; #remove any stray double quotes from transform filenames
        if inverse_flag:
            if tr.find('Affine')!=-1:
                tr='[' + tr + ',1]'
            elif tr.find('Warp')!=-1:
                tr=tr.replace('Warp','InverseWarp');
        t_args=t_args + ' -t ' + tr
    t_args=t_args + ' '
    #print "t_args=%s" % t_args

    if is_timeseries:
        extra_ANTS_args += ' --input-image-type 3 '
    
    if not ANTS_path:
        from cmind.globals import cmind_ANTs_dir as ANTS_path
        
    apply_command = ANTS_path + '/antsApplyTransforms -d %d' %(dim) + ' -i "' + moving + '" -o "' + output_warped + '" -r "' + fixed + '"' + extra_ANTS_args + interp_str + t_args
    #print 'cmind_ants_applywarp.py:  %s' % (apply_command)
    log_cmd(apply_command, verbose=verbose, logger=module_logger)
    
    if not nopng:
        cmind_reg_report_img(output_warped, fixed, output_warped.replace('.nii.gz','.png'))
