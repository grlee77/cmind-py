
Mindboggle-101 atlases 
	
	see: README_mindboggle.txt

	OASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_MNI152_2mm.nii.gz
	mindboggle_101_labels.csv	

Anatomical Automatic Labeling Atlas
	
	see: README_AAL.txt

	Version with SPM8 type integer labels:
		aal_spm8_MNI152_2mm.nii.gz
		AAL_spm8_labels.csv

	Version with sequential 1-116 integer labels:
		aal_MNI152_2mm.nii.gz
		AAL_labels.csv

JHU atlas labels:

    There are also .csv files corresponding to the JHU white matter atlases included with FSL
    
    JHU_WM_labels.csv corresponds to JHU-ICBM-labels*.nii.gz
    JHU_WM_tracts.csv corresponds to JHU-ICBM-tracts-maxprob*.nii.gz