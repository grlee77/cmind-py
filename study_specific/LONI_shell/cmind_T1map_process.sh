#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd


usage()
{
echo "Basic usage: cmind_T1map_preprocess.py -o <output_dir> -i <T1EST_nii_folder>  [options]

usage: cmind_T1map_preprocess.sh [-h] -o OUTPUT_DIR -i T1EST_NII_FOLDER
                                 [-a ANTS_BIAS_CMD] [-mc DO_MOTION_COR]
                                 [-gf GENERATE_FIGURES] [-u FORCEUPDATE] [-v]
                                 [-log LOGGER]

T1EST preprocessing prior to fitting T1 values

required_arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -i T1EST_NII_FOLDER, --T1EST_nii_folder T1EST_NII_FOLDER
                        folder containing all of the T1EST NIFTI-GZ files or a
                        list of the individual files


optional arguments:
  -h, --help            show this help message and exit
  -a ANTS_BIAS_CMD, -ants ANTS_BIAS_CMD, --ANTS_bias_cmd ANTS_BIAS_CMD
                        path to the ANTs bias correction binary
  -mc DO_MOTION_COR, --do_motion_cor DO_MOTION_COR
                        coregister the images from the different TI times
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        filename to log to
  -np
  						number of threads
"
}


source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

REQUIRED_ARGS=1
MAX_ARGS=6
if [ $# -lt $REQUIRED_ARGS ]
then
  echo "Usage: `basename $0` output_dir T1EST_nii_folder ANTS_bias_cmd [do_motion_cor] [generate_figures] [ForceUpdate]"
  exit 1
fi

#Assign input arguments to variables
ANTS_bias_cmd="N4BiasFieldCorrection"
generate_figures="true"
do_motion_cor="true"
ForceUpdate="true"

while [ "$1" != "" ]; do
    case $1 in
	-np )
	    shift
	    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1;   #TODO: also set export OMP_NUM_THREADS=$1;
	    ;;
	-i|--T1EST_nii_folder )
	    shift
	    T1EST_nii_folder=$1
	    ;;
	-o|--output_dir )
         shift
	    output_dir=$1
	    ;;
	-a|-ants|--ANTS_bias_cmd )
	    shift
	    ANTS_bias_cmd=$1
	    ;;	
	-mc|--do_motion_cor )
	    shift
	    do_motion_cor=$1
	    ;;
	-gf|--generate_figures )
	    shift
	    generate_figures=$1
	    ;;	
	-u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done

if [ ! -z $verbose_string ]; then
	debug=true
else
	debug=false
fi


if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi 

if [ ! -d $T1EST_nii_folder ]; then
    echo "ERROR: T1est directory: $T1EST_nii_folder doesn't exist"
    exit 1
fi 


if $debug
then
    echo T1est_nii_folder: $T1EST_nii_folder
    echo output_folder: $output_dir
    echo ANTS bias command: $ANTS_bias_cmd
    echo generate figs:$generate_figures
    echo do motion correction: $do_motion_cor
    echo force update: $ForceUpdate
fi

#Check that the ANTs executable is on the path

#Check that the ANTs executable is on the path
command -v $ANTS_bias_cmd >/dev/null 2>&1 || { echo "The ANTs tool N4BiasFieldCorrection is required, but was not found. Aborting." >&2; exit 1; }
ANTS_bias_cmd=`command -v $ANTS_bias_cmd`

echo "Starting T1map_preprocess"
# t=$(timer)

# if $debug
# then
#     python ${cmind_python_dir}/cmind_T1map_preprocess.py ${output_dir} ${T1EST_nii_folder} ${ANTS_cmd_fullpath} --generate_figures $generate_figures --do_motion_cor $do_motion_cor --ForceUpdate $ForceUpdate --verbose
# else
#     python ${cmind_python_dir}/cmind_T1map_preprocess.py ${output_dir} ${T1EST_nii_folder} ${ANTS_cmd_fullpath} --generate_figures $generate_figures --do_motion_cor $do_motion_cor --ForceUpdate $ForceUpdate
# fi

# printf 'T1map_preprocess:  Elapsed time: %s\n' $(timer $t)
# t=$(timer)


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir T1EST_nii_folder ANTS_bias_cmd do_motion_cor generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
all_optional_args=""
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_T1map_preprocess.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_T1map_preprocess.py ${verbose_string} ${all_optional_args}
printf 'cmind_T1map_preprocess.py:  Elapsed time: %s\n' $(timer $t)

#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    T1concat_mcf
#    TIvals
#    T1concat_mean_N4
#    T1concat_mean_N4_brain
#    T1concat_mean_N4_mask

output_file_test=${output_dir}/T1concat_mean_N4_brain.nii.gz
T1concat_mcf=${output_dir}/T1concat_mcf.nii.gz
TIvals=${output_dir}/TIvals.txt
T1concat_mean_N4=${output_dir}/T1concat_mean_N4.nii.gz
T1concat_mean_N4_mask=${output_dir}/T1concat_mean_N4_brain_mask.nii.gz
T1map_Dir=${output_dir}



#check that T1map Preprocessed output exists
echo "$output_file_test"
if [ ! -f $output_file_test ]; then
    echo "ERROR: Failed to finish T1 Map Preprocessing"
    exit 1
fi 

T1concat_nii=$T1concat_mcf
mask_file=$T1concat_mean_N4_mask

# #T1map fitting
# if $debug
# then
#     python ${cmind_python_dir}/cmind_T1map_fit.py $output_dir $T1concat_mcf $TIvals --generate_figures $generate_figures --ForceUpdate $ForceUpdate --mask_file $T1concat_mean_N4_mask --verbose
# else
#     python ${cmind_python_dir}/cmind_T1map_fit.py $output_dir $T1concat_mcf $TIvals --generate_figures $generate_figures --ForceUpdate $ForceUpdate --mask_file $T1concat_mean_N4_mask 
# fi

# printf 'T1map_fitting:  Elapsed time: %s\n' $(timer $t)

#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir T1concat_nii TIvals mask_file generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
all_optional_args=""
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_T1map_fit.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_T1map_fit.py ${verbose_string} ${all_optional_args}
printf 'cmind_T1map_fit.py:  Elapsed time: %s\n' $(timer $t)

#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    T1_map_fname
#    I0_map_fname
#    NRMSE_map_fname


T1_map=${output_dir}/T1_map.nii.gz
M0_map=${output_dir}/I0_map.nii.gz
T1_NRMSE_map=${output_dir}/T1_NRMSE_map.nii.gz
echo "$output_file_test"
if [ ! -f $T1_map ]; then
    echo "ERROR: Failed to finish T1 Map Fitting"
    exit 1
fi 


# if $debug
# then
#     echo "T1_map:$T1_map"
#     echo "M0_map:$M0_map"
#     echo "T1_NRMSE_map:$T1_NRMSE_map"  #keep alternate name here for now for backwards compatibility with old .pipe
    
#     echo "T1concat_mcf:$T1concat_mcf"  #keep alternate name here for now for backwards compatibility with old .pipe
#     echo "TIvals:$TIvals"
#     echo "T1concat_mean_N4:$T1concat_mean_N4"
#     echo "T1concat_mean_N4_mask:$T1concat_mean_N4_mask"  #keep alternate name here for now for backwards compatibility with old .pipe
#     echo "T1map_Dir:$T1map_Dir"
    
# fi
  

echo "Finished cmind_T1map_preprocess"

