#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def _move_segment_iter(WM_vol, GM_vol, CSF_vol,Seg_img,iter_no,image_only=True, verbose=False, logger=None):
    """copy/rename files from a previous segmentation iteration to prepare for the next
    """
    
    import os
    import warnings
    
    from cmind.utils.file_utils import split_multiple_ext
    from cmind.utils.logging_utils import log_cmd

    iter_no=int(iter_no)
    
    if not image_only: #keep copies of segmentation results, wither iteration number appended to names
        WM_vol_previous_iter=split_multiple_ext(WM_vol)[0]+'_iter%04d' % iter_no
        GM_vol_previous_iter=split_multiple_ext(GM_vol)[0]+'_iter%04d' % iter_no
        CSF_vol_previous_iter=split_multiple_ext(CSF_vol)[0]+'_iter%04d' % iter_no
        
        log_cmd('$FSLDIR/bin/immv %s %s' % (WM_vol,WM_vol_previous_iter), verbose=verbose, logger=logger)
        log_cmd('$FSLDIR/bin/immv %s %s' % (GM_vol,GM_vol_previous_iter), verbose=verbose, logger=logger)
        log_cmd('$FSLDIR/bin/immv %s %s' % (CSF_vol,CSF_vol_previous_iter), verbose=verbose, logger=logger)
    else:  #no copies of intermediate iterations will be kept
       WM_vol_previous_iter=WM_vol
       GM_vol_previous_iter=GM_vol
       CSF_vol_previous_iter=CSF_vol        
   
    #keep a copy of the summary image for each iteration
    try:
        if os.path.exists(Seg_img):
            log_cmd('mv %s %s' % (Seg_img,split_multiple_ext(Seg_img)[0]+'_iter%04d' % iter_no + '.png'), verbose=verbose, logger=logger)
    except:
        warnings.warn("failed to move segmentation summary images from previous iteration")
        
    return (WM_vol_previous_iter,GM_vol_previous_iter,CSF_vol_previous_iter)
                  
                  
def cmind_segment(fname_brain, seg_name=None, reg_struct=None, use_prior=False, use_alt_prior=False, use_prior_throughout=False, resegment_using_N4mask=False, ANTS_bias_cmd=None, generate_figures=False, ForceUpdate=False, verbose=False, logger=None):
    """ Segmentation using FSL's FAST
    
    Parameters
    ----------
    fname_brain : str
        input brain volume
    seg_name : str
        root name to use for the output filenames
    reg_struct : str or dict, optional
        registration structure
    use_prior : bool, optional
        prior image: see FSL Fast documentation
    use_alt_prior : bool, optional    
        alternative prior images: see FSL Fast documentation
    use_prior_throughout : bool, optional
        use priors throughout: see FSL Fast documentation
    resegment_using_N4mask : bool, optional
        rerun N4 bias correction on a mask weighted toward white matter then resegment
    ANTS_bias_cmd : str, optional
        path to ANTs N4BiasFieldCorrection binary (used if resegment_using_N4mask=True)
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    WM_vol : str
        WM partial volume estimate
    GM_vol : str
        GM partial volume estimate
    CSF_vol : str
        CSF partial volume estimate
        
    Notes
    -----
      
    .. figure::  ../img/Segmentation_Slices_squeezed.png
       :align:   center
    
       Examples of segmentation partial volume estimates (Top: CSF, Middle: GM, Bottom: WM)
        
    """

    import os
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict
    from cmind.utils.cmind_HTML_report_gen import cmind_HTML_reports
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    from cmind.pipeline.cmind_segment import _move_segment_iter
        
    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, use_prior, use_alt_prior, use_prior_throughout, resegment_using_N4mask = input2bool([generate_figures, ForceUpdate, verbose, use_prior, use_alt_prior, use_prior_throughout, resegment_using_N4mask])
    
    if resegment_using_N4mask:  #TODO: move old outputs before rerunning so that ForceUpdate=True flag can be removed
        from cmind.pipeline.cmind_bias_correct import cmind_bias_correct
        
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Segmentation")      
            
#    if verbose:
#        from time import time
#        print "Starting Segmentation with FSL's FAST"
#        starttime=time()
    
    (imbool, fname_brain, fname_brain_root, fname_brain_path, fname_brain_ext)=imexist(fname_brain,'nifti-1')
    if not imbool:
        raise IOError("input image, %s, doesn't exist!" % fname_brain)

    if not seg_name:
        if use_prior:
            sn='T1SegmentPrior'
        elif use_alt_prior:
            sn='T1SegmentAltPrior'
        else:
            sn='T1Segment'
        seg_name=os.path.join(fname_brain_path,sn)
        
    elif not os.path.dirname(seg_name):
        seg_name=os.path.join(fname_brain_path,seg_name)
        
    sn=os.path.basename(seg_name)
     
    output_dir=os.path.dirname(seg_name);
    CSF_vol=os.path.join(output_dir,seg_name+'_CSF'+fname_brain_ext)
    GM_vol=os.path.join(output_dir,seg_name+'_GM'+fname_brain_ext)
    WM_vol=os.path.join(output_dir,seg_name+'_WM'+fname_brain_ext)
    
    if (not imexist(CSF_vol,'nifti-1')[0]) or ForceUpdate:
        # run segmentation
        if use_prior or use_alt_prior:
            if isinstance(reg_struct,str):
                if os.path.exists(reg_struct):
                    reg_struct=csv2dict(reg_struct)
                else:
                    raise IOError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
            elif not isinstance(reg_struct,dict):
                raise IOError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
        if use_prior:  #use default FSL priors
           prior_str="-a %s" % reg_struct['FLIRT_HR_to_MNI_Inv_Aff']        
        elif use_alt_prior: #use custom priors
            prior_str="-A %s %s %s" % (reg_struct['StudyTemplate_csf'], reg_struct['StudyTemplate_gm'], reg_struct['StudyTemplate_wm'])
        else: #no prior
            prior_str=""
        
        if use_prior_throughout and (use_prior or use_alt_prior):
            prior_str = prior_str + " -P"
                    
        log_cmd('$FSLDIR/bin/fast -v --nobias -o %s %s %s' % (seg_name,prior_str,fname_brain), verbose=verbose, logger=module_logger)
    
#        if verbose:
#            print 'FAST Segmentation:  Elapsed time: %s\n' % (time()-starttime)
        
        # rename outputs to tissue types
        log_cmd('$FSLDIR/bin/immv %s_pve_0 %s' % (seg_name,CSF_vol), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/immv %s_pve_1 %s' % (seg_name,GM_vol), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/immv %s_pve_2 %s' % (seg_name,WM_vol), verbose=verbose, logger=module_logger)
        #log_cmd('$FSLDIR/bin/fslmaths '+seg_name+'_CSF -add '+seg_name+'_WM '+seg_name+'_WMorCSF', verbose=verbose, logger=logger)
    
        # binary masks where the partial volume of WM is >=0.5
        WM_50pct=os.path.join(output_dir,seg_name+'_WM_50pct')
        log_cmd('$FSLDIR/bin/fslmaths %s -thr 0.5 -bin %s' % (WM_vol, WM_50pct), verbose=verbose, logger=module_logger)
        #fslmaths ${seg_name}_WMorCSF  -thr 0.5 -bin ${seg_name}_WMorCSF_50pct
        
        #create a WM edge mask for overlay in later BBR-based registrations
        
        WM_edge=os.path.join(output_dir,seg_name+'_WMedge')
        log_cmd('$FSLDIR/bin/fslmaths %s -edge -bin -mas %s %s' % (WM_50pct,WM_50pct,WM_edge), verbose=verbose, logger=module_logger)
        
        if generate_figures:
            if use_prior:
                pstr='_Prior'
            elif use_alt_prior:
                pstr='_AltPrior'
            else:
                pstr=''
            CSF_img=os.path.join(output_dir,'CSF%s_slices.png' % pstr)
            GM_img=os.path.join(output_dir,'GM%s_slices.png' % pstr)
            WM_img=os.path.join(output_dir,'WM%s_slices.png' % pstr)
            Seg_img=os.path.join(output_dir,'Segmentation%s_Slices.png' % pstr)
            log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (CSF_vol,CSF_img), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (GM_vol,GM_img), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer %s -i 0 1 -a %s' % (WM_vol,WM_img), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/pngappend %s - %s - %s %s' % (CSF_img,GM_img,WM_img,Seg_img), verbose=verbose, logger=module_logger)
            os.remove(CSF_img)
            os.remove(GM_img)
            os.remove(WM_img)
            WMedge_img=os.path.join(output_dir,'WMedge%s_slices.png' % pstr)
            log_cmd('$FSLDIR/bin/slicer %s %s_WM_50pct -a %s' % (fname_brain, seg_name, WMedge_img), verbose=verbose, logger=module_logger)
        
        
        if resegment_using_N4mask:  #TODO: move old outputs before rerunning so that ForceUpdate=True flag can be removed
            
            #generate a weighting mask for use during bias field correction.
            #using at least a weight of 0.5 in whole brain, but up to 1 in white matter
            weight_mask=os.path.join(output_dir,'N4_weight_mask.nii.gz')
            fname_brain_mask=os.path.join(output_dir,'tmp_brainmask')
            log_cmd('$FSLDIR/bin/fslmaths %s -thr 0 -bin %s' % (fname_brain, fname_brain_mask),verbose=verbose,logger=logger)
            log_cmd('$FSLDIR/bin/fslmaths %s -mul 0.5 -add %s -div 1.5 %s' % (fname_brain_mask,WM_vol,weight_mask),verbose=verbose,logger=logger)
            log_cmd('$FSLDIR/bin/imrm %s' % (fname_brain_mask),verbose=verbose,logger=logger)
            
            if (len(sn)>=2) and sn[0:2]=='T2':
                bias_oname_root='T2W'
            else:
                bias_oname_root='T1W'
            #run bias field correction using this weighting mask
            fname_brain, bias_field_vol=cmind_bias_correct(fname_brain, fname_brain, ANTS_bias_cmd, oname_root=bias_oname_root, weight_mask=weight_mask, generate_figures=generate_figures, ForceUpdate=True, verbose=verbose, logger=logger) #current brain must be overwritten so ForceUpdate=True
            
            #move the results of the previous iteration
            iter_no=1
            _move_segment_iter(WM_vol, GM_vol, CSF_vol,Seg_img,iter_no,image_only=True, verbose=verbose, logger=module_logger)
            
            #rerun segmentation on the new bias corrected brain
            (WM_vol, GM_vol, CSF_vol)=cmind_segment(fname_brain, seg_name=seg_name, generate_figures=generate_figures, use_prior=use_prior, use_alt_prior=use_alt_prior, reg_struct=reg_struct, use_prior_throughout=use_prior_throughout, resegment_using_N4mask=False, ANTS_bias_cmd=ANTS_bias_cmd, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
       
    writeHTML=True
    if writeHTML:
        cmind_HTML_reports('Segment',output_dir,ForceUpdate=ForceUpdate)

    
            #/media/Data1/BRAINSTools/Scripts/antsAtroposN4.sh -d 3 -a IRC04H_00F011_P_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1.nii.gz -a IRC04H_00F011_P_1_WIP_T2W_3D_FLAIR_1x1x1_SENSE_21_1.nii.gz -c 5 -o T1Segment_Atropos
    else:
        print("Brain segmentation previously performed...skipping")        
        
    return (WM_vol, GM_vol, CSF_vol)
        
def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    seg_name=None
    reg_struct=None
    use_prior=False
    use_alt_prior=False
    use_prior_throughout=False
    generate_figures=True
    resegment_using_N4mask=True
    ANTS_bias_cmd=None
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
   
    if verbose:
        func=cmind_timer(cmind_segment,logger=logger)
    else:
        func=cmind_segment

    testcases=['P','F']
    for testcase in testcases:    
        fname_brain=os.path.join(cmind_example_output_dir,testcase,
                                'T1_proc','T1W_Structural_N4_brain.nii.gz')
        func(fname_brain, seg_name=seg_name, reg_struct=reg_struct, use_prior=use_prior, 
            use_alt_prior=use_alt_prior, use_prior_throughout=use_prior_throughout, 
            resegment_using_N4mask=resegment_using_N4mask, ANTS_bias_cmd=ANTS_bias_cmd, 
            generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, 
            logger=logger)  


 
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()

    else:    
        parser = argparse.ArgumentParser(description="Segmentation using FSL's FAST", epilog="")  #-h, --help exist by default
        #example of a positional argument
        
        parser.add_argument("-i","-t1_vol","--fname_brain",required=True, help="input brain volume")
        parser.add_argument("-name","--seg_name",type=str,default="None", help="root name to use for the output filenames")
        parser.add_argument("-rsf","--reg_struct",type=str,default="None", help="registration structure")
        parser.add_argument("-a","--use_prior",type=str,default="False", help="prior image: see FSL Fast documentation")
        parser.add_argument("-A","--use_alt_prior",type=str,default="False", help="alternative prior images: see FSL Fast documentation")
        parser.add_argument("-P","--use_prior_throughout",type=str,default="False", help="use priors throughout: see FSL Fast documentation")
        parser.add_argument("-reseg","--resegment_using_N4mask",type=str,default="False", help="rerun N4 bias correction on a mask weighted toward white matter then resegment")
        parser.add_argument("-ants","--ANTS_bias_cmd",type=str,default="None", help="path to ANTs N4BiasFieldCorrection binary (used if resegment_using_N4mask=True)")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        fname_brain=args.fname_brain;
        seg_name=str2none(args.seg_name, none_strings=['none','default'])
        reg_struct=str2none(args.reg_struct, none_strings=['none','default'])
        use_prior=args.use_prior;
        use_alt_prior=args.use_alt_prior;
        use_prior_throughout=args.use_prior_throughout;
        resegment_using_N4mask=args.resegment_using_N4mask;
        ANTS_bias_cmd=str2none(args.ANTS_bias_cmd, none_strings=['none','default'])
        generate_figures=args.generate_figures;
        #UCLA_flag=args.UCLA_flag;
        #generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger
        
        if verbose:
            cmind_segment=cmind_timer(cmind_segment,logger=logger)

        cmind_segment(fname_brain, seg_name=seg_name, reg_struct=reg_struct, use_prior=use_prior, 
            use_alt_prior=use_alt_prior, use_prior_throughout=use_prior_throughout, 
            resegment_using_N4mask=resegment_using_N4mask, ANTS_bias_cmd=ANTS_bias_cmd, 
            generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)       
             