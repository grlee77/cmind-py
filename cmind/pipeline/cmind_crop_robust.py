#!/usr/bin/env python
from __future__ import division, print_function, absolute_import
#moved all imports to inside the function so it is compatible with wrapping in nipype

def cmind_crop_robust(fname,fname_crop,age_months=252, z_crop_mm=None, oname_root='T1W', minthresh=0.03, fname2=None, fname2_crop=None, oname2_root='T2W', generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """ utility to reduce FOV in S/I direction, removing some of the neck
    
    Parameters
    ----------
    fname : str
        input filename
    fname_crop : str
        output filename.  if a directory name is given instead, a file named 
        `oname_root`+_Structural_Crop.nii.gz will be created in that folder
    age_months : float
        subject age in months
    z_crop_mm : int, optional
        size to crop to in mm.  If specified overrides the automatic setting 
        via age_months
    oname_root : str, optional
        output filename prefix
    minthresh : float, optional
        percentage threshold below which to consider intensities to be noise.
        This is used when detecting the number of "empty" slices above the head.
    fname2 : str, optional
        second input file to apply the identical fslroi crop to
    fname2_crop : str, optional
        output filename for the second input
    oname2_root : str, optional
        output filename prefix for fname2
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    fname_crop : str
        filename of the cropped image
        
    Notes
    -----
    Crops the FOV in S/I direction by an age-dependent amount.
    This is a rough crop and is not intended to remove all of the neck, etc.
  
    .. figure::  ../img/T1W.png
       :align:   center
    
       T1-weighted head prior to crop
    
    .. figure::  ../img/T1W_Crop.png
       :align:   center
    
       T1-weighted head after crop

    
    """
    import os
    
    import nibabel as nib
    import numpy as np
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    from cmind.finders import check_exist
    check_exist('FSL') #raise exception if required external software not found
    #TODO: could easily rewrite this to remove FSL dependency

    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    
    if (minthresh<0) or (minthresh>1):
        raise ValueError("Error:  minthresh (fractional noise intensity) must fall in the range [0 1]")    
        
    
    module_logger.info("Starting Crop of %s" % fname)
    
    if z_crop_mm:
        mm_keep = z_crop_mm
        module_logger.info("Cropping FOV in S/I to %d mm" % (mm_keep))
    else:  #auto-setting the size to keep based on age
        age_months=float(age_months)
        
        if (not age_months) or age_months==0:  #default, keep full adult size
            mm_keep=190;
        elif (age_months<6):
            mm_keep=145;
        elif (age_months<24):
            mm_keep=160;
        elif (age_months<48):
            mm_keep=170; 
        else:
            mm_keep=190;
    
        module_logger.info("Based on age of %s months, cropping FOV in S/I to %d mm" % (age_months, mm_keep))
    
    (imbool, fname_full)=imexist(fname,'nifti-1')[0:2]
    if not imbool:
        raise IOError("input image, %s, doesn't exist!" % fname)
    
    
    if fname2:
        (imbool2, fname2_full)=imexist(fname2,'nifti-1')[0:2]
        if not imbool2:
            raise IOError("second input image, %s, doesn't exist!" % fname2)
            if not fname2_crop:
                fname2_crop=fname2.replace('.nii','_crop.nii')
    
    if fname_crop and os.path.isdir(fname_crop):  #if specified fname_crop was a directory, use a default name
        fname_crop=os.path.join(fname_crop,oname_root+'_Structural_Crop.nii.gz')
    
    outputs_exist=imexist(fname_crop,'nifti-1')[0]
        
    if fname2_crop:
        outputs_exist=outputs_exist and imexist(fname2_crop,'nifti-1')[0]
        
    if (not outputs_exist) or ForceUpdate:
        
        output_dir=os.path.dirname(fname_crop)
        if not output_dir:
            output_dir='.'
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
            
        #read in image and detect number of empty slices to discard at top (ncut)
        nii1=nib.load(fname_full); 
        nii1_hdr=nii1.get_header()
        try:
            voxel_sizes=nii1_hdr.get_zooms()
            z_dim_mm = voxel_sizes[2]
        except:
            raise Exception("ValueError:  unable to get voxel dimensions from image header")
            
        #convert from mm to z pixels
        nz_keep = int(np.ceil(mm_keep/z_dim_mm))
        
        nii1_img=np.asarray(nii1.get_data(),dtype='float'); 
        maxval=float(np.max(nii1_img));
        nii1_img=nii1_img/maxval;        #normalize max intensity to 1
        nii1_img[nii1_img<minthresh]=0;  #set noise voxels to zero
        Nslices=nii1_img.shape[2]
        ncut=0; 
        for ncut in range(int(np.floor(Nslices/4))): #check top portion of the image for empty slices
            minsum=10;  #tested on data from two sites, but may need to be increased or decreased for other datasets
            empty_slice_bool=np.sum(nii1_img[:,:,-ncut-1])<=minsum  #if the sum over the full slice is less than minsum, consider it empty
            if not empty_slice_bool:
                break
        
        #choose starting point for ROI based on size and number of slices to cut
        pad_top=int(np.ceil(5/z_dim_mm));  #leave at least pad_top mm of blank slices above brain in ROI
        zstart=Nslices-ncut+min(ncut,pad_top)-nz_keep;  
        
        log_cmd('$FSLDIR/bin/fslroi %s %s 0 -1 0 -1 %d %d' % (fname_full,fname_crop,zstart,nz_keep), verbose, module_logger)
        if fname2: #optionally apply identical cropping to a second input file
            log_cmd('$FSLDIR/bin/fslroi %s %s 0 -1 0 -1 %d %d' % (fname2_full,fname2_crop,zstart,nz_keep), verbose, module_logger)
            
        if generate_figures:
            figure_dir=os.path.dirname(fname_crop)
            log_cmd('$FSLDIR/bin/slicer %s -a %s' % (fname_full , os.path.join(figure_dir,oname_root+'.png')), verbose, module_logger)
            log_cmd('$FSLDIR/bin/slicer %s -a %s' % (fname_crop, os.path.join(figure_dir,oname_root+'_Crop.png')), verbose, module_logger)
            if fname2: #optionally apply identical cropping to a second input file
                figure_dir=os.path.dirname(fname2_crop)
                log_cmd('$FSLDIR/bin/slicer %s -a %s' % (fname2_full , os.path.join(figure_dir,oname2_root+'.png')), verbose, module_logger)
                log_cmd('$FSLDIR/bin/slicer %s -a %s' % (fname2_crop, os.path.join(figure_dir,oname2_root+'_Crop.png')), verbose, module_logger)
            
        module_logger.info('Crop Finished')
    else:
        module_logger.info("Brain cropping previously performed...skipping")
        
    return fname_crop


def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_dir, cmind_example_output_dir
    
    age_months=84
    #age_months=None
    z_crop_mm=None #190
    oname_root='T1W'
    generate_figures=True
    ForceUpdate=True;
    verbose=True
    fname2=None
    fname2_crop=None
    oname2_root=None
    minthresh=0.03
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
    if verbose:
        func=cmind_timer(cmind_crop_robust,logger=logger)
    else:
        func=cmind_crop_robust
    
    testcases=['P','F']
    for testcase in testcases:
        
        if testcase=='P':
            fname=os.path.join(cmind_example_dir,'IRC04H_06M008_P_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1_defaced.nii.gz')
            fname_crop=os.path.join(cmind_example_output_dir,'P','T1_proc','T1W_Structural_Crop')
        elif testcase=='F':
            fname=os.path.join(cmind_example_dir,'IRC04H_06M008_F_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1_defaced.nii.gz')
            fname_crop=os.path.join(cmind_example_output_dir,'F','T1_proc','T1W_Structural_Crop')

        func(fname,fname_crop,age_months=age_months, z_crop_mm=z_crop_mm, oname_root=oname_root,fname2=fname2, fname2_crop=fname2_crop, oname2_root=oname2_root, minthresh=minthresh, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    

def _arg_parser():
    import argparse
    parser = argparse.ArgumentParser(description="utility to reduce FOV in S/I direction, removing some of the neck", epilog="")  #-h, --help exist by default
    #example of a positional argument
    parser.add_argument("fname", help="input filename")
    parser.add_argument("fname_crop", help="output filename.  if a directory name is given instead, a file named ${oname_root}_Structural_Crop.nii.gz will be created in that folder")
    #group = parser.add_mutually_exclusive_group(required=True)
    #group.add_argument("-a","--age_months", type=float, dest="age_months",help="subject age in months.  If provided will auto-set z_crop_mm based on the subject age.")
    #group.add_argument("-c","--z_crop_mm", dest="z_crop_mm", type=int, help="z FOV extent to keep (in mm).   Takes precedence over age_months value.")
    parser.add_argument("-a","--age_months", type=float, default=252,dest="age_months",help="subject age in months.  If provided will auto-set z_crop_mm based on the subject age.")
    parser.add_argument("-c","--z_crop_mm", dest="z_crop_mm", type=int, help="z FOV extent to keep (in mm).   Takes precedence over age_months value.")
    parser.add_argument("--oname_root",type=str,default="T1W", help="output filename prefix")
    parser.add_argument("--minthresh",type=float,default=0.03, help="percentage threshold below which to consider intensities to be noise. This is used when detecting the number of 'empty' slices above the head.")
    parser.add_argument("--fname2",type=str,default="None", help="second input file to apply the identical fslroi crop to")
    parser.add_argument("--fname2_crop",type=str, help="output filename for the second input")
    parser.add_argument("--oname2_root",type=str,default="T2W", help="output filename prefix for fname2")
    group1 = parser.add_argument_group('additional optional arguments', 'Other arguments affecting program execution')
    group1.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
    group1.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
    group1.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
    group1.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
    return parser
    
    
if __name__ == '__main__':
    import sys
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer
        
    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
        
    else:    
        parser = _arg_parser()
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        fname=args.fname;
        fname_crop=args.fname_crop;
        age_months=args.age_months;
        z_crop_mm=args.z_crop_mm;
        oname_root=args.oname_root
        minthresh=args.minthresh;
        fname2=str2none(args.fname2);
        fname2_crop=str2none(args.fname2_crop);
        oname2_root=str2none(args.oname2_root)
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose; 
        logger=args.logger 

        if verbose:
            cmind_crop_robust=cmind_timer(cmind_crop_robust,logger=logger)
        
        cmind_crop_robust(fname,fname_crop,age_months=age_months, z_crop_mm=z_crop_mm, oname_root=oname_root,fname2=fname2, fname2_crop=fname2_crop, minthresh=minthresh, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
        
                    
                    
