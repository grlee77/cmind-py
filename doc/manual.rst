.. -*- mode: rst -*-

.. _manual:

cmind-py User Manual
====================

This page contains details of the processed data files that are available in the 
C-MIND database as well as a few examples of data processing using files obtained
from the database.

Installation
============
	
.. toctree::

   install.rst	

.. _pipelines:

CMIND Pipeline Details
======================

.. toctree::

   pipelines_struct.rst
   pipelines_physio.rst
   pipelines_func.rst

.. _usage:

Software Usage
==============

.. toctree::

   usage_command_line.rst
   usage_standalone_gui.rst
   usage_LONI.rst
   usage_nipype.rst


Database Outputs
================

.. toctree::

   Database_outputs.rst


.. _examples:

Example usage of C-MIND processed data
======================================

.. toctree::

   examples.rst

References
==========

.. toctree::

   citations.rst

Indices and tables
==================

* :ref:`search`


.. TODO: add link to the public C-MIND database page

.. TODO: add additional examples

