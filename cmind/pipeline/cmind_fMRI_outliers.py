#!/usr/bin/env python
from __future__ import division, print_function, absolute_import
    
def orthogonalize(ts1,ts2):
    """ orthogonalize ts1 with respect to ts2
    (Gram-Schmidt orthogonalization)
    
    Parameters
    ----------
    ts1 : timeseries to process
    ts2 : timeseries to remove
    
    Returns
    -------
    ts1 : ts1 after orthogonalization to ts2
    
    """
    import numpy as np
    
    ts1=np.asarray(ts1)
    ts2=np.asarray(ts2)
    
    #force arrays to be nominally 2D:  [N x 1]
    ndim1=ts1.ndim
    ndim2=ts2.ndim
    if ndim1==1:
        ts1=np.expand_dims(ts1,1)
    if ndim2==1:
        ts2=np.expand_dims(ts2,1)
        
    s1=ts1.shape
    s2=ts2.shape
    if s1[0]>1 and s1[1]>1:
        raise ValueError("input ts1 must be 1D")
    else:
        if s1[1]>s1[0]:
            s1=s1.T;
    if s2[0]>1 and s2[1]>1:
        raise ValueError("input ts2 must be 1D")
    else:
        if s2[1]>s2[0]:
            s2=s2.T;
            
    #ts2inv = np.linalg.pinv(ts2)
    ts2inv=ts2.T/np.dot(ts2.T,ts2) 
    beta=np.dot(ts2inv,ts1)  #matrix multiplication
    ts1=ts1-beta*ts2  #residual after removal of ts2 component from ts1
    ts1=ts1.flatten()
    return ts1

def binstr2dec(binstr):
    """Convert a binary string such as "0100101" to its decimal (base 10)
    representation
    
    Parameters
    ----------
    binstr : str
        binary string to convert
    
    Returns
    -------
    decval : int
        decimal value corresponding to binstr
        
    """
    decval=0;
    for dpos in range(len(binstr)):
        d=int(binstr[-1-dpos])
        if d==1:
            decval=decval+2**dpos
        elif d!=0:
            raise ValueError("binstr must be a string containing only 0 or 1")
    return decval

                
def _eval_comb(output_dir,combIDX, combinations, Ncombos, Nfields, all_nuisance,dm0, mask_gm, mask_gm_filt, unfiltered_func_vol, filtered_func_vol, verbose=False, logger=None, queue=None):
    """Evaluate the temporal SNR and required effect size for a given nuisance regressor combination
    
    Parameters
    ----------
    combIDX : int
    combinations : list
    Ncombos : int
    Nfields : int
    all_nuisance : dict
    dm0 : ndarray
    mask_gm : ndarray
    mask_gm_filt : ndarray
    unfiltered_func_vol : ndarray
        unfiltered 4D fMRI timeseries
    filtered_func_vol : ndarray
        spatial & temporal filtered 4D fMRI timeseries
    fexists : bool
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    RE : ndarray
    mean_tSNR_tmp : float
    Nnuis : int
    mean_tSNR2_tmp : float
    
    Notes
    -----
    This is a private function only meant to be called by cmind_fMRI_outliers
    
    """
    import os
    
    import numpy as np
    
    from cmind.utils.utils import calc_tSNR_regress
    from cmind.utils.cmind_utils import read_fsl_design
    from cmind.utils.logging_utils import log_cmd

    print_str='Testing combination %d of %d' % (combIDX+1,Ncombos)
    print(print_str)
#    if verbose:
#        print_str='Testing combination %d of %d' % (combIDX+1,Ncombos)
#        if logger:
#            logger.info(print_str)
#        else:
            
    #idx_nuis=strfind(combinations(combIDX,:),'1');
    #Nnuis=length(idx_nuis);

    cnuis=combinations[combIDX]
    
    nuis_mat=_nuis_mat_from_combo(cnuis,all_nuisance,Nfields);

    nuisfile=os.path.join(output_dir,'tmp_nuis%04d.txt' % combIDX)
    design_fsf_file=os.path.join(output_dir,'design.fsf')
    design_file_copy=os.path.join(output_dir,'design%04d' % combIDX)
    
    np.savetxt(nuisfile,nuis_mat,delimiter=' ',fmt='%0.7g');

    Nnuis=nuis_mat.shape[1]
    log_cmd('cp "%s" "%s.fsf"' % (design_fsf_file, design_file_copy), verbose=verbose, logger=logger);
    if Nnuis>0:
        log_cmd('$FSLDIR/bin/feat_model "%s" "%s"' % (design_file_copy, nuisfile), verbose=verbose, logger=logger);
    else:
        log_cmd('$FSLDIR/bin/feat_model "%s"' % design_file_copy, verbose=verbose, logger=logger);
    
    """   
    feat_model will compute an estimated required effect size.  This will take 
    into account the conditioning of the design matrix as well as the degrees of
    freedom.  Assumed values for the noise and AR(1) values are read in from the
    design.fsf file.  In this script we do not update the noise and AR(1) directly
    in the .fsf, but instead use the calculated temporal SNR in GM after regressing
    out the design as an estimate of the noise level in the data.  This gets used
    in the metric_1 (on unfiltered data) and metric_2 (on filtered data).  In
    evaluating the different sets of regressors. We are currently ignoring any 
    potential change in AR(1) as a result of the different sets of nuisance regressors.
    """
    
    RE_file=os.path.join(output_dir,'RequiredEffects%04d.txt' % combIDX)
    log_cmd('cat "%s.con" | grep "RequiredEffect" | cut -f 1-2 --complement > "%s"' % (design_file_copy, RE_file), verbose=verbose, logger=logger);
    RE=np.loadtxt(RE_file); 
    #all_RE[combIDX,:]=RE

    if unfiltered_func_vol!=None: #vo
        if Nnuis>0:
            (tSNR_tmp, mask_tmp, mean_tSNR_tmp)=calc_tSNR_regress(unfiltered_func_vol,np.concatenate((dm0, nuis_mat),axis=1),mask_gm)[0:3]
        else:
            (tSNR_tmp, mask_tmp, mean_tSNR_tmp)=calc_tSNR_regress(unfiltered_func_vol,dm0,mask_gm)[0:3]
    else:
        tSNR_tmp==np.nan
        mean_tSNR_tmp=np.nan
        
    #all_tSNR(combIDX)=mean_tSNR_tmp;
    #all_tSNR_gm[combIDX]=mean_tSNR_tmp; #np.mean(tSNR_tmp(mask_gm));   #TODO
    #all_Nnuis[combIDX]=Nnuis  #TODO

    #dm_filt = np.loadtxt('./design.mat','\t',5,0); dm_filt = dm_filt(:,1:end-1);  %nuis_mat already built into this one
    
    dm_filt=read_fsl_design('%s.mat' % design_file_copy);
    dm_filt=np.concatenate((np.ones((dm_filt.shape[0],1)), dm_filt),axis=1);

    if filtered_func_vol!=None:
        #[tSNR2_tmp, mask_tmp,mean_tSNR2_tmp]=calc_tSNR_regress(filtered_func_vol,dm_filt,mask_filt);
        (tSNR2_tmp, mask_tmp,mean_tSNR2_tmp)=calc_tSNR_regress(filtered_func_vol,dm_filt,mask_gm_filt)[0:3];
        #all_tSNR2(combIDX)=mean_tSNR2_tmp;
        #all_tSNR2_gm[combIDX]=mean_tSNR2_tmp; #np.mean(tSNR2_tmp(mask_gm_filt));
        if verbose:
            print_str="combo %d:  mean_tSNR_tmp, mean_tSNR2_tmp = %s, %s" % (combIDX+1, mean_tSNR_tmp, mean_tSNR2_tmp)
            print(print_str)
            #if logger:
            #    logger.info(print_str)
            #else:
            #    print print_str
    else:
        mean_tSNR2_tmp=np.nan
        
    if queue:
        queue.put((combIDX,RE, mean_tSNR_tmp, Nnuis, mean_tSNR2_tmp))    
    else:
        return (RE, mean_tSNR_tmp, Nnuis, mean_tSNR2_tmp) 
            
            
def _nuis_mat_from_combo(cnuis,all_nuisance,Nfields):
    """Build a nuisance matrix corresponding to the boolean string, cnuis
    
    Parameters
    ----------
    cnuis : str
        boolean string specifying which nuisance parameters to include
    all_nuisance : dict
        dictionary containing the various types of nuisance parameter timecourses
    Nfields : int
        

    Returns
    -------
    nuis_mat : ndarray
        nuisance regressor matrix
    
    Notes
    ----------
    This is a private function only meant to be called by cmind_fMRI_outliers
    
    """
    import numpy as np
    
    use_csf=int(cnuis[0])==1;
    use_wm=int(cnuis[1])==1;
    #use_motion=bin2dec(cnuis(3:4))
    use_motion=int(cnuis[2])==1;
    use_motion_sq=int(cnuis[3])==1;
    use_motion_diff=int(cnuis[4])==1;
    use_motion_diff_sq=int(cnuis[5])==1;
    if Nfields>6:
        #use_FSLoutliers=binstr2dec(cnuis[6]);
        use_FSLoutliers=int(cnuis[6])==1;
    else:
        use_FSLoutliers=False
    if len(cnuis)>Nfields: #hack to also consider PCA nuisance regressor cases
        use_PCA75=int(cnuis[Nfields])
        use_PCA90=int(cnuis[Nfields+1])
        use_PCA95=int(cnuis[Nfields+2])
        use_PCA99=int(cnuis[Nfields+3])

    nuis_mat=np.asarray([[],]);
    if use_PCA75:
        nuis_mat = all_nuisance['PCA75']
    elif use_PCA90:
        nuis_mat = all_nuisance['PCA90']
    elif use_PCA95:
        nuis_mat = all_nuisance['PCA95']
    elif use_PCA99:
        nuis_mat = all_nuisance['PCA99']
    else:
        if use_csf:
            if np.any(nuis_mat):
                nuis_mat = np.concatenate((nuis_mat, all_nuisance['csf']),axis=1);
            else:
                nuis_mat = all_nuisance['csf']
        if use_wm:
            if np.any(nuis_mat):
                nuis_mat=np.concatenate((nuis_mat, all_nuisance['wm']),axis=1);
            else:
                nuis_mat = all_nuisance['wm']
        if use_motion:
            if np.any(nuis_mat):
                nuis_mat=np.concatenate((nuis_mat, all_nuisance['motion24'][:,0:6]),axis=1);
            else:
                nuis_mat = all_nuisance['motion24'][:,0:6]
        if use_motion_sq:
            if np.any(nuis_mat):
                nuis_mat=np.concatenate((nuis_mat, all_nuisance['motion24'][:,6:12]),axis=1);
            else:
                nuis_mat = all_nuisance['motion24'][:,6:12]
        if use_motion_diff:
            if np.any(nuis_mat):
                nuis_mat=np.concatenate((nuis_mat, all_nuisance['motion24'][:,12:18]),axis=1);
            else:
                nuis_mat = all_nuisance['motion24'][:,12:18]
        if use_motion_diff_sq:
            if np.any(nuis_mat):
                nuis_mat=np.concatenate((nuis_mat, all_nuisance['motion24'][:,18:24]),axis=1);
            else:
                nuis_mat = all_nuisance['motion24'][:,18:24]
        if use_FSLoutliers:
            if np.any(nuis_mat):
                nuis_mat=np.concatenate((nuis_mat, all_nuisance['outlier_confounds_fsl']),axis=1);
            else:
                nuis_mat = all_nuisance['outlier_confounds_fsl']
    return nuis_mat

                                
def cmind_fMRI_outliers(output_dir,contrast_of_interest, regBBR_ref,WM_pve_vol,GM_pve_vol,CSF_pve_vol, vol_file_niigz, filtered_func_data, motion_parfile, unfiltered_design_matrix,FSF_template, noise_est_file, HRtoLR_Affine=None, HRtoLR_Warp=None, Nproc_max=1, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """Detect outlier timepoints in fMRI timeseries and generate nuisance 
    regressors
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    contrast_of_interest : int or list of int
        which contrast in FSF_template is of primary interest?  Note: Unlike in 
        the FSL GUI, contrast numbering begins from 0!.  If this is a list the
        minimum effect size among the contrasts in the list will be used during
        the computation
    regBBR_ref : str
        filename of the regBBR reference image
    WM_pve_vol : str
        WM partial volume estimate from structural processing
    GM_pve_vol : str
        GM partial volume estimate from structural processing
    CSF_pve_vol : str
        CSF partial volume estimate from structural processing
    vol_file_niigz : str
        unfiltered 4D fMRI timeseries to process
    filtered_func_data : str
        smoothed and temporal filtered 4D fMRI timeseries file to process
    motion_parfile : str
        The 6 column motion parameter (.par) file from mcflirt
    unfiltered_design_matrix : str
        The (unfiltered) Feat design matrix corresponding to `vol_file_niigz`
    FSF_template : str
        THE .fsf template corresponding to `vol_file_niigz`
    noise_est_file : str
        The noise_est.txt file from cmind_fMRI_preprocess2.py
    HRtoLR_Affine : str, optional
        filename for the affine transform from functional->structural space
    HRtoLR_Warp : str, optional
        fieldmap corrected warp from functional->structural space
    Nproc_max : int, optional
        maximum number of processes to launch simultaneously
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool, optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    
    Returns
    -------
    default_confound_file : str
        confound file with 6 motion + outliers
    best_confound_file : str
        optimal confound file
    worst_confound_file : str
        "worst" confound file
    mcf06_confound_file : str
        6 motion parameters confound file
    FSL_outliers_txt : str
        outliers confound file
        
    Notes
    -----
    For the "optimal" nuisance regressor evaluation the following regressor sets are considered:
        
            1. mean CSF regressor  (orthogonalized w.r.t. ASL/BOLD baseline)
            2. mean WM regressor   (orthogonalized w.r.t. ASL/BOLD baseline)
            3. 6 rigid body motion parameters
            4. squares of the 6 motion parameters
            5. derivatives of the 6 motion parameters
            6. squares of the derivatives of the 6 motion parameters
            7. outlier regressors
    
    PCA is also applied to the combined set of regressors and thresholds for 75, 90, 95 and 99 percent
    of explained variance are applied.  These four PCA regressor sets are evaluated in addition to the 2**7=128
    possible combinations of the 7 regressor sets mentioned above.
       
    """
    
    import os
    import glob
    import warnings
    import pickle as pickle
    
    import numpy as np
    import nibabel as nib
    from matplotlib.mlab import PCA
    
    from cmind.globals import cmind_outliers_cmd
    from cmind.utils.utils import input2bool, calc_tSNR_regress
    from cmind.utils.fsf_utils import fsf_substitute
    from cmind.utils.file_utils import imexist
    from cmind.utils.nifti_utils import cmind_save_nii_mod_header
    from cmind.utils.cmind_utils import read_fsl_design
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    from cmind.pipeline.cmind_fMRI_outliers import _eval_comb, _nuis_mat_from_combo, orthogonalize
    
    if filtered_func_data == "None":
        print("default_confound_file:None")
        print("best_confound_file:None")
        print("worst_confound_file:None")
        print("mcf06_confound_file:None")
        print("FSL_outliers_txt:None")
        return 

    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
            
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    if HRtoLR_Affine and (not os.path.exists(HRtoLR_Affine)):
        raise IOError("Missing HRtoLR_Affine, %s" % HRtoLR_Affine)
    if HRtoLR_Warp and (not os.path.exists(HRtoLR_Warp)):
        raise IOError("Missing HRtoLR_Warp, %s" % HRtoLR_Warp)
    if not imexist(regBBR_ref,'nifti-1')[0]:
        raise IOError("Missing regBBR_ref, %s" % regBBR_ref)
    if not imexist(WM_pve_vol,'nifti-1')[0]:
        raise IOError("Missing WM_pve_vol, %s" % WM_pve_vol)
    if not imexist(GM_pve_vol,'nifti-1')[0]:
        raise IOError("Missing GM_pve_vol, %s" % GM_pve_vol)
    if not imexist(CSF_pve_vol,'nifti-1')[0]:
        raise IOError("Missing CSF_pve_vol, %s" % CSF_pve_vol)
    if not imexist(vol_file_niigz,'nifti-1')[0]:
        raise IOError("vol_file_niigz, %s, not found " % (vol_file_niigz))
    if not os.path.exists(FSF_template):
        raise IOError("FSF_template, %s, not found " % (FSF_template))
    if not os.path.exists(unfiltered_design_matrix):
        raise IOError("unfiltered_design_matrix, %s, not found " % (unfiltered_design_matrix))
    if motion_parfile and (not os.path.exists(motion_parfile)):
        raise IOError("motion_parfile, %s, not found " % (motion_parfile))
        
    outlier_params={}
    outlier_params['flags']={}
    outlier_params['thresholds']={}
    outlier_params['flags']['usenorm'] = 0;
    outlier_params['thresholds']['translation'] = 3; #mm
    outlier_params['thresholds']['rotation'] = 2;  #degrees
    outlier_params['thresholds']['intensity'] = 2;  #z (standard deviations)
    outlier_params['thresholds']['relative_mot'] = 0.5; #mm

    #use all absolute filenames so that os.chdir() will not be needed
    nuisance_output_file=os.path.join(output_dir,'all_nuisance.pickle');
    highres_csfseg=os.path.join(output_dir,'highres_csfseg.nii.gz')
    highres_wmseg=os.path.join(output_dir,'highres_wmseg.nii.gz')
    highres_gmseg=os.path.join(output_dir,'highres_gmseg.nii.gz')
    csf_mask=os.path.join(output_dir,'csf_mask.nii.gz')
    gm_mask=os.path.join(output_dir,'gm_mask.nii.gz')
    wm_mask=os.path.join(output_dir,'wm_mask.nii.gz')
    CSF_timeseries=os.path.join(output_dir,'CSF_timeseries.nii.gz')
    WM_timeseries=os.path.join(output_dir,'WM_timeseries.nii.gz')
    csf_timeseries_txt=os.path.join(output_dir,'csf_timeseries.txt')
    wm_timeseries_txt=os.path.join(output_dir,'wm_timeseries.txt')
    mask_file=os.path.join(output_dir,'mask.nii.gz')
    mcf_diff = os.path.join(output_dir,'mcf_diff')
    mcf_final = os.path.join(output_dir,'mcf_final.par')
    outlier_list_txt = os.path.join(output_dir,'outlier_list.txt')
    outlier_confounds_txt = os.path.join(output_dir,'outlier_confounds.txt')
    Noutliers_txt = os.path.join(output_dir,'Noutliers.txt')
    Noutliers_FSL_txt=os.path.join(output_dir,'Noutliers_FSL.txt')
    FSL_outliers_txt=os.path.join(output_dir,'FSL_outliers.txt')
    FSL_metrics_txt=os.path.join(output_dir,'FSL_metrics.txt')
    FSL_metrics_png=os.path.join(output_dir,'FSL_metrics.png')
    best_design_cov_png = os.path.join(output_dir,'best_design_cov.png') 

    all_nuisance={} #dictionary to store various potential nuisance parameters
    if (not os.path.exists(nuisance_output_file)) or ForceUpdate:
        
        #create strict 90% PVE CSF and WM masks
        log_cmd('$FSLDIR/bin/fslmaths "%s" -thr 0.9 -bin "%s"' % (CSF_pve_vol,highres_csfseg), verbose=verbose, logger=module_logger);
        log_cmd('$FSLDIR/bin/fslmaths "%s" -thr 0.9 -bin "%s"' % (WM_pve_vol, highres_wmseg ), verbose=verbose, logger=module_logger);
        
        #create more lenient 66% PVE GM mask
        log_cmd('$FSLDIR/bin/fslmaths "%s" -thr 0.66 -bin "%s"' % (GM_pve_vol, highres_gmseg), verbose=verbose, logger=module_logger);

        #transform the GM, CSF & WM masks from highres to lowres space
        
        #transform the masks from anatomical to subject space
        
        def _fsl_applywarp(r,i,o,w,interp_str):
            log_cmd('$FSLDIR/bin/applywarp -r "{}" -i "{}" -o "{}" -w {} --interp={}'.format(r,i,o,w,interp_str), verbose=verbose, logger=module_logger)
            
        def _fsl_applyaffine(r,i,o,a,interp_str):
            log_cmd('$FSLDIR/bin/flirt -ref "{}" -in "{}" -out "{}" -applyxfm -init "{}" -interp {}'.format(r,i,o,a,interp_str), verbose=verbose, logger=module_logger);
            
        if HRtoLR_Warp:
            _fsl_applywarp(regBBR_ref,highres_csfseg,csf_mask,HRtoLR_Warp,'nn')
            _fsl_applywarp(regBBR_ref,highres_gmseg,gm_mask,HRtoLR_Warp,'nn')
            _fsl_applywarp(regBBR_ref,highres_wmseg,wm_mask,HRtoLR_Warp,'nn')
        elif HRtoLR_Affine:
            _fsl_applyaffine(regBBR_ref,highres_csfseg,csf_mask,HRtoLR_Affine,'nearestneighbour')
            _fsl_applyaffine(regBBR_ref,highres_gmseg,gm_mask,HRtoLR_Affine,'nearestneighbour')
            _fsl_applyaffine(regBBR_ref,highres_wmseg,wm_mask,HRtoLR_Affine,'nearestneighbour')
        else:
            raise IOError("Either highres->lowres affine or warp must be supplied")
    
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mas "%s" "%s"' % (vol_file_niigz, csf_mask, CSF_timeseries), verbose=verbose, logger=module_logger);
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mas "%s" "%s"' % (vol_file_niigz, wm_mask, WM_timeseries), verbose=verbose, logger=module_logger);
        
        #extract average WM and CSF timeseries
        log_cmd('$FSLDIR/bin/fslstats -t "%s" -M > "%s"' % (CSF_timeseries, csf_timeseries_txt), verbose=verbose, logger=module_logger);
        log_cmd('$FSLDIR/bin/fslstats -t "%s" -M > "%s"' % (WM_timeseries, wm_timeseries_txt), verbose=verbose, logger=module_logger);
        
        csf=np.loadtxt(csf_timeseries_txt).reshape(-1,1) #force this to be a 2D array
        csf=csf-np.mean(csf); 
        csf = csf/np.max(csf)/2.0;
        wm=np.loadtxt(wm_timeseries_txt).reshape(-1,1)  #force this to be a 2D array
        wm=wm-np.mean(wm); 
        wm = wm/np.max(wm)/2.0;
        
        #regress any tag/control alternation out of the mean CSF or WM signal before storing it as a nuisance parameter
        #this will preserve the ASL baseline contrast if these are included as confounds in the design
        #For CSF, partial volume with grey matter and/or imperfect segmentation results in residual tag/control alternation
        #This is also true for WM, but WM also has some native baselineCBF signal we don't want to remove!
        tag_control_reg=np.ones_like(csf);  tag_control_reg[1::2]=-1
        csf=orthogonalize(csf,tag_control_reg).reshape(-1,1) #force this to be a 2D array
        wm=orthogonalize(wm,tag_control_reg).reshape(-1,1) #force this to be a 2D array
        
        all_nuisance['csf']=csf;
        all_nuisance['wm']=wm;

        unfiltered_func_vol_nii = nib.load(vol_file_niigz)
        unfiltered_func_vol=np.float32(unfiltered_func_vol_nii.get_data())
        nvols = unfiltered_func_vol.shape[-1]

        mask_gm = nib.load(gm_mask);  
        mask_gm=(mask_gm.get_data())>0;
        
        if motion_parfile:
            #dm_unfilt = np.loadtxt(unfiltered_design_matrix,delimiter='\t',skiprows=5,usecols=(0,1,2,3)); 
            #The FSL design matrix has a tab at the end of each line that was causing a problem from np.loadtxt
            #np.genfromtxt sets a final column on nan in this case.  Can then remove the nan column afterwards
            dm_unfilt=read_fsl_design(unfiltered_design_matrix);
        else:
            dm_unfilt=np.asarray([]);


        mean_unfiltered_func_vol=np.mean(np.abs(unfiltered_func_vol),axis=-1);  
        mask=mean_unfiltered_func_vol > 0.025*np.max(mean_unfiltered_func_vol);
        cmind_save_nii_mod_header(unfiltered_func_vol_nii,mask,mask_file,4);
        log_cmd('$FSLDIR/bin/fslmaths "%s" -bin "%s"' % (mask_file, mask_file), verbose=verbose, logger=module_logger)
        
        #add linear and constant trends to design matrix
        npts_dm0=dm_unfilt.shape[0]
        dm0=np.concatenate((np.ones((npts_dm0,1)),np.linspace(-0.5,0.5,npts_dm0).reshape((npts_dm0,1)) , dm_unfilt),axis=1)
        
        #add csf and wm regressors
        #dm = np.concatenate((dm0,csf,wm),axis=1)
        
        if motion_parfile:
            mcf=np.loadtxt(motion_parfile);
            for n in range(0,6): #demean motion regressors
                mcf[:,n]=mcf[:,n]-np.mean(mcf[:,n])
                mcf[:,n] = mcf[:,n]/np.max(mcf[:,n])/2.0
            #dm2 = np.concatenate((dm0, mcf),axis=1);
            #mask0=np.asarray([]);
            all_nuisance['motion6']=mcf;
        #else: #TODO: remove this old, special case code
        #    dm2 = dm0;
        #    nii=nib.load('../ASL_Stories_SE_mcf_mean_N4_brain_mask.nii.gz'); 
        #    mask0=nii.get_data()>0

        nii_type=16

        if motion_parfile:
            log_cmd('$FSLDIR/bin/mp_diffpow.sh "%s" "%s"' % (motion_parfile, mcf_diff), verbose=verbose, logger=module_logger);
            log_cmd('paste -d " " "%s" "%s.dat"  > "%s"' % (motion_parfile, mcf_diff, mcf_final), verbose=verbose, logger=module_logger);
            mcf24 = np.loadtxt(mcf_final);
            for n in range(0,24): #demean motion regressors
                mcf24[:,n]=mcf24[:,n]-np.mean(mcf24[:,n]); 
                mcf24[:,n] = mcf24[:,n]/np.max(mcf24[:,n])/2.0;
            #dm3 = np.concatenate((dm0, mcf24),axis=1);
            all_nuisance['motion24']=mcf24;
            
            resultsPCA=PCA(mcf24);
            #determine number of components needed to explain 90% of the variance
            Nkeep90=np.max(np.where((np.cumsum(resultsPCA.fracs)<0.90))[0])
            #Nkeep75=np.max(np.where((np.cumsum(resultsPCA.fracs)<0.75))[0])
            all_nuisance['motion24_PCA']=resultsPCA.Y
            all_nuisance['motion24_PCA90']=resultsPCA.Y[:,0:Nkeep90];



        x=np.arange(0,unfiltered_func_vol.shape[-1])
        calc_custom_outliers=False
        if calc_custom_outliers:
            if motion_parfile:
                mcf=np.loadtxt(motion_parfile);
                traval = mcf[:,3:6];       # translation parameters (mm)
                rotval = mcf[:,0:3]*180/np.pi;  # rotation parameters (deg)

                if 'relative_mot' in outlier_params['treshold']:
                    mcf_rel = np.loadtxt(motion_parfile.replace('.par','_rel.rms'));
                    rel_idx = np.where(mcf_rel>outlier_params['thresholds']['relative_mot'])[0];
                else:
                    rel_idx = []

                if outlier_params['flags']['usenorm'] == 0:
                    tidx = np.where(np.sum(np.abs(traval)> outlier_params['thresholds']['translation'],axis=1)>0)[0];
                    ridx = np.where(np.sum(np.abs(rotval)> outlier_params['thresholds']['rotation'],axis=1)>0)[0];
                else:
                    traval = np.sqrt(np.sum(traval*traval,axis=1));
                    rotval = np.sqrt(np.sum(rotval*rotval,axis=1));
                    tidx = np.where(traval>outlier_params['thresholds']['translation'])[0];
                    ridx = np.where(rotval>outlier_params['thresholds']['rotation'])[0];

                if True: #this is a near replica of what FSL does in fsl_motion_outliers2 when refrms is selected (RMS error from mean volume)
                    unfiltered_func_vol_tmean=np.mean(unfiltered_func_vol,axis=3); 
                    unfiltered_func_vol_dm=np.zeros(unfiltered_func_vol.shape);
                    for dd in range(unfiltered_func_vol.shape[3]):
                        unfiltered_func_vol_dm[:,:,:,dd]=mask*(unfiltered_func_vol[:,:,:,dd]-unfiltered_func_vol_tmean)
                    brainmed=np.percentile(unfiltered_func_vol_tmean(mask),q=(50));
                    unfiltered_func_vol_dm=unfiltered_func_vol_dm/brainmed;
                    res_mse=np.sqrt(np.squeeze(np.mean(np.mean(np.mean(unfiltered_func_vol_dm**2,axis=2),axis=1),axis=0))/np.mean(mask))
                    np.concatenate((res_mse[1::].reshape(-1,1),np.asarray(res_mse[0]).reshape(-1,1)),axis=0)
                    res_mse_diff=np.abs(np.roll(res_mse,-1)-res_mse) #Diff to remove slow trends
                    (p25, p75)=np.percentile(res_mse_diff,q=(25, 75))
                    #threshv=p75+1.5*(p75-p25);
                    threshv=p75+outlier_params['thresholds']['intensity']*(p75-p25);
                    bad_idx=np.where(res_mse_diff>threshv)[0]

                    iidx=bad_idx
                else:  #fluctuations in mean intensity
                    g=np.mean(unfiltered_func_vol[mask],axis=0); 
                    g=g.flatten();  
                    g=g-np.polyval(np.polyfit(x,g,2),x);  
                    gz = (g-np.mean(g))/np.std(g,ddof=1);
                    iidx = np.where(np.abs(gz)>outlier_params['thresholds']['intensity'])[0]
                
                outlier_list = np.unique(np.union1d(np.union1d(np.union1d(iidx,tidx),ridx),rel_idx))
                np.savetxt(outlier_list_txt,outlier_list,fmt='%d');
                rtmp=np.zeros((x.shape[0],1)); 
                rtmp[ridx]=1;
                ttmp=np.zeros((x.shape[0],1)); 
                ttmp[tidx]=1;
                itmp=np.zeros((x.shape[0],1)); 
                itmp[iidx]=1;

                if generate_figures:
                    import matplotlib as mpl
                    mpl.use('agg') #don't plot to screen
                    import matplotlib.pylab as pylab
                    import matplotlib.pyplot as plt
        
                    #h=figure('visible','off');
                    plt.stem(x,rtmp,'bo')
                    plt.hold('on')
                    plt.stem(x,0.7*ttmp,'gv')
                    plt.stem(x,0.6*itmp,'rs')
                    rot_str = 'Rotation Outliers (thr=%0.2f deg)' % (outlier_params['thresholds']['rotation'])
                    tran_str = 'Translation Outliers (thr=%0.2f mm)' % (outlier_params['thresholds']['translation'])
                    int_str = 'Intensity Outliers (thr=%0.2f z)' % (outlier_params['thresholds']['intensity'])

                    if 'relative_mot' in outlier_params['thresholds']:
                        rel_tmp=np.zeros((x.shape[0],1)); 
                        rel_tmp[rel_idx]=1;
                        plt.stem(x,0.5*rel_tmp,'cx')
                        rel_mot_str = 'Relative Motion Outliers (thr=%0.2f mm)' % (outlier_params['thresholds']['relative_mot'])
                        plt.legend((rot_str, tran_str, int_str , rel_mot_str))
                    else:
                        plt.legend((rot_str, tran_str, int_str))

                    plt.title('Outlier Summary (%d outliers)' % (outlier_list.shape[0])) #,'FontWeight','bold','FontSize',16)
                    plt.xlabel('Timepoint')
                    outlier_summary_img=os.path.join(output_dir,'outlier_summary.png')
                    figfile=open(outlier_summary_img,'wb')
                    pylab.savefig(figfile,format='png')
                    figfile.close()
                    
                keep_list = np.ones((nvols,1)); 
                keep_list[outlier_list]=0; 
                keep_list = keep_list>0;

                #Also do fsl_motion_outliers for comparison
                if (outlier_list.shape[0]==0):
                    olist=np.asarray([]);
                else:
                    olist = np.loadtxt('outlier_list.txt');

            else:
                olist=np.asarray([]);
                np.savetxt(Noutliers_txt,[0,],fmt='%d');

            
            if not nvols:
                nvols=log_cmd('$FSLDIR/bin/fslnvols %s' % (vol_file_niigz), verbose=verbose, logger=module_logger);
                nvols=int(nvols)
            outlier_confounds=np.zeros((nvols,olist.shape[0]))
            for n in range(olist.shape[0]):
                outlier_confounds[olist[n],n]=1;
            tmp_txt=os.path.join(output_dir,'tmp.txt')
            np.savetxt(tmp_txt,outlier_confounds,delimiter=' ',fmt='%d');
            log_cmd("cat %s | sed 's/ /   /g' > %s" % (tmp_txt, outlier_confounds_txt), verbose=verbose, logger=module_logger);
            os.remove(tmp_txt);
            
            #Noutliers=np.loadtxt(Noutliers_txt)
            all_nuisance['outlier_confounds']=outlier_confounds;
        
        #log_cmd('$FSLDIR/bin/fsl_motion_outliers -i %s -o FSL_outliers.txt -s FSL_metrics.txt -p FSL_metrics.png --nomoco -v' % (vol_file_niigz), verbose=verbose, logger=module_logger)
        log_cmd('%s -i "%s" -o "%s" -s "%s" -p "%s" --mean_ref --nomoco -v' % (cmind_outliers_cmd, vol_file_niigz,FSL_outliers_txt, FSL_metrics_txt, FSL_metrics_png ), verbose=verbose, logger=module_logger)

        olist_FSL=[]
        if not os.path.exists(FSL_outliers_txt):
            fsl_nuisance=np.asarray([])
            olist_FSL=[]
            np.savetxt(Noutliers_FSL_txt,[0,],fmt='%d');
        else:
            fsl_nuisance = np.loadtxt(FSL_outliers_txt);
            if fsl_nuisance.ndim==1:
                fsl_nuisance=fsl_nuisance.reshape(-1,1) #force to 2D
            np.savetxt(Noutliers_FSL_txt,[fsl_nuisance.shape[1],],fmt='%d');
            for nn in range(fsl_nuisance.shape[1]):
                olist_FSL.append(np.where(fsl_nuisance[:,nn])[0][0])

        all_nuisance['outlier_confounds_fsl']=fsl_nuisance;

        with open(nuisance_output_file, "wb") as f:
            pickle.dump(all_nuisance,f)
    else:
        
        if (not os.path.exists(best_design_cov_png)) or ForceUpdate:
            all_nuisance = pickle.load(open( nuisance_output_file, "rb" ))
            mask_gm = nib.load(gm_mask)
            mask_gm=mask_gm.get_data()>0
            mask = nib.load(mask_file)
            mask = mask.get_data()>0
            unfiltered_func_vol_nii = nib.load(vol_file_niigz)
            unfiltered_func_vol=np.float32(unfiltered_func_vol_nii.get_data())
            nvols = unfiltered_func_vol.shape[3]
            if motion_parfile:
                dm_unfilt=read_fsl_design(unfiltered_design_matrix);
            else:
                dm_unfilt=np.asarray([]);

            #remove linear and constant trends
            npts_dm0=dm_unfilt.shape[0]
            dm0=np.concatenate((np.ones((npts_dm0,1)),np.linspace(-0.5,0.5,npts_dm0).reshape((npts_dm0,1)) , dm_unfilt),axis=1)
    
        #load all_nuisance all_nuisance  #TODO
    
    mcf06_confound_file=os.path.join(output_dir,'mcf06_confounds.txt')
    default_confound_file=os.path.join(output_dir,'default_confounds.txt')
    best_confound_file=os.path.join(output_dir,'best_confounds.txt')
    worst_confound_file=os.path.join(output_dir,'worst_confounds.txt')
    PCA95_confound_file=os.path.join(output_dir,'PCA95_confounds.txt')
    PCA99_confound_file=os.path.join(output_dir,'PCA99_confounds.txt')
    
    effect_size_file = os.path.join(output_dir,'effect_size_summaries.pickle')
    all_ratio_file=os.path.join(output_dir,'all_ratios.txt')
    
    design_fsf_file=os.path.join(output_dir,'design.fsf')
    noise_est_copy=os.path.join(output_dir,'noise_est.txt')
    if (not os.path.exists(best_design_cov_png)) or ForceUpdate:
        
        np.savetxt(mcf06_confound_file,all_nuisance['motion6'],delimiter=' ',fmt='%0.7g');
    
        #FSF_template = '/media/Data2/CMIND/ASLBOLD_des/BOLD_template_v2.fsf'
        log_cmd('cp "%s" "%s"' % (FSF_template,design_fsf_file), verbose=verbose, logger=module_logger)
        try:
            log_cmd('cp "%s" "%s"' % (noise_est_file, noise_est_copy), verbose=verbose, logger=module_logger)
            #cmind_update_FSF('noise_stats',design_fsf_dir=output_dir,noise_est_dir=output_dir)
            noise_val=np.genfromtxt(noise_est_copy)
            noise_dict={}
            noise_dict['noise']=noise_val[0]
            noise_dict['noisear']=noise_val[1]
            fsf_substitute(design_fsf_file,noise_dict)  #update the design.fsf with these noise parameters
        except:
            warnings.warn("expected noise_est.txt, {}, file did not exist!  Cannot update noise_stats in FSF file".format(noise_est))

        fexists, unfiltered_func_vol_nii2 = imexist(filtered_func_data,'nifti-1')[0:2]
        if not fexists:
            raise IOError("filtered_func_data file, %s, not found!" % filtered_func_data)
        
        if fexists:
            filtered_func_vol_nii=nib.load(unfiltered_func_vol_nii2);
            filtered_func_vol=np.float32(filtered_func_vol_nii.get_data());
            mask_filt=(mask*(np.mean(filtered_func_vol,axis=-1)!=0))>0;
            mask_gm_filt=(mask_gm*(np.mean(filtered_func_vol,axis=-1)!=0))>0;
        else:
            filtered_func_vol=None

        #nuisance_fields_tmp=fields(all_nuisance);
        
        #if there are no outlier timepoints, then we don't need to test including them
        if not np.any(all_nuisance['outlier_confounds_fsl']):
            Nfields=6
            allnuis_mat=np.concatenate((all_nuisance['csf'],all_nuisance['wm'],all_nuisance['motion24']),axis=1)
        else:
            Nfields=7  #1=csf, 2=wm, 3=motion1-6, 4=motion7-12, 5=motion13-18, 6=motion19-24, 7=outliersFSL
            allnuis_mat=np.concatenate((all_nuisance['csf'],all_nuisance['wm'],all_nuisance['motion24'],all_nuisance['outlier_confounds_fsl']),axis=1)
            
        #PCA decomposition-based choice of nuisance regressors
        #TODO: move outliers outside of this PCA-based computation?
        resultsPCA=PCA(allnuis_mat);
        Nkeep99=np.max(np.where((np.cumsum(resultsPCA.fracs)<0.99))[0])
        Nkeep95=np.max(np.where((np.cumsum(resultsPCA.fracs)<0.95))[0])
        Nkeep90=np.max(np.where((np.cumsum(resultsPCA.fracs)<0.90))[0])
        Nkeep75=np.max(np.where((np.cumsum(resultsPCA.fracs)<0.75))[0])
        np.savetxt(os.path.join(output_dir,'Nkeep75.txt'),[Nkeep75,],fmt='%d');
        np.savetxt(os.path.join(output_dir,'Nkeep90.txt'),[Nkeep90,],fmt='%d');
        np.savetxt(os.path.join(output_dir,'Nkeep95.txt'),[Nkeep95,],fmt='%d');
        np.savetxt(os.path.join(output_dir,'Nkeep99.txt'),[Nkeep99,],fmt='%d');
        all_nuisance['PCA99']=resultsPCA.Y[:,0:Nkeep99];
        all_nuisance['PCA95']=resultsPCA.Y[:,0:Nkeep95];
        all_nuisance['PCA90']=resultsPCA.Y[:,0:Nkeep90];
        all_nuisance['PCA75']=resultsPCA.Y[:,0:Nkeep75];
        NPCA_combos=4
        
        #combinations=dec2bin(0:2**Nfields-1);
        combinations=[np.binary_repr(item,Nfields)+'0'*NPCA_combos for item in np.arange(2**Nfields)]
        
        for pca_combo in range(NPCA_combos):
            append_str=np.binary_repr(2**pca_combo,NPCA_combos)
            combinations.append('0'*Nfields+append_str)
            
        #For 24 motion parameter case:
        # First 6 are the rigid motion estimates, 6-12 are the squares of these estimates, 13-18 are the derivatives of the motion estimates, 19-24 are the squares of the derivatives of the motion estimates
        #clear all_RE all_Nnuis all_tSNR* all_tSNR_gm*  
        
        Ncombos=len(combinations)
        
        #all_tSNR=np.zeros((Ncombos,1))
        #all_tSNR2=np.zeros((Ncombos,1))
        all_tSNR_gm=np.zeros((Ncombos,1))
        all_tSNR2_gm=np.zeros((Ncombos,1))
        all_Nnuis=np.zeros((Ncombos,1))

   #     if verbose: 
   #         from time import time
   #         starttime=time()
            
        Nproc_max=int(Nproc_max)
        
        if (Nproc_max>1) or (Nproc_max==-1):
            use_multiple_threads=True
        else:
            use_multiple_threads=False
            
        module_logger.info("Starting evaluation of nuisance regressor combinations")
        if use_multiple_threads==True:
            
            import multiprocessing
                    
            (RE, mean_tSNR_tmp, Nnuis, mean_tSNR2_tmp)=_eval_comb(output_dir, 0, combinations, Ncombos, Nfields, all_nuisance,dm0, mask_gm, mask_gm_filt, unfiltered_func_vol,filtered_func_vol, False, None)
            all_RE=np.zeros((Ncombos,np.prod(RE.shape)))
            all_RE[0,:]=RE
            all_tSNR_gm[0]=mean_tSNR_tmp; #np.mean(tSNR_tmp(mask_gm));   #TODO
            all_Nnuis[0]=Nnuis  #TODO
            if fexists:
                all_tSNR2_gm[0]=mean_tSNR2_tmp; #np.mean(tSNR2_tmp(mask_gm_filt));
            
            from multiprocessing.queues import Queue    
            import errno
            #retry to avoid EINTR interrupts as suggested here:    http://stackoverflow.com/questions/4952247/interrupted-system-call-with-processing-queue  
            def retry_on_eintr(function, *args, **kw):
                while True:
                    try:
                        return function(*args, **kw)
                    except IOError as e:            
                        if e.errno == errno.EINTR:
                            continue
                        else:
                            raise    
            
            class RetryQueue(Queue):  #KLUDGE to avoid mysterious EINTR interrup problems
                """Queue which will retry if interrupted with EINTR."""
                def get(self, block=True, timeout=None):
                    return retry_on_eintr(Queue.get, self, block, timeout)
                    
            result_queue = RetryQueue() #multiprocessing.Queue()
            
            def evaluate_chunk(chunk_range):
                procs=[]
                for combIDX in chunk_range: #range(1,Ncombos):
                    p = multiprocessing.Process(target=_eval_comb,args=(output_dir, combIDX, combinations, Ncombos, Nfields, all_nuisance,dm0, mask_gm, mask_gm_filt, unfiltered_func_vol,filtered_func_vol, False, None, result_queue))
                    procs.append(p)
                    p.daemon = True
                    p.start()
                for p in procs:
                    (p_idx,RE, mean_tSNR_tmp, Nnuis, mean_tSNR2_tmp)=result_queue.get()
                    all_RE[p_idx,:]=RE
                    all_tSNR_gm[p_idx]=mean_tSNR_tmp; #np.mean(tSNR_tmp(mask_gm));   #TODO
                    all_Nnuis[p_idx]=Nnuis  #TODO
                    if fexists:
                        all_tSNR2_gm[p_idx]=mean_tSNR2_tmp; #np.mean(tSNR2_tmp(mask_gm_filt));
                    p.join()
            
            if Nproc_max!=-1: #break into chunks
               
                #Nper=int(np.ceil((Ncombos-1)/Nproc_max))
                Nper=Nproc_max
                Nchunks=np.ceil(np.ceil((Ncombos-1)/float(Nper)))
                for chunk_idx in range(int(Nchunks)):
                    chunk_range = list(range(1+chunk_idx*Nper,min(((chunk_idx+1)*Nper)+1,Ncombos)))
                    evaluate_chunk(chunk_range)
                    
            else:  #allow all threads to be accessed simultaneously
                chunk_range=list(range(1,Ncombos))
                evaluate_chunk(chunk_range)

            all_tSNR_gm=all_tSNR_gm.squeeze()
            all_Nnuis=all_Nnuis.squeeze()
            all_RE=all_RE.squeeze()
            all_tSNR2_gm=all_tSNR2_gm.squeeze()


        #Main loop to test all requested combinations of nuisance regressors
        else:
            for combIDX in range(Ncombos):
                (RE, mean_tSNR_tmp, Nnuis, mean_tSNR2_tmp)=_eval_comb(output_dir, combIDX, combinations, Ncombos, Nfields, all_nuisance,dm0, mask_gm, mask_gm_filt, unfiltered_func_vol,filtered_func_vol, False, None)
                if combIDX==0: #preallocate all_RE based on size of RE
                    all_RE=np.zeros((Ncombos,np.prod(RE.shape)))
                all_RE[combIDX,:]=RE
                all_tSNR_gm[combIDX]=mean_tSNR_tmp; #np.mean(tSNR_tmp(mask_gm));   #TODO
                all_Nnuis[combIDX]=Nnuis  #TODO
                if fexists:
                    all_tSNR2_gm[combIDX]=mean_tSNR2_tmp; #np.mean(tSNR2_tmp(mask_gm_filt));
                                     
        if isinstance(contrast_of_interest,list):
            denom=np.min(all_RE[:,contrast_of_interest],axis=-1)
        else:
            denom=all_RE[:,contrast_of_interest]
            
        metric_1=all_tSNR_gm.ravel()/denom

        if fexists:
            metric_2=all_tSNR2_gm.ravel()/denom #all_RE[:,contrast_of_interest]
            metric_used=metric_2
        else:
            metric_used=metric_1
        si=np.argsort(metric_used)
        best_idx=si[-1];
        worst_idx=si[0];
        if Nfields==7:
            default_str='0010001'+'0'*NPCA_combos #6 motion + outliers
        elif Nfields==6:
            default_str='001000'+'0'*NPCA_combos #6 motion

        for kk in range(Ncombos):
            comb_str=combinations[kk];
            if (comb_str==default_str):
                default_idx=kk
                
        PCA99_idx=Ncombos-4
        PCA95_idx=Ncombos-3
        #PCA90_idx=Ncombos-2
        #PCA75_idx=Ncombos-1
        
        #make summary images for None, default, and best cases
        for combIDX in [0, default_idx, best_idx, PCA99_idx]:
            #idx_nuis=strfind(combinations(combIDX,:),'1');
            #%Nnuis=length(idx_nuis);

            cnuis=combinations[combIDX]
            
            nuis_mat=_nuis_mat_from_combo(cnuis,all_nuisance,Nfields);
            Nnuis=nuis_mat.shape[1]
            nuisfile=os.path.join(output_dir,'tmp_nuis%04d.txt' % combIDX)
            #np.savetxt(nuisfile,nuis_mat,'delimiter',' ');
            design_root=os.path.join(output_dir,'design')
            if Nnuis>0:
                log_cmd('$FSLDIR/bin/feat_model "%s" "%s"' % (design_root, nuisfile), verbose=verbose, logger=module_logger);
            else:
                log_cmd('$FSLDIR/bin/feat_model "%s"' % design_root, verbose=verbose, logger=module_logger);
            
            dm_filt=read_fsl_design(design_root + '.mat');
            dm_filt=np.concatenate((np.ones((dm_filt.shape[0],1)), dm_filt),axis=1);

            #local function to save the tSNR volume and make a summary image
            def _tSNR_img(filtered_func_vol_nii,nii_data,nii_type,nii_gm_mask,prefix_str): 
                #use absolute paths so os.chdir() is not required
                nii_out_fname=os.path.join(output_dir,prefix_str+'.nii.gz')
                nii_out_fname_gm=os.path.join(output_dir,prefix_str+'_gm.nii.gz')
                png_file=os.path.join(output_dir,prefix_str+'.png')
                txt_file=os.path.join(output_dir,prefix_str+'_gm_mean.txt')
                cmind_save_nii_mod_header(filtered_func_vol_nii,nii_data,nii_out_fname,nii_type)
                log_cmd('$FSLDIR/bin/fslmaths "%s" -mas "%s" "%s"' % (nii_out_fname, nii_gm_mask, nii_out_fname_gm), verbose=verbose, logger=module_logger);
                log_cmd('$FSLDIR/bin/fslstats "%s" -M > "%s"' % (nii_out_fname_gm, txt_file), verbose=verbose, logger=module_logger);
                log_cmd('$FSLDIR/bin/slicer "%s" -A 360 "%s"' % (nii_out_fname, png_file), verbose=verbose, logger=module_logger);
            
            if fexists:
                (tSNR2_tmp, mask_tmp,mean_tSNR2_tmp)=calc_tSNR_regress(filtered_func_vol,dm_filt,mask_filt)[0:3];
                nii_type=16;
                if combIDX==0:
                    _tSNR_img(filtered_func_vol_nii,tSNR2_tmp,nii_type,gm_mask,'tSNR_none')
                elif combIDX==default_idx:
                    _tSNR_img(filtered_func_vol_nii,tSNR2_tmp,nii_type,gm_mask,'tSNR_default')
                elif combIDX==best_idx:
                    _tSNR_img(filtered_func_vol_nii,tSNR2_tmp,nii_type,gm_mask,'tSNR_best')
                elif combIDX==PCA99_idx:
                    _tSNR_img(filtered_func_vol_nii,tSNR2_tmp,nii_type,gm_mask,'tSNR_PCA99')
                    
        ratio_best_worst=metric_used[best_idx]/metric_used[worst_idx]
        ratio_best_default=metric_used[best_idx]/metric_used[default_idx]
        ratio_PCA99_default=metric_used[PCA99_idx]/metric_used[default_idx]
        ratio_default_worst=metric_used[default_idx]/metric_used[worst_idx]
        ratio_default_none=metric_used[default_idx]/metric_used[1]
        ratio_best_none=metric_used[best_idx]/metric_used[1]
        np.savetxt(all_ratio_file,np.asarray([ratio_best_worst, ratio_best_default, ratio_PCA99_default, ratio_default_worst, ratio_default_none, ratio_best_none]),fmt='%0.7g');
        
        if True:  #write out summaries to disk
            effect_size_summaries={}
            effect_size_summaries['ratio_best_worst']=ratio_best_worst
            effect_size_summaries['ratio_best_default']=ratio_best_default
            effect_size_summaries['ratio_PCA99_default']=ratio_PCA99_default
            effect_size_summaries['ratio_default_worst']=ratio_default_worst
            effect_size_summaries['ratio_default_none']=ratio_default_none
            effect_size_summaries['ratio_best_none']=ratio_best_none
            effect_size_summaries['worst_idx']=worst_idx
            effect_size_summaries['best_idx']=best_idx
            effect_size_summaries['default_idx']=default_idx
            effect_size_summaries['all_tSNR_gm']=all_tSNR_gm
            effect_size_summaries['all_tSNR2_gm']=all_tSNR2_gm
            effect_size_summaries['all_RE']=all_RE
            effect_size_summaries['all_Nnuis']=all_Nnuis
            effect_size_summaries['combinations']=combinations
            effect_size_summaries['metric1']=metric_1
            effect_size_summaries['metric2']=metric_2
            with open(effect_size_file, "wb") as f:
                pickle.dump(effect_size_summaries,f)
            #effect_size_summaries = pickle.load(open( 'effect_size_summaries.pickle', "rb" ))
            #all_nuisance=pickle.load(open('all_nuisance.pickle','rb'))

        def _copy_confound(confound_file,confound_idx,prefix_str):
            log_cmd('cp "%s" "%s"' % (os.path.join(output_dir,'tmp_nuis%04d.txt' % confound_idx), confound_file), verbose=verbose, logger=module_logger);
            log_cmd('cp "%s" "%s"' % (os.path.join(output_dir,'design%04d.png' % confound_idx), os.path.join(output_dir,prefix_str+'design.png')), verbose=verbose, logger=module_logger);
            log_cmd('cp "%s" "%s"' % (os.path.join(output_dir,'design%04d_cov.png' % confound_idx), os.path.join(output_dir,prefix_str+'design_cov.png')), verbose=verbose, logger=module_logger);
        
        #local function to copy a specified confound matrix and the corresponding design matrix images
        _copy_confound(default_confound_file,default_idx,'default_')
        _copy_confound(best_confound_file,best_idx,'best_')
        _copy_confound(worst_confound_file,worst_idx,'worst_')
        _copy_confound(PCA95_confound_file,PCA95_idx,'PCA95_')
        _copy_confound(PCA99_confound_file,PCA99_idx,'PCA99_')                                
        
        #cleanup 
        [os.remove(item) for item in glob.glob(os.path.join(output_dir,'design[0|1|2]*'))]
        [os.remove(item) for item in glob.glob(os.path.join(output_dir,'design_cov[0|1|2]*.png'))]
        [os.remove(item) for item in glob.glob(os.path.join(output_dir,'design.*'))]
        [os.remove(item) for item in glob.glob(os.path.join(output_dir,'design_cov.png'))]
        [os.remove(item) for item in glob.glob(os.path.join(output_dir,'RequiredEffects0*.txt'))]
        [os.remove(item) for item in glob.glob(os.path.join(output_dir,'tmp_nuis0*.txt'))]
    
    #best_combinations(si(end),:)
    
    #print any filename outputs for capture by LONI
    print("default_confound_file:{}".format(default_confound_file))
    print("best_confound_file:{}".format(best_confound_file))
    print("worst_confound_file:{}".format(worst_confound_file))
    print("mcf06_confound_file:{}".format(mcf06_confound_file))
    print("FSL_outliers_txt:{}".format(FSL_outliers_txt))
    return (default_confound_file, best_confound_file, worst_confound_file, mcf06_confound_file, FSL_outliers_txt)



def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_ASLBOLD_dir
    T1_dir=os.path.join(cmind_example_output_dir,'F','T1_proc')
    WM_pve_vol=os.path.join(T1_dir,'T1Segment_WM.nii.gz')
    GM_pve_vol=os.path.join(T1_dir,'T1Segment_GM.nii.gz')
    CSF_pve_vol=os.path.join(T1_dir,'T1Segment_CSF.nii.gz')
    generate_figures=True
    ForceUpdate=True #False
    verbose=True
    Nproc_max=16
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
        
    if verbose:
        func=cmind_timer(cmind_fMRI_outliers,logger=logger)
    else:
        func=cmind_fMRI_outliers
    
    paradigms=['Sentences','Stories']
    acq_types=['ASL','BOLD']
    for paradigm in paradigms:
        for acq_type in acq_types:
            regBBR_ref=os.path.join(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'regBBR','EPIvol.nii.gz')
            HRtoLR_Affine=os.path.join(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'regBBR','lowres2highres_inv.mat')
            HRtoLR_Warp=os.path.join(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'regBBR','lowres2highres_warp_inv.nii.gz')
            if not os.path.exists(HRtoLR_Warp):
                HRtoLR_Warp=None
            output_dir=os.path.join(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'outliers_%s' % acq_type)
            if acq_type=='ASL':
                contrast_of_interest=1
            elif acq_type=='BOLD':
                contrast_of_interest=0
            vol_file_niigz=os.path.join(output_dir,'..','%s_%s_mcf.nii.gz' % (acq_type,paradigm))
            filtered_func_data=os.path.join(output_dir,'..','Feat_%s' % acq_type,'filtered_func_data.nii.gz')
            motion_parfile=os.path.join(output_dir,'..','%s_%s_mcf.par' % (acq_type,paradigm))
            unfiltered_design_matrix=os.path.join(cmind_ASLBOLD_dir,'unfilt_%s_%s.mat' % (acq_type,paradigm))
            FSF_template=os.path.join(cmind_ASLBOLD_dir,'%s_level1_%s.fsf' % (acq_type,paradigm))
            noise_est=os.path.join(cmind_example_output_dir,'F','ASLBOLD_%s' % (paradigm),'Feat_%s' % (acq_type),'noise_est.txt')
            func(output_dir,
                 contrast_of_interest, 
                 regBBR_ref, 
                 WM_pve_vol,
                 GM_pve_vol,
                 CSF_pve_vol, 
                 vol_file_niigz, 
                 filtered_func_data, 
                 motion_parfile, 
                 unfiltered_design_matrix,
                 FSF_template, 
                 noise_est, 
                 HRtoLR_Affine=HRtoLR_Affine, 
                 HRtoLR_Warp=HRtoLR_Warp, 
                 Nproc_max=Nproc_max, 
                 generate_figures=generate_figures, 
                 ForceUpdate=ForceUpdate, 
                 verbose=verbose, 
                 logger=logger)

if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        _testme()
    else:  
        parser = argparse.ArgumentParser(description="Detect outlier timepoints in fMRI timeseries and generate nuisance regressors", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-con","--contrast_of_interest", required=True, type=int, help="which contrast in FSF_template is of primary interest?")
        parser.add_argument("-regBBR","--regBBR_ref", required=True, help="filename of the regBBR reference image")
        parser.add_argument("-wm","--WM_pve_vol", required=True, type=str, help="WM partial volume estimate from structural processing")
        parser.add_argument("-gm","--GM_pve_vol", required=True, type=str, help="GM partial volume estimate from structural processing")
        parser.add_argument("-csf","--CSF_pve_vol", required=True, type=str, help="CSF partial volume estimate from structural processing")
        parser.add_argument("-vol","--vol_file_niigz", required=True, type=str, help="unfiltered 4D fMRI timeseries to process")
        parser.add_argument("-volfilt","--filtered_func_data", required=True, type=str, help="smoothed and temporal filtered 4D fMRI timeseries file to process")
        parser.add_argument("-mpar","--motion_parfile", required=True, type=str, help="The 6 column motion parameter (.par) file from mcflirt")
        parser.add_argument("-dm","--unfiltered_design_matrix", required=True, type=str, help="The (unfiltered) Feat design matrix corresponding to `vol_file_niigz`")
        parser.add_argument("-fsf","--FSF_template", required=True, type=str, help="THE .fsf template corresponding to `vol_file_niigz`")
        parser.add_argument("-noise","--noise_est", required=True, type=str, help="The noise_est.txt file from cmind_fMRI_preprocess2.py")
        parser.add_argument("-aff","--HRtoLR_Affine", type=str, default='None', help="filename for the affine transform from functional->structural space")
        parser.add_argument("-w","--HRtoLR_Warp", type=str, default='None', help="filename for the warp from functional->structural space")
        parser.add_argument("-np","--Nproc_max", type=int, default=1, help="maximum number of processes to launch simultaneously")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
    
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        #if len(sys.argv) == 2 and sys.argv[1].lower()=='gui':
        #    from PySide import QtGui
        #    #from argparseui import ArgparseUi
        #    from my_argparseui import ArgparseUi
        #    
        #    app = QtGui.QApplication(sys.argv)
        #    a = ArgparseUi(parser)
        #    a.show()
        #    app.exec_()
        #    print ("Ok" if a.result() == 1 else "Cancel")
        #    if a.result() == 1: # Ok pressed
        #        parsed_args = a.parse_args()
        #    else:
        #        parsed_args = None
        #    
        #    print (parsed_args)
        
        args=parser.parse_args()
        
        output_dir=args.output_dir;
        contrast_of_interest=args.contrast_of_interest;
        regBBR_ref=args.regBBR_ref;
        HRtoLR_Affine=str2none(args.HRtoLR_Affine);
        HRtoLR_Warp=str2none(args.HRtoLR_Warp);
        WM_pve_vol=args.WM_pve_vol;
        GM_pve_vol=args.GM_pve_vol;
        CSF_pve_vol=args.CSF_pve_vol;
        vol_file_niigz=args.vol_file_niigz;
        filtered_func_data=args.filtered_func_data;
        motion_parfile=args.motion_parfile;
        unfiltered_design_matrix=args.unfiltered_design_matrix;
        FSF_template=args.FSF_template;
        noise_est=args.noise_est;
        Nproc_max=args.Nproc_max
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger;
        
        if verbose:
            cmind_fMRI_outliers=cmind_timer(cmind_fMRI_outliers,logger=logger)
            
        cmind_fMRI_outliers(output_dir,
                            contrast_of_interest, 
                            regBBR_ref, 
                            WM_pve_vol,
                            GM_pve_vol,
                            CSF_pve_vol, 
                            vol_file_niigz, 
                            filtered_func_data, 
                            motion_parfile, 
                            unfiltered_design_matrix,
                            FSF_template, 
                            noise_est, 
                            HRtoLR_Affine=HRtoLR_Affine, 
                            HRtoLR_Warp=HRtoLR_Warp, 
                            Nproc_max=Nproc_max, 
                            generate_figures=generate_figures, 
                            ForceUpdate=ForceUpdate, 
                            verbose=verbose, 
                            logger=logger)
    #sys.exit()

#import scipy.stats as stats
#example getting a student's t score corresponding to a given p-value
#ntails=1; p=0.05; dof=100; stats.t.ppf(1-p/ntails,dof)
