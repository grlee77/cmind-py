#!/bin/bash
echo "CMD: $*"

usage()
{
echo "Basic usage: cmind_fMRI_level1_poststats.py -o <output_dir> -fsf <FSF_template>  [options]

usage: cmind_fMRI_level1_poststats.py [-h] -o OUTPUT_DIR -prepdir PREP_DIR -statsdir STATS_DIR
                                  [-p LVL1_STATS_CASE] [-nuis NUISANCE_FILE]
                                  [-feat FEAT_PARAMS] [--z_thresh Z_THRESH]
                                  [--prob_thresh PROB_THRESH]
                                  [--thresh THRESH] [-outtar OUTPUT_TAR]
                                  [-gf GENERATE_FIGURES]
                                  [--CMIND_specific_figures CMIND_SPECIFIC_FIGURES]
                                  [-u FORCEUPDATE] [-v] [-log LOGGER]

Utility to run FEAT poststat processing equivalent

required parameters:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -prepdir PREP_DIR, --prep_dir PREP_DIR
                        prep_dir from cmind_fMRI_preprocess2
  -statsdir STATS_DIR, --stats_dir STATS_DIR
                        stats dir from cmind_fMRI_level1_stats
  -st 					scan type
  				ASL or BOLD
  -p     				paradigm name
  				Stories or Sentences

optional arguments:
  -h, --help            show this help message and exit
  --lvl1_stats_case lvl1_stats_case
                        Feat_ASL_Stories, Feat_BOLD_Sentences, etc
  -nuis NUISANCE_FILE, --nuisance_file NUISANCE_FILE
                        path to a text file with one nuisance regressor per
                        column
  -feat FEAT_PARAMS, --feat_params FEAT_PARAMS
                        filename to a .csv file containing feat parameters
                        listed as key,value pairs. If z_thresh, prob_thresh,
                        or thresh are explicitly passed in, those values will
                        override the ones in the feat_params file. Typically
                        feat_params should be the preproc_options_file output
                        by cmind_fMRI_preprocess2
  --z_thresh Z_THRESH   initial z threshold to use when determining clusters.
                        unused if thresh!=2
  --prob_thresh PROB_THRESH
                        probability threshold for voxelwise or cluster
                        significance
  --thresh THRESH       {0,1,2} 0 = uncorrected, 1 = voxel-based (FWE
                        corrected), 2 = cluster-based
  -outtar OUTPUT_TAR, --output_tar OUTPUT_TAR
                        if true, generate .tar.gz versions of stats and
                        poststats folders
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate overlay images summarizing the
                        registration
  --CMIND_specific_figures CMIND_SPECIFIC_FIGURES
                        generate additional figures for the CMIND paradigm
                        results. set this to False if the data is not from the
                        CMIND project
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        filename to log to
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=10
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

if [ $# == 0 ]; then usage;  exit 0; fi

if [ "$1" == "-h" ]; then usage; exit 0; fi

REQUIRED_ARGS=6
if [ $# -lt $REQUIRED_ARGS ] 
then
    usage
    exit 1
fi


feat_params="default"
lvl1_stats_case="default"
CMIND_specific_figures=true
generate_figures=true
ForceUpdate=true
output_tar=true


verbose_string=""
all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -prepdir|--prep_dir )
        shift
        prep_dir=$1
        ;;
    -statsdir|--stats_dir )
        shift
        stats_dir=$1
        ;;
    -st|--scan_type )
        shift
        scantype_str=$1
        ;;
    -p|-ppc|--paradigm )
        shift
        paradigm=$1
        ;;
    --lvl1_stats_case )
        shift
        lvl1_stats_case=$1
        ;;
    -nuis|--nuisance_file )
        shift
        nuisance_file=$1
        ;;
    -feat|--feat_params )
        shift
        feat_params=$1
        ;;
    --z_thresh )
        shift
        z_thresh=$1
        ;;
    --prob_thresh )
        shift
        prob_thresh=$1
        ;;
    --thresh )
        shift
        thresh=$1
        ;;
    -outtar|--output_tar )
        shift
        output_tar=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    --CMIND_specific_figures )
        shift
        CMIND_specific_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;	 

    esac
    shift
done


if [ ! -z $verbose_string ]; then
	debug=true
else
	debug=false
fi

if [ $feat_params = "default" ]; then
	feat_params="${CMIND_ASLBOLD_DIR}/feat_params.csv"
fi

if [ $lvl1_stats_case = "default" ]; then
	lvl1_stats_case="Feat_${scantype_str}_${paradigm}"
fi

if [ ! -f $nuisance_file ]; then
    echo "ERROR: nuisance_file file: $nuisance_file doesn't exist"
    exit 1
fi 

if [ ! -f $FSF_template ]; then
    echo "ERROR: FSF_template file: $FSF_template doesn't exist"
    exit 1
fi 

if [ ! -f $feat_params ]; then
    echo "ERROR: feat_params file: $feat_params doesn't exist"
    exit 1
fi 

echo "output_dir=$output_dir"
echo "nuisance_file=$nuisance_file"
echo "FSF_template=$FSF_template"
echo "feat_params=$feat_params"
echo "generate_figures=$generate_figures"
echo "ForceUpdate=$ForceUpdate"




#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir prep_dir stats_dir lvl1_stats_case nuisance_file feat_params z_thresh prob_thresh thresh output_tar generate_figures CMIND_specific_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_fMRI_level1_poststats.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_fMRI_level1_poststats.py ${verbose_string} ${all_optional_args}
printf 'cmind_fMRI_level1_poststats.py:  Elapsed time: %s\n' $(timer $t)


# t=$(timer)
# echo "python ${cmind_python_dir}/cmind_fMRI_level1_stats.py ${output_dir} ${stats_case} ${nuisance_file} ${FSF_template} ${feat_params} --CMIND_specific_figures ${CMIND_specific_figures} --generate_figures ${generate_figures} --ForceUpdate ${ForceUpdate} --output_tar $output_tar $verbose_string"
# python ${cmind_python_dir}/cmind_fMRI_level1_stats.py ${output_dir} ${stats_case} ${nuisance_file} ${FSF_template} ${feat_params} --CMIND_specific_figures ${CMIND_specific_figures}  --generate_figures ${generate_figures} --ForceUpdate ${ForceUpdate} --output_tar $output_tar $verbose_string
# printf 'cmind_fMRI_level1_stats:  Elapsed time: %s\n' $(timer $t)


#check that output report exists
output_file_test=${output_dir}/thresh_zstat1.nii.gz
echo "$output_file_test"
if [ ! -f $output_file_test ]; then
    echo "ERROR: Failed to finish fMRI Level1 Post-Stats"
    exit 1
fi 

echo "Finished cmind_fMRI_level1_poststats"

