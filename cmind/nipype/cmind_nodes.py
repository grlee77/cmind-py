import os, warnings
from os.path import join as pjoin
from copy import deepcopy

import cmind.nipype.interfaces as cpipe
import nipype.pipeline.engine as pe
from nipype import logging
logger = logging.getLogger('workflow')
from nipype.utils.filemanip import filename_to_list

from cmind.globals import (cmind_template_dir, 
                           cmind_atlas_labels_AAL, 
                           cmind_atlas_AAL,
                           cmind_DTIPrep, 
                           CMIND_DTIHARDI_DIR, 
                           cmind_Slicer3D, 
                           cmind_Slicer3D_cli_dir, 
                           cmind_Slicer3D_env)

#default variables used by many nodes
gvars={}  
gvars['generate_coverage']=True
gvars['generate_figures']=True
gvars['ForceUpdate']=False #for nipype always set this True?  Nipype will determine if the inputs have changed and the processing should be rerun
gvars['verbose']=True
gvars['logger']=""  #nipype will do its own logging, so turn it off in the individual modules


"""
Because of the way the cmind nodes were written we need to subclass nipype's
Node class and explictly force the "output_dir" input of the interface to 
match the output_dir expected by the nipype workflow.
If this is not done, the processed files will be created outside the expected
worflow directories.
"""
class cmindNode(pe.Node):
    """ version of nipype Node that sets _interface.inputs.output_dir to 
    self.output_dir() prior to calling run().  This is so the outputs will end
    up at the expected location.
    """
    def run(self, updatehash=False):
        try:
            self._interface.inputs.output_dir = self.output_dir()
        except:
            print("unable to set output_dir")
        super(cmindNode,self).run(updatehash=updatehash)

class cmindMapNode(pe.MapNode, cmindNode):
    """Create a MapNode equivalent for cmindNode nodes rather than nipype Nodes
    """

    def __init__(self, interface, iterfield, name, **kwargs):
        """

        Parameters
        ----------
        interface : interface object
            node specific interface (fsl.Bet(), spm.Coregister())
        iterfield : string or list of strings
            name(s) of input fields that will receive a list of whatever kind
            of input they take. the node will be run separately for each
            value in these lists. for more than one input, the values are
            paired (i.e. it does not compute a combinatorial product).
        name : alphanumeric string
            node specific name

        See Node docstring for additional keyword arguments.
        """
        cmindNode.__init__(self,interface, name, **kwargs)  #use __init__() from cmindNode instead of pe.MapNode
        if isinstance(iterfield, str):
            iterfield = [iterfield]
        self.iterfield = iterfield
        self._inputs = self._create_dynamic_traits(self._interface.inputs,
                                                   fields=self.iterfield)
        self._inputs.on_trait_change(self._set_mapnode_input)
        self._got_inputs = False
        
    def _make_nodes(self, cwd=None): 
        if cwd is None:
            cwd = self.output_dir()
        nitems = len(filename_to_list(getattr(self.inputs, self.iterfield[0])))
        for i in range(nitems):
            nodename = '_' + self.name + str(i)
            node = pe.Node(deepcopy(self._interface), name=nodename)
            node.overwrite = self.overwrite
            node.run_without_submitting = self.run_without_submitting
            node.plugin_args = self.plugin_args
            node._interface.inputs.set(
                **deepcopy(self._interface.inputs.get()))
                
            for field in self.iterfield:
                fieldvals = filename_to_list(getattr(self.inputs, field))
                logger.debug('setting input %d %s %s' % (i, field,
                                                         fieldvals[i]))
                setattr(node.inputs, field,
                        fieldvals[i])
            node.config = self.config
            node.base_dir = os.path.join(cwd, 'mapflow')
            try:   #set node._interface.inputs.output_dir to node.output_dir()
                node._interface.inputs.output_dir = node.output_dir()
            except:
                print("unable to set output_dir on node")          
            yield i, node
            

""" Define default nodes for many of the cmind interfaces.  Some of these have
input variables initialized to parameters that are specific to the C-MIND study
 """

cmind_struct_preprocess = cmindNode(name='cmind_struct_preprocess0',
                                    interface=cpipe.cmind_struct_preprocess(
                                        verbose=gvars['verbose'],
                                        generate_figures=gvars[
                                            'generate_figures'],
                                        ForceUpdate=gvars['ForceUpdate'],
                                        logger=gvars['logger'],
                                        oname_root='T1W',
                                        bet_thresh=0.3,
                                        bet_use_T2=False,
                                        reupdate_bias_cor=True,
                                        resegment_using_N4mask=True,
                                        omit_segmentation=False,
                                    ),
                                    )
try:
    cmind_age_to_template = cmindNode(name='cmind_age_to_template0',
                                      interface=cpipe.cmind_age_to_template(
                                          use_OASIS_MNI=False,
                                          verbose=gvars['verbose'],
                                          ForceUpdate=gvars['ForceUpdate'],
                                          # ForceUpdate=True,
                                          logger=gvars['logger'],
                                          ped_template_path=cmind_template_dir,
                                      )
                                      )
except:
    warnings.warn("Pediatric Template not Found")
    
cmind_normalize = cmindNode(name='cmind_normalize0',
                            interface=cpipe.cmind_normalize(
                                 verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                direct_to_MNI=False,
                                reg_flag='101',
                                ANTS_reg_case='SynAff',
                                #MNI_path=cmind_MNI_dir
                                #ANTS_path=cmind_ANTs_dir
                                )
                          )                                                                                                                                                                                                                     
                 

cmind_segment = cmindNode(name='cmind_segment',
                             interface=cpipe.cmind_segment(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                use_prior = False,
                                use_alt_prior = False,
                                use_prior_throughout = False,
                                resegment_using_N4mask = False,
                                ),
                          )   

cmind_segment_atropos = cmindNode(name='cmind_segment_atropos',
                             interface=cpipe.cmind_segment_atropos(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                output_dir='',  #empty string->will be automatically set to default
                                additional_intensity_vols=[],
                                prior_weight=0.1,
                                resegment_using_N4mask=False,
                                ),
                          )                             

                          
cmind_alpha = cmindNode(name='cmind_alpha',
                             interface=cpipe.cmind_alpha(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                )
                          )   
                          
cmind_baselineCBF_preprocess = cmindNode(name='cmind_baselineCBF_preprocess',
                             interface=cpipe.cmind_baselineCBF_preprocess(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                discard_outliers=True,
                                max_percent_diff=3.0,
                                fsl_motion_outliers_cmd='',  #default will be cmind.globals.cmind_outliers_cmd
                                ANTS_bias_cmd='',  #will look for N4BiasFieldCorrection in cmind.globals.cmind_ANTs_dir
                                )
                          )                             

cmind_baselineCBF_coregisterT1 = cmindNode(name='cmind_baselineCBF_coregisterT1',
                             interface=cpipe.cmind_baselineCBF_coregisterT1(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                )
                          )   
                          

cmind_baselineCBF_quantitative = cmindNode(name='cmind_baselineCBF_quantitative',
                             interface=cpipe.cmind_baselineCBF_quantitative(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                relmot_thresh=1.5, 
                                cbf_lambda=0.9,
                                R1a=0.625,
                                delta=1.5,
                                delta_art=1.3,
                                w=1.4,
                                tau=2.0,
                                TR=4.0,
                                T2star_wm=45.0, 
                                T2star_arterial_blood=53.0, 
                                TE=11.5
                                )
                          )                               

cmind_T1_tissue_averages = cmindNode(name='cmind_T1_tissue_averages',
                             interface=cpipe.cmind_T1_tissue_averages(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                PVEthresh_pct=75.0,
                                )
                          )   

cmind_atlas_to_subject = cmindNode(name='cmind_atlas_to_subject',
                             interface=cpipe.cmind_atlas_to_subject(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                             )
                         )   

cmind_atlas_ROI_averages = cmindNode(name='cmind_atlas_ROI_averages',
                             interface=cpipe.cmind_atlas_ROI_averages(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                atlas_vol = cmind_atlas_AAL,
                                ROI_labels_csv = cmind_atlas_labels_AAL
                             )
                         )   

                                                  
cmind_fieldmap_process = cmindNode(name='cmind_fieldmap_process',
                             interface=cpipe.cmind_fieldmap_process(
                                verbose=gvars['verbose'],
                                #generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                deltaTE=0.0023, 
                                unwarpdir='y'
                                )
                          )   
                          
cmind_fieldmap_regBBR_CBF = cmindNode(name='cmind_fieldmap_regBBR_CBF',
                             interface=cpipe.cmind_fieldmap_regBBR(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                bbr_schedule_file='',  #will try to find automatically within cmind.globals.cmind_FSL_dir
                                unwarpdir='y', 
                                EPI_echo_spacing=0.00055, #seconds  ['Stories' , 'Sentences', 'BaselineCBF', 'T1map']
                                SENSE_factor=2  #['Stories' , 'Sentences', 'BaselineCBF', 'T1map']
                                )
                          )   
cmind_fieldmap_regBBR_ASLBOLD = cmind_fieldmap_regBBR_CBF.clone('cmind_fieldmap_regBBR_ASLBOLD')
#cmind_fieldmap_regBBR_T1map = cmind_fieldmap_regBBR_CBF.clone('cmind_fieldmap_regBBR_T1map')

cmind_fieldmap_regBBR_DTI = cmind_fieldmap_regBBR_CBF.clone('cmind_fieldmap_regBBR_DTI')
cmind_fieldmap_regBBR_DTI.inputs.SENSE_factor=3
cmind_fieldmap_regBBR_DTI.inputs.EPI_echo_spacing=0.00069
cmind_fieldmap_regBBR_HARDI = cmind_fieldmap_regBBR_DTI.clone('cmind_fieldmap_regBBR_HARDI')

#cmind_fieldmap_regBBR_DTI = cmindNode(name='cmind_fieldmap_regBBR',
#                             interface=cpipe.cmind_fieldmap_regBBR(
#                                verbose=gvars['verbose'],
#                                generate_figures=gvars['generate_figures'],
#                                ForceUpdate=gvars['ForceUpdate'],
#                                logger=gvars['logger'],
#                                bbr_schedule_file='',  #will try to find automatically within cmind.globals.cmind_FSL_dir
#                                unwarpdir='y', 
#                                EPI_echo_spacing=0.00069, #seconds  ['HARDI', 'DTI']
#                                SENSE_factor=3   #['HARDI', 'DTI']
#                                )
#                          )   
#                          

cmind_fieldmap_regBBR_RestingState = cmind_fieldmap_regBBR_CBF.clone('cmind_fieldmap_regBBR__RestingState')
cmind_fieldmap_regBBR_RestingState.inputs.SENSE_factor=2
cmind_fieldmap_regBBR_RestingState.inputs.EPI_echo_spacing=0.00043                                                          


cmind_T1map_preprocess = cmindNode(name='cmind_T1map_preprocess',
                                interface=cpipe.cmind_T1map_preprocess(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                do_motion_cor=True,
                                ANTS_bias_cmd=''
                                )
                            )         
                            
cmind_T1map_fit = cmindNode(name='cmind_T1map_fit',
                                interface=cpipe.cmind_T1map_fit(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                show_plots=False,
                                )
                            )           
                            
                               
cmind_apply_normalization = cmindNode(name='cmind_apply_normalization',
                                interface=cpipe.cmind_apply_normalization(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                ANTS_path='',
                                Norm_Target='MNI',
                                generate_coverage=gvars['generate_coverage'],
                                output_name="default",
                                interp_type='', 
                                reg_flag='001',
                                )
                            )       
                            
#cmind_apply_normalization_T1map = cmind_apply_normalization.clone('cmind_apply_normalization_T1map')

          
cmind_fMRI_preprocess = cmindNode(name='cmind_fMRI_preprocess',
                             interface=cpipe.cmind_fMRI_preprocess(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                FORCE_SINGLEPROCESS=True,  #multiprocessing was sometimes getting stuck so just force single process
                                #ANTS_bias_cmd='',
                                #fsl_motion_outliers_cmd='',
                                )
                          )     
cmind_fMRI_preprocess2_ASL = cmindNode(name='cmind_fMRI_preprocess2_ASL',
                             interface=cpipe.cmind_fMRI_preprocess2(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                TR=4.0,
                                perfusion_subtract=False,
                                output_tar=True,
                                slice_timing_file='', #None
                                )
                          )     
cmind_fMRI_preprocess2_BOLD=cmind_fMRI_preprocess2_ASL.clone('cmind_fMRI_preprocess2_BOLD')                          
cmind_fMRI_outliers_ASL = cmindNode(name='cmind_fMRI_outliers_ASL',
                             interface=cpipe.cmind_fMRI_outliers(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                Nproc_max=-1,
                                contrast_of_interest=1,
                                )
                          )    
                          
cmind_fMRI_outliers_BOLD=cmind_fMRI_outliers_ASL.clone('cmind_fMRI_outliers_BOLD')
cmind_fMRI_outliers_BOLD.inputs.contrast_of_interest=0

cmind_fMRI_level1_stats_ASL = cmindNode(name='cmind_fMRI_level1_stats_ASL',
                             interface=cpipe.cmind_fMRI_level1_stats(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                CMIND_specific_figures=False,
                                output_tar=True,
                                omit_poststats=True,
                                )
                          )                                                       
cmind_fMRI_level1_stats_BOLD=cmind_fMRI_level1_stats_ASL.clone('cmind_fMRI_level1_stats_BOLD')                                                                                                                        

#cmind_fMRI_level1_poststats_ASL = cmindMapNode(name='cmind_fMRI_level1_poststats_ASL',
cmind_fMRI_level1_poststats_ASL = cmindNode(name='cmind_fMRI_level1_poststats_ASL',
                             interface=cpipe.cmind_fMRI_level1_poststats(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                CMIND_specific_figures=True,
                                output_tar=True,
                                ),
                            iterfield=['z_thresh','prob_thresh','thresh']
                          )                                                       
cmind_fMRI_level1_poststats_BOLD=cmind_fMRI_level1_poststats_ASL.clone('cmind_fMRI_level1_poststats_BOLD')                                                                                                                        

cmind_fMRI_norm_ASL = cmindNode(name='cmind_fMRI_norm_ASL',
                             interface=cpipe.cmind_fMRI_norm(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                output_tar=True,
                                reg_flag='001',
                                #ANTS_path='',
                                )
                          )                 
cmind_fMRI_norm_BOLD=cmind_fMRI_norm_ASL.clone('cmind_fMRI_norm_BOLD')        


cmind_build_feat_dir = cmindNode(name='cmind_build_feat_dir',
                             interface=cpipe.cmind_build_feat_dir(
                                verbose=gvars['verbose'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                reduce_file_level=1,
                                )
                          )                 

         
                          
"""Additional nodes below here for completeness, but not currently used individually"""

cmind_crop_robust = cmindNode(name='cmind_crop_robust',
                            interface=cpipe.cmind_crop_robust(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                oname_root='T1W',
                                #age_months=subject_age,
                                #z_crop_mm=0,
                                #minthresh=0.03,
                                #fname2="", 
                                #fname2_crop=""
                                ),
                            )

cmind_brain_extract = cmindNode(name='cmind_brain_extract',
                            interface=cpipe.cmind_brain_extract(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                #use_AFNI=True, 
                                use_AFNI=False, 
                                )       
                             )

cmind_bias_correct = cmindNode(name='cmind_bias_correct',
                             interface=cpipe.cmind_bias_correct(
                                verbose=gvars['verbose'],
                                generate_figures=gvars['generate_figures'],
                                ForceUpdate=gvars['ForceUpdate'],
                                logger=gvars['logger'],
                                )
                             )        

cmind_calculate_slice_coverage = cmindNode(name='cmind_calculate_slice_coverage',
                             interface=cpipe.cmind_calculate_slice_coverage(
                                verbose=gvars['verbose'],
                                logger=gvars['logger'],
                                )
                          )         
                          
try:
    if cmind_Slicer3D_cli_dir is not None:
        DWIConvert_command=pjoin(cmind_Slicer3D_cli_dir,'DWIConvert')
        Slicer_denoise_module=pjoin(cmind_Slicer3D_cli_dir,'DWIJointRicianLMMSEFilter')
    else:
        DWIConvert_command=None
        Slicer_denoise_module=None
    
    cmind_DTI_QA = cmindNode(name='cmind_DTI_QA',
                                    interface=cpipe.cmind_DTI_HARDI_QA(
                                    verbose=gvars['verbose'],
                                    generate_figures=gvars['generate_figures'],
                                    ForceUpdate=gvars['ForceUpdate'],
                                    logger=gvars['logger'],
                                    gradient_table_file=pjoin(CMIND_DTIHARDI_DIR,'Philips_LAS_bvects.txt'),
                                    gradient_bval_file=pjoin(CMIND_DTIHARDI_DIR,'Philips_LAS_bvals_DTI.txt'),
                                    case_to_run='DTI_QA_DN_process',
                                    DTIPrep_command=cmind_DTIPrep,
                                    DTIPrep_xmlProtocol=pjoin(CMIND_DTIHARDI_DIR,'CMIND_DTIPrep_Protocol_noBrainMask.xml'),
                                    DWIConvert_command=DWIConvert_command,
                                    Slicer_path=cmind_Slicer3D,
                                    Slicer_denoise_module=Slicer_denoise_module,
                                    isSiemens=False,
                                    isOblique=False,
                                    denoise_case='jointLMMSE',
                                    env_dict=cmind_Slicer3D_env
                                    )                               
                             )
                                
    #HARDI version of QA has a different bval file and case name                            
    cmind_HARDI_QA = cmind_DTI_QA.clone('cmind_HARDI_QA')                                                 
    cmind_HARDI_QA.inputs.gradient_bval_file=pjoin(CMIND_DTIHARDI_DIR,'Philips_LAS_bvals_HARDI.txt')
    cmind_HARDI_QA.inputs.case_to_run='HARDI_QA_DN_process'
except:
    warnings.warn("couldn't initialize DTI/HARDI nodes.  check Slicer3D environment variables")
    
    
