#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_baselineCBF_preprocess(output_dir, CBF_nii, max_percent_diff=3.0,
                                 ANTS_bias_cmd=None, UCLA_flag=False,
                                 fsl_motion_outliers_cmd=None,
                                 discard_outliers=True,
                                 generate_figures=True, 
                                 ForceUpdate=False,
                                 verbose=False, logger=None):
    """BaselineCBF preprocessing (motion correction, intensity normalization & 
    outlier rejection)
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    CBF_nii : str
        nifti image containing the 4D ASL timeseries
    max_percent_diff : float, optional
        any control-tag frames greater than this percentage will be automatically
        considered outliers.  Used to intially remove high motion frames.  Note:
        For background-suppressed ASL data, this should be set to None or 100.0.
        Will not be used if `discard_outliers`=False
    ANTS_bias_cmd : str, optional
        path to the bias field correction command to use.
        if None, assumes /usr/lib/ants/N4BiasFieldCorrection
    UCLA_flag : bool, optional
        if True, assumes order is "tag","control","tag",...
        if False, assumes order is "control", "tag", "control"...
        if true, generate summary images
    fsl_motion_outliers_cmd : str, optional
        path to the fsl_motion_outliers_cmd_v2 shell script.  If not specified
        will try to guess it relative to the python script location.
    discard_outliers : bool, optional
        unless False, will detect and discard outliers
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    BaselineCBF_mcf_percent_change : str
        filename of the average control-tag percent change image
    BaselineCBF_mcf_control_mean_N4 : str
        filename of the average N4 bias-corrected control image
    BaselineCBF_Meansub : str
        filename of the average N4 bias-corrected control-tag image
    BaselineCBF_timepoints_kept : str
        textfile listing the control-tag timepoints that were kept
    BaselineCBF_mcf_masked : str
        filename of the ASL subtracted timeseries
    BaselineCBF_N4Bias : str
        filename of the N4 bias field computed from the mean control image
    BaselineCBF_relmot : str
        filename of the relative motion parameters during coregistration (motion-correction)
    
    Notes
    -----
    
    .. figure::  ../img/IRC04H_06M008_P_1_MeanSub_BaselineCBF_N4.png
       :align:   center
       :scale:   100%
       
       Mean (tag-control) image after bias correction and outlier rejection

    LONI_output_dir_dependencies
    ----------------------------
    
    BaselineCBF_mcf_percent_change : BaselineCBF_mcf_percent_change.nii.gz
    BaselineCBF_mcf_control_mean_N4 : BaselineCBF_control_mcf_mean_N4_brain.nii.gz
    Meansub_BaselineCBF : MeanSub_BaselineCBF_N4.nii.gz
    BaselineCBF_timepoints_kept : BaselineCBF_timepoints_kept.txt
    BaselineCBF_mcf_masked : BaselineCBF_mcf_masked.nii.gz
    BaselineCBF_N4Bias : BaselineCBF_N4Bias.nii.gz
    BaselineCBF_relmot : mc/BaselineCBF_mcf_rel.rms

    """

    import os, warnings
    from os.path import join as pjoin    
    
    import nibabel as nib
    import numpy as np
    
    from cmind.utils.fsl_mcflirt_opt import fsl_mcflirt_opt
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, split_multiple_ext
    from cmind.utils.nifti_utils import cmind_save_nii_mod_header
    from cmind.utils.logging_utils import (cmind_init_logging, log_cmd, 
                                           cmind_func_info)
    from cmind.finders import check_exist
    check_exist(['FSL','ANTs']) #raise exception if required external software not found
    
    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, UCLA_flag = input2bool(
                            [generate_figures, ForceUpdate, verbose, UCLA_flag])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting BaselineCBF preprocessing")
            
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    if not imexist(CBF_nii,'nifti-1'):
        raise IOError('specified Baseline CBF image, %s, doesn''t exist!'
                        % (CBF_nii))
    
    if not ANTS_bias_cmd:
        from cmind.globals import cmind_ANTs_dir
        ANTS_bias_cmd=pjoin(cmind_ANTs_dir,'N4BiasFieldCorrection')
 
    (exists, true_fname, fname_root, pth, ext)=imexist(CBF_nii,'nifti-1');
    CBF_nii_root=pjoin(output_dir,'BaselineCBF') #HARDCODE THIS NAME %strrep(fname_root,'.nii','');  %if it was .nii.gz, still need to remove the .nii
    
    mcf_str='_mcf';  #suffix for motion-corrected volume
    mc_dir=pjoin(output_dir,'mc')  #folder in which to store the motion parameters
    CBF_nii_mcflirt=CBF_nii_root+mcf_str  #motion-corrected basename
    control_root=CBF_nii_root+'_control'+mcf_str
    mean_control_root=control_root+'_mean'
    
    BaselineCBF_mcf_percent_change=CBF_nii_mcflirt+'_percent_change.nii.gz'
    BaselineCBF_mcf_control_mean_N4=mean_control_root+'_N4_brain.nii.gz'
    Meansub_BaselineCBF=pjoin(output_dir,'MeanSub_BaselineCBF_N4.nii.gz')
    BaselineCBF_Npairs_kept=pjoin(output_dir,'BaselineCBF_NPairs_kept.txt')
    BaselineCBF_timepoints_kept=pjoin(output_dir,'BaselineCBF_timepoints_kept.txt')
    BaselineCBF_mcf_masked=CBF_nii_mcflirt+'_masked.nii.gz'
    BaselineCBF_N4Bias=CBF_nii_root+'_N4Bias.nii.gz'
    BaselineCBF_mot=pjoin(mc_dir,os.path.basename(CBF_nii_mcflirt) + '.par')
    BaselineCBF_absmot=pjoin(mc_dir,os.path.basename(CBF_nii_mcflirt) + '_abs.rms')
    BaselineCBF_relmot=pjoin(mc_dir,os.path.basename(CBF_nii_mcflirt) + '_rel.rms')
    
    if (not imexist(BaselineCBF_mcf_percent_change,'nifti-1')[0]) or ForceUpdate:
    
        if (not imexist(pjoin(output_dir,CBF_nii_mcflirt),'nifti-1')[0]) or ForceUpdate:
            module_logger.info("Performing motion correction")
            cost_type='normcorr';
            fsl_mcflirt_opt(CBF_nii, cost_type, CBF_nii_mcflirt)  #multistage register to optimal mean volume  #+ '_mcf')
            if not os.path.exists(mc_dir):
                os.mkdir(mc_dir)
            log_cmd('mv "%s"/*.rms "%s"/*.par "%s"' 
                     % (output_dir,output_dir,mc_dir), 
                     verbose=verbose, logger=module_logger)
        
        #If Nifti-1 data is not FLOAT32 or FLOAT64, it will cause problems in later processing.  convert to float32 before further proceeding
        data_dtype=log_cmd('$FSLDIR/bin/fslval "%s" data_type' 
                            % (CBF_nii_mcflirt), 
                            verbose=verbose, logger=module_logger)
                            
        if data_dtype.find('FLOAT')==-1:
            log_cmd('$FSLDIR/bin/fslmaths "%s" "%s" -odt float' 
                    % (CBF_nii_mcflirt,CBF_nii_mcflirt), 
                    verbose=verbose, logger=module_logger)
        
        if generate_figures:
            try:
                import matplotlib as mpl
                mpl.use('agg') #don't plot to screen
                #import matplotlib.pylab as pylab
                import matplotlib.pyplot as plt
                
                motionpar=np.loadtxt(BaselineCBF_mot);
                absmotionpar=np.loadtxt(BaselineCBF_absmot);
                relmotionpar=np.loadtxt(BaselineCBF_relmot);
                
                f, axarr = plt.subplots(2, 2)
                axarr[0, 0].plot(motionpar[:,0:3]*180/np.pi)
                axarr[0, 0].set_title('rotations (deg)')
                axarr[0, 1].plot(motionpar[:,3:])
                axarr[0, 1].set_title('translations (mm)')
                axarr[1, 0].plot(absmotionpar)
                axarr[1, 0].set_title('Absolute RMS motion')
                axarr[1, 1].plot(relmotionpar)
                axarr[1, 1].set_title('Relative RMS motion')
                # Fine-tune figure; hide x ticks for top plots
                plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
                f.savefig(CBF_nii_root+'_motion.png')
            except:
                warnings.warn("Failed to generate image in cmind_baselineCBF_preprocess")

        BaselineCBF_nii=nib.load(CBF_nii_mcflirt +'.nii.gz')
        
        
        #nii_hdr=BaselineCBF_nii.get_header();
        #nii_affine=BaselineCBF_nii.get_affine();
        if UCLA_flag:  #if UCLA data, swap control/tag order to match UCLA and CCHMC protocols
            tmpImg=BaselineCBF_nii.get_data();
            newImg=np.zeros_like(tmpImg);  #empty array to hold swapped control/tag order
            newImg[:,:,:,1::2]=tmpImg[:,:,:,0::2];
            newImg[:,:,:,0::2]=tmpImg[:,:,:,1::2];  
            cmind_save_nii_mod_header(BaselineCBF_nii,newImg,CBF_nii_mcflirt + '.nii.gz')
            BaselineCBF_nii=nib.load(CBF_nii_mcflirt +'.nii.gz')
        
       
        BaselineCBF={}
        BaselineCBF['img'] = BaselineCBF_nii.get_data();
        BaselineCBF['control'] = BaselineCBF['img'][:,:,:,0::2]
        BaselineCBF['tag'] = BaselineCBF['img'][:,:,:,1::2]
        BaselineCBF['avgcontrol'] = np.mean(BaselineCBF['control'],3)
        BaselineCBF['diff'] = BaselineCBF['control']-BaselineCBF['tag']
        BaselineCBF['avgdiff'] = np.mean(BaselineCBF['diff'],3)
        BaselineCBF['stddiff'] = np.std(BaselineCBF['diff'],3,ddof=1)
       
        
        def _CBF_tSNR(meanvol, stdvol):  
            """ signal to fluctuation noise ratio (often referred to as SFNR or tSNR)
            """
            stdvol_copy=stdvol.copy() #copy to avoid modifying the original
            stdvol_copy[stdvol_copy==0]=np.Inf #set 0=np.Inf to avoid divide by zero warnings
            tSNR=meanvol/stdvol_copy
            tSNR[np.isnan(tSNR)]=0
            tSNR[np.isinf(tSNR)]=0
            return tSNR
        
        BaselineCBF['tSNR_orig']=_CBF_tSNR(BaselineCBF['avgdiff'],BaselineCBF['stddiff'])
        
        if generate_figures:
            try:
                from cmind.utils.montager import montager
                from cmind.utils.image_utils import mpl_white_on_black
                #max_tSNR=np.max(BaselineCBF['tSNR_orig'])
                p99_tSNR=np.percentile(BaselineCBF['tSNR_orig'],q=99.5)

                f=plt.figure()
                plt.imshow(montager(BaselineCBF['tSNR_orig'],flipy=True),
                           cmap=mpl.cm.gray,vmin=0,vmax=p99_tSNR)
                plt.title(r'Original tSNR: $99.5^{th}$%% = %0.4g' % p99_tSNR)
                plt.axis('off')
                mpl_white_on_black(f)
                f.savefig(CBF_nii_root+'_tSNR_orig.png', facecolor='black')
                plt.close(f)
            except:
                warnings.warn("Failed to save CBF tSNR figure")

        #new_hdr=nii_hdr.copy()
        #new_hdr.set_data_shape(BaselineCBF['avgcontrol'].shape)
        #new_BaselineCBF_nii=nib.Nifti1Image(BaselineCBF['avgcontrol'],nii_affine,new_hdr);
        #new_BaselineCBF_nii.to_filename('BaselineCBF_control_mcf_mean.nii.gz')
        cmind_save_nii_mod_header(BaselineCBF_nii,BaselineCBF['avgcontrol'],mean_control_root+'.nii.gz')
                
        log_cmd('%s -d 3 -i "%s" -o ["%s","%s"] -s 1 -b 200 -c [100x100x100x100, 1e-05]' 
                 % (ANTS_bias_cmd,mean_control_root+'.nii.gz',
                    BaselineCBF_mcf_control_mean_N4,
                    BaselineCBF_N4Bias), 
                    verbose=verbose, logger=module_logger)
                    
        CBF_brain_file=split_multiple_ext(BaselineCBF_mcf_control_mean_N4)[0]+'_brain.nii.gz'
        CBF_mask_file=split_multiple_ext(BaselineCBF_mcf_control_mean_N4)[0]+'_brain_mask.nii.gz'
        #use FSL's bet & fslmaths to create rough brain mask
        log_cmd('$FSLDIR/bin/bet "%s" "%s" -f 0.3 -g 0 -R -m' 
                % (BaselineCBF_mcf_control_mean_N4,CBF_brain_file), 
                verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mul "%s" "%s"'
                % (CBF_nii_mcflirt,CBF_mask_file,BaselineCBF_mcf_masked), 
                verbose=verbose, logger=logger)
                
        #[status out]=system('imrm BaselineCBF_mcf');
        mask_nii=nib.load(CBF_mask_file);
        BaselineCBF['mask']=mask_nii.get_data()>0
        
        BaselineCBF_nii=nib.load(BaselineCBF_mcf_masked);
        data_tmp=BaselineCBF_nii.get_data();
        BaselineCBF['diff_masked'] =  data_tmp[:,:,:,0::2]-data_tmp[:,:,:,1::2]
            
        if discard_outliers:   #Much more robust to motion if we discard outliers
            module_logger.info("Checking for outliers")
            
            if not fsl_motion_outliers_cmd:
                from cmind.globals import cmind_outliers_cmd as fsl_motion_outliers_cmd
            
            # First discard any really extreme outliers where the mean intensity 
            # difference over the full brain mask is greater than max_percent_diff%
            #   This threshold should never be reached in ASL unless there was 
            #   bulk head motion
            if max_percent_diff and (max_percent_diff<np.Inf):
                denom=BaselineCBF['avgcontrol']*BaselineCBF['mask']
                denom=np.mean(np.abs(denom));
                adiff=np.zeros((BaselineCBF['diff_masked'].shape[3],1))
                for nn in range(BaselineCBF['diff_masked'].shape[3]):
                    dframe=np.abs(BaselineCBF['diff_masked'][:,:,:,nn])
                    adiff[nn]=100*np.mean(dframe/np.float32(denom))
                
                t_keep1=np.where(adiff<max_percent_diff)[0]
            else:  #keep all points
                t_keep1=np.where(adiff<np.Inf)[0]
                
            #save the masked difference image, only for timepoints in t_keep1
            #outliers will then be detected from this volume
            
            diff_fname_out=CBF_nii_mcflirt+'_diff.nii.gz'
            cmind_save_nii_mod_header(BaselineCBF_nii,
                                      BaselineCBF['diff_masked'][:,:,:,t_keep1],
                                      diff_fname_out)
            
            outlier_file=pjoin(output_dir,'FSL_outliers.txt')
            metric_file=pjoin(output_dir,'outlier_metrics.txt')
            metric_image=pjoin(output_dir,'outlier_metrics.png')
            log_cmd('%s -i "%s" -o "%s" --mean_ref --nomoco  --nodiff_in_refrms -v  -s "%s" -p "%s"'
                    % (fsl_motion_outliers_cmd, diff_fname_out,
                       outlier_file,metric_file,metric_image), 
                       verbose=verbose, logger=module_logger)
                    
            if os.path.exists(outlier_file): #if no outliers found, this file will not exist
                out_txt=np.loadtxt(outlier_file)
                if out_txt.ndim>1:
                    out_txt=np.sum(out_txt,1)
                timepoints_keep=t_keep1[np.where(out_txt==0)[0]]  #indices here are for t_keep1
                os.remove(outlier_file)
            else:  #no outliers found
                timepoints_keep=np.arange(t_keep1.shape[0])
                
            BaselineCBF['avgdiff'] = np.mean(BaselineCBF['diff'][:,:,:,timepoints_keep],3);
            BaselineCBF['stddiff'] = np.std(BaselineCBF['diff'][:,:,:,timepoints_keep],3,ddof=1)
            BaselineCBF['tSNR_cor']=_CBF_tSNR(BaselineCBF['avgdiff'],BaselineCBF['stddiff'])
            p99_tSNR=np.percentile(BaselineCBF['tSNR_cor'],q=99.5)
            BaselineCBF['p99_tSNR']=p99_tSNR
            BaselineCBF['diff_masked_nooutliers']=BaselineCBF['diff_masked'][:,:,:,timepoints_keep]
            Npairs_kept=timepoints_keep.shape[0]
            Noutliers = BaselineCBF['diff'].shape[-1]-Npairs_kept
            BaselineCBF['Noutliers']=Noutliers
            
            if generate_figures: # and (Noutliers>0):
                try:
                    #max_tSNR=np.max(BaselineCBF['tSNR_cor'])
                    f=plt.figure()
                    plt.imshow(montager(BaselineCBF['tSNR_cor'],flipy=True),
                               cmap=mpl.cm.gray,vmin=0,vmax=p99_tSNR)
                    plt.title(r'tSNR (%d outliers removed): $99.5^{th}$%% = %0.4g' 
                              % (Noutliers, p99_tSNR))
                    plt.axis('off')
                    mpl_white_on_black(f)
                    f.savefig(CBF_nii_root+'_tSNR_cor.png', facecolor='black')
                    plt.close(f)
                except:
                    warnings.warn("Failed to save CBF tSNR figure")

            #update the average control images to only keep the non-outliers
            BaselineCBF['avgcontrol'] = np.mean(BaselineCBF['control'][:,:,:,timepoints_keep],3);
            cmind_save_nii_mod_header(BaselineCBF_nii,BaselineCBF['control'][:,:,:,timepoints_keep],control_root+'.nii.gz')
        else:  #no outlier-discards
            timepoints_keep=np.arange(BaselineCBF['diff_masked'].shape[-1])
            Npairs_kept=timepoints_keep.shape[0]
            Noutliers = 0
            
        fo=open(BaselineCBF_Npairs_kept,'w');
        fo.write("%d\n" % (Npairs_kept));
        fo.close()
        np.savetxt(BaselineCBF_timepoints_kept,timepoints_keep,fmt='%d');
        
        if discard_outliers:
            module_logger.info("Discarded %d outliers" % Noutliers)

            BaselineCBF['timepoints_keep']=timepoints_keep
            #resave BaselineCBF['diff_masked'] with only the "good" subtraction timepoints
            BaselineCBF['diff_masked']=BaselineCBF['diff_masked'][:,:,:,timepoints_keep]
            cmind_save_nii_mod_header(BaselineCBF_nii,
                                          BaselineCBF['diff_masked'],
                                          diff_fname_out)
            
        #apply N4 bias correction field to remove non-uniform coil sensitivity
        N4=nib.load(BaselineCBF_N4Bias); 
        N4=N4.get_data();
        MeanSubN4=BaselineCBF['avgdiff']/N4;
        cmind_save_nii_mod_header(BaselineCBF_nii,MeanSubN4,Meansub_BaselineCBF)
        
        #save an image of the percent signal change:  100*(tag-control)/control
        maskim=BaselineCBF['mask']
        BaselineCBF['percent_change']=100*(BaselineCBF['avgdiff']/np.float32(BaselineCBF['avgcontrol']))*maskim;
        BaselineCBF['percent_change'][np.isinf(BaselineCBF['percent_change'])]=0
        BaselineCBF['percent_change'][np.isnan(BaselineCBF['percent_change'])]=0
        cmind_save_nii_mod_header(BaselineCBF_nii,BaselineCBF['percent_change'],BaselineCBF_mcf_percent_change)
        
        save_pickle=False
        if save_pickle:
            try:  #save full BaselineCBF structure out to disk
                import cPickle as pickle
                with open(pjoin(output_dir,"BaselineCBF.pickle"), "wb") as f:   
                    pickle.dump(BaselineCBF,f)
            except:
                warnings.warn("Failed to write BaselineCBF.pickle")
                   
        #generate images of the mean subtraction and percentage signal change    
        if generate_figures:
            try:
                pct_img=BaselineCBF_mcf_percent_change.replace('.nii.gz','.png')
                three_plane_img=BaselineCBF_mcf_percent_change.replace('.nii.gz','_3plane.png')
                if not os.path.exists(pct_img) or ForceUpdate:
                    log_cmd('$FSLDIR/bin/slicer "%s" -i -1 1.5 -a "%s"'
                            % (BaselineCBF_mcf_percent_change,three_plane_img), 
                            verbose=verbose, logger=logger)
                    log_cmd('$FSLDIR/bin/slicer "%s" -i -1 1.5 -s 1 -A 400 "%s"'
                            % (BaselineCBF_mcf_percent_change,pct_img), 
                            verbose=verbose, logger=logger)
                meansub_img=Meansub_BaselineCBF.replace('.nii.gz','.png')
                if not os.path.exists(meansub_img) or ForceUpdate:
                    log_cmd('$FSLDIR/bin/slicer "%s" -s 1 -A 400 "%s"'
                            % (Meansub_BaselineCBF,meansub_img), 
                            verbose=verbose, logger=logger)
            except:
                warnings.warn("Failed to generate image in cmind_baselineCBF_preprocess")
    else:
        module_logger.info("Existing outputs found.  Skipping BaselineCBF preprocessing...")
    
    
    #print any filename outputs for capture by LONI
    print("BaselineCBF_mcf_percent_change:{}".format(BaselineCBF_mcf_percent_change))
    print("BaselineCBF_mcf_control_mean_N4:{}".format(BaselineCBF_mcf_control_mean_N4))
    print("Meansub_BaselineCBF:{}".format(Meansub_BaselineCBF))
    print("BaselineCBF_timepoints_kept:{}".format(BaselineCBF_timepoints_kept))
    print("BaselineCBF_mcf_masked:{}".format(BaselineCBF_mcf_masked))
    print("BaselineCBF_N4Bias:{}".format(BaselineCBF_N4Bias))
    print("BaselineCBF_relmot:{}".format(BaselineCBF_relmot))
    return (BaselineCBF_mcf_percent_change,
            BaselineCBF_mcf_control_mean_N4,
            Meansub_BaselineCBF,
            BaselineCBF_timepoints_kept,
            BaselineCBF_mcf_masked,
            BaselineCBF_N4Bias,
            BaselineCBF_relmot)


def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import (cmind_example_dir,cmind_example_output_dir,
                                cmind_ANTs_dir,cmind_shellscript_dir)
    output_dir=pjoin(cmind_example_output_dir,'P','BaselineCBF')
    CBF_nii=pjoin(cmind_example_dir,'IRC04H_06M008_P_1_WIP_BaselineCBF_SENSE_8_1.nii.gz')
    ANTS_bias_cmd=pjoin(cmind_ANTs_dir,'N4BiasFieldCorrection')
    max_percent_diff=3.0
    UCLA_flag=False
    discard_outliers=True
    generate_figures=True
    ForceUpdate=True;
    verbose=True
    fsl_motion_outliers_cmd=pjoin(cmind_shellscript_dir,'fsl_motion_outliers_v2')
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',
                        file_mode='w')  
    if verbose:
        func=cmind_timer(cmind_baselineCBF_preprocess,logger=logger)
    else:
        func=cmind_baselineCBF_preprocess
        
    func(output_dir,
         CBF_nii,
         max_percent_diff=max_percent_diff,
         ANTS_bias_cmd=ANTS_bias_cmd,
         UCLA_flag=UCLA_flag,
         fsl_motion_outliers_cmd=fsl_motion_outliers_cmd,
         discard_outliers=discard_outliers,
         generate_figures=generate_figures,
         ForceUpdate=ForceUpdate,
         verbose=verbose,
         logger=logger)
                
                
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer
    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="BaselineCBF preprocessing (motion correction and outlier rejection)", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-cbf","--CBF_nii", required=True, help="nifti image containing the 4D ASL timeseries")
        parser.add_argument("-a","-ants","--ANTS_bias_cmd", help="path to the bias field correction command to use. if None, assumes /usr/lib/ants/N4BiasFieldCorrection")
        parser.add_argument("-ucla","--UCLA_flag", type=str, help="if True, assumes order is tag,control,tag,...if False, assumes order is control,tag,control...if true, generate summary images")
        parser.add_argument("--max_percent_diff", type=float, default=3.0, help="if True, assumes order is tag,control,tag,...if False, assumes order is control,tag,control...if true, generate summary images")
        parser.add_argument("--fsl_motion_outliers_cmd", type=str, default='default', help="path to the fsl_motion_outliers_cmd_v2 shell script.  If not specified will try to guess it relative to the python script location.")
        parser.add_argument("--discard_outliers",type=str,default="True", help="unless False, detect and discard outliers")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        output_dir=args.output_dir
        CBF_nii=args.CBF_nii
        ANTS_bias_cmd=args.ANTS_bias_cmd
        UCLA_flag=args.UCLA_flag
        fsl_motion_outliers_cmd=str2none(args.fsl_motion_outliers_cmd, none_strings=['none','default'])
        discard_outliers=args.discard_outliers
        generate_figures=args.generate_figures
        ForceUpdate=args.ForceUpdate
        verbose=args.verbose
        logger=args.logger
        max_percent_diff=args.max_percent_diff
    
        if verbose:
            cmind_baselineCBF_preprocess=cmind_timer(cmind_baselineCBF_preprocess,
                                                     logger=logger)
    
        cmind_baselineCBF_preprocess(output_dir,
                                     CBF_nii,
                                     max_percent_diff=max_percent_diff,
                                     ANTS_bias_cmd=ANTS_bias_cmd,
                                     UCLA_flag=UCLA_flag,
                                     fsl_motion_outliers_cmd=fsl_motion_outliers_cmd,
                                     discard_outliers=discard_outliers,
                                     generate_figures=generate_figures,
                                     ForceUpdate=ForceUpdate,
                                     verbose=verbose,
                                     logger=logger)
                
