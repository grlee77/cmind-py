#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_fieldmap_regBBR(output_dir,lowres_vol,reg_struct_file,T1_vol,T1_brain_vol,WMseg_vol,use_bbr=True,bbr_schedule_file=None,Fieldmap_vol_processed=None,Fieldmap_vol_unmasked=None,fieldmap2struct_affine=None,unwarpdir=None, EPI_echo_spacing=None, SENSE_factor=None, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """coregister functional images to a structural image, optionally using a
    fieldmap to correct distortions along the phase encoding direction
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    lowres_vol : str
        image filename for the "moving" image
    reg_struct_file : str
        filename of the existing .csv registration structure
    T1_vol : str
        filename of the "fixed" head image  (before brain extraction)
    T1_brain_vol : str
        filename of the "fixed" brain image
    WMseg_vol : str
        filename of the white matter partial volume estimate image
    use_bbr : bool, optional
        use BBR registration for FLIRT
    bbr_schedule_file : str, optional
        filename of flirt schedule to use  (e.g. $FSLDIR/etc/flirtsch/bbr.sch)
    Fieldmap_vol_processed : str or None, optional
        filename of the fieldmap volume
    Fieldmap_vol_unmasked : str or None, optional
        filename of the unmasked fieldmap volume
    fieldmap2struct_affine : str or None, optional
        filename of the affine transform of the fieldmap to structural space
    unwarpdir : {'x','y','z','x-','y-','z-'}, optional
        phase encoding direction during the readout
    EPI_echo_spacing : float, optional
        EPI echo spacing (seconds)
    SENSE_factor : float, optional
        SENSE acceleration factor       
    generate_figures : bool,optional
        if true, generate overlay images summarizing the registration
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    LRtoHR_Affine_fname : str or None
        If no fieldmap is used, this is the transform from lowres->structural
    HRtoLR_Affine_fname : str or None
        If no fieldmap is used, this is the transform from structural->lowres
    LRtoHR_Warp_fname : str or None
        If a fieldmap is used, this is the warp from lowres->structural  
    HRtoLR_Warp_fname : str or None
        If a fieldmap is used, this is the warp from structural->lowres
    regBBR_dir : str
        directory where the registration outputs were stored (= output_dir input)
    
    See Also
    --------
    cmind_fieldmap_process
    
    Notes
    -----
    run boundary based registration (BBR) via FSL's mcflirt. optionally uses
    fugue to do fieldmap unwarping. 
    This processing stream is based on FSL's epi_reg
    
     Notes
    -----
    
    .. figure::  ../img/IRC04H_06M008_P_1_wANTS_CBF_Wang2002.png
       :align:   center
       :scale:   100%
       
       quantitative CBF volume (top row) registered to MNI standard space (bottom row)
       
    .. figure::  ../img/06F010_P_BaselineCBF_regBBR_Fieldmap_Example.png
       :align:   center
       :scale:   100%
       
       Example of a subject where the registration is substantial improved by fieldmap correction

    """

    import os
    from os.path import join as pjoin    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict
    from cmind.utils.cmind_utils import cmind_reg_report_img, cmind_lowres_to_T1
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    
            
    #convert various strings to boolean
    use_bbr, generate_figures, ForceUpdate, verbose = input2bool([use_bbr, generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting regBBR boundary-based registration")        
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir);
    
    if (not bbr_schedule_file) or (isinstance(bbr_schedule_file,str) and bbr_schedule_file.lower()=='default'):
        from cmind.globals import cmind_FSL_dir    
        bbr_schedule_file=pjoin(cmind_FSL_dir,'etc','flirtsch','bbr.sch')
        #bbr_schedule_file=log_cmd('echo "$FSLDIR/etc/flirtsch/bbr.sch"', verbose=verbose, logger=logger).strip()

    if not os.path.exists(bbr_schedule_file):
        raise IOError("specified bbr_schedule_file, %s doesn't exist!" % bbr_schedule_file)
        
    if not os.path.exists(reg_struct_file):
        raise IOError("Missing reg_struct_file, %s" % (reg_struct_file))

    
    #[pth,CBF_root,ext]=fileparts(CBF_nii);
    #CBF_root=strrep(CBF_root,'.nii','');  %if it was .nii.gz, still need to remove the .nii
    
    
    if ((not Fieldmap_vol_processed) or (isinstance(Fieldmap_vol_processed,str) and Fieldmap_vol_processed.lower()=='none')):
        use_fieldmap=False
    else:
        use_fieldmap=True

    (imexists, WMseg_vol_full)=imexist(WMseg_vol,'nifti-1')[0:2];
    if not imexists:
        raise IOError("specified WMseg_vol, {}, not found!".format(WMseg_vol))
    else:
        WMseg_vol=WMseg_vol_full
    
    (imexists, T1_vol_full)=imexist(T1_vol,'nifti-1')[0:2];
    if not imexists:
        raise IOError("specified T1_vol, {}, not found!".format(T1_vol))
    else:
        T1_vol=T1_vol_full
        
    (imexists, lowres_vol_full)=imexist(lowres_vol,'nifti-1')[0:2];
    if not imexists:
        raise IOError("specified lowres_vol, {}, not found!".format(lowres_vol))
    else:
        lowres_vol=lowres_vol_full
    
       
    if use_fieldmap:   
        fieldmap_dict={}

        (imexists,Fieldmap_vol_processed)=imexist(Fieldmap_vol_processed,'nifti-1')[0:2];
        if not imexists:
            raise IOError("specified Fieldmap_vol_processed found!")

        (imexists,Fieldmap_vol_unmasked)=imexist(Fieldmap_vol_unmasked,'nifti-1')[0:2];
        if not imexists:
            raise IOError("specified Fieldmap_vol_unmasked found!")

        if not os.path.exists(fieldmap2struct_affine):
            raise IOError("specified fieldmap2struct_affine found!")
        fieldmap2struct_affine=os.path.abspath(fieldmap2struct_affine)
        
        if not unwarpdir:
            raise ValueError("must specify unwarpdir")
        if not EPI_echo_spacing:
            raise ValueError("must specify EPI_echo_spacing")
        if not SENSE_factor:
            raise ValueError("must specify SENSE_factor")
        EPI_echo_spacing=float(EPI_echo_spacing)    
        SENSE_factor=float(SENSE_factor)
        dwell = EPI_echo_spacing/SENSE_factor
        
        
    #RUN BBR WITH FIELDMAP
    vout=pjoin(output_dir,'lowres2highres')
    
    if use_fieldmap:
        img_out=pjoin(output_dir,'lowres2highres_fmap.png')
    else:
        img_out=pjoin(output_dir,'lowres2highres.png')
    
    
    if use_fieldmap:
        LRtoHR_Affine_fname=None
        HRtoLR_Affine_fname=None
        LRtoHR_Warp_fname=pjoin(output_dir,'lowres2highres_warp.nii.gz')
        HRtoLR_Warp_fname=pjoin(output_dir,'lowres2highres_warp_inv.nii.gz')
        previous_output_exists=os.path.exists(HRtoLR_Warp_fname)
    else:
        LRtoHR_Affine_fname=pjoin(output_dir,'lowres2highres.mat')
        HRtoLR_Affine_fname=pjoin(output_dir,'lowres2highres_inv.mat')
        previous_output_exists=os.path.exists(HRtoLR_Affine_fname)
        LRtoHR_Warp_fname=None
        HRtoLR_Warp_fname=None
    
    if (not previous_output_exists) or ForceUpdate:
        
        reg_struct=csv2dict(reg_struct_file)
        
        standard_vol="" #; %reg_struct.MNI_standard;
        warp_highres2std="" # = [];  %TODO: add FNIRT transform here?
        
        
        if use_fieldmap:
            unwarpdir=unwarpdir.lower()  #allow case-insensitive match
            if unwarpdir=='x':
                pe_dir=1;
            elif unwarpdir=='y':
                pe_dir=2;
            elif unwarpdir=='z':
                pe_dir=3;
            elif unwarpdir=='x-' or unwarpdir=='-x':
                pe_dir=-1;
            elif unwarpdir=='y-' or unwarpdir=='-y':
                pe_dir=-2;
            elif unwarpdir=='z-' or unwarpdir=='-z':
                pe_dir=-3;
            else:
                raise ValueError("Invalid unwarpdir, %s" % unwarpdir)
                
            fieldmap_dict['Fieldmap_vol_processed']=Fieldmap_vol_processed
            fieldmap_dict['Fieldmap_vol_unmasked']=Fieldmap_vol_unmasked
            fieldmap_dict['dwell']=dwell
            fieldmap_dict['unwarpdir']=unwarpdir
            fieldmap_dict['pe_dir']=pe_dir
            fieldmap_dict['EPI_echo_spacing']=EPI_echo_spacing
            fieldmap_dict['SENSE_factor']=SENSE_factor
                
            #GRL: constrain to +/-20 degree rotations
            tight_angle_range_flag=True
            cmind_lowres_to_T1(reg_struct,lowres_vol,T1_vol,T1_brain_vol, WMseg_vol,output_dir,bbr_schedule_file,standard_vol,warp_highres2std,use_bbr,tight_angle_range_flag,fieldmap_dict)
            log_cmd('$FSLDIR/bin/convert_xfm -omat %s_inv.mat -inverse %s.mat' % (vout,vout), verbose=verbose, logger=module_logger);
            log_cmd('$FSLDIR/bin/convert_xfm -omat %s_fieldmaprads2epi.mat -concat %s_inv.mat %s' % (vout,vout,fieldmap2struct_affine), verbose=verbose, logger=module_logger);
            log_cmd('$FSLDIR/bin/applywarp -i %s -r %s --premat=%s_fieldmaprads2epi.mat -o %s_fieldmaprads2epi' % (Fieldmap_vol_unmasked,lowres_vol,vout,vout), verbose=verbose, logger=module_logger);
            log_cmd('$FSLDIR/bin/fslmaths %s_fieldmaprads2epi -abs -bin %s_fieldmaprads2epi_mask' % (vout,vout), verbose=verbose, logger=module_logger);
            log_cmd('$FSLDIR/bin/fugue --loadfmap=%s_fieldmaprads2epi --mask=%s_fieldmaprads2epi_mask --saveshift=%s_fieldmaprads2epi_shift --unmaskshift --dwell=%f --unwarpdir=%s' % (vout,vout,vout,dwell,unwarpdir), verbose=verbose, logger=module_logger);
            #log_cmd('$FSLDIR/bin/convertwarp -r %s -s %s_fieldmaprads2epi_shift --postmat=%s.mat -o %s_warp --shiftdir=%s --absout' % (T1_vol,vout,vout,vout,unwarpdir), verbose=verbose, logger=module_logger);
            #log_cmd('$FSLDIR/bin/applywarp -i %s -r %s -o %s -w %s_warp --interp=spline --abs' % (lowres_vol,T1_vol,vout,vout), verbose=verbose, logger=module_logger);
            log_cmd('$FSLDIR/bin/convertwarp -r %s -s %s_fieldmaprads2epi_shift --postmat=%s.mat -o %s_warp --shiftdir=%s --relout' % (T1_vol,vout,vout,vout,unwarpdir), verbose=verbose, logger=module_logger);  #--premat or --postmat seems to give the same result
            log_cmd('$FSLDIR/bin/applywarp -i %s -r %s -o %s -w %s_warp --interp=spline --rel' % (lowres_vol,T1_vol,vout,vout), verbose=verbose, logger=module_logger);
            
            #also compute the inverse warp
            log_cmd('$FSLDIR/bin/invwarp -w %s_warp -o %s_warp_inv -r %s  --rel' % (vout,vout,lowres_vol), verbose=verbose, logger=module_logger);
            log_cmd('$FSLDIR/bin/applywarp -i %s -r %s -o %s_inv -w %s_warp_inv --interp=spline --rel' % (T1_vol, lowres_vol,vout,vout), verbose=verbose, logger=module_logger);
            
            #eval(sprintf('!flirt -ref %s -in %s -dof 6 -omat T1Reg_init.mat  -out %s_1vol_init -searchrx -20 20 -searchry -20 20 -searchrz -20 20',T1_vol,lowres_vol,vout))
            #wopt=''  % wopt='-refweight $refweight'
            #eval(sprintf('!flirt -ref %s -in %s -dof 6 -cost bbr -wmseg %s -init T1Reg_init.mat -omat %s.mat -out %s_1vol -schedule $[FSLDIR]/etc/flirtsch/bbr.sch -echospacing %f -pedir %d -fieldmap %s %s',T1_vol,lowres_vol,WMseg_vol,vout,vout,dwell,pe_dir,Fieldmap_vol_processed,wopt))
            
            
            if generate_figures:
                input_vol1 = pjoin(output_dir, vout);
                input_vol2 = T1_brain_vol;
                output_file = pjoin(output_dir, img_out);
                
                cmind_reg_report_img(input_vol1,input_vol2, output_file);   #Create image of BBR registration
                
                #make overlay for the inverse warp as well
                input_vol1 = pjoin(output_dir, vout + '_inv');
                input_vol2 = lowres_vol
                output_file = pjoin(output_dir, img_out.replace('.png','_inv.png'));
                cmind_reg_report_img(input_vol1,input_vol2, output_file);   #Create image of BBR registration
                
            do_MNI_reg = ('FLIRT_HR_to_MNI_Aff' in reg_struct and 'MNI_standard_brain' in reg_struct)
            if do_MNI_reg:
                LRtoMNI_warp_fname=pjoin(output_dir,'lowres2MNI_warp')
                LRtoMNI_fname=pjoin(output_dir,'lowres2MNI')
                log_cmd('$FSLDIR/bin/convertwarp -r %s -s %s_fieldmaprads2epi_shift --premat=%s.mat --postmat=%s -o %s --shiftdir=%s --relout' % (reg_struct['MNI_standard_brain'],vout,vout,reg_struct['FLIRT_HR_to_MNI_Aff'],LRtoMNI_warp_fname,unwarpdir), verbose=verbose, logger=module_logger);  #--premat or --postmat seems to give the same result
                log_cmd('$FSLDIR/bin/applywarp -i %s -r %s -o %s -w %s --interp=spline --rel' % (lowres_vol,reg_struct['MNI_standard_brain'],LRtoMNI_fname,LRtoMNI_warp_fname), verbose=verbose, logger=module_logger);
                    
            if generate_figures and do_MNI_reg:
                input_vol1 = pjoin(output_dir, 'lowres2MNI.nii.gz');
                input_vol2 = reg_struct['MNI_standard_brain'];
                output_file_MNI = pjoin(output_dir, 'lowres2MNI.png');
                cmind_reg_report_img(input_vol1,input_vol2, output_file_MNI);   #Create image of BBR registration
    
        else:
            cmind_lowres_to_T1(reg_struct,lowres_vol,T1_vol,T1_brain_vol, WMseg_vol,output_dir,bbr_schedule_file,standard_vol,warp_highres2std,use_bbr,tight_angle_range_flag=True)

            #also compute the inverse transform from highres to lowres
            log_cmd('$FSLDIR/bin/convert_xfm -inverse -omat %s_inv.mat %s.mat' % (vout,vout), verbose=verbose, logger=module_logger);
        
            if (not imexist(vout + '_inv','nifti-1')[0]) or ForceUpdate:
                log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o %s_inv --premat=%s_inv.mat --interp=spline' % (T1_brain_vol, lowres_vol, vout, vout), verbose=verbose, logger=module_logger);
                if generate_figures:
                    input_vol1 = imexist(pjoin(output_dir,vout +"_inv"),'nifti-1')[1];
                    output_file = pjoin(output_dir,vout +'_inv.png');
                    cmind_reg_report_img(input_vol1,lowres_vol, output_file);  
            
            if generate_figures:
                HRvol= imexist(T1_brain_vol,'nifti-1')[1];
                input_vol1 = imexist(pjoin(output_dir,vout),'nifti-1')[1];
                output_file = pjoin(output_dir,vout + '.png');
                cmind_reg_report_img(input_vol1,HRvol, output_file); 
                #input_vol1 = imexist(pjoin(output_dir,'lowres2standard'),'nifti-1')[1];
                #output_file = pjoin(output_dir,'lowres2standard.png');
                #cmind_reg_report_img(pjoin(output_dir,'lowres2standard'),reg_struct.MNI_standard_brain, output_file, working_dir); 

    writeHTML=True;
    if writeHTML:  #TODO?:  modify cmind_HTML_reports.m for this case?
        from cmind.utils.cmind_HTML_report_gen import cmind_HTML_report_gen

        report_dir=pjoin(output_dir,'report');
        lowres_root=imexist(lowres_vol,'nifti-1')[2];
        report_fname=lowres_root + '_to_T1_Registration_Report';
        report_title=lowres_root + ' to T1 Registration';
        #fnames=[pjoin(output_dir,vout+'.png')];
        fnames=[pjoin(output_dir,img_out)];
        titles=['Registration of ' + lowres_root + ' to T1 Structural'];
                #'Registration of T1 map across TI values'];
        move_flag=False
        output_fname=pjoin(report_dir,'%s.html' % report_fname);
        if not os.path.exists(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles,move_flag);
        else:
            module_logger.info("HTML report found...skipping")
                    
    return (LRtoHR_Affine_fname, HRtoLR_Affine_fname, LRtoHR_Warp_fname, HRtoLR_Warp_fname, output_dir)   
    
    
def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_FSL_dir
    
    test_cases=['DTI','HARDI','CBF','Stories','Sentences']
    for test_case in test_cases:
        if test_case=='CBF':
            root_dir=pjoin(cmind_example_output_dir,'P')
            output_dir=pjoin(root_dir,'BaselineCBF','reg_BBR_fmap')
            lowres_vol=pjoin(root_dir,'BaselineCBF','MeanSub_BaselineCBF_N4.nii.gz')       
            EPI_echo_spacing=0.00055
            SENSE_factor=2.0
        elif test_case=='Stories': 
            root_dir=pjoin(cmind_example_output_dir,'F')
            fmap_dir=pjoin(root_dir,'PhilipsGRE')
            output_dir=pjoin(root_dir,'ASLBOLD_Stories','regBBR')
            lowres_vol=pjoin(root_dir,'ASLBOLD_Stories','MeanSub_ASL_Stories_mcf_N4.nii.gz')       
            EPI_echo_spacing=0.00055
            SENSE_factor=2.0
        elif test_case=='Sentences': 
            root_dir=pjoin(cmind_example_output_dir,'F')            
            output_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_Sentences','regBBR')
            lowres_vol=pjoin(cmind_example_output_dir,'F','ASLBOLD_Sentences','MeanSub_ASL_Sentences_mcf_N4.nii.gz')       
            EPI_echo_spacing=0.00055
            SENSE_factor=2.0
        elif test_case=='HARDI': 
            root_dir=pjoin(cmind_example_output_dir,'F')            
            output_dir=pjoin(cmind_example_output_dir,'F','HARDI','regBBR')
            lowres_vol=pjoin(cmind_example_output_dir,'F','HARDI','b0avg.nii.gz')       
            EPI_echo_spacing=0.00069
            SENSE_factor=3.0
        elif test_case=='DTI': 
            root_dir=pjoin(cmind_example_output_dir,'P')            
            output_dir=pjoin(cmind_example_output_dir,'P','DTI','regBBR')
            lowres_vol=pjoin(cmind_example_output_dir,'P','DTI','b0avg.nii.gz')       
            EPI_echo_spacing=0.00069
            SENSE_factor=3.0
        else:
            raise ValueError("Invalid test case: {}".format(test_case))
            
        reg_struct_file=pjoin(root_dir,'T1_proc','reg_struct.csv')
        T1_vol=pjoin(root_dir,'T1_proc','T1W_Structural_N4.nii.gz')
        T1_brain_vol=pjoin(root_dir,'T1_proc','T1W_Structural_N4_brain.nii.gz')
        WMseg_vol=pjoin(root_dir,'T1_proc','T1Segment_WM.nii.gz')
        bbr_schedule_file=pjoin(cmind_FSL_dir,'etc','flirtsch','bbr.sch')
        generate_figures=True
        fmap_dir=pjoin(root_dir,'PhilipsGRE')
        Fieldmap_vol_processed=pjoin(fmap_dir,'Fieldmap_medianfilt.nii.gz')
        Fieldmap_vol_unmasked=pjoin(fmap_dir,'fieldmaprads_unmasked.nii.gz')
        fieldmap2struct_affine=pjoin(fmap_dir,'fieldmap2struct.mat')
        unwarpdir="y"
        ForceUpdate=False;
        verbose=True
        logger=cmind_logger(log_level_console='INFO',
                            logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                            log_level_file='DEBUG',
                            file_mode='w')
        
        if verbose:
            func=cmind_timer(cmind_fieldmap_regBBR,logger=logger)
        else:
            func=cmind_fieldmap_regBBR
    
        func(output_dir,
             lowres_vol,
             reg_struct_file,
             T1_vol,
             T1_brain_vol,
             WMseg_vol,
             use_bbr=True,
             bbr_schedule_file=bbr_schedule_file, 
             Fieldmap_vol_processed=Fieldmap_vol_processed,
             Fieldmap_vol_unmasked=Fieldmap_vol_unmasked,
             fieldmap2struct_affine=fieldmap2struct_affine,
             unwarpdir=unwarpdir, 
             EPI_echo_spacing=EPI_echo_spacing, 
             SENSE_factor=SENSE_factor,
             generate_figures=generate_figures,
             ForceUpdate=ForceUpdate,
             verbose=verbose,
             logger=logger)
        
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv)==2 and sys.argv[1]=='test':
        _testme()
    else:      
        parser = argparse.ArgumentParser(description="coregister functional images to a structural image, optionally using a fieldmap to correct distortions along the phase encoding direction", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True,help="directory in which to store the output")
        parser.add_argument("-lrvol","--lowres_vol",required=True,help='image filename for the "moving" image')
        parser.add_argument("-rsf","--reg_struct_file",required=True,help="filename of the existing .csv registration structure")
        parser.add_argument("-head","-t1vol","--T1_vol",required=True,help="filename of the 'fixed' head image  (before brain extraction)")
        parser.add_argument("-brain","-t1bvol","--T1_brain_vol",required=True,help="filename of the 'fixed' brain image")
        parser.add_argument("-wm","-wmvol","--WMseg_vol",required=True,help="filename of the white matter partial volume estimate image")
        parser.add_argument("-ub","--use_bbr",type=str,default="True",help="use BBR registration for FLIRT")
        parser.add_argument("-bbrf","--bbr_schedule_file",type=str,default="none",help="filename of flirt schedule to use  (e.g. $FSLDIR/etc/flirtsch/bbr.sch)")
        parser.add_argument("-fvolp","--Fieldmap_vol_processed",type=str,default="none",help="filename of the fieldmap volume")
        parser.add_argument("-fvolu","--Fieldmap_vol_unmasked",type=str,default="none",help="filename of the unmasked fieldmap volume")
        parser.add_argument("-fmapaff","--fieldmap2struct_affine",type=str,default="none",help="filename of the affine transform of the fieldmap to structural space")
        parser.add_argument("-unw","--unwarpdir",type=str,default='y',help="{'x','y','z','x-','y-','z-'} phase encoding direction during the readout")
        parser.add_argument("-epies","--EPI_echo_spacing",type=float,default=0.00055,help="EPI echo spacing (seconds)")
        parser.add_argument("-sense","--SENSE_factor",type=float,default=2.0,help="SENSE acceleration factor")
        parser.add_argument("-gf","--generate_figures",type=str,default="True",help="if true, generate overlay images summarizing the registration")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False",help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose",help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger',type=str, default="",help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        output_dir=args.output_dir
        lowres_vol=args.lowres_vol
        reg_struct_file=args.reg_struct_file
        T1_vol=args.T1_vol
        T1_brain_vol=args.T1_brain_vol
        WMseg_vol=args.WMseg_vol
        use_bbr=args.use_bbr
        bbr_schedule_file=str2none(args.bbr_schedule_file)
        generate_figures=args.generate_figures
        ForceUpdate=args.ForceUpdate
        Fieldmap_vol_processed=str2none(args.Fieldmap_vol_processed)
        Fieldmap_vol_unmasked=str2none(args.Fieldmap_vol_unmasked)
        fieldmap2struct_affine=str2none(args.fieldmap2struct_affine)
        unwarpdir=args.unwarpdir
        EPI_echo_spacing=args.EPI_echo_spacing
        SENSE_factor=args.SENSE_factor
        verbose=args.verbose
        logger=args.logger

        if verbose:
            cmind_fieldmap_regBBR=cmind_timer(cmind_fieldmap_regBBR,logger=logger)
    
        cmind_fieldmap_regBBR(output_dir,
                              lowres_vol,
                              reg_struct_file,
                              T1_vol,
                              T1_brain_vol,
                              WMseg_vol,
                              use_bbr=use_bbr,
                              bbr_schedule_file=bbr_schedule_file, 
                              Fieldmap_vol_processed=Fieldmap_vol_processed,
                              Fieldmap_vol_unmasked=Fieldmap_vol_unmasked,
                              fieldmap2struct_affine=fieldmap2struct_affine,
                              unwarpdir=unwarpdir, 
                              EPI_echo_spacing=EPI_echo_spacing, 
                              SENSE_factor=SENSE_factor,
                              generate_figures=generate_figures,
                              ForceUpdate=ForceUpdate,
                              verbose=verbose,
                              logger=logger)
