#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

            
def cmind_deface(input_nii, output_nii=None, ped_template_path=None, age_months=252, bias_cor=False, keep_intermediate_files=False, iso_xfm_mm=None, no_crop=False, crop_thresh=0.03, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):  #, iterate_segmentation=True, Nsegmentation_iters=1
    """Structural volume preprocessing pipeline
    
    Parameters
    ----------
    input_nii : str
        filename of NIFTI or NIFTIGZ volume to process.  This should be a head 
        volume prior to brain extraction.
    output_nii : str, optional
        defaced output filename.  default is to append "_defaced" to the input filename
    ped_template_path : str, optional
        path to the directory containing the pediatric templates
    age_months : float, optional
        subject age in months  (used during cropping)
    bias_cor : bool, optional
        if True, bias correct prior to registration.  The masked output volume
        will still be without bias correction, though.
    keep_intermediate_files : bool, optional
        if True, all intermediate files/transforms will be retained
    iso_xfm_mm : float, optional
        if specified, will use FSL FLIRT -applyisoxfm to adjust resolution before
        doing the registration.  setting this to 2 mm can speed up processing of
        high resolution volumes
    no_crop : bool, optional
        if False, do not attempt to crop z FOV first
    crop_thresh : float, optional
        percentile threshold to use during cropping.  range [0,1],  default = 0.03
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    fname_defaced : str
        head volume with face removed
       
    See Also
    --------
    cmind_crop_robust.py, cmind_bias_correct.py, cmind_brain_extract.py, cmind_structural.py
    
    Notes
    -----
    
    This function is a wrapper that calls other structural processing routines in the following order:
        - cmind_crop_robust.py
        - cmind_bias_correct.py
        - cmind_brain_extract.py
        - cmind_structural.py (optional)
        
    """

    import os, shutil, tempfile
    
    from cmind.globals import cmind_ANTs_dir
    from cmind.pipeline.cmind_crop_robust import cmind_crop_robust
    #from cmind.pipeline.cmind_brain_extract import cmind_brain_extract
    #from cmind.pipeline.cmind_bias_correct import cmind_bias_correct
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist
    from cmind.utils.image_utils import slicer_mid_sag_fig
    from cmind.utils.logging_utils import cmind_init_logging, cmind_func_info, log_cmd
    from cmind.pipeline.ants_register import ants_register
    from cmind.pipeline.ants_applywarp import ants_applywarp

    #convert various strings to boolean
    bool_list=[bias_cor, keep_intermediate_files, no_crop, generate_figures, 
               ForceUpdate, verbose]
    bool_list = input2bool(bool_list)
        
    if verbose:
        cmind_func_info(logger=logger)     
    
    (imbool, input_nii_full, input_nii_root, input_nii_path, input_nii_ext)=imexist(input_nii,'nifti-1')
    if not imbool:
        raise IOError("input image, %s, doesn't exist!" % input_nii)

    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Defacing")      
    
    age_months=float(age_months)
        
    if age_months<=6.0:
        age_str='00to06'
    elif (age_months>6) and (age_months<=24):
        age_str='06to24'
    elif (age_months>24) and (age_months<=36):
        age_str='24to48'
    elif (age_months>36) and (age_months<=252):
        age_str='48to216'

    if not ped_template_path:
        from cmind.globals import cmind_template_dir as ped_template_path
     
    if iso_xfm_mm:
        if isinstance(iso_xfm_mm,(int,float)):
            if iso_xfm_mm<=0:
                raise ValueError("iso_xfm_mm must be > 0!")
        else:
            raise ValueError("iso_xfm_mm must be of type int or float")
        
    RIGID_template_path=os.path.join(ped_template_path,age_str,'RIGID')

    reg_struct={}
    if iso_xfm_mm and iso_xfm_mm>1.5: #use the 2mm standards instead
        resolution_string='2mm'
        ANTS_precision='defacer' #'lenient'
    else:
        resolution_string='1mm'
        ANTS_precision='defacer'
        
    reg_struct['StudyTemplate']=os.path.join(RIGID_template_path,age_str + '_template_' + resolution_string + '.nii.gz')
    reg_struct['StudyTemplate_noface_mask']=os.path.join(RIGID_template_path,age_str + '_template_noface_' + resolution_string + '.nii.gz')
        
    #convert strings to appropriate type
    if isinstance(age_months,str):
        age_months=float(age_months)
          
    if not output_nii:
        output_nii=input_nii_full.replace(input_nii_ext,'_defaced'+input_nii_ext)
        
    output_dir=os.path.dirname(output_nii)
    if not output_dir:
        output_dir=os.getcwd()
    elif not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    fname_defaced=output_nii
    if (not imexist(fname_defaced,'nifti-1')[0]) or ForceUpdate:
        
        if not imexist(reg_struct['StudyTemplate'])[0]:
            raise ValueError("couldn't find study template file: {}".format(reg_struct['StudyTemplate']))
        if not imexist(reg_struct['StudyTemplate_noface_mask'])[0]:
            raise ValueError("couldn't find study template noface mask file: {}".format(reg_struct['StudyTemplate_noface_mask']))
        
    
        working_dir=tempfile.mkdtemp()  #do the computations in a working directory
        output_root=os.path.join(working_dir,'StudyTemplate_to_Struct_');
        
        fname_crop=os.path.join(working_dir,'Structural_Crop')
        if no_crop:
            log_cmd('$FSLDIR/bin/imcp %s %s' % (input_nii_full,fname_crop), verbose=verbose, logger=module_logger)
        else:
            cmind_crop_robust(input_nii_full, fname_crop, age_months, oname_root='defacer', minthresh = crop_thresh, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=module_logger)
        
#        if iso_xfm_mm:
#            log_cmd('flirt -in "%s" -ref "%s" -out "%s" -applyisoxfm %f' % (fname_crop, fname_crop ,fname_crop, iso_xfm_mm),verbose=verbose,logger=module_logger)
           
        if bias_cor:
            from cmind.pipeline.cmind_bias_correct import cmind_bias_correct
            fname_bias = os.path.join(working_dir,'Structural_Crop_Biascorr')
            fname_head, bias_field_vol=cmind_bias_correct(fname_crop, fname_bias, ANTS_bias_cmd=None, oname_root='defacer', generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=module_logger)
            fixed=fname_bias
        else:
            fixed=fname_crop
            
        moving=reg_struct['StudyTemplate']
        nopng=0
        
        affine_only=False
        (warp_file, aff_file, output_warped)=ants_register(fixed,moving,working_dir, output_root,ANTS_path=cmind_ANTs_dir,operation_type="both",affine_only=affine_only ,rigid_affine=False,concat_transforms=True,dim=3,nopng=nopng,precision_case=ANTS_precision,verbose=verbose,logger=module_logger)
        
        transform_list=[]
        transform_list.append(aff_file);
        if not affine_only:
            transform_list.append(warp_file);
        moving=reg_struct['StudyTemplate_noface_mask']
        output_warped=os.path.join(working_dir,'noface_mask.nii.gz');
        ants_applywarp(transform_list,fixed,moving,output_warped,ANTS_path=cmind_ANTs_dir,dim=3,nopng=nopng,inverse_flag=False,verbose=verbose,logger=module_logger)
        
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mul "%s" "%s"' % (fname_crop,output_warped,fname_defaced),verbose=verbose,logger=module_logger)
        
        if generate_figures:
            slicer_mid_sag_fig(fname_defaced)
            
        if keep_intermediate_files:
            module_logger.info("Intermediate Files were retained in {}".format(working_dir))  
        else:
            shutil.rmtree(working_dir)  #remove the working directory
    
    #fname_head,bias_field_vol=cmind_bias_correct(fname_crop, fname_bias, ANTS_bias_cmd, oname_root=oname_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
    #(fname_brain,fname_brain_mask)=cmind_brain_extract(fname_head, bet_thresh=bet_thresh, oname_root=oname_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
    output_tuple=(fname_defaced) #fname_defaced)
    return output_tuple
    
def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_dir, cmind_example_output_dir, cmind_template_dir
       
    age_months=84.7
    ped_template_path=cmind_template_dir
    iso_xfm_mm=2.
    generate_figures=True
    ForceUpdate=True
    verbose=True
    bias_cor=True
    keep_intermediate_files=True
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',file_mode='w')        
    if verbose:
        func=cmind_timer(cmind_deface,logger=logger)
    else:
        func=cmind_deface

    testcases=['P'] #,'F']
    for testcase in testcases:    
        if testcase == 'F':
            input_nii = pjoin(cmind_example_dir,
                   'IRC04H_06M008_F_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1.nii.gz')
            output_nii = pjoin(cmind_example_output_dir,'F','T1W_defaced.nii.gz')
        elif testcase == 'P':
            input_nii = pjoin(cmind_example_dir,
                   'IRC04H_06M008_P_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1.nii.gz')
            output_nii = pjoin(cmind_example_output_dir,'P','T1W_defaced.nii.gz')
     
        func(input_nii,output_nii=output_nii,ped_template_path=ped_template_path,
            age_months=age_months, bias_cor=bias_cor, 
            keep_intermediate_files=keep_intermediate_files, 
            iso_xfm_mm=iso_xfm_mm, generate_figures=generate_figures, 
            ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
  
def _arg_parser():
    import argparse
    parser = argparse.ArgumentParser(description="Deface a structural volume", epilog="")  #-h, --help exist by default
    #example of a positional argument
    #parser.add_argument("-i","--input_nii", required=True, dest="input_nii",help="filename of NIFTI or NIFTIGZ volume to process")
    parser.add_argument("input_nii", help="filename of NIFTI or NIFTIGZ volume to process")
    parser.add_argument("-o","--output_nii", dest="output_nii",help='defaced output filename.  default is to append "_defaced" to the input filename')
    parser.add_argument("--ped_template_path",type=str,default="None", help="path to the directory containing the pediatric templates. will attempt to set this automatically")
    parser.add_argument("--age_months", help="subject age in months  (used during cropping)")
    parser.add_argument("--bias_cor",type=str,default="True", help="perform bias correction prior to registration.  final output will be defaced, but not bias corrected.")
    parser.add_argument("--keep_intermediate_files",type=str,default="False", help="if True, all intermediate files/transforms will be retained.")
    parser.add_argument("--iso_xfm_mm",type=str,default="None", help="if specified, will use FSL FLIRT -applyisoxfm to adjust resolution before doing the registration.  setting this to 2 mm can speed up processing of high resolution volumes")
    parser.add_argument("--no_crop",type=str,default="False", help="if False, do not attempt to crop z FOV first")
    parser.add_argument("--crop_thresh",type=float,default=0.03, help="percentile threshold to use during cropping.  range [0,1],  default = 0.03")
    group1 = parser.add_argument_group('additional optional arguments', 'Other arguments affecting program execution')
    group1.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
    group1.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
    group1.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
    group1.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
    return parser
        
     
if __name__ == '__main__':
    import sys
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and (sys.argv[1]=='test' or sys.argv[1]=='testF'):
        _testme()
    else:    
        parser=_arg_parser()
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        input_nii=args.input_nii;
        output_nii=args.output_nii;
        ped_template_path=str2none(args.ped_template_path);
        bias_cor=args.bias_cor
        keep_intermediate_files=args.keep_intermediate_files
        iso_xfm_mm=str2none(args.iso_xfm_mm)
        age_months=args.age_months;
        bias_cor=args.bias_cor;
        crop_thresh = args.crop_thresh;
        no_crop = args.no_crop;
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger
        
        if verbose:
            cmind_deface=cmind_timer(cmind_deface,logger=logger)
    
        cmind_deface(input_nii=input_nii,output_nii=output_nii, 
            ped_template_path=ped_template_path,age_months=age_months, 
            bias_cor=bias_cor, keep_intermediate_files=keep_intermediate_files, 
            iso_xfm_mm=iso_xfm_mm, no_crop=no_crop, crop_thresh=crop_thresh, 
            generate_figures=generate_figures, 
            ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
        
                        