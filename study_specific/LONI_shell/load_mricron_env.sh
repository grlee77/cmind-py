#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $cwd/exec_exists.sh

if exec_exists module;
then
	#echo "found module"
	module load mricron
else
	if exec_exists mricron;
	then
		#echo "module not found, but mricron already in path"	
		echo ""
	else
		echo "Error:  module not found, and mricron not already on path"
		exit 1
	fi
fi

