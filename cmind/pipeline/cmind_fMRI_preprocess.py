#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

# TODO:  add option for antsMotionCorr in place of mcflirt?
# see example at:
#     https://github.com/stnava/ANTs/blob/master/Scripts/antsMotionCorrExample


def cmind_fMRI_preprocess(output_dir, BOLD_vol, ASL_vol=None, paradigm_name='',
                          ANTS_bias_cmd='', UCLA_flag=False,
                          fsl_motion_outliers_cmd=None, 
                          FORCE_SINGLEPROCESS=False, generate_figures=True,
                          ForceUpdate=False, verbose=False, logger=None):
    """ run coregistration of ASL and BOLD timeseries

    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    BOLD_vol : str
        filename of the NIFTI/NIFTI-GZ BOLD 4D timeseries
    ASL_vol : str or None, optional
        filename of the NIFTI/NIFTI-GZ ASL 4D timeseries
    paradigm_name : str, optional
        output filenames will incorporate this name.  may also be used in the
        future to apply different processing streams to different cases.
        e.g. {'Stories','Sentences',''}
    ANTS_bias_cmd : str, optional
        pathname to the ANTs bias field correction binary
    UCLA_flag : bool, optional
        if True, ASL labeling order is tag, control, tag, ...
        if False, ASL labeling order is control, tag, control...
    fsl_motion_outliers_cmd : str, optional
        path to the fsl_motion_outliers_cmd_v2 shell script.  If not specified
        will try to guess it relative to the python script location.
    FORCE_SINGLEPROCESS : bool, optional
        if False will try to run ASL & BOLD motion correction in parallel via
        multiprocessing
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    Returns
    -------
    ASL_mcf_vol : str or None
        motion corrected ASL timeseries
    BOLD_mcf_vol : str
        motion corrected BOLD timeseries
    meanCBF_N4_vol : str or None
        mean control-tag image from ASL timeseries
    meanBOLD_N4_vol : str
        mean image from BOLD timeseries
    motion_parfile_ASL : str or None
        ASL motion parameters
    motion_parfile_BOLD : str
        BOLD motion parameters

    Notes
    -----
    By default will try to run two instances of mcflirt simultaneously
    (for ASL and BOLD)

    """

    import os
    from os.path import join as pjoin

    import nibabel as nib
    import numpy as np

    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import (imexist, split_multiple_ext,
                                        filecheck_bool)
    from cmind.utils.cmind_utils import (cmind_reg_report_img,
                                         cmind_outlier_remove)
    from cmind.utils.fsl_mcflirt_opt import fsl_mcflirt_opt
    from cmind.utils.fsl_mcflirt_opt_ASLBOLD import fsl_mcflirt_opt_ASLBOLD
    from cmind.utils.logging_utils import (cmind_init_logging, log_cmd,
                                           cmind_func_info)

    # convert various strings to boolean
    generate_figures, ForceUpdate, verbose, UCLA_flag = input2bool(
        [generate_figures, ForceUpdate, verbose, UCLA_flag])

    if verbose:
        cmind_func_info(logger=logger)

    module_logger = cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting fMRI Preprocessing: Stage 1")

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if not ANTS_bias_cmd:
        from cmind.globals import cmind_ANTs_dir
        ANTS_bias_cmd = pjoin(cmind_ANTs_dir, 'N4BiasFieldCorrection')

    # If no ASL volume specified, assume it was a BOLD-only scan
    if not ASL_vol:
        ASLBOLD = False
    else:
        ASLBOLD = True

    BOLD_mcf_vol = pjoin(output_dir, 'BOLD_' + paradigm_name + '_mcf')
    BOLD_mcf_abs_mean = pjoin(BOLD_mcf_vol + '_abs_mean.rms')
    BOLD_mcf_rel_mean = pjoin(BOLD_mcf_vol + '_rel_mean.rms')
    motion_parfile_BOLD = pjoin(
        output_dir, 'BOLD_' + paradigm_name + '_mcf.par')

    if not ASLBOLD:
        BOLD_mcf_vol = fsl_mcflirt_opt(
            BOLD_vol,
            cost_type='normcorr',
            out_name=BOLD_mcf_vol,
            output_dir=output_dir,
            verbose=verbose,
            logger=module_logger)        
        ASL_mcf_vol = ''
        motion_parfile_ASL = ''            
    else:
        ASL_mcf_vol = pjoin(output_dir, 'ASL_' + paradigm_name + '_mcf')
        ASL_mcf_abs_mean = pjoin(ASL_mcf_vol + '_abs_mean.rms')
        ASL_mcf_rel_mean = pjoin(ASL_mcf_vol + '_rel_mean.rms')
        # 1a) Perform mcflirt-based motion correction independently for ASL and
        # BOLD
        if (not imexist(ASL_mcf_vol, 'nifti-1')[0]) or (not imexist(
                BOLD_mcf_vol, 'nifti-1')[0]) or ForceUpdate:
            (ASL_mcf_vol,
             BOLD_mcf_vol) = fsl_mcflirt_opt_ASLBOLD(
                 ASL_vol,
                 BOLD_vol,
                 cost_type='normcorr',
                 out_name_ASL=ASL_mcf_vol,
                 out_name_BOLD=BOLD_mcf_vol,
                 output_dir=output_dir,
                 FORCE_SINGLEPROCESS=FORCE_SINGLEPROCESS,
                 verbose=verbose,
                 logger=module_logger)

        motion_parfile_ASL = pjoin(
            output_dir, 'ASL_' + paradigm_name + '_mcf.par')

        # 1b) Plot the motion parameters using fsl_tsplot
        if generate_figures:
            fname_base = pjoin(output_dir, 'ASL_' + paradigm_name)
            fname_abs = fname_base + '_mcf_abs.rms'
            fname_rel = fname_base + '_mcf_rel.rms'
            output_fname_rot = fname_base + '_rot.png'
            output_fname_trans = fname_base + '_trans.png'
            output_fname_disp = fname_base + '_disp.png'

            if filecheck_bool(motion_parfile_ASL, output_fname_rot):
                log_cmd(
                    '$FSLDIR/bin/fsl_tsplot -i "%s" -t "MCFLIRT estimated rotations (radians)" -u 1 --start=1 --finish=3 -a x,y,z -w 640 -h 144 -o "%s"' %
                    (motion_parfile_ASL, output_fname_rot),
                    verbose=verbose,
                    logger=module_logger)
            if filecheck_bool(motion_parfile_ASL, output_fname_trans):
                log_cmd(
                    '$FSLDIR/bin/fsl_tsplot -i "%s" -t "MCFLIRT estimated translations (mm)" -u 1 --start=4 --finish=6 -a x,y,z -w 640 -h 144 -o "%s"' %
                    (motion_parfile_ASL, output_fname_trans),
                    verbose=verbose,
                    logger=module_logger)
            if filecheck_bool(motion_parfile_ASL, output_fname_disp):
                log_cmd(
                    '$FSLDIR/bin/fsl_tsplot -i "%s","%s" -t "MCFLIRT estimated mean displacement (mm)" -u 1 -w 640 -h 144 -a absolute,relative -o "%s"' %
                    (fname_abs, fname_rel, output_fname_disp),
                    verbose=verbose,
                    logger=module_logger)

            output_fname_disp2 = fname_base + '_disp_fixed2.png'
            output_fname_disp3 = fname_base + '_disp_fixed5.png'
            output_fname_disp4 = fname_base + '_disp_fixed10.png'
            output_fname_disp5 = fname_base + '_disp_fixed25.png'
            if filecheck_bool(motion_parfile_ASL, output_fname_disp2):
                log_cmd(
                    '$FSLDIR/bin/fsl_tsplot -i "%s","%s" -t "MCFLIRT estimated mean displacement (mm)" --ymin=0 --ymax=2 -u 1 -w 640 -h 144 -a absolute,relative -o "%s" ' %
                    (fname_abs, fname_rel, output_fname_disp2),
                    verbose=verbose,
                    logger=module_logger)
            if filecheck_bool(motion_parfile_ASL, output_fname_disp3):
                log_cmd(
                    '$FSLDIR/bin/fsl_tsplot -i "%s","%s" -t "MCFLIRT estimated mean displacement (mm)" --ymin=0 --ymax=5 -u 1 -w 640 -h 144 -a absolute,relative -o "%s" ' %
                    (fname_abs, fname_rel, output_fname_disp3),
                    verbose=verbose,
                    logger=module_logger)
            if filecheck_bool(motion_parfile_ASL, output_fname_disp4):
                log_cmd(
                    '$FSLDIR/bin/fsl_tsplot -i "%s","%s" -t "MCFLIRT estimated mean displacement (mm)" --ymin=0 --ymax=10 -u 1 -w 640 -h 144 -a absolute,relative -o "%s" ' %
                    (fname_abs, fname_rel, output_fname_disp4),
                    verbose=verbose,
                    logger=module_logger)
            if filecheck_bool(motion_parfile_ASL, output_fname_disp5):
                log_cmd(
                    '$FSLDIR/bin/fsl_tsplot -i "%s","%s" -t "MCFLIRT estimated mean displacement (mm)" --ymin=0 --ymax=25 -u 1 -w 640 -h 144 -a absolute,relative -o "%s" ' %
                    (fname_abs, fname_rel, output_fname_disp5),
                    verbose=verbose,
                    logger=module_logger)

        # .replace('.nii.gz','').replace('.nii','')
        ASLstring = split_multiple_ext(ASL_mcf_vol)[0]
        (mean_exists,
         mean_fname_full,
         mean_fname_root,
         mean_pth,
         mean_ext) = imexist(ASLstring, 'nifti-1')
        mean_fname_out = pjoin(mean_pth, 'MeanSub_' + mean_fname_root)
        meanCBF_N4_vol = pjoin(
            output_dir, 'MeanSub_' + mean_fname_root + '_N4')

        # 2a) Save images of the mean time-series subtraction after outlier
        # removal
        if (not imexist(meanCBF_N4_vol, 'nifti-1')[0]) or ForceUpdate:
            log_cmd(
                '$FSLDIR/bin/fslmaths "%s" -Tmean "%s_mean"' %
                (ASLstring, ASLstring),
                verbose=verbose,
                logger=module_logger)
            log_cmd(
                '%s -d 3 -i "%s_mean.nii.gz" -o ["%s_mean_N4.nii.gz","%s_N4Bias.nii.gz"] -s 1 -b 200 -c [50x50x30x20, 1e-04]' %
                (ANTS_bias_cmd, ASLstring, ASLstring, ASLstring),
                verbose=verbose,
                logger=module_logger)
            log_cmd(
                '$FSLDIR/bin/bet2 "%s_mean_N4" "%s_mean_N4_brain" -f 0.3 -m' %
                (ASLstring, ASLstring), verbose=verbose, logger=module_logger)
            mask_str = ASLstring + "_mean_N4_brain_mask"
            log_cmd(
                '$FSLDIR/bin/fslmaths "%s" -mas "%s" "%s_bet"' %
                (ASLstring, mask_str, ASLstring),
                verbose=verbose,
                logger=module_logger)

            nii = nib.load(ASLstring + '_bet.nii.gz')
            ASLimgs = nii.get_data()
            nii_hdr = nii.get_header()
            nii_affine = nii.get_affine()

            if UCLA_flag:
                ctrl_imgs = ASLimgs[:, :, :, 1::2]
                tag_imgs = ASLimgs[:, :, :, 0::2]
            else:
                ctrl_imgs = ASLimgs[:, :, :, 0::2]
                tag_imgs = ASLimgs[:, :, :, 1::2]

            diff_imgs = ctrl_imgs - tag_imgs
            # remove if more than 1.5 standard deviations from mean artifact
            # level
            thresh = 1.5
            mask_nii = nib.load(mask_str + '.nii.gz')
            mask = mask_nii.get_data()

            new_hdr = nii_hdr.copy()
            new_hdr.set_data_shape(diff_imgs.shape)
            diff_imgs_nii = nib.Nifti1Image(diff_imgs, nii_affine, new_hdr)
            diff_fname_out = pjoin(output_dir, 'AllSub_' + mean_fname_root)
            diff_imgs_nii.to_filename(diff_fname_out + '.nii.gz')

            if False:  # OLD WAY
                (timepoints_keep, AL) = cmind_outlier_remove(
                    diff_imgs, thresh, mask)
            else:
                if not fsl_motion_outliers_cmd:
                    from cmind.globals import cmind_outliers_cmd as fsl_motion_outliers_cmd
                log_cmd(
                    '%s -i "%s" -m "%s" -o %s --mean_ref --nomoco -v' %
                    (fsl_motion_outliers_cmd,
                     diff_fname_out,
                     mask_str,
                     pjoin(output_dir, 'FSL_outliers.txt')),
                    verbose=verbose,
                    logger=module_logger)
                if os.path.exists(pjoin(output_dir, 'FSL_outliers.txt')):
                    out_txt = np.loadtxt(pjoin(output_dir, 'FSL_outliers.txt'))
                    if out_txt.ndim > 1:
                        out_txt = np.sum(out_txt, axis=1)
                    timepoints_keep = np.where(out_txt == 0)[0]
                    # os.remove('FSL_outliers.txt')
                else:
                    timepoints_keep = np.arange(diff_imgs_nii.shape[-1])
                log_cmd(
                    '$FSLDIR/bin/imrm "%s"' %
                    (diff_fname_out),
                    verbose=verbose,
                    logger=module_logger)

            diff_mean = np.mean(diff_imgs[:, :, :, timepoints_keep], axis=-1)
            # nii_type=16; #floating point
            (p_min, p_max) = np.percentile(diff_mean.ravel(), q=(5, 99.8))
            diff_mean[diff_mean < p_min] = p_min
            diff_mean[diff_mean > p_max] = p_max

            new_hdr = nii_hdr.copy()
            new_hdr.set_data_shape(diff_mean.shape)
            diff_mean_nii = nib.Nifti1Image(diff_mean, nii_affine, new_hdr)
            diff_mean_nii.to_filename(mean_fname_out + '.nii.gz')
            # cmind_save_nii_mod_header(nii,diff_mean,[mean_fname_out
            # '.nii'],nii_type)

            log_cmd(
                '$FSLDIR/bin/fslmaths  "%s" -div "%s_N4Bias" "%s"' %
                (mean_fname_out,
                 pjoin(
                     mean_pth,
                     mean_fname_root),
                    meanCBF_N4_vol),
                verbose=verbose,
                logger=module_logger)

    # 2b) generate BOLD timeseries average volume
    # .replace('.nii.gz','').replace('.nii','')
    BOLDstring = split_multiple_ext(BOLD_mcf_vol)[0]
    meanBOLD_N4_vol = BOLDstring + '_mean_N4.nii.gz'

    if (not imexist(meanBOLD_N4_vol, 'nifti-1')[0]) or ForceUpdate:
        log_cmd(
            '$FSLDIR/bin/fslmaths "%s" -Tmean "%s_mean"' %
            (BOLDstring, BOLDstring),
            verbose=verbose,
            logger=module_logger)
        log_cmd(
            '%s -d 3 -i "%s_mean.nii.gz" -o ["%s_mean_N4.nii.gz","%s_N4Bias.nii.gz"] -s 1 -b 200 -c [50x50x30x20, 1e-04]' %
            (ANTS_bias_cmd, BOLDstring, BOLDstring, BOLDstring),
            verbose=verbose,
            logger=module_logger)

    if ASLBOLD:
        # overlay BOLD and ASL averages to make sure they are aligned
        if generate_figures:
            (exists, BOLD_avg_fname_full) = imexist(BOLDstring + '_mean')[0:2]
            (exists, ASL_avg_fname_full) = imexist(ASLstring + '_mean')[0:2]
            cmind_reg_report_img(
                ASL_avg_fname_full,
                BOLD_avg_fname_full,
                pjoin(output_dir, 'ASL_BOLD_overlay.png'))

        # 2c) make figure of mean tag-control image
        if generate_figures:
            (mean_N4_exists,
             mean_N4_fname_full,
             mean_N4_fname_root) = imexist(meanCBF_N4_vol, 'nifti-1')[0:3]
            output_fname = pjoin(output_dir, 'MeanSub_ASLBOLD_N4.png')
            if filecheck_bool(mean_N4_fname_full, output_fname):
                log_cmd(
                    '$FSLDIR/bin/slicer "%s" -a "%s"' %
                    (mean_N4_fname_full, output_fname),
                    verbose=verbose,
                    logger=module_logger)

        # make sure output filenames include the proper file extensions
        (imbool, ASL_mcf_vol) = imexist(ASL_mcf_vol, 'nifti-1')[0:2]
        (imbool, meanCBF_N4_vol) = imexist(meanCBF_N4_vol, 'nifti-1')[0:2]
    else:
        ASL_mcf_vol = ''
        meanCBF_N4_vol = ''

    (imbool, BOLD_mcf_vol) = imexist(BOLD_mcf_vol, 'nifti-1')[0:2]

    module_logger.info("Finished fMRI Preprocessing: Stage 1")

    # print any filename outputs for capture by LONI
    print("BOLD_mcf_vol:{}".format(BOLD_mcf_vol))
    print("BOLD_mcf_abs_mean:{}".format(BOLD_mcf_abs_mean))
    print("BOLD_mcf_rel_mean:{}".format(BOLD_mcf_rel_mean))
    print("motion_parfile_BOLD:{}".format(motion_parfile_BOLD))
    if ASLBOLD:
        print("ASL_mcf_vol:{}".format(ASL_mcf_vol))
        print("meanCBF_N4_vol:{}".format(meanCBF_N4_vol))
        print("motion_parfile_ASL:{}".format(motion_parfile_ASL))
        print("Registration_Target:{}".format(meanCBF_N4_vol))
        print("ASL_mcf_abs_mean:{}".format(ASL_mcf_abs_mean))
        print("ASL_mcf_rel_mean:{}".format(ASL_mcf_rel_mean))
    else:
        print("Registration_Target:{}".format(meanBOLD_N4_vol))
        print("ASL_mcf_vol:None")
        print("meanCBF_N4_vol:None")
        print("motion_parfile_ASL:None")        
        print("ASL_mcf_abs_mean:None")
        print("ASL_mcf_rel_mean:None")
    # return the filenames of interest
    return (ASL_mcf_vol, BOLD_mcf_vol, meanCBF_N4_vol,
            meanBOLD_N4_vol, motion_parfile_ASL, motion_parfile_BOLD)


def _testme():
    import tempfile
    from os.path import join as pjoin

    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import (cmind_example_dir, cmind_example_output_dir,
                               cmind_ANTs_dir, cmind_shellscript_dir)

    cases = ['Stories', 'Sentences']

    ANTS_bias_cmd = pjoin(cmind_ANTs_dir, 'N4BiasFieldCorrection')
    UCLA_flag = False
    fsl_motion_outliers_cmd = pjoin(
        cmind_shellscript_dir,
        'fsl_motion_outliers_v2')
    generate_figures = True
    ForceUpdate = True
    verbose = True
    logger = cmind_logger(log_level_console='INFO',
                          logfile=pjoin(
                              tempfile.gettempdir(),
                              'cmind_log.txt'),
                          log_level_file='DEBUG',
                          file_mode='w')

    if verbose:
        func = cmind_timer(cmind_fMRI_preprocess, logger=logger)
    else:
        func = cmind_fMRI_preprocess

    for case_str in cases:
        output_dir = pjoin(
            cmind_example_output_dir,
            'F',
            'ASLBOLD_' +
            case_str)
        paradigm_name = case_str
        if case_str == 'Stories':
            ASL_vol = pjoin(
                cmind_example_dir,
                'IRC04H_06M008_F_1_WIP_ASLBOLD_Stories_SENSE_8_1_ASL.nii.gz')
            BOLD_vol = pjoin(
                cmind_example_dir,
                'IRC04H_06M008_F_1_WIP_ASLBOLD_Stories_SENSE_8_1_BOLD.nii.gz')

        elif case_str == 'Sentences':
            ASL_vol = pjoin(
                cmind_example_dir,
                'IRC04H_06M008_F_1_WIP_ASLBOLD_Sentences_SENSE_10_1_ASL.nii.gz')
            BOLD_vol = pjoin(
                cmind_example_dir,
                'IRC04H_06M008_F_1_WIP_ASLBOLD_Sentences_SENSE_10_1_BOLD.nii.gz')

        func(output_dir,
             BOLD_vol,
             ASL_vol,
             paradigm_name=paradigm_name,
             ANTS_bias_cmd=ANTS_bias_cmd,
             UCLA_flag=UCLA_flag,
             fsl_motion_outliers_cmd=fsl_motion_outliers_cmd,
             generate_figures=generate_figures,
             ForceUpdate=ForceUpdate,
             verbose=verbose,
             logger=logger)


if __name__ == '__main__':
    import sys
    import argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        _testme()
    else:
        parser = argparse.ArgumentParser(
            description="Run coregistration of ASL and BOLD timeseries",
            epilog="")  # -h, --help exist by default
        # example of a positional argument
        parser.add_argument(
            "-o",
            "--output_dir",
            required=True,
            help="directory in which to store the output")
        parser.add_argument(
            "-bold",
            "--BOLD_vol",
            required=True,
            help="filename of the NIFTI/NIFTI-GZ BOLD 4D timeseries")
        parser.add_argument(
            "-asl",
            "--ASL_vol",
            type=str,
            default='',
            help="filename of the NIFTI/NIFTI-GZ ASL 4D timeseries")
        parser.add_argument(
            "-p",
            "--paradigm_name",
            type=str,
            default='',
            help="[Feat_BOLD_Sentences,Feat_BOLD_Stories,Feat_ASL_Sentences,Feat_ASL_Stories];may be used in future to apply different processing to different cases")
        parser.add_argument(
            "-ants",
            "--ANTS_bias_cmd",
            type=str,
            default='',
            help="pathname to the ANTs bias field correction binary")
        parser.add_argument(
            "-ucla",
            "--UCLA_flag",
            type=str,
            default="false",
            help="if True, ASL labeling order is tag, control, tag...; if False, ASL labeling order is control, tag, control... ")
        parser.add_argument(
            "--fsl_motion_outliers_cmd",
            type=str,
            default='',
            help="path to the fsl_motion_outliers_cmd_v2 shell script.  If not specified will try to guess it relative to the python script location.")
        parser.add_argument(
            "-gf",
            "--generate_figures",
            type=str,
            default="True",
            help="if true, generate additional summary images")
        parser.add_argument(
            "-u",
            "--ForceUpdate",
            type=str,
            default="False",
            help="if True, rerun and overwrite any previously existing results")
        parser.add_argument(
            "-v",
            "-debug",
            "--verbose",
            help="print additional output (to terminal and log)",
            action="store_true")
        parser.add_argument(
            "-log",
            "--logfile",
            dest='logger',
            type=str,
            default="",
            help="logging.Logger object (or string of a filename to log to)")

        # if first command line argument is 'loni_bash', print bash template
        # instead of proceeding
        # kludge to auto-generate case/esac and usage statements for the
        # corresponding LONI shell scripts
        _parser_to_loni(parser, sys.argv, __file__)

        args = parser.parse_args()

        output_dir = args.output_dir
        ASL_vol = str2none(args.ASL_vol)
        BOLD_vol = args.BOLD_vol
        paradigm_name = args.paradigm_name
        ANTS_bias_cmd = str2none(args.ANTS_bias_cmd)
        UCLA_flag = args.UCLA_flag
        fsl_motion_outliers_cmd = str2none(args.fsl_motion_outliers_cmd)
        generate_figures = args.generate_figures
        ForceUpdate = args.ForceUpdate
        verbose = args.verbose
        logger = args.logger

        if verbose:
            cmind_fMRI_preprocess = cmind_timer(
                cmind_fMRI_preprocess,
                logger=logger)

        cmind_fMRI_preprocess(output_dir,
                              BOLD_vol,
                              ASL_vol=ASL_vol,
                              paradigm_name=paradigm_name,
                              ANTS_bias_cmd=ANTS_bias_cmd,
                              UCLA_flag=UCLA_flag,
                              fsl_motion_outliers_cmd=fsl_motion_outliers_cmd,
                              generate_figures=generate_figures,
                              ForceUpdate=ForceUpdate,
                              verbose=verbose,
                              logger=logger)

