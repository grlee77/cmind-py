#!/bin/bash
cmind_sh_path="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $cmind_sh_path/load_cmind_env.sh

input_file=junk

UCLA_flag=false;
isOblique=false;
if [ -z "$ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS" ]; then
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

while [ "$1" != "" ]; do
    case $1 in
        -f )
            shift
            input_file=$1
            ;;
        -u )
            UCLA_flag=true
            ;;
        -o )
            shift
            isOblique=true
            ;;
    esac
    shift
done

if [[ ! -f ${input_file} ]]
then
    echo need -f argument.
    exit 1
fi

echo tempdir = $tempdir or $temp

output_dir=`mktemp -d`

cp $input_file $output_dir

DTI_input_nii=${output_dir}/`basename $input_file`  #SUBJECT-SPECIFIC

if [[ $input_file =~ ^(.*)(.nii.gz)$ ]] ; then
    output_filestem=${BASH_REMATCH[1]}
fi

if [[ $input_file =~ ^(.*)(HARDI|DTI)(.*)$ ]] ; then
    fileType=${BASH_REMATCH[2]}
fi

#cmind_sh_path='/cluster/loni/code/mwagner/DTI_HARDI'

#dtiPrepPath=`cd ${cmind_sh_path}/../../../../; pwd`
gradient_table_file=${CMIND_DTIHARDI_DIR}/Philips_LAS_bvects.txt
gradient_bval_file=${CMIND_DTIHARDI_DIR}/Philips_LAS_bvals_$fileType.txt
DTIPrep_xmlProtocol=${CMIND_DTIHARDI_DIR}/CMIND_DTIPrep_Protocol_noBrainMask.xml

if $isOblique; then
    isOblique_str=True
else
    isOblique_str=False
fi

if $UCLA_flag; then
    UCLA_flag=True
    ScannerType="Siemens"
    isSiemens=True
else
    UCLA_flag=False
    ScannerType="Philips"
    isSiemens=False
fi

ForceUpdate_DTI_prep="False"

source $cmind_sh_path/load_slicer4_env.sh
source $cmind_sh_path/load_DTIPrep_env.sh

#module load DTIPrep

DTIPrep_command='DTIPrep1.2'
DWIConvert_command=${SLICER4_HOME}/lib/Slicer-4.2/cli-modules/DWIConvert

DTIPrep_command_fullpath=`command -v $DTIPrep_command`

#echo DTIPrep_command_fullpath = ${DTIPrep_command_fullpath}

Slicer_path=${SLICER4_HOME}/Slicer
Slicer_denoise_module=${SLICER4_HOME}/lib/Slicer-4.2/cli-modules/DWIJointRicianLMMSEFilter

case_to_run='DTI_QA_DN_process'  #if "_DN" is removed, slicer4 denoising is skipped

#module load matlab
source $cmind_sh_path/load_python_env.sh
source $cmind_sh_path/load_ANTS_env.sh
source $cmind_sh_path/load_mricron_env.sh
source $cmind_sh_path/load_fsl_env.sh
source $cmind_sh_path/load_ffmpeg_env.sh

#Set defaults
default_ANTS_bias_cmd=N4BiasFieldCorrection
default_generate_figures=True
ForceUpdate=True
Flip_ap=False

echo "python $cmind_python_dir/cmind_DTI_HARDI_QA.py $DTI_input_nii $gradient_table_file $gradient_bval_file -o $output_dir $case_to_run $DTIPrep_command $DWIConvert_command $DTIPrep_xmlProtocol $Flip_ap $isSiemens $isOblique_str $Slicer_path $Slicer_denoise_module -v --ForceUpdate $ForceUpdate"
python $cmind_python_dir/cmind_DTI_HARDI_QA.py $DTI_input_nii $gradient_table_file $gradient_bval_file -o $output_dir $case_to_run $DTIPrep_command $DWIConvert_command $DTIPrep_xmlProtocol $Flip_ap $isSiemens $isOblique_str $Slicer_path $Slicer_denoise_module -v --ForceUpdate $ForceUpdate

#check that QA report exists
output_file_test=${output_dir}/DTI_avgb0_at_start_QCReport.txt
output_file_test2=${output_dir}/HARDI_avgb0_at_start_QCReport.txt
#echo "$output_file_test"
if [ ! -f $output_file_test ] && [ ! -f $output_file_test2 ]; then
    echo "ERROR: Failed to finish DTI/HARDI QA processing"
    exit 1
fi

echo "Finished cmind_DTI_HARDI_QA"

if [ -f ${output_dir}/Directions_Kept.txt ] ; then

    cp ${output_dir}/DTI_avgb0_at_start_QCReport.txt ${output_filestem}_QCed_Report.txt
    cp ${output_dir}/DTI_avgb0_at_start_XMLQCResult.xml ${output_filestem}_QCed_Report.xml
    cp ${output_dir}/DTI_avgb0_at_start_QCed_bvals ${output_filestem}_QCed_bvals.txt
    cp ${output_dir}/DTI_avgb0_at_start_QCed_bvects ${output_filestem}_QCed_bvecs.txt
    cp ${output_dir}/DTI_avgb0_at_start_QCed.nii.gz ${output_filestem}_QCed.nii.gz
    cp ${output_dir}/Directions_Kept.txt ${output_filestem}_QCed_dirs_kept.txt
    echo Directions kept: `cat ${output_dir}/Directions_Kept.txt`
    exit 0
else
    echo Directions kept: 0
    echo QC failed.
    exit 1
fi

