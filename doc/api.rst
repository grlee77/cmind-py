.. -*- mode: rst -*-
    
`Main C-MIND Project Website <http://research.cchmc.org/c-mind>`_

.. note:  first call: sphinx-autogen -o generated *.rst

.. _api:

**************************
Pipeline API Documentation
**************************

.. .. autosummary::
..   :toctree: generated
..
..   cmind


Primary Modules
===============

The primary functions for data processing are within the cmind.pipeline module.
These are grouped by the type of data processing to which they apply below.

.. autosummary::
   :toctree: generated

   cmind.pipeline   

Pipeline Modules Listed by Function
===================================
  
.. the rest of the modules are relative to the top-level

.. currentmodule:: cmind.pipeline

Structural Processing Modules
----------------------------------------

.. autosummary::
   :toctree: generated

   cmind_crop_robust
   cmind_bias_correct
   cmind_brain_extract
   cmind_segment
   cmind_struct_preprocess
   cmind_age_to_template
   cmind_normalize
   cmind_fieldmap_regBBR
   cmind_rigid_coregister
   cmind_apply_normalization
   cmind_atlas_to_subject
   cmind_calculate_slice_coverage

CBF Processing Modules
----------------------

.. autosummary::
   :toctree: generated

   cmind_alpha
   cmind_baselineCBF_preprocess
   cmind_baselineCBF_coregisterT1
   cmind_baselineCBF_quantitative

Fieldmap Processing Modules
---------------------------

.. autosummary::
   :toctree: generated

   cmind_fieldmap_process

Simultaneous ASL/BOLD Modules
-----------------------------

.. autosummary::
   :toctree: generated

   cmind_fMRI_preprocess
   cmind_fMRI_preprocess2
   cmind_fMRI_outliers
   cmind_fMRI_level1_stats
   cmind_fMRI_norm  
   cmind_build_feat_dir
   cmind_stage_second_level

T1 Mapping Modules
------------------

.. autosummary::
   :toctree: generated

   cmind_T1map_preprocess
   cmind_T1map_fit
   cmind_T1_tissue_averages
   

DTI/HARDI Modules
-----------------

.. autosummary::
   :toctree: generated

   cmind_DTI_HARDI_QA
   cmind_DWI_TRK_to_3plane
..   pipeline_devel.cmind_DIPY_processing


Supporting Modules
==================

.. currentmodule:: cmind

.. autosummary::
   :toctree: generated

   globals
   finders
   pipeline.ants_applywarp
   pipeline.ants_register   
   utils.utils
   utils.image_utils
   utils.file_utils
   utils.fsf_utils
   utils.logging_utils
   utils.nifti_utils
   utils.montager
   utils.fsl_mcflirt_opt
   utils.fsl_mcflirt_opt_ASLBOLD
   utils.decorators
   utils.cmind_HTML_report_gen

.. .. currentmodule:: cmind.utils.image_utils

.. Image utilities (cmind.utils.image_utils)
.. ===============

.. .. autosummary::
.. 	:toctree: generated

.. 	slicer_3plane_fig
.. 	slicer_mid_sag_fig
.. 	slicer_tile_slices
.. 	ImageMagick_get_size
.. 	ImageMagick_build_colorbar
.. 	ImageMagick_crop_border
.. 	ImageMagick_append_label
.. 	random_color_str
	



.. .. currentmodule:: cmind.utils.montager

.. Image Display utilities  (cmind.utils.montager)
.. =======================

.. .. autosummary::
.. 	:toctree: generated

.. 	montager



.. .. currentmodule:: cmind.utils.file_utils

.. File Handling utilities   (cmind.utils.file_utils)
.. =======================

.. .. autosummary::
.. 	:toctree: generated

.. 	export_tarfile
.. 	multiglob_list
.. 	imexist        
.. 	filecheck_bool
.. 	csv2dict
.. 	dict2csv
.. 	remove_file
.. 	rm_broken_links
.. 	cmind_copy_all
.. 	which       
	


.. .. currentmodule:: cmind.utils.fsf_utils

.. design.fsf Utilities   (cmind.utils.fsf_utils)
.. =======================

.. .. autosummary::
.. 	:toctree: generated

.. 	create_fsf
.. 	create_con
.. 	create_ev
.. 	write_fsf
.. 	fsf_substitute



.. .. currentmodule:: cmind.utils.logging_utils


.. Logging utilities   (cmind.utils.logging_utils)
.. =================

.. .. autosummary::
.. 	:toctree: generated
	
.. 	cmind_func_info
.. 	cmind_logger
.. 	cmind_init_logging
.. 	log_cmd


.. .. currentmodule:: cmind.utils.cmind_utils


.. CMIND Support utilities   (cmind.utils.cmind_utils)
.. =======================

.. .. autosummary::
.. 	:toctree: generated

.. 	run_mcflirt
.. 	run_mcflirt_parallel
.. 	cmind_reg_report_img
.. 	cmind_lowres_to_T1
.. 	find_optimal_FSL_refvol
.. 	cmind_scaleimg
.. 	cmind_dual_func_overlay    
.. 	read_fsl_design
.. 	oblique_bool
.. 	AFNI_3dCM
.. 	cmind_outlier_remove


.. .. currentmodule:: cmind.utils.utils

.. Miscellaneous utilities   (cmind.utils)
.. =======================

.. .. autosummary::
.. 	:toctree: generated

.. 	str2list
.. 	dprint
.. 	input2bool        
.. 	sphere
.. 	str2none  
.. 	is_string_like 
.. 	parser_to_bash
.. 	fsl_highpass_temporal_filter1D
.. 	calc_tSNR_regress


.. .. currentmodule:: cmind

.. Other supporting utilities
.. ==========================

.. .. autosummary::
..    :toctree: generated

   
..    utils.nifti_utils.cmind_save_nii_mod_header
..    utils.fsl_mcflirt_opt
..    utils.fsl_mcflirt_opt_ASLBOLD
..    utils.decorators.cmind_timer
..    utils.cmind_HTML_report_gen.cmind_HTML_reports
..    utils.cmind_HTML_report_gen.cmind_HTML_report_gen
..    utils.cmind_HTML_report_gen.cmind_combine_HTML_reports
..    pipeline.ants_applywarp
..    pipeline.ants_register   
..    utils.nipype_interfaces
..    utils.nipype_nodes



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`