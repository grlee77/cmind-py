#!/bin/bash
echo "CMD: $*"

usage()
{
echo "Basic usage: cmind_baselineCBF_quantitative.py -o <output_dir> -cbfm <BaselineCBF_mcf_masked> -n4cbf <N4BiasCBF> -tk <BaselineCBF_timepoints_kept> -af <alpha_file> -wm <WMpve> -T1map <T1_map_reg2CBF>  [options]

usage: cmind_baselineCBF_quantitative.py [-h] -o OUTPUT_DIR -cbfm
                                         BASELINECBF_MCF_MASKED -n4cbf
                                         N4BIASCBF -tk
                                         BASELINECBF_TIMEPOINTS_KEPT -af
                                         ALPHA_FILE -wm WMPVE -T1map
                                         T1_MAP_REG2CBF [-aff HRTOLR_AFFINE]
                                         [-w HRTOLR_WARP] [-relmot RELMOT]
                                         [-relmot_thresh RELMOT_THRESH]
                                         [--cbf_lambda CBF_LAMBDA] [--R1a R1A]
                                         [--delta DELTA]
                                         [--delta_art DELTA_ART] [--w W]
                                         [--tau TAU] [--TR TR]
                                         [-gf GENERATE_FIGURES]
                                         [-u FORCEUPDATE] [-v] [-log LOGGER]

Quantify CBF

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -wm WMPVE, -wmvol WMPVE, --WMpve WMPVE
                        white matter partial volume estimate
  -T1map T1_MAP_REG2CBF, --T1_map_reg2CBF T1_MAP_REG2CBF
                        T1map that has been registered to
                        BaselineCBF_mcf_masked
  -af ALPHA_FILE, --alpha_file ALPHA_FILE
                        alpha txt file containing the inversion efficiency
  
and either:
    -cbf_pre_dir DIRECTORY
    					directory with CBF processing results (assumes default filenames)
    -reg_dir DIRECTORY       
						directory with regBBR registration results (assumes default filenames)

or:
  -cbfm BASELINECBF_MCF_MASKED, --BaselineCBF_mcf_masked BASELINECBF_MCF_MASKED
                        masked, motion-corrected BaselineCBF volume
  -n4cbf N4BIASCBF, --N4BiasCBF N4BIASCBF
                        BaselineCBF N4Bias field
  -tk BASELINECBF_TIMEPOINTS_KEPT, --BaselineCBF_timepoints_kept BASELINECBF_TIMEPOINTS_KEPT
                        Timepoints kept during BaselineCBF preprocessing
  -relmot RELMOT, --relmot RELMOT
                        relative motion .rms file from mcflirt

           AND
  -aff HRTOLR_AFFINE, -h2l HRTOLR_AFFINE, --HRtoLR_affine HRTOLR_AFFINE
                        affine from highres (structural) to lowres space
           OR
  -w HRTOLR_WARP, -h2lw HRTOLR_WARP, --HRtoLR_warp HRTOLR_WARP
                        warp from highres (structural) to lowres space
  	
	
optional arguments:
  -h, --help            show this help message and exit
  --alpha_const ALPHA_CONST
			also repeat CBF calculation at this alternative value of alpha
  -relmot_thr RELMOT_THRESH, --relmot_thresh RELMOT_THRESH
                        relative motion threshold (mm) for discarding
                        timepoints
  --cbf_lambda CBF_LAMBDA
                        blood-brain partition coefficient
  --R1a R1A             R1 relaxation rate of arterial blood (1/seconds)
  --delta DELTA         transit time (to capillary compartment) (seconds)
  --delta_art DELTA_ART
                        arterial transit time (seconds)
  --w W                 post-labeling delay (seconds)
  --tau TAU             label duration (seconds)
  --TR TR               TR (seconds)
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        filename to log to
  -use_fm USE_FIELDMAP: 
  						using fieldmap?
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=5
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi


cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

REQUIRED_ARGS=12
if [ $# -lt $REQUIRED_ARGS ] 
then
    usage
    exit 1
fi

cbf_lambda=0.9
R1a=0.6250
delta=1.5
delta_art=1.3
w=1.4
tau=2.0
TR=4.0
alpha_const=0.75

debug=false
ForceUpdate=true
relmot_thr=1.5
use_fieldmap=true
all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
	-cbf_pre_dir )
	    shift
	    CBF_pre_dir=$1
	    ;;
	-reg_dir )
	    shift
	    reg_dir=$1
	    ;;
  	-use_fm )
	    shift
	    use_fieldmap=$1
	    ;;
	-o|--output_dir )
        shift
        output_dir=$1
        ;;
    -cbfm|--BaselineCBF_mcf_masked )
        shift
        BaselineCBF_mcf_masked=$1
        ;;
    -n4cbf|--N4BiasCBF )
        shift
        N4BiasCBF=$1
        ;;
    -tk|--BaselineCBF_timepoints_kept )
        shift
        BaselineCBF_timepoints_kept=$1
        ;;
    -af|--alpha_file )
        shift
        alpha_file=$1
        ;;
    -wm|-wmvol|--WMpve )
        shift
        WMpve=$1
        ;;
    -T1map|--T1_map_reg2CBF )
        shift
        T1_map_reg2CBF=$1
        ;;
    --alpha_const )
        shift
        alpha_const=$1
        ;;
    -aff|-h2l|--HRtoLR_affine )
        shift
        HRtoLR_affine=$1
        ;;
    -warp|-h2lw|--HRtoLR_warp )
        shift
        HRtoLR_warp=$1
        ;;
    -relmot|--relmot )
        shift
        relmot=$1
        ;;
    -relmot_thr|--relmot_thresh )
        shift
        relmot_thresh=$1
        ;;
    --cbf_lambda )
        shift
        cbf_lambda=$1
        ;;
    --R1a )
        shift
        R1a=$1
        ;;
    --delta )
        shift
        delta=$1
        ;;
    --delta_art )
        shift
        delta_art=$1
        ;;
    --w )
        shift
        w=$1
        ;;
    --tau )
        shift
        tau=$1
        ;;
    --TR )
        shift
        TR=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
	esac
    shift
done

if [ ! -z $verbose_string ]; then
  debug=true
else
  debug=false
fi


if [ ! -z ${reg_dir} ]
then
	if [ -d ${reg_dir} ]
	then
	    if ! $use_fieldmap
	    then
			if [ -z "${HRtoLR_affine+xxx}" ] || [ ! -f $HRtoLR_affine ]
			then
			    HRtoLR_affine=${reg_dir}/lowres2highres_inv.mat
			fi
			if [ ! -f $HRtoLR_affine ]
			then
				echo "Error: affine not found"
				exit 1
			fi
			HRtoLR_warp="None"
	    else
			if [ -z "${HRtoLR_warp+xxx}" ] || [ ! -f $HRtoLR_warp ]
			then
			    HRtoLR_warp=${reg_dir}/lowres2highres_warp_inv.nii.gz		    
			fi
			if [ ! -f $HRtoLR_warp ]
			then
		       HRtoLR_warp="None"
			   echo "Warning:  HRtoLR_warp file, $HRtoLR_warp not found"
			   echo "will try without fieldmap instead"
			   if [ -z "${HRtoLR_affine+xxx}" ] || [ ! -f $HRtoLR_affine ]
	       	   then
	       	       HRtoLR_affine=${reg_dir}/lowres2highres_inv.mat
	       	   fi 
	       	   if [ ! -f $HRtoLR_affine ]
	       	   then
	       	       echo "Error: affine not found either"
	       	       exit 1       	   
	       	   fi
	   	    else 
	   	   		HRtoLR_affine="None"
	       	fi
	    fi
	fi
fi

if [ ! -z ${CBF_pre_dir} ]
then
	if [ -d ${CBF_pre_dir} ]
	then
	    
	    if [ -z "${BaselineCBF_mcf_masked+xxx}" ] || [ ! -f $BaselineCBF_mcf_masked ]
	    then
		BaselineCBF_mcf_masked=${CBF_pre_dir}/BaselineCBF_mcf_masked.nii.gz
	    fi
	    
	    if [ -z "${N4BiasCBF+xxx}" ] || [ ! -f $N4BiasCBF ]
	    then
		N4BiasCBF=${CBF_pre_dir}/BaselineCBF_N4Bias.nii.gz
	    fi

	    if [ -z "${BaselineCBF_timepoints_kept+xxx}" ] || [ ! -f $BaselineCBF_timepoints_kept ]
	    then
		BaselineCBF_timepoints_kept=${CBF_pre_dir}/BaselineCBF_timepoints_kept.txt

	    fi
	    
	    if [ -z "${relmot+xxx}" ] || [ ! -f $relmot ]
	    then
		relmot=${CBF_pre_dir}/mc/BaselineCBF_mcf_rel.rms
	    fi
	fi
fi

if [ -z "${BaselineCBF_mcf_masked+xxx}" ] || [ ! -f $BaselineCBF_mcf_masked ]; then
    echo "ERROR: BaselineCBF_mcf_masked file: $BaselineCBF_mcf_masked doesn't exist"
    exit 1
fi 

if [ -z "${T1_map_reg2CBF+xxx}" ] || [ ! -f $T1_map_reg2CBF ]; then
    echo "ERROR: T1_map_reg2CBF file: $T1_map_reg2CBF doesn't exist"
    exit 1
fi 

if [ -z "${N4BiasCBF+xxx}" ] || [ ! -f $N4BiasCBF ]; then
    echo "ERROR: N4BiasCBF file: $N4BiasCBF doesn't exist"
    exit 1
fi 
if [ -z "${relmot+xxx}" ] || [ ! -f $relmot ]; then
    echo "ERROR: relmot file: $relmot doesn't exist"
    exit 1
fi 

if [ -z "${alpha_file+xxx}" ] || [ ! -f $alpha_file ]; then
    echo "ERROR: alpha text file: $alpha_file doesn't exist"
    exit 1
fi 

if [ -z "${BaselineCBF_timepoints_kept+xxx}" ] || [ ! -f $BaselineCBF_timepoints_kept ]; then
    echo "ERROR: BaselineCBF_timepoints_kept: $BaselineCBF_timepoints_kept doesn't exist"
    exit 1
fi 

if [ -z "${WMpve+xxx}" ] || [ ! -f $WMpve ]; then
    echo "ERROR: WM pve file: $WMpve doesn't exist"
    exit 1
fi 

if [ ! -f $HRtoLR_affine ] && [ ! -f $HRtoLR_warp ]
then
    echo "ERROR: either HRtoLR_affine or HRtoLR_warp must be defined.\n"
    exit 1;
fi

if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi 



#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir BaselineCBF_mcf_masked N4BiasCBF BaselineCBF_timepoints_kept alpha_file WMpve T1_map_reg2CBF alpha_const HRtoLR_affine HRtoLR_warp relmot relmot_thresh cbf_lambda R1a delta delta_art w tau TR generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_baselineCBF_quantitative.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_baselineCBF_quantitative.py ${verbose_string} ${all_optional_args}
printf 'cmind_baselineCBF_quantitative.py:  Elapsed time: %s\n' $(timer $t)
#print output filenames for capture by LONI pipeline
#The following filename should be printed by the python function above:
#    CBF_Wang_file


# t=$(timer)

# #if [ -f $HRtoLR_warp ]
# #then
# #    optional_args="--HRtoLR_warp $HRtoLR_warp "
# #elif [ -f $HRtoLR_affine ]
# #then
# #    optional_args="--HRtoLR_affine $HRtoLR_affine "
# #fi
# optional_args="--HRtoLR_affine $HRtoLR_affine --HRtoLR_warp $HRtoLR_warp "

# if ${debug}
# then
#     echo DEBUGGING
#     optional_args=${optional_args}"--verbose"
# fi

# echo "python ${cmind_python_dir}/cmind_baselineCBF_quantitative.py ${output_dir} $BaselineCBF_mcf_masked $N4BiasCBF $BaselineCBF_timepoints_kept ${alpha_file} $WMpve $T1_map_reg2CBF --relmot $relmot --relmot_thresh $relmot_thresh --ForceUpdate ${ForceUpdate} --cbf_lambda $cbf_lambda --R1a $R1a --delta $delta --delta_art ${delta_art} --w $w --tau $tau --TR $TR $optional_args"

# python ${cmind_python_dir}/cmind_baselineCBF_quantitative.py ${output_dir} $BaselineCBF_mcf_masked $N4BiasCBF $BaselineCBF_timepoints_kept ${alpha_file} $WMpve $T1_map_reg2CBF --relmot $relmot --relmot_thresh $relmot_thresh --ForceUpdate ${ForceUpdate} --cbf_lambda $cbf_lambda --R1a $R1a --delta $delta --delta_art ${delta_art} --w $w --tau $tau --TR $TR $optional_args

# printf 'cmind_baselineCBF_quantitative:  Elapsed time: %s\n' $(timer $t)

#check that CBF_Wang2002.nii.gz exists
output_file_test=$output_dir/CBF_Wang2002.nii.gz
echo "$output_file_test"
if [ ! -f $output_file_test ]; then
    echo "ERROR: Failed to finish BaselineCBF quantitative"
    exit 1
fi 

if $debug
then  
    echo "CBF_Wang:$output_file_test" 
    echo "Finished cmind_baselineCBF_quantitative"
fi

