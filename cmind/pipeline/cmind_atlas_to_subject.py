#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def split_atlas_outputs(Atlas_out):
    #For convenience with nipype use, explicitly list up to six outputs
    out1=Atlas_out
    out2=None
    out3=None
    out4=None
    out5=None
    out6=None
    
    if isinstance(Atlas_out,list):
        for idx,o in enumerate(Atlas_out):
            exec('out%d=Atlas_out[%d]' % (idx+1,idx))
    return out1,out2,out3,out4,out5,out6
    
def cmind_atlas_to_subject(output_dir,atlas_vol,ref_vol,reg_struct,ANTS_path=None, interp_type='NearestNeighbor', output_prefix='Atlas', split_script=None, omit_MNItoHR=False,HRtoLR_Affine=None,HRtoLR_Warp=None,generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """Transform a multi-label atlas from standard MNI standard space to subject space
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    atlas_vol : str or list of str
        filename of the NIFTI(GZ) format atlas
    ref_vol : str
        image filename for the "fixed" image (registration target)
    reg_struct : str or dict
        filename of the .csv file containing the registration dictionary
    ANTS_path : str, optional
        path to ANTs binaries
    omit_MNItoHR : bool, optional
        if True assume the atlas is already in structural space
    interp_type : {'NearestNeighbor','','HammingWindowedSinc','BSpline[3]'}, optional
        interpolation to use (should be nearest neighbor for binary atlases)
    output_prefix : str or list of str, optional
        prepend this string to the output filenames (default = 'Atlas')
    split_script : str or None, optional
        file name of the shell script that splits all masks within an atlas to separate volumes
    HRtoLR_Affine : str or None, optional
        affine transform from MNI standard space to functional space
    HRtoLR_Warp : str or None, optional
        fieldmap warp correction for transform from MNI standard space to functional space
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)

    Returns
    -------
    Atlas_out : str or list of str
        filename of the atlas in subject space
    out1 : str
        1st output 
    out2 : str or None
        2jd output, if it exists
    out3 : str or None
        3rd output, if it exists
    out4 : str or None
        4th output, if it exists
    out5 : str or None
        5th output, if it exists
    out6 : str or None
        6th output, if it exists
    
    """
    
    #recursive calls to this function if atlas_vol input is a list of volumes to warp
    if isinstance(atlas_vol,(list,tuple)):
        if not isinstance(output_prefix,(list,tuple)):
            output_prefix=[output_prefix,]*len(atlas_vol)
        Atlas_out=[]
        for av,op in zip(atlas_vol,output_prefix):
            out_vol=cmind_atlas_to_subject(output_dir,
                                   av,
                                   ref_vol,
                                   reg_struct,
                                   ANTS_path=ANTS_path,
                                   interp_type=interp_type,
                                   output_prefix=op,
                                   split_script=split_script, 
                                   omit_MNItoHR=omit_MNItoHR,
                                   HRtoLR_Affine=HRtoLR_Affine,
                                   HRtoLR_Warp=HRtoLR_Warp,
                                   generate_figures=generate_figures, 
                                   ForceUpdate=ForceUpdate, 
                                   verbose=verbose, 
                                   logger=logger)[0] 
            Atlas_out.append(out_vol)
            
        out1,out2,out3,out4,out5,out6 = split_atlas_outputs(Atlas_out)
        return Atlas_out, out1, out2, out3, out4, out5, out6
            
    import os
    from os.path import join as pjoin

    from cmind.pipeline.ants_applywarp import ants_applywarp
    from cmind.pipeline.atlas_labels import atlas_labels
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict
    from cmind.utils.cmind_utils import cmind_reg_report_img
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if not ANTS_path:
        from cmind.globals import cmind_ANTs_dir as ANTS_path
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Transforming Atlas to Subject space")

    if HRtoLR_Affine and (not os.path.exists(HRtoLR_Affine)):
        raise IOError("Missing HRtoLR_Affine, %s" % HRtoLR_Affine)
    if HRtoLR_Warp and (not os.path.exists(HRtoLR_Warp)):
        raise IOError("Missing HRtoLR_Warp, %s" % HRtoLR_Warp)
    if not imexist(ref_vol,'nifti-1')[0]:
        raise IOError("Missing reference volume, %s" % ref_vol)
    if not imexist(atlas_vol,'nifti-1')[0]:
        raise IOError("Missing atlas volume, %s" % atlas_vol)
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
 
    if isinstance(reg_struct,str):
        if os.path.exists(reg_struct):
            reg_struct=csv2dict(reg_struct)
        else:
            raise IOError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
    elif not isinstance(reg_struct,dict):
        raise ValueError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
        
    if output_prefix:
        output_prefix=output_prefix+'_'
        
    Atlas_HR_out=pjoin(output_dir,output_prefix+'Inv2T1Wbrain.nii.gz');
    Atlas_out=pjoin(output_dir,output_prefix+'Inv2lowres.nii.gz');
    Atlas_split_out=pjoin(output_dir,output_prefix+'mask');


    #convert ANTs interpolation flag to the nearest FSL interpolation option
    if interp_type=='NearestNeighbor':
        fsl_interp_type='nn'
    elif interp_type.lower().find('bspline')!=-1:
        fsl_interp_type='spline'
    elif interp_type.lower().find('sinc')!=-1:
        fsl_interp_type='sinc'
    else:
        fsl_interp_type='trilinear'
        
    if (not imexist(Atlas_out)[0]) or ForceUpdate:
        
        if omit_MNItoHR:
#            #Atlas_HR_out = atlas_vol
#            log_cmd('$FSLDIR/bin/imcp "%s" "%s"' % (atlas_vol, Atlas_HR_out), 
#                    verbose=verbose, logger=module_logger)
            Atlas_HR_out=atlas_vol
        else:  #transform Atlas from standard space to subject highres space
            fixed=reg_struct['HR_img'];
            moving=atlas_vol;
            transform_list=reg_struct['ANTS_HR_to_MNI_transform_list']
            for idx,t in enumerate(transform_list):  #TODO: fix reg_struct.csv so this kludge is unnecessary
                transform_list[idx]=transform_list[idx].replace('"','') 
            dim=3
            inverse_flag=1
        
            nopng=0
            #for "inverse" case, have to swap the order of the fixed, moving arguments
            ants_applywarp(transform_list, moving, fixed, Atlas_HR_out, 
                        ANTS_path=ANTS_path, dim=dim, interp_type=interp_type, 
                        nopng=nopng, inverse_flag=inverse_flag, verbose=verbose, logger=module_logger)

        if HRtoLR_Warp:
            log_cmd('$FSLDIR/bin/applywarp --in="%s" --ref="%s" --out="%s" --warp="%s" --interp=%s --super --superlevel=a' % (Atlas_HR_out, ref_vol, Atlas_out, HRtoLR_Warp, fsl_interp_type), 
                    verbose=verbose, logger=module_logger)
        elif HRtoLR_Affine:            
    #        log_cmd('$FSLDIR/bin/flirt -interp nearestneighbour -in "%s" -ref "%s" -out "%s" -init "%s" -applyxfm' % (Atlas_HR_out,ref_vol,Atlas_out,HRtoLR_Affine), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/applywarp --in="%s" --ref="%s" --out="%s" --premat="%s" --interp=%s --super --superlevel=a' % (Atlas_HR_out, ref_vol, Atlas_out, HRtoLR_Affine, fsl_interp_type), 
                    verbose=verbose, logger=module_logger)
        else:
            raise Exception("Either HRtoLR_Warp or HRtoLR_Affine must be specified")
        
        cmind_reg_report_img(Atlas_out,ref_vol, Atlas_out.replace('.nii.gz','.png'))
        
        if split_script:  #if split script is given, also split the subject space atlas into separate NIFTI masks for each structure
            if not os.path.exists(split_script):
                raise Exception("Specified atlas splitting script, %s, not found!" % split_script)
            else:
                log_cmd('/bin/bash %s %s %s' % (split_script,Atlas_out,Atlas_split_out), 
                        verbose=verbose, logger=module_logger)
    else:
        module_logger.info("registration previously performed...skipping")
        
#    if omit_MNItoHR:
#        #Atlas_HR_out = atlas_vol
#        log_cmd('$FSLDIR/bin/imrm "%s""' % (Atlas_HR_out), 
#                verbose=verbose, logger=module_logger)
#        

    out1,out2,out3,out4,out5,out6 = split_atlas_outputs(Atlas_out)
    return Atlas_out, out1, out2, out3, out4, out5, out6
            


def _testme():

    import os, tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_ANTs_dir, cmind_shellscript_dir
    #output_dir_DTI=pjoin(cmind_example_output_dir,'P','DTI')
    output_dir_CBF=pjoin(cmind_example_output_dir,'P','BaselineCBF')
    #output_dir=pjoin(output_dir_DTI,'MASK_AAL116_2mm_Py');
    output_dir=pjoin(output_dir_CBF,'MASK_AAL116_2mm_Py');

    atlas_vol_aal='/media/Data1/src_repositories/svn_stuff/cmind-matlab/trunk/aal/2mm/aal_MNI152_2mm.nii.gz'                      #TODO: fix path
    #atlas_vol_harvard_subcort='/usr/share/data/harvard-oxford-atlases/HarvardOxford/HarvardOxford-sub-maxprob-thr50-2mm.nii.gz '  #TODO: fix path
    #atlas_vol_harvard_cort='/usr/share/data/harvard-oxford-atlases/HarvardOxford/HarvardOxford-cort-maxprob-thr50-2mm.nii.gz '    #TODO: fix path                                                                       
    
    atlas_vol=atlas_vol_aal;
    interp_type='NearestNeighbor'
    omit_MNItoHR=False
    output_prefix='Atlas'
    split_script=pjoin(cmind_shellscript_dir,'aal_split.sh')
    #ref_vol=pjoin(output_dir_DTI,'b0avg.nii.gz')
    ref_vol=pjoin(output_dir_CBF,'MeanSub_BaselineCBF_N4.nii.gz')
    reg_struct=pjoin(cmind_example_output_dir,'P','T1_proc','reg_struct.csv')
    ANTS_path=cmind_ANTs_dir
    HRtoLR_Affine=[] #pjoin(output_dir_DTI,'reg','highres2lowres.mat')
    #HRtoLR_Warp=pjoin(output_dir_DTI,'reg_BBR_fmap','lowres2highres_warp_inv.nii.gz')
    HRtoLR_Warp=pjoin(output_dir_CBF,'reg_BBR_fmap','lowres2highres_warp_inv.nii.gz')
    generate_figures=False
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  

    if verbose:
        func=cmind_timer(cmind_atlas_to_subject,logger=logger)
    else:
        func=cmind_atlas_to_subject

    func(output_dir,atlas_vol,ref_vol,reg_struct,ANTS_path=ANTS_path,
        interp_type=interp_type,output_prefix=output_prefix, 
        split_script=split_script, omit_MNItoHR=omit_MNItoHR, 
        HRtoLR_Affine=HRtoLR_Affine,HRtoLR_Warp=HRtoLR_Warp,
        generate_figures=generate_figures, 
        ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
    #2nd test case on a non-binary registration from structural to lowres space
    #atlas_vol = pjoin(cmind_example_output_dir,'P','T1_proc','T1Segment_pveseg.nii.gz')
    interp_type='BSpline[3]'
    split_script=None
    omit_MNItoHR=True
    output_dir=pjoin(output_dir_CBF,'PVE_lowres');
    T1_dir=pjoin(cmind_example_output_dir,'P','T1_proc')
    pve_vols = [pjoin(T1_dir,'T1Segment_GM.nii.gz'),
                pjoin(T1_dir,'T1Segment_WM.nii.gz'),
                pjoin(T1_dir,'T1Segment_CSF.nii.gz')]
    prefixes = ['GMpve','WMpve','CSFpve']
    
    if False:
        for atlas_vol, output_prefix in zip(pve_vols,prefixes):
            func(output_dir,atlas_vol,ref_vol,reg_struct,ANTS_path=ANTS_path,
                interp_type=interp_type, output_prefix=output_prefix,
                omit_MNItoHR=omit_MNItoHR, split_script=split_script, 
                HRtoLR_Affine=HRtoLR_Affine, HRtoLR_Warp=HRtoLR_Warp, 
                generate_figures=generate_figures, 
                ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
    else:
        func(output_dir,atlas_vol=pve_vols,ref_vol=ref_vol,
             reg_struct=reg_struct,ANTS_path=ANTS_path,
                interp_type=interp_type, output_prefix=prefixes,
                omit_MNItoHR=omit_MNItoHR, split_script=split_script, 
                HRtoLR_Affine=HRtoLR_Affine, HRtoLR_Warp=HRtoLR_Warp, 
                generate_figures=generate_figures, 
                ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)

if __name__ == '__main__':
    import sys,argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()

    else: 
        parser = argparse.ArgumentParser(description="Transform a multi-label atlas from standard MNI standard space to subject space", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("atlas_vol", help="filename of the NIFTI(GZ) format atlas")
        parser.add_argument("ref_vol", type=str, help="image filename for the 'fixed' image (registration target)")
        parser.add_argument("reg_struct", help="filename of the .csv file containing the registration dictionary")
        parser.add_argument("--ANTS_path", type=str,default="None", help="path to ANTs binaries")
        parser.add_argument("--interp_type", type=str,default="NearestNeighbor", help="interpolation type (use 'NearestNeighbor' for binary atlases)")
        parser.add_argument("--output_prefix", type=str,default="Atlas", help="interpolation type (use 'NearestNeighbor' for binary atlases)")
        parser.add_argument("--split_script", type=str,default="None", help="file name of the shell script that splits all masks within an atlas to separate volumes")
        parser.add_argument("--omit_MNItoHR", type=str,default="False", help="if True, assume atlas is in subject highres space instead of MNI space")
       
        parser.add_argument("--HRtoLR_Affine", type=str, help="affine transform from MNI standard space to functional space")
        parser.add_argument("--HRtoLR_Warp", type=str, help="fieldmap warp correction for transform from MNI standard space to functional space")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="rerun, overwriting any existing outputs?")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        

        args=parser.parse_args()
        
        output_dir=args.output_dir;
        atlas_vol=args.atlas_vol;
        ref_vol=args.ref_vol;
        reg_struct=args.reg_struct;
        ANTS_path=str2none(args.ANTS_path);
        interp_type=args.interp_type;
        output_prefix=args.output_prefix
        split_script=str2none(args.split_script);
        omit_MNItoHR=args.omit_MNItoHR
        HRtoLR_Affine=str2none(args.HRtoLR_Affine);
        HRtoLR_Warp=str2none(args.HRtoLR_Warp);
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger;
        
        if verbose:
            cmind_atlas_to_subject=cmind_timer(cmind_atlas_to_subject,logger=logger)

        cmind_atlas_to_subject(output_dir,atlas_vol,ref_vol,reg_struct,
            ANTS_path=ANTS_path, interp_type=interp_type, output_prefix=output_prefix,
            split_script=split_script, omit_MNItoHR=omit_MNItoHR, 
            HRtoLR_Affine=HRtoLR_Affine, HRtoLR_Warp=HRtoLR_Warp, 
            generate_figures=generate_figures, 
            ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
                