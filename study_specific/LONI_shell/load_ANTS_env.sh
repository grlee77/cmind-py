#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $cwd/exec_exists.sh

if exec_exists module;
then
	#echo "found module"
	#module load ANTS/1.9.x
	module load ANTS
	module load itk  #required for ANTS/1.9.x to run properly
	if [ -z "$ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS" ]; then
		export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1;
    fi
else
	if exec_exists ANTS;
	then
		#echo "module not found, but ANTS already in path"
		echo ""
	else
		echo "Error:  module not found, and ANTS not already on path"
		exit 1
	fi
fi

