# -*- coding: utf-8 -*-
""" This file contains defines parameters for cmind that we use to fill
settings in setup.py, the cmind top-level docstring, and for building the
docs.  In setup.py in particular, we exec this file, so it cannot import cmind
"""

# dipy version information.  An empty _version_extra corresponds to a
# full release.  '.dev' as a _version_extra string means this is a development
# version
_version_major = 1
_version_minor = 1
_version_micro = 0
_version_extra = ''
#_version_extra = ''

# Format expected by setup.py and doc/source/conf.py: string of form "X.Y.Z"
__version__ = "%s.%s.%s%s" % (_version_major,
                              _version_minor,
                              _version_micro,
                              _version_extra)

CLASSIFIERS = ["Development Status :: 4 - Beta",
               "Environment :: Console",
               "Intended Audience :: Science/Research",
               "License :: OSI Approved :: BSD License",
               "Operating System :: POSIX",
               "Programming Language :: Python",
               "Topic :: Scientific/Engineering"]

description  = 'CMIND physiological and functional MRI pipelines in python'

# Note: this long_description is actually a copy/paste from the top-level
# README.txt, so that it shows up nicely on PyPI.  So please remember to edit
# it only in one place and sync it correctly.
long_description = """
========
cmind-py
========

The cmind-py python package consists of a number of pipeline modules for the 
analysis of MRI images.  These modules can either be called as a function 
within python or run standalone from the command line. In addition to the 
pipeline modules, there are a number of additional support functions available.

cmind-py is for research only and is not intended for clincal use

Website
=======

http://research.cchmc.org/c-mind/

Code
====

http://bitbucket.org/grlee77/cmind-py/

License
=======

cmind is licensed under the terms of the BSD license. Some code included with
cmind is also licensed under the BSD license.  Please the LICENSE file in the
cmind distribution.
"""

# versions for dependencies (TODO: Need to check/update this list)
NUMPY_MIN_VERSION='1.3'
SCIPY_MIN_VERSION='0.8'
MATPLOTLIB_MIN_VERSION='1.0.0'
NIBABEL_MIN_VERSION='1.0.0'

# Main setup parameters
NAME                = 'cmind'
MAINTAINER          = "Gregory Lee"
MAINTAINER_EMAIL    = "gregory.lee@cchmc.org"
DESCRIPTION         = description
LONG_DESCRIPTION    = long_description
URL                 = "http://bitbucket.org/grlee77/cmind-py/"
DOWNLOAD_URL        = "http://bitbucket.org/grlee77/cmind-py/‎"  #TODO: update link
LICENSE             = "BSD license"
CLASSIFIERS         = CLASSIFIERS
AUTHOR              = "cmind developers"
AUTHOR_EMAIL        = "gregory.lee@cchmc.org"
PLATFORMS           = "POSIX"
MAJOR               = _version_major
MINOR               = _version_minor
MICRO               = _version_micro
ISRELEASE           = _version_extra == ''
VERSION             = __version__
PROVIDES            = ["cmind"]
REQUIRES            = ["numpy (>=%s)" % NUMPY_MIN_VERSION,
                       "matplotlib (>=%s)" % MATPLOTLIB_MIN_VERSION,
                       "nibabel (>=%s)" % NIBABEL_MIN_VERSION]
