#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_feat_poststats(prep_dir,stats_dir,poststats_dir,z_thresh=2.3,prob_thresh=0.05,thresh=2, verbose=False, logger=None):
    """ utility to run "poststats" portion of FEAT processing
    
    Parameters
    ----------
    feat_dir : str
        FEAT output directory
    poststats_dir : str
        output directory to store the poststats images in
    z_thresh : float
        initial z threshold to use when determining clusters.  
        unused if thresh!=2
    prob_thresh : float
        probability threshold for voxelwise or cluster significance
    thresh : {0,1,2}
        0 = uncorrected, 1 = voxel-based (FWE corrected), 2 = cluster-based
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
        
    """
    import os
    from os.path import join as pjoin
    import glob
    
    import numpy as np
    
    from cmind.utils.file_utils import split_multiple_ext
    from cmind.utils.logging_utils import log_cmd
    
    #TODO: loop over rawstats
    pd=poststats_dir
    rawstat_zfiles = glob.glob(pjoin(stats_dir, 'stats','zstat*.nii*'))
    rawstat_zffiles = glob.glob(pjoin(stats_dir, 'stats','zfstat*.nii*'))    
    if len(rawstat_zffiles)>0:  #add zfstat images to the zstat list
        [rawstat_zfiles.append(f) for f in rawstat_zffiles]

    for rawstats in rawstat_zfiles:
        rawstats = split_multiple_ext(os.path.basename(rawstats))[0] #os.path.basename(rawstats).replace('.gz','').replace('.nii','')
        THRESH_file=pjoin(pd,'thresh_%s' % rawstats)
                
        log_cmd('$FSLDIR/bin/fslmaths "%s/stats/%s" -mas "%s/mask" "%s"' % (stats_dir,rawstats,prep_dir,THRESH_file), verbose=verbose, logger=logger)
    
        if rawstats.find('zfstat')!=-1:
            zfstat_flag=True
            copeIDX=int(rawstats.replace('zfstat',''))  #get cope# from zfstat filename
        else:
            zfstat_flag=False
            copeIDX=int(rawstats.replace('zstat',''))  #get cope# from zstat filename
        
        DLH_file=pjoin(pd,'DLH%s.txt' % rawstats)
        RESEL_file=pjoin(pd,'RESELS%s.txt' % rawstats)
        THRESH_VOL_file=pjoin(pd,'thresh_%s.vol' % rawstats)
        if os.path.exists(pjoin(stats_dir, 'stats','smoothness')):
            log_cmd("grep DLH \"%s/stats/smoothness\" | awk '{ print $2 }' > \"%s\"" % (stats_dir,DLH_file), verbose=verbose, logger=logger)
            log_cmd("grep RESELS \"%s/stats/smoothness\" | awk '{ print $2 }' > \"%s\"" % (stats_dir,RESEL_file), verbose=verbose, logger=logger)
        else:
            log_cmd('$FSLDIR/bin/smoothest -m "%s/mask" -z "%s/stats/%s" > "%s/stats/%s.smoothness"' % (prep_dir,stats_dir,rawstats,stats_dir,rawstats), verbose=verbose, logger=logger)
            log_cmd("grep DLH \"%s/stats/%s.smoothness\" | awk '{ print $2 }' > \"%s\"" % (stats_dir,rawstats,DLH_file), verbose=verbose, logger=logger)
            log_cmd("grep RESELS \"%s/stats/%s.smoothness\" | awk '{ print $2 }' > \"%s\"" % (stats_dir,rawstats,RESEL_file), verbose=verbose, logger=logger)
        log_cmd("$FSLDIR/bin/fslstats \"%s\" -V | awk '{ print $1 }' > \"%s\"" % (THRESH_file,THRESH_VOL_file), verbose=verbose, logger=logger)
        DLH = np.genfromtxt(DLH_file)
        RESELS = np.genfromtxt(RESEL_file)
        VOL = np.genfromtxt(THRESH_VOL_file)
            
        #See around lines 5700 of featlib.tcl
        VOXorMM=''  #blank for voxels.  set to '--mm' if want mm instead.
        #cope_files = glob.glob(feat_dir + '/stats/cope*.nii*')
        
        if thresh==2: #cluster-based
            if zfstat_flag:
                log_cmd('$FSLDIR/bin/cluster -i "%s" -t %f -p %f -d %f --volume=%d --othresh="%s" -o "%s/cluster_mask_%s" --connectivity=26  --olmax=lmax_zstat1.txt %s --scalarname=Z > "%s/cluster_%s.txt"' % (THRESH_file,z_thresh,prob_thresh,DLH,VOL,THRESH_file,pd,rawstats,VOXorMM,pd,rawstats), verbose=verbose, logger=logger)
            else:
                log_cmd('$FSLDIR/bin/cluster -i "%s" -c "%s/stats/cope%d" -t %f -p %f -d %f --volume=%d --othresh="%s" -o "%s/cluster_mask_%s" --connectivity=26  --olmax="%s/lmax_zstat1.txt" %s --scalarname=Z > "%s/cluster_%s.txt"' % (THRESH_file,stats_dir,copeIDX,z_thresh,prob_thresh,DLH,VOL,THRESH_file,pd,rawstats,pd,VOXorMM,pd,rawstats), verbose=verbose, logger=logger)
        elif thresh==1: #voxel-based (FWE corrected)
            Nresels=VOL/float(RESELS)
            z_thr_cor=log_cmd('$FSLDIR/bin/ptoz %f -g %f' % (prob_thresh,Nresels), verbose=verbose, logger=logger)
            z_thr_cor=z_thr_cor.strip()
            log_cmd('$FSLDIR/bin/fslmaths "%s" -thr %s "%s"' % (THRESH_file,z_thr_cor,THRESH_file), verbose=verbose, logger=logger)
        elif thresh==0: #Uncorrected
            z_thr_uncor=log_cmd('$FSLDIR/bin/ptoz %f' % (prob_thresh), verbose=verbose, logger=logger)
            z_thr_uncor=z_thr_uncor.strip()
            log_cmd('$FSLDIR/bin/fslmaths "%s" -thr %s "%s"' % (THRESH_file,z_thr_uncor,THRESH_file), verbose=verbose, logger=logger)
        else:
            raise ValueError("Invalid thresh")

        min_max_file = pjoin(pd,rawstats+'_min_max.txt')
        log_cmd('$FSLDIR/bin/fslstats "%s" -l 0.0001 -R > "%s"' % (THRESH_file,min_max_file), verbose=verbose, logger=logger)
        zz=np.loadtxt(min_max_file)
        zmin=zz[0]; zmax=zz[1];
        if True: #cleanup temporary text files
            os.remove(DLH_file)
            os.remove(RESEL_file)
            os.remove(THRESH_VOL_file)        
            os.remove(min_max_file)        
    
        log_cmd('$FSLDIR/bin/overlay 1 0 "%s/example_func" -a "%s" %f %f "%s/rendered_thresh_%s"' % (prep_dir,THRESH_file,zmin,zmax,pd,rawstats), verbose=verbose, logger=logger)
        log_cmd('$FSLDIR/bin/slicer "%s/rendered_thresh_%s" -A 750 "%s/rendered_thresh_%s.png"' % (pd,rawstats,pd,rawstats), verbose=verbose, logger=logger)
    log_cmd('/bin/cp $FSLDIR/etc/luts/ramp.gif "%s/.ramp.gif"' % pd, verbose=verbose, logger=logger)


def cmind_fMRI_level1_poststats(output_dir, prep_dir, stats_dir, lvl1_stats_case="", feat_params=None, z_thresh=None,prob_thresh=None,thresh=None,output_tar=False, CMIND_specific_figures=False, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """Utility to run FEAT poststats processing equivalent
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    prep_dir : str
        specify location where cmind_fMRI_preprocess2.py output is located
    stats_dir : str
        specify location where stats output is already located
    lvl1_stats_case : str, optional
        may be used in future to apply different processing to different cases
    feat_params : str, optional
        filename to a .csv file containing feat parameters listed as key,value 
        pairs.  If z_thresh, prob_thresh, or thresh are explicitly passed in, 
        those values will override the ones in the feat_params file.  Typically
        feat_params should be the preproc_options_file output by 
        cmind_fMRI_preprocess2
    z_thresh : float, optional
        initial z threshold to use when determining clusters.  
        unused if thresh!=2
    prob_thresh : float, optional
        probability threshold for voxelwise or cluster significance
    thresh : {'uncorrected','voxel','cluster'}, optional
        0 = uncorrected, 1 = voxel-based (FWE corrected), 2 = cluster-based
    output_tar : bool, optional
        if true, generate .tar.gz versions of stats and poststats folders
    CMIND_specific_figures : bool, optional
        generate additional figures for the CMIND paradigm results.  set this to False if the data is not from the CMIND project
    generate_figures : bool,optional
        if true, generate overlay images summarizing the registration
    ForceUpdate : bool, optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    poststats_tarfile : str or None
        filename of the .tar.gz file containing the poststats output
    poststats_dir : str or None
        poststsats output folder
                        
    See Also
    --------
    cmind_fMRI_outliers
    
    Notes
    -----
    `cmind_fMRI_outliers` should be used to generate a file for `extra_confounds`
    FEAT stats subdirectory will be created as a softlink to `stats_dir`
    
    For the C-MIND study, CMIND-specific figures will be created for the following
    lvl1_stats_case values:
    Feat_BOLD_Stories
    Feat_BOLD_Sentences    
    Feat_ASL_Stories            
    Feat_ASL_Sentences 
           
    """

    import os
    from os.path import join as pjoin
#    import shutil
#    import glob
    import warnings
    
#    import numpy as np
    
#    from cmind.globals import cmind_annotateimg_cmd
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict, export_tarfile, cmind_copy_all #, rm_broken_links, cmind_copy_all
    from cmind.utils.image_utils import cmind_dual_func_overlay
#    from cmind.utils.fsf_utils import fsf_substitute
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    from cmind.pipeline.cmind_fMRI_level1_poststats import cmind_feat_poststats
     
    #TODO: implement, CSV, xml, JSON, or other filetypes for feat_params

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, output_tar = input2bool([generate_figures, ForceUpdate, verbose, output_tar])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Feat level-1 stats/postats")        
    
    if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
            
    if feat_params:
        if isinstance(feat_params,str):
            if os.path.exists(feat_params):
                #[f p ext]=fileparts(feat_params);
                #if(streq(ext,'.mat'))
                #    load(feat_params,'feat_params')
                #elif(streq(ext,'.txt'))
                #    feat_params_file=feat_params; clear feat_params
                feat_params=csv2dict(feat_params);
            else:
                raise IOError("feat_params must either be the feat parameter structure, or a path to the .txt file containing it")
        elif not isinstance(feat_params,dict):
            raise IOError("feat_params must either be the feat parameter structure, or a path to the .txt file containing it")

    if not feat_params:
        feat_params={}

    #First priority is the passed value, then the value in feat_params
    #and then a default if no feat_params file was given
    if z_thresh:
        feat_params['z_thresh']=z_thresh
    else:
        z_thresh=float(feat_params.get('z_thresh',3.09))
    if(z_thresh<0):
        raise ValueError("Invalid z_thresh<0 specified")

    if prob_thresh:
        feat_params['prob_thresh']=prob_thresh
    else:
        prob_thresh=float(feat_params.get('prob_thresh',0.05))
    if(prob_thresh<0):
        raise ValueError("Invalid prob_thresh<0 specified")

    if thresh is not None:
        if thresh=='uncorrected':
            thresh=0
        if thresh=='voxel':
            thresh=1
        if thresh=='cluster':
            thresh=2
        feat_params['thresh']=int(thresh)
    else:
        thresh=int(feat_params.get('thresh',2))   #0=uncorrected, 1=Voxel (FWE), 2=Cluster
    if thresh<0 or thresh>2:
        raise ValueError("invalid thresh argument specified.  must be 0, 1, or 2")
   
    poststats_dir = output_dir #pjoin(output_dir,'poststats')
                        
    if output_tar:
        poststats_tarfile = pjoin(poststats_dir,'poststats.tar.gz')
    else:
        poststats_tarfile = None

    lvl1_stats_case_substr = lvl1_stats_case.replace('Feat_','')
    
    filtfunc_froot=pjoin(prep_dir,'filtered_func_data')
    fexists=imexist(filtfunc_froot,'nifti-1')[0]
    if not fexists:  # and not os.path.exists('./%s' % lvl1_stats_case]); 
        raise IOError("Did not find filtered_func_data in the feat directory at {}".format(filtfunc_froot))
    
    if not os.path.exists(poststats_dir):
        os.makedirs(poststats_dir)  
                        
    if (not imexist(pjoin(poststats_dir, 'rendered_thresh_zstat1'),'nifti-1')[0]) or ForceUpdate:
        #[os.remove(f) for f in glob.glob(pjoin(poststats_dir,'*.txt'))]
        #[os.remove(f) for f in glob.glob(pjoin(poststats_dir,'*.nii'))]
        #[os.remove(f) for f in glob.glob(pjoin(poststats_dir,'*.nii.gz'))]
        #[os.remove(f) for f in glob.glob(pjoin(poststats_dir,'*.png'))]
                            
        cmind_feat_poststats(prep_dir,stats_dir,poststats_dir,z_thresh,prob_thresh,thresh, verbose=verbose, logger=logger)
        
        ts_dir=pjoin(poststats_dir,'tsplots')
        if not os.path.exists(ts_dir): 
            os.makedirs(ts_dir); 
        
        if False:
            try: 
                module_logger.info("Starting tsplots")
                
                #tsplot has to find various files from stats_dir to work properly, so temporarily link them all here
                all_links=cmind_copy_all(stats_dir, poststats_dir,match_str='*',use_symlinks=True, overwrite_existing=True)
                
                #run tsplot            
                log_cmd('$FSLDIR/bin/tsplot "%s" -f "%s" -o "%s"' % (poststats_dir,pjoin(prep_dir,'filtered_func_data.nii.gz'),ts_dir), verbose=verbose, logger=module_logger);
                
                #remove the links that were created
                for f in all_links: 
                    if os.path.islink(f):
                        os.unlink(f)
                temp_statsdir=pjoin(poststats_dir,'stats')
                if os.path.isdir(temp_statsdir) and os.listdir(temp_statsdir)==[]:
                    os.rmdir(temp_statsdir)
            except:
                warnings.warn("Failed to create tsplot report")
            
        if CMIND_specific_figures:
            try:
                [acq_type,paradigm]=lvl1_stats_case[lvl1_stats_case.find('_')+1::].split('_')
                BOLD_TE=35
                ASL_TE=11
                
                do_transparent=False
                slicer_width=750
                bg_image=pjoin(prep_dir,'example_func')
                
                if (lvl1_stats_case.find('BOLD')!=-1) or (lvl1_stats_case.find('EchoCombined')!=-1):
                    #BOLD component at TE=35
                    pos_con_img=pjoin(poststats_dir,'thresh_zstat1')
                    neg_con_img=pjoin(poststats_dir,'thresh_zstat2')
                    #output_vol=pjoin(poststats_dir,'copes1_2_posneg')
                    output_vol=pjoin(poststats_dir,"{}_TE{}_BOLD".format(paradigm,BOLD_TE))
                    html_out=None #pjoin(poststats_dir,'grl_report_bold_TE35.html')
                    conname=None #lvl1_stats_case_substr
                    title_string="{}:  BOLD Activation (TE={})".format(paradigm,BOLD_TE)
                else:
                    #BOLD component at TE=11
                    pos_con_img=pjoin(poststats_dir,'thresh_zstat1')
                    neg_con_img=pjoin(poststats_dir,'thresh_zstat4')
                    #output_vol=pjoin(poststats_dir,'copes1_4_posneg')
                    output_vol=pjoin(poststats_dir,"{}_TE{}_BOLD".format(paradigm,ASL_TE))
                    html_out=None #pjoin(poststats_dir,'grl_report_bold_TE11.html')
                    conname=None #lvl1_stats_case_substr + ' (BOLD component)'
                    title_string="{}:  BOLD Activation (TE={})".format(paradigm,ASL_TE)
                    cmind_dual_func_overlay(bg_image,
                                            pos_con_img,
                                            neg_con_img,
                                            output_vol,
                                            slice_skip=2,
                                            overlay_colorbars=True,
                                            title_str=title_string,
                                            html_out=html_out,
                                            conname=conname,
                                            do_transparent=do_transparent,
                                            slicer_width=slicer_width)
                                            
                    #ASL component at TE=35
                    html_out=pjoin(poststats_dir,'grl_report_bold_TE11.html')
                    conname=lvl1_stats_case_substr + ' (BOLD component)'
                    output_vol=pjoin(poststats_dir,"{}_TE{}_ASL".format(paradigm,ASL_TE))
                    pos_con_img=pjoin(poststats_dir,'thresh_zstat2')
                    neg_con_img=pjoin(poststats_dir,'thresh_zstat5')
                    html_out=None #pjoin(poststats_dir,'grl_report_asl_TE11.html')
                    conname=None #lvl1_stats_case_substr + ' (ASL component)'
                    title_string="{}:  ASL Activation (TE={})".format(paradigm,ASL_TE)
                    
    
                cmind_dual_func_overlay(bg_image,
                                        pos_con_img,
                                        neg_con_img,
                                        output_vol,
                                        slice_skip=2,
                                        overlay_colorbars=True,
                                        title_str=title_string,
                                        html_out=html_out,
                                        conname=conname,
                                        do_transparent=do_transparent,
                                        slicer_width=slicer_width)
            
            except:
                warnings.warn("Failed to create CMIND-specific Feat level1 figures")
    
    if poststats_tarfile:
        if (not os.path.exists(poststats_tarfile)) or ForceUpdate:
            export_tarfile(poststats_tarfile, poststats_dir, arcnames='') #tar poststats_dir into output_tar

    #print any filename outputs for capture by LONI
    print("poststats_tarfile:{}".format(poststats_tarfile))
    print("poststats_dir:{}".format(poststats_dir))
    return (poststats_tarfile, 
            poststats_dir)

def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    output_tar=True
    #output_tar=False
    generate_figures=True
    CMIND_specific_figures=True
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',
                        file_mode='w')  
    if verbose:
        func=cmind_timer(cmind_fMRI_level1_poststats,logger=logger)
    else:
        func=cmind_fMRI_level1_poststats
    
    z_thresh=None
    prob_thresh=None
    thresh=None
    paradigms=['Stories'] #['Sentences','Stories']
    acq_types=['BOLD'] #['ASL','BOLD']
    for paradigm in paradigms:
        for acq_type in acq_types:
            output_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s_poststats' % acq_type)
            prep_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s' % acq_type)
            stats_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s_stats' % acq_type)
            lvl1_stats_case='Feat_%s_%s' % (acq_type, paradigm)
            feat_params=pjoin(prep_dir,'cmind_fMRI_preprocess2_options.csv')

            
            func(output_dir, 
                 prep_dir=prep_dir, 
                 stats_dir=stats_dir,                 
                 lvl1_stats_case=lvl1_stats_case, 
                 feat_params=feat_params, 
                 z_thresh=z_thresh, prob_thresh=prob_thresh, thresh=thresh, 
                 output_tar=output_tar, 
                 CMIND_specific_figures=CMIND_specific_figures, 
                 generate_figures=generate_figures, 
                 ForceUpdate=ForceUpdate,
                 verbose=verbose,logger=logger)
    
    
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Utility to run FEAT stats/poststats processing equivalent", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-prepdir","--prep_dir", required=False, type=str, default="None", help="specify location where cmind_fMRI_preprocess2.py output is located")
        parser.add_argument("-statsdir","--stats_dir", required=False, type=str, default="None", help="specify location where stats output should go (or is already located)")
        parser.add_argument("-p","--lvl1_stats_case", help="[Feat_BOLD_Sentences,Feat_BOLD_Stories,Feat_ASL_Sentences,Feat_ASL_Stories]; may be used in future to apply different processing to different cases")
        parser.add_argument("-feat","--feat_params", type=str, help="        filename to a .csv file containing feat parameters listed as key,value pairs.  If z_thresh, prob_thresh, or thresh are explicitly passed in, those values will override the ones in the feat_params file.  Typically feat_params should be the preproc_options_file output by cmind_fMRI_preprocess2")
        #parser.add_argument("--preproc_params", type=str, default="None", help="dictionary (or .csv filename) containing keys for 'smooth' and 'paradigm_hp' that will override the values in feat_params.csv if provided")
        parser.add_argument("--z_thresh", type=str, default="None", help="initial z threshold to use when determining clusters. unused if thresh!=2")
        parser.add_argument("--prob_thresh", type=str, default="None", help="probability threshold for voxelwise or cluster significance")
        parser.add_argument("--thresh", type=str, default="None", help="{0,1,2} 0 = uncorrected, 1 = voxel-based (FWE corrected), 2 = cluster-based")
        parser.add_argument("-outtar","--output_tar", type=str, default="false", help="if true, generate .tar.gz versions of stats and poststats folders")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate overlay images summarizing the registration")
        parser.add_argument("--CMIND_specific_figures",type=str,default="False", help="generate additional figures for the CMIND paradigm results.  set this to False if the data is not from the CMIND project")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
                
        args=parser.parse_args()
    
        output_dir=args.output_dir
        prep_dir=str2none(args.prep_dir)
        stats_dir=str2none(args.stats_dir)
        lvl1_stats_case=args.lvl1_stats_case
        feat_params=args.feat_params
        #preproc_params=str2none(args.preproc_params)
        z_thresh=str2none(args.z_thresh)
        prob_thresh=str2none(args.prob_thresh)
        thresh=str2none(args.thresh)
        output_tar=args.output_tar
        generate_figures=args.generate_figures
        CMIND_specific_figures=args.CMIND_specific_figures
        ForceUpdate=args.ForceUpdate
        verbose=args.verbose
        logger=args.logger

        if verbose:
            cmind_fMRI_level1_poststats=cmind_timer(cmind_fMRI_level1_poststats,logger=logger)
    
        cmind_fMRI_level1_poststats(output_dir, 
                                prep_dir=prep_dir, 
                                stats_dir=stats_dir,
                                lvl1_stats_case=lvl1_stats_case, 
                                feat_params=feat_params, 
                                z_thresh=z_thresh, 
                                prob_thresh=prob_thresh, 
                                thresh=thresh, 
                                output_tar=output_tar, 
                                CMIND_specific_figures=CMIND_specific_figures, 
                                generate_figures=generate_figures, 
                                ForceUpdate=ForceUpdate, 
                                verbose=verbose, 
                                logger=logger)
        