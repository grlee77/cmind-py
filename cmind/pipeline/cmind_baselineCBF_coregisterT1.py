#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

#TODO: add NRMSE_map_nii to LONI pipeline

def cmind_baselineCBF_coregisterT1(output_dir,CBF_nii,T1concat_N4_brain_nii,T1_map_nii, I0_map_nii, T1_NRMSE_map_nii=None, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """coregister T1map images to CBF
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    CBF_nii : str
        filename of Baseline CBF volume to register to
    T1concat_N4_brain_nii : str
        filename containing the brain extracted 4D TI image series used during 
        T1 fitting
    T1_map_nii : str
        filename containing the T1_map volume to register
    I0_map_nii : str
        filename containing the I0_map (M0) volume to register
    T1_NRMSE_map_nii : str or None, optional  
        filename containing the T1 fit NRMSE_map volume to register
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
             
    Returns
    -------
    T1map_reg2CBF : str
        filename of the T1_map volume that has been registered to `CBF_nii`
    I0map_reg2CBF : str
        filename of the I0_map volume that has been registered to `CBF_nii`
    T1_NRMSE_map_reg2CBF : str or None
        filename of the NRMSE map that has been registered to `CBF_nii`
    T1map2CBF_affine : str
        affine transform of the T1_map to the BaselineCBF image
        
    Notes
    -----
    
    .. figure::  ../img/IRC04H_06M008_P_1_T1map_to_CBF_reg.png
       :align:   center
       :scale:   66%
       
       T1map image registered to the BaselineCBF volume

        
    """
    
    import os
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, filecheck_bool
    from cmind.utils.cmind_utils import cmind_reg_report_img
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    
    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting coregistration of T1map to BaselineCBF")
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    imbool, CBF_nii = imexist(CBF_nii,'nifti-1')[0:2]
    if not imbool:
        raise IOError("specified CBF image, %s, doesn't exist!" % (CBF_nii))
        #raise EnvironmentError("File not found")
    if not imexist(T1_map_nii,'nifti-1')[0]:
        raise IOError("specified T1 map image, %s, doesn't exist!" % (T1_map_nii))
    if not imexist(I0_map_nii,'nifti-1')[0]:
        raise IOError("specified I0 map image, %s, doesn't exist!" % (I0_map_nii))
    (T1concat_exists, T1concat_true_fname, T1concat_root, T1concat_path, T1concat_ext)=imexist(T1concat_N4_brain_nii,'nifti-1')
    if not T1concat_exists:
        raise IOError("specified T1 brain image, %s, doesn't exist!" % (T1concat_N4_brain_nii))      
    #(T1path,T1concat_root,ext)=fileparts(T1concat_N4_brain_nii);
    
    T1map_reg2CBF=os.path.join(output_dir,'T1_map_reg2CBF.nii.gz')
    I0map_reg2CBF=os.path.join(output_dir,'I0_map_reg2CBF.nii.gz')
    if T1_NRMSE_map_nii:
        T1_NRMSE_map_reg2CBF=os.path.join(output_dir,'T1_NRMSE_map_reg2CBF.nii.gz')
    else:
        T1_NRMSE_map_reg2CBF=None
            
    temp_img=os.path.join(output_dir,'%s_reg2CBF' % T1concat_root)
    T1map2CBF_affine=os.path.join(output_dir,'flirt_T1concat2CBF.mat')
    
    if not imexist(T1map_reg2CBF,'nifti-1')[0] or ForceUpdate:
        #Register the T1concat_N4_brain_nii image to the CBF_nii volume since these are more similar in contrast.
        log_cmd('$FSLDIR/bin/flirt -ref "%s" -in "%s" -out "%s" -omat "%s" -cost corratio -dof 6 -searchrx -60 60 -searchry -60 60 -searchrz -60 60 -interp sinc' % (CBF_nii,T1concat_true_fname,temp_img, T1map2CBF_affine), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/imrm "%s"' % (temp_img)) #remove the unneeded output image
        
        #Apply the computed registration parameters to the T1_map, I0_map and NRMSE_map
        log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" --premat="%s"  --interp=spline' % (I0_map_nii,CBF_nii,I0map_reg2CBF,T1map2CBF_affine), verbose=verbose, logger=module_logger)
        log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" --premat="%s"  --interp=spline' % (T1_map_nii,CBF_nii,T1map_reg2CBF,T1map2CBF_affine), verbose=verbose, logger=module_logger)
        if T1_NRMSE_map_nii:
            log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" --premat="%s"  --interp=spline' % (T1_NRMSE_map_nii,CBF_nii,T1_NRMSE_map_reg2CBF,T1map2CBF_affine), verbose=verbose, logger=module_logger)
    
    if generate_figures:
        output_img=os.path.join(output_dir,'T1map_to_CBF_reg.png')
        if filecheck_bool(CBF_nii,output_img):
            cmind_reg_report_img(CBF_nii,T1map_reg2CBF,output_img)
        
    #print any filename outputs for capture by LONI
    print("T1map_reg2CBF:{}".format(T1map_reg2CBF))
    print("I0map_reg2CBF:{}".format(I0map_reg2CBF))
    print("T1_NRMSE_map_reg2CBF:{}".format(T1_NRMSE_map_reg2CBF))
    print("T1map2CBF_affine:{}".format(T1map2CBF_affine))
    return (T1map_reg2CBF, I0map_reg2CBF, T1_NRMSE_map_reg2CBF, T1map2CBF_affine)
        
def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    output_dir=os.path.join(cmind_example_output_dir,'P','BaselineCBF')
    CBF_nii=os.path.join(output_dir,'MeanSub_BaselineCBF_N4.nii.gz')
    T1concat_N4_brain_nii=os.path.join(output_dir,'..','T1map','T1concat_mean_N4_brain.nii.gz')
    T1_map_nii=os.path.join(output_dir,'..','T1map','T1_map.nii.gz')
    I0_map_nii=os.path.join(output_dir,'..','T1map','I0_map.nii.gz')
    T1_NRMSE_map_nii=os.path.join(output_dir,'..','T1map','T1_NRMSE_map.nii.gz')
    generate_figures=True;
    ForceUpdate=True;
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
 
    if verbose:
        func=cmind_timer(cmind_baselineCBF_coregisterT1,logger=logger)
    else:
        func=cmind_baselineCBF_coregisterT1

    func(output_dir,CBF_nii,T1concat_N4_brain_nii,T1_map_nii,I0_map_nii,
         T1_NRMSE_map_nii=T1_NRMSE_map_nii,generate_figures=generate_figures,
         ForceUpdate=ForceUpdate,verbose=verbose,logger=logger)        
       
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:

        parser = argparse.ArgumentParser(description="coregister T1map images to CBF", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-cbf","--CBF_nii",required=True, help="filename of Baseline CBF volume to register to")
        parser.add_argument("-T1concat","--T1concat_N4_brain_nii",required=True, help="filename containing the brain extracted 4D TI image series used during T1 fitting")
        parser.add_argument("-t1map","--T1_map_nii",required=True, help="filename containing the T1_map volume to register")
        parser.add_argument("-M0map","--I0_map_nii",required=True, help="filename containing the I0_map (M0) volume to register")
        parser.add_argument("-nrmse_map","--T1_NRMSE_map_nii", help="filename containing the T1 fit NRMSE_map volume to register")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        output_dir=args.output_dir;
        CBF_nii=args.CBF_nii;
        T1concat_N4_brain_nii=args.T1concat_N4_brain_nii;
        T1_map_nii=args.T1_map_nii;
        I0_map_nii=args.I0_map_nii;
        T1_NRMSE_map_nii=args.T1_NRMSE_map_nii;
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        logger=args.logger
        verbose=args.verbose
        
            
        if verbose:
            cmind_baselineCBF_coregisterT1=cmind_timer(cmind_baselineCBF_coregisterT1,logger=logger)
    
        cmind_baselineCBF_coregisterT1(output_dir,CBF_nii,
                                       T1concat_N4_brain_nii,T1_map_nii,
                                       I0_map_nii,
                                       T1_NRMSE_map_nii=T1_NRMSE_map_nii,
                                       generate_figures=generate_figures,
                                       ForceUpdate=ForceUpdate,
                                       verbose=verbose,logger=logger)        