The folder structure is as follows:

/cmind - base package folder

/cmind/bin - bash shell script utilities used by some of the python modules

/cmind/cmind - cmind python module code
/cmind/cmind/extern - external modules
/cmind/cmind/pipeline - python/command line pipeline modules
/cmind/cmind/pipeline_devel - python/command line pipeline modules that are still in the early stages of development
/cmind/cmind/nipype - nipype interfaces to the modules in /cmind/cmind/pipeline
/cmind/cmind/utils - common python utilities used across multiple pipeline modules
/cmind/cmind/data - example data from a single subject

/cmind/doc - project documentation

/cmind/study_specific/LONI_shell - shell script wrappers to cmind python modules for use with LONI.  Some values are hardcoded for the C-MIND study
/cmind/study_specific/ASLBOLD - FSL Feat .fsf templates and slice timing information for the C-MIND Stories and Sentences paradigms
/cmind/study_specific/DTIPrep_protocols - bval, bvec files and DTIPrep templates for the C-MIND study

To build the package locally without installing:
python setup.py build

installation can be done via:  (superuser priveleges may be requied)
python setup.py install 

A custom install location can be chosen by adding the --prefix option (e.g. --prefix=/usr/local)


To build the documentation:
move into the /cmind/doc folder and run:

sphinx-autogen -o generated *.rst

Then to build HTML documentation run:
make html

for PDF documentation:
make latexpdf

