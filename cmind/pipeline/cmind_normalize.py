#!/usr/bin/env python
from __future__ import division, print_function, absolute_import


#TODO: Run FLIRT, FNIRT, ANTS registrations in parallel
#TODO: set ITK environment variable to force ANTs calls to be limited to the specified number of cores
#      ANTS already uses multiple cores, though

def cmind_normalize(output_dir,T1_volume_nii,reg_struct,direct_to_MNI=False,reg_flag='101',ANTS_path=None,MNI_path=None,ANTS_reg_case='SynAff',ANTS_precision='lenient',MNI_resolution='2mm',generate_figures=True,ForceUpdate=False,verbose=False,logger=None):
    """Normalize a structural image to MNI space
    
    Parameters
    ----------
    output_dir : str
        directory in which structural processing results are stored
    T1_volume_nii : str
        T1W image (after brain extraction).  This is the image to register to
        standard space
    reg_struct : str or dict
        filename of the .csv file containing the registration dictionary
    direct_to_MNI : bool, optional
        if True, bypass study template and register directly to the MNI standard
    reg_flag : str or int, optional
        string of 3 characters controlling whether FLIRT, FNIRT and/or ANTS registrations of T1 to standard are to be run
        doFLIRT = 0 or 1  (1 = do the regisration using FLIRT)
        doFNIRT = 0 or 1  (1 = do the regisration using FNIRT) (NOT CURRENTLY IMPLEMENTED)
        doANTS = 0, 1 or 2  (0 = skip, 1 = do using Exp, 2 = do using SyN)
        default = '101' does FLIRT and ANTs (Exp)
    ANTS_path : str, optional
        path to the ANTs binaries
    MNI_path : str, optional
        path containing the MNI standard brains
    ANTS_reg_case : {'Aff','SyN','AffSyn','SynAff'}, optional
        Type of ANTS registration
    ANTS_precision : {'lenient','ants_defaults','strict'}, optional
        choose from preset precision levels for the registrations
        (default = 'lenient')
    MNI_resolution : {'2mm','1mm'}, optional
        resolution of MNI standard space image to register 
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
            
    Returns
    -------
    
    reg_struct : dict
        dictionary corresponding to the registration structure
    reg_struct_file : file
        filename of the registration structure
    output_files : list
        list of registered volumes
            
    See Also
    --------
    ants_register
    ants_applywarp
    
    Notes
    -----
    `ANTS_reg_case` values correspond to:
        - 'Aff',    HR --Affine--> StudyTemplate --Affine(precalculated)--> MNI
        - 'SyN',    HR --SyN--> StudyTemplate --SyN(precalcalculated)--> MNI
        - 'AffSyN', HR --Affine--> StudyTemplate --SyN(precalcalculated)--> MNI
        - 'SyNAff', HR --SyN--> StudyTemplate --Affine(precalculated)--> MNI
   
    SyNAff is recommended to avoid overwarping the volumes.  This will nonlinearly
    register each subject's structural to the StudyTemplate.  The StudyTemplate's
    precomputed affine transform to MNI space is then applied.
    
    .. figure::  ../img/T1_to_MNI_lin.png
       :align:   center
       :scale:   50%
       
       example linear registration of T1 structural to MNI using FLIRT

    .. figure::  ../img/T1_to_MNI_ANTS_SyN_Deformed.png
       :align:   center
       :scale:   50%

       example non-linear registration of T1 structural to MNI using ANTs SyN
    
    """

    import os
    from os.path import join as pjoin
    import shutil
    import glob
    import tempfile
    from time import time
    
    import numpy as np
    
    from cmind.pipeline.ants_applywarp import ants_applywarp
    from cmind.pipeline.ants_register import ants_register
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict, dict2csv
    from cmind.utils.cmind_utils import cmind_reg_report_img
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, direct_to_MNI = input2bool([generate_figures, ForceUpdate, verbose, direct_to_MNI])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Normalization")      

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    if isinstance(reg_flag,str):
        if len(reg_flag)==1:
            reg_flag=int(reg_flag);
            reg_flag=bin(reg_flag)[2::].zfill(3);
    
    if len(reg_flag)==3:  #should work for str, list, or ndarray input
        reg_flag_array=np.zeros((3,),int); 
        reg_flag_array[0]=int(reg_flag[0])
        reg_flag_array[1]=int(reg_flag[1])
        reg_flag_array[2]=int(reg_flag[2])
    else:
        raise ValueError("reg_flag should be a string, list or ndarray containing three integer values")
    
    if not ANTS_path:
        from cmind.globals import cmind_ANTs_dir as ANTS_path
    
    if not MNI_path:
        from cmind.globals import cmind_MNI_dir as MNI_path
    
    if generate_figures:
        nopng=False
    else:
        nopng=True
    
    ANTS_interp_type=""
    
    if isinstance(reg_struct,str):
        if os.path.exists(reg_struct):
            reg_struct=csv2dict(reg_struct)
        else:
            raise IOError("reg_struct must either be the registration dictionary, reg_struct, or a path to the .csv file containing it")

    elif not isinstance(reg_struct,dict):
        raise IOError("reg_struct must either be the registration dictionary, reg_struct, or a path to the .csv file containing it")
    
    reg_struct['T1proc_dir'] = output_dir;
    if not T1_volume_nii:
        T1_volumes=glob.glob('*T1W_3D*_N*_brain.nii.gz');
        if len(T1_volumes)!=1:
            raise IOError("unable to find appropriate T1W image for registration")
        else:
            T1_volume_nii=T1_volumes[0];
    
    (T1exists, T1_volume_nii)=imexist(T1_volume_nii)[0:2]
    
    if not T1exists:
        raise IOError("Error! specified T1_volume_nii: %s not found" % (T1_volume_nii))
        
    reg_struct['HR_img']=T1_volume_nii;

    reg_struct['MNI_standard']=pjoin(MNI_path,'MNI152_T1_' + MNI_resolution + '.nii.gz');
    reg_struct['MNI_standard_brain']=pjoin(MNI_path,'MNI152_T1_' + MNI_resolution + '_brain.nii.gz');
    reg_struct['MNI_standard_brain_mask']=pjoin(MNI_path,'MNI152_T1_' + MNI_resolution + '_brain_mask.nii.gz');
    if (not imexist(reg_struct['MNI_standard'],'nifti-1')[0]) or (not imexist(reg_struct['MNI_standard_brain'],'nifti-1')[0]) or (not imexist(reg_struct['MNI_standard_brain_mask'],'nifti-1')[0]):
        raise IOError("Error specified MNI standards:  %s, %s, %s  don't exist!" % (reg_struct['MNI_standard'],reg_struct['MNI_standard_brain'],reg_struct['MNI_standard_brain_mask']))
            
    reg_struct_file=pjoin(output_dir,'reg_struct.csv')            
    dict2csv(reg_struct,reg_struct_file)
    
    flirt_flag=reg_flag_array[0]
    fnirt_flag=reg_flag_array[1]
    ANTS_flag=reg_flag_array[2]    #0=none, 1= Exp, 2=SyN
    if verbose:
        module_logger.debug("flirt_flag, fnirt_flag, ANTS_flag = %d, %d, %d" % (flirt_flag,fnirt_flag, ANTS_flag))
    
    if generate_figures:
        working_dir=tempfile.mkdtemp()  
        rm_working_dir=True
    else:
        rm_working_dir=False
    
    if not T1_volume_nii:
        register_brain=False
    else:
        register_brain=True
    
    use_cerebrum_mask=False  #TODO: add as input argument
    if register_brain:
        if use_cerebrum_mask:
#            reg_struct['MNI_standard_cerebrum']=
            if direct_to_MNI:
                MNI_fixed=reg_struct['MNI_standard_cerebrum'];
            else:
                MNI_fixed=reg_struct['MNI_standard_cerebrum'];
                StudyTemplate_fixed=reg_struct['StudyTemplate_cerebrum'];
        else:
            if direct_to_MNI:
                MNI_fixed=reg_struct['MNI_standard_brain'];
            else:
                MNI_fixed=reg_struct['MNI_standard_brain'];
                StudyTemplate_fixed=reg_struct['StudyTemplate_brain'];
    else:
        if direct_to_MNI:
            MNI_fixed=reg_struct['MNI_standard'];
        else:
            MNI_fixed=reg_struct['MNI_standard'];
            StudyTemplate_fixed=reg_struct['StudyTemplate'];
    
    output_files=[]
    if (flirt_flag or fnirt_flag):
        starttime = time()
        reg_struct['FLIRT_HR_to_MNI_img']=pjoin(reg_struct['T1proc_dir'],'T1_to_MNI_lin.nii.gz');
        reg_struct['FLIRT_HR_to_MNI_Aff']=pjoin(reg_struct['T1proc_dir'],'T1_to_MNI_lin.mat');
        output_files.append(reg_struct['FLIRT_HR_to_MNI_img'])  #TODO: also list the generated transforms?
        
        if direct_to_MNI:
            if (not os.path.exists(reg_struct['FLIRT_HR_to_MNI_Aff'])) or ForceUpdate:
                shell_cmd='$FSLDIR/bin/flirt -in "%s" -ref "%s" -omat "%s" -out "%s" -dof 12 -searchrx -30 30 -searchry -30 30 -searchrz -30 30 -interp sinc' % (reg_struct['HR_img'], MNI_fixed, reg_struct['FLIRT_HR_to_MNI_Aff'], reg_struct['FLIRT_HR_to_MNI_img'])
                log_cmd(shell_cmd, verbose=verbose, logger=logger)

                if generate_figures:
                    cmind_reg_report_img(reg_struct['FLIRT_HR_to_MNI_img'],MNI_fixed, pjoin(output_dir,'T1_to_MNI_lin.png'),working_dir); #, working_dir,fewer_flag)
        else:
            reg_struct['FLIRT_HR_to_StudyTemplate_img']=pjoin(reg_struct['T1proc_dir'],'T1_to_StudyTemplate_lin.nii.gz');
            reg_struct['FLIRT_HR_to_StudyTemplate_Aff']=pjoin(reg_struct['T1proc_dir'],'T1_to_StudyTemplate_lin.mat');
    
            output_files.append(reg_struct['FLIRT_HR_to_StudyTemplate_img'])  #TODO: also list the generated transforms?
            
            if (not os.path.exists(reg_struct['FLIRT_HR_to_StudyTemplate_Aff'])) or ForceUpdate:
                shell_cmd='$FSLDIR/bin/flirt -in "%s" -ref "%s" -omat "%s" -out "%s" -dof 12 -searchrx -30 30 -searchry -30 30 -searchrz -30 30 -interp sinc' % (reg_struct['HR_img'],StudyTemplate_fixed,reg_struct['FLIRT_HR_to_StudyTemplate_Aff'],reg_struct['FLIRT_HR_to_StudyTemplate_img'])
                log_cmd(shell_cmd, verbose=verbose, logger=logger)
                if generate_figures:
                    cmind_reg_report_img(reg_struct['FLIRT_HR_to_StudyTemplate_img'],StudyTemplate_fixed, pjoin(output_dir,'T1_to_StudyTemplate_lin.png'),working_dir); #, working_dir,fewer_flag)
    
            # Transform StudyTemplate standard space to MNI space.
            if(not os.path.exists(reg_struct['FLIRT_HR_to_MNI_img'])) or ForceUpdate:
                if(os.path.exists(reg_struct['FLIRT_HR_to_StudyTemplate_img'])):
                    shell_cmd='$FSLDIR/bin/convert_xfm -omat "%s" -concat "%s" "%s"' % (reg_struct['FLIRT_HR_to_MNI_Aff'],reg_struct['FLIRT_StudyTemplate_to_MNI_Aff'],reg_struct['FLIRT_HR_to_StudyTemplate_Aff'])
                    log_cmd(shell_cmd, verbose=verbose, logger=logger)
                    shell_cmd='$FSLDIR/bin/flirt -ref "%s" -in "%s" -out "%s" -applyxfm -init "%s" -interp sinc' % (reg_struct['MNI_standard'],reg_struct['HR_img'],reg_struct['FLIRT_HR_to_MNI_img'],reg_struct['FLIRT_HR_to_MNI_Aff'])
                    log_cmd(shell_cmd, verbose=verbose, logger=logger)
                    if generate_figures:
                        cmind_reg_report_img(reg_struct['FLIRT_HR_to_MNI_img'],MNI_fixed, pjoin(output_dir,'T1_to_MNI_lin.png'), working_dir) #, working_dir,fewer_flag)
                    #cmind_reg_report_img(reg_struct['FLIRT_HR_to_oldpstd_img,reg_struct['oldpstd_ref, pjoin(output_dir,'T1_to_oldpstd_lin.png'), working_dir) %, working_dir,fewer_flag)
        
        #calculate the inverse MNI->T1 transform
        reg_struct['FLIRT_HR_to_MNI_Inv_Aff']=pjoin(reg_struct['T1proc_dir'],'MNI_to_T1_lin.mat');
        if (not os.path.exists(reg_struct['FLIRT_HR_to_MNI_Inv_Aff'])) or ForceUpdate:
            shell_cmd='$FSLDIR/bin/convert_xfm -inverse -omat "%s" "%s"' %(reg_struct['FLIRT_HR_to_MNI_Inv_Aff'],reg_struct['FLIRT_HR_to_MNI_Aff']);
            log_cmd(shell_cmd, verbose=verbose, logger=logger)
        module_logger.info('FLIRT registration, time elapsed: {}'.format(time() - starttime))
    
    # FNIRT T1 to StudyTemplate space
    if fnirt_flag:
        starttime = time()
        #tic; 
        reg_struct['nonFLIRT_HR_to_StudyTemplate_img']=pjoin(reg_struct['T1proc_dir'],'T1_to_StudyTemplate_nonlin.nii.gz');
        reg_struct['nonFLIRT_HR_to_StudyTemplate_Warp']=pjoin(reg_struct['T1proc_dir'],'T1_to_StudyTemplate_nonlin_field.nii.gz');
        reg_struct['nonFLIRT_HR_to_MNI_img']=pjoin(reg_struct['T1proc_dir'],'T1_to_MNI_nonlin.nii.gz');
        reg_struct['nonFLIRT_HR_to_MNI_Warp']=pjoin(reg_struct['T1proc_dir'],'T1_to_MNI_nonlin_field.nii.gz');
        
        output_files.append(reg_struct['nonFLIRT_HR_to_MNI_img'])
            
        if direct_to_MNI:
            if (not os.path.exists(reg_struct['nonFLIRT_HR_to_MNI_Warp'])) or ForceUpdate:
                #Nonlinear Registration to the standard
                shell_cmd='$FSLDIR/bin/fslmaths %s -fillh -dilF %s' % (pjoin(output_dir,'standard_brain_mask'),pjoin(output_dir,'standard_brain_mask_dil1'))
                log_cmd(shell_cmd, verbose=verbose, logger=logger)
                
                shell_cmd='$FSLDIR/bin/fnirt --in="%s" --ref="%s" --fout="%s" --iout="%s" --logout=T1_to_MNI_nonlin.txt --aff="%s" --refmask="%s"' % (reg_struct['HR_img'], MNI_fixed, reg_struct['nonFLIRT_HR_to_MNI_Warp'], reg_struct['nonFLIRT_HR_to_MNI_img'], reg_struct['FLIRT_HR_to_MNI_Aff'],pjoin(output_dir,'standard_brain_mask_dil1'));
                log_cmd(shell_cmd, verbose=verbose, logger=logger)
                if generate_figures:
                    cmind_reg_report_img(reg_struct['nonFLIRT_HR_to_MNI_img'],MNI_fixed, pjoin(output_dir,'T1_to_MNI_nonlin.png'),working_dir) #, working_dir,fewer_flag)
        else:
            output_files.append(reg_struct['nonFLIRT_HR_to_StudyTemplate_img'])

            if (not os.path.exists(reg_struct['nonFLIRT_HR_to_StudyTemplate_Warp'])) or ForceUpdate:
                #Nonlinear Registration to the standard
                shell_cmd='$FSLDIR/bin/fslmaths "%s" -fillh -dilF "%s_dil1"' % (reg_struct['StudyTemplate_brain_mask'],reg_struct['StudyTemplate_brain_mask'])
                log_cmd(shell_cmd, verbose=verbose, logger=logger)
                shell_cmd='$FSLDIR/bin/fnirt --in="%s" --ref="%s" --fout="%s" --iout="%s" --aff="%s" --refmask="%s_dil1"' % (reg_struct['HR_img'], StudyTemplate_fixed, reg_struct['nonFLIRT_HR_to_StudyTemplate_Warp'], reg_struct['nonFLIRT_HR_to_StudyTemplate_img'], reg_struct['FLIRT_HR_to_StudyTemplate_Aff'],reg_struct['StudyTemplate_brain_mask']);
                log_cmd(shell_cmd, verbose=verbose, logger=logger)
                if generate_figures:
                    cmind_reg_report_img(reg_struct['nonFLIRT_HR_to_StudyTemplate_img'],StudyTemplate_fixed, pjoin(output_dir,'T1_to_StudyTemplate_nonlin.png'),working_dir) #, working_dir,fewer_flag)
        
        #concatenate the T1->pstd and pstd->MNI warps
        if (not os.path.exists(reg_struct['nonFLIRT_HR_to_MNI_img'])) or ForceUpdate:
            shell_cmd='$FSLDIR/bin/convertwarp --ref="%s" --warp1="%s" --warp2="%s" --out="%s"' % (reg_struct['MNI_standard'],reg_struct['nonFLIRT_HR_to_StudyTemplate_Warp'],reg_struct['nonFLIRT_StudyTemplate_to_MNI_Warp'],reg_struct['nonFLIRT_HR_to_MNI_Warp'])
            log_cmd(shell_cmd, verbose=verbose, logger=logger)
            if os.path.exists(reg_struct['nonFLIRT_HR_to_StudyTemplate_img']):
                shell_cmd='$FSLDIR/bin/applywarp -r "%s" -i "%s" -w "%s" -o "%s"' % (reg_struct['MNI_standard'],reg_struct['HR_img'],reg_struct['nonFLIRT_HR_to_MNI_Warp'],reg_struct['nonFLIRT_HR_to_MNI_img'])
                log_cmd(shell_cmd, verbose=verbose, logger=logger)
                if generate_figures:
                    cmind_reg_report_img(reg_struct['nonFLIRT_HR_to_MNI_img'],MNI_fixed, pjoin(output_dir,'T1_to_MNI_nonlin.png'), working_dir) #, working_dir,fewer_flag)
        module_logger.info('FNIRT registration, time elapsed: {}'.format(time() - starttime))

    
    
    # Transform from subject space to MNI space using ANTS non-linear transformation
    #tic;
    if ANTS_flag==1:
        starttime = time()
        reg_struct['ANTS_HR_to_MNI_img']=pjoin(reg_struct['T1proc_dir'],'T1_to_MNI_ANTS_SyN_Deformed.nii.gz');
        output_files.append(reg_struct['ANTS_HR_to_MNI_img'])
        
        moving=reg_struct['HR_img'];
        T1proc_dir_ANTS=reg_struct['T1proc_dir'];
        
        if direct_to_MNI:  #direct warp to MNI space without intermediate StudyTemplate template(s)
            fixed=MNI_fixed;
            reg_struct['ANTS_HR_to_MNI_Aff']=pjoin(reg_struct['T1proc_dir'],'T1_to_MNI_ANTS_SyN_0GenericAffine.mat');
            reg_struct['ANTS_HR_to_MNI_Warp']=pjoin(reg_struct['T1proc_dir'],'T1_to_MNI_ANTS_SyN_1Warp.nii.gz');
            reg_struct['ANTS_HR_to_MNI_Inv_Warp']=pjoin(reg_struct['T1proc_dir'],'T1_to_MNI_ANTS_SyN_1InverseWarp.nii.gz');
            output_root=pjoin(output_dir,'T1_to_MNI_ANTS_SyN_');
            output_files.append(reg_struct['ANTS_HR_to_MNI_img'])
            
        else: #warp to study template.  assumes StudyTemplate to MNI transform was precomputed
            reg_struct['ANTS_HR_to_StudyTemplate_img']=pjoin(reg_struct['T1proc_dir'],'T1_to_StudyTemplate_ANTS_SyN_Deformed.nii.gz');
            reg_struct['ANTS_HR_to_StudyTemplate_Aff']=pjoin(reg_struct['T1proc_dir'],'T1_to_StudyTemplate_ANTS_SyN_0GenericAffine.mat');
            reg_struct['ANTS_HR_to_StudyTemplate_Warp']=pjoin(reg_struct['T1proc_dir'],'T1_to_StudyTemplate_ANTS_SyN_1Warp.nii.gz');
            reg_struct['ANTS_HR_to_StudyTemplate_Inv_Warp']=pjoin(reg_struct['T1proc_dir'],'T1_to_StudyTemplate_ANTS_SyN_1InverseWarp.nii.gz');
            fixed=StudyTemplate_fixed;
            output_root=pjoin(output_dir,'T1_to_StudyTemplate_ANTS_SyN_');
            output_files.append(reg_struct['ANTS_HR_to_StudyTemplate_img'])
        
        module_logger.info("expect ANTs output: {}.  already exists? {}".format(output_files[-1],imexist(output_files[-1])[0]))
        if (not imexist(output_files[-1])[0]) or ForceUpdate:
        
            (warp_file, aff_file, output_warped)=ants_register(fixed,
                                                                moving,
                                                                T1proc_dir_ANTS, 
                                                                output_root,
                                                                ANTS_path=ANTS_path,
                                                                operation_type="both",
                                                                affine_only=False ,
                                                                rigid_affine=False,
                                                                concat_transforms=True,
                                                                dim=3,
                                                                interp_type=ANTS_interp_type,
                                                                nopng=nopng,
                                                                precision_case=ANTS_precision,
                                                                verbose=verbose,
                                                                #logger=logger,
                                                                )

            transform_list=[]
            transform_list.append(aff_file);
            transform_list.append(warp_file);
            if direct_to_MNI:
                reg_struct['ANTS_HR_to_MNI_transform_list'] = transform_list;
            else:
                reg_struct['ANTS_HR_to_StudyTemplate_transform_list'] = transform_list;
                
        module_logger.info('ANTS registration, time elapsed: {}'.format(time() - starttime))
        
    if ANTS_flag!=0:
        
        transform_list=[]
        module_logger.debug('direct_to_MNI: {}'.format(direct_to_MNI)) 
        module_logger.debug('ANTS_reg_case: {}'.format(ANTS_reg_case.lower())) 
        if direct_to_MNI:  #direct from structural to MNI
            if ANTS_reg_case.lower()=='aff':
                transform_list.append(reg_struct['ANTS_HR_to_MNI_Aff'])
            elif ANTS_reg_case.lower()=='syn':
                transform_list.append(reg_struct['ANTS_HR_to_MNI_Aff'])
                transform_list.append(reg_struct['ANTS_HR_to_MNI_Warp'])
            else:
                raise ValueError("Invalid ANTS_reg_case specified.  For direct_to_MNI=True, must be 'aff' or 'syn'")
            reg_struct['ANTS_HR_to_MNI_transform_list'] = transform_list;
        else:  #append the StudyTemplate_to_MNI transforms to transform_list
            
            #Nonlin to pstd then nonlin to MNI
    
            if (ANTS_reg_case.lower()=='syn' or ANTS_reg_case.lower()=='synsyn'):  #Nonlin to pstd then nonlin to MNI
                    transform_list.append(reg_struct['ANTS_HR_to_StudyTemplate_Aff'])
                    transform_list.append(reg_struct['ANTS_HR_to_StudyTemplate_Warp'])
                    transform_list.append(reg_struct['ANTS_StudyTemplate_to_MNI_Aff'])
                    transform_list.append(reg_struct['ANTS_StudyTemplate_to_MNI_Warp'])
            elif ANTS_reg_case.lower()=='synaff': #Nonlin to pstd then affine to MNI
                    transform_list.append(reg_struct['ANTS_HR_to_StudyTemplate_Aff'])
                    transform_list.append(reg_struct['ANTS_HR_to_StudyTemplate_Warp'])
                    transform_list.append(reg_struct['ANTS_StudyTemplate_to_MNI_Aff'])
            elif ANTS_reg_case.lower()=='affsyn': #affine to pstd then nonlin to MNI
                    transform_list.append(reg_struct['ANTS_HR_to_StudyTemplate_Aff'])
                    transform_list.append(reg_struct['ANTS_StudyTemplate_to_MNI_Aff'])
                    transform_list.append(reg_struct['ANTS_StudyTemplate_to_MNI_Warp'])
            elif (ANTS_reg_case.lower()=='aff' or ANTS_reg_case.lower()=='affaff'):  #affine to pstd then affine to MNI
                    transform_list.append(reg_struct['ANTS_HR_to_StudyTemplate_Aff'])
                    transform_list.append(reg_struct['ANTS_StudyTemplate_to_MNI_Aff'])
            else:                
                    raise ValueError("Invalid ANTS_reg_case specified")
    
            reg_struct['ANTS_HR_to_MNI_transform_list'] = transform_list;
            if (not os.path.exists(reg_struct['ANTS_HR_to_MNI_img']) or ForceUpdate):
                if os.path.exists(reg_struct['ANTS_HR_to_StudyTemplate_img']):
            
                        fixed=reg_struct['MNI_standard_brain'];
                        moving=reg_struct['HR_img'];
            
                        ants_applywarp(transform_list,
                                    fixed,
                                    moving,
                                    reg_struct['ANTS_HR_to_MNI_img'],
                                    ANTS_path=ANTS_path,
                                    dim=3,
                                    interp_type=ANTS_interp_type,
                                    nopng=nopng,
                                    inverse_flag=False)
                                    
        reg_struct['ANTS_reg_case'] = ANTS_reg_case;
    
                #inverse_flag=1;
                #ants_applywarp(transform_list,fixed,moving,'InverseWarp_MNI.nii.gz',ANTS_path,dim,interp_str,nopng,inverse_flag)
    #toc_ANTS=toc;
    
    #save the updated registration structure
    
    dict2csv(reg_struct,reg_struct_file)
    
    writeHTML=True
    if writeHTML:
        from cmind.utils.cmind_HTML_report_gen import cmind_HTML_reports
        cmind_HTML_reports('T1_Normalize',output_dir,report_dir=None,ForceUpdate=ForceUpdate);
    
    #make sure temporary working directory has been removed
    if rm_working_dir and os.path.exists(working_dir):
        shutil.rmtree(working_dir)
        
    #print any filename outputs for capture by LONI
    print("reg_struct:{}".format(reg_struct_file))
    return (reg_struct, reg_struct_file, output_files)        

def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_ANTs_dir, cmind_MNI_dir
         
    direct_to_MNI=False
    #StudyTemplate_template_path=cmind_template_dir
    ForceUpdate=True;
    verbose=True
    MNI_path=cmind_MNI_dir 
    ANTS_path=cmind_ANTs_dir
    reg_flag='101';
    ANTS_reg_case='SyNAff'
    MNI_resolution='2mm'
    #fsl_motion_outiliers_v2_cmd=args.fsl_motion_outiliers_v2_cmd;
    generate_figures=True
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',
                        file_mode='w')  
    ANTS_precision='lenient' #'ants_defaults'
    
    if verbose:
        func=cmind_timer(cmind_normalize,logger=logger)
    else:
        func=cmind_normalize

    testcases=['P','F']
    for testcase in testcases:    
        if testcase=='F':
            output_dir=pjoin(cmind_example_output_dir,'F','T1_proc')
        elif testcase=='P':
            output_dir=pjoin(cmind_example_output_dir,'P','T1_proc')
    
        T1_volume_nii=pjoin(output_dir,'T1W_Structural_N4_brain.nii.gz')
        reg_struct=pjoin(output_dir,'reg_struct.csv')
    
        func(output_dir=output_dir, 
             T1_volume_nii=T1_volume_nii, 
             reg_struct=reg_struct, 
             direct_to_MNI=direct_to_MNI,
             reg_flag=reg_flag,
             ANTS_path=ANTS_path, 
             MNI_path=MNI_path, 
             ANTS_reg_case=ANTS_reg_case,
             ANTS_precision=ANTS_precision, 
             MNI_resolution=MNI_resolution, 
             generate_figures=generate_figures, 
             ForceUpdate=ForceUpdate, 
             verbose=verbose, 
             logger=logger)
    
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and  sys.argv[1] == 'test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Normalize a structural image to MNI space", epilog="")  #-h, --help exist by default
        parser.add_argument("-o","--output_dir",required=True, help="directory in which structural processing results are stored")
        parser.add_argument("-t1","-t1_vol","--T1_volume_nii",required=True, help="T1W image (after brain extraction).  This is the image to register to standard space")
        parser.add_argument("-rsf","--reg_struct",required=True, help="filename of the .csv file containing the registration dictionary")
        parser.add_argument("-direct","--direct_to_MNI", type=str, default='False', help="bypass study template and register directly to MNI space")
        parser.add_argument("-rf","--reg_flag", type=str, default='101', help="string of 3 characters controlling whether FLIRT, FNIRT and/or ANTS registrations of T1 to standard are to be run; doFLIRT = 0 or 1  (1 = do the regisration using FLIRT),doFNIRT = 0 or 1  (1 = do the regisration using FNIRT) (NOT CURRENTLY IMPLEMENTED),doANTS = 0, 1 or 2  (0 = skip, 1 = do using Exp, 2 = do using SyN), default = '101' does FLIRT and ANTs (Exp)")
        parser.add_argument("-a","-ants","--ANTS_path", type=str, default='None', help="path to the ANTs binaries")
        parser.add_argument("-mni","--MNI_path", type=str, default='None', help="path containing the MNI standard brains")
        parser.add_argument("-areg","--ANTS_reg_case", type=str, default='SyN', help="{'Aff','SyN','AffSyn','SynAff'}; Type of ANTS registration")
        parser.add_argument("-prec","--ANTS_precision", type=str, default='ants_defaults', help="{'lenient','ants_defaults','strict'}, choose from preset precision levels for the registrations (default = 'lenient')")
        parser.add_argument("-MNIres","--MNI_resolution", type=str, default='2mm', help="{'2mm','1mm'}, resolution of MNI standard space image to register ")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', 
        #   print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  
        
        args=parser.parse_args()
        
        output_dir=args.output_dir;
        T1_volume_nii=args.T1_volume_nii;
        reg_struct=args.reg_struct;
        direct_to_MNI=args.direct_to_MNI;
        reg_flag=args.reg_flag;
        ANTS_path=str2none(args.ANTS_path);
        MNI_path=str2none(args.MNI_path);
        ANTS_reg_case=args.ANTS_reg_case;
        ANTS_precision=args.ANTS_precision;
        MNI_resolution=args.MNI_resolution;        
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger;
    
        if verbose:
            cmind_normalize=cmind_timer(cmind_normalize,logger=logger)
    
        cmind_normalize(output_dir, 
                        T1_volume_nii, 
                        reg_struct, 
                        direct_to_MNI=direct_to_MNI,
                        reg_flag=reg_flag,
                        ANTS_path=ANTS_path, 
                        MNI_path=MNI_path, 
                        ANTS_reg_case=ANTS_reg_case,
                        ANTS_precision=ANTS_precision, 
                        MNI_resolution=MNI_resolution, 
                        generate_figures=generate_figures, 
                        ForceUpdate=ForceUpdate, 
                        verbose=verbose, 
                        logger=logger)
