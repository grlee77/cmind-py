#!/bin/bash
echo "CMD: $*"

usage()
{
echo "Basic usage: cmind_fMRI_norm.py -o <output_dir> -rstr <reg_struct> -regbbr <regBBR_dir>  [options]

usage: cmind_fMRI_norm.py [-h] -o OUTPUT_DIR -rstr REG_STRUCT -regbbr
                          REGBBR_DIR [-ants ANTS_PATH] [-rflg REG_FLAG]
                          [-gf GENERATE_FIGURES] [-u FORCEUPDATE] [-v]
                          [-log LOGGER] [-tar OUTPUT_TAR]

Apply previously computed transforms to normalize fMRI results to MNI standard
space

Required parameters:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -rstr REG_STRUCT, --reg_struct REG_STRUCT
                        filename of the .csv containing the registration
                        structure
  -regbbr REGBBR_DIR, --regBBR_dir REGBBR_DIR
                        pathname to the BBR registration results (rigid from
                        functional to structural)
  -prep PREP_DIR, --prep_dir PREP_DIR
                        pathname to the output directory from cmind_fMRI_preprocess2
  -stats STATS_DIR, --stats_dir STATS_DIR
                        pathname to the output directory from cmind_fMRI_level1_stats


optional arguments:
  -h, --help            show this help message and exit
  -ants ANTS_PATH, --ANTS_path ANTS_PATH
                        path to the ANTs binaries
  -rflg REG_FLAG, --reg_flag REG_FLAG
                        string of 3 characters controlling whether FLIRT,
                        FNIRT and/or ANTS registrations of T1 to standard are
                        to be run. doFLIRT = 0 or 1 (1 = do the regisration
                        using FLIRT). doFNIRT = 0 or 1 (1 = do the regisration
                        using FNIRT) (NOT CURRENTLY IMPLEMENTED). doANTS = 0,
                        1 or 2 (0 = skip, 1 = do using Exp, 2 = do using SyN)
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate overlay images summarizing the
                        registration
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
  -tar OUTPUT_TAR, --output_tar OUTPUT_TAR
                        compress the registration folders into a .tar.gz
                        archive
  -np NUM_PROC
                        number of threads to usage
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi


if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

if [ $# == 0 ]; then usage;  exit 0; fi

if [ "$1" == "-h" ]; then usage; exit 0; fi

REQUIRED_ARGS=10
if [ $# -lt $REQUIRED_ARGS ] 
then
    usage
    exit 1
fi

output_dir="default"
feat_params="default"
regBBR_dir="default"
ANTS_path="Default"
reg_flag=1
generate_figures=true
ForceUpdate=true
output_tar=true
verbose_string=""

all_optional_args=""
while [ "$1" != "" ]; do
  case $1 in
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -rstr|--reg_struct )
        shift
        reg_struct=$1
        ;;
    -regbbr|--regBBR_dir )
        shift
        regBBR_dir=$1
        ;;
    -prep|--prep_dir )
        shift
        prep_dir=$1
        ;;
    -stats|--stats_dir )
        shift
        stats_dir=$1
        ;;
    -ants|--ANTS_path )
        shift
        ANTS_path=$1
        ;;
    -rflg|--reg_flag )
        shift
        reg_flag=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    -tar|--output_tar )
        shift
        output_tar=$1
        ;;
    -np )
        shift
        export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1
        ;;
    esac
    shift
done


if [ ! -z $verbose_string ]; then
    debug=true
else
    debug=false
fi


echo "cope=$cope1"
echo "output_dir=$output_dir"
echo "reg_struct=$reg_struct"
echo "regBBR_dir=$regBBR_dir"
echo "reg_flag=$reg_flag"
echo "generate_figures=$generate_figures"
echo "ForceUpdate=$ForceUpdate"

#preprocess_case="Feat_${scantype_str}_${preprocess_case}"

if [ $regBBR_dir = "default" ]; then
    regBBR_dir=`dirname $output_dir`
    regBBR_dir=$regBBR_dir/reg_BBR    
fi 

#have to append the /stats subfolder to the folder where cmind_fMRI_level1_stats.py was run
stats_dir="$stats_dir/stats"

echo "ANTS_path=$ANTS_path"
#Check that the ANTs executable is on the path
if [ $ANTS_path = Default ]; then
	command -v antsApplyTransforms >/dev/null 2>&1 || { echo "The ANTs tool antsApplyTransforms is required, but was not found. Aborting." >&2; exit 1; }
	ANTS_path=`command -v antsApplyTransforms`
    ANTS_path=`dirname $ANTS_path`

fi

echo "ANTS_path=$ANTS_path"
echo "output_dir=$output_dir"
echo "reg_struct=$reg_struct"
echo "regBBR_dir=$regBBR_dir"
echo "reg_flag=$reg_flag"
echo "generate_figures=$generate_figures"
echo "ForceUpdate=$ForceUpdate"



#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir reg_struct regBBR_dir prep_dir stats_dir ANTS_path reg_flag generate_figures ForceUpdate logger output_tar  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

if [ "$(ls -Ap $prep_dir | grep -v paradigm.txt)" ]; then
    t=$(timer)
    echo "python ${cmind_python_dir}/cmind_fMRI_norm.py ${verbose_string} ${all_optional_args}"
    python ${cmind_python_dir}/cmind_fMRI_norm.py ${verbose_string} ${all_optional_args}
    printf 'cmind_fMRI_norm.py:  Elapsed time: %s\n' $(timer $t)

    #check that QA report exists
    output_file_test=${output_dir}/reg/highres.nii.gz
    echo "$output_file_test"
    if [ ! -f $output_file_test ]; then
        echo "ERROR: Failed to finish fMRI norm"
        exit 1
    fi 
    echo "Finished cmind_fMRI_norm"
fi
