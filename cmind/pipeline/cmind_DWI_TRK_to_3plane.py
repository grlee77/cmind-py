#!/usr/bin/env python
from __future__ import division, print_function, absolute_import
        
def cmind_DWI_TRK_to_3plane(trackfile, output_image_prefix, track_lengths=40, add_length_label=True, figure_title=None, image_scale_pct=50, extra_options='--no_annotation', stack_length_images=True, remove_intermediate_images=True, ForceUpdate=False, verbose=False, logger=None):
    """Use the command line version of TrackVis, FSL & ImageMagick tools to generate 3-plane Tractography images
    
    Parameters
    ----------
    trackfile : str
        filename of the .trk file to plot
    output_image_prefix : str
        full path of the output image, minus the image extension
    track_lengths : int, optional
        only display tracks > track_length.  If this is a list, generate separate
        plots for each length in the list
    add_length_label : bool, optional
        If true, the left margin of the image will contain text annotating the lengths
    figure_title : str, optional
        Title to put at top of image
    image_scale_pct : int, optional
        rescale the rendered images by this amount before making the 3-plane image
    extra_options : str, optional
        string containing extra options to be passed on to track_vis.  
        (see track_vis --help.  e.g. --do_not_render to disable rendering to 
        screen)
    stack_length_images : bool, optional
        If this is true, stack the images corresponding to each length vertically
        into a single image
    remove_intermediate_images : bool, optional
        If this is true, any intermediate images will be removed and only the
        final stacked image will be generated.
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    
    all_output_images : list
        list of images generated
        
    """

    import os
    import shutil
    import tempfile
    import warnings
    
    import numpy as np
    
    from cmind.globals import has_ImageMagick, has_GraphicsMagick
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import remove_file
    from cmind.utils.image_utils import (ImageMagick_append_label, 
                                         ImageMagick_crop_border,
                                         ImageMagick_resize)
    from cmind.utils.logging_utils import (cmind_init_logging, 
                                           log_cmd, 
                                           cmind_func_info)
    
    ForceUpdate, verbose=input2bool([ForceUpdate, verbose])

    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting 3-plane tractography image generation")      
            
    cam_file='./tmp.cam'
    track_lengths=np.asarray(track_lengths)
    
    if not os.path.dirname(output_image_prefix):  #if unspecified, put the output in the current path
        output_image_prefix=os.path.join(os.getcwd(),output_image_prefix)

    all_output_images=[]
    #extra_options = '--anti-aliasing  --no_annotation' 
        
    for tracklen in track_lengths:
        output_image='{}_len{}.png'.format(output_image_prefix,tracklen)
        all_output_images.append(output_image)
        if (not os.path.exists(output_image)) or ForceUpdate:
            working_dir=tempfile.mkdtemp()
            remove_file(cam_file, warn=False)

            view1=os.path.join(working_dir,'view1.png')
            log_cmd('track_vis "%s" -l %d -camera azimuth 0 elevation 0 %s -sc "%s"' % (trackfile,tracklen,extra_options,view1), verbose=verbose, logger=module_logger);
            remove_file(cam_file, warn=False)
            
            view2=os.path.join(working_dir,'view2.png')            
            log_cmd('track_vis "%s" -l %d -camera azimuth 90 elevation 0 %s -sc "%s"' % (trackfile,tracklen,extra_options,view2), verbose=verbose, logger=module_logger);
            remove_file(cam_file, warn=False)

            view3=os.path.join(working_dir,'view3.png')            
            log_cmd('track_vis "%s" -l %d -camera azimuth 0 elevation 90 %s -sc "%s"' % (trackfile,tracklen,extra_options,view3), verbose=verbose, logger=module_logger);
            remove_file(cam_file, warn=False)

            if has_ImageMagick or has_GraphicsMagick:
                try:
                    ImageMagick_crop_border(view1, verbose=verbose, logger=module_logger)  #crop borders using ImageMagick
                    ImageMagick_crop_border(view2, verbose=verbose, logger=module_logger)  #crop borders using ImageMagick
                    ImageMagick_crop_border(view3, verbose=verbose, logger=module_logger)  #crop borders using ImageMagick
                except:
                   warnings.warn("ImageMagick command failed")
               
            log_cmd('$FSLDIR/bin/pngappend "%s" + "%s" + "%s" "%s"' % (view1,view2,view3,output_image), verbose=verbose, logger=module_logger);
            shutil.rmtree(working_dir)
            
            if add_length_label and (has_ImageMagick or has_GraphicsMagick):#add label using ImageMagick
                try:                
                    label_string="len >= %d" % tracklen
                    ImageMagick_append_label(output_image,label_string,output_image=output_image, position='left',bg_color='black',text_color='white',pointsize=70,verbose=verbose,logger=module_logger)
                except:
                    warnings.warn("ImageMagick command failed")
                    
    if os.path.exists('./track_vis.log'):
        os.remove('./track_vis.log')    
                    
    if stack_length_images:  
        stack_image=output_image_prefix+'.png'
        stack_cmd="$FSLDIR/bin/pngappend " + " - ".join(all_output_images) + ' ' + stack_image 
        log_cmd(stack_cmd, verbose=verbose, logger=logger)
    
        if has_ImageMagick or has_GraphicsMagick:
            try:
                if figure_title:  #add title using ImageMagick
                    ImageMagick_append_label(stack_image,figure_title, output_image=stack_image, position='top',bg_color='black',text_color='white',pointsize=90, verbose=verbose,logger=module_logger)
                if (image_scale_pct>0) and (image_scale_pct<100):  #resize using ImageMagick
                    ImageMagick_resize(stack_image,image_scale_pct, verbose=verbose, logger=module_logger)
                elif image_scale_pct!=100:
                    warnings.warn("Invalid image_scale_pct, %f, specified.  No resize applied.  image_scale must be in range (0 100]" % image_scale_pct)
            except:
                warnings.warn("ImageMagick command failed")
        else:
            if figure_title:
                warnings.warn("ImageMagick not found.  could not add title")
            if image_scale_pct!=100:
                warnings.warn("ImageMagick not found.  could not rescale image")
                
        if remove_intermediate_images:
            for img in all_output_images:
                os.remove(img)
            all_output_images=stack_image
        else:
            all_output_images.append(stack_image)
                    
    return all_output_images       
    
#cmind_DWI_TRK_to_3plane(trackfile, output_image_prefix, track_lengths, add_length_label=add_length_label, figure_title=figure_title, image_scale_pct=image_scale_pct, stack_length_images=stack_length_images, remove_intermediate_images=remove_intermediate_images, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)


def _testme():
    trackfile='/media/Data1/CMIND_Other/TEMPLATE_PROCESSING/MultiContrast/newest/DTI/ODF_tracks.trk'            
    output_image_prefix='/media/Data1/CMIND_Other/TEMPLATE_PROCESSING/MultiContrast/newest/DTI/PyDTI'
    track_lengths=[30, 40, 60, 100]
    figure_title='Tractography Results'
    extra_options='--no_annotation'
    image_scale_pct=50
    add_length_label=True
    stack_length_images=True
    remove_intermediate_images=True
    ForceUpdate=False
    verbose=True
    logger=None
    if verbose:
        func=cmind_timer(cmind_DWI_TRK_to_3plane,logger=logger)
    else:
        func=cmind_DWI_TRK_to_3plane

    func(trackfile,output_image_prefix, track_lengths=track_lengths, 
         add_length_label=add_length_label, figure_title=figure_title, 
         image_scale_pct=image_scale_pct, extra_options=extra_options, 
         stack_length_images=stack_length_images, 
         remove_intermediate_images=remove_intermediate_images, 
         ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
    
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import _parser_to_loni
    from cmind.utils.decorators import cmind_timer
    
    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Make an image showing the slice coverage relative to the MNI standard brain", epilog="")  #-h, --help exist by default
        #example of a positional argument
        
        parser.add_argument("-t","--trackfile",dest='trackfile',required=True, type=str, help="filename of the .trk file to plot")
        parser.add_argument("-oroot","--output_image_prefix", dest='output_image_prefix', type=str, default='None', help="full path of the output image, minus the image extension")
        parser.add_argument("-tlen","--track_lengths", dest='track_lengths', type=float, default=40, help="only display tracks > track_length.  If this is a list, generate separate plots for each length in the list")
        parser.add_argument("-l","--add_length_label", dest='add_length_label', type=str, default='True', help="If true, the left margin of the image will contain text annotating the lengths")
        parser.add_argument("-title","--figure_title", dest='figure_title', type=str, default='None', help="Title to put at top of image")
        parser.add_argument("-s","--image_scale_pct", dest='image_scale_pct', type=int, default=50, help="rescale the rendered images by this amount before making the 3-plane image")
        parser.add_argument("-e","--extra_options", dest='extra_options', type=str, default='--no_annotation', help="string containing extra options to be passed on to track_vis.  (see track_vis --help.  e.g. --do_not_render to disable rendering to screen)")
        parser.add_argument("-stack","--stack_length_images", dest='stack_length_images', type=str, default='True', help="If this is true, stack the images corresponding to each length vertically into a single image")
        parser.add_argument("-r","--remove_intermediate_images", dest='remove_intermediate_images', type=str, default='True', help="If this is true, any intermediate images will be removed and only the final stacked image will be generated.")
        parser.add_argument("-u","--ForceUpdate", dest='ForceUpdate',type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-id","--ForceUpdate", dest='ForceUpdate',type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
          
        
        args=parser.parse_args()
        
        trackfile=args.trackfile   
        output_image_prefix=args.output_image_prefix   
        track_lengths=args.track_lengths   
        figure_title=args.figure_title   
        image_scale_pct=args.image_scale_pct   
        add_length_label=args.add_length_label   
        extra_options=args.extra_options   
        stack_length_images=args.stack_length_images   
        remove_intermediate_images=args.remove_intermediate_images   
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose   
        logger=args.logger   
                
        if verbose:
            cmind_DWI_TRK_to_3plane=cmind_timer(cmind_DWI_TRK_to_3plane,logger=logger)
    
        cmind_DWI_TRK_to_3plane(trackfile,output_image_prefix, 
                                track_lengths=track_lengths, 
                                add_length_label=add_length_label, 
                                figure_title=figure_title, 
                                image_scale_pct=image_scale_pct, 
                                extra_options=extra_options,
                                stack_length_images=stack_length_images, 
                                remove_intermediate_images=remove_intermediate_images, 
                                ForceUpdate=ForceUpdate, verbose=verbose, 
                                logger=logger)
