#!/bin/bash

#Need to move into the directory containing the bash source file
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd


usage()
{
echo "Basic usage: cmind_struct_preprocess.py -o <output_dir> -i <struct_nii>  [options]

usage: cmind_struct_preprocess.py [-h] -o OUTPUT_DIR -i STRUCT_NII
                                  [-t2 T2_STRUCT_NII] [-age AGE_MONTHS]
                                  [-bet BET_THRESH]
                                  [-rebias REUPDATE_BIAS_COR]
                                  [-reseg RESEGMENT_USING_N4MASK]
                                  [-a ANTS_BIAS_CMD]
                                  [-noseg OMIT_SEGMENTATION]
                                  [-oroot ONAME_ROOT] [-gf GENERATE_FIGURES]
                                  [-u FORCEUPDATE] [-v] [-log LOGGER]

Structural volume preprocessing pipeline
     requires FSL 5.0+,  ANTS
     uses the following FSL commands:  bet, fast, fslmaths, immv, fslroi, fslval, flirt
     uses the following from ANTS: N4BiasFieldCorrection or N3BiasFieldCorrection, antsRegistration
    
     This function does the following operations:
      1.)  Keeps top of the 256 mm FOV to remove neck/chest  (by default, an age dependent size is kept)
      2.)  Performs bias correction using ANTS
      3.)  Brain extraction using FSL's BET

      optional:  (specify -noseg to omit)
           4.)  Segmentation of the T1 volume using FSL's FAST

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -i STRUCT_NII, -t1 STRUCT_NII, -fname STRUCT_NII, --struct_nii STRUCT_NII
                        filename of NIFTI or NIFTIGZ volume to process


optional arguments:
  -h, --help            show this help message and exit
  -t2 T2_STRUCT_NII, --T2_struct_nii T2_STRUCT_NII
                        if T2_struct_nii is specified it will be rigidly
                        registered to struct_nii. Brain extraction will be run
                        on T2_struct_nii instead
  -age AGE_MONTHS, --age_months AGE_MONTHS
                        subject age in months (used during cropping)
  -bet BET_THRESH, --bet_thresh BET_THRESH
                        bet brain extraction threshold
  -rebias REUPDATE_BIAS_COR, --reupdate_bias_cor REUPDATE_BIAS_COR
                        rerun bias correction after brain extraction
  -reseg RESEGMENT_USING_N4MASK, --resegment_using_N4mask RESEGMENT_USING_N4MASK
                        if True, after initial segmentation rerun N4 bias
                        correction, weighted to the WM mask. Repeat
                        segmentation on the newly bias corrected image.
  -a ANTS_BIAS_CMD, -ants ANTS_BIAS_CMD, --ANTS_bias_cmd ANTS_BIAS_CMD
                        location of N4BiasFieldCorrection binary
  -noseg OMIT_SEGMENTATION, --omit_segmentation OMIT_SEGMENTATION
                        if True, omit segmentation
  -oroot ONAME_ROOT, --oname_root ONAME_ROOT
                        output filename prefix
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        filename to log to
  -np
                        number of threads
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=4
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi


source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/timer.sh

#this version of structural processing requires Python 2.7+
source $cwd/load_python_env.sh

#defaults for arguments:
age_months=200  #use older child template by default
ANTS_bias_cmd=N4BiasFieldCorrection
bet_thresh=0.30
ForceUpdate=true
generate_figures=true
omit_segmentation=true
oname_root='T1W'
verbose_str=""  #non-verbose by default


all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
  	-np )
  	    shift
  	    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1;
  	    ;;
    -o|--output_dir )
          shift
          output_dir=$1
          ;;
    -i|-t1|-fname|--struct_nii )
        shift
        struct_nii=$1
        ;;
    -t2|--T2_struct_nii )
        shift
        T2_struct_nii=$1
        ;;
    -age|--age_months )
        shift
        age_months=$1
        ;;
    -bet|--bet_thresh )
        shift
        bet_thresh=$1
        ;;
    -rebias|--reupdate_bias_cor )
        shift
        reupdate_bias_cor=$1
        ;;
    -reseg|--resegment_using_N4mask )
        shift
        resegment_using_N4mask=$1
        ;;
    -a|-ants|--ANTS_bias_cmd )
        shift
        ANTS_bias_cmd=$1
        ;;
    -noseg|--omit_segmentation )
        shift
        omit_segmentation=$1
        ;;
    -oroot|--oname_root )
        shift
        oname_root=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done

if [ ! -z $verbose_string ]; then
  debug=true
else
  debug=false
fi


#output_dir=`dirname $fname`
#output_dir=${output_dir}

echo $output_dir
echo $fname
echo $ANTS_bias_cmd
echo $BET_threshold
echo $ForceUpdate
echo $oname_root


if [ ! -d ${output_dir} ]; then
    echo "Creating T1 output directory: ${output_dir}"
    mkdir -p "${output_dir}"
fi 

if [ ! -f $fname ]; then
    echo "ERROR: Input file: $fname doesn't exist"
    exit 1
fi 

#Check if N4 or N3 based Bias Field Correction was requested
if grep -q N4BiasFieldCorrection <<<$ANTS_bias_cmd; then
    echo "Using N4 bias correction"
	use_N4=true
#elif [[ "$ANTS_bias_cmd" == *N3BiasFieldCorrection* ]] ; then
elif grep -q N3BiasFieldCorrection <<<$ANTS_bias_cmd; then
    echo "Using N3 bias correction"
    use_N4=false
else
    echo "Error, ANTS_bias_cmd must be either N4BiasFieldCorrection or N3BiasFieldCorrection"
    exit 1
fi
#Check that the ANTs bias correction executable is on the path
command -v $ANTS_bias_cmd >/dev/null 2>&1 || { echo "The ANTs tool $3 is required, but was not found. Aborting." >&2; exit 1; }
ANTS_bias_cmd=`command -v $ANTS_bias_cmd`  #use full path to the command

echo "${age_months}"

#existence checks for all required conditional arguments
if [ -z "$output_dir" ]; then
   echo "missing required argument: output_dir"
fi
if [ -z "$struct_nii" ]; then
   echo "missing required argument: struct_nii"
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir struct_nii T2_struct_nii age_months bet_thresh reupdate_bias_cor resegment_using_N4mask ANTS_bias_cmd omit_segmentation oname_root generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_struct_preprocess.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_struct_preprocess.py ${verbose_string} ${all_optional_args}
printf 'cmind_struct_preprocess.py:  Elapsed time: %s\n' $(timer $t)



# t=$(timer)
# echo "python ${cmind_python_dir}/cmind_struct_preprocess.py ${output_dir} ${fname} --age_months=${age_months} --bet_thresh=${BET_threshold} --ANTS_bias_cmd=$ANTS_bias_cmd --omit_segmentation=$omit_segmentation --generate_figures=$generate_figures --ForceUpdate=$ForceUpdate $verbose_str"
# python ${cmind_python_dir}/cmind_struct_preprocess.py ${output_dir} ${fname} --age_months=${age_months} --oname_root=${oname_root} --bet_thresh=${BET_threshold} --ANTS_bias_cmd=${ANTS_bias_cmd} --omit_segmentation=${omit_segmentation} --generate_figures=${generate_figures} --ForceUpdate=$ForceUpdate $verbose_str
# printf 'Structural Preprocessing:  Elapsed time: %s\n' $(timer $t)


#bet_thresh_int=$(echo "scale=0; $bet_thresh*100/1;" | bc)  #round 100*thresh to integer
#bet_thresh_int=$(printf "%.3d" $bet_thresh_int)    #pad to 3 digits

#brain_volume_nii="${output_dir}/${oname_root}_Structural_N4_BET_thresh${bet_thresh_int}_brain.nii.gz"


brain_volume_nii="${output_dir}/${oname_root}_Structural_N4_brain.nii.gz"

if [ ! -f $brain_volume_nii ] ; then
    echo "ERROR: Failed to finish Structural Preprocessing"
    exit 1
fi 

echo "output_dir:${output_dir}"
echo "output_head:${output_dir}/${oname_root}_Structural_N4.nii.gz"
#echo "output_brain:${output_dir}/${oname_root}_Structural_N4_BET_thresh${bet_thresh_int}_brain.nii.gz"
echo "output_brain:${output_dir}/${oname_root}_Structural_N4_brain.nii.gz"
#echo "output_brain_mask:${output_dir}/${oname_root}_Structural_N4_BET_thresh${bet_thresh_int}_brain_mask.nii.gz"
echo "output_brain_mask:${output_dir}/${oname_root}_Structural_N4_brain_mask.nii.gz"
echo "output_N4Bias:${output_dir}/${oname_root}_N4Bias.nii.gz"

if [ ! $omit_segmentation ] ; then
    echo "output_T1Segment_GM:${output_dir}/T1Segment_GM.nii.gz"
    echo "output_T1Segment_WM:${output_dir}/T1Segment_WM.nii.gz"
    echo "output_T1Segment_WMedge:${output_dir}/T1Segment_WMedge.nii.gz"
    echo "output_T1Segment_CSF:${output_dir}/T1Segment_CSF.nii.gz"
    output_file_test="${output_dir}/T1Segment_WMedge.nii.gz"
    if [ ! -f $output_file_test ] ; then
        echo "ERROR: Failed to finish T1 Segmentation"
        exit 1
    fi 
fi

echo "Finished `basename $0`"
exit 0
