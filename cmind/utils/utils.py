"""
**Miscellaneous Utilities**
A group of small functions commonly used within the CMIND pipelines

str2list
dprint
input2bool        
sphere
str2none  
is_string_like 
parser_to_bash
fsl_highpass_temporal_filter1D
calc_tSNR_regress

"""


#from IPython.core.debugger import Tracer
#debug_here = Tracer()
#debug_here()

from __future__ import division, print_function, absolute_import
import sys
import numpy as np

def str2list(csvstring,str_strip='"',str_split=','):
    """separates a comma seperated string such as "a, b, c, d"
       into a list ['a','b','c','d']
       
    Parameters
    ----------     
    csvstring : str
        string to be split into a list
    str_strip : str, optional
        strip instances of `str_strip` from `csvstring` before splitting.
        default = '"'
    str_split : str, optional
        string argument to be used by split() when processing `csvstring`
        default = ','
                
    Returns
    -------   
    str_list : list
        list of strings
        
    Notes
    -----
       
    """
    if csvstring is None:
        return [None,]
        
    if csvstring.find(str_split)!=-1:  #special case: split entries with str_split into a list   e.g.  "e1,e2,e3" -> ["e1","e2","e3"]
        str_list=csvstring.strip().strip(str_strip).split(str_split) 
    else:
        str_list=[]
        str_list.append(csvstring.strip()) #make sure no newline characters
    return (str_list)
        
def dprint(input_dict,HTML_markup=False, silent=False):
    """ print dictionary key,value pairs
    
    Parameters
    ----------
    input_dict : dict
        dictionary to print
    """
    try: #Python 2
        from cStringIO import StringIO
    except ImportError: #Python 3
        from io import StringIO
       
    out=StringIO()
    for key in input_dict:
        if HTML_markup:
            out.write("<b>{}</b>: {}<br>".format(key,input_dict[key]))
        else:
            out.write("{}: {}\n".format(key,input_dict[key]))
            
    if not silent:
        print(out.getvalue())
    return out.getvalue()
    
def lprint(input_list,HTML_markup=False, silent=False):
    """ print list items, one per line
    
    Parameters
    ----------
    input_list : dict
        list to print
    """
    try: #Python 2
        from cStringIO import StringIO
    except ImportError: #Python 3
        from io import StringIO
       
    out=StringIO()
    for item in input_list:
        if HTML_markup:
            out.write("{}<br>".format(item))
        else:
            out.write("{}\n".format(item))
    if not silent:
        print(out.getvalue())
    return out.getvalue()    
        
def input2bool(input_val):
    """convert various input types to boolean
    
    Parameters
    ----------     
    input_val : str or bool or int or float
        input to convert
        
    Returns
    -------   
    boolval : bool
        boolean output
        
    Notes
    -----
    If `input_val` is a bool, `boolval` = `input_val`.
    
    If `input_val` is a str, the following case-insensitive conversions are made:
        - [true, t, 1]   evaluate to True  (case insensitive)
        - [false, f, 0, none]    evaluate to False  (case insensitive)
        - any other string raises an error

    If `input_val` is numeric and equals 0, `boolval` is False.
        
    """

    #if input is a list, call input2bool on each item in the list
    if isinstance(input_val,(tuple,list)):
        return [input2bool(item) for item in input_val]
        
    if isinstance(input_val,bool):  #return bool unmodified
        boolval=input_val
        
    elif is_string_like(input_val):   #string to boolean     
        if input_val.lower()=='true' or input_val.lower()=='t' or input_val=='1':
            boolval=True; 
        elif input_val.lower()=='' or input_val.lower()=='false' or input_val.lower()=='none' or input_val.lower()=='f' or input_val=='0':
            boolval=False;
        else:
            raise Exception('Unrecognized input_val string, %s' % input_val)
            
    elif isinstance(input_val,(int,float)):  #int/float to boolean
        if input_val==0:
            boolval=False;
        else:
            boolval=True;
    else: #have python do the conversion for other cases such as None
        try:
            if input_val:
                boolval=True;
            else:
                boolval=False;
        except:
            raise Exception('Unable to convert the given input into a boolean value')
            
    return boolval    
    

def input2list(input_val,seperator=',',convert_to=None):  #TODO:  move this to utils
    """convert various input types to lists
    
    Parameters
    ----------     
    input_val : str or bool or int or float or numpy.ndarray or list or tuple
        input to convert to list
                
    Returns
    -------   
    output_list : list
        
    Notes
    -----
    If `input_val` is a string, any brackets will be stripped and then the
    string will be split via the `seperator` argument.
    e.g.  'x,y,z' would become the list: ['x', 'y', 'z']
        
    """
    if is_string_like(input_val):
        if len(input_val)==1:
            output_list=list(input_val)
        else: #handle strings such as   'x,z'  or '[x,y]'
            output_list=input_val.strip('([{').rstrip('})]').split(seperator)
    elif isinstance(input_val,(int,float,bool)): #convert int to list
        output_list=[input_val,]
    elif isinstance(input_val,np.ndarray): #convert int to list
        output_list=input_val.tolist()
    elif isinstance(input_val,(list,tuple)):
        output_list = input_val
    else:
        raise ValueError("Invalid input type: {}".format(type(input_val)))
        
    if convert_to:
        if convert_to=='float':
            output_list=[float(item) for item in output_list]
        elif convert_to=='int':
            output_list=[int(item) for item in output_list]
        elif convert_to=='bool':
            output_list=[bool(item) for item in output_list]
        else:
            ValueError("Invalid convert_type")
    return output_list
    
def sphere(r=1,N=100):
    """create an ndarray (x,y,z) coordinates over a sphere of radius r
    
    Parameters
    ----------     
    r : float, optional
        radius of sphere
    N : int, optional
        number of equal angular divisions to use along theta and phi
        
    Returns
    -------   
    x : ndarray
        x coordinates
    y : ndarray
        y coordinates
    z : ndarray
        z coordinates
    
    Notes
    -----
    
    """
    # def sphere(N,r=1):
    
    # Create a sphere for use in plotting (e.g. in mayavi)
    pi = np.pi
    cos = np.cos
    sin = np.sin
    phi_step=pi/float(N)
    theta_step=2*phi_step
    phi, theta = np.mgrid[0:pi+phi_step:phi_step, 0:2*pi+theta_step:theta_step]
    
    x = r*sin(phi)*cos(theta)
    y = r*sin(phi)*sin(theta)
    z = r*cos(phi)
    return (x,y,z)
    
def str2none(s,none_strings=['none']):
    """convert strings such as "none" to the Python None object, otherwise
    return the orginal string. 
    
    Parameters
    ----------
    s : str
        input string to potentially convert
    none_matches : list, optional
        lowercase list of input strings to convert to None
        
    """
    
    if s==None:
        return None
    else:
        if not is_string_like(s):
            raise ValueError("expected string-like input")
        else:
        
            for ns in none_strings:
                if s.lower()==ns:
                    return None        
                    
            return s

def is_string_like(obj):  #copied from numpy _iotools.py
    """
    Check whether obj behaves like a string.
    """
    try:
        obj + ''
    except (TypeError, ValueError):
        return False
    return True
    

def parser_to_bash(parser, script_name, npdocstr=None, out_file=None):
    #script_name=__file__
    import argparse, os, warnings
    from string import Template
    if not out_file:
        import io
    
    if not isinstance(parser,argparse.ArgumentParser):
        raise ValueError("parser input must be of class argparse.ArgumentParser")

    positional_args=parser._get_positional_actions()
    conditional_args=parser._get_optional_actions()

    #for action in parser._get_positional_actions():
    #    positional_args.append( (action.option_strings, action.dest, action.required))
    #for action in parser._get_optional_actions():
    #    conditional_args.append( (action.option_strings, action.dest, action.required))
    
    help=parser.format_help()

    

    if out_file:
        if os.path.exists(out_file):
            warnings.warn("output file already exists... overwriting")
        f=open(out_file,'w')
    else:
        f=io.StringIO()
    
    required_conditional_args=[]
    required_args_str=''
    for param in positional_args:
        required_args_str=required_args_str+'<%s> ' % param.dest
    for param in conditional_args:
        if param.required: #only add here if it is a required optional argument
            required_conditional_args.append(param)
            required_args_str=required_args_str+'%s <%s> ' % (param.option_strings[0],param.dest)
    required_args_str += ' [options]'
    
    required_args=len(positional_args)+2*len(required_conditional_args) #multiply conditionals by two to account for arguments names and values
    
    for param in required_conditional_args:
        conditional_args.remove(param)
    
    f.write('#!/bin/bash\n')
    f.write('echo "CMD: $*"\n\n')
    f.write('usage()\n{\necho "Basic usage: %s %s\n\n' % (script_name,required_args_str))
    f.write(help)
    f.write('"\n}\n\n')
    f.write('if [ $# == 0 ]; then usage;  exit 0; fi\n')
    f.write('if [ "$1" == "-h" ]; then usage; exit 0; fi\n')    
    f.write('if [ "$1" == "--help" ]; then usage; exit 0; fi\n\n')    
    f.write('REQUIRED_ARGS=%d\n' % required_args)
    f.write('if [ $# -lt $REQUIRED_ARGS ]\nthen\n    usage\n    exit 1\nfi\n\n')

    
    f.write('if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]\n')
    f.write('then\n')
    f.write('    #variable is empty or not set at all\n')
    f.write('    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1\n')
    f.write('fi\n\n')
    
    f.write('######################################################\n')
    f.write('###  Begin site-specific module load commands here ###\n\n')
    
    f.write('###  End site-specific module load commands here   ###\n')
    f.write('######################################################\n\n\n')


    f.write('verbose_string=""\n')
    f.write('all_optional_args=""\n')
    f.write('while [ "$1" != "" ]; do\n')
    f.write('  case $1 in\n')
    
    python_cmd = 'python ${cmind_python_dir}/'+script_name
    
    positional_case_template=Template('    -$option_strings )\n        shift\n        $dest=$$1\n        ;;\n')
    conditional_case_template=Template('    $option_strings )\n        shift\n        $dest=$$1\n')
    verbose_template=Template('    $option_strings )\n        verbose_string="--verbose"\n        ;;\n')
    print(required_conditional_args)
    print(conditional_args)
    conditionals_array='( '
    for param in positional_args:
        option_strings=param.dest
        dest=param.dest
        f.write(positional_case_template.substitute({'option_strings':option_strings,'dest':dest}))
        python_cmd=python_cmd+' ${%s}' % dest
    for param in required_conditional_args:
        option_strings='|'.join(param.option_strings)
        dest=param.dest
        f.write(conditional_case_template.substitute({'option_strings':option_strings,'dest':dest}))
        f.write('        ;;\n')
        #f.write('        all_optional_args="${all_optional_args} %s ${%s}"\n        ;;\n' % (param.option_strings[-1],dest))
        #python_cmd=python_cmd+' %s ${%s}' % (param.option_strings[-1], dest)
        conditionals_array += dest + ' '
    for param in conditional_args:
        option_strings='|'.join(param.option_strings)
        dest=param.dest
        if dest!='help': #--help should be stripped from the command line
            #python_cmd=python_cmd+' %s ${%s}' % (param[0][-1],dest)
            if dest=='verbose': #special action=store_true case
                f.write(verbose_template.substitute({'option_strings':option_strings}))
                python_cmd=python_cmd+" ${verbose_string}"
            else:
                f.write(conditional_case_template.substitute({'option_strings':option_strings,'dest':dest}))
                f.write('        ;;\n')
                #f.write('        all_optional_args="${all_optional_args} %s ${%s}"\n        ;;\n' % (param.option_strings[-1],dest))
                #python_cmd=python_cmd+' %s ${%s}' % (param.option_strings[-1], dest)
                conditionals_array += dest + ' '
    
    conditionals_array += ' )'
    python_cmd=python_cmd+' ${all_optional_args}'
    f.write('    esac\n')
    f.write('    shift\n')
    f.write('done\n\n')
    
    #write checks for required conditional arguments
    f.write("#existence checks for all required conditional arguments\n")
    for param in required_conditional_args:
        dest=param.dest
        f.write('if [ -z "$%s" ]; then\n' % dest)
        f.write('   echo "missing required argument: %s"\n' % dest)
        f.write('fi\n')
    f.write('\n')

    f.write('######################################################\n')
    f.write('###  Begin custom argument processing here         ###\n\n')
    
    f.write('###  End custom argument processing here           ###\n')
    f.write('######################################################\n\n\n')

    f.write("#All conditional arguments must be in the following list or they will not be passed onto python\n")
    f.write('conditionals_array = %s\n\n' % conditionals_array)
    #build a list of all conditional arguments that aren't empty
    f.write("#build a list of all conditional arguments that aren't empty\n")
    f.write('for cond_arg in  "${conditionals_array[@]}"\n')
    f.write('do\n')
    f.write('    cond_val=$cond_arg     #name of variable\n')
    f.write('    cond_val=${!cond_val}  #corresponding argument\n')
    f.write('    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?\n') 
    f.write('        all_optional_args="${all_optional_args} --$cond_arg $cond_val"\n')
    f.write('    fi\n')
    f.write('done\n\n')
    
    f.write('t=$(timer)\n')
    f.write('echo "%s"\n' % python_cmd)
    f.write(python_cmd+'\n')
    f.write("printf '{}:  Elapsed time: %s\\n' $(timer $t)\n\n".format(script_name))

    f.write('#print output filenames for capture by LONI pipeline\n')
    f.write('#The following options should be printed by the python function above:\n')
    if npdocstr:  #read return parameters from the docstring
        for param in npdocstr._parsed_data['Returns']:
            pname,ptype,pdesc=param
            #f.write('echo "%s:${%s}"\n' % (pname,pname))
            f.write('#    %s\n' % (pname))
    f.write('\n')
    
    f.write('#########################################################\n')
    f.write('###  Custom post-processing output after this point   ###\n\n')
            
    if not out_file:
        return_str=f.getvalue()
        f.close()
        return return_str
    else:
        f.close()
    
    

def _parser_to_loni(parser, sys_argv,py_file):
    if len(sys_argv)>1 and sys_argv[1]=='loni_bash':  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        from cmind.utils.utils import parser_to_bash
        import os, tempfile
        #print("pyfile = {}".format(os.path.basename(py_file).replace('.py','')))
        
        try:
            import inspect
            import numpydoc
            import cmind.pipeline  
            func=eval("cmind.pipeline.{}".format(os.path.basename(py_file).replace('.py','')))
            npdocstr=numpydoc.docscrape.NumpyDocString(inspect.getdoc(func))
        except:
            npdocstr=None
        #print("npdocstr = {}".format(npdocstr))
        out_file=os.path.join(tempfile.gettempdir(),os.path.basename("%s" % py_file).replace('.py','.in'))
        parser_to_bash(parser,os.path.basename(py_file),npdocstr=npdocstr,out_file=out_file)
        print("Printed LONI bash command text to %s" % out_file)
        sys.exit()
                  
                

def fsl_highpass_temporal_filter1D(volume1D,sigma,demean_result=True):
    """perform highpass temporal filtering the way it is done in FSL
    (based on the FSL C function: bandpass_temporal_filter)
    
    Parameters
    ----------
    volume1D : array or list
        1D timeseries to filter
    sigma : float
        sigma is in timepoints    
        
    Returns
    -------
    volume1D_filt : array
        filtered timeseries
            
    """

    #TODO: support > 1D?
    #if np.ndim(volume1D)>1:
    #    original_dims=volume1D.shape
    #    volume1D=volume1D.reshape(volume1D.shape[0],-1)

    sigma = sigma/2.0 #hardcoded in FSL
    hp_mask_size=int(np.floor(sigma*3))
    t=np.arange(-hp_mask_size,hp_mask_size+1)
    hp_exp= np.exp( -0.5 * (t*t) / (sigma*sigma) );
    
    sz1=volume1D.shape[0]
    
    volume1D_filt = np.zeros_like(volume1D);
    done_c0=False;
    c0=0;
    for t in range(sz1):
        A=0; B=0; C=0; D=0; N=0;
        tmin = t-hp_mask_size;
        tmax = t+hp_mask_size;
        #if((t<=hp_mask_size) | (t+hp_mask_size>sz4))
        tt=np.arange(max(tmin,0),min(tmax,sz1),dtype='int')
        dt=tt-t;
        w = hp_exp[hp_mask_size+dt];
        wdt=w*dt;
        A=np.sum(wdt);
        C=np.sum(wdt*dt);
        N=np.sum(w);
    
        tmpdenom=C*N-A*A;
            
        tmp=volume1D[tt];
        B=sum(w*tmp);
        D=sum(wdt*tmp);

        if(tmpdenom!=0):
            c = (B*C-A*D)/float(tmpdenom);
            if not done_c0:
                c0=c;
                done_c0=True;
            volume1D_filt[t]=c0+volume1D[t]-c;
        else:
            volume1D_filt[t]=volume1D[t];
        
        if demean_result:
            volume1D_filt[t]=volume1D_filt[t]-np.mean(volume1D_filt[t])
            
    return (volume1D_filt)

        
def calc_tSNR_regress(image, DesMat=None, mask=None, doResidual=False):
    """regresses out Design Matrix components before calculating the standard deviation so that task related signal changes don't penalize the tSNR value
    
    Parameters
    ----------
    image : ndarray
        input image to calculate tSNR on
    DesMat : ndarray
        design matrix [Time x Regressors]
    mask : ndarray
        boolean image of pixel locations over which to calculate tSNR
    doResidual : bool
        compute residuals?
    
    Returns
    -------
    tSNR : ndarray
        map of the computed tSNR values
    mask : ndarray
        the mask that was used in computing tSNR
    mean_tSNR : float
        mean values of tSNR over the mask
    res : ndarray
        residuals
    
    """
    
    #assume that if input is only 2D, that we have all spatial dimensions already collapsed onto the first dimension and the 2nd dimension is time
    

    if not np.any(mask):
        maskthr=0.075
        mask = np.mean(image,axis=-1) > maskthr*np.max(np.abs(np.mean(image,axis=-1)))
    #pyplot.imshow(montager(mask).T,pyplot.cm.gray)
    
    numT=image.shape[-1]
    if len(image.shape)==2:
        pass
        #image is already masked to a 2D array
    else:
        im_mask = np.zeros((np.sum(mask),numT))
        for ll in range(numT):
            if len(image.shape)==3:
                tmp = image[:,:,ll];
            elif len(image.shape)==4:
                tmp = image[:,:,:,ll];
            else:
                raise ValueError("Error: invalid number of dimensions.  image must be 2D, 3D or 4D")
            im_mask[:,ll] = tmp[mask];
        image = im_mask
    
    if not np.any(DesMat): #set to remove mean and linear trend
        DesMat = np.concatenate((np.ones((numT,1)), np.linspace(-0.5,0.5,numT).reshape((-1,1))),axis=1)

    if doResidual:
        res=np.zeros(image.shape,dtype=np.float32);
    else:
        res=None
        
    DesMat = np.asmatrix(DesMat);
    xtx_inv = np.asmatrix(np.linalg.pinv(DesMat));
    stdvals=np.zeros((image.shape[0],1));
    for dd in range(image.shape[0]):
        tmp = np.asmatrix(np.abs(image[dd,:]));
        beta_est = xtx_inv*tmp.T
        
        if doResidual:
            res[dd,:]=np.asarray(tmp.T-DesMat*beta_est).squeeze();
            stdvals[dd] = np.std(res[dd,:],ddof=1)
        else:
            stdvals[dd] = np.std(tmp.T-DesMat*beta_est,ddof=1)

    tSNR = np.mean(np.abs(image),axis=-1).reshape((-1,1))/stdvals;
    if doResidual:
        mean_tSNR=np.mean(tSNR);
    else:
        if np.any(mask):
            tSNR_masked=np.squeeze(tSNR.copy()) #have to remove singleton dimensions!
            tSNR=np.zeros(mask.shape);
            tSNR[mask]=tSNR_masked
            mean_tSNR=np.mean(tSNR_masked)
    
    return (tSNR, mask, mean_tSNR, res)            
    
def get_unicode_type():  
    """ 
    determine if the Python installation was compiled with --enable-unicode=ucs4 or ucs2
    """
    return (sys.maxunicode > 65536 and 'UCS4' or 'UCS2')


def reindent(s, numSpaces=4):  
    """add indentation to a multiline string 
    """
    s = s.split('\n')
    s = [(numSpaces * ' ') + line.lstrip() for line in s]
    s = '\n'.join(s)
    return s
    
def confirm(prompt=None, resp=False):
    """prompts for yes or no response from the user. Returns True for yes and
    False for no.

    'resp' should be set to the default value assumed by the caller when
    user simply types ENTER.

    >>> confirm(prompt='Create Directory?', resp=True)
    Create Directory? [y]|n: 
    True
    >>> confirm(prompt='Create Directory?', resp=False)
    Create Directory? [n]|y: 
    False
    >>> confirm(prompt='Create Directory?', resp=False)
    Create Directory? [n]|y: y
    True

    modified from:
        http://code.activestate.com/recipes/541096-prompt-the-user-for-confirmation/
    """
    
    if prompt is None:
        prompt = 'Confirm'

    if resp:
        prompt = '%s ([%s]/%s): ' % (prompt, 'y', 'n')
    else:
        prompt = '%s ([%s]/%s): ' % (prompt, 'n', 'y')
        
    while True:
        ans = raw_input(prompt).lower()
        if not ans:
            return resp
        if ans.lower() not in ['y', 'n', 'yes', 'no']:
            print('please enter y or n.')
            continue
        if ans == 'y' or ans == 'Y':
            return True
        if ans == 'n' or ans == 'N':
            return False    