#!/usr/bin/env python
"""
**mcflirt motion correction utility**
"""
from __future__ import division, print_function, absolute_import
import os
import glob
import shutil

from cmind.utils.file_utils import imexist, split_multiple_ext
from cmind.utils.cmind_utils import find_optimal_FSL_refvol, run_mcflirt
from cmind.utils.logging_utils import log_cmd

#scriptpath = '/home/lee8rx/src_repositories/svn_stuff/cmind-matlab/trunk/lib/'  #os.getcwd(); #"../Test/MyModule.py"
#sys.path.append(os.path.abspath(scriptpath))

def fsl_mcflirt_opt(input_name,cost_type='normcorr',out_name="", output_dir=None, verbose=False, logger=None):
    """Utility that tries to improve the robustness of mcflirt by running in 3 stages:
        
    Parameters
    ----------
    input_name : str
        4D NIFTI or NIFTI-GZ volume to motion correct
    cost_type : {'normcorr','mutualinfo','woods','corratio','normmi','leastsquares'}, optional
        cost function used by mcflirt (default = 'normcorr')
    out_name : str, optional
        output filename to be used for the motion-corrected timeseries
    output_dir : str, optional
        if None, output will be stored in current working directory via os.getcwd()
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    out_name : str
        output filename of the motion-corrected timeseries
        
    Notes
    -----
    The mcflirt stages run are:
    1.)  Fast initial registration to an initial frame without sinc interpolation to find the frame closest to the mean position
    2.)  Rerun mcflirt using this "optimal" reference frame and with sinc interpolation to reduce blurring
    3.)  Take a timeseries average of registered frames from stage two and run mcflirt a 3rd time using this high SNR timeseries average as the reference
    mcflirt will duplicate the top and bottom slice during the
    calculations to allow some corrections to be made to the edge slices
       
    cost_type options are: normcorr (default), mutualinfo, woods, corratio, normmi, leastsquares
    """
    
    #if False:
    #    proc = subprocess.Popen("$FSLDIR/bin/imtest " + input_name, stdout=subprocess.PIPE, verbose, logger)
    #    (out, err) = proc.communicate()
    #    print "program output:", out
    #else:
    #   exists = log_cmd("$FSLDIR/bin/imtest " + input_name, verbose, logger)
    #    exists = log_cmd("$FSLDIR/bin/imtest {}".format(input_name), verbose, logger)
        
        #[pth,fname,ext]=fileparts(input_name); %strip .nii or .nii.gz from filename
        #fname=strrep(fname,'.nii','');
        
    (exists,fname_full,fname_root,pth,ext)=imexist(input_name,'nifti');
    if not exists:
        raise IOError("Volume {} doesn't exist".format(input_name))
        
    
    #Note: FSL doesn't care if inputs have no file extension or .nii or .nii.gz
    if not output_dir: #avoid os.getcwd() if output_dir is passed as input
        output_dir=os.getcwd()
    
    
    if not out_name:  #reuse input filename, but with _mcf appended
        out_pth=output_dir
        fname_out_root=os.path.join(out_pth,fname_root + '_mcf')
        out_name=fname_out_root+ext #fname_full #os.path.join(os.getcwd(),fname_root)
    else:
        fname_out_root=split_multiple_ext(out_name)[0]
        out_pth=os.path.dirname(out_name)
        if not out_pth:
            out_pth=output_dir
            out_name=os.path.join(out_pth,out_name)
    
    #To save time, dont use -sinc_final on this first time through.  using timepoint 0 as the initial reference
    #print "$FSLDIR/bin/mcflirt -in {} -out {}_mcf -plots -rmsrel -rmsabs -refvol 0 -cost {}".format(input_name,out_name,cost_type)
#    out = log_cmd('$FSLDIR/bin/mcflirt -in "%s" -out "%s_mcf" -plots -rmsrel -rmsabs -refvol 0 -cost %s' % (input_name,out_name,cost_type), verbose, logger)
    print("input_name={},out_name={}".format(input_name,out_name))
    run_mcflirt(input_name,out_name,cost_type, verbose=verbose, logger=logger)
    ##eval(sprintf('!mcflirt -in %s -out %s_mcf -plots -rmsrel -rmsabs  -refvol 0 -cost %s',input_name,out_name,cost_type));
    
    #Find the timepoint closest to the mean position
    my_refvol = find_optimal_FSL_refvol(out_name + '.par')[0]
    
    #redo the registration using this "optimal" minimal-distance reference volume
    #print "$FSLDIR/bin/mcflirt -in %s -out %s_mcf -plots -rmsrel -rmsabs -refvol %d -cost %s" % (input_name,out_name,my_refvol,cost_type)
#    out = log_cmd('$FSLDIR/bin/mcflirt -in "%s" -out "%s_mcf" -plots -rmsrel -rmsabs -refvol %d -cost %s' % (input_name,out_name,my_refvol,cost_type), verbose, logger)
    run_mcflirt(input_name,out_name,cost_type,my_refvol, verbose=verbose, logger=logger)
    #eval(sprintf('!mcflirt -in %s -out %s_mcf -plots -rmsrel -rmsabs  -refvol %d -sinc_final -cost %s',input_name,out_name,my_refvol,cost_type));
    
    #Now, may want to rerun the registration to this higher SNR, timeseries averaged frame
    #probably of minimal benefit, but doesn't take too long to run
    
    #Create a time-series average of the "optimally" referenced timecourse
    log_cmd('$FSLDIR/bin/fslmaths "%s" -Tmean "%s_avg"' % (out_name,out_name), verbose=verbose, logger=logger);
    run_mcflirt(input_name,out_name,cost_type,refvol=out_name+"_avg",extra_args="-sinc_final", verbose=verbose, logger=logger)
    
    if(fname_out_root.find(' ')==-1):  #for safety don't allow spaces in the argument to rm
        #pathname = os.path.abspath(os.path.join(out_pth, fname_out_root+ext))
        mcf_fnames=glob.glob(os.path.join(out_pth,'*_avg.*'));
        [os.remove(f) for f in mcf_fnames]
        
        temp_dirs=[os.path.join(out_pth,o) for o in os.listdir(out_pth) if (os.path.isdir(os.path.join(out_pth,o)) and o.find('.mat')!=-1)]  #find all subdirectories with .mat in the directory name
        [shutil.rmtree(d) for d in temp_dirs]          #os.rmdir(d)  #note: doesn't work with non-empty directories
        
    return out_name