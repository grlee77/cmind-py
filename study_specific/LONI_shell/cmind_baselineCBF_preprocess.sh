#!/bin/bash

usage()
{
echo "Basic usage: cmind_baselineCBF_preprocess.py -o <output_dir> -cbf <CBF_nii>  [options]

usage: cmind_baselineCBF_preprocess.py [-h] -o OUTPUT_DIR -cbf CBF_NII
                                       [-a ANTS_BIAS_CMD] [-ucla UCLA_FLAG]
                                       [--fsl_motion_outliers_cmd FSL_MOTION_OUTLIERS_CMD]
                                       [-gf GENERATE_FIGURES] [-u FORCEUPDATE]
                                       [-v] [-log LOGGER]

BaselineCBF preprocessing (motion correction and outlier rejection)

required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
  -cbf CBF_NII, --CBF_nii CBF_NII
                        nifti image containing the 4D ASL timeseries

optional arguments:
  -h, --help            show this help message and exit
  -a ANTS_BIAS_CMD, -ants ANTS_BIAS_CMD, --ANTS_bias_cmd ANTS_BIAS_CMD
                        path to the bias field correction command to use. if
                        None, assumes /usr/lib/ants/N4BiasFieldCorrection
  -ucla UCLA_FLAG, --UCLA_flag UCLA_FLAG
                        if True, assumes order is tag,control,tag,...if False,
                        assumes order is control,tag,control...if true,
                        generate summary images
  --fsl_motion_outliers_cmd FSL_MOTION_OUTLIERS_CMD
                        path to the fsl_motion_outliers_cmd_v2 shell script.
                        If not specified will try to guess it relative to the
                        python script location.
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=4
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/load_python_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

echo "cmind_python_dir = ${cmind_python_dir}"

#Assign default input arguments to variables
ANTS_bias_cmd=N4BiasFieldCorrection
UCLA_flag=false
generate_figures=true
ForceUpdate=true
debug=false

all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
  	-np )
  	    shift
  	    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1;
  	    ;;
  	-o|--output_dir )
        shift
        output_dir=$1
        ;;
    -cbf|--CBF_nii )
        shift
        CBF_nii=$1
        ;;
    -a|-ants|--ANTS_bias_cmd )
        shift
        ANTS_bias_cmd=$1
        ;;
    -ucla|--UCLA_flag )
        shift
        UCLA_flag=$1
        ;;
    --fsl_motion_outliers_cmd )
        shift
        fsl_motion_outliers_cmd=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done

if [ ! -z $verbose_string ]; then
  debug=true
else
  debug=false
fi

if [ -z "${output_dir+xxx}" ]
then
    echo "ERROR: need output folder specified via -o flag."
    exit 1;
fi

if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi 

if [ ! -f $CBF_nii ]; then
    echo "ERROR: input file CBF_nii: $CBF_nii doesn't exist"
    exit 1
fi 

#Check that the ANTs executable is on the path
command -v $ANTS_bias_cmd >/dev/null 2>&1 || { echo "The ANTs tool $ANTS_bias_cmd is required, but was not found. Aborting." >&2; exit 1; }
ANTS_cmd_fullpath=`command -v $ANTS_bias_cmd`

if ${debug}
then
    echo "input file: $CBF_nii"
    echo "output folder: $output_dir"
    echo "ANTS bias cmd: $ANTS_bias_cmd"
    echo "UCLA flag: $UCLA_flag"
    echo "generate figures: $generate_figures"
    echo "force update: $ForceUpdate"
fi



#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir CBF_nii ANTS_bias_cmd UCLA_flag fsl_motion_outliers_cmd generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_baselineCBF_preprocess.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_baselineCBF_preprocess.py ${verbose_string} ${all_optional_args}
printf 'cmind_baselineCBF_preprocess.py:  Elapsed time: %s\n' $(timer $t)
#print output filenames for capture by LONI pipeline
#The following filenames should be printed by the python function above:
#    BaselineCBF_mcf_percent_change
#    BaselineCBF_mcf_control_mean_N4
#    BaselineCBF_Meansub
#    BaselineCBF_timepoints_kept
#    BaselineCBF_mcf_masked
#    BaselineCBF_N4Bias
#    BaselineCBF_relmot


# t=$(timer)

# echo "python ${cmind_python_dir}/cmind_baselineCBF_preprocess.py -o ${output_dir} ${CBF_nii} ${ANTS_bias_cmd} ${UCLA_flag} --generate_figures ${generate_figures} --ForceUpdate ${ForceUpdate}"

# if $debug
# then
#     python ${cmind_python_dir}/cmind_baselineCBF_preprocess.py -o ${output_dir} ${CBF_nii} ${ANTS_bias_cmd} ${UCLA_flag} --generate_figures ${generate_figures} --ForceUpdate ${ForceUpdate} --verbose
# else
#     python ${cmind_python_dir}/cmind_baselineCBF_preprocess.py -o ${output_dir} ${CBF_nii} ${ANTS_bias_cmd} ${UCLA_flag} --generate_figures ${generate_figures} --ForceUpdate ${ForceUpdate}
# fi

# printf 'cmind_baselineCBF_preprocess:  Elapsed time: %s\n' $(timer $t)

#check that BaselineCBF_mcf_percent_change.nii.gz exists
output_file_test=${output_dir}/BaselineCBF_mcf_percent_change.nii.gz
# BaselineCBF_mcf_mean_N4=${output_dir}/BaselineCBF_control_mcf_mean_N4_brain.nii.gz
# Meansub_BaselineCBF=${output_dir}/MeanSub_BaselineCBF_N4.nii.gz
# BaselineCBF_timepoints_kept=${output_dir}/BaselineCBF_timepoints_kept.txt
# BaselineCBF_mcf_masked=${output_dir}/BaselineCBF_mcf_masked.nii.gz
# N4BiasCBF=${output_dir}/N4BiasCBF.nii.gz
# relmot=${output_dir}/mc/BaselineCBF_mcf_rel.rms

echo "$output_file_test"
if [ ! -f $output_file_test ]; then
    echo "ERROR: Failed to finish BaselineCBF Preprocess"
    exit 1
fi 


