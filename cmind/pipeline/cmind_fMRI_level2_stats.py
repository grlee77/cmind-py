#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

#def create_group_fsf(group_output_dir, Feat_Directory_List, run_feat=False, analysis_type='flame1', thresholding='voxel', prob_thresh=0.05, z_thresh=2.3, highres_files=[], regressor_file_list=[], regressor_names=[], contrasts=[], contrast_names=[], copeinputs=[], group_list=[], fsf_overrides={}, verbose=False, logger=None):
def cmind_fMRI_level2_stats(group_output_dir, Feat_Directory_List, run_feat=False, analysis_type='flame1', thresholding='voxel', prob_thresh=0.05, z_thresh=2.3, highres_files=[], regressor_file_list=[], regressor_names=[], regressor_demean_flags=[], contrasts=[], contrast_names=[], copeinputs=[], group_list=[], fsf_overrides={}, verbose=False, logger=None):
    """create a FSL Feat design.fsf file for 2nd level analysis (and optionally run it)
    
    Parameters
    ----------
    group_output_dir : str
        group analysis output directory
    Feat_Directory_List : list of str or str
        list of the 1st-level Feat directories or the name of a text file containing this list
    run_feat : bool, optional
        if True, call feat to run the generated the fsf file
    analysis_type : {'fixed','ols','flame1','flame2'}, optional
        type of 2nd level analysis to perform
    thresholding : {'none','uncorrected','voxel','cluster'}, optional
        Thresholding type
    prob_thresh : float, optional
        corrected voxel P threshold.  used during analysis types 'uncorrected', 'voxel', and 'cluster'
    z_thresh : float, optional
        initial Z threshold when defining clusters  (for analysis type 'cluster' only)
    highres_files : list of str or str, optional
        list of the 1st-level anatomicals or the name of a text file containing this list
    regressor_file_list : list of str, optional
        list of regressor files.  each file should have one value per row. If none is given, a single group mean will be performed
    regressor_names : list of str, optional
        list of corresponding names for the regressors
    regressor_demean_flags : list of int, optional
        set 1 or 0 to control whether a given regressor should be demeaned
    contrasts : list of str or str, optional
        a list of lists of the desired contrasts (default is each regressor individually)
    contrast_names : list of str or str, optional
        list of names corresponding `contrasts`
    copeinputs : list of int, optional
        list of boolean integers equal in leangth to the number of first-level 
        copes.  For each entry that is a 1, that cope will be analyzed at the
        2nd level
    group_list : list of str or str, optional
        list of the group membership of each subject (default will 1 group for all) OR filename of the text file containing this list
    fsf_overrides : dict, optional
        dictionary of specific fsf values that will override any defaults set up by this script or a .csv file containing key,value pairs
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    
    Returns
    -------
    fsf : dict
        the fsf dictionary that was generated
    
    """
    
    import os
    from os.path import join as pjoin
    import glob
    import numpy as np
    import nibabel as nib
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict
    from cmind.utils.fsf_utils import create_fsf, write_fsf, create_ev, create_con
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    #convert various strings to boolean
    run_feat, verbose = input2bool([run_feat, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Generating Feat level-2 design")        
    
    
    analysis_type_list=['fixed','ols','flame1','flame2']
    if not analysis_type.lower() in analysis_type_list:
        raise ValueError("invalid analysis_type option: %s" & analysis_type)

    thresholdings_list=['none','uncorrected','voxel','cluster']
    if not thresholding.lower() in thresholdings_list:
        raise ValueError("invalid thresholding option: %s" & thresholding)

    if prob_thresh<0 or prob_thresh>1:
        raise ValueError("prob_thresh must fall within [0 1]")
        
    if z_thresh<0:
        raise ValueError("z_thresh must be >0")
    
    if not os.path.exists(group_output_dir):
        os.makedirs(group_output_dir)
    
    #basedir='/home/lee8rx/smb_shares/cmind/DATA/Processed_Data/'
    
    fsf=create_fsf(vers=6.0)
    # Output directory
    fsf['outputdir']=group_output_dir;
    fsf['fsldir']=group_output_dir;  #directory where design.fsf created by write_fsf will be stored
    
    #read in the first level feat directories and make sure they all exist
    if isinstance(Feat_Directory_List,(list,tuple)):
        fsf['feat_files']=Feat_Directory_List
    else: #can read from a file as well
        if not os.path.exists(Feat_Directory_List):
            raise IOError("Specified Feat_Directory_List file, %s, not found!" % Feat_Directory_List)
        for line in open(Feat_Directory_List,'r'):
            line=line.strip()
            if line:
                fsf['feat_files'].append(line)
    for fd in fsf['feat_files']:
        if not os.path.exists(fd):
            raise IOError("Specified Feat directory, %s, not found!" % fd)
    Nfeat_dirs=len(fsf['feat_files'])
   
    #Set up paths to all of the structural files    
    if not highres_files: #try to automatically find them based on the Feat directories
        highres_files=[]
        for Feat_dir in fsf['feat_files']:
            highres_files.append(os.path.join(Feat_dir,'reg','highres.nii.gz'))

            
    if isinstance(highres_files,(list,tuple)):  
        fsf['highres_files']=highres_files
    else:  #can read from a file as well
        if not os.path.exists(highres_files):
            raise IOError("Specified highres_files file, %s, not found!" % highres_files)
        for line in open(highres_files,'r'):
            fsf['highres_files'].append(line).strip()
    for hr in fsf['highres_files']:
        if not imexist(hr,'nifti-1'):
            raise IOError("Specified highres file, %s, not found!" % hr)
    
    module_logger.debug("fsf['highres_files']:")
    module_logger.debug(fsf['highres_files'])
    if not regressor_file_list: #if none, just do a single group mean.  
        regressor_file_list=[]
        mean_reg_file=os.path.join(group_output_dir,'mean_reg.txt') 
        mean_reg=np.asarray([1]*Nfeat_dirs)
        np.savetxt(mean_reg_file,mean_reg,fmt='%d') #will create mean_reg.txt in group_output_dir
        regressor_file_list.append(mean_reg_file)
        regressor_names=["Group Mean",]
        
    all_regs=None
    for rf in regressor_file_list:
        if not os.path.exists(rf):
            raise IOError("Specified regressor file, %s, not found!" % rf)
        else:
            regs=np.genfromtxt(rf)
            if regs.ndim==1:
               regs=regs[:,np.newaxis]  #force to 2D
               #regs=np.expand_dims(regs,1); #force to 2D
               #regs.shape=(regs.shape[0], 1) 
            if all_regs is None:
                all_regs=regs;
            else:
                all_regs=np.concatenate((all_regs,regs),axis=1)
    
    #For second-level analysis, regressors should be de-meaned
    demean_regs=True
    Nregressors=all_regs.shape[1] #total number of regressors found across all files
    

    if demean_regs:
        for idx in range(Nregressors):
            reg = all_regs[:,idx];
            if regressor_demean_flags is not None:
                if regressor_demean_flags[idx]==1:
                    all_regs[:,idx]=reg-np.mean(reg)
            else:  #demean everything aside from a constant DC value regressor
                if np.sum(reg-np.mean(reg))!=0: #only demean non-constant regressors, to maintain any group effect regressor
                    all_regs[:,idx]=reg-np.mean(reg)
                else:
                    module_logger.debug("mean_reg_found")

    #TODO?:  generate a mean regressor if it doesn't exist
    #TODO?:  sort the mean regressor so it is the first in the list    (remember to resort the names as well!)
    #TODO?:  allow regressor names at top of regressor files
                     
    #store each regressors in it's own .txt file for later use.  Update the regressor_file_list using these new, demeaned regressors      
    regressor_file_list=[] #will build up a full list of regressors from the input files
    for idx in range(Nregressors):              
        rf=os.path.join(group_output_dir,'reg%04d.txt' % idx)
        regressor_file_list.append(rf)
        np.savetxt(rf,all_regs[:,idx],fmt='%0.7g')
            
    if Nregressors != len(regressor_names):
        raise ValueError("Mismatch between # of regressor files (%d), and number of regressor names (%d) provided" % (Nregressors, len(regressor_names)))

    if not contrasts:  #default is one independent contrast for each regressor
        contrasts=[]
        contrast_names=[]
        for n in range(Nregressors):
            c=[0]*Nregressors
            c[n]=1
            contrasts.append(c)
            contrast_names.append(regressor_names[n])
            
    if isinstance(contrast_names,str):
        contrast_names=[contrast_names,]
        
    Ncontrasts=len(contrasts)
    if Ncontrasts != len(contrast_names):
        raise ValueError("Mismatch between # of contrasts specified (%d), and number of contrast names (%d) provided" % (Ncontrasts, len(contrast_names)))
    
    copefile_list=glob.glob(os.path.join(fsf['feat_files'][0],'stats','cope*.nii*'))

    fsf['ncopeinputs']=len(copefile_list);
    if not copeinputs:  #process all copes from first level
        fsf['copeinputs']=[1]*fsf['ncopeinputs'];  
    else:
        if isinstance(copeinputs,(list,tuple)):  
            fsf['copeinputs']=copeinputs
        else:  #can read from a file as well
            if not os.path.exists(copeinputs):
                raise IOError("Specified cope input list file, %s, not found!" % copeinputs)
            for line in open(copeinputs,'r'):
                fsf['copeinputs'].append(line).strip()
     
    if len(fsf['copeinputs'])!=fsf['ncopeinputs']:
        raise ValueError("Number of booleans in copeinputs must match the total number of first-level copes")
    if np.any(np.asarray(fsf['copeinputs'])>1) or np.any(np.asarray(fsf['copeinputs'])<0):
        raise ValueError("copeinputs should be all zeros or ones")


    if group_list is None: #if not specified, assume all subjects belong to group 1
        fsf['groupmem']=[1]*len(fsf['feat_files'])
    else:
        if isinstance(group_list,np.ndarray):
            group_list=list(group_list)
        if isinstance(group_list,(list,tuple)):  
            fsf['groupmem']=group_list
        else:  #can read from a file as well
            if not os.path.exists(group_list):
                raise IOError("Specified group membership file, %s, not found!" % group_list)
            for line in open(group_list,'r'):
                fsf['groupmem'].append(line).strip()


    #TODO: nuisance regressors
    #TODO: voxelwise regressors
    #TODO: check size of contrast matrix verses Nregressors
    
    #clear allinfo 
    #load(os.path.join(basedir, 'all_subj_info.mat'))
    #for ff in range(len(fsf['feat_files'])):
        #tmp=fsf['feat_files'][ff]
        #loc1=tmp.find('/F/')-6;
        #subjID=tmp[loc1:loc1+6]
        #alpha_exists=[]
       #allinfo['subjID'][ff]=subjID;
        #index=cmind_get_from_structure(all_subj_info,subjID,'F');
        #allinfo.age_regressor(ff)=all_subj_info(index).ageF;
        #allinfo.mean_regressor(ff)=1;
        #allinfo.sleep_regressor(ff)=any(strcmp(all_subj_info(index).sleep_statusF,{'sleep','asleep'}));
        #allinfo.gender_regressor(ff)=~isempty(strfind(subjID,'M'));
        
        #fsf['highres_files'][ff]=os.path.join(tmp(1:loc1+8),'T1proc/T1W_3D_IRCstandard32_N3_brain30')
    
    #read in all custom regressors:  e.g. mean_regressor_file, age_regressor_file, gender_regressor_file, alpha_regressor_file
    fsf['ev']={}
    for regIDX in range(Nregressors):
        fsf['ev'][regIDX] = create_ev();
        fsf['ev'][regIDX]['title'] = '"'+regressor_names[regIDX]+'"'
        fsf['ev'][regIDX]['shape'] = 2  #2 : Custom (1 entry per volume)
        fsf['ev'][regIDX]['fname'] = regressor_file_list[regIDX];  #TODO
        fsf['ev'][regIDX]['ortho'] = [0]*Nregressors
        
   
    # Number of EVs
    fsf['evs_orig']=Nregressors;
    fsf['evs_vox']=0;  #number of voxelwise regressors
    fsf['evs_real']=fsf['evs_orig']+fsf['evs_vox'];
    
    # Number of contrasts
    fsf['ncon_orig']=Ncontrasts;  #TODO: is this correct?
    fsf['ncon_real']=Ncontrasts;  #TODO: is this correct?
    
    fsf['con_mode_old']='real';
    fsf['con_mode']='real';
    
    fsf['con']={}
    for n in range(fsf['ncon_real']):
        fsf['con'][n]=create_con('real')
        fsf['con'][n]['conname']=contrast_names[n]
        #fsf['con%d' % (n+1)]['con_vect']=np.zeros(fsf['ncon_real'],1); 
        #fsf['con%d' % (n+1)]['con_vect'][n]=1;
        fsf['con'][n]['con_vect']=contrasts[n]
    
    # TR(s)
    fsf['tr']=4.222;    #TODO
    
    # Total volumes
    fsf['npts']=Nfeat_dirs;    #TODO
    
    # Total voxels
    m1=nib.load(os.path.join(fsf['feat_files'][0],'reg_standard','mask.nii.gz')); 
    nvoxels=np.prod(m1.get_data().shape)   #FSL uses the product of the dimensions, not the number of non-zero
    #nvoxels=np.sum(m1.get_data())
    
    fsf['totalVoxels'] = nvoxels;    #TODO: check if this matches how FSL computes?
    
    # EPI TE (ms).  doesn't matter for the analysis
    if fsf['feat_files'][0].find('BOLD')!=-1:
        fsf['te'] = 35    #BOLD acquisition
    else:
        fsf['te'] = 11    #ASL acquisition
    
    # Spatial smoothing FWHM (mm)
    smoothing_file=os.path.join(fsf['feat_files'][0],'smooth_mm.txt')
    if os.path.exists(smoothing_file):
        fsf['smooth']=np.genfromtxt(smoothing_file);
    
    # Highpass temporal filtering
    fsf['temphp_yn']=1;
    
    # High pass filter cutoff
    fsf['paradigm_hp']=128;
        
    # Analysis level
    # 1 : First-level analysis
    # 2 : Higher-level analysis
    fsf['level']=2;
    
    # Higher-level modelling
    # 3 : Fixed effects
    # 0 : Mixed Effects: Simple OLS
    # 2 : Mixed Effects: FLAME (stage 1 only)
    # 1 : Mixed Effects: FLAME (full)
    if analysis_type:
        if analysis_type.lower()=='fixed':
            fsf['mixed_yn']=3
        elif analysis_type.lower()=='ols':
            fsf['mixed_yn']=0
        elif analysis_type.lower()=='flame1' or analysis_type.lower()=='flame':
            fsf['mixed_yn']=2
        elif analysis_type.lower()=='flame2':
            fsf['mixed_yn']=1
        else:
            raise ValueError("Unknown analysis_type: %s" % analysis_type)
    else:
        fsf['mixed_yn']=2;
    
    #Thresholding
    # 0 : None
    # 1 : Uncorrected
    # 2 : Voxel
    # 3 : Cluster
    if thresholding:
       if thresholding.lower()=='none':
            fsf['thresh']=0
       elif thresholding.lower()=='uncorrected':
            fsf['thresh']=1
       elif thresholding.lower()=='voxel':
            fsf['thresh']=2
       elif thresholding.lower()=='cluster':
            fsf['thresh']=3
       else:
            raise ValueError("Unknown analysis_type: %s" % analysis_type)
    else:   
        fsf['thresh']=2
    
    # P threshold
    fsf['prob_thresh']=float(prob_thresh);
    
    # Z threshold
    fsf['z_thresh']=float(z_thresh);    
    
    # Number of F-tests
    fsf['nftests_orig']=0;
    fsf['nftests_real']=0;
    
    # Background image for higher-level stats overlays
    # 1 : Mean highres
    # 2 : First highres
    # 3 : Mean functional
    # 4 : First functional
    # 5 : Standard space template
    fsf['bgimage']=1;
    
    
    # Registration to initial structural
    fsf['reginitial_highres_yn']=0;
    if fsf['reginitial_highres_yn']==0:
        fsf['initial_highres_files']=[]

    # Search space for registration to initial structural
    # 0   : No search
    # 90  : Normal search
    # 180 : Full search
    fsf['reginitial_highres_search']=90;
    
    # Degrees of Freedom for registration to initial structural
    fsf['reginitial_highres_dof']=12;
    
    # Registration to main structural
    fsf['reghighres_yn']=1;
    if fsf['reghighres_yn']==0:
        fsf['highres_files']=[]
    
    # Search space for registration to main structural
    # 0   : No search
    # 90  : Normal search
    # 180 : Full search
    fsf['reghighres_search']=90;
    
    # Degrees of Freedom for registration to main structural
    fsf['reghighres_dof']='BBR';
    
    # Registration to standard image?
    fsf['regstandard_yn']=1;
    
    # Standard image
    
    fsf['regstandard']=pjoin(os.environ['FSLDIR'],'data','standard','MNI152_T1_2mm_brain.nii.gz')
    
    # Search space for registration to standard space
    # 0   : No search
    # 90  : Normal search
    # 180 : Full search
    fsf['regstandard_search']=90;
    
    # Degrees of Freedom for registration to standard space
    fsf['regstandard_dof']=12;
    
    # Do nonlinear registration to standard space?
    fsf['regstandard_nonlinear_yn']=0;
    
    # Which stages to run
    # 0 : No first-level analysis (registration and/or group stats only)
    # 7 : Full first-level analysis
    # 1 : Pre-Stats
    # 3 : Pre-Stats + Stats
    # 2 :             Stats
    # # # # version 5.4
    # 6 :             Stats + Contrasts, Thresholding, Rendering
    # 4 :                     Contrasts, Thresholding, Rendering
    # # # # version 5.98
    # 6:              Stats + Post-Stats
    # 4:                      Post-Stats
    fsf['analysis']=6;
    
    # Delete volumes
    fsf['ndelete']=0;
    
    fsf['featwatcher_yn']=0  #disable web browser popup
    
    # Number of first-level analyses
    fsf['multiple']=len(fsf['feat_files']);  # Not a flag - actual num of analyses
    
    # Higher-level input type
    # 1 : Inputs are lower-level FEAT directories
    # 2 : Inputs are cope images from FEAT directories
    fsf['inputtype']=1;
    
    # Carry out pre-stats processing?
    fsf['filtering_yn']=0;
    
    # Brain/background threshold, #
    fsf['brain_thresh']=7;
    
    # Z min/max for colour rendering
    # 0 : Use actual Z min/max
    # 1 : Use preset Z min/max
    fsf['zdisplay']=0;
    
    # Z min in colour rendering
    fsf['zmin']=2;
    
    # Z max in colour rendering
    fsf['zmax']=8;
    
    # Colour rendering type
    # 0 : Solid blobs
    # 1 : Transparent blobs
    fsf['rendertype']=1;

    if fsf_overrides:  #allow customization of the .fsf file
        if isinstance(fsf_overrides,'str'):
            if not os.path.exists(fsf_overrides):
                raise IOError("fsf_overrides .csv file, %s, not found" % fsf_overrides)
            fsf_overrides=csv2dict(fsf_overrides)
        for key in fsf_overrides:
            fsf[key]=fsf_overrides[key]
            
    fsf_file=write_fsf(fsf)
    
    gfeat_dir=fsf['outputdir']+'.gfeat'  #default output path
    HTML_reg_report_file=os.path.join(gfeat_dir,'report_reg.html')
    module_logger.debug("HTML_reg_report_file = %s" % HTML_reg_report_file)
    
    if run_feat:
        log_cmd('$FSLDIR/bin/feat %s' % (fsf_file.replace('.fsf','')), verbose=verbose, logger=logger, print_stdout=True)

        if not os.path.exists(HTML_reg_report_file):  #update HTML report to point to the proper registration summary images
            #find the most recent report_reg.html file within fsf['outputdir']
            HTML_reg_report_file=log_cmd('find %s -name "report_reg.html" -exec ls -1t "{}" +' % (fsf['outputdir']),verbose=verbose,logger=logger)
            HTML_reg_report_file=HTML_reg_report_file.split('\n')[0].strip()  #first in list will be the most recently created

        if os.path.exists(HTML_reg_report_file):  #update HTML report to point to the proper registration summary images
            log_cmd("sed -i 's/reg\/example_func2standard1.gif/..\/reg_standard\/example_func.png/' %s" % (HTML_reg_report_file), verbose=verbose, logger=module_logger)
        
#    if False:  #pos/neg activation overlays
#        if preprocess_case=='Feat_BOLD_Sentences':
#            log_cmd('%s ./ ./cope1.feat/thresh_zstat1 ./cope2.feat/thresh_zstat1 "Sentences:  BOLD Activation (TE=35)" "Sentences_TE35_BOLD.png"' % (annotate_img_cmd), verbose=verbose, logger=module_logger)
#        elif preprocess_case=='Feat_ASL_Sentences':
#            log_cmd('%s ./ ./cope1.feat/thresh_zstat1 ./cope4.feat/thresh_zstat1 "Sentences:  BOLD Activation (TE=11)" "Sentences_TE11_BOLD.png"' % (annotate_img_cmd), verbose=verbose, logger=module_logger)
#            log_cmd('%s ./ ./cope2.feat/thresh_zstat1 ./cope5.feat/thresh_zstat1 "Sentences:  ASL Activation (TE=11)" "Sentences_TE11_ASL.png"' % (annotate_img_cmd), verbose=verbose, logger=module_logger)
    
    return (fsf, HTML_reg_report_file)
    
def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    group_output_dir='/media/Data1/temp/GROUP_TEST/GroupResult/'
    analysis_type='fixed'
    thresholding='cluster'
    prob_thresh=0.05;
    z_thresh=2.3
           
    subjIDs=['08F004','08F006','08F008','08F010','08F013','08F014','08F015','08F016','08F017','08F019','08F020','08F021','08M002','08M008','08M011']
    Feat_Directory_List=[]
    base_dir='/media/Data1/temp/GROUP_TEST'

    Feat_Directory_List=[]
    highres_files=[]
    for ID in subjIDs:
        Feat_dir=os.path.join(base_dir,ID,'Feat_BOLD')
        if not os.path.isdir(Feat_dir):
            raise OSError("Feat_dir, {}, doesn't exist.  can't run test case".format(Feat_dir))

        Feat_Directory_List.append(Feat_dir)
        highres_files.append(os.path.join(Feat_dir,'reg_s08_bptf0128_best','highres.nii.gz'))

    regressor_file_list=[]
    regressor_file_list.append(os.path.join(base_dir,'mean_reg.txt'))
    regressor_names=['Group Mean',]
    contrasts=[[1],]
    contrast_names=['Group Mean',]
    copeinputs=None
    group_list=None
    fsf_overrides=None
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  

    regressor_demean_flags=[]
    
    if verbose:
        func=cmind_timer(cmind_fMRI_level2_stats,logger=logger)
    else:
        func=cmind_fMRI_level2_stats

    for run_feat in [True,]: #True
        func(group_output_dir, Feat_Directory_List, run_feat=run_feat, analysis_type=analysis_type, thresholding=thresholding, prob_thresh=prob_thresh, z_thresh=z_thresh, highres_files=highres_files, regressor_file_list=regressor_file_list, regressor_names=regressor_names, regressor_demean_flags=regressor_demean_flags, contrasts=contrasts, contrast_names=contrast_names, copeinputs=copeinputs, group_list=group_list, fsf_overrides=fsf_overrides, verbose=verbose, logger=logger)

         
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2list, str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer
    
    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
        
    else:
        parser = argparse.ArgumentParser(description="create a FSL Feat design.fsf file for 2nd level analysis (and optionally run it)", epilog="")  #-h, --help exist by default
        #example of a positional argument
        
        parser.add_argument("group_output_dir", type=str, help="group analysis output directory")
        parser.add_argument("Feat_Directory_List", type=str, help="list of the 1st-level Feat directories or the name of a text file containing this list")
        parser.add_argument("-run","--run_feat", type=str, default="False", help="if True, call feat to run the generated the fsf file")
        parser.add_argument("-a","--analysis_type", type=str, default='flame1',help="{'fixed','ols','flame1','flame2'}, type of 2nd level analysis to perform")
        parser.add_argument("-t","--thresholding", type=str, default='cluster',help="{'none','uncorrected','voxel','cluster'}, Thresholding type")
        parser.add_argument("-p","--prob_thresh", type=float, default=0.05, help="corrected voxel P threshold.  used during analysis types 'uncorrected', 'voxel', and 'cluster'")
        parser.add_argument("-z","--z_thresh", type=float, default=2.3, help="initial Z threshold when defining clusters  (for analysis type 'cluster' only)")
        parser.add_argument("-hr","--highres_files", type=str, default="None", help="list of the 1st-level anatomicals or the name of a text file containing this list")
        parser.add_argument("-rlist","--regressor_file_list", type=str, default="None", help="list of regressor files.  each file should have one value per row. If none is given, a single group mean will be performed")
        parser.add_argument("-rnames","--regressor_names", type=str, default="None", help="list of corresponding names for the regressors")
        parser.add_argument("-rdmean","--regressor_demean_flags", type=str, default="None", help="list of corresponding demean flags for the regressors")
        parser.add_argument("-con","--contrasts", type=str, default="None", help="a list of lists of the desired contrasts (default is each regressor individually)")
        parser.add_argument("-connames","--contrast_names", type=str, default="None", help="list of names corresponding `contrasts`")
        parser.add_argument("-copes","--copeinputs", type=str, default="None", help="list of boolean integers equal in leangth to the number of first-level copes.  For each entry that is a 1, that cope will be analyzed at the 2nd level")
        parser.add_argument("-g","--group_list", type=str, default="None", help="list of the group membership of each subject (default will 1 group for all) OR filename of the text file containing this list")
        parser.add_argument("-fsf","--fsf_overrides", type=str, default="None", help="dictionary of specific fsf values that will override any defaults set up by this script or a .csv file containing key,value pairs")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        group_output_dir=args.group_output_dir   
        Feat_Directory_List=args.Feat_Directory_List
        run_feat=args.run_feat
        analysis_type=args.analysis_type
        thresholding=args.thresholding
        prob_thresh=args.prob_thresh
        z_thresh=args.z_thresh

        highres_files=args.highres_files
        highres_files=str2list(args.highres_files)
        if len(highres_files)==1:
            highres_files=str2none(highres_files[0])
                
        regressor_file_list=args.regressor_file_list
        regressor_file_list=str2list(args.regressor_file_list);
        if len(regressor_file_list)==1:
            regressor_file_list=str2none(regressor_file_list[0])
        
        regressor_names=args.regressor_names
        regressor_names=str2list(args.regressor_names);
        if len(regressor_names)==1:
            regressor_names=str2none(regressor_names[0])
        
        regressor_demean_flags=args.regressor_demean_flags
        regressor_demean_flags=str2list(args.regressor_demean_flags);
        if len(regressor_demean_flags)==1:
            regressor_demean_flags=str2none(regressor_demean_flags[0])
        
        contrasts=args.contrasts
        contrasts=str2list(args.contrasts);
        if len(contrasts)==1:
            contrasts=str2none(contrasts[0])
        
        contrast_names=args.contrast_names
        contrast_names=str2list(args.contrast_names);
        if len(contrast_names)==1:
            contrast_names=str2none(contrast_names[0])
            
        copeinputs=args.copeinputs
        copeinputs=str2list(args.copeinputs);
        if len(copeinputs)==1:
            copeinputs=str2none(copeinputs[0])
        
        group_list=args.group_list
        group_list=str2list(args.group_list);
        if len(group_list)==1:
            group_list=str2none(group_list[0])
        
        fsf_overrides=args.fsf_overrides
        if fsf_overrides.lower()=='none' or  fsf_overrides.lower()=='default':
            fsf_overrides={}
            
        verbose=args.verbose   
        logger=args.logger 
            
        if verbose:
            cmind_fMRI_level2_stats=cmind_timer(cmind_fMRI_level2_stats,logger=logger)

        cmind_fMRI_level2_stats(group_output_dir, Feat_Directory_List, run_feat=run_feat, analysis_type=analysis_type, thresholding=thresholding, prob_thresh=prob_thresh, z_thresh=z_thresh, highres_files=highres_files, regressor_file_list=regressor_file_list, regressor_names=regressor_names, regressor_demean_flags=regressor_demean_flags, contrasts=contrasts, contrast_names=contrast_names, copeinputs=copeinputs, group_list=group_list, fsf_overrides=fsf_overrides, verbose=verbose, logger=logger)
            
