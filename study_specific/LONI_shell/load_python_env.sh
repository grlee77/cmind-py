#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $cwd/exec_exists.sh

cmind_python_dir=`cd $cwd/../../cmind/pipeline; pwd`
if exec_exists module;
then
	CMIND_PYTHON_LIB_PATH=`cd "$cwd/../..";pwd`
	export PYTHONPATH="${PYTHONPATH}:${CMIND_PYTHON_LIB_PATH}"
    echo $PYTHONPATH

	#echo "found module"
	module load python/2.7.5
	#module load python3/3.3.2
	#if not (sys.version_info[0]==2 and sys.version_info[1]>=7)
	#CMIND_PYTHON_LIB_PATH="/scratch/LONI/Akila_LONI_Test/trunk/lib/Python"  #TODO:  fix this

	case "$(python --version 2>&1)" in
    *" 2.7"*)
        echo "Fine!"
        ;;
    *" 3."*)
        echo "Fine!"
        ;;
    *)
        echo "Wrong Python version!  >2.7 required..."
        ;;
	esac
else
	if exec_exists python;
	then
		#echo "module not found, but fsl already in path"
		case "$(python --version 2>&1)" in
		    *" 2.7"*)
		        echo "Fine!"
		        ;;
		    *" 3."*)
		        echo "Fine!"
		        ;;
		    *)
		        echo "Wrong Python version!  >2.7 required..."
		        ;;
		    esac
	else
		echo "Error:  module not found, and python not already on path"
		exit 1
	fi
fi

