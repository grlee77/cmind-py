.. _usage_command_line:  

==================
Command Line Usage
==================

Individual pipeline modules can be called directly from the command line.  Using the , ``-h`` or ``--help`` command line argument will print usage information.

Here is an example for ``cmind_baselineCBF_preprocess.py``:

:: 

	cmind_baselineCBF_preprocess.py -h

	usage: cmind_baselineCBF_preprocess.py [-h] -o OUTPUT_DIR -cbf CBF_NII
                                       [-a ANTS_BIAS_CMD] [-ucla UCLA_FLAG]
                                       [--max_percent_diff MAX_PERCENT_DIFF]
                                       [--fsl_motion_outliers_cmd FSL_MOTION_OUTLIERS_CMD]
                                       [--discard_outliers DISCARD_OUTLIERS]
                                       [-gf GENERATE_FIGURES] [-u FORCEUPDATE]
                                       [-v] [-log LOGGER]

	BaselineCBF preprocessing (motion correction and outlier rejection)

	optional arguments:
	  -h, --help            show this help message and exit
	  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
	                        directory in which to store the output
	  -cbf CBF_NII, --CBF_nii CBF_NII
	                        nifti image containing the 4D ASL timeseries
	  -a ANTS_BIAS_CMD, -ants ANTS_BIAS_CMD, --ANTS_bias_cmd ANTS_BIAS_CMD
	                        path to the bias field correction command to use. if
	                        None, assumes /usr/lib/ants/N4BiasFieldCorrection
	  -ucla UCLA_FLAG, --UCLA_flag UCLA_FLAG
	                        if True, assumes order is tag,control,tag,...if False,
	                        assumes order is control,tag,control...if true,
	                        generate summary images
	  --max_percent_diff MAX_PERCENT_DIFF
	                        if True, assumes order is tag,control,tag,...if False,
	                        assumes order is control,tag,control...if true,
	                        generate summary images
	  --fsl_motion_outliers_cmd FSL_MOTION_OUTLIERS_CMD
	                        path to the fsl_motion_outliers_cmd_v2 shell script.
	                        If not specified will try to guess it relative to the
	                        python script location.
	  --discard_outliers DISCARD_OUTLIERS
	                        unless False, detect and discard outliers
	  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
	                        if true, generate additional summary images
	  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
	                        if True, rerun and overwrite any previously existing
	                        results
	  -v, -debug, --verbose
	                        print additional output (to terminal and log)
	  -log LOGGER, --logfile LOGGER
	                        logging.Logger object (or string of a filename to log
	                        to)

From the above, it can be seen that only the output directory (``-o`` or ``--output_dir``) and 4D CBF nifti volume (``-cbf`` or ``--CBF_nii``) arguments are required.

A minimal call to this command would be as follows:

:: 

	cmind_baselineCBF_preprocess.py -cbf <CBF_NII> -o /tmp
	
where <CBF_NII> would be replaced with the name of the Baseline CBF timeseries to process

.. _usage_inpython:  

====================================
Calling Functions From Within Python
====================================

In addition to command line usage, each module can also be called as a function
from within other python scripts.  For interactive data analysis we strongly 
recommend use of the `ipython`_ python environment.  

From within python, we could import the cmind-py package and run the equivalent
of the command-line example above via:

:: 

    import cmind
    from cmind.pipeline import cmind_baselineCBF_preprocess
    output_dir='/tmp'
    input_CBF_volume='myCBFfile.nii.gz'
    cmind_baselineCBF_preprocess(output_dir,input_CBF_volume)
    
    
The expected arguments and their defaults can be easily queried interactively
from the `ipython`_ environment via:

:: 

    cmind.pipeline.cmind_baselineCBF_preprocess?
    
    
which in this case would produce the following output:

:: 

    Type:       function
    String Form:<function cmind_baselineCBF_preprocess at 0x43e1398>
    File:       /media/Data1/src_repositories/my_git/cmind-py/cmind/pipeline/cmind_baselineCBF_preprocess.py
    Definition: cmind.pipeline.cmind_baselineCBF_preprocess(output_dir, CBF_nii, max_percent_diff=3.0, ANTS_bias_cmd=None, UCLA_flag=False, fsl_motion_outliers_cmd=None, discard_outliers=True, generate_figures=True, ForceUpdate=False, verbose=False, logger=None)
    Docstring:
    BaselineCBF preprocessing (motion correction, intensity normalization & 
    outlier rejection)
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    CBF_nii : str
        nifti image containing the 4D ASL timeseries
    max_percent_diff : float, optional
        any control-tag frames greater than this percentage will be automatically
        considered outliers.  Used to intially remove high motion frames.  Note:
        For background-suppressed ASL data, this should be set to None or 100.0.
        Will not be used if `discard_outliers`=False
    ANTS_bias_cmd : str, optional
        path to the bias field correction command to use.
        if None, assumes /usr/lib/ants/N4BiasFieldCorrection
    UCLA_flag : bool, optional
        if True, assumes order is "tag","control","tag",...
        if False, assumes order is "control", "tag", "control"...
        if true, generate summary images
    fsl_motion_outliers_cmd : str, optional
        path to the fsl_motion_outliers_cmd_v2 shell script.  If not specified
        will try to guess it relative to the python script location.
    discard_outliers : bool, optional
        unless False, will detect and discard outliers
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    BaselineCBF_mcf_percent_change : str
        filename of the average control-tag percent change image
    BaselineCBF_mcf_control_mean_N4 : str
        filename of the average N4 bias-corrected control image
    BaselineCBF_Meansub : str
        filename of the average N4 bias-corrected control-tag image
    BaselineCBF_timepoints_kept : str
        textfile listing the control-tag timepoints that were kept
    BaselineCBF_mcf_masked : str
        filename of the ASL subtracted timeseries
    BaselineCBF_N4Bias : str
        filename of the N4 bias field computed from the mean control image
    BaselineCBF_relmot : str
        filename of the relative motion parameters during coregistration (motion-correction)
    
    Notes
    -----
    

.. include:: links_names.txt
