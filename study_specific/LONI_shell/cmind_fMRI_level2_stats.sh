#!/bin/bash
echo "CMD: $*"

usage() 
{
echo "Recommended Usage:      
   cmind_fMRI_level2_stats.sh -o output_dir -fdl Feat_Directory_List
   
Required parameters:
    -o              : output_dir - directory in which to store the output
    -fdl            : Feat_Directory_List - list of the 1st-level Feat directories or the name of a text file containing this list
    
Optional parameters:
    -r_feat         : if True, call feat to run the generated the fsf file
    -atype          : {'fixed','ols','flame1','flame2'}, type of 2nd level analysis to perform
    -thresh         : {'none','uncorrected','voxel','cluster'}, Thresholding type
    -p_thr          : float, corrected voxel P threshold.  used during analysis types 'uncorrected', 'voxel', and 'cluster'
    -z_thr          : float, initial Z threshold when defining clusters  (for analysis type 'cluster' only)
    -hrf            : list/str, list of the 1st-level anatomicals or the name of a text file containing this list
    -regfl          : list, list of regressor files.  each file should have one value per row. If none is given, a single group mean will be performed
    -reg_names      : list, list of corresponding names for the regressors
    -cont           : list/str, a list of lists of the desired contrasts (default is each regressor individually)
    -cont_names     : list, list of names corresponding `contrasts`
    -cope           : list/str, list of the first-level copes to analyze in the 2nd level (default will be all) OR filename of the text file containing this list
    -grp_list       : list/str, list of the group membership of each subject (default will 1 group for all) OR filename of the text file containing this list
    -fsf_overrides  : dict, dictionary of specific fsf values that will override any defaults set up by this script or a .csv file containing key,value pairs
    -debug          : verbose output

Returns:    
    fsf             : the fsf dictionary that was generated "    
}

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

if [ $# == 0 ]; then usage;  exit 0; fi

if [ "$1" == "-h" ]; then usage; exit 0; fi

REQUIRED_ARGS=1
if [ $# -lt $REQUIRED_ARGS ] 
then
    usage
    exit 1
fi

run_feat=true #true
analysis_type='fixed'
thresholding='voxel'
prob_thresh=0.05
z_thresh=2.3
highres_files="none"
regressor_file_list="none"
regressor_names="none"
contrasts="none"
contrast_names="none"
copeinputs="none"
group_list="none"
fsf_overrides="none"
logfile="none"
verbose_string="--verbose"
all_optional_args=""

while [ "$1" != "" ]; do
    case $1 in
    -o )   
		shift
		output_dir=$1
		;;
	-fdl )
		shift
	    Feat_Directory_List=$1
	    ;;	
	-r_feat )
	 	shift
	    run_feat=$1
	    ;;	
	-atype )
		shift
	    analysis_type=$1
	    ;;	
	-thresh)
		shift
	    thresholding=$1
	    ;;	 
	 -p_thr)
	 	shift
	    prob_thresh=$1
	    ;;	
	 -z_thr )
		shift
	    z_thresh=$1
	    ;;	 
	 -hrf )
		shift
	    highres_files=$1
	    ;;	 
	 -regfl )
		shift
	    regressor_file_list=$1
	    ;;	
	 -reg_names )
		shift
	    regressor_names=$1
	    ;;	 
	 -cont )
		shift
	    contrasts=$1
	    ;;	 
	 -cont_names )
		shift
	    contrast_names=$1
	    ;;		 
	 -cope )
		shift
	    copeinputs=$1
	    ;;	 
	 -grp_list )
		shift
	    group_list=$1
	    ;;	 
	 -fsf_override )
		shift
	    fsf_overrides=$1
	    ;;	
	 -debug )
	    shift
	    debug=$1
	    verbose_string="--verbose"
	    ;;
    esac
    shift
done

echo "output_dir=$output_dir"
echo "Feat_Directory_List=$Feat_Directory_List"	
echo "run_feat=$run_feat"
echo "analysis_type=$analysis_type"
echo "thresholding=$thresholding"
echo "prob_thresh=$prob_thresh"
echo "z_thresh=$z_thresh"
echo "highres_files=$highres_files"
echo "regressor_file_list=$regressor_file_list"
echo "regressor_names=$regressor_names"
echo "contrasts=$contrasts"
echo "contrasts_names=$contrasts_names"
echo "copeinputs=$copeinputs"
echo "group_list=$group_list"
echo "fsf_overrides=$fsf_overrides"

t=$(timer)
echo "${cmind_python_dir}/cmind_fMRI_level2_stats.py ${output_dir} ${Feat_Directory_List} --run_feat ${run_feat} --analysis_type ${analysis_type} --thresholding ${thresholding} --prob_thresh ${prob_thresh} --z_thresh ${z_thresh} --highres_files ${highres_files} --regressor_file_list ${regressor_file_list} --regressor_names ${regressor_names} --contrasts ${contrasts} --contrast_names ${contrast_names} --copeinputs ${copeinputs} --group_list ${group_list} --fsf_overrides ${fsf_overrides} $verbose_string"
python ${cmind_python_dir}/cmind_fMRI_level2_stats.py ${output_dir} ${Feat_Directory_List} --run_feat ${run_feat} --analysis_type ${analysis_type} --thresholding ${thresholding} --prob_thresh ${prob_thresh} --z_thresh ${z_thresh} --highres_files ${highres_files} --regressor_file_list ${regressor_file_list} --regressor_names ${regressor_names} --contrasts ${contrasts} --contrast_names ${contrast_names} --copeinputs ${copeinputs} --group_list ${group_list} --fsf_overrides ${fsf_overrides} $verbose_string
printf 'cmind_fMRI_level2_stats:  Elapsed time: %s\n' $(timer $t)

#check that QA report exists
output_file_test=${output_dir}/design.fsf
echo "$output_file_test"
if [ ! -f $output_file_test ]; then
    echo "ERROR: Failed to finish fMRI Level2_Stats"
    exit 1
fi 

mv "$output_dir.gfeat" "$output_dir"

echo "Finished cmind_fMRI_level2_stats"

