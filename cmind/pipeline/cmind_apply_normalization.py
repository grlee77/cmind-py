#!/usr/bin/env python
from __future__ import division, print_function, absolute_import


def cmind_apply_normalization(output_dir, reg_struct, input_name, output_name="default", LRtoHR_affine=None, LRtoHR_warp=None, ANTS_path=None, Norm_Target='MNI', reg_flag='001', interp_type="", is_timeseries = False, generate_coverage=False, generate_figures=True, ForceUpdate=False, verbose=False, logger=False):
    """normalize input_name to standard space using previously computed transforms
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    reg_struct : str or dict
        filename of the .csv file containing the registration dictionary
    input_name : str or list of str
        image filename for the "moving" image
    output_name : str or list of str
        image filename for the output
    LRtoHR_affine : str or list of str or None, optional
        affine transform from functional to structural space.
        if LRtoHR_affine='', it is assumed the image is already in structural
        space
    LRtoHR_warp : str or list of str or None, optional
        fieldmap warp correction for functional space image
        if LRtoHR_warp='' or None, no warping is applied
    ANTS_path : str, optional
        path to ANTs binaries
    Norm_Target : {'MNI','StudyTemplate','Structural'}, optional
        Normalization target
    reg_flag : str or int, optional
        3 character string such as 101 controlling which registration to perform
        first digit is for FLIRT
        second digit is for FNIRT (not currently implemented)
        third digit is for ANTs, SyN
        e.g.  101 would run FLIRT & ANTs registrations.
        can also specify the binary "101" as the integer 5, etc.
    interp_type : str or list of str, optional
        {'spline','bspline'} call spline based interpolation
        {'lin', 'linear','trilinear'} call trilinear interpolation
        {'nearestneighbour','nearest','nn'} call nearest neighbor interpolation
        interpolation type to use
    is_timeseries : bool, optional
        if true, input_name corresponds to timeseries data.  the transformation will
        be applied to all time frames
    generate_coverage : bool or list of bool, optional
        if true, generate binary masks of the slice coverage.  can be an array
        of values for each input image
    generate_figures : bool,optional
        if true, generate overlay images summarizing the registration
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
            
    Returns
    -------
    
    all_output_names : list
        list of all registered image filenames
    
    See Also
    --------
    ants_applywarp
    
    Notes
    -----
    
    """
    import os
    import shutil
    import tempfile
    import warnings
    
    import numpy as np
    
    from cmind.pipeline.ants_applywarp import ants_applywarp
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, split_multiple_ext, csv2dict
    from cmind.utils.cmind_utils import cmind_reg_report_img
    from cmind.utils.cmind_utils import _transformlist_from_case, _convert_reg_struct  #needed for backwards compatiblity
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    from cmind.pipeline.cmind_calculate_slice_coverage import cmind_calculate_slice_coverage    

    #convert various strings to boolean
    generate_coverage, generate_figures, ForceUpdate, verbose = input2bool([generate_coverage, generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Apply Normaliztion")
                
    if isinstance(reg_struct,str):
        if os.path.exists(reg_struct):
            reg_struct=csv2dict(reg_struct)
        else:
            raise IOError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
    elif not isinstance(reg_struct,dict):
        raise ValueError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
        
    reg_struct_format='New'
    reg_struct=_convert_reg_struct(reg_struct,target=reg_struct_format)
    
    if isinstance(input_name,str):
        input_names=[]
        input_names.append(input_name)
    elif isinstance(input_name,list):
        input_names=input_name;

    if verbose:
        module_logger.debug("output_name = %s, type = %s" % (output_name,output_name.__class__))

    if (isinstance(output_name,list) and len(output_name)==1):
         output_name=output_name[0]  

    if verbose:
        module_logger.debug("output_name = %s, type = %s" % (output_name,output_name.__class__))

    if (isinstance(output_name,list) and len(output_name)==1):
        output_name=output_name[0]
         
    if output_name:
        if isinstance(output_name,str) and output_name.lower()=='default':
            default_output_names=True
        else:
            if isinstance(output_name,list):
                output_names=output_name;
            elif isinstance(output_name,str):
                output_names=[]
                output_names.append(output_name);
            if (len(output_names)!=len(input_names)):
                raise ValueError("length of output_names list, must equal length of input_names")
            default_output_names=False;
    else:  #use default filenames
        default_output_names=True
    
    if isinstance(output_dir,list):
        output_dirs=output_dir;
        if len(output_dir)!=len(input_names):
            if len(output_dir)==1:
                for ii in range(1,len(input_names)):
                    output_dirs.append(output_dirs[0]);
            else:
                raise ValueError("Length of output_dirs list, must be either a single directory or a list equal in length to the list of input_dirs")
    else:
        output_dirs=[]
        for ii in range(len(input_name)):
            output_dirs.append(output_dir)
    
    for ii in range(len(output_dirs)):
        if not os.path.exists(output_dirs[ii]):
            os.makedirs(output_dirs[ii])
    
    if isinstance(reg_flag,str):
        if len(reg_flag)==1:
            reg_flag=int(reg_flag);
            reg_flag=bin(reg_flag)[2::].zfill(3);
    #    elif len(reg_flag)==3:  #assume binary to decimal conversion is necessary
    #        reg_flag=int(reg_flag,2);
    #else:
    #    reg_flag=int(reg_flag);
    
    if len(reg_flag)==3:
        reg_flag_array=np.zeros((3,),int); 
        reg_flag_array[0]=int(reg_flag[0])
        reg_flag_array[1]=int(reg_flag[1])
        reg_flag_array[2]=int(reg_flag[2])
    else:
        raise ValueError("reg_flag should be a string of three integer values")

    do_flirt=reg_flag_array[0]
    do_fnirt=reg_flag_array[1]
    do_ANTS=reg_flag_array[2]    #0=none, 1= Exp, 2=SyN
    
    if (Norm_Target=='Structural') and (do_fnirt or do_ANTS):
        raise ValueError("only FLIRT linear transform is supported for NormTarget='Structural'")
    
    if verbose:
        module_logger.debug("reg_flag_array = {}".format(reg_flag_array))
        module_logger.debug("do_ANTS = {}".format(do_ANTS))
    
    if ( (not default_output_names) and (np.sum(reg_flag_array)!=1)):
        raise ValueError("when specific output names are given, can only choose FLIRT, FNIRT or ANTS alone.  otherwise same files would be overwritten")
    
    if not ANTS_path:
        from cmind.globals import cmind_ANTs_dir as ANTS_path
    
    if generate_figures:
        working_dir=tempfile.mkdtemp()
        rm_working_dir=True
    else:
        rm_working_dir=False    
    
    if LRtoHR_affine:  #transform from native space->structural->pediatric standard->MNI
        if isinstance(LRtoHR_affine,list):
            LRtoHR_affines=LRtoHR_affine;
            if len(LRtoHR_affine)!=len(input_names):
                if len(LRtoHR_affine)==1:
                    for ii in range(1,len(input_names)):
                        LRtoHR_affines.append(LRtoHR_affines[0]);
                else:
                    raise ValueError("Length of LRtoHR_affine list, must be either a single directory or a list equal in length to the list of input_dirs")
        else:  #if LRtoHR_affines is not a list, use the same warp for all inputs
            LRtoHR_affines=[]
            if isinstance(LRtoHR_affine,str) and LRtoHR_affine.lower()=="none":
                LRtoHR_affine=None
            for ii in range(len(input_names)):
                LRtoHR_affines.append(LRtoHR_affine)        
        
    if LRtoHR_warp:  #transform from native space->structural->pediatric standard->MNI
        if isinstance(LRtoHR_warp,list):
            LRtoHR_warps=LRtoHR_warp;
            if len(LRtoHR_warp)!=len(input_names):
                if len(LRtoHR_warp)==1:
                    for ii in range(1,len(input_names)):
                        LRtoHR_warps.append(LRtoHR_warps[0]);
                else:
                    raise ValueError("Length of LRtoHR_warp list, must be either a single directory or a list equal in length to the list of input_dirs")
        else:  #if LRtoHR_warps is not a list, use the same warp for all inputs
            LRtoHR_warps=[]
            if isinstance(LRtoHR_warp,str) and LRtoHR_warp.lower()=="none":
                LRtoHR_warp=None
                LRtoHR_warps=None
            for ii in range(len(input_names)):
                LRtoHR_warps.append(LRtoHR_warp)        
    
    if not isinstance(generate_coverage,(list,tuple)):
        generate_coverage=[generate_coverage, ]*len(input_names)
    else:
        if len(generate_coverage)==1:
            generate_coverage=generate_coverage*len(input_names)
        elif (len(generate_coverage)!=len(input_names)):
            raise ValueError("generate_coverage length must match the number of input names (or be length 1)")
            
    
    #optional list of interpolation types to perform
    if interp_type:
        if isinstance(interp_type,list):
            interp_types=interp_type;
        elif isinstance(interp_type,str):
            interp_types=[]
            interp_types.append(interp_type)
        if (len(interp_types)!=len(input_names)):
            if len(interp_types)==1:
                for ii in range(1,len(input_names)):
                    interp_types.append(interp_types[0])
            else:
                raise ValueError("Length of interp_type list, must equal length of input_names")
    else:
        interp_types=[]
        for ii in range(len(input_names)):
            interp_types.append('spline')
    
    if verbose:
        module_logger.debug("input_names={}".format(input_names))
        module_logger.debug("default_output_names={}".format(default_output_names))
        if not default_output_names:
            module_logger.debug("output_names={}".format(output_names))
        module_logger.debug("LRtoHR_affines={}".format(LRtoHR_affine))
        module_logger.debug("LRtoHR_warps={}".format(LRtoHR_warp))
        module_logger.debug("interp_types={}\n".format(interp_types))
    
    #T1proc_dir = reg_struct['T1proc_dir']
    standard_vol = reg_struct['MNI_standard_brain']
    Ninputs=len(input_names)
    all_output_names=[]
    for nn in range(Ninputs):
        
        input_name = input_names[nn]
        gen_coverage=generate_coverage[nn]
        
        if verbose:
            module_logger.info("processing input #%03d of %03d:%s" % (nn+1, Ninputs, input_name))
        (iexist, true_fname, input_name_root, input_dir, ext)=imexist(input_names[nn],'nifti-1');
        
        if not iexist:
            raise OSError("Specified input image, %s, doesn't exist!" % input_names[nn])
        input_name = true_fname #input_name_root + ext
    
        if LRtoHR_affine:
            LRtoHR_affine=LRtoHR_affines[nn];
        if LRtoHR_warp:
            LRtoHR_warp=LRtoHR_warps[nn];
        
        interp_type=interp_types[nn];
        interp_type_lower=interp_type.lower()
        if (interp_type_lower=='spline') or (interp_type_lower=='bspline'):
                ANTS_interp_type='BSpline';
                applywarp_interp_str='spline';
        elif (interp_type_lower=='lin') or (interp_type_lower=='linear') or (interp_type_lower=='trilinear'):
                ANTS_interp_type='';
                applywarp_interp_str='trilinear';
        elif (interp_type_lower=='nearestneighbour') or (interp_type_lower=='nearest') or (interp_type_lower=='nn'):
                ANTS_interp_type='NearestNeighbor';
                applywarp_interp_str='nn';
        else:
            raise ValueError("Error, invalid interp_type, %s" % interp_type)
        
        
        if Norm_Target=='MNI':
            HRtoStd_affine=reg_struct['FLIRT_HR_to_MNI_Aff']
            FLIRT_prefix='wlin_'
            FNIRT_prefix='wnonlin_'
            ANTS_prefix='wANTS_'
        elif Norm_Target=='StudyTemplate':
            HRtoStd_affine=reg_struct['FLIRT_HR_to_StudyTemplate_Aff']
            FLIRT_prefix='wlin_StudyTemplate_'
            FNIRT_prefix='wnonlin_StudyTemplate_'
            ANTS_prefix='wANTS_StudyTemplate_'
        elif Norm_Target=='Structural':
            HRtoStd_affine=None
            FLIRT_prefix='wlin_Structural_'
            FNIRT_prefix='wnonlin_Structural_'
            ANTS_prefix='wANTS_Structural_'
            standard_vol = reg_struct['HR_img']
            if ((not LRtoHR_warp) and (not LRtoHR_affine)):
                raise ValueError("must have LRtoHR_warp or LRtoHRaffine in order to transform to structural space")
        else:
            raise ValueError("Invalid Norm_Target.  Must be one of:  MNI, StudyTemplate or Structural")
            
        if LRtoHR_affine and HRtoStd_affine:
            LRtoStd_affine=os.path.join(working_dir, 'lowres2Std.mat')
            log_cmd('$FSLDIR/bin/convert_xfm -omat "%s" -concat "%s" "%s"' % (LRtoStd_affine,HRtoStd_affine,LRtoHR_affine), verbose=verbose, logger=module_logger);            
        #if LRtoHR_warp:
            #LRtoMNI_warp=os.path.join(working_dir, 'lowres2MNI_warp.nii.gz')
            #convertwarp --ref=%s --warp1=%s --postmat=%s --out=output_warp
            
        
        if do_flirt:
            if default_output_names:
                output_name = os.path.join(output_dirs[nn], FLIRT_prefix + os.path.basename(input_name));
                output_name_png = os.path.join(output_dirs[nn], FLIRT_prefix + os.path.basename(input_name_root) + '.png');
            else:
                output_name = output_names[nn];
                #[f p ext]=fileparts(output_name);
                if not os.path.splitext(output_name):  #if no extension given, set to .nii.gz
                    output_name= output_name + '.nii.gz'
                output_name_png = split_multiple_ext(output_names[nn])[0] + '.png'
            
            all_output_names.append(output_name)
            if (not imexist(output_name,'nifti-1')[0]) or ForceUpdate:
                if LRtoHR_warp:  #apply fieldmap warp for LR->HR then postmat is the affine from HR->MNI
                    if Norm_Target=='Structural':
                        flirt_affine = None;
                        flirt_warp = LRtoHR_warp
                        log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" -w %s --interp=%s' % (input_name, standard_vol, output_name,flirt_warp,applywarp_interp_str), verbose=verbose, logger=module_logger)
                    else:
                        flirt_affine = HRtoStd_affine;
                        flirt_warp = LRtoHR_warp
                        log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" --postmat="%s" -w %s --interp=%s' % (input_name, standard_vol, output_name,flirt_affine,flirt_warp,applywarp_interp_str), verbose=verbose, logger=module_logger)
                elif LRtoHR_affine:  #direct affine from LR->MNI
                    if Norm_Target=='Structural':
                        flirt_affine = LRtoHR_affine;
                    else:
                        flirt_affine = LRtoStd_affine;
                    log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" --premat="%s" --interp=%s' % (input_name, standard_vol, output_name,flirt_affine,applywarp_interp_str), verbose=verbose, logger=module_logger)
                else:  #assume already in HR space, so just affine from HR->MNI
                    flirt_affine = HRtoStd_affine;
                    log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" --premat="%s" --interp=%s' % (input_name, standard_vol, output_name,flirt_affine,applywarp_interp_str), verbose=verbose, logger=module_logger)
                    
                
            if generate_figures and (not imexist(output_name_png,'2dimg')[0]) or ForceUpdate:
                cmind_reg_report_img(output_name,standard_vol, output_name_png, working_dir)
                
            if gen_coverage:
                try:
                    cmind_calculate_slice_coverage(output_name,bg_thresh=0.05,fill_holes=False)
                except:
                    warnings.warn("Failed to generate FLIRT slice coverage image")

        if do_fnirt:  #TODO
            raise Exception("FNIRT case still incomplete!")
            if default_output_names:
                output_name_nonlin = os.path.join(output_dirs[nn], FNIRT_prefix + os.path.basename(input_name));
                output_name_nonlin_png = os.path.join(output_dirs[nn], FNIRT_prefix + os.path.basename(input_name_root) + '.png');
            else:
                output_name_nonlin = output_names[nn];
                if not os.path.splitext(output_name):  #if no extension given, set to .nii.gz
                    output_name_nonlin= output_name_nonlin + '.nii.gz'
                output_name_nonlin_png = split_multiple_ext(output_name_nonlin_png[nn])[0] + '.png'
            
            all_output_names.append(output_name_nonlin)
#            if (not imexist(output_name,'nifti-1')[0]) or ForceUpdate:
#                if(LRtoHR_affine)
#                    [status out]=system(sprintf('applywarp -i %s -r %s -o %s --premat=%s -w %s --interp=%s', input_name, standard_vol,output_name_nonlin,LRtoHR_affine,reg_struct['fnirt_warp_highres2std'],applywarp_interp_str));
#                else
#                    [status out]=system(sprintf('applywarp -i %s -r %s -o %s -w %s --interp=%s', input_name, standard_vol,output_name_nonlin,reg_struct['fnirt_warp_highres2std'],applywarp_interp_str));
#
#            if generate_figures and (not imexist(output_name_nonlin_png,'2dimg')[0]) or ForceUpdate:
#                cmind_reg_report_img(output_name_nonlin,standard_vol, output_name_nonlin_png, working_dir)
#                
#            if gen_coverage:
#                try:
#                    cmind_calculate_slice_coverage(output_name_nonlin,bg_thresh=0.05,fill_holes=False)
#                except:
#                    warnings.warn("Failed to generate FNIRT slice coverage image")
#
#        
        if do_ANTS:
            if verbose:
                module_logger.debug("In ANTS case now")
            if default_output_names:
                output_name_ANTS = os.path.join(output_dirs[nn], ANTS_prefix + os.path.basename(input_name));
            else:
                output_name_ANTS = output_names[nn];
                if not os.path.splitext(output_name_ANTS):  #if no extension given, set to .nii.gz
                    output_name_ANTS= output_name_ANTS + '.nii.gz'
            
            all_output_names.append(output_name_ANTS)   
            if ((not os.path.exists(output_name_ANTS)) or ForceUpdate):
                if verbose:
                    module_logger.debug("prepping to run ANTS")
            
                if not os.path.exists(reg_struct['ANTS_HR_to_StudyTemplate_Aff']):
                    raise Exception("Unable to find reg_struct['ANTS_HR_to_StudyTemplate_Aff'] at expected location: {}".format(reg_struct['ANTS_HR_to_StudyTemplate_Aff']))
                
                if not os.path.exists(reg_struct['ANTS_HR_to_StudyTemplate_img']):
                    raise Exception("Unable to find reg_struct['ANTS_HR_to_StudyTemplate_img'] at expected location: {}".format(reg_struct['ANTS_HR_to_StudyTemplate_img']))
                
                if LRtoHR_warp:  #affine + fieldmap warp in LR->HR
                    HR_image=reg_struct['HR_img']
                    input_HR=os.path.join(working_dir, 'tmpHR')
                    log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" -w "%s" --interp=%s' % (input_name, HR_image, input_HR, LRtoHR_warp, applywarp_interp_str), verbose=verbose, logger=module_logger)
                elif LRtoHR_affine:  #affine LR-HR
                    HR_image=reg_struct['HR_img']
                    input_HR=os.path.join(working_dir, 'tmpHR')
                    log_cmd('$FSLDIR/bin/applywarp -i "%s" -r "%s" -o "%s" --premat="%s" --interp=%s' % (input_name, HR_image, input_HR, LRtoHR_affine, applywarp_interp_str), verbose=verbose, logger=module_logger)
                else:
                    input_HR=input_name;
                    
                if 'ANTS_reg_case' in reg_struct and reg_struct['ANTS_reg_case']:
                    ANTS_reg_case=reg_struct['ANTS_reg_case']
                else:
                    ANTS_reg_case='SyN'
                    
                if Norm_Target=='MNI':
                    fixed=reg_struct['MNI_standard_brain']
                    if 'ANTS_HR_to_MNI_transform_list' in reg_struct and reg_struct['ANTS_HR_to_MNI_transform_list']:
                        transform_list=reg_struct['ANTS_HR_to_MNI_transform_list']
                    else:
                        transform_list=_transformlist_from_case(reg_struct,ANTS_reg_case)
                        reg_struct['ANTS_HR_to_MNI_transform_list'] = transform_list;
                        
                elif Norm_Target=='StudyTemplate':
                    fixed=reg_struct['StudyTemplate_brain']
                    if 'ANTS_HR_to_StudyTemplate_transform_list' in reg_struct and reg_struct['ANTS_HR_to_StudyTemplate_transform_list']:
                        transform_list=reg_struct['ANTS_HR_to_StudyTemplate_transform_list']
                    else:
                        transform_list=_transformlist_from_case(reg_struct,ANTS_reg_case)
                        reg_struct['ANTS_HR_to_StudyTemplate_transform_list'] = transform_list;
                    
                elif Norm_Target=='Structural':
                    fixed=reg_struct['HR_img']
                else:
                    raise ValueError("Invalid Norm_Target.  Must be one of:  MNI, StudyTemplate or Structural")
                
                moving=input_HR

                if (input_name.find('cope')!=-1) or (input_name.find('mask')!=-1) or (input_name.find('tstat')!=-1):  #don't make overlay images for COPES/VARCOPES
                    nopng = True
                else:
                    nopng = not generate_figures
                dim=3; inverse_flag=0;
                
                ants_applywarp(transform_list,fixed,moving,output_name_ANTS,ANTS_path=ANTS_path,dim=dim,interp_type=ANTS_interp_type,nopng=nopng,inverse_flag=inverse_flag, is_timeseries = is_timeseries);
                if gen_coverage:
                    try:
                        from .cmind_calculate_slice_coverage import cmind_calculate_slice_coverage
                        cmind_calculate_slice_coverage(output_name_ANTS,bg_thresh=0.05,fill_holes=False)
                    except:
                        warnings.warn("Failed to generate slice coverage image")
                        
            if not os.path.exists(output_name_ANTS):
                raise ValueError("Expected registration output: %s was not generated" % output_name_ANTS)

    #                 %reg_struct.ANTS_HR_to_StudyTemplate_Warp=os.path.join(T1proc_dir,reg_struct['ANTS_HR_to_StudyTemplate_Warp)
    #                 %reg_struct['ANTS_HR_to_StudyTemplate_Aff=os.path.join(T1proc_dir,reg_struct['ANTS_HR_to_StudyTemplate_Aff)
    #                 [status out]=system(sprintf('%s 3 %s %s -R %s --use-BSpline %s %s',ANTS_Warp_cmd,input_HR,output_name_ANTS,reg_struct['StudyTemplate_brain, reg_struct['ANTS_HR_to_StudyTemplate_Warp, reg_struct['ANTS_HR_to_StudyTemplate_Aff))
    #                 if(isempty(reg_struct['ANTS_StudyTemplate_to_oldpstd_Warp))  %already registered to oldest pediatric standard
    #                     [status out]=system(sprintf('%s 3 %s %s -R %s --use-BSpline %s %s',ANTS_Warp_cmd,output_name_ANTS,output_name_ANTS,standard_vol,reg_struct['ANTS_oldpstd_to_MNI_Warp,reg_struct['ANTS_oldpstd_to_MNI_Aff))
    #                 else
    #                     [status out]=system(sprintf('%s 3 %s %s -R %s --use-BSpline %s %s',ANTS_Warp_cmd,output_name_ANTS,output_name_ANTS,reg_struct['oldpstd_ref,reg_struct['ANTS_StudyTemplate_to_oldpstd_Warp,reg_struct['ANTS_StudyTemplate_to_oldpstd_Aff))
    #                     [status out]=system(sprintf('%s 3 %s %s -R %s --use-BSpline %s %s',ANTS_Warp_cmd,output_name_ANTS,output_name_ANTS,standard_vol,reg_struct['ANTS_oldpstd_to_MNI_Warp,reg_struct['ANTS_oldpstd_to_MNI_Aff))
    #                 end
                    
    
    #make sure temporary working directory has been removed
    if rm_working_dir and os.path.exists(working_dir):
        shutil.rmtree(working_dir)
        
    return all_output_names
    

def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    
    ANTS_path = '/media/Data1/BRAINSTools/bin/'
#    Norm_Target='StudyTemplate'
    interp_type = '';
    generate_figures = True;
    generate_coverage = True
    ForceUpdate = True;
    verbose = True
    
    reg_struct = pjoin(cmind_example_output_dir,'P','T1_proc','reg_struct.csv')    
    
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',file_mode='w')  
    if verbose:
        func=cmind_timer(cmind_apply_normalization,logger=logger)
    else:
        func=cmind_apply_normalization

    test_cases=['CBF'] #'HARDI','DTI']
    for test_case in test_cases:
        if test_case=='CBF':
            output_dir = pjoin(cmind_example_output_dir,'P','BaselineCBF')
            input_name = [pjoin(output_dir,'CBF_Wang2002.nii.gz'),
                        pjoin(output_dir,'I0_map_reg2CBF.nii.gz'),
                        pjoin(output_dir,'T1_map_reg2CBF.nii.gz') ]
            output_name = 'default'
            Norm_Target = 'MNI'  
            reg_flag = '101'          
            LRtoHR_affine = pjoin(output_dir,'reg_BBR_fmap','lowres2highres.mat')
            LRtoHR_warp = pjoin(output_dir,'reg_BBR_fmap','lowres2highres_warp.nii.gz')
        elif test_case=='DTI':
            output_dir = pjoin(cmind_example_output_dir,'P','DTI')
            input_name = [pjoin(output_dir,'dtifit_FA.nii.gz'),
                        pjoin(output_dir,'dtifit_MD.nii.gz'),
                        ]
            output_name = 'default'
            Norm_Target = 'Structural'
            reg_flag = '100'
            LRtoHR_affine = pjoin(output_dir,'regBBR','lowres2highres.mat')
            LRtoHR_warp = pjoin(output_dir,'regBBR','lowres2highres_warp.nii.gz')
        elif test_case=='HARDI':
            output_dir = pjoin(cmind_example_output_dir,'F','HARDI')
            input_name = [pjoin(output_dir,'dtifit_FA.nii.gz'),
                        pjoin(output_dir,'dtifit_MD.nii.gz'),
                        ]
            output_name = 'default'
            Norm_Target = 'Structural'  
            reg_flag = '100'          
            LRtoHR_affine = pjoin(output_dir,'regBBR','lowres2highres.mat')
            LRtoHR_warp = pjoin(output_dir,'regBBR','lowres2highres_warp.nii.gz')
#            applywarp -i custom_mask.nii.gz -r ../T1_proc/T1W_Structural_N4_brain -o wline_Structural_custom_mask --interp=nn -w regBBR/lowres2highres_warp.nii.gz
        else:
            raise ValueError("Unknown test case: {}".format(test_case))  
     
        func(output_dir, reg_struct, input_name, output_name, LRtoHR_affine, 
            LRtoHR_warp=LRtoHR_warp, ANTS_path=ANTS_path, Norm_Target=Norm_Target, 
            reg_flag=reg_flag, interp_type=interp_type, generate_coverage=generate_coverage,
            generate_figures=generate_figures, ForceUpdate=ForceUpdate, 
            verbose=verbose, logger=logger)            
        
    
if __name__=='__main__':
    import sys,argparse
    from cmind.utils.utils import str2none, str2list, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Normalize input_name to standard space using previously computed transforms", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-rsf","--reg_struct",required=True, help="filename of the .csv file containing the registration dictionary")
        parser.add_argument("-i","--input_name",required=True, help='image filename for the "fixed" image (registration target)')
        parser.add_argument("-out","--output_name", type=str, help="image filename for the output")
        parser.add_argument("-aff","--LRtoHR_affine", type=str, help="affine transform from functional to structural space.if LRtoHR_affine='', it is assumed the image is already in structural space")
        parser.add_argument("-w","--LRtoHR_warp", type=str, default="none", help="fieldmap warp correction for functional space image, if LRtoHR_warp='' or None, no warping is applied")
        parser.add_argument("-a","-ants","--ANTS_path", type=str, default="", help="path to ANTs binaries")
        parser.add_argument("-reg","--reg_flag", type=str, default='001', help='3 character string such as 101 controlling which registration to perform. first digit is for FLIRT; second digit is for FNIRT (not currently implemented); third digit is for ANTs, SyN; e.g.  101 would run FLIRT & ANTs registrations. can also specify the binary "101" as the integer 5, etc.')
        parser.add_argument("--Norm_Target", type=str, default='MNI', help="Normalization target: {'MNI','StudyTemplate'}")
        parser.add_argument("-interp","--interp_type", type=str, default='default', help="{'spline','bspline'} call spline based interpolation; {'lin', 'linear','trilinear'} call trilinear interpolation; {'nearestneighbour','nearest','nn'} call nearest neighbor interpolation; interpolation type to use")
        parser.add_argument("--generate_coverage", type=str, default="false", help="if true, generate binary masks of the slice coverage.  can be an array of values for each input image")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate overlay images summarizing the registration")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
          
        
        
        args=parser.parse_args()
        
        output_dir=args.output_dir;
        reg_struct=args.reg_struct;
        input_name=str2list(args.input_name);
        output_name=str2list(args.output_name);
        LRtoHR_affine=str2list(args.LRtoHR_affine);
        LRtoHR_warp=str2list(args.LRtoHR_warp);
        
        if len(LRtoHR_affine)==1:
            LRtoHR_affine=str2none(LRtoHR_affine[0]) 

        if len(LRtoHR_warp)==1:
            LRtoHR_warp=str2none(LRtoHR_warp[0])
            
        ANTS_path=str2none(args.ANTS_path);
        Norm_Target=args.Norm_Target;
        reg_flag=args.reg_flag;
        interp_type=args.interp_type;
        generate_figures=args.generate_figures;
        generate_coverage=str2list(args.generate_coverage);
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger
        if interp_type.lower()=='default':
            interp_type=''

        if verbose:
            cmind_apply_normalization=cmind_timer(cmind_apply_normalization,logger=logger)
    
        cmind_apply_normalization(output_dir, 
                                  reg_struct, 
                                  input_name, 
                                  output_name, 
                                  LRtoHR_affine=LRtoHR_affine, 
                                  LRtoHR_warp=LRtoHR_warp, 
                                  ANTS_path=ANTS_path, 
                                  Norm_Target=Norm_Target, 
                                  reg_flag=reg_flag, 
                                  interp_type=interp_type, 
                                  generate_coverage=generate_coverage, 
                                  generate_figures=generate_figures, 
                                  ForceUpdate=ForceUpdate, 
                                  verbose=verbose, 
                                  logger=logger)

