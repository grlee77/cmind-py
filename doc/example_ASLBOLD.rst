ASL/BOLD Data files
-------------------

First-level analysis results for the simultaneous ASL/BOLD data analysis are avaialable 
in the database.  The ASL and BOLD results are stored as separate .tar.gz folders

Details of the first-level processing:
First-level analysis was carried out primarily via FSL.  ANTs tools were used for bias 
correction and spatial normalisation.  The results in the database correspond to
a spatial smoothing of 8 mm and highpass filter of 104 s.  In greater detail:

1.) motion correction
2.) spatial/temporal filtering
3.) outlier detection
4.) GLM
5.) lowres->structural coregistration
6.) structural->pediatric template->MNI normalization

Note:  A small number of subjects will have only a BOLD result, with no corrsponding 
processed ASL data.  This correspond to subjects where the arterial spin labeling 
process was deemed to have failed across at least one of the major arterial territories.


Setting up a second level analysis
----------------------------------

Second level analysis can most easily be performed using FSL's FEAT.  Extracting the
.tar.gz files from several subjects would result in a series of first-level Feat
analysis folders.  These can then be selected from the FSL Feat GUI in the usual
manner to carry out a second level analysis.

A python script is also available that can build (and optionally run) a FEAT .fsf file
from a list of subject folders and regressors.

