.. _installation:

============
Installation
============

cmind-py can be installed in linux or Mac OS X.  

Although installation is probably possible on Windows, the utility will be 
greatly reduced as many of the functions rely on tools from FSL_, which only 
works in UNIX-like OSes.  To use the software on Windows we recommend the use of 
a linux virtual machine with the appropriate dependencies_ installed: 
`more info <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation/Windows>`_

.. _downloading:

Downloading
-----------

A GIT_ repository containing the package source code is available at: 
`https://bitbucket.org/grlee77/cmind-py <https://bitbucket.org/grlee77/cmind-py/>`_ 

A zipped or tarred archive of the source code can be downloaded from the 
`Downloads tab <https://bitbucket.org/grlee77/cmind-py/downloads>`_.  

Alternatively, the source code may be checked out via git:

:: 

    git clone https://grlee77@bitbucket.org/grlee77/cmind-py.git


.. TODO: upload to pypy or other package index for easier installation via pip or easy_install

Installation from source
------------------------

:: 

    python setup.py install

Note: You may need to have root access to install the python packages.  This can 
be achieved using sudo to obtain superuser priveledges as follows:

:: 

    sudo python setup.py install
    
  
To specify an alternative installation location use the --prefix argument:

:: 

    sudo python setup.py install --prefix=/usr/local

.. _environment_vars:

Specifying Pediatric Template Locations
---------------------------------------

A set of C-MIND pediatric study templates are available here:  `CMIND_Templates <https://bitbucket.org/grlee77/cmind-py/downloads>`_

cmind-py will search for these templates in the following locations, using the 
first location at which the template folder is found:

:: 

    CMIND_TEMPLATE_DIR environment variable
    ~/cmind_templates    (where ~ is the user's home account)
    /usr/local/cmind_templates
    /usr/share/cmind_templates
    /opt/cmind_templates

Setting Up the Environment
--------------------------

cmind-py will attempt to automatically find FSL_, ANTS_ and other software as 
needed at run time.  

Run the following command to start cmind and print environment info:

:: 

    python -c 'import cmind; cmind.print_environment_info()'

Install and/or set the suggested environment variables for any desired packages 
that are listed as missing.

.. _dependencies:

Dependencies
------------

All version numbers listed are those the software was developed with.  Older versions are likely to work in some instances, but this has not been tested.  The python pipelines make heavy use of routines from the fsl software library and the Advanced Normalization Tools.

-	Python_ 2.7+ (recommended) or 3.2+ (currently untested)
-	fsl_ 5.0+  (developed using fsl 5.0.2, 5.0.4 and 5.0.6). 
-	ANTs_ 1.9.x+  (needs to be recent enough to have antsRegistration & antsApplyTransforms)

Most of the pipeline modules should also work with FSL 4.1, but *cmind_fieldmap_regBBR.py* will not.

**The following Python modules are required**

-	`numpy`_
-	`scipy`_
-	`matplotlib`_
-	`nibabel`_

The minimum required version for each of these packages has not been determined.  The software was developed using numpy 1.8.0, scipy 0.13.0, matplotlib 1.3.1, nibabel 1.3.0.

**python modules needed for building the documentation**

-	`sphinx`_

**The DTI/HARDI preprocessing pipeline requires the following non-Python software**

-	DTIPrep_ 1.1+  (tested with versions 1.1.6 and 1.2)
-	(optional):  Slicer_ 4.2+ (tested with version 4.2.0)  (only needed if denoising options are selected)

**other optional non-Python dependencies**

-	ImageMagick_ (tested usign 6.5.4 and 6.6.9)
-	AFNI_ (not currently used by default)

**The following module, required by cmind_T1map_fit.py, is packaged with cmind-py**

-	leastsqbound_

.. include:: links_names.txt
