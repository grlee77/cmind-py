#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

#TODO:  use zcrop, standard_head,  standard_head_noface_mask            
def cmind_deface(input_nii, output_nii=None, standard_head=None, standard_head_noface_mask=None, z_crop_mm=None, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):  #, iterate_segmentation=True, Nsegmentation_iters=1
    """Structural volume preprocessing pipeline
    
    Parameters
    ----------
    input_nii : str
        filename of NIFTI or NIFTIGZ volume to process.  This should be a head 
        volume prior to brain extraction.
    output_nii : str, optional
        defaced output filename.  default is to append "_defaced" to the input filename
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    fname_defaced : str
        head volume with face removed
       
    See Also
    --------
    cmind_crop_robust.py, cmind_bias_correct.py, cmind_brain_extract.py, cmind_structural.py
    
    Notes
    -----
    
    This function is a wrapper that calls other structural processing routines in the following order:
        - cmind_crop_robust.py
        - cmind_bias_correct.py
        - cmind_brain_extract.py
        - cmind_structural.py (optional)
        
    """

    import os, shutil, tempfile
    
    from cmind.globals import cmind_ANTs_dir
    from cmind.pipeline.cmind_crop_robust import cmind_crop_robust
    #from cmind.pipeline.cmind_brain_extract import cmind_brain_extract
    #from cmind.pipeline.cmind_bias_correct import cmind_bias_correct
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist
    from cmind.utils.image_utils import slicer_mid_sag_fig
    from cmind.utils.logging_utils import cmind_init_logging, cmind_func_info, log_cmd
    from cmind.pipeline.ants_register import ants_register
    from cmind.pipeline.ants_applywarp import ants_applywarp

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
        
    if verbose:
        cmind_func_info(logger=logger)     
    
    (imbool, input_nii_full, input_nii_root, input_nii_path, input_nii_ext)=imexist(input_nii,'nifti-1')
    if not imbool:
        raise IOError("input image, %s, not found!" % input_nii)

    (imbool, standard_head_full)=imexist(standard_head,'nifti-1')[0:2]
    if not imbool:
        raise IOError("standard_head image, %s, not found!" % standard_head)
    else:
        standard_head=standard_head_full

    (imbool, standard_head_noface_mask_full)=imexist(standard_head_noface_mask,'nifti-1')[0:2]
    if not imbool:
        raise IOError("standard_head_noface_mask image, %s, not found!" % standard_head_noface_mask)
    else:
        standard_head_noface_mask=standard_head_noface_mask_full

    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Defacing")      
          
    if not output_nii:
        output_nii=input_nii_full.replace(input_nii_ext,'_defaced'+input_nii_ext)
        
    output_dir=os.path.dirname(output_nii)
    if not output_dir:
        output_dir=os.getcwd()
    elif not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    fname_defaced=output_nii
    if (not imexist(fname_defaced,'nifti-1')[0]) or ForceUpdate:
        working_dir=tempfile.mkdtemp()  #do the computations in a working directory
        output_root=os.path.join(working_dir,'StudyTemplate_to_Struct_');
        
        if zcrop:
            fname_crop=os.path.join(working_dir,'Structural_Crop')
            cmind_crop_robust(input_nii_full, fname_crop, z_crop_mm=z_crop_mm, oname_root='T1W', generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
        else:
            fname_crop=input_nii_full
        
        fixed=fname_crop
        moving=standard_head
        nopng=0
        ANTS_precision='defacer'
        affine_only=False
        (warp_file, aff_file, output_warped)=ants_register(fixed,moving,working_dir, output_root,ANTS_path=cmind_ANTs_dir,operation_type="both",affine_only=affine_only ,rigid_affine=False,concat_transforms=True,dim=3,nopng=nopng,precision_case=ANTS_precision,verbose=verbose,logger=module_logger)
        
        transform_list=[]
        transform_list.append(aff_file);
        if not affine_only:
            transform_list.append(warp_file);
        moving=standard_head_noface_mask
        output_warped=os.path.join(working_dir,'noface_mask.nii.gz');
        ants_applywarp(transform_list,fixed,moving,output_warped,ANTS_path=cmind_ANTs_dir,dim=3,nopng=nopng,inverse_flag=False,verbose=verbose,logger=module_logger)
        
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mul "%s" "%s"' % (fname_crop,output_warped,fname_defaced),verbose=verbose,logger=module_logger)
        
        if generate_figures:
            slicer_mid_sag_fig(fname_defaced)
            
        shutil.rmtree(working_dir)  #remove the working directory
    
    #fname_head,bias_field_vol=cmind_bias_correct(fname_crop, fname_bias, ANTS_bias_cmd, oname_root=oname_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
    #(fname_brain,fname_brain_mask)=cmind_brain_extract(fname_head, bet_thresh=bet_thresh, oname_root=oname_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
    output_tuple=(fname_defaced) #fname_defaced)
    return output_tuple
    
def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_dir, cmind_example_output_dir, cmind_template_dir
       
    age_months=84.7
    ped_template_path=cmind_template_dir
    
    generate_figures=True
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')        
    if verbose:
        func=cmind_timer(cmind_deface,logger=logger)
    else:
        func=cmind_deface

    testcases=['P'] #,'F']
    for testcase in testcases:    
        if testcase=='F':
            input_nii=os.path.join(cmind_example_dir,'IRC04H_06M008_F_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1.nii.gz')
            output_nii=os.path.join(cmind_example_output_dir,'F','T1W_defaced.nii.gz')
        elif testcase=='P':
            input_nii=os.path.join(cmind_example_dir,'IRC04H_06M008_P_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1.nii.gz')
            output_nii=os.path.join(cmind_example_output_dir,'P','T1W_defaced.nii.gz')
     
        func(input_nii,output_nii=output_nii,ped_template_path=ped_template_path,age_months=age_months, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
  
def _arg_parser():
    import argparse
    parser = argparse.ArgumentParser(description="Deface a structural volume.  This is done by inverse registration of a template into the subject space.  A predifined mask excluding face voxels is then applied to the structural image.", epilog="")  #-h, --help exist by default
    #example of a positional argument
    parser.add_argument("-i","--input_nii", required=True, dest="input_nii",help="filename of NIFTI or NIFTIGZ volume to process")
    parser.add_argument("-o","--output_nii", dest="output_nii",help='defaced output filename.  default is to append "_defaced" to the input filename')
    parser.add_argument("-h","--standard_head",dest="standard_head", required=True, type=str, help="head volume")
    parser.add_argument("-m","--standard_noface_mask",dest="standard_noface_mask", required=True, type=str, help="binary mask for head volume excluding face voxels")
    parser.add_argument("-c","--z_crop_mm",dest="z_crop_mm",type=str,default="None", help="extent to crop volume in z-direction prior to registration.  Can be used to remove neck/shoulders from FOV to improve registration.")
    group1 = parser.add_argument_group('additional optional arguments', 'Other arguments affecting program execution')
    group1.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
    group1.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
    group1.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
    group1.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
    return parser
        
     
if __name__ == '__main__':
    import sys
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and (sys.argv[1]=='test' or sys.argv[1]=='testF'):
        _testme()
    else:    
        parser=_arg_parser()
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        input_nii=args.input_nii;
        output_nii=args.output_nii;
        z_crop_mm=str2none(args.z_crop_mm);
        standard_head=args.standard_head;
        standard_noface_mask=args.standard_noface_mask;
        generate_figures=args.generate_figures;
        #UCLA_flag=args.UCLA_flag;
        #generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger
        
        if verbose:
            cmind_deface=cmind_timer(cmind_deface,logger=logger)
    
        cmind_deface(input_nii=input_nii,output_nii=output_nii, ped_template_path=ped_template_path,age_months=age_months, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
        
                        