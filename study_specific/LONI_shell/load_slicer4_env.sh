#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $cwd/exec_exists.sh

if exec_exists module;
then
	#echo "found module"
	module load slicer4/4.2.0
	LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SLICER4_HOME/lib/Slicer-4.2
else
	if exec_exists /media/Data1/Downloads/Slicer-4.2.0-2013-03-12-linux-amd64/Slicer
	then
		#echo "module not found, but Slicer already in path"	
		echo ""
	else
		echo "Error:  module not found, and Slicer not already on path"
		exit 1
	fi
fi

