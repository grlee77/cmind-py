#!/bin/bash
echo "CMD: $*"

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/load_python_env.sh
source $cwd/timer.sh

REQUIRED_ARGS=2

if [ $# -lt $REQUIRED_ARGS ]
then
  echo "Usage: `basename $0` input_file --age_months age_months [generate_figures] [bias_cor]"
  echo "output will be appended (_defaced) to input file"
  echo "generate_figures        [true|false]"
  echo "bias_cor                [true|false]"
  exit 1
fi


#Assign input arguments to variables
generate_figures="True"
bias_cor="True"
while [ "$1" != "" ]; do
    case $1 in
    -i )
        shift
        input=$1
        ;;
    --age_months )
        shift
        age_months=$1
        ;;
    --generate_figures )
        shift
        generate_figures=$1
        ;;
    --bias_cor )
        shift
        bias_cor=$1
        ;;
    --oblique )
        shift
        oblique="True"
        ;;
    esac
    shift
done

echo "INPUT: $input"
echo "AGE_MONTHS: $age_months"
echo "GENERATE_FIGURES: $generate_figures"
echo "BIAS_COR: $bias_cor"
echo "OBLIQUE: $oblique (NOT Implemented)"

t=$(timer)

python ${cmind_python_dir}/cmind_deface.py $input --age_months $age_months --generate_figures $generate_figures --bias_cor $bias_cor --verbose
printf 'cmind_deface:  Elapsed time: %s\n' $(timer $t)

defaced=${input/.nii.gz/_defaced.nii.gz}
if [ ! -f $defaced ];then
    echo "Could not create $defaced file"
    exit 1
fi

echo "Finished cmind_deface conversion"
