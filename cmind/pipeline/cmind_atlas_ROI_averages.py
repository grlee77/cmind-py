#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

#TODO:  could just take a list of any lowres volumes as input instead of T1/CBF explicitly

def cmind_atlas_ROI_averages(output_dir, GMpve, WMpve, CSFpve, atlas_vol, CBF_quant_vol=None, T1_quant_vol=None, DeepGMpve = None, ROI_labels_csv=None, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):
    """calculate average T1 value for each tissue type (GM, WM, CSF)
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    GMpve : str or list of str
        GM partial volume estimate in subject space
    WMpve : str or list of str
        WM partial volume estimate in subject space
    CSFpve : str or list of str
        CSF partial volume estimate in subject space
    atlas_vol : str
        AAL atlas registered to subject space
    CBF_quant_vol : str or None, optional
        quantitative ASL image in subject space
    T1_quant_vol : str or None, optional
        quantitative T1 map image in subject space
    DeepGMpve : str or list of str, optional
        Deep GM partial volume estimate in subject space
    ROI_labels_csv : str, optional
        .csv file with integers in first column and corresponding labels in 2nd column.  
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    dataframe_csvfile : str
        pandas dataframe stored as a .csv file
    
    """
    
    import os
    from os.path import join as pjoin
    
    import nibabel as nib
    import numpy as np
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict
    from cmind.utils.logging_utils import cmind_init_logging, cmind_func_info #, log_cmd

    try:
        import pandas as pd
    except:
        raise ValueError("This module requires Pandas to run")

    #KLUDGE:  Support list input in addition to string so this can be connected 
    #         directly to the output of a nipype Split() node
    if isinstance(GMpve,list):
        if len(GMpve)>1:
            raise ValueError("Only single item list input allowed for GMpve")
        else:
            GMpve=GMpve[0]
    if isinstance(WMpve,list):
        if len(WMpve)>1:
            raise ValueError("Only single item list input allowed for WMpve")
        else:
            WMpve=WMpve[0]
    if isinstance(CSFpve,list):
        if len(CSFpve)>1:
            raise ValueError("Only single item list input allowed for CSFpve")
        else:
            CSFpve=CSFpve[0]
    if isinstance(DeepGMpve,list):
        if len(DeepGMpve)>1:
            raise ValueError("Only single item list input allowed for DeepGMpve")
        else:
            DeepGMpve=DeepGMpve[0]
            
    ROI_labels=None
    if ROI_labels_csv is not None:
        if not os.path.exists(ROI_labels_csv):
            raise ValueError("specified ROI label file not found")
        else:
            ROI_labels=csv2dict(ROI_labels_csv)
            
    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting to compute atlas-based ROI averages")
            
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    (imbool, CSFpve)=imexist(CSFpve,'nifti-1')[0:2]; 
    if not imbool:
        raise IOError("CSFpve, %s, missing" % CSFpve)
        
    #GMpve=pjoin(T1segment_dir,'T1Segment_GM.nii.gz');                                
    (imbool, GMpve)=imexist(GMpve,'nifti-1')[0:2];  
    if not imbool:
        raise IOError("GMpve, %s, missing" % GMpve)

    #WMpve=pjoin(T1segment_dir,'T1Segment_WM.nii.gz');                                
    (imbool, WMpve)=imexist(WMpve,'nifti-1')[0:2]; 
    if not imbool:
        raise IOError("WMpve, %s, missing" % WMpve)
        raise

    if DeepGMpve is not None:
        (imbool, DeepGMpve)=imexist(DeepGMpve,'nifti-1')[0:2];  
        if not imbool:
            raise IOError("DeepGMpve, %s, missing" % DeepGMpve)       
        has_DeepGM=True
    else:
        has_DeepGM=False
            
    (imbool, atlas_vol)=imexist(atlas_vol,'nifti-1')[0:2];  
    if not imbool:
        raise IOError("atlas_vol not found!" % atlas_vol)
        
    if CBF_quant_vol is not None:
        (imbool, CBF_quant_vol)=imexist(CBF_quant_vol,'nifti-1')[0:2];  
        if not imbool:
            raise IOError("CBF_quant_vol not found!" % CBF_quant_vol)
        has_CBF = True
    else:
        has_CBF = False

    if T1_quant_vol is not None:
        (imbool, T1_quant_vol)=imexist(T1_quant_vol,'nifti-1')[0:2];  
        if not imbool:
            raise IOError("CBF_quant_vol not found!" % T1_quant_vol)
        has_T1 = True
    else:
        has_T1 = False
        
    dataframe_csvfile = pjoin(output_dir,'ROI_data.csv')

    #TODO :output_to_check=pjoin(output_dir,'mean_T1s_NRMSE_masked.txt'))
    if (not os.path.exists(dataframe_csvfile)) or ForceUpdate:
        
        atlas_nii = nib.load(atlas_vol)
        atlas = atlas_nii.get_data()
        
        
        if has_CBF:
            CBF_nii = nib.load(CBF_quant_vol)
            CBF = CBF_nii.get_data()
            
        GM_nii = nib.load(GMpve)
        GM = GM_nii.get_data()

        if has_T1:
            T1map_nii = nib.load(T1_quant_vol)
            T1map = T1map_nii.get_data()
        
        if has_DeepGM:
            DeepGM_nii = nib.load(DeepGMpve)
            DeepGM = DeepGM_nii.get_data()
        else:
            DeepGM = 0

        WM_nii = nib.load(WMpve)
        WM = WM_nii.get_data()

        CSF_nii = nib.load(CSFpve)
        CSF = CSF_nii.get_data()

        #ROI_integers=np.sort(np.unique(atlas[atlas>0]))
        ROI_integers=np.sort(np.unique(atlas))
        
        if np.sum(ROI_integers-np.asarray(ROI_integers,dtype=int))>0:
            raise ValueError("Expected to only find integer values in ROI_integers")
#        else:
#            ROI_integers = np.asarray(list(ROI_integers,dtype=np.int)

        df_columns=['ROI name',
                                   'ival',
                                   'fGM',
                                   'fWM',
                                   'fCSF',
                                   'fOther',
                                   'N_GM75',
                                   'N_WM75',
                                   'N_GM90',
                                   'N_WM90',
                                   'N_ROI']
                                   
        if has_CBF:
            df_columns = df_columns + ['CBF_GM75',
                                      'CBF_WM75',
                                      'CBF_GM90',
                                      'CBF_WM90',
                                      'CBF_ratio_90',
                                      'CBF',
                                      'CBF_cor_ratio3',
                                      'CBF_cor_ratio_meas',
                                      'rel_CBF',
                                      'rel_CBF_cor_ratio3',
                                      'rel_CBF_cor_ratio_meas',
                                      ]           
        if has_T1:
             df_columns = df_columns + ['T1_GM75',
                                      'T1_WM75',
                                      'T1_GM90',
                                      'T1_WM90',
                                      'T1_ratio_75',
                                      'T1_ratio_90',
                                      'rel_GM_T1_75',
                                      'rel_WM_T1_75',
                                      'rel_GM_T1_90',
                                      'rel_WM_T1_90',
                                      ]           
                                       
        if has_DeepGM:
            df_columns = df_columns + ['fDeepGM',
                                       'N_DeepGM75',
                                       'N_DeepGM9']

        if has_DeepGM and has_CBF:
            df_columns = df_columns + ['CBF_DeepGM75',
                                       'CBF_DeepGM90',
                                       'CBF_DeepGM_ratio_90',
                                      ]
                                       
        df = pd.DataFrame(index=np.asarray(ROI_integers,dtype=np.int),
                          columns=df_columns)

        global_mask = (GM+WM+DeepGM) > 0.25;
        GM_75 = GM>0.75
        WM_75 = WM>0.75
        GM_90 = GM>0.90
        WM_90 = WM>0.90
        
        if has_CBF:
            CBF_fit_mask = (CBF>0)
            global_mask = global_mask * CBF_fit_mask #only keep pixels where CBF was fitted (some slices may be missing, etc)
            CBF_GM75 = np.mean(CBF[GM_75])
            CBF_WM75 = np.mean(CBF[WM_75])
            CBF_GM90 = np.mean(CBF[GM_90])
            CBF_WM90 = np.mean(CBF[WM_90])
            GM_WM_ratio_3 = 3.0
            GM_DeepGM_ratio_1pt5 = 1.5
            GM_WM_ratio = CBF_GM90/CBF_WM90
            CBF_global = np.mean(CBF[global_mask])
        elif has_T1:
            T1map_fit_mask = (T1map>0)
            global_mask = global_mask * T1map_fit_mask #only keep pixels where CBF was fitted (some slices may be missing, etc)
        
        if has_T1:
            T1_GM75 = np.mean(T1map[GM_75])
            T1_WM75 = np.mean(T1map[WM_75])
            T1_GM90 = np.mean(T1map[GM_90])
            T1_WM90 = np.mean(T1map[WM_90])            
        
        fGM = np.mean(GM[global_mask])
        fWM = np.mean(WM[global_mask])
        fCSF = np.mean(CSF[global_mask])
        f_other = 1 - fGM - fWM - fCSF
        
        use_DeepGM_in_PVE_calc = True
        
        
        if has_DeepGM:
            DeepGM_75 = DeepGM>0.75
            DeepGM_90 = DeepGM>0.90            
            fDeepGM = np.mean(DeepGM[global_mask])
            if has_CBF:            
                CBF_DeepGM75 = np.mean(CBF[DeepGM_75])
                CBF_DeepGM90 = np.mean(CBF[DeepGM_90])
                GM_DeepGM_ratio = CBF_GM90/CBF_DeepGM90
            
        if has_CBF:
            if has_DeepGM and use_DeepGM_in_PVE_calc:
                CBF_global_PVEcor_ratio3 = CBF_global/(np.mean(GM[global_mask])+np.mean(WM[global_mask])/GM_WM_ratio_3 + np.mean(DeepGM[global_mask])/GM_DeepGM_ratio_1pt5)
                CBF_global_PVEcor = CBF_global/(np.mean(GM[global_mask])+np.mean(WM[global_mask])/GM_WM_ratio+ np.mean(DeepGM[global_mask])/GM_DeepGM_ratio)
            else:
                CBF_global_PVEcor_ratio3 = CBF_global/(np.mean(GM[global_mask])+np.mean(WM[global_mask])/GM_WM_ratio_3)
                CBF_global_PVEcor = CBF_global/(np.mean(GM[global_mask])+np.mean(WM[global_mask])/GM_WM_ratio)
            
        #use index 0 for global properties
        df.loc[0]['ROI name']='Global Average'
        df.loc[0]['ival']=0
        df.loc[0]['fGM']=fGM
        df.loc[0]['fWM']=fWM
        df.loc[0]['fCSF']=fCSF
        df.loc[0]['fOther']=f_other
        df.loc[0]['N_GM75']=np.sum(GM_75)
        df.loc[0]['N_WM75']=np.sum(WM_75)
        df.loc[0]['N_GM90']=np.sum(GM_90)
        df.loc[0]['N_WM90']=np.sum(WM_90)
        df.loc[0]['N_ROI']=np.sum(global_mask)

        if has_CBF:
            df.loc[0]['CBF_GM75']=CBF_GM75
            df.loc[0]['CBF_WM75']=CBF_WM75
            df.loc[0]['CBF_GM90']=CBF_GM90
            df.loc[0]['CBF_WM90']=CBF_WM90
            df.loc[0]['CBF_ratio_90']=GM_WM_ratio
            df.loc[0]['CBF']=CBF_global
            df.loc[0]['CBF_cor_ratio3']=CBF_global_PVEcor_ratio3
            df.loc[0]['CBF_cor_ratio_meas']=CBF_global_PVEcor

        if has_T1:
            df.loc[0]['T1_GM75']=T1_GM75
            df.loc[0]['T1_WM75']=T1_WM75
            df.loc[0]['T1_GM90']=T1_GM90
            df.loc[0]['T1_WM90']=T1_WM90
            df.loc[0]['T1_ratio_75']=T1_GM75/T1_WM75
            df.loc[0]['T1_ratio_90']=T1_GM90/T1_WM90

        if has_DeepGM:
            df.loc[0]['N_DeepGM75']=np.sum(DeepGM_75)
            df.loc[0]['N_DeepGM90']=np.sum(DeepGM_90)            
            df.loc[0]['fDeepGM']=fDeepGM
            if has_CBF:
                df.loc[0]['CBF_DeepGM75']=CBF_DeepGM75
                df.loc[0]['CBF_DeepGM90']=CBF_DeepGM90
                df.loc[0]['CBF_DeepGM_ratio_90']=GM_DeepGM_ratio
                
        if False:
            import matplotlib.pyplot as plt
            from cmind.utils.montager import montager
            plt.figure(); plt.imshow(montager(GM_75))
            plt.figure(); plt.imshow(montager(global_mask))        

                                   
        for ROI_idx,ROI in enumerate(ROI_integers[1:]):
            ROI_mask = (atlas==ROI) * global_mask
            GM_ROI_75 = ROI_mask * (GM>0.75)
            WM_ROI_75 = ROI_mask * (WM>0.75)
            GM_ROI_90 = ROI_mask * (GM>0.90)
            WM_ROI_90 = ROI_mask * (WM>0.90)
            N_GM75 = np.sum(GM_ROI_75)
            N_WM75 = np.sum(WM_ROI_75)
            N_GM90 = np.sum(GM_ROI_90)
            N_WM90 = np.sum(WM_ROI_90)
            
            GM_fraction = np.mean(GM[ROI_mask])
            WM_fraction = np.mean(WM[ROI_mask])
            CSF_fraction = np.mean(CSF[ROI_mask])
            if has_DeepGM:
                DeepGM_ROI_75 = ROI_mask * (DeepGM>0.75)
                DeepGM_ROI_90 = ROI_mask * (DeepGM>0.90)
                N_DeepGM75 = np.sum(DeepGM_ROI_75)
                N_DeepGM90 = np.sum(DeepGM_ROI_90)
                DeepGM_fraction = np.mean(DeepGM[ROI_mask])
            else:
                DeepGM_fraction = 0;
            other_fraction = 1 - GM_fraction - WM_fraction - CSF_fraction - DeepGM_fraction
            
            if has_CBF:
                CBF_avg = np.mean(CBF[ROI_mask])
                
                if has_DeepGM and use_DeepGM_in_PVE_calc:
                    CBF_avg_pvecor_ratio3 = CBF_avg/(GM_fraction + WM_fraction/GM_WM_ratio_3 + DeepGM_fraction/GM_DeepGM_ratio_1pt5)
                    CBF_avg_pvecor = CBF_avg/(GM_fraction + WM_fraction/GM_WM_ratio + DeepGM_fraction/GM_DeepGM_ratio)
                else:
                    CBF_avg_pvecor_ratio3 = CBF_avg/(GM_fraction + WM_fraction/GM_WM_ratio_3)
                    CBF_avg_pvecor = CBF_avg/(GM_fraction + WM_fraction/GM_WM_ratio)
                
            idx=int(ROI)
            
            df.loc[idx]['ROI name']=''
            if ROI_labels is not None:
                try:
                    df.loc[idx]['ROI name']=ROI_labels["%d" % idx]
                except:
                    pass
                    
            N_ROI = np.sum(ROI_mask)
            df.loc[idx]['ival']=idx
            df.loc[idx]['N_GM75']=N_GM75
            df.loc[idx]['N_WM75']=N_WM75
            df.loc[idx]['N_GM90']=N_GM90
            df.loc[idx]['N_WM90']=N_WM90
            df.loc[idx]['N_ROI']=N_ROI
            
            if N_ROI>10:  #only record if the ROI has a more than 10 pixels
                df.loc[idx]['fGM']=GM_fraction
                df.loc[idx]['fWM']=WM_fraction
                df.loc[idx]['fCSF']=CSF_fraction
                df.loc[idx]['fOther']=other_fraction
                if has_CBF:
                    df.loc[idx]['CBF']=CBF_avg
                    df.loc[idx]['CBF_cor_ratio3']=CBF_avg_pvecor_ratio3
                    df.loc[idx]['CBF_cor_ratio_meas']=CBF_avg_pvecor
                    df.loc[idx]['CBF_GM75']=np.mean(CBF[GM_ROI_75])
                    df.loc[idx]['CBF_WM75']=np.mean(CBF[WM_ROI_75])
                    df.loc[idx]['CBF_GM90']=np.mean(CBF[GM_ROI_90])
                    df.loc[idx]['CBF_WM90']=np.mean(CBF[WM_ROI_90])
                    df.loc[idx]['rel_CBF']=CBF_avg/CBF_global
                    df.loc[idx]['rel_CBF_cor_ratio3']=CBF_avg_pvecor_ratio3/CBF_global_PVEcor_ratio3
                    df.loc[idx]['rel_CBF_cor_ratio_meas']=CBF_avg_pvecor/CBF_global_PVEcor
                
                if has_T1:
                    df.loc[idx]['T1_GM75']=np.mean(T1map[GM_ROI_75])
                    df.loc[idx]['T1_WM75']=np.mean(T1map[WM_ROI_75])
                    df.loc[idx]['T1_GM90']=np.mean(T1map[GM_ROI_90])
                    df.loc[idx]['T1_WM90']=np.mean(T1map[WM_ROI_90])                   
                    df.loc[idx]['rel_GM_T1_75']=df.loc[idx]['T1_GM75']/T1_GM75
                    df.loc[idx]['rel_WM_T1_75']=df.loc[idx]['T1_WM75']/T1_WM75
                    df.loc[idx]['rel_GM_T1_90']=df.loc[idx]['T1_GM90']/T1_GM90
                    df.loc[idx]['rel_WM_T1_90']=df.loc[idx]['T1_WM90']/T1_WM90
                    
                if has_DeepGM:
                    df.loc[idx]['N_DeepGM75'] = N_DeepGM75
                    df.loc[idx]['N_DeepGM90'] = N_DeepGM90
                    #df.loc[idx]['CBF_DeepGM_ratio_90']=
                    df.loc[idx]['fDeepGM'] = DeepGM_fraction               
                    if has_CBF:
                        if N_DeepGM75>0:
                            df.loc[idx]['CBF_DeepGM75'] = np.mean(CBF[DeepGM_ROI_75])
                        else:
                            df.loc[idx]['CBF_DeepGM75'] = np.nan
                        if N_DeepGM90>0:
                            df.loc[idx]['CBF_DeepGM90'] = np.mean(CBF[DeepGM_ROI_90])
                        else:
                            df.loc[idx]['CBF_DeepGM90'] = np.nan

        df.to_csv(dataframe_csvfile)
    return dataframe_csvfile

def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    #from cmind.globals import cmind_example_output_dir
    output_dir='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/test_ROI_avg'
    #GMpve='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/Physio/PVE_to_subject/GMpve_Inv2lowres.nii.gz'
    GMpve='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/Physio/PVE_to_subject6/GMpve_Inv2lowres.nii.gz'
    DeepGMpve='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/Physio/PVE_to_subject6/DeepGMpve_Inv2lowres.nii.gz'
    WMpve='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/Physio/PVE_to_subject6/WMpve_Inv2lowres.nii.gz'
    CSFpve='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/Physio/PVE_to_subject6/CSFpve_Inv2lowres.nii.gz'
    atlas_vol='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/Physio/AAL_to_subject/AAL_Inv2lowres.nii.gz'
    CBF_quant_vol='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/Physio/Physio/BaselineCBF/cmind_baselineCBF_quantitative/CBF_Wang2002_alphaconst.nii.gz'
    T1_quant_vol='/media/Data2/CMIND_NIPYPE/IRC04H_17M003_P_1/Physio/Physio/BaselineCBF/cmind_baselineCBF_coregisterT1/T1_map_reg2CBF.nii.gz'
    ROI_labels_csv='/media/Data1/src_repositories/my_git/cmind-py-private/cmind/misc/AAL_labels.csv'

    generate_figures = True
    ForceUpdate = True
    verbose = True
    logger=cmind_logger(log_level_console='INFO',logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
    
    if verbose:
        func=cmind_timer(cmind_atlas_ROI_averages,logger=logger)
    else:
        func=cmind_atlas_ROI_averages

    func(output_dir=output_dir, 
         GMpve=GMpve, 
         WMpve=WMpve, 
         CSFpve=CSFpve, 
         atlas_vol=atlas_vol, 
         CBF_quant_vol=CBF_quant_vol, 
         T1_quant_vol=T1_quant_vol,
         DeepGMpve = DeepGMpve, 
         ROI_labels_csv=ROI_labels_csv, 
         generate_figures=generate_figures, 
         ForceUpdate=ForceUpdate, 
         verbose=verbose, 
         logger=logger)
    
        
if __name__=='__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Calculate average T1 values for each tissue type (GM, WM, CSF)", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("--GMpve", help="GM partial volume estimate from structural processing")
        parser.add_argument("--WMpve", help="WM partial volume estimate from structural processing")
        parser.add_argument("--CSFpve", help="CSF partial volume estimate from structural processing")
        parser.add_argument("--atlas_vol", type=str, default='none', help="reference atlas in subject space")
        parser.add_argument("--CBF_quant_vol", type=str, default='none', help="quantitative ASL volume")
        parser.add_argument("--T1_quant_vol", type=str, default='none', help="quantitative T1 map volume")
        parser.add_argument("--DeepGMpve", required=False, type=str, default='none', help="DeepGM partial volume estimate from structural processing")
        parser.add_argument("--ROI_labels_csv", required=False, type=str, default='none', help=".csv file with integers in first column and corresponding labels in 2nd column.  ")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
          
        args=parser.parse_args()
        
        output_dir=args.output_dir
        GMpve=args.GMpve
        WMpve=args.WMpve
        CSFpve=args.CSFpve
        atlas_vol=args.atlas_vol
        CBF_quant_vol=str2none(args.CBF_quant_vol)
        T1_quant_vol=str2none(args.T1_quant_vol)
        DeepGMpve=str2none(args.DeepGMpve)
        ROI_labels_csv=str2none(args.ROI_labels_csv)
        generate_figures=args.generate_figures
        ForceUpdate=args.ForceUpdate
        verbose=args.verbose
        logger=args.logger
        
        if verbose:
            cmind_atlas_ROI_averages=cmind_timer(cmind_atlas_ROI_averages,logger=logger)


        cmind_atlas_ROI_averages(output_dir=output_dir, 
             GMpve=GMpve, 
             WMpve=WMpve, 
             CSFpve=CSFpve, 
             atlas_vol=atlas_vol, 
             CBF_quant_vol=CBF_quant_vol, 
             T1_quant_vol=T1_quant_vol, 
             DeepGMpve = DeepGMpve, 
             ROI_labels_csv=ROI_labels_csv, 
             generate_figures=generate_figures, 
             ForceUpdate=ForceUpdate, 
             verbose=verbose, 
             logger=logger)