#!/bin/bash
echo "CMD: $*"

usage() 
{
echo "Recommended Usage:      
	 cmind_apply_normalization.sh -o output_dir -rsf reg_struct -in* all_inputs -out output_name -aff LRtoHR_affine -a ANTS_path [-w LRtoHR_warp -reg reg_flag -interp interp_type -gf generate_figures -u ForceUpdate ]

Required parameters:
    -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
    -rsf REG_STRUCT, --reg_struct REG_STRUCT
                        filename of the .csv file containing the registration
                        dictionary
    -in* INPUT_NAME
                        input_name - image filename for the fixed image (registration target)
 
Requires one and only one of the following:    
  -aff LRTOHR_AFFINE, --LRtoHR_affine LRTOHR_AFFINE
                        affine transform from functional to structural
                        space.if LRtoHR_affine='', it is assumed the image is
                        already in structural space
  -w LRTOHR_WARP, --LRtoHR_warp LRTOHR_WARP
                        fieldmap warp correction for functional space image,
                        if LRtoHR_warp='' or None, no warping is applied
                  OR
  -regdir DIRECTORY, --regBBR_dir DIRECTORY
                        directory containing the affine or warp transforms
  
Optional parameters:
  -a ANTS_PATH, -ants ANTS_PATH, --ANTS_path ANTS_PATH
                        path to ANTs binaries
  -out OUTPUT_NAME, --output_name OUTPUT_NAME
                        image filename for the output.  by default will be a modified version of the input name
  -reg REG_FLAG, --reg_flag REG_FLAG
                        3 character string such as 101 controlling which
                        registration to perform. first digit is for FLIRT;
                        second digit is for FNIRT (not currently implemented);
                        third digit is for ANTs, SyN; e.g. 101 would run FLIRT
                        & ANTs registrations. can also specify the binary
                        "101" as the integer 5, etc.
  --Norm_Target NORM_TARGET
                        Normalization target: {'MNI','StudyTemplate'}
  -interp INTERP_TYPE, --interp_type INTERP_TYPE
                        {'spline','bspline'} call spline based interpolation;
                        {'lin', 'linear','trilinear'} call trilinear
                        interpolation; {'nearestneighbour','nearest','nn'}
                        call nearest neighbor interpolation; interpolation
                        type to use
  --generate_coverage GENERATE_COVERAGE
                        if true, generate binary masks of the slice coverage.
                        can be an array of values for each input image
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate overlay images summarizing the
                        registration
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        filename to log to
  -np
                        number of threads
"
}



if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi


REQUIRED_ARGS=8
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi


if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

reg_flag=5 #[1 0 1] = 5 after conversion to decimal
interp_type="spline"
generate_figures=true
ForceUpdate=true
ANTS_path="default"
output_name="default"
LRtoHR_warp="None"
LRtoHR_affine="None"

all_inputs=""
verbose_string=""
all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
  	-np )  #extra argument, only present in shell script
  	    shift
  	    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1
  	    ;;
  	-regdir )  #extra argument, only present in shell script
  	    shift
  	    regBBR_dir=$1
  	    ;;
  	-u|--ForceUpdate )
  	    shift
  	    ForceUpdate=$1
  	    ;;	
  	-reg|--reg_flag )
  	    shift
  	    reg_flag=$1
  	    ;;	
  	-a|-ants|--ANTS_path )
  	    shift
  	    ANTS_path=$1
  	    ;;	
  	--generate_coverage )
        shift
        generate_coverage=$1
        ;;
    --Norm_Target )
        shift
        Norm_Target=$1
        ;;
    -gf|--generate_figures )
  	    shift
  	    generate_figures=$1
  	    ;;	
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -rsf|--reg_struct )
        shift
        reg_struct=$1
        ;;
    -out|--output_name )
        shift
        output_name=$1
        ;;
    -interp|--interp_type ) #MUST BE ABOVE in* case listed below
  	    shift
  	    interp_type=$1
  	    ;;	
  	-in* )
              shift
              if [ ! -f $1 ]; then
                echo "ERROR: input_name: $1 doesn't exist"
                exit 1
              fi
              if [ "$all_inputs" = "" ]; then
                all_inputs=$1
              else
                all_inputs="${all_inputs},$1"
              fi
              ;;
          -out )
  	    shift
  	    output_name=$1
  	    ;;	
  	-aff|--LRtoHR_affine )
        shift
        LRtoHR_affine=$1
        ;;
    -w|--LRtoHR_warp )
        shift
        LRtoHR_warp=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
      
    esac
    shift
done

if [ ! -z $verbose_string ]; then
  debug=true
else
  debug=false
fi


if $debug 
then
	echo "LRtoHR_warp=$LRtoHR_warp"
fi

#Check that the ANTs executable is on the path
if [ $ANTS_path = "default" ]; then
    command -v antsApplyTransforms >/dev/null 2>&1 || { echo "The ANTs tool antsRegistration is required, but was not found. Aborting." >&2; exit 1; }
    ANTS_path=`command -v antsApplyTransforms`
    ANTS_path=`dirname $ANTS_path`
fi


if [ ! -d ${output_dir} ]; then
    mkdir -p "${output_dir}"
fi 

if [ ! -f $reg_struct ]; then
    echo "ERROR: reg_struct $reg_struct doesn't exist"
    exit 1
fi 

echo "regBBR_dir: $regBBR_dir"
if [ "$regBBR_dir" != "" ] && [ -d $regBBR_dir ]
then
  if [ -z "${LRtoHR_affine+xxx}" ] || [ ! -f $LRtoHR_affine ]
  then
	  LRtoHR_affine=${regBBR_dir}/lowres2highres.mat
  	if [ ! -f $LRtoHR_affine ]  #pass "None" if this file doesn't exist!
  	then
  	   LRtoHR_affine="None"
  	fi
  fi
   
  if [ -z "${LRtoHR_warp+xxx}" ] || [ ! -f $LRtoHR_warp ]
  then
	  LRtoHR_warp=${regBBR_dir}/lowres2highres_warp.nii.gz
  	if [ ! -f $LRtoHR_warp ]  #pass "None" if this file doesn't exist!
  	then
  	   LRtoHR_warp="None"
  	fi
  fi
fi

echo "LRtoHR_affine: $LRtoHR_affine"
if [ "$LRtoHR_affine" != "None" ] && [ ! -f $LRtoHR_affine ]; then
    echo "ERROR: LRtoHR_affine: $LRtoHR_affine doesn't exist"
    exit 1
fi 


#existence checks for all required conditional arguments
if [ -z "$output_dir" ]; then
   echo "missing required argument: output_dir"
fi
if [ -z "$reg_struct" ]; then
   echo "missing required argument: reg_struct"
fi
if [ -z "$input_name" ]; then
   echo "missing required argument: input_name"
fi
if [ -z "$output_name" ]; then
   echo "missing required argument: output_name"
fi


input_name=$all_inputs
#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir reg_struct input_name output_name LRtoHR_affine LRtoHR_warp ANTS_path reg_flag Norm_Target interp_type generate_coverage generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_apply_normalization.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_apply_normalization.py ${verbose_string} ${all_optional_args}
echo "Applying Previously Computed Normalization via cmind_apply_normalization.py:  Elapsed time: $(timer $t)"



# t=$(timer)

# #make slice coverage image?
# generate_coverage=true

# if $debug
# then
#     python ${cmind_python_dir}/cmind_apply_normalization.py ${output_dir} ${reg_struct} ${all_inputs} ${output_name} --LRtoHR_affine ${LRtoHR_affine} --LRtoHR_warp $LRtoHR_warp --ANTS_path ${ANTS_path} --reg_flag ${reg_flag} --interp_type ${interp_type} --generate_figures ${generate_figures} --generate_coverage ${generate_coverage} --ForceUpdate $ForceUpdate --verbose
# else
#     python ${cmind_python_dir}/cmind_apply_normalization.py ${output_dir} ${reg_struct} ${all_inputs} ${output_name} --LRtoHR_affine ${LRtoHR_affine} --LRtoHR_warp $LRtoHR_warp --ANTS_path ${ANTS_path} --reg_flag ${reg_flag} --interp_type ${interp_type} --generate_figures ${generate_figures} --generate_coverage ${generate_coverage} --ForceUpdate $ForceUpdate
# fi


inputArray=( ${all_inputs//,/ } )
for name in "${inputArray[@]}"
do
    name=`basename $name`
    file="${output_dir}/wANTS_$name"
    if [ ! -f $file ]; then
       echo "ERROR: Output file $file doesn't exist."
       exit 1;
    fi 
    file="${output_dir}/wlin_$name"
    if [ ! -f $file ]; then
       echo "ERROR: Output file $file doesn't exist."
       exit 1;
    fi 
done

echo "Finished cmind_apply_normalization"
