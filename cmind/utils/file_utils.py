"""
File Handling Utilities
"""

from __future__ import division, print_function, absolute_import
import os
import shutil
import csv
import glob
import fnmatch
import warnings

from cmind.utils.utils import input2bool, is_string_like


def file_update_bool(input_file, output_file, NoCheck=False, remove_output_if_true=False):
    """ return a boolean that determines if output_file should be updated based
    on the relative date as compared to input_file

        if input_file doesn't exist, returns False
        if input_file exists, but output_file doesn't return True
        if both exist, but input_file is newer return True
        if both exist, but output_fiel is newer return False
        if output_file is a directory return True

    Parameters
    ----------
    input_file : str
    output_file : str
    NoCheck : bool, optional
    remove_output_if_true : bool, optional

    """
    #from stat import ST_MODE S_ISDIR S_ISLNK S_ISREG

    if NoCheck: #just assume the input exists
        input_bool=True
    else:
        if os.path.islink(input_file):
            input_bool=os.path.isfile(os.readlink(input_file))
        else:
            input_bool=os.path.isfile(input_file)

    output_exists=False
    if not input_bool:
        cond=False
    elif os.path.isdir(output_file) or (not os.path.isfile(output_file)):
        cond=True
    else:
        output_exists=True
        if os.path.getmtime(input_file) > os.path.getmtime(output_file):
            cond=True
        else:
            cond=False

    if cond and output_exists and remove_output_if_true:
        os.remove(output_file)

    return cond


def export_tarfile(filename, filelist, arcnames=None, dereference=True):
    """ utility for sending a list of files/directories to a .tar archive

    Parameters
    ----------
    filename : str
        name of the tar file to create.  It will be overwritten if it already
        exists.  If the filename ends in .gz or .bz2, gzip or bz2 compression
        will be used, respectively.
    filelist : list of str
        the list of files to be added to the archive.  This can also include
        directories.
    arcnames : list of str, optional
        optional list of the filenames to use inside the archive for each file
        in filelist
    dereference : bool, optional
        if True, replace any links with a copy of the linked file.

    Returns
    -------
    filename : str
        the file that was created

    """
    import tarfile
    if os.path.isfile(filename):
        print("Warning: tar file %s already exists... overwriting" % filename)

    #choose tar archive compression type based on file extension
    tar_ext=os.path.splitext(filename)[1]
    if tar_ext=='.gz':  #gzipped tar
        tar = tarfile.open(filename, "w:gz", dereference=dereference)
    elif tar_ext=='.bz2':
        tar = tarfile.open(filename, "w:bz2", dereference=dereference)
    elif tar_ext=='.tar':
        tar = tarfile.open(filename, "w", dereference=dereference)
    elif tar_ext=='': # if no extension, append .tar
        filename=filename + '.tar'
        tar = tarfile.open(filename, "w", dereference=dereference)

    #allow a single string instead of list for filelist, arcnames
    if is_string_like(filelist):
        filelist=[filelist,]
    if is_string_like(arcnames): #allow a single string instead of list
        arcnames=[arcnames,]

    if arcnames==None:
        pass
    else:
        if len(arcnames)!=len(filelist):
            # check that length of lists are compatible
            raise Exception("arcnames list length must match filelist length")

    for idx,item in enumerate(filelist):
        if os.path.isdir(item):
            rm_broken_links(item)
        if arcnames!=None:
            tar.add(item, arcname=arcnames[idx])
        else:
            tar.add(item)
    tar.close()
    return filename


def split_multiple_ext(input_name):
    """function to recursively remove multiple file extensions from a filename

    Parameters
    ----------
    input_name : str
        filename to process

    Returns
    -------
    input_name_base : str
        base of input_name
    ext : str
        extension(s) of input_name

    Notes
    -----
    filename.nii.gz ->  (filename, .nii.gz)

    """
    ext=""
    input_name_base=input_name
    while os.path.splitext(input_name_base)[1]:
        tmp=os.path.splitext(input_name_base)
        input_name_base=tmp[0];
        ext=tmp[1]+ext
    return (input_name_base,ext)


def multiglob_list(strlist,basedir=None):
    """concatenate results of multiple glob operations into a single list

    Parameters
    ----------
    strlist : list
        list of strings to glob
    basedir : str, optional
        if provided, prepend basedir to the filenames in `filelist`
    Returns
    -------
    filelist : str
        list of files found
    """

    filelist=[]
    for search_str in strlist:
        matches=glob.glob(search_str);
        if matches:
            filelist = filelist + matches
    if basedir:
        for n,f in enumerate(filelist):
            filelist[n]=os.path.join(basedir,f)
    filelist=list(set(filelist))  #remove any duplicates in the list.  Warning:  may cause order of list to be modified
    return filelist

def imexist(image_filename,im_subset='all'):
    """Extension insensitive image match.  optional second argument controls which file extensions are searched for

    Parameters
    ----------
    image_filename : str
        image filename to check
    im_subset : list or {'all','nii','nifti','nifti-1','nrrd','img','analyze','png','2dreport','2dimg'}, optional
        default is 'all', which tries to match the following extensions: ['.nii.gz','.nii','.img','.hdr','.nrrd','.nhdr']

    Returns
    -------
    exists : bool
        true if an image with the same basename as image_filename exists
        (specify extensions to search via `im_subset`)
    true_fname : str
        full path to the image file (with extension)
    im_root : str
        basename of the image file (without extension)
    im_path : str
        path containing the image file
    im_ext : str
        file extension (.e.g. .nii.gz)

    Notes
    -----
    possible string inputs to `im_subset` and their corresponding file extensions are:
        - all      [.nii.gz,.nii,.img,.hdr,.nrrd,.nhdr]  (default)
        - nifti-1  [.nii.gz,.nii]
        - nifti    [.nii.gz,.nii]
        - nii      [.nii.gz,.nii]
        - nrrd     [.nrrd,.nhdr]
        - img      [.img,.hdr]
        - analyze  [.img,.hdr]
        - png      [.png]
        - 2dreport [.gif,.png]
        - 2dimg    [.gif,.jpg,.tif,.tiff,.eps,.png,.bmp]

    alternatively, `im_subset` can be a list of the file extensions to consider


    """


    if not image_filename:
        exists=False; true_fname=""; im_root=""; im_path=""; im_ext="";
        return (exists, true_fname, im_root, im_path, im_ext)

    if not is_string_like(image_filename): #modified to allow unicode as well in python 2.x.  isinstance(image_filename,str):
        raise ValueError('image_filename must be a string (or None)')

    root,im_ext = os.path.splitext(image_filename)
    im_path = os.path.dirname(root);
    if im_ext in ['.gz']:
        root,ext2 = os.path.splitext(root)
        im_ext = ext2 + im_ext
#    root,im_ext = split_multiple_ext(image_filename)

    im_root = os.path.basename(root);

    if not im_path:  #make sure im_path and true_fname contain the full path, not a relative one
        im_path=os.getcwd();
    else:
        if not os.path.isdir(im_path): #specified path does not exist
            exists=False; true_fname=""; im_root=""; im_path=""; im_ext="";
            return (exists, true_fname, im_root, im_path, im_ext)

    if isinstance(im_subset,str):
        im_subset_l = im_subset.lower()
        if im_subset_l == 'all' or im_subset_l == 'any':
            img_suffixes=['.nii.gz','.nii','.img','.hdr','.nrrd','.nhdr'];
        elif im_subset_l == 'nii' or im_subset_l == 'nifti' or im_subset_l == 'nifti-1':
            img_suffixes=['.nii.gz','.nii'];
        elif im_subset_l == 'nii' or im_subset_l == 'nifti' or im_subset_l == 'nifti-1':
            img_suffixes=['.nii.gz','.nii'];
        elif im_subset_l == 'img' or im_subset_l == 'analyze':
            img_suffixes=['.img','.hdr'];
        elif im_subset_l == 'nrrd':
            img_suffixes=['.nrrd','.nhdr'];
        elif im_subset_l == '2dimg':
            img_suffixes=['.gif','.jpg','.tif','.tiff','.eps','.png','.bmp'];
        elif im_subset_l == '2dreport':
            img_suffixes=['.gif','.png'];
        elif im_subset_l == 'png':
            img_suffixes=['.png'];
        else:
            raise Exception("Error: invalid im_subset: {}".format(im_subset))
            #exists=False; true_fname=""; im_root=""; im_path=""; im_ext="";
            #return (exists, true_fname, im_root, im_path, im_ext)
    elif isinstance(im_subset,list):
        img_suffixes=im_subset
    else:
        raise Exception("Error: invalid im_subset")

    exists=0;
    true_fname="";
    for im_ext in img_suffixes:
        fname_tmp=os.path.join(im_path,im_root+im_ext);
        exists = os.path.isfile(fname_tmp);
        if(exists):
            true_fname=fname_tmp;
            break;

    if not true_fname:  #if not found, set extension, path, basename all to empty string
        im_ext="";
        im_root="";
        im_path="";
        #print "Did not find `", image_filename, "`"
    else:
        #print "Found `", true_fname, "`"
        true_fname=os.path.abspath(true_fname)

    return (exists, true_fname, im_root, im_path, im_ext)

def filecheck_bool(input_file,output_file,NoCheck=False):
    """Returns True if input_file exists, but output_file doesn't exist
      or if input_file is newer than output_file

    Parameters
    ----------
    input_file : str
        input filename
    output_file : str
        output filename
    NoCheck : bool, optional
        if True, skip check of input file existence (default = False)

    Returns
    -------
    filebool : bool
        True if `input_file` exists and is newer than `output_file`

    Notes
    -----
    Intended for as a check to be used in cases where `output_file` should
    only be updated if it does not exist or if it has an older timestamp than
    the `input_file`.


    """

    NoCheck=input2bool(NoCheck)
    if ((not NoCheck) and (not os.path.isfile(input_file))):
        filebool=False;
        print('Input file, %s, doesn''t exist' % (input_file))
    elif ((not os.path.isfile(output_file)) or os.path.isdir(output_file)):
        filebool=True;
    else:
        time1=os.path.getmtime(input_file);
        time2=os.path.getmtime(output_file);
        if(time1>time2): #if input file has changed, then update the output file
            filebool=True;
        else:
            filebool=False;
    return filebool

def csv2dict(csvfile,strip_quotes=False):
    """write out a dictionary containing strings or lists of strings into a
    .csv file. The first column of the .csv contains the key names. The second
    contains the values.  Will ignore any comments (denoted by #) or empty lines

    Parameters
    ----------
    csvfile : str
        filename of the .csv file to read in
    strip_quotes : str
        if True, strip any double quotes from the strings before storing in the
        dictionary

    Returns
    -------
    output_dict : dict
        dictionary where the keys are the first column of the .csv and the
        values are the second column of the .csv

    Notes
    -----
    Reads from a a simple 2 column CSV file where the keys are stored in the
    first column and the values in the second.

    Is not a strict .csv reader.  Will skip any empty lines or lines without
    commas.  Will also strip any comments denoted with # from the end of lines


    """
    output_dict={}
    csv_lines=open(csvfile,'r').readlines()
    #Strip out comment lines and empty lines
    csv_lines=[item for item in csv_lines if (item.lstrip() and item.lstrip()[0]!='#')]

    #Check the number of columns in the .csv
    ncols = len(csv.reader(csv_lines).next())
    if ncols<2:
        raise ValueError("Expected 2 columns in CSV, found {}".format(ncols))
    elif ncols>2:
        warnings.warn("Expected 2 columns in CSV, found {}.  Ignoring additional columns...".format(ncols))

    #for key, val in csv.reader(open(csvfile,'r')):
    for line in csv.reader(csv_lines):
        key = line[0]
        val = line[1]
        if val.find(',')!=-1:  #special case: split entries with commas into a list   e.g.  "e1,e2,e3" -> ["e1","e2","e3"]
            val=val.split(',')
            val[-1]=val[-1].split('#')[0].rstrip() #remove any trailing comments and any
            if strip_quotes:
                val = [item.replace('"','') for item in val]

        else:
            val=val.strip() #make sure no newline characters
            val=val.split('#')[0] #remove any trailing comments
            if strip_quotes:
                val=val.replace('"','')
        output_dict[key] = val
    return (output_dict)

def dict2csv(input_dict,csvfile):
    """write a simple dictionary into a .csv file containing strings or
    lists of strings.  The first column of the .csv contains the key names.
    The second contains the values. If values are multiple comma-separated
    strings contained in "", a list of strings will be stored

    Parameters
    ----------
    input_dict : dict
        dictionary to store as .csv
    csvfile : string
        output filename

    """
    if os.path.isfile(csvfile):
        print("Warning: overwriting file: %s" % (csvfile))
    w = csv.writer(open(csvfile, "w"))
    for key, val in list(input_dict.items()):
        if isinstance(val,(list,tuple)): #CSV entries containing commas must be enclosed in ""
            #val='"' + ','.join(val) + '"'  #csv.writerow will add the "'s for us.  don't do it again here!
            val=','.join(val)
        w.writerow([key, val])


def remove_file(f,warn=False):  #check if a file exists before calling os.remove
    """simple wrapper to call os.remove() only if the file exists

    Parameters
    ----------
    f : str
        file to be removed

    """
    if os.path.isfile(f):
        os.remove(f)
    elif warn:
        warnings.warn('unable to remove non-existant file, {}'.format(f))


def rm_broken_links(basedir='.', use_shell=False):
    """ recursively remove any broken links within basedir via find/exec

    Parameters
    ----------
    basedir : str
        remove all broken links within basedir (recursively)
    use_shell : str, optional
        if True, uses the linux find command.  otherwise uses python builtins
    """
    #-type l   tells find to only return links
    #test ! -e {} \;   will return only those that are broken
    #-delete  will delete the broken links

    if use_shell:
        import subprocess
        subprocess.check_output("find %s -type l -exec test ! -e {} \; -delete" % (basedir), shell=True)
    else:  #TODO: test this version and then switch to it
        for (root, subFolders, filenames) in os.walk(basedir):  #recursively walk the full path
            if '.svn' in subFolders: #skip any .svn subfolders that may exist
                subFolders.remove('.svn')
            for f in filenames:
                f=os.path.join(root,f) #full path to the file
                if os.path.islink(f):  #is it a link?
                    if not os.path.isfile(os.readlink(f)):  #is the link broken?
                        os.remove(f)

#cmind_copy_all('/home/lee8rx/src_repositories/svn_stuff/cmind-matlab/trunk/lib/Python/cmind/cmind/data/P/BaselineCBF','/home/lee8rx/src_repositories/svn_stuff/cmind-matlab/trunk/lib/Python/cmind/doc/img',match_str='*.png',use_symlinks=False,overwrite_existing=False,prepend_str='IRC06M008_P_1_',recursive=False)

def cmind_copy_all(source_dir,dest_dir,match_str='*',use_symlinks=False, overwrite_existing=True, prepend_str='', recursive=True):
    """recursively copy (or symlink) all files in source_dir to dest_dir

    Parameters
    ----------
    source_dir : str
        source directory name
    dest_dir : str
        destination directory name
    match_str : str, optional
        wildcard string specifying file match
    use_symlinks : bool, optional
        make symbolic links instead of copies
    overwrite_existing : bool, optional
        if True, any already existing destination files will be removed
        before the copy is attempted
    prepend_str : str, optional
        if specified, prepend this string to all destination filenames
    recursive : bool, optional
        if False, do not copy from subdirectories of source_dir

    Returns
    -------
    all_destfile : list of str
        list of the files that were copied

    """

    source_dir=source_dir.rstrip(os.path.sep) #must strip any trailing path separator

    if not os.path.isdir(source_dir):
        raise ValueError("source_dir must be a directory")

    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)

    if not os.path.isdir(dest_dir):
        raise ValueError("dest_dir must be a directory")

    #recursively search for all files in dest_dir matchin match_str
    if recursive:
        filelist = [os.path.join(dirpath, f)
                    for dirpath, dirnames, files in os.walk(source_dir)
                    for f in fnmatch.filter(files, match_str)]
        #filelist = [f[0] for f in [glob.glob(os.path.join(d[0], match_str)) for d in os.walk(source_dir)] if f]
    else:
        filelist=glob.glob(os.path.join(source_dir,match_str))

    all_destfile=[]
    for f in filelist:        #log_cmd('ln -s -f -t "%s" "%s"/*' % (output_dir,prepdir), verbose=verbose, logger=module_logger);
        source_dir_full=os.path.dirname(f)
        subdir_name=source_dir_full.replace(source_dir,'').lstrip(os.path.sep)
        dest_dir_full=os.path.join(dest_dir,subdir_name)
        f_base=os.path.basename(f)
        dest_file=os.path.join(dest_dir_full,prepend_str+f_base)
        all_destfile.append(dest_file)
        if not os.path.isdir(dest_dir_full): #make destination directory if it doesn't exist
            os.makedirs(dest_dir_full)

        #if desired, remove the destination file first
        if overwrite_existing:
            if os.path.islink(dest_file):  #remove the link, not the file it points too!  this will remove broken links too
                os.unlink(dest_file)
            elif os.path.exists(dest_file):
                os.remove(dest_file)

        if use_symlinks:
            #print "f={}, dest_file={}, overwrite_existing={}, exists={}".format(f,dest_file,overwrite_existing,os.path.exists(dest_file))
            if overwrite_existing:
                os.symlink(f,dest_file)
            else:
                if not os.path.exists(dest_file):
                    os.symlink(f,dest_file)
                else:
                    warnings.warn('destination symlink, {}, already existed.  no symlink was made'.format(dest_file))
        else:
            if overwrite_existing:
                shutil.copy(f,dest_file)
            else:
                if not os.path.exists(dest_file):
                    shutil.copy(f,dest_file)
                else:
                    warnings.warn('destination file, {}, already existed, so no copy was performed'.format(dest_file))

                    split_multiple_ext
    return all_destfile

def which(program):
    """ Search for a binary program on the system path
    this code from:  http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python

    Parameters
    ----------
    program : str
        name of the executable to search for on the path

    Returns
    -------
    path : str
        path to the binary file corresponding to program

    """
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return os.path.realpath(program)  #resolve symbolic links
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return os.path.realpath(exe_file)  #resolve symbolic links

    return None
