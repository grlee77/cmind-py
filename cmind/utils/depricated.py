from __future__ import division, print_function, absolute_import
import os
import subprocess

import numpy as np

from sys import version_info

if version_info < (3,3):
    from funcsigs import signature
else:
    from inspect import signature


        
#NOTE:  just use tempfile.mkdtemp() instead of createRandomDirectory()
def createRandomDirectory( basedir=None ):
    """create a random directory in the basedir directory
    
    Parameters
    ----------     
    basedir : str, optional
        create a random subdirectory within `basedir`
    
    Returns
    -------   
    directory : str
        the name of the directory that was created
        
    Notes
    -----
    requies that md5sum or md5 is on the system path
    
    If basedir=None, will check if any of the following locations exist and use
    that as a default.  The first one found will be used:
        
    /scratch/LONI/tmp/
    /tmp/
    /temp/
    os.getcwd()
    """
    
    import socket, time
    
    if not basedir:  #GRL:  attempt to choose a suitable default basedir 
        basedir='/scratch/LONI/tmp/';
        if not os.path.isdir(basedir):
            basedir='/tmp/';
        if not os.path.isdir(basedir):
            basedir='/temp/';
        if not os.path.isdir(basedir):
            basedir=os.getcwd() #make the directory within the current path
        if not os.path.isdir(basedir):
            print 'Unable to find a suitable default basedir.  Please manually specify it.'
            raise #Exception('Fail')
        
    if not (basedir[-1]==os.sep):
        basedir=basedir + os.sep
    
    directory = basedir;
    messid='path_exists'
    while messid=='path_exists':
        # generate unique string from hostname and time
        hostname = socket.gethostname()
        try:   #will raise exception if md5sum command doesn't exist
            hash = cmind_shell_cmd('echo "%s%f" | md5sum | cut -d " " -f 1' % (hostname, time.time()))
        except: #on some OsX, use "md5" instead of "md5sum"
            try:
                hash = cmind_shell_cmd('echo "%s%f" | md5 | cut -d " " -f 1' % (hostname, time.time()))
            except:
                raise
        # generate a directory name from unique hash
        directory = os.path.abspath(os.path.join(basedir, hash.strip()));
        if os.path.isdir(directory):  #if directory already exists, try again
            messid='path_exists'
        else:
            messid='path_is_new'
    os.makedirs(directory)
    return directory
    

#Note: masker function not needed.  x[mask] will have the same result
def masker(x,mask):
    """opposite of embed.   in 3d case, if x.shape=[nx ny nz n4], output x.shape=[np n4] where np = sum(mask)
    
    Parameters
    ----------
    x : ndarray
        input to mask
    mask : ndarray
        logical array.  should match shape of x (except for, optionally, the last dimension)
        
    Returns
    -------
    x : ndarray
        masked input
    """
    
    x=x.reshape((np.prod(mask.shape), -1)) #, order='F')
    x = x[mask.ravel(),:]
   
    return x    
    

#Note: replaced by log_cmd()   
def cmind_shell_cmd(shell_cmd,verbose=False,logfile=None,print_stdout=False,print_cmd_header=False):
    """ wrapper around stdout=subprocess.check_output(shell_cmd, shell=True)
    
    Parameters
    ----------
    shell_cmd : str
        string representing the shell command to be run
    verbose : boolean, optional
        if True, print the shell_cmd to standard out
    logfile : str or file handle
        if logfile: print the shell_cmd to the logfile as well
    print_stdout : boolean, optional
        if True, also print out the standard output of the shell commands
    print_cmd_header : boolean, optional
        if True, print "Executing shell command" header 
        
    Returns
    -------
    stdout : str
        Standard output of the command
        
    """
    
    if print_cmd_header:
        header_str="Executing shell command:\n"
    else:
        header_str=""
    if verbose:
        if logfile:
            if isinstance(logfile,str):
                try:
                    print 'Logging to:', logfile
                    logfile=open(logfile,'a')
                    logfile.write("%s%s\n\n" % (header_str,shell_cmd))
                except:
                    print "WARNING: failed to print to requested log file"
        print "%s%s\n" % (header_str,shell_cmd)
    stdout=subprocess.check_output(shell_cmd, shell=True)
    if print_stdout:
        print stdout
    return stdout    


def timed_v0(func):
    """decorator to print elapsed time to run a function
       OLD TEST VERSION USING
    """
    @functools.wraps(func)
    def wrapper(*args,**kwargs):
        t1 = time.time()
        res = func(*args,**kwargs)
        t2 = time.time()
        print 'func:%r took: %2.4f sec' % (func.__name__, t2-t1)
        return res
    return wrapper



def timed(func):
    """decorator to print elapsed time to run a function
       OLD TEST VERSION USING kwargs.pop
    """
    @functools.wraps(func)
    def wrapper(*args,**kwargs):
#        verbose=True #print the timing by default
#        if kwargs.has_key('verbose'):
#            verbose=kwargs['verbose']
        verbose=kwargs.pop('timer',True)
        logger=kwargs.pop('logger',None)
        t1 = time.time()
        res = func(*args,**kwargs)
        if verbose:
            t2 = time.time()
            #time_str='func:%r took: %2.4f sec' % (func.__name__, t2-t1)
            time_str='func:{} took {} seconds'.format(func.__name__, t2-t1)
            if logger:
                logger.info(time_str)
            else:
                print time_str
        return res
    return wrapper

    #http://stackoverflow.com/questions/10724854/how-to-do-a-conditional-decorator-in-python-2-6
#class conditional_decorator(object):
#    """@conditional_decorator(dec,condition) 
#    """
#    def __init__(self, dec, condition):
#        self.decorator = dec
#        self.condition = condition
#
#    def __call__(self, func):
#       if not self.condition:
#           # Return the function unchanged, not decorated.
#           return func
#       return self.decorator(func)   

def conditional_decorator(dec,cond):
    """decorate a function with dec, depending on truth of cond 
    @conditional_decorator(dec,cond)
    """
    @functools.wraps(dec)
    def wrapper(func):
        if not cond:
            # Return the function unchanged, not decorated.
            return func
        return dec(func) 
    return wrapper          
                 
                                 
#TODO: the signature function below requires is either inspect.signature from Python 3.3+ or the funcsigs.signature from the funcsigs package in Python 2.6, 2.7 or 3.2
def make_verbose(f):
    """verbose wrapper for a function
    """
    @functools.wraps(f)
    def wrapper(*args):
        fname = f.__name__
        fsig = signature(f)
        
        params = '\n     '.join('{}={}'.format(*pair) for pair in zip(fsig.parameters, args))
        print('calling function {} with arguments:\n     {}'.format(fname, params))
        #params = ', '.join('{}={}'.format(*pair) for pair in zip(fsig.parameters, args))
        #print('calling {}({})'.format(fname, params))
        #for paramname, parameter in fsig.parameters.items():
        #    print('  {} param\'s annotation: "{}"'.format(paramname, parameter.annotation))
        ret = f(*args)
        #print('  returning {} with annotation: "{}"'.format(ret, fsig.return_annotation))
        return ret
    return wrapper


if False:
    import numpy as np
    @conditional_decorator(timed_v2,True)
    @conditional_decorator(make_verbose,True) #this one has to be last in the list of decorators in order to get the argument names right
    def tsum3(a):
        return sum(a)
    a=np.arange(1e8,dtype=np.int16)
    tsum3(a,timer=False)
    tsum3(a,timer=True)


      
#@timed
#def tsum2(a):
#    return sum(a)
    



#See the functools @wraps decorator, to preserve the name and docstrings of the original function
#>>> def my_decorator(f):
#...     @wraps(f)
#...     def wrapper(*args, **kwds):
#...         print('Calling decorated function')
#...         return f(*args, **kwds)
#...     return wrapper
#...
#>>> @my_decorator
#... def example():
#...     """Docstring"""
#...     print('Called example function')


#if verbose:
#    cmind_matlab_batch=verbose_decorator(cmind_matlab_batch)




##Logging decorator example from: https://wiki.python.org/moin/PythonDecoratorLibrary
#import functools, logging
#
#log = logging.getLogger(__name__)
#log.setLevel(logging.DEBUG)
#
#class log_with(object):
#    """Logging decorator that allows you to log with a specific logger.
#    """
#    # Customize these messages
#    ENTRY_MESSAGE = 'Entering {}'
#    EXIT_MESSAGE = 'Exiting {}'
#
#    def __init__(self, logger=None):
#        self.logger = logger
#
#    def __call__(self, func):
#        """Returns a wrapper that wraps func. The wrapper will log the entry and 
#           exit points of the function with logging.INFO level.
#        """
#
#        # set logger if it was not set earlier
#        if not self.logger:
#            logging.basicConfig()
#            self.logger = logging.getLogger(func.__module__)
#
#        @functools.wraps(func)
#        def wrapper(*args, **kwds):
#            self.logger.info(self.ENTRY_MESSAGE.format(func.__name__))  # logging level .info(). Set to .debug() if you want to
#            f_result = func(*args, **kwds)
#            self.logger.info(self.EXIT_MESSAGE.format(func.__name__))   # logging level .info(). Set to .debug() if you want to
#            return f_result
#        return wrapper
### Sample use and output:
##if __name__ == '__main__':
##    logging.basicConfig()
##    log = logging.getLogger('custom_log')
##    log.setLevel(logging.DEBUG)
##    log.info('ciao')
##
##    @log_with(log)     # user specified logger
##    def foo():
##        print 'this is foo'
##    foo()
##
##    @log_with()        # using default logger
##    def foo2():
##        print 'this is foo2'
##    foo2()       


 

    
#The example below shows how to use a verbose kwarg from the function within the decorator    
#http://stackoverflow.com/questions/889088/function-decorators
#
"""
Note:  

    verbose = kwargs.pop('verbose', False)

is equivalent to:
    
    verbose = False
    if 'verbose' in kwargs:
        verbose = True
        del kwargs['verbose']
"""
#import time
#
#def timed(f):
#    def dec(*args, **kwargs):
#        verbose = kwargs.pop('verbose', False)
#        t = time.clock()
#
#        ret = f(*args, **kwargs)
#
#        if verbose:
#            print("%s executed in %ds" % (f.__name__, time.clock() - t))
#
#        return ret
#
#    return dec
#
#@timed
#def add(a, b):
#    return a + b
#
#print(add(2, 2, verbose=True))    

#http://www.thumbtack.com/engineering/a-primer-on-python-decorators/
#def memoize(fn):
#    stored_results = {}
#
#    def memoized(*args):
#        try:
#            # try to get the cached result
#            return stored_results[args]
#        except KeyError:
#            # nothing was cached for those args. let's fix that.
#            result = stored_results[args] = fn(*args)
#            return result
#
#    return memoized



def cmind_update_FSF(case_str,design_fsf_dir="",noise_est_dir=""):
    """utility to perform special case updates of a FEAT .fsf file
    
    Parameters
    ----------
    case_str : {'noise_stats'}
        update case to run: 
        'noise_states' update fmri(noise) and fmri(noisear) from values stored in noise_est.txt
    feat_dir : str
        directory containing the design.fsf file to update
        
    Notes
    -----
    currently only updating of fmri(noise) and fmri(noisear) fields of the .fsf
    from values stored in noise_est.txt is supported
    
    """    
    if design_fsf_dir:
        design_fsf_file=os.path.join(design_fsf_dir,'design.fsf')
    else: 
        design_fsf_file='design.fsf'
        
    if noise_est_dir:
        noise_est_file=os.path.join(noise_est_dir,'noise_est.txt')
    else: 
        noise_est_file='noise_est.txt'
        
    if case_str=='noise_stats':
        oldval=log_cmd("cat %s | grep 'fmri(noise)' | awk '{print $3}'" % (design_fsf_file))
        newval=log_cmd("cat %s | awk '{print $1}'" % (noise_est_file))
        log_cmd("sed -i 's/fmri(noise) %s/fmri(noise) %s/' %s" % (oldval.strip(),newval.strip(),design_fsf_file))
        
        oldval=log_cmd("cat %s | grep 'fmri(noisear)' | awk '{print $3}'" % (design_fsf_file))
        newval=log_cmd("cat %s | awk '{print $2}'" % (noise_est_file))
        log_cmd("sed -i 's/fmri(noisear) %s/fmri(noisear) %s/' %s" % (oldval.strip(),newval.strip(),design_fsf_file))
    else:
         raise Exception("Error: Unknown cmind_update_FSF case")
         

def csv2matlab_string(csvfile):
    """prints out a string containing the matlab commands that would be needed 
       to generate a registration structure equivalent to the CSV file in matlab
       
    Parameters
    ----------     
    csvfile : str
        filename of the .csv file to read in
        
    Notes
    -----
    Reads from a a simple 2 column CSV file where the keys are stored in the
    first column and the values in the second.
    """
    for key, val in csv.reader(open(csvfile,'r')):
        if val.find(',')!=-1:  #special case: split entries with commas into a list   e.g.  "e1,e2,e3" -> ["e1","e2","e3"]
            val=val.strip('"').split(',') 
        else:
            val=val.strip() #make sure no newline characters
        if isinstance(val,list):
            print "reg_struct.%s=%s;" % (key,val)
        else:
            print "reg_struct.%s='%s';" % (key,val)
         
         
#def cmind_unify_base_dirs(workflow):
#    """ Set the base_dir of each node to match the base_dir of the overall 
#    workflow
#    
#    Parameters
#    ----------
#    workflow : nipype.pipeline.engine.Workflow
#        input workflow
#        
#    Returns
#    -------
#    workflow : nipype.pipeline.engine.Workflow
#        output workflow
#        
#    """
#    import nipype.pipeline.engine
#    
#    if not isinstance(workflow,nipype.pipeline.engine.Workflow):
#        raise ValueError("workflow must be of type nipype.pipeline.engine.Workflow")
#        
#    #iterate through each node in the workflow, setting base_dir
#    for node_name in workflow.list_node_names():
#        node = workflow.get_node(node_name)
#        node.base_dir = workflow.base_dir
#    
#    return workflow
#
#def cmind_autoset_output_dirs(workflow,unify_base_dirs=True):
#    """ Set the "output_dir" input to each CMIND module in the workflow so that
#    it matches the output directory selected by nipype.
#    
#    Parameters
#    ----------
#    workflow : nipype.pipeline.engine.Workflow
#        input workflow
#        
#    Returns
#    -------
#    workflow : nipype.pipeline.engine.Workflow
#        output workflow
#        
#    """
#    import nipype.pipeline.engine
#    
#    if not isinstance(workflow,nipype.pipeline.engine.Workflow):
#        raise ValueError("workflow must be of type nipype.pipeline.engine.Workflow")
#        
#    if unify_base_dirs:
#        workflow=cmind_unify_base_dirs(workflow)
#        
#    #iterate through each node in the workflow, setting the output_dir
#    #appropriately
#    for node_name in workflow.list_node_names():
#        node = workflow.get_node(node_name)
#        if 'output_dir' in node.inputs.trait_names():
#            node.inputs.output_dir = node.output_dir()        
#    
#    return workflow
         