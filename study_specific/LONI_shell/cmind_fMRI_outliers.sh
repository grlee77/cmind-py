#!/bin/bash
echo "CMD: $*"
#TODO:  remove -nproc from LONI pipeline.  add -use_fm, -scantype to pipeline,  -debug to pipeline
# -vol, -mpar, -regBBR_ref from fMRI_preprocess.sh
# -filt, -noise from fMRI_preprocess2.sh
# -gm, -wm, -csf from segment.sh
# -w, -aff from regBBR
usage() 
{
echo "Recommended Usage:      
	 cmind_fMRI_outliers.sh -o output_dir  -con int -seg_dir dirname -pre_dir dirname  -pre2_dir dirname -outcase string -scantype string [-reg_dir dirname]
	 
Required parameters:
    -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which to store the output
    -con CONTRAST_OF_INTEREST, --contrast_of_interest CONTRAST_OF_INTEREST
                        which contrast in FSF_template is of primary interest?
  

    -seg_dir DIRECTORY : directory containing GM, WM, CSF segmentation files
                OR
    -wm WM_PVE_VOL, --WM_pve_vol WM_PVE_VOL
                        WM partial volume estimate from structural processing
    -gm GM_PVE_VOL, --GM_pve_vol GM_PVE_VOL
                        GM partial volume estimate from structural processing
    -csf CSF_PVE_VOL, --CSF_pve_vol CSF_PVE_VOL
                        CSF partial volume estimate from structural processing
  

    -pre_dir DIRECTORY : directory containing motion correction preprocessing result
                OR    
    -vol VOL_FILE_NIIGZ, --vol_file_niigz VOL_FILE_NIIGZ
                        unfiltered 4D fMRI timeseries to process
    -mpar MOTION_PARFILE, --motion_parfile MOTION_PARFILE
                        The 6 column motion parameter (.par) file from mcflirt
    -regBBR REGBBR_REF, --regBBR_ref REGBBR_REF
                        filename of the regBBR reference image
  
    
    -pre2_dir DIRECTORY : directory containing preprocessed fMRI output
                OR
    -volfilt FILTERED_FUNC_DATA, --filtered_func_data FILTERED_FUNC_DATA
                        smoothed and temporal filtered 4D fMRI timeseries file
                        to process
    -noise NOISE_EST, --noise_est NOISE_EST
                        The noise_est.txt file from cmind_fMRI_preprocess2.py
  
    -outcase      : Stories or Sentences
    -scantype     : ASL or BOLD
    
Optional parameters:

    -reg_dir DIRECTORY : directory containing transforms to standard space
                    OR
    -aff HRTOLR_AFFINE, --HRtoLR_Affine HRTOLR_AFFINE
                        filename for the affine transform from
                        functional->structural space
    -w HRTOLR_WARP, --HRtoLR_Warp HRTOLR_WARP
                        filename for the warp from functional->structural
                        space
  

    -dm UNFILTERED_DESIGN_MATRIX, --unfiltered_design_matrix UNFILTERED_DESIGN_MATRIX
                        The (unfiltered) Feat design matrix corresponding to
                        `vol_file_niigz`
    -fsf FSF_TEMPLATE, --FSF_template FSF_TEMPLATE
                        THE .fsf template corresponding to `vol_file_niigz`

    -use_fm USE_FIELDMAP  
                        if true, use fieldmap
    -ucla UCLA_flag             
                        true if data is from UCLA, false if data is from CCHMC
    -np NPROC_MAX, --Nproc_max NPROC_MAX
                        number of threads for ANTS/ITK
    -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
    -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
    -v, -debug, --verbose
                        print additional output (to terminal and log)
    -log LOGGER, --logfile LOGGER
                        filename to log to
    "
}

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=12
if [ $# -lt $REQUIRED_ARGS ] 
then
    usage
    exit 1
fi


if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

ForceUpdate=true
generate_figures=true
output_dir="default"
unfiltered_design_matrix="default"
FSF_template="default"
UCLA_flag=false
logfile="None"
Nproc_max=1
verbose_string=""
use_fieldmap=true
verbose_string=""
all_optional_args=""
while [ "$1" != "" ]; do
  case $1 in
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -con|--contrast_of_interest )
        shift
        contrast_of_interest=$1
        ;;
    -regBBR|--regBBR_ref )
        shift
        regBBR_ref=$1
        ;;
    -wm|--WM_pve_vol )
        shift
        WM_pve_vol=$1
        ;;
    -gm|--GM_pve_vol )
        shift
        GM_pve_vol=$1
        ;;
    -csf|--CSF_pve_vol )
        shift
        CSF_pve_vol=$1
        ;;
    -vol|--vol_file_niigz )
        shift
        vol_file_niigz=$1
        ;;
    -volfilt|--filtered_func_data )
        shift
        filtered_func_data=$1
        ;;
    -mpar|--motion_parfile )
        shift
        motion_parfile=$1
        ;;
    -dm|--unfiltered_design_matrix )
        shift
        unfiltered_design_matrix=$1
        ;;
    -fsf|--FSF_template )
        shift
        FSF_template=$1
        ;;
    -noise|--noise_est )
        shift
        noise_est=$1
        ;;
    -aff|--HRtoLR_Affine )
        shift
        HRtoLR_Affine=$1
        ;;
    -w|--HRtoLR_Warp )
        shift
        HRtoLR_Warp=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    -np|--Nproc_max )
        shift
        Nproc_max=$1
        ;;
    -reg_dir )    #extra for shell script only.  not a python argument
        shift
        reg_dir=$1
        ;;
    -pre_dir )    #extra for shell script only.  not a python argument  
        shift
        pre_dir=$1
        ;;
    -pre2_dir )   #extra for shell script only.  not a python argument 
        shift
        pre2_dir=$1
        ;;
    -seg_dir )    #extra for shell script only.  not a python argument 
        shift
        seg_dir=$1
        ;;
    -use_fm )     #extra for shell script only.  not a python argument
        shift
        use_fieldmap=$1
        ;;
    -scantype )   #extra for shell script only.  not a python argument
        shift
        scantype_str=$1
        ;;
    -outcase )    #extra for shell script only.  not a python argument
        shift
        outlier_case=$1
        ;;  
    esac
    shift
done


if [ ! -z $verbose_string ]; then
    debug=true
else
    debug=false
fi

if [ -d ${reg_dir} ]
then
    if ! $use_fieldmap
    then
		if [ -z "${HRtoLR_Affine+xxx}" ] || [ ! -f $HRtoLR_Affine ]
		then
		    HRtoLR_Affine=${reg_dir}/lowres2highres_inv.mat
		fi
		if [ ! -f $HRtoLR_Affine ]
		then
			echo "Error: affine not found"
			exit 1
		fi
		HRtoLR_Warp="None"
    else
		if [ -z "${HRtoLR_Warp+xxx}" ] || [ ! -f $HRtoLR_Warp ]
		then
		    HRtoLR_Warp=${reg_dir}/lowres2highres_warp_inv.nii.gz
		fi
		if [ ! -f $HRtoLR_Warp ]
		then
	       HRtoLR_Warp="None"
		   echo "Warning:  fieldmap file, $HRtoLR_Warp not found"
		   echo "will try without fieldmap instead"
		   if [ -z "${HRtoLR_Affine+xxx}" ] || [ ! -f $HRtoLR_Affine ]
       	   then
       	       HRtoLR_Affine=${reg_dir}/lowres2highres_inv.mat
       	   fi 
       	   if [ ! -f $HRtoLR_Affine ]
       	   then
       	       echo "Error: affine not found either"
       	       exit 1
       	   fi
       	else
       		HRtoLR_Affine="None"
       	fi
    fi
fi


if [ -d ${seg_dir} ]
then
    if [ -z "${GM_pve_vol+xxx}" ] || [ ! -f $GM_pve_vol ]
    then
        GM_pve_vol=${seg_dir}/T1Segment_GM.nii.gz
    fi
    
    if [ -z "${WM_pve_vol+xxx}" ] || [! -f $WM_pve_vol ]
    then
        WM_pve_vol=${seg_dir}/T1Segment_WM.nii.gz
    fi
    
    if [ -z "${CSF_pve_vol+xxx}" ] || [ ! -f $CSF_pve_vol ]
    then
        CSF_pve_vol=${seg_dir}/T1Segment_CSF.nii.gz
    fi
fi

if [ -d ${pre2_dir} ]; then
    if [ "$(ls -A $pre2_dir)" ]; then
        if [ -z "${filtered_func_data+xxx}" ] || [ ! -f $filtered_func_data ]
        then
            filtered_func_data=${pre2_dir}/filtered_func_data.nii.gz
        fi
        
        if [ -z "${noise_est+xxx}" ] || [! -f $noise_est ]
        then
            noise_est=${pre2_dir}/noise_est.txt
        fi
        check_output="true"
    else  
        filtered_func_data="None"
        noise_est="None"  
        check_output="false"  
    fi
fi

if [ -d ${pre_dir} ]
then
    if [ -z "${vol_file_niigz+xxx}" ] || [ ! -f $vol_file_niigz ]
    then
        vol_file_niigz=${pre_dir}/${scantype_str}_${outlier_case}_mcf.nii.gz
    fi
    
    if [ -z "${motion_parfile+xxx}" ] || [! -f $motion_parfile ]
    then
        motion_parfile=${pre_dir}/${scantype_str}_${outlier_case}_mcf.par
    fi
    
    if [ -z "${regBBR_ref+xxx}" ] || [ ! -f $regBBR_ref ]
    then
        #regBBR_ref=${pre_dir}/MeanSub_${scantype_str}_${outlier_case}_mcf_N4.nii.gz
        regBBR_ref=${pre_dir}/MeanSub_ASL_${outlier_case}_mcf_N4.nii.gz
    fi
fi


if [ "$UCLA_flag" == "true" ]; then 
    UCLA_string="_UCLA"
else
    UCLA_string=""
fi
if [ "$FSF_template" == "default" ]; then
	FSF_template="${CMIND_ASLBOLD_DIR}/${scantype_str}_level1_${outlier_case}${UCLA_string}.fsf"
fi

if [ "$unfiltered_design_matrix" == "default" ]; then	
	unfiltered_design_matrix="${CMIND_ASLBOLD_DIR}/unfilt_${scantype_str}_${outlier_case}${UCLA_string}.mat"
fi


if [ "$scantype_str" != "ASL" ] && [ "$scantype_str" != "BOLD" ]; then
#	outlier_case="Feat_Outliers_${scantype_str}_${outlier_case}"
#else
	echo "invalid scantype: $scantype_str"
	exit 1
fi

#which contrast in the design is the one we are trying to optimize?
if [ "$scantype_str" == "ASL" ]; then
    contrast_of_interest=2
fi
if [ "$scantype_str" == "BOLD" ]; then
    contrast_of_interest=1
fi

if [ "$filtered_func_data" != "None" ]; then
    if [ ! -f $vol_file_niigz ]; then
        echo "ERROR: vol_file_niigz file: $vol_file_niigz doesn't exist"
        exit 1
    fi 

    if [ ! -f $motion_parfile ]; then
        echo "ERROR: motion_parfile file: $motion_parfile doesn't exist"
        exit 1
    fi 
fi

if [ ! -f $FSF_template ]; then
    echo "ERROR: FSF_template file: $FSF_template doesn't exist"
    exit 1
fi 

if [ ! -f $unfiltered_design_matrix ]; then
    echo "ERROR: unfiltered_design_matrix: $unfiltered_design_matrix doesn't exist"
    exit 1
fi 

if [ ! -f $regBBR_ref ]; then
    echo "ERROR: regbbr ref file: $regBBR_ref doesn't exist"
    exit 1
fi 

if [ ! -f $WM_pve_vol ]; then
    echo "ERROR: WM_pve_vol file: $WM_pve_vol doesn't exist"
    exit 1
fi 

if [ ! -f $GM_pve_vol ]; then
    echo "ERROR: GM_pve_vol file: $GM_pve_vol doesn't exist"
    exit 1
fi 

if [ ! -f $CSF_pve_vol ]; then
    echo "ERROR: CSF_pve_vol file: $CSF_pve_vol doesn't exist"
    exit 1
fi 

if [ "$filtered_func_data" != "None" ]; then
    if [ ! -f $filtered_func_data ]; then
        echo "ERROR: filtered_func_data: $filtered_func_data doesn't exist"
        exit 1
    fi 
fi

if [ "$noise_est" != "None" ]; then
    if [ ! -f $noise_est ]; then
        echo "ERROR: noise_est: $noise_est doesn't exist"
        exit 1
    fi 
fi

if [ ! -d $output_dir ]; then
    mkdir -p "${output_dir}"
fi

if $debug
then
    echo "output_dir=$output_dir"
    echo "contrast_of_interest=$contrast_of_interest"
    echo "regBBR_ref=$regBBR_ref"
    echo "WM_pve_vol=$WM_pve_vol"
    echo "GM_pve_vol=$GM_pve_vol"
    echo "CSF_pve_vol=$CSF_pve_vol"
    echo "vol_file_niigz=$vol_file_niigz"
    echo "filtered_func_data=$filtered_func_data"
    echo "motion_parfile=$motion_parfile"
    echo "unfiltered_design_matrix=$unfiltered_design_matrix"
    echo "FSF_template=$FSF_template"
    echo "noise_est=$noise_est"
    echo "generate_figures=$generate_figures"
    echo "ForceUpdate=$ForceUpdate"
    echo "HRtoLR_Affine=$HRtoLR_Affine"
    echo "HRtoLR_Warp=$HRtoLR_Warp"
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir contrast_of_interest regBBR_ref WM_pve_vol GM_pve_vol CSF_pve_vol vol_file_niigz filtered_func_data motion_parfile unfiltered_design_matrix FSF_template noise_est HRtoLR_Affine HRtoLR_Warp generate_figures ForceUpdate logger Nproc_max  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_fMRI_outliers.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_fMRI_outliers.py ${verbose_string} ${all_optional_args}
printf 'cmind_fMRI_outliers.py:  Elapsed time: %s\n' $(timer $t)


#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    default_confound_file
#    best_confound_file
#    worst_confound_file
#    mcf06_confound_file
#    FSL_outliers_txt


# t=$(timer)
# echo "python ${cmind_python_dir}/cmind_fMRI_outliers.py ${output_dir} ${contrast_of_interest} ${regBBR_ref} ${WM_pve_vol} ${GM_pve_vol} ${CSF_pve_vol} ${vol_file_niigz} ${filtered_func_data} ${motion_parfile} ${unfiltered_design_matrix} ${FSF_template} ${noise_est} --HRtoLR_Affine ${HRtoLR_affine} --HRtoLR_Warp ${HRtoLR_warp} --generate_figures ${generate_figures} --ForceUpdate ${ForceUpdate}  --verbose ${verbose} --logfile ${logfile} --Nproc_max ${Nproc_max} $verbose_string"
# python ${cmind_python_dir}/cmind_fMRI_outliers.py ${output_dir} ${contrast_of_interest} ${regBBR_ref} ${WM_pve_vol} ${GM_pve_vol} ${CSF_pve_vol} ${vol_file_niigz} ${filtered_func_data} ${motion_parfile} ${unfiltered_design_matrix} ${FSF_template} ${noise_est_file} --HRtoLR_Affine ${HRtoLR_affine} --HRtoLR_Warp ${HRtoLR_warp} --generate_figures ${generate_figures} --ForceUpdate ${ForceUpdate}  --verbose ${verbose} --logfile ${logfile} --Nproc_max ${Nproc_max} $verbose_string
# printf 'cmind_fMRI_outliers:  Elapsed time: %s\n' $(timer $t)

#check that QA report exists
if [ "$check_output" == "true" ]; then 
    output_file_test=${output_dir}/best_design_cov.png
    echo "$output_file_test"
    if [ ! -f $output_file_test ]; then
        echo "ERROR: Failed to finish fMRI Outliers"
        exit 1
    fi 
fi

# #print output filenames for capture by LONI pipeline
# echo "nuisance_file:${output_dir}/default_confounds.txt"
# echo "nuisance_file_best:${output_dir}/best_confounds.txt"

echo "Finished cmind_fMRI_outliers"

