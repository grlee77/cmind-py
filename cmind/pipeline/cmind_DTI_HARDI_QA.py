#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_plot_DTI_coverage(bvecs_orig,bvecs_QC,output_img,isSiemens=False,silent_flag=False):
    """Generate summary image of the DTI directions before and after quality 
    control (QC)
    
    Parameters
    ----------
    bvecs_orig : file
        three column text file containing the pre-QC b vectors
    bvecs_QC : file
        three column text file containing the post-QC b vectors
    output_img : file
        filename of the output image
    isSiemens : bool, optional
        if True, negates the values in `bvecs_orig` and `bvecs_QC` before plotting
    silent_flag : bool, optional
        if True, silently generate the figures without display
        
    Notes
    -----
    
    
    """
    import warnings
    import numpy as np
    
    from cmind.globals import (has_ImageMagick, 
                               has_GraphicsMagick, 
                               cmind_imageconvert_cmd)
    from cmind.utils.utils import sphere
    from cmind.utils.logging_utils import log_cmd
        
    use_mayavi=False 
    if use_mayavi:  #This case gave me a segfault when running the script from the command line, so switched to mplot3d solution instead
        from mayavi import mlab
        if silent_flag:
            mlab.options.offscreen=True
    else:
        import matplotlib as mpl
        mpl.use('agg')
        #import matplotlib.pylab as pylab
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        
    bv0=np.loadtxt(bvecs_orig)  
    bv0=bv0[np.where(np.sum(bv0,axis=1)!=0)[0],:]       #discard b=0 directions
    #Ndirections0=bv0.shape[0]
    
    bv1=np.loadtxt(bvecs_QC)  
    bv1=bv1[np.where(np.sum(bv1,axis=1)!=0)[0],:]       #discard b=0 directions
    #Ndirections=bv1.shape[0]
    
    if isSiemens:
        bv0=-bv0
        bv1=-bv1
    
    if use_mayavi:
        (x,y,z)=sphere(.99,50);
        mlab.show()
        mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(800, 600))
        mlab.clf()
        mlab.mesh(x, y, z, color=(0.6,0.6,0.6),opacity=1,representation='surface') #colormap='Greys')
        mlab.points3d(bv0[:,0],bv0[:,1],bv0[:,2], color=(0,0,0),opacity=1,mode='2dcircle',scale_factor='0.07') #colormap='Greys')
        mlab.points3d(bv0[:,0],bv0[:,1],bv0[:,2], color=(0,0,0),opacity=1,mode='2dcross',scale_factor='0.09') #colormap='Greys')
        mlab.points3d(bv1[:,0],bv1[:,1],bv1[:,2], color=(0,0,0),opacity=1,mode='sphere',scale_factor='0.05') #colormap='Greys')
        mlab.view(13.888,12.356)
        #See notes here on rendering to a virtual frame buffer...
        #http://docs.enthought.com/mayavi/mayavi/tips.html
    else:
        (x,y,z)=sphere(.995,64);
        #mpl.lines.Line2D.markers
        fig = plt.figure(figsize=(10,10))
        if True: #matplotlib 1.0+ preferred notation
            ax = fig.add_subplot(111, projection='3d')
        else:  #for compatibility with matplotlib<1.0
            ax=Axes3D(fig);
        ax.view_init(elev=90,azim=0)
        r=0.72
        ax.set_xlim3d(-r,r)
        ax.set_ylim3d(-r,r)
        ax.set_zlim3d(-r,r)
        ax.axis("off")
        ax.plot_surface(x,y,z,linewidth=0, rstride=1, cstride=1,color=(0.6,0.6,0.6),alpha=1)
        ax.scatter3D(bv0[:,0],bv0[:,1],bv0[:,2], c='k', marker='x',s=80)
        ax.scatter3D(bv1[:,0],bv1[:,1],bv1[:,2], c='r', marker='.',s=100)
        p1 = plt.Rectangle((0, 0), 1, 1, fc="k") #shou
        p2 = plt.Rectangle((0, 0), 1, 1, fc="r")
        ax.legend((p1,p2),('Original','Post-QA'),fontsize=14)
        ax.set_title('Directions Covered',fontsize=18,weight='bold')
        fig.savefig(output_img)
        if has_ImageMagick or has_GraphicsMagick:
            try: #use ImageMagick to auto-crop the borders
                log_cmd('%s -trim %s %s' % (cmind_imageconvert_cmd, output_img,output_img), verbose=False, logger=None)
            except:
                warnings.warn("Failed to trim borders of image via ImageMagick")
    

def cmind_DTI_HARDI_QA(DTI_input_source,gradient_table_file,gradient_bval_file,output_dir,case_to_run,DTIPrep_command,DTIPrep_xmlProtocol,DWIConvert_command=None,Flip_ap=False,isSiemens=False,isOblique=False,Slicer_path=None,Slicer_denoise_module=None,ForceUpdate=False,denoise_case = 'jointLMMSE', generate_figures=True, verbose=False, logger=None, env_dict={}):
    """Utility to run DTIPrep for quality control of DTI/HARDI image series
    
    Parameters
    ----------
    DTI_input_source : str
        raw, 4D DTI nifti-1 volumes (can be gzipped)
        Alternatively, this can point to a DICOM directory
    gradient_table_file : str
        three column text file containing the pre-QC diffusion direction vectors
    gradient_bval_file : str
        text file containing the pre-QC b values
    output_dir : str
        directory in which to store the output
    case_to_run : {'DTI_QA_DN_process','HARDI_QA_DN_process','DTI_QA_process','HARDI_QA_process'}
        choose which processing case to run. "DN" in names means use 3D slicer
        to denoise
    DTIPrep_command : str
        path to DTIPrep
    DTIPrep_xmlProtocol : str or None
        path to the xml protocol to be used by DTIPrep
    DWIConvert_command : str or None, optional
        path to DWIConvert (used for NIFTI<->NRRD conversions)
    isSiemens : bool, optional
        currently must be False
    isOblique : bool, optional
        currently must be False
    Slicer_path : str or None, optional
        path to 3D Slicer, must be specified if `case_to_run` contains 'DN'
    Slicer_denoise_module : str or None, optional    
        path to the 3D Slicer denoising module to use, must be specified if 
        `case_to_run` contains 'DN'
    denoise_case : {'LMMSE','jointLMMSE','UnbiasedNLMeans'}, optional
        the denoising case corresponding to `Slicer_denoise_module`
    generate_figures : bool, optional
        if True, generate summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    env_dict : dict, optional
        dictionary of environment variables to add to os.environ when calling 
        shell commands via log_cmd().  e.g. could use to add appropriate 
        LD_LIBRARY_PATH, etc for Slicer
    
    Returns
    -------
    
    QCd_nii_file : file
        filename of Quality Controled DTI/HARDI 4D volume
    QCd_bval_file : file
        filename of b-values corresponding to the data in QCd_nii_file
    QCd_bvec_file : file    
        filename of b-vectors corresponding to the data in QCd_nii_file

    Notes
    -----
    
    
    """

    import os
    import subprocess
    import warnings
    from os.path import join as pjoin    
    
    import nibabel as nib
    import numpy as np
        
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, multiglob_list
    from cmind.utils.nifti_utils import cmind_save_nii_mod_header
    from cmind.utils.cmind_utils import cmind_scaleimg
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    #get default slicer binary paths from cmind.globals
    from cmind.globals import cmind_Slicer3D_cli_dir, cmind_Slicer3D

    if verbose:
        cmind_func_info(logger=logger)     

    if Flip_ap:
        raise ValueError("A/P flip not implemented")
        
    #convert various strings to boolean
    ForceUpdate, verbose, isSiemens, isOblique = input2bool([ForceUpdate, verbose, isSiemens, isOblique])
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting DTi/HARDI QA")        
    
    if os.path.isdir(DTI_input_source):
        DICOM_dir=DTI_input_source  #directory instead of NIFTI file was passed in
        DTI_input_source=None
    else:
        DICOM_dir=None
        
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
     #input2bool(generate_figures)
    if generate_figures:
        generate_mplot3d_figures=True  #couldn't get this to work when called from the command line at the moment
    else:
        generate_mplot3d_figures=False
    
    if isSiemens:  #TODO: may need to slightly modify the processing for the Siemens data from UCLA
        raise Exception("DEVEL: Siemens input case not yet tested")

    if isOblique:
        raise Exception("DEVEL: Oblique input case not yet tested")
    
    if not os.path.exists(DTIPrep_xmlProtocol):
        raise IOError("specified xmlProtocol, %s, doesn't exist" % (DTIPrep_xmlProtocol))
    
    if case_to_run.lower() in ['hardi_qa_process','hardi_qa_dn_process']:
        #case_str='HARDI_QA';
        #case_str2='HARDI';
        #case_bval=3000;
        QAdir=output_dir;
        nrrd_file=pjoin(QAdir,'HARDI_avgb0_at_start.nrrd');
    elif case_to_run.lower() in ['dti_qa_process','dti_qa_dn_process']:
        #case_str='DTI_QA';
        #case_str2='DTI';
        #case_bval=1000;
        QAdir=output_dir;
        nrrd_file=pjoin(QAdir,'DTI_avgb0_at_start.nrrd');
    else:
        raise ValueError("Unrecognized case_to_run: %s" % case_to_run)
    
    if case_to_run.lower().find('dn_process')!=-1:
        do_denoising=True
        if (cmind_Slicer3D is None) or (cmind_Slicer3D_cli_dir is None):
            raise ValueError("cannot run case: {} because 3D Slicer was not found".format(case_to_run))
    else:
        do_denoising=False

    if not DWIConvert_command: #do the conversion in Matlab
        DWIConvert_command=pjoin(cmind_Slicer3D_cli_dir,'DWIConvert')
        
    if not os.path.exists(DWIConvert_command):                            
        raise Exception("Couldn't find DWIConvert binary.  Please specify explicitly via DWIConvert_command")    
        
        
        
    if not isOblique:
        #                 %                      f=dir('HARDI_QA/HARDI.trk'); if(not isempty(f)); d=f.date; end
        #                 %                      dstr=d(4:6); if(strcmp(dstr,'May')); ForceUpdate=0; end
        #                 %
        #                 if(ForceUpdate)
        #                     log_cmd('rm -rf %s' % case_str, verbose=verbose, logger=logger)
        #                 end
        #                 %                     if(strcmp(case_to_run,'HARDI_QA_process'))
        #                 %                         !rm -rf HARDI_QA
        #                 %                     end
        #                 if (not os.path.exists(case_str)); os.makedirs(case_str); end
        #                 os.chdir(case_str)
        #
        #DICOM_dir=[];
        
        bvec_file=nrrd_file.replace('.nrrd','_bvects')
        bval_file=nrrd_file.replace('.nrrd','_bvals')
        QCd_nrrd_file=nrrd_file.replace('.nrrd','_QCed.nrrd')
        QCd_nii_file=nrrd_file.replace('.nrrd','_QCed.nii')
        QCd_bval_file=bval_file.replace('_bvals','_QCed_bvals');
        QCd_bvec_file=bvec_file.replace('_bvects','_QCed_bvects');
        
        if not DICOM_dir:
            if not imexist(DTI_input_source,'nifti-1')[0]:
                raise IOError("DTI_input_source, %s, doesn't exist!" % DTI_input_source)
            else:
                DTI_input_nii = nib.load(DTI_input_source) #TODO: verify non-oblique


                ori = DTI_input_nii.get_affine()[:3,:3]
                
                #is the data axial?                
                if not np.all(np.argmax(np.abs(ori),axis=1) == np.array([0,1,2])):
                    raise ValueError("Expected axial input data")

                #are the non-zero off-diagonal elements in the affine?
                ori_zeromask=(np.ones((3,3))-np.eye(3))>0
                max_oblique_val = np.max(np.abs(ori[ori_zeromask]))
                if max_oblique_val>0:
                    if max_oblique_val<1e-3:
                        warnings.warn("very small nonzero diagonals (< 1e-3) found in input data affine.  truncating these to zero before further processing")
                        #import tempfile
                        #tempdir = tempfile.mkdtemp()
                        
                        sform = DTI_input_nii.get_sform()
                        sform[:3,:3] = sform[:3,:3]*np.eye(3)
                        qform = DTI_input_nii.get_qform()
                        qform[:3,:3] = qform[:3,:3]*np.eye(3)
                        DTI_input_nii.set_sform(sform)
                        DTI_input_nii.set_qform(qform)
                        
                        DTI_input_source = pjoin(QAdir,'DTI_input_converted_to_axial.nii.gz')
                        DTI_input_nii.to_filename(DTI_input_source)
                        
                    else:
                        raise ValueError("DTI QA processing will not run on non-axial (oblique) data")
                    
                    

            if not os.path.exists(gradient_table_file):
                raise IOError("gradient_table_file, %s, doesn't exist!" % gradient_table_file)
            if not os.path.exists(gradient_bval_file):
                raise IOError("gradient_bval_file, %s, doesn't exist!" % gradient_bval_file)
            #NIFTI doesn't have the bvectors or bvalues in the header, so copy it over from the specified text files
            
            
            
        
        
        #bvecs=np.loadtxt(gradient_table_file);
        #np.savetxt(bvec_file,bvecs,'delimiter',' ')
        #bvals=np.loadtxt(gradient_bval_file);
        #np.savetxt(bval_file,bvals,'delimiter',' ')
        
        
        
        #Run automated QA
        (QC_output_exists, trueQC_fname)=imexist(QCd_nii_file,'nifti-1')[0:2];
        
        if ForceUpdate:  #cleanup results from a previous run in this folder
            
            img_files_to_remove=[trueQC_fname, pjoin(QAdir,'b0avg'), pjoin(QAdir,'DWavg'), pjoin(QAdir,'custom_mask'), pjoin(QAdir,'DTI_rescaled')]
            for f in img_files_to_remove:
                if imexist(f,'nifti-1')[0]:
                    log_cmd('imrm "%s"' % f, verbose=verbose, logger=logger, env_dict=env_dict)
                            
            other_files_to_remove=['Directions_Kept.txt','Ndirections_kept.txt','HARDI_avgb0*','DTI_avgb0*','custom_mask*.nii.gz','b0avg*','DWavg*','DW_denoise*','b0_denoise*','*_coverage.png','*QCResult*','*QCed*']
            for n in range(len(other_files_to_remove)):  #convert to absolute filenames
                other_files_to_remove[n]=pjoin(QAdir,other_files_to_remove[n])
            filelist=multiglob_list(other_files_to_remove) #use glob() to resolve wildcards and concatenate into a single file list
            for rfile in filelist:
                os.remove(rfile)

        if not DICOM_dir:
            log_cmd('cp "%s" "%s"' % (gradient_table_file,bvec_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
            log_cmd('cp "%s" "%s"' % (gradient_bval_file,bval_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
            
        if (not QC_output_exists) or ForceUpdate:
                
            #Convert input to NRRD for use with DTIPrep
            
            if DICOM_dir:  #DICOM directory to NRRD
                from cmind.utils import nrrd_cmind as nrrd
                
                warnings.warn("direct conversion from DICOM_dir currently experimental!")
                #Convert DICOM to NRRD
                if (not imexist(nrrd_file,'nrrd')[0]) or ForceUpdate:
                    if False: #use older DicomToNrrdConverter
                        log_cmd('~/Downloads/DTIPrep_1.1.6_linux64/DicomToNrrdConverter --inputDicomDirectory "%s"  --outputVolume "%s"'% (DICOM_dir,nrrd_file), verbose=verbose, logger=module_logger, env_dict=env_dict);
                    else:  #use DWIConvert
                        ###log_cmd('%s --conversionMode DicomToNrrd --inputVolume "%s"  --outputVolume "%s"  --inputBVectors "%s"  --inputBValues "%s"' % (DWIConvert_command,DICOM_dir,nrrd_file,bvec_file,bval_file), verbose=verbose, logger=module_logger)
                        log_cmd('%s --conversionMode DicomToNrrd --inputDicomDirectory "%s"  --outputVolume "%s"' % (DWIConvert_command,DICOM_dir,nrrd_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                
                nrrd_data, options = nrrd.read(nrrd_file)       #read in NRRD file
                nrrd.write(nrrd_file,nrrd_data,options=options,fix_b0=True) #write it back out to same filename, but with b0 labels fixed
                
                #write out the bvec and bval files for the pre-QA volumes to text files
                bvec_nrrd=nrrd.read_bvec_from_options(options,omit_b0=False)
                bval=float(options['keyvaluepairs']['DWMRI_b-value'])
                bval_nrrd=bval*np.ones((bvec_nrrd.shape[0],1))
                bval_nrrd[np.where(np.sum(bvec_nrrd,axis=1)==0)[0]]=0
                np.savetxt(bvec_file,bvec_nrrd,delimiter=' ',fmt='%0.16g')
                np.savetxt(bval_file,bval_nrrd,delimiter=' ',fmt='%0.16g')
            else:  #NIFTI to NRRD
                #DWIConvert and DTIPrep don't seem to work right with large float values so rescale to INT16 if it isn't already in that format
                FSL_dtype=log_cmd('$FSLDIR/bin/fslval "%s" data_type' % DTI_input_source, verbose=verbose, logger=logger)
            
                #did_rescale=False
                FORCE_INT16=True #convert FLOAT32 or other datatypes to INT16 before further processing.  Otherwise DTIPrep seems to have problems
                if FORCE_INT16 and (FSL_dtype.find('INT16')==-1):
                    #FSL_minmax=log_cmd('$FSLDIR/bin/fslstats "%s" -R' % DTI_input_source, verbose=verbose, logger=logger);
                    #FSL_minmax=str2num(FSL_minmax); maxabs=max(abs(FSL_minmax))
                    #div_factor=maxabs/32760.0
                    DTI_orig=nib.load(DTI_input_source)
                    DTI_img=DTI_orig.get_data();
                    DTI_img=cmind_scaleimg(DTI_img,99.999,4)
                    cmind_save_nii_mod_header(DTI_orig,DTI_img,pjoin(QAdir,'DTI_rescaled.nii.gz'),4)
                    DTI_input_source=pjoin(QAdir,'DTI_rescaled.nii.gz')
                
                if not DWIConvert_command:
                    raise IOError("DWIConvert not found!  unable to perform nifti_to_nrrd conversion!")
                    #nifti_to_nrrd(DTI_input_source,nrrd_file,bvec_file,bval_file);
                else:
                    log_cmd('%s --conversionMode FSLToNrrd --inputVolume "%s"  --outputVolume "%s"  --inputBVectors "%s"  --inputBValues "%s"' % (DWIConvert_command,DTI_input_source,nrrd_file,bvec_file,bval_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    
                    #!/media/Data1/local_installs/Slicer_Nightly/Slicer-4.2.0-2013-03-12-linux-amd64/Slicer --no-splash --launch DWIConvert --conversionMode FSLToNrrd --inputVolume ../DTI_avgb0_at_start.nii.gz --outputVolume ./test.nhdr --inputBVectors test_bvecs --inputBValues test_bval

            #Note: NRRD file from DWIConvert incorrectly claims left-posterior-superior orientation even though the nifti is LAS
                
                            
            #QCd file doesn't exist, so create it
            if (not QC_output_exists) or ForceUpdate: #skip DTIPrep if it's output already exists...
                try:
                    log_cmd('%s --DWINrrdFile "%s" --xmlProtocol "%s" --check --outputFolder "%s"' % (DTIPrep_command,nrrd_file, DTIPrep_xmlProtocol, QAdir), verbose=verbose, logger=module_logger);
                except subprocess.CalledProcessError as e:
                    #Note: some non-essential checks in DTIPrep always fail with the current xmlProtocol.  
                    #usually see:   Image_information_checkingImage_origin_check FAILED
                    #               DWMRI_bValue Check: 		FAILED
                    #               Diffusion gradient Check: 	FAILED
                    #               Diffusion information Check FAILED.
                    #For now, just proceed even if DTI prep didn't return 0, but print out a warning message
                    #print "Warning: \n" 
                    #print e  # prints (e.command, e.returncode)
                    print("DTIPrep stdout output:  %s" % e.output)
                
            QC_output_exists=imexist(QCd_nrrd_file,'nrrd')[0];    
            if (not QC_output_exists):
                np.savetxt(pjoin(QAdir,'Directions_Kept.txt'),[0,]);
                raise Exception("Too few gradient directions remaining.  aborted")
                
            #if(0) %command line tensor estimation
            #    dtiestim_cmd='/media/Data1/local_installs/DTIPrep_new/bin/dtiestim'
            #    dti_baseline_thresh=50;
            #    dtiprocess_cmd='/media/Data1/local_installs/DTIPrep_new/bin/dtiprocess'
            #    tensor_file=QCd_nrrd_file.replace('.nrrd','_DTI.nrrd')
            #    Baseline_file=tensor_file.replace(.nrrd','_Baseline.nrrd')
            #    IDWI_file=tensor_file.replace('.nrrd','_IDWI.nrrd')
            #    log_cmd('%s --dwi_image "%s" --tensor_output "%s" -t %d -m wls  --B0 "%s" --idwi "%s" ',dtiestim_cmd,QCd_nrrd_file,tensor_file,dti_baseline_thresh,Baseline_file,IDWI_file))
            #    
            #    fa_file=tensor_file.replace('.nrrd','_FA.nrrd')
            #    colorfa_file=tensor_file.replace('.nrrd','_colorFA.nrrd')
            #    md_file=tensor_file.replace('.nrrd','_MD.nrrd')
            #    frobenius_file=tensor_file.replace('.nrrd','_frobeniusnorm.nrrd') 
            #    RD_file=tensor_file.replace('.nrrd','_RD.nrrd')
            #    log_cmd('%s --dti_image "%s" -f "%s" -m "%s" --color_fa_output "%s" --frobenius_norm_output "%s" --RD_output "%s"',dtiprocess_cmd,tensor_file,fa_file,md_file,colorfa_file,frobenius_file,RD_file))

            
            #Optionally denoise the QC'd output
            if do_denoising: #Running denoising via Slicer 4 command line interface
                if not Slicer_path:
                    Slicer_path=cmind_Slicer3D
                nrrd_denoise_file2=""
                #use --ng 8 to for jointLMMSE using 8 closest Neighbors
                if denoise_case=='LMMSE':
                    if not Slicer_denoise_module:
                        Slicer_denoise_module=pjoin(cmind_Slicer3D_cli_dir,'DWIRicianLMMSEFilter')
                    nrrd_denoise_file=QCd_nrrd_file.replace('.nrrd','_LMMSE_denoised.nrrd');
                    if (not imexist(nrrd_denoise_file,'nrrd')[0]) or ForceUpdate:
                        log_cmd('%s --launcher-no-splash --launch "%s" "%s" "%s"' % (Slicer_path,Slicer_denoise_module,QCd_nrrd_file,nrrd_denoise_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                elif denoise_case=='jointLMMSE':
                    if not Slicer_denoise_module:
                        Slicer_denoise_module=pjoin(cmind_Slicer3D_cli_dir,'DWIJointRicianLMMSEFilter')
                    nrrd_denoise_file=QCd_nrrd_file.replace('.nrrd','_jointLMMSE_denoised.nrrd');
                    if (not imexist(nrrd_denoise_file,'nrrd')[0]) or ForceUpdate:
                        log_cmd('%s --launcher-no-splash --launch "%s" "%s" "%s"' % (Slicer_path,Slicer_denoise_module,QCd_nrrd_file,nrrd_denoise_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    
                    #rerun but using only the 6 nearest gradient directions instead of all gradient directions
                    nrrd_denoise_file2=QCd_nrrd_file.replace('.nrrd','_jointLMMSE_denoised_6dir.nrrd');
                    if (not imexist(nrrd_denoise_file2,'nrrd')[0]) or ForceUpdate:
                        log_cmd('%s --launcher-no-splash --launch "%s" "%s" "%s" --ng 6' % (Slicer_path,Slicer_denoise_module,QCd_nrrd_file,nrrd_denoise_file2), verbose=verbose, logger=module_logger, env_dict=env_dict)

                elif denoise_case=='UnbiasedNLMeans':   #warning:  MUCH slower than the other options, but better edge preservation.  Took around 6 minutes on the 2mm resolution test data I tried it on
                    if not Slicer_denoise_module:
                        Slicer_denoise_module=pjoin(cmind_Slicer3D_cli_dir,'DWIUnbiasedNonLocalMeansFilter')
                    nrrd_denoise_file=QCd_nrrd_file.replace('.nrrd','_NLMeans_denoised.nrrd');
                    if (not imexist(nrrd_denoise_file,'nrrd')[0]) or ForceUpdate:
                        log_cmd('%s --launcher-no-splash --launch "%s" "%s" "%s"' % (Slicer_path,Slicer_denoise_module,QCd_nrrd_file,nrrd_denoise_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                else:
                    raise ValueError("Unknown denoise_case, %s" % denoise_case)
                QCd_nii_denoise_file=nrrd_denoise_file.replace('.nrrd','.nii');
                if nrrd_denoise_file2:
                    QCd_nii_denoise_file2=nrrd_denoise_file2.replace('.nrrd','.nii');

                #if False:
                    #[X_orig meta_orig]=nrrdread('DTI_avgb0_at_start.nrrd');
                    #[X_LMMSE meta_LMMSE]=nrrdread('Denoised_Rician.nrrd');       X_LMMSE=permute(X_LMMSE,[2 3 4 1]);
                    #[X_jointLMMSE meta_jointLMMSE]=nrrdread('Denoised.nrrd');    X_jointLMMSE=permute(X_jointLMMSE,[2 3 4 1]);
                    #[X_jointLMMSE4 meta_jointLMMSE]=nrrdread('Denoised_joint4.nrrd');    X_jointLMMSE4=permute(X_jointLMMSE4,[2 3 4 1]);
                    #[X_jointLMMSE8 meta_jointLMMSE]=nrrdread('Denoised_joint8.nrrd');    X_jointLMMSE8=permute(X_jointLMMSE8,[2 3 4 1]);
                    #[X_jointLMMSE16 meta_jointLMMSE]=nrrdread('Denoised_joint16.nrrd');    X_jointLMMSE16=permute(X_jointLMMSE16,[2 3 4 1]);
                    #[X_NLMeans meta_NLMeans]=nrrdread('Denoised_NLMeans.nrrd');  X_NLMeans=permute(X_NLMeans,[2 3 4 1]);

            #Convert back to NIFTI
            if not DWIConvert_command: #do the conversion in Matlab
                    raise Exception("DEVEL: python-based NRRD->NIFTI not currently implemented.  Please supply DWIConvert_command")
            else:
                log_cmd('%s --conversionMode NrrdToFSL --inputVolume "%s"  --outputVolume "%s"  --outputBValues "%s"  --outputBVectors "%s"' % (DWIConvert_command,QCd_nrrd_file,QCd_nii_file,QCd_bval_file,QCd_bvec_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                if do_denoising:
                    dn_bval=pjoin(output_dir,'denoised.bval')
                    dn_bvec=pjoin(output_dir,'denoised.bvec')
                    log_cmd('%s --conversionMode NrrdToFSL --inputVolume "%s"  --outputVolume "%s"  --outputBValues "%s"  --outputBVectors "%s"' % (DWIConvert_command,nrrd_denoise_file,QCd_nii_denoise_file,dn_bval,dn_bvec), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    if nrrd_denoise_file2:
                        log_cmd('%s --conversionMode NrrdToFSL --inputVolume "%s"  --outputVolume "%s"  --outputBValues "%s"  --outputBVectors "%s"' % (DWIConvert_command,nrrd_denoise_file2,QCd_nii_denoise_file2,dn_bval,dn_bvec), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    os.remove(dn_bval);
                    os.remove(dn_bvec);
            
            if DICOM_dir:
                #For DICOM case, need to manually fix the NIFTI orientations...
                log_cmd('$FSLDIR/bin/fslchfiletype NIFTI_GZ "%s"' % (QCd_nii_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                log_cmd('$FSLDIR/bin/fslswapdim "%s" x -y z "%s"' % (QCd_nii_file,QCd_nii_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                log_cmd('$FSLDIR/bin/fslorient -swaporient "%s"' % (QCd_nii_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                log_cmd('$FSLDIR/bin/fslchfiletype NIFTI "%s"' % (QCd_nii_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                #bvecs=np.loadtxt(QCd_bvec_file);
                #bvecs[:,0]=-bvecs[:,0];
                #np.savetxt(QCd_bvec_file,bvecs,fmt='%0.16g')
                if do_denoising:
                    log_cmd('$FSLDIR/bin/fslchfiletype NIFTI_GZ "%s"' % (QCd_nii_denoise_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    log_cmd('$FSLDIR/bin/fslswapdim "%s" x -y z "%s"' % (QCd_nii_denoise_file,QCd_nii_denoise_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    log_cmd('$FSLDIR/bin/fslorient -swaporient "%s"' % (QCd_nii_denoise_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    log_cmd('$FSLDIR/bin/fslchfiletype NIFTI "%s"' % (QCd_nii_denoise_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    if nrrd_denoise_file2:
                        log_cmd('$FSLDIR/bin/fslchfiletype NIFTI_GZ "%s"' % (QCd_nii_denoise_file2), verbose=verbose, logger=module_logger, env_dict=env_dict)
                        log_cmd('$FSLDIR/bin/fslswapdim "%s" x -y z "%s"' % (QCd_nii_denoise_file2,QCd_nii_denoise_file2), verbose=verbose, logger=module_logger, env_dict=env_dict)
                        log_cmd('$FSLDIR/bin/fslorient -swaporient "%s"' % (QCd_nii_denoise_file2), verbose=verbose, logger=module_logger, env_dict=env_dict)
                        log_cmd('$FSLDIR/bin/fslchfiletype NIFTI "%s"' % (QCd_nii_denoise_file2), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    
        
            Ntmp=log_cmd('$FSLDIR/bin/fslval "%s" dim4' % (QCd_nii_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
            Ndirections_kept=int(Ntmp)-1;  #subtract the average b0 to get the number of diffusion directions kept
            np.savetxt(pjoin(QAdir,'Directions_Kept.txt'),np.asarray([Ndirections_kept,]),fmt='%d');

                    
            log_cmd('$FSLDIR/bin/fslroi "%s" "%s"/b0avg 0 1' % (QCd_nii_file,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
            log_cmd('$FSLDIR/bin/fslroi "%s" "%s"/DWavg 1 -1' % (QCd_nii_file,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
            log_cmd('$FSLDIR/bin/fslmaths "%s"/DWavg  -Tmean "%s"/DWavg -odt int' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
            if do_denoising:
                log_cmd('$FSLDIR/bin/fslroi "%s" "%s"/b0_denoise_avg 0 1' % (QCd_nii_denoise_file,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                log_cmd('$FSLDIR/bin/fslroi "%s" "%s"/DW_denoise_avg 1 -1' % (QCd_nii_denoise_file,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                log_cmd('$FSLDIR/bin/fslmaths "%s"/DW_denoise_avg  -Tmean "%s"/DW_denoise_avg -odt int' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                if nrrd_denoise_file2:
                    log_cmd('$FSLDIR/bin/fslroi "%s" "%s"/b0_denoise2_avg 0 1' % (QCd_nii_denoise_file2,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    log_cmd('$FSLDIR/bin/fslroi "%s" "%s"/DW_denoise2_avg 1 -1' % (QCd_nii_denoise_file2,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    log_cmd('$FSLDIR/bin/fslmaths "%s"/DW_denoise2_avg  -Tmean "%s"/DW_denoise2_avg -odt int' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                
            #Note: automated masking wasn't working in HARDI case, so I do it manually
            make_custom_mask=True
            if make_custom_mask:
                if (not imexist(pjoin(QAdir,'custom_mask.nii.gz'),'nifti-1')[0]) or ForceUpdate:
                    QCd_nii_masked_file=QCd_nii_file.replace('QCed.nii','QCed_masked.nii');

                    log_cmd('$FSLDIR/bin/imcp "%s" "%s"' % (QCd_nii_file,QCd_nii_masked_file), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    nii1=nib.load(QCd_nii_masked_file);
                    nii1_img=nii1.get_data();
                    b0avg=nii1_img[:,:,:,0];
                    DWavg=np.mean(nii1_img[:,:,:,1::],3);
                    DWdropout_mask=np.sum(nii1_img[:,:,:,1::]>0,3)==nii1_img.shape[3]-1
                    #b0mask=abs(b0avg)>.075*max(b0avg(:));
                    b0_95p=np.percentile(b0avg.flatten(),q=95.0)
                    b0mask=np.abs(b0avg)>.1*b0_95p;
                    #DWmask=abs(DWavg)>.11*max(DWavg(:));
                    DW_99p=np.percentile(DWavg.flatten(),q=99.0)
                    DWmask=np.abs(DWavg)>.08*DW_99p
                    custom_mask=b0mask*DWmask*DWdropout_mask;
                    custom_mask=custom_mask>0;
                
                    #log_cmd('fslview DWavg custom_mask
                    #                             nii1.img=double(nii1.img);
                    #                             for zz=1:size(nii1.img,4)
                    #                                 nii1.img(:,:,:,zz)=nii1.img(:,:,:,zz).*custom_mask;
                    #                             end
                    #                             int16_img=int16(nii1.img);

                    cmind_save_nii_mod_header(nii1,custom_mask,pjoin(QAdir,'custom_mask.nii.gz'));
                    log_cmd('$FSLDIR/bin/fslmaths "%s"/custom_mask -fillh "%s"/custom_mask' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict) #fill any holes in mask
                    log_cmd('$FSLDIR/bin/fslmaths "%s"/b0avg.nii.gz -mul "%s"/custom_mask "%s"/b0avg_masked.nii.gz' % (QAdir,QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    log_cmd('$FSLDIR/bin/bet "%s"/b0avg_masked "%s"/b0avg_masked -f 0.3 -R' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    log_cmd('$FSLDIR/bin/fslmaths "%s"/b0avg_masked.nii.gz -thr 0 -bin "%s"/custom_mask2.nii.gz' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    log_cmd('$FSLDIR/bin/imrm %s' % QCd_nii_masked_file, verbose=verbose, logger=logger, env_dict=env_dict)
        
            #os.remove('*.nrrd')
            log_cmd('gzip "%s"/*.nrrd' % QAdir, verbose=verbose, logger=logger, env_dict=env_dict)
#                     if(did_rescale)
#                        os.remove('DTI_rescaled.nii.gz') 
#                     end
            

            log_cmd('$FSLDIR/bin/fslchfiletype NIFTI_GZ "%s"' % QCd_nii_file, verbose=verbose, logger=logger, env_dict=env_dict)
            if do_denoising:
                log_cmd('$FSLDIR/bin/fslchfiletype NIFTI_GZ "%s"' % QCd_nii_denoise_file, verbose=verbose, logger=logger, env_dict=env_dict)
                if imexist(QCd_nii_denoise_file2,'nrrd')[0]:
                    log_cmd('$FSLDIR/bin/fslchfiletype NIFTI_GZ "%s"' % QCd_nii_denoise_file2, verbose=verbose, logger=logger)

        else:
            if do_denoising: #Running denoising via Slicer 4 command line interface
                denoise_case = 'jointLMMSE';
                
                #use --ng 8 to for jointLMMSE using 8 closest Neighbors
                if denoise_case=='LMMSE':
                        nrrd_denoise_file=QCd_nrrd_file.replace('.nrrd','_LMMSE_denoised.nrrd');
                elif denoise_case=='jointLMMSE':
                        nrrd_denoise_file=QCd_nrrd_file.replace('.nrrd','_jointLMMSE_denoised.nrrd');
                        nrrd_denoise_file2=QCd_nrrd_file.replace('.nrrd','_jointLMMSE_denoised_6dir.nrrd');
                elif denoise_case=='UnbiasedNLMeans':   #warning:  MUCH slower than the other options, but better edge preservation.  Took around 6 minutes on the 2mm resolution test data I tried it on
                        nrrd_denoise_file=QCd_nrrd_file.replace('.nrrd','_NLMeans_denoised.nrrd');
                else:
                        raise ValueError("Unknown denoise_case, %s" % denoise_case)
                QCd_nii_denoise_file=nrrd_denoise_file.replace('.nrrd','.nii');
                if nrrd_denoise_file2:
                    QCd_nii_denoise_file2=nrrd_denoise_file2.replace('.nrrd','.nii');
            
        try:
            
            if generate_figures:
                if (not os.path.exists(pjoin(QAdir,'b0avg.png'))) or ForceUpdate:
                    log_cmd('$FSLDIR/bin/slicer -s 2 -a "%s"/b0avg.png "%s"/b0avg' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                if (not os.path.exists(pjoin(QAdir,'DWavg.png'))) or ForceUpdate:
                    log_cmd('$FSLDIR/bin/slicer -s 2 -a "%s"/DWavg.png "%s"/DWavg' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                
                if do_denoising:
                    if (not os.path.exists(pjoin(QAdir,'b0_denoise_avg.png'))) or ForceUpdate:
                        log_cmd('$FSLDIR/bin/slicer -s 2 -a "%s"/b0_denoise_avg.png "%s"/b0_denoise_avg' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    if (not os.path.exists(pjoin(QAdir,'DW_denoise_avg.png'))) or ForceUpdate:
                        log_cmd('$FSLDIR/bin/slicer -s 2 -a "%s"/DW_denoise_avg.png "%s"/DW_denoise_avg' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                    
                    if nrrd_denoise_file2:
                        if(not os.path.exists(pjoin(QAdir,'b0_denoise2_avg.png'))) or ForceUpdate:
                            log_cmd('$FSLDIR/bin/slicer -s 2 -a "%s"/b0_denoise2_avg.png "%s"/b0_denoise2_avg' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                        if(not os.path.exists(pjoin(QAdir,'DW_denoise2_avg.png'))) or ForceUpdate:
                            log_cmd('$FSLDIR/bin/slicer -s 2 -a "%s"/DW_denoise2_avg.png "%s"/DW_denoise2_avg' % (QAdir,QAdir), verbose=verbose, logger=module_logger, env_dict=env_dict)
                if generate_mplot3d_figures:
                    try: 
                        import imp
                        #imp.find_module('mayavi') #can only make this plot if mayavi is installed
                        imp.find_module('mpl_toolkits')  #need mlab3d to make this plot
                        if (not os.path.exists(pjoin(QAdir,'DTI_coverage.png'))) or ForceUpdate:
                            silent_flag=False; #True;  #don't pop up a figure
                            isSiemens=gradient_table_file.find('zflip')!=-1
                            cmind_plot_DTI_coverage(bvec_file,QCd_bvec_file,pjoin(QAdir,'DTI_coverage.png'),isSiemens,silent_flag);
                    except ImportError:
                        warnings.warn("cmind_plot_DTI_coverage failed to generate 3D figures")
        except:
            warnings.warn("cmind_DTI_HARDI_QA failed to generate figures")
            
            #%Not sure how to get the proper scale factor out of the NRRD output, so skip this for now
            #%                         nii1=load_niigz('b0avg.nii.gz');
            #%                         QCd_FA_file=QCd_nrrd_file.replace('.nrrd','_DTI_FA.nrrd');
            #%                         if(exist(QCd_FA_file,'file'))
            #%                             [FA meta]=nrrdread(QCd_FA_file);
            #%                             nii_type=16;
            #%                             cal_range=[0 1];
            #%                             cmind_save_nii_mod_header(nii1,FA,'FA.nii',nii_type,cal_range)
            #%                         end
            #%                         QCd_MD_file=QCd_nrrd_file.replace('.nrrd','_DTI_MD.nrrd');
            #%                         if(exist(QCd_MD_file,'file'))
            #%                             [MD meta]=nrrdread(QCd_MD_file);
            #%                             nii_type=16;
            #%                             cmind_save_nii_mod_header(nii1,MD,'MD.nii',nii_type)
            #%                         end
            #%                         log_cmd('$FSLDIR/bin/slicer -s 2 -i 0 1 -a FA.png FA', verbose=verbose, logger=logger)
            #%                         log_cmd('$FSLDIR/bin/slicer -s 2 -a MD.png MD', verbose=verbose, logger=logger)
            #
    return (QCd_nii_file,QCd_bval_file,QCd_bvec_file)


def _testme():  #TODO: fix this test case to use /cmind/data dir
    import os
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger

    from cmind.globals import (cmind_example_dir, cmind_example_output_dir, 
                               cmind_DTIPrep, CMIND_DTIHARDI_DIR, 
                               cmind_Slicer3D,
                               cmind_Slicer3D_cli_dir)

    case_to_run='DTI_QA_DN_process'
        
  
    DTIPrep_command=cmind_DTIPrep
    cmind_DTI_path=CMIND_DTIHARDI_DIR
    #LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SLICER4_HOME/lib/Slicer-4.2/lib/cli-modules/
    #LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SLICER4_HOME/lib/Slicer-4.2/lib/
    #LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SLICER4_HOME/lib/Slicer-4.2/
    #DICOM_dir='/media/Data2/CMIND/UCLA_09M004_P_1/DTI_68_B1000_22/'\
    #DICOM_dir='/media/Data2/CMIND/IRC04H_06M008_P_1/2001_WIP_DTI_68_iso_32chSHC_SENSE_012120717050376706/'
    #DTI_input_source=DICOM_dir
    DICOM_dir=None
        
    DTIPrep_xmlProtocol=pjoin(cmind_DTI_path,'CMIND_DTIPrep_Protocol_noBrainMask.xml')    
    gradient_table_file=pjoin(cmind_DTI_path,'Philips_LAS_bvects.txt')
    
    
    isSiemens=False;
    isOblique=False;
    Slicer_path=cmind_Slicer3D
    
    
    DWIConvert_command=pjoin(cmind_Slicer3D_cli_dir,'DWIConvert')
    #DWIConvert_command='/home/lee8rx/local_libs/bin/DTIPrep_etc/bin/DWIConvert'  #TODO: add to C-MIND globals
    Slicer_denoise_module=pjoin(cmind_Slicer3D_cli_dir,'DWIRicianLMMSEFilter')
    if not os.path.isfile(Slicer_denoise_module):
        raise ValueError("Slicer_denoise_module not found at: {}".format(Slicer_denoise_module))
    ForceUpdate=False
    verbose=True
    logger=None
    denoise_case='jointLMMSE'

    if verbose:
        func=cmind_timer(cmind_DTI_HARDI_QA,logger=logger)
    else:
        func=cmind_DTI_HARDI_QA

    Flip_ap=False

    cases=['DTI','HARDI']
    for DTIcase in cases:
        if DTIcase=='DTI':
            DTI_input_source=pjoin(cmind_example_dir,'IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1.nii.gz')
            output_dir=pjoin(cmind_example_output_dir,'P','DTI')
            gradient_bval_file=pjoin(cmind_DTI_path,'Philips_LAS_bvals_DTI.txt')
        elif DTIcase=='HARDI':
            DTI_input_source=pjoin(cmind_example_dir,'IRC04H_06M008_F_1_WIP_HARDI_b3000_SENSE_12_1.nii.gz')
            output_dir=pjoin(cmind_example_output_dir,'F','HARDI')
            gradient_bval_file=pjoin(cmind_DTI_path,'Philips_LAS_bvals_HARDI.txt')
        else:
            raise ValueError("Unknown test case")

        func(DTI_input_source,gradient_table_file,gradient_bval_file,
            output_dir,case_to_run,DTIPrep_command,DWIConvert_command,DTIPrep_xmlProtocol,
            Flip_ap=Flip_ap, isSiemens=isSiemens,isOblique=isOblique,Slicer_path=Slicer_path,
            Slicer_denoise_module=Slicer_denoise_module,ForceUpdate=ForceUpdate,
            denoise_case=denoise_case, verbose=verbose, logger=logger, env_dict=cmind_Slicer3D_env)


if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer
    from cmind.globals import cmind_Slicer3D_env

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
        
    else:
        parser = argparse.ArgumentParser(description="Utility to run DTIPrep for quality control of DTI/HARDI image series", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("DTI_input_source", help="raw, 4D DTI nifti-1 volumes (can be gzipped); Alternatively, this can point to a DICOM directory")
        parser.add_argument("gradient_table_file", help="three column text file containing the pre-QC diffusion direction vectors")
        parser.add_argument("gradient_bval_file", help="text file containing the pre-QC b values")
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("case_to_run", help="{'DTI_QA_DN_process','HARDI_QA_DN_process','DTI_QA_process','HARDI_QA_process'} choose which processing case to run ('DN' in names means use 3D slicer to denoise')")
        parser.add_argument("DTIPrep_command", help="path to DTIPrep")
        parser.add_argument("DWIConvert_command", help="path to DWIConvert (used for NIFTI<->NRRD conversions)")
        parser.add_argument("DTIPrep_xmlProtocol", help="path to the xml protocol to be used by DTIPrep")
        parser.add_argument("Flip_ap", help="currently must be False")
        parser.add_argument("isSiemens", help="currently must be False")
        parser.add_argument("isOblique", help="currently must be False")
        parser.add_argument("Slicer_path", help="path to 3D Slicer, must be specified if `case_to_run` contains 'DN'")
        parser.add_argument("Slicer_denoise_module", help="path to the 3D Slicer denoising module to use, must be specified if `case_to_run` contains 'DN'")
        #parser.add_argument("--fsl_motion_outiliers_v2_cmd", type=str, default="", help='full path to fsl_motion_outliers_v2 shell script')
        #parser.add_argument("generate_figures", type=bool, help="generate images of fits")
        #parser.add_argument("ForceUpdate", type=bool, help="Overwrite any existing fits that may reside in the data directory")
        #parser.add_argument("-gf","--generate_figures", action="store_true", default=True, help="generate images of fits")
        #parser.add_argument("-u","--ForceUpdate", action="store_true", default=True, help="Overwrite any existing fits that may reside in the data directory")
        parser.add_argument("--denoise_case", type=str, default='jointLMMSE', help="{'LMMSE','jointLMMSE','UnbiasedNLMeans'}; the denoising case corresponding to Slicer_denoise_module.  recommend: jointLMMSE")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()

        DTI_input_source=args.DTI_input_source
        gradient_table_file=args.gradient_table_file
        gradient_bval_file=args.gradient_bval_file
        output_dir=args.output_dir
        case_to_run=args.case_to_run
        DTIPrep_command=args.DTIPrep_command
        DWIConvert_command=str2none(args.DWIConvert_command)
        DTIPrep_xmlProtocol=args.DTIPrep_xmlProtocol
        Flip_ap=args.Flip_ap
        isSiemens=args.isSiemens
        isOblique=args.isOblique
        Slicer_path=str2none(args.Slicer_path)
        Slicer_denoise_module=str2none(args.Slicer_denoise_module)
        denoise_case=args.denoise_case
        ForceUpdate=args.ForceUpdate
        verbose=args.verbose
        logger=args.logge
            
        if verbose:
            cmind_DTI_HARDI_QA=cmind_timer(cmind_DTI_HARDI_QA,logger=logger)

        cmind_DTI_HARDI_QA(DTI_input_source,gradient_table_file,gradient_bval_file,
            output_dir,case_to_run,DTIPrep_command,DWIConvert_command,DTIPrep_xmlProtocol,
            Flip_ap,isSiemens,isOblique,Slicer_path,Slicer_denoise_module,ForceUpdate=ForceUpdate,
            denoise_case=denoise_case, verbose=verbose, logger=logger, env_dict=cmind_Slicer3_env)

