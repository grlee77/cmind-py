import os
from nipype.interfaces.base import (traits,BaseInterface, BaseInterfaceInputSpec, TraitedSpec, isdefined) #, File


class cmind_DTI_HARDI_QAInputSpec(BaseInterfaceInputSpec):
    DTI_input_source = traits.String(mandatory=True, position=0, desc="raw, 4D DTI nifti-1 volumes (can be gzipped)")
    gradient_table_file = traits.String(mandatory=True, position=1, desc="three column text file containing the pre-QC diffusion direction vectors")
    gradient_bval_file = traits.String(mandatory=True, position=2, desc="text file containing the pre-QC b values")
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    case_to_run = traits.Enum("DTI_QA_DN_process","DTI_QA_process","HARDI_QA_process","HARDI_QA_DN_process",mandatory=True, position=4, desc="choose which processing case to run. 'DN' in names means use 3D slicer")
    DTIPrep_command = traits.String(mandatory=True, position=5, desc="path to DTIPrep")
    DTIPrep_xmlProtocol = traits.Any(mandatory=True, position=6, desc="path to the xml protocol to be used by DTIPrep")
    DWIConvert_command = traits.Any(mandatory=False, desc="path to DWIConvert (used for NIFTI<->NRRD conversions)")
    isSiemens = traits.Bool(mandatory=False, desc="currently must be False")
    isOblique = traits.Bool(mandatory=False, desc="currently must be False")
    Slicer_path = traits.Any(mandatory=False, desc="path to 3D Slicer, must be specified if `case_to_run` contains 'DN'")
    Slicer_denoise_module = traits.Any(mandatory=False, desc="path to the 3D Slicer denoising module to use, must be specified if ")
    denoise_case = traits.Enum("jointLMMSE","LMMSE","UnbiasedNLMeans",mandatory=False, desc="the denoising case corresponding to `Slicer_denoise_module`")
    generate_figures = traits.Bool(mandatory=False, desc="if True, generate summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")
    env_dict = traits.DictStrAny(mandatory=False, desc="dictionary of environment variables to add to os.environ when calling ")


class cmind_DTI_HARDI_QAOutputSpec(TraitedSpec):
    QCd_nii_file = traits.File(exists=True,desc="filename of Quality Controled DTI/HARDI 4D volume")
    QCd_bval_file = traits.File(exists=True,desc="filename of b-values corresponding to the data in QCd_nii_file")
    QCd_bvec_file = traits.File(exists=True,desc="filename of b-vectors corresponding to the data in QCd_nii_file")


class cmind_DTI_HARDI_QA(BaseInterface):
    input_spec=cmind_DTI_HARDI_QAInputSpec
    output_spec=cmind_DTI_HARDI_QAOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_DTI_HARDI_QA as DTI_HARDI_QA
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._QCd_nii_file,self._QCd_bval_file,self._QCd_bvec_file) = DTI_HARDI_QA(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["QCd_nii_file"]=self._QCd_nii_file
        outputs["QCd_bval_file"]=self._QCd_bval_file
        outputs["QCd_bvec_file"]=self._QCd_bvec_file
        return outputs


class cmind_DWI_TRK_to_3planeInputSpec(BaseInterfaceInputSpec):
    trackfile = traits.String(mandatory=True, position=0, desc="filename of the .trk file to plot")
    output_image_prefix = traits.String(mandatory=True, position=1, desc="full path of the output image, minus the image extension")
    track_lengths = traits.Int(mandatory=False, desc="only display tracks > track_length.  If this is a list, generate separate")
    add_length_label = traits.Bool(mandatory=False, desc="If true, the left margin of the image will contain text annotating the lengths")
    figure_title = traits.String(mandatory=False, desc="Title to put at top of image")
    image_scale_pct = traits.Int(mandatory=False, desc="rescale the rendered images by this amount before making the 3-plane image")
    extra_options = traits.String(mandatory=False, desc="string containing extra options to be passed on to track_vis.  ")
    stack_length_images = traits.Bool(mandatory=False, desc="If this is true, stack the images corresponding to each length vertically")
    remove_intermediate_images = traits.Bool(mandatory=False, desc="If this is true, any intermediate images will be removed and only the")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_DWI_TRK_to_3planeOutputSpec(TraitedSpec):
    all_output_images = traits.List(exists=True,desc="list of images generated")


class cmind_DWI_TRK_to_3plane(BaseInterface):
    input_spec=cmind_DWI_TRK_to_3planeInputSpec
    output_spec=cmind_DWI_TRK_to_3planeOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_DWI_TRK_to_3plane as DWI_TRK_to_3plane
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._all_output_images) = DWI_TRK_to_3plane(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["all_output_images"]=self._all_output_images
        return outputs


class cmind_T1_tissue_averagesInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    T1_map_file = traits.String(mandatory=True, position=1, desc="image file containing the T1map fitting results")
    NRMSE_file = traits.String(mandatory=True, position=2, desc="normalized root-mean-square error (NRMSE) image from T1_map fit")
    GMpve = traits.String(mandatory=True, position=3, desc="GM partial volume estimate from structural processing")
    WMpve = traits.String(mandatory=True, position=4, desc="WM partial volume estimate from structural processing")
    CSFpve = traits.String(mandatory=True, position=5, desc="CSF partial volume estimate from structural processing")
    CBF_quant_file = traits.Any(mandatory=False, desc="quantitative ASL image")
    CBF_percent_file = traits.Any(mandatory=False, desc="ASL control-tag signal percentage image")
    HRtoCBF_affine = traits.Any(mandatory=False, desc="affine transform:  structural->BaselineCBF")
    HRtoCBF_warp = traits.Any(mandatory=False, desc="warp:  structural->BaselineCBF")
    PVEthresh_pct = traits.Float(mandatory=False, desc="threshold PVE maps by this percentage.  Valid range: (0 100]")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_T1_tissue_averagesOutputSpec(TraitedSpec):
    T1_avg_file = traits.String(exists=True,desc="")
    CBF_avg_file = traits.Any(exists=True,desc="")


class cmind_T1_tissue_averages(BaseInterface):
    input_spec=cmind_T1_tissue_averagesInputSpec
    output_spec=cmind_T1_tissue_averagesOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_T1_tissue_averages as T1_tissue_averages
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._T1_avg_file,self._CBF_avg_file) = T1_tissue_averages(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["T1_avg_file"]=self._T1_avg_file
        outputs["CBF_avg_file"]=self._CBF_avg_file
        return outputs


class cmind_T1map_fitInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    T1concat_file = traits.String(mandatory=True, position=1, desc="file containing the 4D concatenated T1EST files (from cmind_T1map_preprocess.py)")
    TIvals_file = traits.String(mandatory=True, position=2, desc="text file listing the TI times corresponding to the images in T1concat_file")
    mask_file = traits.String(mandatory=False, desc="logical ndarray of location over which to perform the fit")
    auto_discard = traits.Bool(mandatory=False, desc="if True, first fit a small subset of voxels and detect any TI points that")
    nmax_discards = traits.Int(mandatory=False, desc="maximum number of outliers that will be allowed to be discarded via")
    show_plots = traits.Bool(mandatory=False, desc="if False, suppress onscreen display of the plotted results")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_T1map_fitOutputSpec(TraitedSpec):
    T1_map_fname = traits.String(exists=True,desc="filename of the T1 map image")
    I0_map_fname = traits.String(exists=True,desc="filename of the I0 map (M0) image")
    T1_NRMSE_map_fname = traits.String(exists=True,desc="filename of the normalized root mean squared error of the fits image")


class cmind_T1map_fit(BaseInterface):
    input_spec=cmind_T1map_fitInputSpec
    output_spec=cmind_T1map_fitOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_T1map_fit as T1map_fit
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._T1_map_fname,self._I0_map_fname,self._T1_NRMSE_map_fname) = T1map_fit(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["T1_map_fname"]=self._T1_map_fname
        outputs["I0_map_fname"]=self._I0_map_fname
        outputs["T1_NRMSE_map_fname"]=self._T1_NRMSE_map_fname
        return outputs


class cmind_T1map_preprocessInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    T1EST_nii_folder = traits.Either(traits.String, traits.List(traits.String), traits.List(traits.Tuple), mandatory=True, position=1, desc="folder containing all of the T1EST NIFTI-GZ files or a list of tuples where the first")
    T1EST_glob = traits.String(mandatory=False, desc="if this string is not empty, use this as the file pattern to match within")
    ANTS_bias_cmd = traits.String(mandatory=False, desc="path to the ANTs bias correction binary")
    do_motion_cor = traits.Bool(mandatory=False, desc="coregister the images from the different TI times")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_T1map_preprocessOutputSpec(TraitedSpec):
    T1concat_mcf = traits.String(exists=True,desc="concatenated T1EST images after (optional) motion correction")
    TIvals_file = traits.String(exists=True,desc="text file containing the TI values")
    T1concat_mean_N4 = traits.String(exists=True,desc="output volume containing mean over T1concat_mcf")
    T1concat_mean_N4_brain = traits.String(exists=True,desc="T1concat_mean_N4 after brain extraction")
    T1concat_mean_N4_mask = traits.String(exists=True,desc="mask corresponding to T1concat_mean_N4_brain")


class cmind_T1map_preprocess(BaseInterface):
    input_spec=cmind_T1map_preprocessInputSpec
    output_spec=cmind_T1map_preprocessOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_T1map_preprocess as T1map_preprocess
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._T1concat_mcf,self._TIvals_file,self._T1concat_mean_N4,self._T1concat_mean_N4_brain,self._T1concat_mean_N4_mask) = T1map_preprocess(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["T1concat_mcf"]=self._T1concat_mcf
        outputs["TIvals_file"]=self._TIvals_file
        outputs["T1concat_mean_N4"]=self._T1concat_mean_N4
        outputs["T1concat_mean_N4_brain"]=self._T1concat_mean_N4_brain
        outputs["T1concat_mean_N4_mask"]=self._T1concat_mean_N4_mask
        return outputs


class cmind_age_to_templateInputSpec(BaseInterfaceInputSpec):
    age_months = traits.Float(mandatory=True, position=0, desc="age of subject in months")
    ped_template_path = traits.Directory(mandatory=True, position=1, desc="path to the directory containing the pediatric templates")
    output_dir = traits.String(mandatory=False, desc="output directory in which to store reg_struct.csv")
    resolution_str = traits.Enum("1mm","2mm",mandatory=False, desc="resolution of the templates to use (default = '2mm')")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_age_to_templateOutputSpec(TraitedSpec):
    reg_struct = traits.DictStrAny(exists=True,desc="dictionary of registration related atlases and transforms")
    reg_struct_file = traits.String(exists=True,desc="filename of the .csv file containing reg_struct")


class cmind_age_to_template(BaseInterface):
    input_spec=cmind_age_to_templateInputSpec
    output_spec=cmind_age_to_templateOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_age_to_template as age_to_template
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._reg_struct,self._reg_struct_file) = age_to_template(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["reg_struct"]=self._reg_struct
        outputs["reg_struct_file"]=self._reg_struct_file
        return outputs


class cmind_alphaInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory to store output")
    alpha_complex_nii = traits.String(mandatory=True, position=1, desc="filename to the complex alpha .nii.gz volume")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_alphaOutputSpec(TraitedSpec):
    alpha_fname = traits.String(exists=True,desc="text file containing the computed alpha value")
    alpha = traits.Float(exists=True,desc="alpha value")


class cmind_alpha(BaseInterface):
    input_spec=cmind_alphaInputSpec
    output_spec=cmind_alphaOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_alpha as alpha
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._alpha_fname,self._alpha) = alpha(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["alpha_fname"]=self._alpha_fname
        outputs["alpha"]=self._alpha
        return outputs


class cmind_apply_normalizationInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    reg_struct = traits.Either(traits.String, traits.DictStrAny, mandatory=True, position=1, desc="filename of the .csv file containing the registration dictionary")
    input_name = traits.Either(traits.String, traits.List(traits.String), mandatory=True, position=2, desc="image filename for the 'moving' image")
    output_name = traits.Either(traits.String, traits.List(traits.String), mandatory=True, position=3, desc="image filename for the output")
    LRtoHR_affine = traits.Any(mandatory=False, desc="affine transform from functional to structural space.")
    LRtoHR_warp = traits.Any(mandatory=False, desc="fieldmap warp correction for functional space image")
    ANTS_path = traits.String(mandatory=False, desc="path to ANTs binaries")
    Norm_Target = traits.Enum("MNI","StudyTemplate","Structural",mandatory=False, desc="Normalization target")
    reg_flag = traits.Either(traits.String, traits.Int, mandatory=False, desc="3 character string such as 101 controlling which registration to perform")
    interp_type = traits.Either(traits.String, traits.List(traits.String), mandatory=False, desc="{'spline','bspline'} call spline based interpolation")
    is_timeseries = traits.Bool(mandatory=False, desc="if true, input_name corresponds to timeseries data.  the transformation will")
    generate_coverage = traits.Either(traits.Bool, traits.List(traits.Bool), mandatory=False, desc="if true, generate binary masks of the slice coverage.  can be an array")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate overlay images summarizing the registration")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_apply_normalizationOutputSpec(TraitedSpec):
    all_output_names = traits.List(exists=True,desc="list of all registered image filenames")


class cmind_apply_normalization(BaseInterface):
    input_spec=cmind_apply_normalizationInputSpec
    output_spec=cmind_apply_normalizationOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_apply_normalization as apply_normalization
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._all_output_names) = apply_normalization(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["all_output_names"]=self._all_output_names
        return outputs


class cmind_atlas_ROI_averagesInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    GMpve = traits.Either(traits.String, traits.List(traits.String), mandatory=True, position=1, desc="GM partial volume estimate in subject space")
    WMpve = traits.Either(traits.String, traits.List(traits.String), mandatory=True, position=2, desc="WM partial volume estimate in subject space")
    CSFpve = traits.Either(traits.String, traits.List(traits.String), mandatory=True, position=3, desc="CSF partial volume estimate in subject space")
    atlas_vol = traits.String(mandatory=True, position=4, desc="AAL atlas registered to subject space")
    CBF_quant_vol = traits.Any(mandatory=False, desc="quantitative ASL image in subject space")
    T1_quant_vol = traits.Any(mandatory=False, desc="quantitative T1 map image in subject space")
    DeepGMpve = traits.Either(traits.String, traits.List(traits.String), mandatory=False, desc="Deep GM partial volume estimate in subject space")
    ROI_labels_csv = traits.String(mandatory=False, desc=".csv file with integers in first column and corresponding labels in 2nd column.  ")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_atlas_ROI_averagesOutputSpec(TraitedSpec):
    dataframe_csvfile = traits.String(exists=True,desc="pandas dataframe stored as a .csv file")


class cmind_atlas_ROI_averages(BaseInterface):
    input_spec=cmind_atlas_ROI_averagesInputSpec
    output_spec=cmind_atlas_ROI_averagesOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_atlas_ROI_averages as atlas_ROI_averages
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._dataframe_csvfile) = atlas_ROI_averages(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["dataframe_csvfile"]=self._dataframe_csvfile
        return outputs


class cmind_atlas_to_subjectInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    atlas_vol = traits.Either(traits.String, traits.List(traits.String), mandatory=True, position=1, desc="filename of the NIFTI(GZ) format atlas")
    ref_vol = traits.String(mandatory=True, position=2, desc="image filename for the 'fixed' image (registration target)")
    reg_struct = traits.Either(traits.String, traits.DictStrAny, mandatory=True, position=3, desc="filename of the .csv file containing the registration dictionary")
    ANTS_path = traits.String(mandatory=False, desc="path to ANTs binaries")
    omit_MNItoHR = traits.Bool(mandatory=False, desc="if True assume the atlas is already in structural space")
    interp_type = traits.Enum("HammingWindowedSinc","","BSpline[3]","NearestNeighbor",mandatory=False, desc="interpolation to use (should be nearest neighbor for binary atlases)")
    output_prefix = traits.Either(traits.String, traits.List(traits.String), mandatory=False, desc="prepend this string to the output filenames (default = 'Atlas')")
    split_script = traits.Any(mandatory=False, desc="file name of the shell script that splits all masks within an atlas to separate volumes")
    HRtoLR_Affine = traits.Any(mandatory=False, desc="affine transform from MNI standard space to functional space")
    HRtoLR_Warp = traits.Any(mandatory=False, desc="fieldmap warp correction for transform from MNI standard space to functional space")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_atlas_to_subjectOutputSpec(TraitedSpec):
    Atlas_out = traits.Either(traits.String, traits.List(traits.String), exists=True,desc="filename of the atlas in subject space")
    out1 = traits.String(exists=True,desc="1st output ")
    out2 = traits.Any(exists=True,desc="2jd output, if it exists")
    out3 = traits.Any(exists=True,desc="3rd output, if it exists")
    out4 = traits.Any(exists=True,desc="4th output, if it exists")
    out5 = traits.Any(exists=True,desc="5th output, if it exists")
    out6 = traits.Any(exists=True,desc="6th output, if it exists")


class cmind_atlas_to_subject(BaseInterface):
    input_spec=cmind_atlas_to_subjectInputSpec
    output_spec=cmind_atlas_to_subjectOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_atlas_to_subject as atlas_to_subject
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._Atlas_out,self._out1,self._out2,self._out3,self._out4,self._out5,self._out6) = atlas_to_subject(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["Atlas_out"]=self._Atlas_out
        outputs["out1"]=self._out1
        outputs["out2"]=self._out2
        outputs["out3"]=self._out3
        outputs["out4"]=self._out4
        outputs["out5"]=self._out5
        outputs["out6"]=self._out6
        return outputs


class cmind_baselineCBF_coregisterT1InputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    CBF_nii = traits.String(mandatory=True, position=1, desc="filename of Baseline CBF volume to register to")
    T1concat_N4_brain_nii = traits.String(mandatory=True, position=2, desc="filename containing the brain extracted 4D TI image series used during ")
    T1_map_nii = traits.String(mandatory=True, position=3, desc="filename containing the T1_map volume to register")
    I0_map_nii = traits.String(mandatory=True, position=4, desc="filename containing the I0_map (M0) volume to register")
    T1_NRMSE_map_nii = traits.Any(mandatory=False, desc="filename containing the T1 fit NRMSE_map volume to register")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_baselineCBF_coregisterT1OutputSpec(TraitedSpec):
    T1map_reg2CBF = traits.String(exists=True,desc="filename of the T1_map volume that has been registered to `CBF_nii`")
    I0map_reg2CBF = traits.String(exists=True,desc="filename of the I0_map volume that has been registered to `CBF_nii`")
    T1_NRMSE_map_reg2CBF = traits.Any(exists=True,desc="filename of the NRMSE map that has been registered to `CBF_nii`")
    T1map2CBF_affine = traits.String(exists=True,desc="affine transform of the T1_map to the BaselineCBF image")


class cmind_baselineCBF_coregisterT1(BaseInterface):
    input_spec=cmind_baselineCBF_coregisterT1InputSpec
    output_spec=cmind_baselineCBF_coregisterT1OutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_baselineCBF_coregisterT1 as baselineCBF_coregisterT1
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._T1map_reg2CBF,self._I0map_reg2CBF,self._T1_NRMSE_map_reg2CBF,self._T1map2CBF_affine) = baselineCBF_coregisterT1(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["T1map_reg2CBF"]=self._T1map_reg2CBF
        outputs["I0map_reg2CBF"]=self._I0map_reg2CBF
        outputs["T1_NRMSE_map_reg2CBF"]=self._T1_NRMSE_map_reg2CBF
        outputs["T1map2CBF_affine"]=self._T1map2CBF_affine
        return outputs


class cmind_baselineCBF_preprocessInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    CBF_nii = traits.String(mandatory=True, position=1, desc="nifti image containing the 4D ASL timeseries")
    max_percent_diff = traits.Float(mandatory=False, desc="any control-tag frames greater than this percentage will be automatically")
    ANTS_bias_cmd = traits.String(mandatory=False, desc="path to the bias field correction command to use.")
    UCLA_flag = traits.Bool(mandatory=False, desc="if True, assumes order is 'tag','control','tag',...")
    fsl_motion_outliers_cmd = traits.String(mandatory=False, desc="path to the fsl_motion_outliers_cmd_v2 shell script.  If not specified")
    discard_outliers = traits.Bool(mandatory=False, desc="unless False, will detect and discard outliers")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_baselineCBF_preprocessOutputSpec(TraitedSpec):
    BaselineCBF_mcf_percent_change = traits.String(exists=True,desc="filename of the average control-tag percent change image")
    BaselineCBF_mcf_control_mean_N4 = traits.String(exists=True,desc="filename of the average N4 bias-corrected control image")
    BaselineCBF_Meansub = traits.String(exists=True,desc="filename of the average N4 bias-corrected control-tag image")
    BaselineCBF_timepoints_kept = traits.String(exists=True,desc="textfile listing the control-tag timepoints that were kept")
    BaselineCBF_mcf_masked = traits.String(exists=True,desc="filename of the ASL subtracted timeseries")
    BaselineCBF_N4Bias = traits.String(exists=True,desc="filename of the N4 bias field computed from the mean control image")
    BaselineCBF_relmot = traits.String(exists=True,desc="filename of the relative motion parameters during coregistration (motion-correction)")


class cmind_baselineCBF_preprocess(BaseInterface):
    input_spec=cmind_baselineCBF_preprocessInputSpec
    output_spec=cmind_baselineCBF_preprocessOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_baselineCBF_preprocess as baselineCBF_preprocess
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._BaselineCBF_mcf_percent_change,self._BaselineCBF_mcf_control_mean_N4,self._BaselineCBF_Meansub,self._BaselineCBF_timepoints_kept,self._BaselineCBF_mcf_masked,self._BaselineCBF_N4Bias,self._BaselineCBF_relmot) = baselineCBF_preprocess(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["BaselineCBF_mcf_percent_change"]=self._BaselineCBF_mcf_percent_change
        outputs["BaselineCBF_mcf_control_mean_N4"]=self._BaselineCBF_mcf_control_mean_N4
        outputs["BaselineCBF_Meansub"]=self._BaselineCBF_Meansub
        outputs["BaselineCBF_timepoints_kept"]=self._BaselineCBF_timepoints_kept
        outputs["BaselineCBF_mcf_masked"]=self._BaselineCBF_mcf_masked
        outputs["BaselineCBF_N4Bias"]=self._BaselineCBF_N4Bias
        outputs["BaselineCBF_relmot"]=self._BaselineCBF_relmot
        return outputs


class cmind_baselineCBF_quantitativeInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    BaselineCBF_mcf_masked = traits.String(mandatory=True, position=1, desc="masked, motion-corrected BaselineCBF volume")
    N4BiasCBF = traits.String(mandatory=True, position=2, desc="BaselineCBF N4Bias field")
    BaselineCBF_timepoints_kept = traits.String(mandatory=True, position=3, desc="Timepoints kept during BaselineCBF preprocessing")
    alpha_file = traits.Either(traits.Float, traits.String, mandatory=True, position=4, desc="inversion efficiency or a text file containing the inversion efficiency")
    WMpve = traits.String(mandatory=True, position=5, desc="white matter partial volume estimate")
    T1_map_reg2CBF = traits.Either(traits.String, traits.Float, mandatory=False, desc="T1map that has been registered to BaselineCBF_mcf_masked")
    alpha_const = traits.Float(mandatory=False, desc="if alpha_file was specified, also repeat the CBF mapping using this ")
    HRtoLR_affine = traits.Any(mandatory=False, desc="affine from highres (structural) to lowres space")
    HRtoLR_warp = traits.Any(mandatory=False, desc="warp from highres (structural) to lowres space")
    relmot = traits.String(mandatory=False, desc="relative motion .rms file from mcflirt")
    relmot_thresh = traits.Float(mandatory=False, desc="relative motion threshold (mm) for discarding timepoints")
    cbf_lambda = traits.Float(mandatory=False, desc="blood-brain partition coefficient")
    R1a = traits.Float(mandatory=False, desc="R1 relaxation rate of arterial blood (1/seconds)")
    delta = traits.Float(mandatory=False, desc="transit time (to capillary compartment) (seconds)")
    delta_art = traits.Float(mandatory=False, desc="arterial transit time (seconds)")
    w = traits.Float(mandatory=False, desc="post-labeling delay (seconds)")
    tau = traits.Float(mandatory=False, desc="label duration (seconds) ")
    TR = traits.Float(mandatory=False, desc="TR (seconds)")
    T2star_wm = traits.Float(mandatory=False, desc="transverse relaxation rate of white matter")
    T2star_arterial_blood = traits.Float(mandatory=False, desc="transverse relaxation rate of arterial blood")
    TE = traits.Float(mandatory=False, desc="echo time of the ASL acquisition")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_baselineCBF_quantitativeOutputSpec(TraitedSpec):
    CBF_Wang_file = traits.String(exists=True,desc="filename of the quantitative CBF image using alpha from alpha_file")
    CBF_Wang_alphaconst_file = traits.String(exists=True,desc="filename of the quantitative CBF image using alpha = alpha_const")


class cmind_baselineCBF_quantitative(BaseInterface):
    input_spec=cmind_baselineCBF_quantitativeInputSpec
    output_spec=cmind_baselineCBF_quantitativeOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_baselineCBF_quantitative as baselineCBF_quantitative
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._CBF_Wang_file,self._CBF_Wang_alphaconst_file) = baselineCBF_quantitative(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["CBF_Wang_file"]=self._CBF_Wang_file
        outputs["CBF_Wang_alphaconst_file"]=self._CBF_Wang_alphaconst_file
        return outputs


class cmind_bias_correctInputSpec(BaseInterfaceInputSpec):
    fname_crop = traits.String(mandatory=True, position=0, desc="input volume")
    fname_bias = traits.String(mandatory=True, position=1, desc="filename to use for output volume. if a directory name is given instead, ")
    ANTS_bias_cmd = traits.String(mandatory=False, desc="location of N4BiasFieldCorrection binary")
    oname_root = traits.String(mandatory=False, desc="output filename prefix")
    weight_mask = traits.String(mandatory=False, desc="filename of optional weighting mask for ANTs N4BiasFieldCorrection (see ANTs documentation)")
    N4_shrinkFactor = traits.Int(mandatory=False, desc="resolution reduction factor during N4 computations (default=3)")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_bias_correctOutputSpec(TraitedSpec):
    fname_bias = traits.String(exists=True,desc="Bias corrected head")
    bias_field_vol = traits.String(exists=True,desc="The bias field")


class cmind_bias_correct(BaseInterface):
    input_spec=cmind_bias_correctInputSpec
    output_spec=cmind_bias_correctOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_bias_correct as bias_correct
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._fname_bias,self._bias_field_vol) = bias_correct(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["fname_bias"]=self._fname_bias
        outputs["bias_field_vol"]=self._bias_field_vol
        return outputs


class cmind_brain_extractInputSpec(BaseInterfaceInputSpec):
    fname_bias = traits.String(mandatory=True, position=0, desc="input head volume")
    bet_thresh = traits.Float(mandatory=False, desc="bet brain extraction threshold")
    brain_vol = traits.String(mandatory=False, desc="output volume  (default is to append _brain to the input volume name)")
    extra_AFNI_args = traits.String(mandatory=False, desc="string of extra arguments to pass on to AFNI  (e.g.  '-use_skull')")
    oname_root = traits.String(mandatory=False, desc="output image filename prefix")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")
    use_AFNI = traits.Bool(mandatory=False, desc="if True use AFNI's 3dSkullStrip instead of BET")


class cmind_brain_extractOutputSpec(TraitedSpec):
    brain_vol = traits.String(exists=True,desc="Bias corrected + brain extracted")
    brain_mask = traits.String(exists=True,desc="brain mask")


class cmind_brain_extract(BaseInterface):
    input_spec=cmind_brain_extractInputSpec
    output_spec=cmind_brain_extractOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_brain_extract as brain_extract
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._brain_vol,self._brain_mask) = brain_extract(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["brain_vol"]=self._brain_vol
        outputs["brain_mask"]=self._brain_mask
        return outputs


class cmind_build_feat_dirInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="desired output location for the first-level feat directory structure.")
    preproc_tarfile = traits.String(mandatory=True, position=1, desc="tar file of preprocessing results")
    stats_tarfile = traits.String(mandatory=True, position=2, desc="tar file of stats results")
    reg_tarfile = traits.String(mandatory=True, position=3, desc="tar file of registration to anatomical/structural space")
    reg_standard_tarfile = traits.String(mandatory=True, position=4, desc="tar file of registration to standard space")
    reduce_file_level = traits.Int(mandatory=True, position=5, desc="if 0 include all files from the source .tar.gz.  If 1, minimize the ")
    poststats_tarfile = traits.String(mandatory=False, desc="tar file of first level poststats results (not required for 2nd level analysis)")
    output_tar = traits.String(mandatory=False, desc="output name for .tar.gz (will remove output_dir after compression).  If ")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_build_feat_dirOutputSpec(TraitedSpec):
    output_name = traits.Any(exists=False,desc="will be the name of the generated directory or tar file")


class cmind_build_feat_dir(BaseInterface):
    input_spec=cmind_build_feat_dirInputSpec
    output_spec=cmind_build_feat_dirOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_build_feat_dir as build_feat_dir
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._output_name) = build_feat_dir(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["output_name"]=self._output_name
        return outputs


class cmind_calculate_slice_coverageInputSpec(BaseInterfaceInputSpec):
    inputvol = traits.List(mandatory=True, position=0, desc="list of input volumes to compute slice coverage for")
    standard_maskvol = traits.String(mandatory=True, position=1, desc="MNI brain mask image (e.g. MNI152_T1_2mm_brain_mask.nii.gz)")
    bg_thresh = traits.Float(mandatory=True, position=2, desc="percentage of 95th percentile intensity to mask out as background")
    fill_holes = traits.Bool(mandatory=False, desc="fill holes in binary mask after mask=inputvol>(bg_thresh * p95)")
    output_img = traits.List(mandatory=False, desc="list of images to generate.  If empty, name will be autogenerated based ")
    subjID = traits.String(mandatory=False, desc="subject ID to overlay onto the images generated ")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_calculate_slice_coverageOutputSpec(TraitedSpec):
    all_frac = traits.Array(exists=True,desc="percent slice coverage for each slice in the MNI brain")
    missing_slices = traits.List(exists=True,desc="list of slices that have 25 percent or less coverage")
    Nmissing_top = traits.Int(exists=True,desc="number of slices missing from the top of the brain")


class cmind_calculate_slice_coverage(BaseInterface):
    input_spec=cmind_calculate_slice_coverageInputSpec
    output_spec=cmind_calculate_slice_coverageOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_calculate_slice_coverage as calculate_slice_coverage
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._all_frac,self._missing_slices,self._Nmissing_top) = calculate_slice_coverage(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["all_frac"]=self._all_frac
        outputs["missing_slices"]=self._missing_slices
        outputs["Nmissing_top"]=self._Nmissing_top
        return outputs


class cmind_crop_robustInputSpec(BaseInterfaceInputSpec):
    fname = traits.String(mandatory=True, position=0, desc="input filename")
    fname_crop = traits.String(mandatory=True, position=1, desc="output filename.  if a directory name is given instead, a file named ")
    age_months = traits.Float(mandatory=True, position=2, desc="subject age in months")
    z_crop_mm = traits.Int(mandatory=False, desc="size to crop to in mm.  If specified overrides the automatic setting ")
    oname_root = traits.String(mandatory=False, desc="output filename prefix")
    minthresh = traits.Float(mandatory=False, desc="percentage threshold below which to consider intensities to be noise.")
    fname2 = traits.String(mandatory=False, desc="second input file to apply the identical fslroi crop to")
    fname2_crop = traits.String(mandatory=False, desc="output filename for the second input")
    oname2_root = traits.String(mandatory=False, desc="output filename prefix for fname2")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_crop_robustOutputSpec(TraitedSpec):
    fname_crop = traits.String(exists=True,desc="filename of the cropped image")


class cmind_crop_robust(BaseInterface):
    input_spec=cmind_crop_robustInputSpec
    output_spec=cmind_crop_robustOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_crop_robust as crop_robust
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._fname_crop) = crop_robust(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["fname_crop"]=self._fname_crop
        return outputs


class cmind_fMRI_level1_poststatsInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    prep_dir = traits.String(mandatory=True, position=1, desc="specify location where cmind_fMRI_preprocess2.py output is located")
    stats_dir = traits.String(mandatory=True, position=2, desc="specify location where stats output is already located")
    lvl1_stats_case = traits.String(mandatory=False, desc="may be used in future to apply different processing to different cases")
    feat_params = traits.String(mandatory=False, desc="filename to a .csv file containing feat parameters listed as key,value ")
    z_thresh = traits.Float(mandatory=False, desc="initial z threshold to use when determining clusters.  ")
    prob_thresh = traits.Float(mandatory=False, desc="probability threshold for voxelwise or cluster significance")
    thresh = traits.Enum("cluster","uncorrected","voxel",mandatory=False, desc="0 = uncorrected, 1 = voxel-based (FWE corrected), 2 = cluster-based")
    output_tar = traits.Bool(mandatory=False, desc="if true, generate .tar.gz versions of stats and poststats folders")
    CMIND_specific_figures = traits.Bool(mandatory=False, desc="generate additional figures for the CMIND paradigm results.  set this to False if the data is not from the CMIND project")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate overlay images summarizing the registration")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fMRI_level1_poststatsOutputSpec(TraitedSpec):
    poststats_tarfile = traits.Any(exists=True,desc="filename of the .tar.gz file containing the poststats output")
    poststats_dir = traits.Any(exists=True,desc="poststsats output folder")


class cmind_fMRI_level1_poststats(BaseInterface):
    input_spec=cmind_fMRI_level1_poststatsInputSpec
    output_spec=cmind_fMRI_level1_poststatsOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fMRI_level1_poststats as fMRI_level1_poststats
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._poststats_tarfile,self._poststats_dir) = fMRI_level1_poststats(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["poststats_tarfile"]=self._poststats_tarfile
        outputs["poststats_dir"]=self._poststats_dir
        return outputs


class cmind_fMRI_level1_statsInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    FSF_template = traits.String(mandatory=True, position=1, desc="design.fsf template filename")
    prep_dir = traits.String(mandatory=True, position=2, desc="specify location where cmind_fMRI_preprocess2.py output is located")
    stats_dir = traits.String(mandatory=False, desc="specify location where stats output should go (or is already located)")
    lvl1_stats_case = traits.String(mandatory=False, desc="may be used in future to apply different processing to different cases")
    nuisance_file = traits.String(mandatory=False, desc="path to a text file with one nuisance regressor per column")
    feat_params = traits.String(mandatory=False, desc="filename to a .csv file containing feat parameters listed as key,value ")
    z_thresh = traits.Float(mandatory=False, desc="initial z threshold to use when determining clusters.  ")
    prob_thresh = traits.Float(mandatory=False, desc="probability threshold for voxelwise or cluster significance")
    thresh = traits.Enum("0","1","2",mandatory=False, desc="0 = uncorrected, 1 = voxel-based (FWE corrected), 2 = cluster-based")
    output_tar = traits.Bool(mandatory=False, desc="if true, generate .tar.gz versions of stats and poststats folders")
    CMIND_specific_figures = traits.Bool(mandatory=False, desc="generate additional figures for the CMIND paradigm results.  set this to False if the data is not from the CMIND project")
    omit_poststats = traits.Bool(mandatory=False, desc="it True, run stats only without poststats thresholding")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate overlay images summarizing the registration")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fMRI_level1_statsOutputSpec(TraitedSpec):
    stats_tarfile = traits.Any(exists=True,desc="filename of the .tar.gz file containing the stats output")
    poststats_tarfile = traits.Any(exists=True,desc="filename of the .tar.gz file containing the poststats output")
    output_dir = traits.String(exists=True,desc="Feat directory where the analysis was performed")
    stats_dir = traits.String(exists=True,desc="stats output folder")
    poststats_dir = traits.Any(exists=True,desc="poststsats output folder")


class cmind_fMRI_level1_stats(BaseInterface):
    input_spec=cmind_fMRI_level1_statsInputSpec
    output_spec=cmind_fMRI_level1_statsOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fMRI_level1_stats as fMRI_level1_stats
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._stats_tarfile,self._poststats_tarfile,self._output_dir,self._stats_dir,self._poststats_dir) = fMRI_level1_stats(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["stats_tarfile"]=self._stats_tarfile
        outputs["poststats_tarfile"]=self._poststats_tarfile
        outputs["output_dir"]=self._output_dir
        outputs["stats_dir"]=self._stats_dir
        outputs["poststats_dir"]=self._poststats_dir
        return outputs


class cmind_fMRI_level2_statsInputSpec(BaseInterfaceInputSpec):
    group_output_dir = traits.String(mandatory=True, position=0, desc="group analysis output directory")
    Feat_Directory_List = traits.Either(traits.List(traits.String), traits.String, mandatory=True, position=1, desc="list of the 1st-level Feat directories or the name of a text file containing this list")
    run_feat = traits.Bool(mandatory=False, desc="if True, call feat to run the generated the fsf file")
    analysis_type = traits.Enum("fixed","ols","flame1","flame2",mandatory=False, desc="type of 2nd level analysis to perform")
    thresholding = traits.Enum("cluster","none","uncorrected","voxel",mandatory=False, desc="Thresholding type")
    prob_thresh = traits.Float(mandatory=False, desc="corrected voxel P threshold.  used during analysis types 'uncorrected', 'voxel', and 'cluster'")
    z_thresh = traits.Float(mandatory=False, desc="initial Z threshold when defining clusters  (for analysis type 'cluster' only)")
    highres_files = traits.Either(traits.List(traits.String), traits.String, mandatory=False, desc="list of the 1st-level anatomicals or the name of a text file containing this list")
    regressor_file_list = traits.List(traits.String)(mandatory=False, desc="list of regressor files.  each file should have one value per row. If none is given, a single group mean will be performed")
    regressor_names = traits.List(traits.String)(mandatory=False, desc="list of corresponding names for the regressors")
    regressor_demean_flags = traits.List(traits.Int)(mandatory=False, desc="set 1 or 0 to control whether a given regressor should be demeaned")
    contrasts = traits.Either(traits.List(traits.String), traits.String, mandatory=False, desc="a list of lists of the desired contrasts (default is each regressor individually)")
    contrast_names = traits.Either(traits.List(traits.String), traits.String, mandatory=False, desc="list of names corresponding `contrasts`")
    copeinputs = traits.List(traits.Int)(mandatory=False, desc="list of boolean integers equal in leangth to the number of first-level ")
    group_list = traits.Either(traits.List(traits.String), traits.String, mandatory=False, desc="list of the group membership of each subject (default will 1 group for all) OR filename of the text file containing this list")
    fsf_overrides = traits.DictStrAny(mandatory=False, desc="dictionary of specific fsf values that will override any defaults set up by this script or a .csv file containing key,value pairs")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fMRI_level2_statsOutputSpec(TraitedSpec):
    fsf = traits.DictStrAny(exists=True,desc="the fsf dictionary that was generated")


class cmind_fMRI_level2_stats(BaseInterface):
    input_spec=cmind_fMRI_level2_statsInputSpec
    output_spec=cmind_fMRI_level2_statsOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fMRI_level2_stats as fMRI_level2_stats
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._fsf) = fMRI_level2_stats(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["fsf"]=self._fsf
        return outputs


class cmind_fMRI_normInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    reg_struct = traits.Either(traits.String, traits.DictStrAny, mandatory=True, position=1, desc="filename of the .csv containing the registration structure")
    regBBR_dir = traits.String(mandatory=True, position=2, desc="pathname to the BBR registration results (rigid from functional to structural)")
    prep_dir = traits.String(mandatory=True, position=3, desc="directory containing the preprocessed fMRI volumes from cmind_fMRI_preprocess2")
    stats_dir = traits.String(mandatory=True, position=4, desc="directory containing the first-level stats results")
    ANTS_path = traits.String(mandatory=False, desc="path to the ANTs binaries")
    reg_flag = traits.Either(traits.String, traits.Int, mandatory=False, desc="string of 3 characters controlling whether FLIRT, FNIRT and/or ANTS registrations")
    output_tar = traits.Bool(mandatory=False, desc="compress the registration folders into a .tar.gz archive")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate overlay images summarizing the registration")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fMRI_normOutputSpec(TraitedSpec):
    reg_tarfile = traits.String(exists=True,desc="filename of the .tar.gz filenames corresponding to the lowres->highres ")
    reg_standard_tarfile = traits.Either(traits.DictStrAny, traits.String, exists=True,desc="dictionary containing the .tar.gz filenames corresponding to 'FLIRT', ")
    reg_dir = traits.String(exists=True,desc="directory containing highres registration")
    reg_standard_dir = traits.String(exists=True,desc="directory containing standard space registration")


class cmind_fMRI_norm(BaseInterface):
    input_spec=cmind_fMRI_normInputSpec
    output_spec=cmind_fMRI_normOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fMRI_norm as fMRI_norm
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._reg_tarfile,self._reg_standard_tarfile,self._reg_dir,self._reg_standard_dir) = fMRI_norm(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["reg_tarfile"]=self._reg_tarfile
        outputs["reg_standard_tarfile"]=self._reg_standard_tarfile
        outputs["reg_dir"]=self._reg_dir
        outputs["reg_standard_dir"]=self._reg_standard_dir
        return outputs


class cmind_fMRI_outliersInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    contrast_of_interest = traits.Either(traits.Int, traits.List(traits.Int), mandatory=True, position=1, desc="which contrast in FSF_template is of primary interest?  Note: Unlike in ")
    regBBR_ref = traits.String(mandatory=True, position=2, desc="filename of the regBBR reference image")
    WM_pve_vol = traits.String(mandatory=True, position=3, desc="WM partial volume estimate from structural processing")
    GM_pve_vol = traits.String(mandatory=True, position=4, desc="GM partial volume estimate from structural processing")
    CSF_pve_vol = traits.String(mandatory=True, position=5, desc="CSF partial volume estimate from structural processing")
    vol_file_niigz = traits.String(mandatory=True, position=6, desc="unfiltered 4D fMRI timeseries to process")
    filtered_func_data = traits.String(mandatory=True, position=7, desc="smoothed and temporal filtered 4D fMRI timeseries file to process")
    motion_parfile = traits.String(mandatory=True, position=8, desc="The 6 column motion parameter (.par) file from mcflirt")
    unfiltered_design_matrix = traits.String(mandatory=True, position=9, desc="The (unfiltered) Feat design matrix corresponding to `vol_file_niigz`")
    FSF_template = traits.String(mandatory=True, position=10, desc="THE .fsf template corresponding to `vol_file_niigz`")
    noise_est_file = traits.String(mandatory=True, position=11, desc="The noise_est.txt file from cmind_fMRI_preprocess2.py")
    HRtoLR_Affine = traits.String(mandatory=False, desc="filename for the affine transform from functional->structural space")
    HRtoLR_Warp = traits.String(mandatory=False, desc="fieldmap corrected warp from functional->structural space")
    Nproc_max = traits.Int(mandatory=False, desc="maximum number of processes to launch simultaneously")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fMRI_outliersOutputSpec(TraitedSpec):
    default_confound_file = traits.String(exists=True,desc="confound file with 6 motion + outliers")
    best_confound_file = traits.String(exists=True,desc="optimal confound file")
    worst_confound_file = traits.String(exists=True,desc="'worst' confound file")
    mcf06_confound_file = traits.String(exists=True,desc="6 motion parameters confound file")
    FSL_outliers_txt = traits.String(exists=True,desc="outliers confound file")


class cmind_fMRI_outliers(BaseInterface):
    input_spec=cmind_fMRI_outliersInputSpec
    output_spec=cmind_fMRI_outliersOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fMRI_outliers as fMRI_outliers
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._default_confound_file,self._best_confound_file,self._worst_confound_file,self._mcf06_confound_file,self._FSL_outliers_txt) = fMRI_outliers(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["default_confound_file"]=self._default_confound_file
        outputs["best_confound_file"]=self._best_confound_file
        outputs["worst_confound_file"]=self._worst_confound_file
        outputs["mcf06_confound_file"]=self._mcf06_confound_file
        outputs["FSL_outliers_txt"]=self._FSL_outliers_txt
        return outputs


class cmind_fMRI_preprocessInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    BOLD_vol = traits.String(mandatory=True, position=1, desc="filename of the NIFTI/NIFTI-GZ BOLD 4D timeseries")
    ASL_vol = traits.Any(mandatory=False, desc="filename of the NIFTI/NIFTI-GZ ASL 4D timeseries")
    paradigm_name = traits.String(mandatory=False, desc="output filenames will incorporate this name.  may also be used in the")
    ANTS_bias_cmd = traits.String(mandatory=False, desc="pathname to the ANTs bias field correction binary")
    UCLA_flag = traits.Bool(mandatory=False, desc="if True, ASL labeling order is tag, control, tag, ...")
    fsl_motion_outliers_cmd = traits.String(mandatory=False, desc="path to the fsl_motion_outliers_cmd_v2 shell script.  If not specified")
    FORCE_SINGLEPROCESS = traits.Bool(mandatory=False, desc="if False will try to run ASL & BOLD motion correction in parallel via multiprocessing")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fMRI_preprocessOutputSpec(TraitedSpec):
    ASL_mcf_vol = traits.Any(exists=True,desc="motion corrected ASL timeseries")
    BOLD_mcf_vol = traits.String(exists=True,desc="motion corrected BOLD timeseries")
    meanCBF_N4_vol = traits.Any(exists=True,desc="mean control-tag image from ASL timeseries")
    meanBOLD_N4_vol = traits.String(exists=True,desc="mean image from BOLD timeseries")
    motion_parfile_ASL = traits.Any(exists=True,desc="ASL motion parameters")
    motion_parfile_BOLD = traits.String(exists=True,desc="BOLD motion parameters")


class cmind_fMRI_preprocess(BaseInterface):
    input_spec=cmind_fMRI_preprocessInputSpec
    output_spec=cmind_fMRI_preprocessOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fMRI_preprocess as fMRI_preprocess
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._ASL_mcf_vol,self._BOLD_mcf_vol,self._meanCBF_N4_vol,self._meanBOLD_N4_vol,self._motion_parfile_ASL,self._motion_parfile_BOLD) = fMRI_preprocess(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["ASL_mcf_vol"]=self._ASL_mcf_vol
        outputs["BOLD_mcf_vol"]=self._BOLD_mcf_vol
        outputs["meanCBF_N4_vol"]=self._meanCBF_N4_vol
        outputs["meanBOLD_N4_vol"]=self._meanBOLD_N4_vol
        outputs["motion_parfile_ASL"]=self._motion_parfile_ASL
        outputs["motion_parfile_BOLD"]=self._motion_parfile_BOLD
        return outputs


class cmind_fMRI_preprocess2InputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    func_file = traits.String(mandatory=True, position=1, desc="filename of the 4D NIFTI/NIFTI-GZ fMRI timeseries")
    feat_params = traits.String(mandatory=False, desc="filename of a text (.csv) file containing parameters of the FEAT preprocessing")
    TR = traits.Float(mandatory=False, desc="repetition time, TR (seconds).  If not specified will be read from feat_params")
    smooth = traits.Float(mandatory=False, desc="spatial smoothing (mm).  If not specified will be read from feat_params")
    paradigm_hp = traits.Float(mandatory=False, desc="high pass filter cutoff (seconds).  If not specified will be read from feat_params")
    brain_thresh = traits.Float(mandatory=False, desc="brain threshold (percent).  If not specified will be read from feat_params")
    perfusion_subtract = traits.Bool(mandatory=False, desc="if True, perform ASL surround subtraction.  If not specified will be read from feat_params")
    preprocess_case = traits.Any(mandatory=False, desc="placeholder that may be used in the future to apply different processing ")
    slice_timing_file = traits.Any(mandatory=False, desc="slice timing file")
    usan_vol = traits.Any(mandatory=False, desc="filename of alternative usan_vol for susan smoothing.  if not specified")
    output_tar = traits.Bool(mandatory=False, desc="compress the feat preprocessing folder into a .tar.gz archive")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fMRI_preprocess2OutputSpec(TraitedSpec):
    filtered_func_data = traits.String(exists=True,desc="filename corresponding to the filtered functional data")
    noise_est_file = traits.String(exists=True,desc="noise_est.txt file corresponding to filtered_func_data")
    preproc_tarfile = traits.Any(exists=True,desc=".tar.gz archive where results were stored")
    feat_params_out = traits.String(exists=True,desc="filename of a .csv file containing the preprocessing options that were used")
    prep_dir = traits.String(exists=True,desc="directory where the preprocessed results were stored        ")


class cmind_fMRI_preprocess2(BaseInterface):
    input_spec=cmind_fMRI_preprocess2InputSpec
    output_spec=cmind_fMRI_preprocess2OutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fMRI_preprocess2 as fMRI_preprocess2
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._filtered_func_data,self._noise_est_file,self._preproc_tarfile,self._feat_params_out,self._prep_dir) = fMRI_preprocess2(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["filtered_func_data"]=self._filtered_func_data
        outputs["noise_est_file"]=self._noise_est_file
        outputs["preproc_tarfile"]=self._preproc_tarfile
        outputs["feat_params_out"]=self._feat_params_out
        outputs["prep_dir"]=self._prep_dir
        return outputs


class cmind_fieldmap_processInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    fieldmap_nii_rads = traits.String(mandatory=True, position=1, desc="fieldmap volume in rad/s")
    fieldmap_nii_mag = traits.String(mandatory=True, position=2, desc="magnitude image corresponding to `fieldmap_nii_rads`")
    T1_head_vol = traits.String(mandatory=True, position=3, desc="filename of the 'fixed' head image  (before brain extraction)")
    T1_brain_vol = traits.String(mandatory=True, position=4, desc="filename of the 'fixed' brain image")
    deltaTE = traits.String(mandatory=False, desc="echo time difference used to generate `fieldmap_nii_rads`")
    unwarpdir = traits.Enum("z-","y-","x-","y","x","z",mandatory=False, desc="phase encoding direction during the readout")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fieldmap_processOutputSpec(TraitedSpec):
    fieldmap_vol = traits.String(exists=True,desc="processed fieldmap")
    fieldmap_unmasked = traits.String(exists=True,desc="unmasked processed fieldmap")
    fieldmap2struct_affine = traits.String(exists=True,desc="affine transform from fieldmap to structural space")


class cmind_fieldmap_process(BaseInterface):
    input_spec=cmind_fieldmap_processInputSpec
    output_spec=cmind_fieldmap_processOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fieldmap_process as fieldmap_process
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._fieldmap_vol,self._fieldmap_unmasked,self._fieldmap2struct_affine) = fieldmap_process(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["fieldmap_vol"]=self._fieldmap_vol
        outputs["fieldmap_unmasked"]=self._fieldmap_unmasked
        outputs["fieldmap2struct_affine"]=self._fieldmap2struct_affine
        return outputs


class cmind_fieldmap_regBBRInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which to store the output")
    lowres_vol = traits.String(mandatory=True, position=1, desc="image filename for the 'moving' image")
    reg_struct_file = traits.String(mandatory=True, position=2, desc="filename of the existing .csv registration structure")
    T1_vol = traits.String(mandatory=True, position=3, desc="filename of the 'fixed' head image  (before brain extraction)")
    T1_brain_vol = traits.String(mandatory=True, position=4, desc="filename of the 'fixed' brain image")
    WMseg_vol = traits.String(mandatory=True, position=5, desc="filename of the white matter partial volume estimate image")
    bbr_schedule_file = traits.String(mandatory=False, desc="filename of flirt schedule to use  (e.g. $FSLDIR/etc/flirtsch/bbr.sch)")
    Fieldmap_vol_processed = traits.Any(mandatory=False, desc="filename of the fieldmap volume")
    Fieldmap_vol_unmasked = traits.Any(mandatory=False, desc="filename of the unmasked fieldmap volume")
    fieldmap2struct_affine = traits.Any(mandatory=False, desc="filename of the affine transform of the fieldmap to structural space")
    unwarpdir = traits.Enum("z-","y-","x-","y","x","z",mandatory=False, desc="phase encoding direction during the readout")
    EPI_echo_spacing = traits.Float(mandatory=False, desc="EPI echo spacing (seconds)")
    SENSE_factor = traits.Float(mandatory=False, desc="SENSE acceleration factor       ")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate overlay images summarizing the registration")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_fieldmap_regBBROutputSpec(TraitedSpec):
    LRtoHR_Affine_fname = traits.Any(exists=True,desc="If no fieldmap is used, this is the transform from lowres->structural")
    HRtoLR_Affine_fname = traits.Any(exists=True,desc="If no fieldmap is used, this is the transform from structural->lowres")
    LRtoHR_Warp_fname = traits.Any(exists=True,desc="If a fieldmap is used, this is the warp from lowres->structural  ")
    HRtoLR_Warp_fname = traits.Any(exists=True,desc="If a fieldmap is used, this is the warp from structural->lowres")
    regBBR_dir = traits.String(exists=True,desc="directory where the registration outputs were stored (= output_dir input)")


class cmind_fieldmap_regBBR(BaseInterface):
    input_spec=cmind_fieldmap_regBBRInputSpec
    output_spec=cmind_fieldmap_regBBROutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_fieldmap_regBBR as fieldmap_regBBR
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._LRtoHR_Affine_fname,self._HRtoLR_Affine_fname,self._LRtoHR_Warp_fname,self._HRtoLR_Warp_fname,self._regBBR_dir) = fieldmap_regBBR(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["LRtoHR_Affine_fname"]=self._LRtoHR_Affine_fname
        outputs["HRtoLR_Affine_fname"]=self._HRtoLR_Affine_fname
        outputs["LRtoHR_Warp_fname"]=self._LRtoHR_Warp_fname
        outputs["HRtoLR_Warp_fname"]=self._HRtoLR_Warp_fname
        outputs["regBBR_dir"]=self._regBBR_dir
        return outputs


class cmind_normalizeInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="directory in which structural processing results are stored")
    T1_volume_nii = traits.String(mandatory=True, position=1, desc="T1W image (after brain extraction).  This is the image to register to")
    reg_struct = traits.Either(traits.String, traits.DictStrAny, mandatory=True, position=2, desc="filename of the .csv file containing the registration dictionary")
    direct_to_MNI = traits.Bool(mandatory=False, desc="if True, bypass study template and register directly to the MNI standard")
    reg_flag = traits.Either(traits.String, traits.Int, mandatory=False, desc="string of 3 characters controlling whether FLIRT, FNIRT and/or ANTS registrations of T1 to standard are to be run")
    ANTS_path = traits.String(mandatory=False, desc="path to the ANTs binaries")
    MNI_path = traits.String(mandatory=False, desc="path containing the MNI standard brains")
    ANTS_reg_case = traits.Enum("Aff","AffSyn","SynAff","SyN",mandatory=False, desc="Type of ANTS registration")
    ANTS_precision = traits.Enum("strict","ants_defaults","lenient",mandatory=False, desc="choose from preset precision levels for the registrations")
    MNI_resolution = traits.Enum("1mm","2mm",mandatory=False, desc="resolution of MNI standard space image to register ")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_normalizeOutputSpec(TraitedSpec):
    reg_struct = traits.DictStrAny(exists=True,desc="dictionary corresponding to the registration structure")
    reg_struct_file = traits.File(exists=True,desc="filename of the registration structure")
    output_files = traits.List(exists=True,desc="list of registered volumes")


class cmind_normalize(BaseInterface):
    input_spec=cmind_normalizeInputSpec
    output_spec=cmind_normalizeOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_normalize as normalize
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._reg_struct,self._reg_struct_file,self._output_files) = normalize(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["reg_struct"]=self._reg_struct
        outputs["reg_struct_file"]=self._reg_struct_file
        outputs["output_files"]=self._output_files
        return outputs


class cmind_rigid_coregisterInputSpec(BaseInterfaceInputSpec):
    output_nii = traits.String(mandatory=True, position=0, desc="filename for the registered volume output")
    fixed_nii = traits.String(mandatory=True, position=1, desc="filename of fixed (reference) volume to register to")
    moving_nii = traits.String(mandatory=True, position=2, desc="filename of moving volume to be registered")
    flirt_args = traits.String(mandatory=False, desc="additional command line arguments to FLIRT")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_rigid_coregisterOutputSpec(TraitedSpec):
    output_nii = traits.String(exists=True,desc="filename of the registered volume")
    output_affine = traits.String(exists=True,desc="filename of the affine transformation used during registration")


class cmind_rigid_coregister(BaseInterface):
    input_spec=cmind_rigid_coregisterInputSpec
    output_spec=cmind_rigid_coregisterOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_rigid_coregister as rigid_coregister
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._output_nii,self._output_affine) = rigid_coregister(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["output_nii"]=self._output_nii
        outputs["output_affine"]=self._output_affine
        return outputs


class cmind_segmentInputSpec(BaseInterfaceInputSpec):
    fname_brain = traits.String(mandatory=True, position=0, desc="input brain volume")
    seg_name = traits.String(mandatory=True, position=1, desc="root name to use for the output filenames")
    reg_struct = traits.Either(traits.String, traits.DictStrAny, mandatory=False, desc="registration structure")
    use_prior = traits.Bool(mandatory=False, desc="prior image: see FSL Fast documentation")
    use_alt_prior = traits.Bool(mandatory=False, desc="alternative prior images: see FSL Fast documentation")
    use_prior_throughout = traits.Bool(mandatory=False, desc="use priors throughout: see FSL Fast documentation")
    resegment_using_N4mask = traits.Bool(mandatory=False, desc="rerun N4 bias correction on a mask weighted toward white matter then resegment")
    ANTS_bias_cmd = traits.String(mandatory=False, desc="path to ANTs N4BiasFieldCorrection binary (used if resegment_using_N4mask=True)")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_segmentOutputSpec(TraitedSpec):
    WM_vol = traits.String(exists=True,desc="WM partial volume estimate")
    GM_vol = traits.String(exists=True,desc="GM partial volume estimate")
    CSF_vol = traits.String(exists=True,desc="CSF partial volume estimate")


class cmind_segment(BaseInterface):
    input_spec=cmind_segmentInputSpec
    output_spec=cmind_segmentOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_segment as segment
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._WM_vol,self._GM_vol,self._CSF_vol) = segment(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["WM_vol"]=self._WM_vol
        outputs["GM_vol"]=self._GM_vol
        outputs["CSF_vol"]=self._CSF_vol
        return outputs


class cmind_segment_atroposInputSpec(BaseInterfaceInputSpec):
    fname_brain = traits.String(mandatory=True, position=0, desc="input T1-weighted brain volume")
    fname_head = traits.String(mandatory=True, position=1, desc="input T1-weighted head volume")
    fname_brain_mask = traits.String(mandatory=True, position=2, desc="input T1-weighted brain mask")
    additional_intensity_vols = traits.Either(traits.String, traits.List(traits.String), mandatory=False, desc="additional volumes (e.g. T2-weighted) to use during segmentation ")
    output_dir = traits.String(mandatory=False, desc="output directory for the segmentation")
    reg_struct = traits.Either(traits.String, traits.DictStrAny, mandatory=False, desc="registration structure")
    Nclasses = traits.Int(mandatory=False, desc="number of classes to segment.  If priors_dir is specified, there must")
    priors_dir = traits.Any(mandatory=False, desc="directory containing the priors.  Prior files must be named prior*.nii.gz")
    prior_weight = traits.Float(mandatory=False, desc="weight applied to the priors during segmentation.  valid range: [0 1]")
    resegment_using_N4mask = traits.Bool(mandatory=False, desc="rerun N4 bias correction on a mask weighted toward white matter then resegment")
    ANTS_bias_cmd = traits.String(mandatory=False, desc="path to ANTs N4BiasFieldCorrection binary (used if resegment_using_N4mask=True)")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_segment_atroposOutputSpec(TraitedSpec):
    WM_vol = traits.String(exists=True,desc="WM partial volume estimate")
    GM_vol = traits.String(exists=True,desc="GM partial volume estimate")
    CSF_vol = traits.String(exists=True,desc="CSF partial volume estimate")
    DeepGM_vol = traits.Any(exists=True,desc="Deep gray matter partial volume estimate")
    Brainstem_vol = traits.Any(exists=True,desc="Brainstem partial volume estimate")
    Cerebellar_vol = traits.Any(exists=True,desc="Cerebellar partial volume estimate")


class cmind_segment_atropos(BaseInterface):
    input_spec=cmind_segment_atroposInputSpec
    output_spec=cmind_segment_atroposOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_segment_atropos as segment_atropos
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._WM_vol,self._GM_vol,self._CSF_vol,self._DeepGM_vol,self._Brainstem_vol,self._Cerebellar_vol) = segment_atropos(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["WM_vol"]=self._WM_vol
        outputs["GM_vol"]=self._GM_vol
        outputs["CSF_vol"]=self._CSF_vol
        outputs["DeepGM_vol"]=self._DeepGM_vol
        outputs["Brainstem_vol"]=self._Brainstem_vol
        outputs["Cerebellar_vol"]=self._Cerebellar_vol
        return outputs


class cmind_stage_second_levelInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.String(mandatory=False, desc="desired directory in which to extract the first level results for further processing")
    tarfile_list = traits.Either(traits.List(traits.String), traits.String, mandatory=True, position=1, desc="list of .tar or .tar.gz files corresponding to first level analyses")
    output_tar = traits.String(mandatory=False, desc="If provided, all first level subjects can be combined into a single .tar.gz")
    preserve_subdir_names = traits.Bool(mandatory=False, desc="If provided, the subject directory names will match the basename of the ")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_stage_second_levelOutputSpec(TraitedSpec):
    level1_dir_list = traits.String(exists=True,desc="list of the first-level feat analysis folders.  ")


class cmind_stage_second_level(BaseInterface):
    input_spec=cmind_stage_second_levelInputSpec
    output_spec=cmind_stage_second_levelOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_stage_second_level as stage_second_level
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._level1_dir_list) = stage_second_level(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["level1_dir_list"]=self._level1_dir_list
        return outputs


class cmind_struct_preprocessInputSpec(BaseInterfaceInputSpec):
    output_dir = traits.Directory(mandatory=False, desc="directory in which to store the output")
    struct_nii = traits.String(mandatory=True, position=1, desc="filename of T1-weighted NIFTI or NIFTIGZ volume to process")
    T2_struct_nii = traits.String(mandatory=False, desc="if T2_struct_nii is specified it will be rigidly registered to")
    age_months = traits.Float(mandatory=False, desc="subject age in months  (used during cropping)")
    bet_thresh = traits.Float(mandatory=False, desc="bet brain extraction threshold")
    bet_use_T2 = traits.Bool(mandatory=False, desc="if True, brain extract via T2_struct_nii instead of struct_nii")
    oname_root = traits.String(mandatory=False, desc="output filename prefix")
    reupdate_bias_cor = traits.Bool(mandatory=False, desc="rerun bias correction after brain extraction")
    resegment_using_N4mask = traits.Bool(mandatory=False, desc="if True, after initial segmentation rerun N4 bias correction, weighted to")
    ANTS_bias_cmd = traits.String(mandatory=False, desc="location of N4BiasFieldCorrection binary")
    omit_segmentation = traits.Bool(mandatory=False, desc="if True, omit segmentation")
    generate_figures = traits.Bool(mandatory=False, desc="if true, generate additional summary images")
    ForceUpdate = traits.Bool(mandatory=False, desc="if True, rerun and overwrite any previously existing results")
    verbose = traits.Bool(mandatory=False, desc="print additional output (to terminal and log)")
    logger = traits.String(mandatory=False, desc="logging.Logger object (or string of a filename to log to)")


class cmind_struct_preprocessOutputSpec(TraitedSpec):
    fname_head = traits.String(exists=True,desc="filename of Bias corrected head")
    fname_brain = traits.String(exists=True,desc="filename of Bias corrected after brain extraction")
    fname_brain_mask = traits.String(exists=True,desc="filename of brain mask")
    WM_vol = traits.Any(exists=True,desc="filename of WM partial volume estimate")
    GM_vol = traits.Any(exists=True,desc="filename of GM partial volume estimate")
    CSF_vol = traits.Any(exists=True,desc="filename of CSF partial volume estimate")
    bias_field_vol = traits.String(exists=True,desc="filename of the N4 bias correction field")
    fname2_head = traits.Any(exists=True,desc="if T2_struct_nii is input, this is the corresponding bias-corrected head")
    fname2_brain = traits.Any(exists=True,desc="if T2_struct_nii is input, this is the corresponding bias-corrected brain")


class cmind_struct_preprocess(BaseInterface):
    input_spec=cmind_struct_preprocessInputSpec
    output_spec=cmind_struct_preprocessOutputSpec

    def _run_interface(self, runtime):
        from cmind.pipeline import cmind_struct_preprocess as struct_preprocess
        args = {}
        for key in [k for k, _ in self.inputs.items() if k not in BaseInterfaceInputSpec().trait_names()]:
            value = getattr(self.inputs, key)
            if isdefined(value):
                args[key] = value
        (self._fname_head,self._fname_brain,self._fname_brain_mask,self._WM_vol,self._GM_vol,self._CSF_vol,self._bias_field_vol,self._fname2_head,self._fname2_brain) = struct_preprocess(**args)
        return runtime

    def _list_outputs(self):
        outputs=self._outputs().get()
        outputs["fname_head"]=self._fname_head
        outputs["fname_brain"]=self._fname_brain
        outputs["fname_brain_mask"]=self._fname_brain_mask
        outputs["WM_vol"]=self._WM_vol
        outputs["GM_vol"]=self._GM_vol
        outputs["CSF_vol"]=self._CSF_vol
        outputs["bias_field_vol"]=self._bias_field_vol
        outputs["fname2_head"]=self._fname2_head
        outputs["fname2_brain"]=self._fname2_brain
        return outputs


