#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $cwd/exec_exists.sh

if exec_exists module;
then
	#echo "found module"
	module load matlab
    mroot=`cd $cwd/../../../../; pwd`
    MATLAB_PATH_ADDITIONS="cmind_root='$mroot';  addpath(cmind_root); addpath(fullfile(cmind_root,'lib','MATLAB')); addpath(fullfile(cmind_root,'lib')); addpath(fullfile(cmind_root,'lib','nifti_tools')); addpath(fullfile(cmind_root,'lib','PARREC_tools')); addpath(fullfile(cmind_root,'lib','MATLAB','freezeColors')); addpath(fullfile(cmind_root,'lib','MATLAB','moving_average_v3.1')); irt_dir=fullfile(cmind_root,'lib','irt'); run(fullfile(irt_dir, 'setup.m'));" #" path=(irtdir,path);"
else
	if exec_exists matlab;
	then
		#echo "module not found, but matlab already in path"
		echo ""
		MATLAB_PATH_ADDITIONS="addpath('/home/lee8rx/src_repositories/svn_stuff/cmind-matlab/trunk'); addpath('/home/lee8rx/src_repositories/svn_stuff/cmind-matlab/trunk/lib');"
	else
		echo "Error:  module not found, and matlab not already on path"
		exit 1
	fi
fi

