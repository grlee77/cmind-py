#!/usr/bin/env python
"""
**generate HTML reports**
"""
from __future__ import division, print_function, absolute_import
import os, glob
import numpy as np

from cmind.utils.utils import input2bool
from cmind.utils.file_utils import imexist
from cmind.utils.logging_utils import log_cmd

def cmind_combine_HTML_reports(base_dir, report_name=None, max_depth=None, report_title=None, ForceUpdate=True):
    """
    Find all .html reports within base_dir and combine them into a single report
    """
    
    HTML_report_list=[]
    base_dir=base_dir.rstrip(os.path.sep) #strip trailing file seperator from base_dir

    for (dirpath, dirnames, filenames) in os.walk(base_dir):
        #if dirpath==base_dir: #don't search in the base directory itself
        #    continue
        if max_depth: #only recurse to a given depth
            max_depth=int(max_depth)
            if max_depth<0:
                raise Exception("max_depth: %d is invalid, must be a positive integer" % max_depth)
            current_depth = dirpath[len(base_dir):].count(os.path.sep)
            #print "{}:  depth = {}".format(dirpath,current_depth)
            if current_depth>=max_depth:
                del dirnames[:]
                
        html_list=glob.glob(os.path.join(dirpath,'*.html'))
        if html_list:
            if not HTML_report_list:
                HTML_report_list=html_list
            else:
                for item in html_list:
                        HTML_report_list.append(item)

    if HTML_report_list: 
        if not report_name:
            report_name=os.path.join(base_dir,'all_reports.html')
        if not report_title:
            report_title='Summary Reports'
        fidHTML=open(report_name,'w')
        use_table=True
        if use_table==False:
            fidHTML.write('<HTML><HEAD>\n') #<link REL=stylesheet TYPE=text/css href=.files/fsl.css>
            fidHTML.write('<TITLE>CMIND Report</TITLE></HEAD><BODY>\n')
            fidHTML.write('<h2>%s</h2>\n' % report_title)
        else:
            fidHTML.write('<TABLE BORDER=0><TR>\n') #<link REL=stylesheet TYPE=text/css href=.files/fsl.css>
            fidHTML.write('<TD ALIGN=CENTER WIDTH=100%>\n') #<link REL=stylesheet TYPE=text/css href=.files/fsl.css>
            fidHTML.write('<TABLE BORDER=0>\n') #<link REL=stylesheet TYPE=text/css href=.files/fsl.css>
            fidHTML.write('<tr><td align=center><font size=+3><b>%s</b></font></tr>\n' % report_title) #<link REL=stylesheet TYPE=text/css href=.files/fsl.css>
            
        for report in HTML_report_list:
            if use_table==False:
                fidHTML.write('<br><a href=%s>%s</a><p>\n' % (report, os.path.basename(report).replace('.html','')))
            else:
                fidHTML.write('<tr valign=bottom><td align=center>\n')
                fidHTML.write('<a href=%s>%s</a>\n' % (report, os.path.basename(report).replace('.html','')))
                fidHTML.write('</tr>\n')
        if use_table:
            fidHTML.write('</table>\n')
            fidHTML.write('</TR></TABLE>\n')
        else:
            fidHTML.write('</BODY></HTML>\n')
        fidHTML.close()

    for report in HTML_report_list:
        print(os.path.basename(report))                    
    print(HTML_report_list)
    

def cmind_HTML_reports(report_case,base_dir,report_dir=None,ForceUpdate=False,extra_params=""):
    """utility function that generates HTML reports for several particular cases
    
    report_case : {'t1_crop_extract_biascor_segment','t1_normalize','alpha','t1map','baselinecbf'}
        select particular preset filenames, titles etc for use in calling cmind_HTML_report_gen
    base_dir : str
        directory where the image files are located
    report_dir : str, optional
        directory to store the report in
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    extra_params : str,optional
        custom parameters used during report generation
    
    """
    
    if (not report_dir) or report_dir=="None":
        report_dir=os.path.join(base_dir,'report')

    ForceUpdate=input2bool(ForceUpdate)
   
    #note:  fnames and titles must be lists of filenames and their matching titles, respectively
        #       the HTML report will skip any of these filenames that are not present in the directory
        # final report will be in report_dir/report_fname.html

    if ((report_case.lower()=='t1_crop_extract_biascor_segment') or (report_case.lower()=='t1w_crop_extract_biascor_segment')):
            
        report_fname='Structural_Report';
        report_title='T1 Structural Processing';
        fnames=['./T1W.png',
                './T1W_Crop.png',
                './T1W_N4.png',
                './T1W_N4_brain.png',
                './Segmentation_Slices.png',
                './WMedge_slices.png',
                './brain_mask_overlay.png'];
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        
        titles=['raw T1',
                'cropped T1',
                'bias corrected T1',
                'brain extracted T1',
                'segmented T1',
                'WM edge overlay',
                'Brain mask overlay'];
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles);
        else:
            print('HTML report found...skipping')

    elif ((report_case.lower()=='t2_crop_extract_biascor_segment') or (report_case.lower()=='t2w_crop_extract_biascor_segment')):
            
        report_fname='Structural_Report';
        report_title='T2 Structural Processing';
        fnames=['./T2W.png',
                './T2W_Crop.png',
                './T2W_N4.png',
                './T2W_N4_brain.png',
                './Segmentation_Slices.png',
                './WMedge_slices.png',
                './T2W_Structural_N4_brain_overlay.png'];
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        
        titles=['raw T2',
                'cropped T2',
                'bias corrected T2',
                'brain extracted T2',
                'segmented T2',
                'WM edge overlay',
                'Brain mask overlay'];
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles);
        else:
            print('HTML report found...skipping')

    elif report_case.lower()=='segment':
            
        report_fname='Segmentation_Report';
        report_title='Segmentation Processing';
        fnames=['./Segmentation_Slices.png',
                './WMedge_slices.png'];
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        titles=['segmented T1',
                'WM edge overlay'];
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles);
        else:
            print('HTML report found...skipping')

    elif ((report_case.lower()=='t1_crop_extract_biascor') or (report_case.lower()=='t1w_crop_extract_biascor')):
            
        report_fname='Structural_Report';
        report_title='T1 Structural Processing';
        fnames=['./T1W.png',
                './T1W_Crop.png',
                './T1W_N4.png',
                './T1W_N4_brain.png',
                './T1W_Structural_N4_brain_overlay.png'];
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        titles=['raw T1',
                'cropped T1',
                'bias corrected T1',
                'brain extracted T1',
                'Brain mask overlay'];
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles);
        else:
            print('HTML report found...skipping')

    elif ((report_case.lower()=='t2_crop_extract_biascor') or (report_case.lower()=='t2w_crop_extract_biascor')):
            
        report_fname='Structural_Report';
        report_title='T2 Structural Processing';
        fnames=['./T2W.png',
                './T2W_Crop.png',
                './T2W_N4.png',
                './T2W_N4_brain.png',
                './brain_mask_overlay.png'];
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        titles=['raw T2',
                'cropped T2',
                'bias corrected T2',
                'brain extracted T2',
                 'Brain mask overlay'];
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles);
        else:
            print('HTML report found...skipping')

    elif report_case.lower()=='t1_normalize':
        report_fname='Structural_Registration_Report';
        report_title='T1 Structural Processing';
        fnames=['./T1_to_StudyTemplate_lin.png',
                './T1_to_MNI_lin.png',
                './T1_to_StudyTemplate_nonlin.png',
                './T1_to_MNI_nonlin.png',
                './T1_to_StudyTemplate_ANTS_SyN_Deformed.png',
                './T1_to_MNI_ANTS_SyN_Deformed.png'];
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        titles=['FLIRT Highres->Ped. Standard',
                'FLIRT Highres->MNI',
                'FNIRT Highres->Ped. Standard',
                'FNIRT Highres->MNI',
                'ANTS Highres->Ped. Standard',
                'ANTS Highres->MNI'];
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles,im_width=1600);
        else:
            print('HTML report found...skipping')

    elif report_case.lower()=='alpha':
        report_fname='Alpha_Report';
        report_title='Alpha Processing';
        fnames=['./alpha_details.png',
                './alpha_control_overlay.png'];
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        titles=['Alpha Map Details',
                'Alpha Map Overlay'];
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles);
        else:
            print('HTML report found...skipping')

        #system(sprintf('firefox %s',output_fname))
    elif report_case.lower()=='t1map':
        report_dir=os.path.join(base_dir,'report');
        report_fname='T1map_Report';
        report_title='T1map Processing';
        fnames=['./T1_map_0pt5_to_3.png',
                './I0_map.png',
                './T1_alpha_map_0pt5_to_1.png',
                './T1_NRMSE_map_0_to_0pt1.png',
                './allTI_img.png',
                './allTI_uncor_mov.gif',
                './allTI_mov.gif',
                './T1map_rot.png',
                './T1map_trans.png',
                './T1map_disp.png'];
                #'./T1EST_register.png'];
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        titles=['T1 map [0.5 3]',
                'M0 map [0.5 3]',
                'inversion efficiency map [0.5 1]',
                'NRMSE map [0 0.1]',
                'individual TI images (post registration)',
                'TI movie loop (pre registration)',
                'TI movie loop (post registration)',
                'T1map registration summary',
                '',
                ''];
                #'Registration of T1 map across TI values'};
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles);
        else:
            print('HTML report found...skipping')

    elif report_case.lower()=='baselinecbf':
        ##load ./CBFstats.mat #TODO:  load CBFstats from file
        report_dir=os.path.join(base_dir,'report');
        report_fname='BaselineCBF_Report';
        report_title='BaselineCBF Processing';
    
        fnames=['./MeanSub_BaselineCBF_N4.png',
                './BaselineCBF_mcf_percent_change.png',
                './BaselineCBF_motion.png',
                './CBF_Wang2002.png',
                './CBF_Wang2002_alphaconst.png'];
                #'./T1EST_register.png'};
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        titles=['Mean Control-Tag',
                'Control-Tag percentage signal change [-1 1.5]',
                'BaselineCBF registration summary',
                'Quantitative CBF [0 160] using measured alpha={}, meanGM={}, meanWM={}, ratio={}'.format(extra_params['alpha'],extra_params['mean_CBF_GM'], extra_params['mean_CBF_WM'], extra_params['ratio_CBF']),
                'Quantitative CBF [0 160] using fixed alpha={}'.format(extra_params['alpha_const'])];
                #'Registration of T1 map across TI values'];

        move_flag=False
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles,move_flag);
        else:
            print('HTML report found...skipping')

    elif report_case.lower()=='t1histogram':
        report_dir=os.path.join(base_dir,'report');
        report_fname='T1Histogram_Report'
        report_title='T1 Histograms'
    
        fnames=['./overlays.png',
                './T1histograms.png'];
               
        for hh in range(len(fnames)):
            fnames[hh]=os.path.join(base_dir,fnames[hh].replace('./',''))
        
        titles=['ROI overlays',
                'T1 histograms corresponding to each ROI'];

        move_flag=False
        output_fname=os.path.join(report_dir,'%s.html' % report_fname);
        if not os.path.isfile(output_fname) or ForceUpdate:
            output_fname=cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles,move_flag);
        else:
            print('HTML report found...skipping')

    else:
        raise Exception("Unknown case, %s" % report_case.lower())

    return (output_fname)


def cmind_HTML_report_gen(report_dir,report_fname,report_title,fnames,titles,move_flag=False,im_width='',subject_string=[],ForceUpdate=True):
    """utility function for generating HTML reports

    Parameters
    ----------
    report_dir : str
        directory in which to store the HTML report
    report_fname : str
        filename for the HTML report
    report_title : str
        title of the report
    fnames : list
        filenames to include in the report
    titles : list
        titles corresponding to each filename
    move_flag : bool, optional
        if True, move the files in fnames into report dir
        if False, copy the files in fnames into report dir
    im_width : bool, optional
        width of images in the HTML report
    subject_string : str, optional
        subject ID to use in figure labels
    move_flag : bool, optional
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    
    Notes
    -----
    
    """

    ForceUpdate=input2bool(ForceUpdate)
    move_flag=input2bool(move_flag)

    if(len(fnames) != len(titles)):
        print('arrays of filenames and titles must have same length')

    if not os.path.isdir(report_dir):
        os.makedirs(report_dir)
    
    if not subject_string:
        subject_string=report_dir;

    for f in fnames:
        if not imexist(f,'2dimg')[0]:
            print('File %s not found' % f)
        else:
            if move_flag:
                log_cmd('mv "%s" "%s"' % (f,report_dir));
            else:
                print("cp %s %s" % (f,report_dir))
                log_cmd('cp "%s" "%s"' % (f,report_dir));

    output_fname=os.path.join(report_dir,'%s.html' % report_fname);
    if not os.path.isfile(output_fname) or ForceUpdate:

        titles_full=[]        
        for nn in range(len(titles)):
            titles_full.append('%s:  %s\n' % (subject_string, titles[nn]))

        HTML_title=os.path.join(report_dir,'html_hdr')
        fidHTML=open(HTML_title,'w')
        fidHTML.write('<HTML><HEAD>\n') #<link REL=stylesheet TYPE=text/css href=.files/fsl.css>
        fidHTML.write('<TITLE>CMIND Report</TITLE></HEAD><BODY><OBJECT data=report.html></OBJECT>\n')
        fidHTML.close()

        HTML_title=os.path.join(report_dir,'html_' + report_fname + '_title')
        fidHTML=open(HTML_title,'w');
        fidHTML.write('<h2>%s</h2>\n' % report_title)
        fidHTML.close()

        HTML_title=os.path.join(report_dir,'html_foot')
        fidHTML=open(HTML_title,'w');
        fidHTML.write('</BODY></HTML>\n')
        fidHTML.close()

        HTML_root=os.path.join(report_dir,'html_' + report_fname + '_')

        #clean up old results
        oldfiles=glob.glob(os.path.join(report_dir,'%s??' % HTML_root))
        for oldf in oldfiles:
            os.remove(oldf)

        for hh in range(len(fnames)):
            #note:  fnames and titles must be cell-arrays of filenames and their matching titles, respectively
            #       the HTML report will skip any of these filenames that are not present in the directory
            # final report will be in report_dir/report_fname.html
            fidHTML=open(HTML_root + '%02d' % (hh),'w')
            imfile=fnames[hh];
            ext=os.path.splitext(imfile)
            if imexist(imfile,'2dimg')[0]:
                if not im_width:
                    try:
                        xmin=600    #force all images in report to be at or above this width
                        xmax=1920   #force all images in report to be at or below this width
                        #use file command to determine true image dimensions
                        if ext=='.png':
                            xsize=log_cmd('file %s | cut -f 2 -d , | cut -f 1 -d x' % (imfile)); 
                        elif ext=='.gif':
                            xsize=log_cmd('file %s | cut -f 3 -d , | cut -f 1 -d x' % (imfile)); 

                        xsize=int(xsize);

                        if xsize<xmin:
                            xsize=xsize*np.ceil(xmin/float(xsize));
                        elif xsize>xmax:
                            xsize=xmax;
                        
                    except:
                        xsize=800
                else:
                    xsize=im_width

                if titles[hh]: #skip if title is empty
                    fidHTML.write(titles_full[hh])
                
                fidHTML.write('<br><a href=%s><IMG BORDER=0 SRC=%s WIDTH=%d></a><p>\n' % (os.path.basename(imfile),os.path.basename(imfile),xsize))
            else:
                print('Warning: Image %s did not exist' % (imfile))
            fidHTML.close();

        log_cmd('cat "%s"/html_hdr "%s"/html_%s_title "%s"/html_%s_?? "%s"/html_foot > "%s"' % (report_dir,report_dir,report_fname,report_dir,report_fname,report_dir,output_fname))
    return output_fname


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="BaselineCBF preprocessing", epilog="")  #-h, --help exist by default
    #example of a positional argument
    parser.add_argument("report_case", help="volume to crop")
    parser.add_argument("base_dir", help="cropped output")
    parser.add_argument("--report_dir", type=str, default="None", help='')
    parser.add_argument("-u","--ForceUpdate", type=str, default="None", help='')
    parser.add_argument("--extra_params", type=str, default="None", help='')
    parser.add_argument("-v","-debug","--verbose", help="increase output verbosity", action="store_true")
    args=parser.parse_args()
    
    report_case=args.report_case;
    base_dir=args.base_dir;
    report_dir=args.report_dir;
    ForceUpdate=args.ForceUpdate;
    extra_params=args.extra_params;
    if extra_params.lower()=="none":
        extra_params=""
    #UCLA_flag=args.UCLA_flag;
    #generate_figures=args.generate_figures;
    #ForceUpdate=args.ForceUpdate;
    verbose=args.verbose;
    print(args)
    
    if report_dir=="None":
        report_dir=None
    if extra_params=="None":
        extra_params=""
    cmind_HTML_reports(report_case,base_dir,report_dir,ForceUpdate,extra_params)
     
                    