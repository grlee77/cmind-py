This folder contains example LONI pipelines

These will not run as is because they refer to files on the CCHMC computing cluster that are not publicly accessable

With minor modification to the paths, these can be adapted to other sites.  

The ../LONI_shell/load_*_env.sh scripts will also need minor modification to use the module names / paths appropriate to the local cluster