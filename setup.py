#!/usr/bin/env python
''' 
Installation script for cmind package 

Note:
To make a source distribution:
python setup.py sdist

To make an RPM distribution:
python setup.py bdist_rpm

To Install:
python setup.py install --prefix=/usr/local

See also:  
python setup.py bdist --help-formats
'''

import os
import sys
from os.path import join as pjoin, dirname
from glob import glob

# BEFORE importing distutils, remove MANIFEST. distutils doesn't properly
# update it when the contents of directories change.
if os.path.exists('MANIFEST'): os.remove('MANIFEST')

# Import build helpers
try:
    from nisext.sexts import package_check #, get_comrec_build
except ImportError:
    raise RuntimeError('Need nisext package from nibabel installation'
                       ' - please install nibabel first')

# Get version and release info, which is all stored in dipy/info.py
ver_file = os.path.join('cmind', 'info.py')
# Use exec for compabibility with Python 3
exec(open(ver_file).read())

extra_setuptools_args = {}
#from distutils.command import install

# Do dependency checking
package_check('numpy', NUMPY_MIN_VERSION)
package_check('scipy', SCIPY_MIN_VERSION)
package_check('nibabel', NIBABEL_MIN_VERSION)
custom_matplotlib_messages = {'missing opt': 'Missing optional package "%s"'
        ' provided by package "matplotlib"'
}
package_check('matplotlib',
        MATPLOTLIB_MIN_VERSION,
        optional=True,
        messages = custom_matplotlib_messages)
        
from numpy.distutils.core import setup
                       
def main(**extra_args):
    setup(name=NAME,
          maintainer=MAINTAINER,
          maintainer_email=MAINTAINER_EMAIL,
          description=DESCRIPTION,
          long_description=LONG_DESCRIPTION,
          url=URL,
          download_url=DOWNLOAD_URL,
          license=LICENSE,
          classifiers=CLASSIFIERS,
          author=AUTHOR,
          author_email=AUTHOR_EMAIL,
          platforms=PLATFORMS,
          version=VERSION,
          requires=REQUIRES,
          provides=PROVIDES, 
          #configuration = configuration,
          #cmdclass = cmdclass,
          #scripts = glob('bin/*'),
          packages=['cmind', 
                    'cmind.extern',
                    'cmind.gui',
                    'cmind.nipype', 
                    'cmind.pipeline', 
                    'cmind.utils'], 
          # package_data = {'cmind':
          #                 [pjoin('data', '*.nii.gz'),                     
          #                 ]},
#          scripts      = [pjoin('LONI_shell', 'cmind_alpha.sh'),  #only intended for use with python scripts.  distutils will copy shell scripts, but setuptools apparently will not
#                          pjoin('bin', '*'),
#                          ],
#          scripts=glob('study_specific/LONI_shell')+glob('bin/*'),
          scripts=glob('bin/*'),
          data_files=[('study_specific/LONI_shell',glob('study_specific/LONI_shell/*.sh')),
                       ('study_specific/ASLBOLD',glob('study_specific/ASLBOLD/*')),
                       ('study_specific/DTIPrep_protocols',glob('study_specific/DTIPrep_protocols/*')),
                      ],
          #cmdclass = cmdclass,
          **extra_args)

if __name__ == "__main__":
    main(**extra_setuptools_args)
	