#!/bin/bash
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $cwd/exec_exists.sh

if exec_exists module;
then
	#echo "found module"
	module load DTIPrep
	# need to make sure $FSLOUTPUTTYPE=NIFTI_GZ
else
	if exec_exists DTIPrep;
	then
		#echo "module not found, but fsl already in path"	
		echo ""
	else
		echo "Error:  module not found, and DTIPrep not already on path"
		exit 1
	fi
fi

