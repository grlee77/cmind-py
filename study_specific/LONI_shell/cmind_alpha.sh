#!/bin/bash
echo "CMD: $*"

cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd


usage()
{
echo "Basic usage: cmind_alpha.py -o <output_dir> -fname <alpha_complex_nii>  [options]

usage: cmind_alpha.py [-h] -o OUTPUT_DIR -fname ALPHA_COMPLEX_NII
                      [-gf GENERATE_FIGURES] [-u FORCEUPDATE] [-v]
                      [-log LOGGER]

ASL labeling efficiency estimation

required_arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory to store output
  -fname ALPHA_COMPLEX_NII, --alpha_complex_nii ALPHA_COMPLEX_NII
                        filename to the complex alpha .nii.gz volume

optional arguments:
  -h, --help            show this help message and exit
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=4
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/timer.sh

source $cwd/load_python_env.sh

#Assign input arguments to variables
ForceUpdate="true"
generate_figures="true"

verbose_string=""
all_optional_args=""
while [ "$1" != "" ]; do
  case $1 in
    -o|--output_dir )
        shift
        output_dir=$1
        ;;
    -fname|--alpha_complex_nii )
        shift
        alpha_complex_nii=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done

if [ ! -z $verbose_string ]; then
  debug=true
else
  debug=false
fi


if $debug
then
    echo $output_dir
    echo $generate_figures
    echo $save_additional_nii_files
    echo $ForceUpdate
    echo $make_overlay_images
    echo $alpha_complex_nii
fi

if [ ! -f $alpha_complex_nii ]; then
    echo "ERROR: Input file: $alpha_complex_nii doesn't exist"
    exit 1
fi 

if [ ! -d $output_dir ]; then
    mkdir -p "$output_dir"
fi 

# t=$(timer)

# if $debug 
# then
#     python ${cmind_python_dir}/cmind_alpha.py -o $output_dir $alpha_complex_nii --ForceUpdate $ForceUpdate --generate_figures $generate_figures --verbose
# else
#     python ${cmind_python_dir}/cmind_alpha.py -o $output_dir $alpha_complex_nii --ForceUpdate $ForceUpdate --generate_figures $generate_figures
# fi

# if $debug
# then
#     printf 'Alpha:  Elapsed time: %s\n' $(timer $t)
# fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( output_dir alpha_complex_nii generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_alpha.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_alpha.py ${verbose_string} ${all_optional_args}
printf 'cmind_alpha.py:  Elapsed time: %s\n' $(timer $t)

#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    alpha_fname


output_file_test=${output_dir}/myalpha.txt
if [ ! -f $output_file_test ] ; then
    echo "ERROR: Failed to finish alpha calculation"
    exit 1
fi 

echo "Finished Alpha calculation"
