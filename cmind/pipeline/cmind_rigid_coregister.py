#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_rigid_coregister(output_nii,fixed_nii,moving_nii, flirt_args="-cost corratio -dof 6 -searchrx -60 60 -searchry -60 60 -searchrz -60 60 -interp sinc", generate_figures=True, ForceUpdate=True, verbose=False, logger=None):
    """Rigid registration of moving_nii to fixed_nii
    
    Parameters
    ----------
    output_nii : str
        filename for the registered volume output
    fixed_nii : str
        filename of fixed (reference) volume to register to
    moving_nii : str
        filename of moving volume to be registered
    flirt_args : str, optional
        additional command line arguments to FLIRT
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
             
    Returns
    -------
    output_nii : str
        filename of the registered volume
    output_affine : str
        filename of the affine transformation used during registration
        
    """
    
    import os
    import warnings
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, split_multiple_ext, filecheck_bool
    from cmind.utils.cmind_utils import cmind_reg_report_img
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Rigid Coregistration")      
         
    output_dir = os.path.dirname(output_nii); #determine output directory from the filename
    if not output_dir:
        output_dir=os.getcwd()
        
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    imbool, fixed_nii_full = imexist(fixed_nii,'nifti-1')[0:2]
    if not imbool:
        raise IOError("specified reference image, %s, doesn't exist!" % (fixed_nii))
    fixed_nii=fixed_nii_full
        #raise EnvironmentError("File not found")
    if not imexist(moving_nii,'nifti-1')[0]:
        raise IOError("specified moving image, %s, doesn't exist!" % (moving_nii))

    #(T1path,T1concat_root,ext)=fileparts(T1concat_N4_brain_nii);
    
    output_affine = split_multiple_ext(output_nii)[0] + '.mat'
    
    if not imexist(output_nii,'nifti-1')[0] or ForceUpdate:
        if os.path.exists(output_nii):
            warnings.warn("output_file: %s already exists and will be overwritten." % output_nii)
    
        #Register the T1concat_N4_brain_nii image to the CBF_nii volume since these are more similar in contrast.
        if not flirt_args:
            flirt_args="-cost corratio -dof 6 -searchrx -60 60 -searchry -60 60 -searchrz -60 60 -interp sinc"
            
        log_cmd('$FSLDIR/bin/flirt -ref "%s" -in "%s" -out "%s" -omat "%s" %s' % (fixed_nii,moving_nii,output_nii, output_affine, flirt_args), verbose=verbose, logger=module_logger)

    #get full path with image extension for output_nii
    [outbool,output_nii]=imexist(moving_nii,'nifti-1')[0:2]   
    
    if generate_figures:
        output_img=output_affine.replace('.mat','_registered.png')
        if filecheck_bool(output_nii,output_img):
            cmind_reg_report_img(fixed_nii,output_nii,output_img)
        
    return (output_nii, output_affine)
        
def _testme():
    import os, tempfile
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    fixed_nii=os.path.join(cmind_example_output_dir,'P','T1_proc','T1W_Structural_N4.nii.gz')
    moving_nii=os.path.join(cmind_example_output_dir,'P','T2_proc','T2W_Structural_N4.nii.gz')
    output_nii=os.path.join(cmind_example_output_dir,'P','T2regT1.nii.gz')
    generate_figures=True;
    ForceUpdate=True;
    verbose=True
    flirt_args=None
    logger=cmind_logger(log_level_console='INFO',logfile=os.path.join(tempfile.gettempdir(),'cmind_log.txt'),log_level_file='DEBUG',file_mode='w')  
       
    if verbose:
        func=cmind_timer(cmind_rigid_coregister,logger=logger)
    else:
        func=cmind_rigid_coregister
        
    func(output_nii,fixed_nii,moving_nii,generate_figures=generate_figures,ForceUpdate=ForceUpdate,verbose=verbose,logger=logger,flirt_args=flirt_args)        
      
      
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        parser = argparse.ArgumentParser(description="Rigid registration of moving_nii to fixed_nii", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_nii",dest="output_nii",required=True, help="filename for the registered volume output")
        parser.add_argument("-i","--fixed_nii",dest="fixed_nii",required=True, help="filename of fixed (reference) volume to register to")
        parser.add_argument("-r","--moving_nii",dest="moving_nii",required=True, help="filename of moving volume to be registered")
        parser.add_argument("--flirt_args",dest="flirt_args", type=str, default="default", help="additional command line arguments to FLIRT")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        output_nii=args.output_nii;
        fixed_nii=args.fixed_nii;
        moving_nii=args.moving_nii;
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        logger=args.logger
        verbose=args.verbose
        flirt_args=str2none(args.flirt_args,none_strings=['none','default'])
        
        if verbose:
            cmind_rigid_coregister=cmind_timer(cmind_rigid_coregister,logger=logger)
    
        cmind_rigid_coregister(output_nii,fixed_nii,moving_nii,generate_figures=generate_figures,ForceUpdate=ForceUpdate,verbose=verbose,logger=logger,flirt_args=flirt_args)        