#!/bin/bash

#Need to move into the directory containing the bash source file
cwd="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$cwd"
cd $cwd


usage()
{
echo "Basic usage: cmind_segment.py -i <fname_brain>  [options]

usage: cmind_segment.py [-h] -i FNAME_BRAIN [-name SEG_NAME] [-rsf REG_STRUCT]
                        [-a USE_PRIOR] [-A USE_ALT_PRIOR]
                        [-P USE_PRIOR_THROUGHOUT]
                        [-reseg RESEGMENT_USING_N4MASK] [-ants ANTS_BIAS_CMD]
                        [-gf GENERATE_FIGURES] [-u FORCEUPDATE] [-v]
                        [-log LOGGER]

Segmentation using FSL's FAST


required arguments:
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        directory in which structural processing results are
                        stored
  -i FNAME_BRAIN, -t1_vol FNAME_BRAIN, --fname_brain FNAME_BRAIN
                        input brain volume

optional arguments:
  -h, --help            show this help message and exit
  -i FNAME_BRAIN, -t1_vol FNAME_BRAIN, --fname_brain FNAME_BRAIN
                        input brain volume
  -name SEG_NAME, --seg_name SEG_NAME
                        root name to use for the output filenames
  -rsf REG_STRUCT, --reg_struct REG_STRUCT
                        registration structure
  -a USE_PRIOR, --use_prior USE_PRIOR
                        prior image: see FSL Fast documentation
  -A USE_ALT_PRIOR, --use_alt_prior USE_ALT_PRIOR
                        alternative prior images: see FSL Fast documentation
  -P USE_PRIOR_THROUGHOUT, --use_prior_throughout USE_PRIOR_THROUGHOUT
                        use priors throughout: see FSL Fast documentation
  -reseg RESEGMENT_USING_N4MASK, --resegment_using_N4mask RESEGMENT_USING_N4MASK
                        rerun N4 bias correction on a mask weighted toward
                        white matter then resegment
  -ants ANTS_BIAS_CMD, --ANTS_bias_cmd ANTS_BIAS_CMD
                        path to ANTs N4BiasFieldCorrection binary (used if
                        resegment_using_N4mask=True)
  -gf GENERATE_FIGURES, --generate_figures GENERATE_FIGURES
                        if true, generate additional summary images
  -u FORCEUPDATE, --ForceUpdate FORCEUPDATE
                        if True, rerun and overwrite any previously existing
                        results
  -v, -debug, --verbose
                        print additional output (to terminal and log)
  -log LOGGER, --logfile LOGGER
                        logging.Logger object (or string of a filename to log
                        to)
"
}

if [ $# == 0 ]; then usage;  exit 0; fi
if [ "$1" == "-h" ]; then usage; exit 0; fi
if [ "$1" == "--help" ]; then usage; exit 0; fi

REQUIRED_ARGS=2
if [ $# -lt $REQUIRED_ARGS ]
then
    usage
    exit 1
fi

if [ -z "${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}" ]
then
    #variable is empty or not set at all
    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1
fi

source $cwd/load_cmind_env.sh
source $cwd/load_fsl_env.sh
source $cwd/load_ANTS_env.sh
source $cwd/timer.sh

#this version of structural processing requires Python 2.7+
source $cwd/load_python_env.sh


#defaults for arguments:
ForceUpdate=true
generate_figures=true
debug=false

ANTS_bias_cmd="N4BiasFieldCorrection"
use_prior=false
use_alt_prior=false
use_prior_throughout=false

verbose_string=""
all_optional_args=""
while [ "$1" != "" ]; do
    case $1 in
	#-np )
	#    shift
	#    export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$1;
	#    ;;
	-o|--output_dir )     #extra argument only in shell version
        shift
        output_dir=$1
        ;;
    -i|-t1_vol|--fname_brain )
        shift
        fname_brain=$1
        ;;
    -name|--seg_name )
        shift
        seg_name=$1
        ;;
    -rsf|--reg_struct )
        shift
        reg_struct=$1
        ;;
    -a|--use_prior )
        shift
        use_prior=$1
        ;;
    -A|--use_alt_prior )
        shift
        use_alt_prior=$1
        ;;
    -P|--use_prior_throughout )
        shift
        use_prior_throughout=$1
        ;;
    -reseg|--resegment_using_N4mask )
        shift
        resegment_using_N4mask=$1
        ;;
    -ants|--ANTS_bias_cmd )
        shift
        ANTS_bias_cmd=$1
        ;;
    -gf|--generate_figures )
        shift
        generate_figures=$1
        ;;
    -u|--ForceUpdate )
        shift
        ForceUpdate=$1
        ;;
    -v|-debug|--verbose )
        verbose_string="--verbose"
        ;;
    -log|--logfile )
        shift
        logger=$1
        ;;
    esac
    shift
done

if [ ! -z $verbose_string ]; then
    debug=true
else
    debug=false
fi


#Check that the ANTs executable is on the path
command -v $ANTS_bias_cmd >/dev/null 2>&1 || { echo "The ANTS command $ANTS_bias_cmd is required, but was not found. Aborting." >&2; exit 1; }
ANTS_bias_cmd=`command -v $ANTS_bias_cmd`


if [ ! -f ${T1_volume_nii} ]; then
    echo "ERROR: T1 volume: ${T1_volume_nii} doesn't exist"
    exit 1
fi 

if [ ! -d ${output_dir} ]; then
    echo "Creating T1 normalization output directory: ${output_dir}"
    mkdir -p "${output_dir}"
fi

seg_name=${output_dir}/T1Segment
#seg_name determines the base of the filenames for the output
#cmind_segment.py sets the output directory based on the directory extracted from seg_name rather than also passing output_dir as a parameter


#existence checks for all required conditional arguments
if [ -z "$fname_brain" ]; then
   echo "missing required argument: fname_brain"
fi


#All conditional arguments must be in the following list or they will not be passed onto python
conditionals_array=( fname_brain seg_name reg_struct use_prior use_alt_prior use_prior_throughout resegment_using_N4mask ANTS_bias_cmd generate_figures ForceUpdate logger  )

#build a list of all conditional arguments that aren't empty
for cond_arg in  "${conditionals_array[@]}"
do
    cond_val=$cond_arg     #name of variable
    cond_val=${!cond_val}  #corresponding argument
    if [ ! -z "$cond_val" ]; then   #is the argument non-empty?
        all_optional_args="${all_optional_args} --$cond_arg $cond_val"
    fi
done

t=$(timer)
echo "python ${cmind_python_dir}/cmind_segment.py ${verbose_string} ${all_optional_args}"
python ${cmind_python_dir}/cmind_segment.py ${verbose_string} ${all_optional_args}
printf 'cmind_segment.py:  Elapsed time: %s\n' $(timer $t)

#print output filenames for capture by LONI pipeline
#The following options should be printed by the python function above:
#    WM_vol
#    GM_vol
#    CSF_vol


# #shell script version doesn't currently support changing the following arguments (these are mostly untested and may not work properly if changed)
# fixed_arguments="--use_prior False --use_alt_prior False --use_prior_throughout False --reg_struct None"

# #the following lines replace the previous cmind_age_to_template.sh script
# t=$(timer)
# echo "python ${cmind_python_dir}/cmind_segment.py ${T1_brain_volume_nii} --seg_name=$seg_name --generate_figures=$generate_figures --ForceUpdate $ForceUpdate $verbose_str $fixed_arguments"
# python ${cmind_python_dir}/cmind_segment.py ${T1_brain_volume_nii} --seg_name=$seg_name --generate_figures=$generate_figures --ForceUpdate $ForceUpdate $verbose_str $fixed_arguments
# printf 'Segmentation:  Elapsed time: %s\n' $(timer $t)

#check that T1Segment_WMedge.nii.gz exists
output_file_test=${seg_name}_WMedge.nii.gz
if [ ! -f $output_file_test ] ; then
    echo "ERROR: Failed to finish Segmentation"
    exit 1
fi 

echo "Finished `basename $0`"
exit 0
