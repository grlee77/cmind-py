#!/usr/bin/env python

import os, urllib, tarfile
from os.path import join as pjoin

def download_CMIND_templates(destination_dir=None, remove_intermediate_files = True):
    """ retrieves C-MIND templates from the web.
    
    Parameters
    ----------
    destination_dir : str
        location where the templates will be stored
    remove_intermediate_files : str
        if True, remove the downloaded .tar.gz files after extraction
        
    """
    
    if destination_dir is None:
        raise ValueError("Must specify the destination directory")
    
    if not os.path.exists(destination_dir):
        os.makedirs(destination_dir)
    elif not os.path.isdir(destination_dir):
        raise ValueError("Specified destination_dir must be a directory")
        
        
    #FancyURLopener auto-handles redirect errors, etc.
    testfile = urllib.FancyURLopener() 
    
    template_list = ['http://bitbucket.org/grlee77/cmind-py/downloads/00to06_2mm.tar.gz',
                     'http://bitbucket.org/grlee77/cmind-py/downloads/06to24_2mm.tar.gz',
                     'http://bitbucket.org/grlee77/cmind-py/downloads/24to48_2mm.tar.gz',
                     'http://bitbucket.org/grlee77/cmind-py/downloads/48to216_2mm.tar.gz']
                     
    for template in template_list:
        dest_file = pjoin(destination_dir,os.path.basename(template))
        print("Downloading {} to destination:  {}".format(template, dest_file))
        
        if os.path.exists(dest_file):
            print("destination file already exists... skipping download")
        else:
            try:
                testfile.retrieve(template, dest_file)
            except:
                raise ValueError("Download failed.  Please manually download the following files and extract them: {}".format(template_list))
            
        print("Extracting  {}".format(dest_file))
        tar = tarfile.open(dest_file, "r:gz")
        tar.extractall(destination_dir)
        
        print("Deleting  {}".format(dest_file))    
        if remove_intermediate_files:
            os.remove(dest_file)
            
    print("Template download and extraction completed.\n\n")
    print("Please set environment variable CMIND_TEMPLATE_DIR to: {}".format(destination_dir))
    print("e.g. in bash, add the following line to ~/.bashrc")
    print("export CMIND_TEMPLATE_DIR={}".format(destination_dir))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="CMIND Template Downloader", epilog="")  #-h, --help exist by default
    #example of a positional argument
    
    parser.add_argument("destination_dir", help="location where the templates will be stored")
    parser.add_argument("-r","--remove_intermediate_files",type=str,default="False", help="if True, remove the downloaded .tar.gz files after extraction")
        
    args=parser.parse_args()
        
    destination_dir=args.destination_dir;
    remove_intermediate_files=args.remove_intermediate_files;
   
    download_CMIND_templates(destination_dir = destination_dir,
                             remove_intermediate_files = remove_intermediate_files)       
                             
