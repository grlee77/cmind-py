"""
Global variables, pathnames, version information, etc.

"""

from __future__ import division, print_function, absolute_import

__all__ = [
    'cmind_verbosity_level',       # control verbosity
    'cmind_template_dir',          # cmind templates are here
    'cmind_shellscript_dir',       # cmind shell scripts are here
    'cmind_outliers_cmd',          # outlier script is here
    'cmind_annotateimg_cmd',       # image annotation script is here
    'cmind_ASLBOLD_dir',           # ASL/BOLD Feat .fsf and .mat files
    'CMIND_DTIHARDI_DIR',          # DTI/HARDI QA template files are here
    'cmind_FSL_dir',               # $FSLDIR
    'fsl_version',                 # FSL version number
    'cmind_MNI_dir',               # MNI standard brains are here
    'cmind_ANTs_dir',              # ANTs binaries are here
    'cmind_ANTs_N4',               # ANTs N4 bias-field correction binary
    'cmind_AFNI_dir',              # AFNI binaries are here
    'has_ImageMagick',             # is ImageMagick available?
    'has_GraphicsMagick',          # is GraphicsMagick available?
    'cmind_imageconvert_cmd',      # ImageMagick/GraphicsMagick binary
    'cmind_Slicer3D',              # 3D Slicer binary location
    'cmind_Slicer3D_libs',         # 3D Slicer library location
    'cmind_Slicer3D_cli_dir',      # 3D Slicer CLI directory
    'cmind_Slicer3D_env',          # 3D slicer environment vars
    'cmind_DTIPrep',               # the DTIPrep binary
    'print_environment_info',      # print out version information
    'cmind_atlas_mindboggle',         # mindboggle atlas
    'cmind_atlas_labels_mindboggle',  # mindboggle atlas labels
    'cmind_atlas_AAL',                # AAL atlas
    'cmind_atlas_labels_AAL',         # AAL atlas labels
    # antsRegistration version-dependent initialization string
    'cmind_ANTs_init_COM_string',
]

import os
import warnings
import sys
import subprocess
from os.path import join as pjoin

from cmind.finders import (find_FSL,
                           find_ANTS,
                           find_AFNI,
                           find_DTIPrep,
                           find_ImageMagick,
                           find_GraphicsMagick,
                           find_Slicer3D,
                           get_Slicer3D_version,
                           find_MNI_templates,
                           find_shellscripts)

warn_on_missing = False

# control verbosity level when verbose=True
# if > 1, stdout and stderr from the from all FSL, ANTs or other shell
# commands will be captured to the log files
cmind_verbosity_level = 2

# path to example datasets
if os.getenv('CMIND_EXAMPLE_DIR'):
    cmind_example_dir = os.getenv('CMIND_EXAMPLE_DIR')
else:
    cmind_example_dir = os.path.realpath(
        pjoin(
            os.path.dirname(
                os.path.realpath(__file__)), 'data'))
if cmind_example_dir and (not os.path.isdir(cmind_example_dir)):
    cmind_example_dir = None

# path where output of tests on example data should be stored
if os.getenv('CMIND_EXAMPLE_OUTPUT_DIR'):
    cmind_example_output_dir = os.getenv('CMIND_EXAMPLE_OUTPUT_DIR')
else:
    from tempfile import mkdtemp
    cmind_example_output_dir = mkdtemp()

if cmind_example_output_dir and (not os.path.isdir(cmind_example_output_dir)):
    cmind_example_output_dir = None

# path to pediatric templates
if os.getenv('CMIND_TEMPLATE_DIR'):
    cmind_template_dir = os.getenv('CMIND_TEMPLATE_DIR')
else:
    cmind_template_dir = None
    search_locations = [os.path.expanduser('~/cmind_templates'),
                        '/usr/local/cmind_templates',
                        '/usr/share/cmind_templates',
                        '/opt/cmind_templates',
                        ]
    for loc in search_locations:
        if os.path.isdir(loc):  # TODO: check for an actual file
            cmind_template_dir = os.path.realpath(loc)
            break

if cmind_template_dir and (not os.path.isdir(cmind_template_dir)):
    cmind_template_dir = None

# path to cmind-py shell scripts
cmind_shellscript_dir = find_shellscripts()

if (not cmind_shellscript_dir) or (
        not os.path.isdir(cmind_shellscript_dir)):  # shell scripts not found
    cmind_shellscript_dir = None
    cmind_outliers_cmd = None
    cmind_annotateimg_cmd = None
else:  # path to specific shell commands
    cmind_outliers_cmd = pjoin(cmind_shellscript_dir, 'fsl_motion_outliers_v2')
    if not os.path.exists(cmind_outliers_cmd):
        cmind_outliers_cmd = None
    cmind_annotateimg_cmd = pjoin(
        cmind_shellscript_dir,
        'annotate_img_glm1.sh')
    if not os.path.exists(cmind_annotateimg_cmd):
        cmind_annotateimg_cmd = None

# path to ASL/BOLD fMRI .fsf and .mat GLM design files for FSL's Feat
if os.getenv('CMIND_ASLBOLD_DIR'):
    cmind_ASLBOLD_dir = os.getenv('CMIND_ASLBOLD_DIR')
else:
    cmind_ASLBOLD_dir = os.path.realpath(
        pjoin(os.path.dirname(os.path.realpath(__file__)),
              '..', 'study_specific', 'ASLBOLD'))
if cmind_ASLBOLD_dir and (not os.path.isdir(cmind_ASLBOLD_dir)):
    cmind_ASLBOLD_dir = None

# path to DTI/HARDI b-value and b-vector files and DTIPrep templates
if os.getenv('CMIND_DTIHARDI_DIR'):
    CMIND_DTIHARDI_DIR = os.getenv('CMIND_DTIHARDI_DIR')
else:
    CMIND_DTIHARDI_DIR = os.path.realpath(
        pjoin(os.path.dirname(os.path.realpath(__file__)),
              '..', 'study_specific', 'DTIPrep_protocols'))
if CMIND_DTIHARDI_DIR and (not os.path.isdir(CMIND_DTIHARDI_DIR)):
    CMIND_DTIHARDI_DIR = None

# find paths and version info for several external software packages
(cmind_FSL_dir, fsl_version) = find_FSL()
(cmind_ANTs_dir, ANTs_version) = find_ANTS()

if cmind_ANTs_dir:

    # Kludge to try to deal with different useCenterofMass syntax across ANTs
    # versions
    try:
        # if "fixedImage,movingImage,useCenterOfMass" is in the help string, it
        # is an older ANTS version"
        shell_cmd = '{}/antsRegistration --help '.format(cmind_ANTs_dir) + \
                    '| grep "fixedImage,movingImage,useCenterOfMass"'
        stdout = subprocess.check_output(shell_cmd, shell=True)
        cmind_ANTs_init_COM_string = 'useCenterOfMass'
    except subprocess.CalledProcessError:
        cmind_ANTs_init_COM_string = '0'

    cmind_ANTs_N4 = pjoin(cmind_ANTs_dir, 'N4BiasFieldCorrection')
    if not os.path.exists(cmind_ANTs_N4):
        cmind_ANTs_N4 = None
else:
    cmind_ANTs_N4 = None
    cmind_ANTs_init_COM_string = '0'

(cmind_AFNI_dir, AFNI_version_info) = find_AFNI()
(cmind_DTIPrep, DTIPrep_version) = find_DTIPrep()
(has_ImageMagick, ImageMagick_version_info) = find_ImageMagick()
(has_GraphicsMagick, GraphicsMagick_version_info) = find_GraphicsMagick()


# use ImageMagick if present, otherwise try GraphicsMagick
if has_ImageMagick:
    cmind_imageconvert_cmd = 'convert'
elif has_GraphicsMagick:
    cmind_imageconvert_cmd = 'gm convert'
    has_ImageMagick = True  # for convenience, also set has_ImageMagick True
else:
    cmind_imageconvert_cmd = None

cmind_Slicer3D, cmind_Slicer3D_libs, cmind_Slicer3D_cli_dir, cmind_Slicer3D_env = find_Slicer3D()

# Slicer3D_version = get_Slicer3D_version(cmind_3DSlicer)  #Slow, so don't
# run by default
Slicer3D_version = None
cmind_MNI_dir = find_MNI_templates()

if warn_on_missing and (not cmind_FSL_dir):
    warnings.warn(
        'path to FSL could not be found.  Specify using environment '
        'variable FSLDIR')
if warn_on_missing and (not cmind_ANTs_dir):
    warnings.warn(
        'path to ANTs could not be found.  Specify using environment '
        'variable ANTSPATH')
if warn_on_missing and (not cmind_DTIPrep):
    warnings.warn(
        'DTIPrep not found on PATH. try setting environment variable '
        'CMIND_DTIPREP')
if warn_on_missing and (not cmind_Slicer3D):
    warnings.warn(
        '3D Slicer binaries not found.  specify explicitly via CMIND_3DSLICER'
        ' environment variable')
if warn_on_missing and (not cmind_AFNI_dir):
    warnings.warn(
        'path to AFNI could not be found.  Specify using environment variable'
        ' AFNIPATH')
if warn_on_missing and (not (has_GraphicsMagick or has_ImageMagick)):
    warnings.warn(
        'ImageMagick/GraphicsMagick convert not found on PATH. \n'
        'ImageMagick/GraphicsMagick will not be used')
if warn_on_missing and (not cmind_MNI_dir):
    warnings.warn(
        'path to MNI standard brains could not be found.  Specify using'
        ' environment variable MNIDIR')
if warn_on_missing and (not cmind_template_dir):
    warnings.warn(
        'path to Study-specific templates not be found.  Specify using '
        'environment variable CMIND_TEMPLATE_DIR')
if warn_on_missing and (not cmind_shellscript_dir):
    warnings.warn(
        'path to CMIND shell scripts could not be found.  Specify using '
        'environment variable CMIND_SHELLSCRIPT_DIR')
if warn_on_missing and (not CMIND_DTIHARDI_DIR):
    warnings.warn(
        'path to MNI standard brains could not be found.  Specify using '
        'environment variable CMIND_DTIHARDI_DIR')


# atlas directory
cmind_atlases_dir = os.path.realpath(
    pjoin(os.path.dirname(os.path.realpath(__file__)), 'atlases'))

# specific atlases
cmind_atlas_mindboggle = pjoin(
    cmind_atlases_dir,
    'OASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_MNI152_2mm.nii.gz')
cmind_atlas_AAL = pjoin(cmind_atlases_dir, 'aal_MNI152_2mm.nii.gz')
cmind_atlas_AAL_spm8 = pjoin(cmind_atlases_dir, 'aal_spm8_MNI152_2mm.nii.gz')
cmind_atlas_JHU = os.path.realpath(
    pjoin(os.environ['FSLDIR'], 'data', 'atlases', 'JHU',
          'JHU-ICBM-labels-2mm.nii.gz'))
cmind_atlas_JHU_tracts = os.path.realpath(
    pjoin(os.environ['FSLDIR'], 'data', 'atlases', 'JHU',
          'JHU-ICBM-tracts-maxprob-thr50-2mm.nii.gz'))

# warn about missing optional atlases
for vol in [cmind_atlas_mindboggle, cmind_atlas_AAL,
            cmind_atlas_AAL_spm8, cmind_atlas_JHU, cmind_atlas_JHU_tracts]:
    if not os.path.exists(vol):
        warnings.warn('Could not find the following atlas in the expected'
                      ' location:\n   {}'.format(vol))

# specific atlas labels
cmind_atlas_labels_mindboggle = pjoin(
    cmind_atlases_dir, 'mindboggle_101_labels.csv')
cmind_atlas_labels_AAL = pjoin(cmind_atlases_dir, 'AAL_labels.csv')
cmind_atlas_labels_AAL_spm8 = pjoin(cmind_atlases_dir, 'AAL_spm8_labels.csv')
cmind_atlas_labels_JHU = pjoin(cmind_atlases_dir, 'JHU_WM_labels.csv')
cmind_atlas_labels_JHU_tracts = pjoin(cmind_atlases_dir, 'JHU_WM_tracts.csv')


def print_package_versions(pkgs, indent_spaces=0):

    from cmind.utils.utils import reindent
    versions = []
    if isinstance(pkgs, str):
        pkgs = [pkgs, ]
    elif not isinstance(pkgs, (list, tuple)):
        raise ValueError("pkgs must be a string, or list of strings")
    for pkg in pkgs:
        try:
            exec('import {}'.format(pkg))
            try:
                try:
                    v = eval('%s.__version__' % pkg)
                # if __version__ not found, try alternates
                except AttributeError:
                    try:
                        v = eval('%s.VERSION' % pkg)
                    except AttributeError:
                        v = eval('%s.version' % pkg)
                ostr = reindent("{}: {}".format(pkg, v), indent_spaces) + '\n'
            except Exception:
                ostr = reindent(
                    "{}: version unknown".format(pkg), indent_spaces) + '\n'
        except Exception:
            ostr = reindent("{} not found".format(pkg), indent_spaces) + '\n'
        versions.append(ostr)
    return versions


def print_environment_info(Slicer3D_version=None, extended_info=False):
    """ Print version information for Python packages and external binaries
    used in cmind-py
    """
    from cmind.utils.utils import reindent
    try:  # Python 2
        from cStringIO import StringIO
    except ImportError:  # Python 3
        from io import StringIO

    out = StringIO()
    out.write("\nVERSION INFORMATION\n")
    out.write("===================\n\n")
    out.write("Python: {}\n".format(sys.executable))
    out.write(reindent(sys.version, 8) + "\n")
    out.write(reindent("platform: {}".format(sys.platform), 8))

    packages = ['cmind',
                'numpy',
                'matplotlib',
                'scipy',
                'nibabel']

    if extended_info:
        packages = packages + ['nipype',
                               'dipy',
                               'pandas',
                               'nipy',
                               'skimage',
                               'sympy',
                               'nitime',
                               'mayavi']

    # write all requested python package versions
    out.write('\n\n')
    out.writelines(print_package_versions(packages, indent_spaces=4))

    # write external software versions
    out.write('\n\n')
    if cmind_FSL_dir:
        out.write("FSL folder: {}\n".format(cmind_FSL_dir))
        out.write(reindent('version: ' + fsl_version, 4) + '\n\n')
    else:
        out.write("FSL not found\n")
    if cmind_ANTs_dir:
        out.write("ANTs binaries in: {}\n".format(cmind_ANTs_dir))
        out.write(reindent('version: ' + ANTs_version, 4) + '\n\n')
    else:
        out.write("ANTs not found\n")
    if cmind_AFNI_dir:
        out.write("AFNI binaries in: {}\n".format(cmind_AFNI_dir))
        if AFNI_version_info:
            out.write(
                reindent(
                    AFNI_version_info.replace(
                        'Version',
                        'version:'),
                    4) +
                '\n\n')
        else:
            out.write(reindent('version: unknown', 4) + '\n\n')
    else:
        out.write("AFNI not found\n")
    if cmind_DTIPrep:
        out.write("DTIPrep binary: {}\n".format(cmind_DTIPrep))
        if DTIPrep_version:
            out.write(reindent('version: ' + DTIPrep_version, 4) + '\n\n')
        else:
            out.write(reindent('version: unknown', 4) + '\n\n')
    else:
        out.write("DTIPrep not found\n")
    if cmind_Slicer3D:
        if not Slicer3D_version:
            Slicer3D_version = get_Slicer3D_version(cmind_Slicer3D)
        out.write("3D Slicer: {}\n".format(cmind_Slicer3D))
        if Slicer3D_version:
            out.write(reindent('version: ' + Slicer3D_version, 4) + '\n\n')
        else:
            out.write(reindent('version: unknown', 4) + '\n\n')
    else:
        out.write("3D Slicer not found\n")
    if find_ImageMagick()[0]:
        out.write("ImageMagick:\n")
        out.write(
            reindent(
                ImageMagick_version_info.replace(
                    'Version:',
                    'version:'),
                4) +
            '\n\n')
    elif has_GraphicsMagick:
        out.write("GraphicsMagick:\n")
        out.write(
            reindent(
                GraphicsMagick_version_info.replace(
                    'Version:',
                    'version:'),
                4) +
            '\n\n')
    else:
        out.write("Neither GraphicsMagick or ImageMagick found\n\n")
    if CMIND_DTIHARDI_DIR:
        out.write(
            "Found CMIND DTI/HARDI files in:  {}\n".format(CMIND_DTIHARDI_DIR))
    else:
        out.write(
            "could not find CMIND DTI/HARDI files, specify using environment "
            "variable: CMIND_DTIHARDI_DIR\n")
    if cmind_shellscript_dir:
        out.write(
            "Found CMIND shell scripts in:  {}\n".format(
                cmind_shellscript_dir))
    else:
        out.write(
            "could not find CMIND shell scripts, specify using environment "
            "variable: CMIND_SHELLSCRIPT_DIR\n")
    if cmind_template_dir:
        out.write(
            "Found CMIND study templates in:  {}\n\n".format(
                cmind_template_dir))
    else:
        out.write(
            "could not find CMIND study templates, specify using environment "
            "variable: CMIND_TEMPLATE_DIR\n\n")
    print(out.getvalue())
    return out.getvalue()
