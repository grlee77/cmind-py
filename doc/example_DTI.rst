QC Data files
-------------

If the DTI_QC data is downloaded for subject 06M008_P_1, the following files will be extracted:

- **IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCReport.txt**
- **IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCReport.xml**
- **IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCed.nii.gz**
- **IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCed.tar.gz**
- **IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_bvals.txt**
- **IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_bvecs.txt**
- **IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_dirs_kept.txt**


DTI Toolkit: obtaining FA, ADC and tensor information via dti_recon
-------------------------------------------------------------------

When using `DTI toolkit <http://http://trackvis.org>`_, it is often much faster to work with NIFTI rather than gzipped NIFTI format data::

	gunzip IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCed.nii.gz


`DTI Toolkit <http://http://trackvis.org>`_ can be run as follows::

	dti_recon IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCed.nii DTI -gm IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_bvecs.txt -b IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_bvals.txt -b0 1 -ot nii


The outputs of this run will be:

- **DTI_adc.nii** - apparent diffusion coefficient (ADC) map
- **DTI_b0.nii** - averaged low b (b0) map
- **DTI_dwi.nii** - averaged diffusion weighted image map
- **DTI_e1.nii** - 1st eigenvalue (largest)
- **DTI_e2.nii** - 2nd eigenvalue
- **DTI_e3.nii** - 3rd eigenvalue (smallest)
- **DTI_exp.nii**
- **DTI_fa.nii** - fractional anistropy (FA) map
- **DTI_fa_color.nii** - color FA map
- **DTI_tensor.nii** - diffusion tensor data.  4D NIFTI with 4th dimension as size 6
- **DTI_v1.nii** - 1st eigenvector  (5D, 4th dimension is size 1)
- **DTI_v2.nii** - 2nd eigenvector  (5D, 4th dimension is size 1)
- **DTI_v3.nii** - 3rd eigenvector  (5D, 4th dimension is size 1)

Create ADC and FA summary images::

	$FSLDIR/bin/slicer DTI_fa.nii -a DTI_fa.png
	$FSLDIR/bin/slicer DTI_adc.nii -a DTI_adc.png

.. figure::  img/06M008_P_1_DTI_fa.png
   :align:   center
   
   3-plane FA image   	

.. figure::  img/06M008_P_1_DTI_adc.png
   :align:   center

   3-plane ADC image   	


Create a mask of only the non-zero voxels in FA the output::

	$FSLDIR/bin/fslmaths DTI_fa -thr 0 -bin DTI_mask
	$FSLDIR/bin/fslchfiletype NIFTI DTI_mask

DTI tractography using DTI Toolkit
----------------------------------

Run DTI tractography on the output (angle threshold = 35 & using an FA threshold of 0.15)::

	mkdir DTI_temp
	dti_tracker DTI DTI_temp/tmp.trk -at 35 -m DTI_mask.nii -m2 DTI_fa.nii 0.15 -it nii
	spline_filter DTI_temp/tmp.trk 1 DTI_tracks.trk 

Create summary images in each plane using the command line version of trackvis  (-l specifies the track length threshold)::

	track_vis DTI_tracks.trk -l 30 -camera azimuth 0 elevation 0 -sc view1.png  
	rm tmp.cam
	track_vis DTI_tracks.trk -l 30 -camera azimuth 90 elevation 0 -sc view2.png 
	rm tmp.cam
	track_vis DTI_tracks.trk -l 30 -camera azimuth 0 elevation 90 -sc view3.png
	rm tmp.cam

Combine the 3 orthogonal views into a single image::

	$FSLDIR/bin/pngappend view1.png + view2.png + view3.png DTI_tracks_3plane.png

.. figure::  img/06M008_P_1_DTI_tracks_3plane.png
   :align:   center
   
   DTI Tractography Result


HARDI/ODF tractography using DTI Toolkit
----------------------------------------

Run Q-Ball tractography on the output (angle threshold = 35 & using an FA threshold of 0.15)::

	mkdir ODF_temp
	tail -n+2 IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_bvecs.txt > bvecs_noB0.txt
	hardi_mat bvecs_noB0.txt ODF_temp/temp_mat.dat -ref IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCed.nii
	export NDIRECTIONS=`wc -l IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_bvecs.txt | cut -d " " -f 1`
	odf_recon IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCed.nii $NDIRECTIONS 181 ODF -b0 0 -mat ODF_temp/temp_mat.dat -ot nii -iop 1 0 0 0 1 0
	odf_tracker ODF ODF_temp/tmp.trk -at 35 -m DTI_mask.nii -m2 DTI_fa.nii 0.15 -it nii
	spline_filter ODF_temp/tmp.trk 1 ODF_tracks.trk 

Note that for the ODF case, the tail command was used to return a version of the b-vector table without the b=0 element at the top.  We then specify -b0 0 in the call to odf_recon.  We store the number of directions (including b=0) into the $NDIRECTIONS environment variable.

Create summary images in each plane using the command line version of `trackvis <http://http://trackvis.org>`_  (-l specifies the track length threshold)::

	track_vis ODF_tracks.trk -l 30 -camera azimuth 0 elevation 0 -sc view1_ODF.png  
	rm tmp.cam
	track_vis ODF_tracks.trk -l 30 -camera azimuth 90 elevation 0 -sc view2_ODF.png 
	rm tmp.cam
	track_vis ODF_tracks.trk -l 30 -camera azimuth 0 elevation 90 -sc view3_ODF.png
	rm tmp.cam
	$FSLDIR/bin/pngappend view1_ODF.png + view2_ODF.png + view3_ODF.png ODF_tracks_3plane.png


FSL's dtifit: obtaining FA, ADC and tensor information
------------------------------------------------------

The provided b-value files are single column and the b-vector files are 3-column.  For use with FSL the columns will need to be transposed to rows before these files can be used for further processing.  This can be done a number of ways.  For users on linux or MacOsX, the following bash script should do the trick (**ADD LINK TO transpose_dirs.sh**) as follows::

	./transpose_dirs.sh IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_bvals.txt bvals_transposed.txt
	./transpose_dirs.sh IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_bvecs.txt bvecs_transposed.txt

dtifit will also require a brain mask.  A very lenient one (that will include non-brain tissue, but at least mask out the background) can be generated as follows::

	bet IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCed.nii brain -f 0.1 -m

The generated brain_mask.nii.gz file can now be used as the mask input to dtifit.  dtifit can now be called as follows::

	dtifit --data=IRC04H_06M008_P_1_WIP_WIP_DTI_68_iso_32chSHC_SENSE_20_1_QCed --out=FSL_DTIFIT --bvecs=bvecs_transposed.txt --bvals=bvals_transposed.txt --mask=brain_mask --save_tensor --sse

This will generate the following outputs:  `dtifit documentation:  <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FDT/UserGuide#DTIFIT>`_
	
- **DTI_FSL_FA.nii.gz** - fractional anisotropy
- **DTI_FSL_L1.nii.gz** - 1st eigenvalue
- **DTI_FSL_L2.nii.gz** - 2nd eigenvalue
- **DTI_FSL_L3.nii.gz** - 3rd eigenvalue
- **DTI_FSL_MD.nii.gz** - mean diffusivity
- **DTI_FSL_MO.nii.gz** - mode of the anisotropy (oblate -1; isotropic 0, prolate 1)
- **DTI_FSL_S0.nii.gz** - raw T21 signal with no diffusion weighting
- **DTI_FSL_V1.nii.gz** - 1st eigenvector
- **DTI_FSL_V2.nii.gz** - 2nd eigenvector
- **DTI_FSL_V3.nii.gz** - 3rd eigenvector
- **DTI_FSL_tensor.nii.gz** - tensor as a 4D file in this order: Dxx,Dxy,Dxz,Dyy,Dyz,Dzz 
- **DTI_FSL_sse.nii.gz** - sum of squared error 

.. TODO:  FSL output _tensor seems to have volumes 0,1,4,5 matching DTI Toolkit.  2 and 3 appear to be swapped.  problem with table or just a different convention?





