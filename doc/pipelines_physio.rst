.. _pipelines_physio:

CBF and T1 map Processing
=========================

This section describes the current processing stream for Baseline CBF data.

CBF preprocessing
-----------------

The preprocessing module incorporates the following:

- motion correction (via FSL's mcflirt)
- outlier rejection
- intensity normalization

Outlier rejection is performed using the series of tag-control images after the
motion correction stage. This is criteria to determine outliers is based on the
interquartile range of mean-squared-error in intensity differences as described 
in more detail in the functional pipeline section on 
:ref:`outlier_detection <outlier_detection>`.

Intensity normalization is performed to remove the non-uniform coil sensitivity
from the BaselineCBF images prior to quantification.

In addition to the above processing, summary images and volumes corresponding 
to the mean percent signal change and average intensity normalized subtraction 
volume are produced.

pipeline module: ``cmind_baselineCBF_preprocess.py``

T1 estimation preprocessing
---------------------------

This script takes a series of volumes acquired at different inversion times and
prepares them for later fitting to an inversion recovery model.  The individual
files are sorted by TI value, coregistered and stored in a 4D file named 
``T1concat_mcf.nii.gz``.  The TI values are stored in a corresponding ``TIvals.txt``
file.

.. Note:: by default an initial estimation of motion is performed using mcflirt.  
    If no motion exceeding half a voxel is present, motion correction is not 
    performed to avoid potential introduction of errors via misregistration of 
    already aligned volumes of differing contrast.  When substantial motion
    is detected the motion corrected timeseries is returned.

pipeline module: ``cmind_T1map_preprocess.py``

T1 map fitting
--------------

This script takes the 4D NIFTI series and TI values from T1 map preprocessing
and performs a fit to the following inversion recovery model on a voxelwise
basis:


.. math::

    \^{\vec{s}} = M_0*(1-2*\alpha*exp^{-\vec{TI}/T1})

where :math:`\alpha`, :math:`T1` and :math:`M_0` are the three parameters being 
fit.  The normalized root-mean-square error (NRMSE) between the data, :math:`\vec{s}`, 
and the fit :math:`\^{\vec{s}}` is also evaluated and stored.

.. Note:: To improve the robustness of the fits, an initial fit on a small 
    subset of the voxels is performed.  The level of error between the fit and
    each TI value is then evaluated to look for potential outliers.  Up to three
    outlying TI values may be discarded in this manner and then the full fit is
    only performed on the surviving TI timepoints.

pipeline module: ``cmind_T1map_fit.py``

Coregistration of T1map and CBF volumes
---------------------------------------

The T1 map is rigidly coregistered to the mean BaselineCBF subtraction volume.

.. Note:: Both acquisitions utilized the same EPI readout parameters so any susceptibility
    related distortions are expected to be the same.

script: ``cmind_baselineCBF_coregister.py``

Inversion Efficiency
--------------------

A series of 10 control/tag pairs were acquired using the same labling settings 
as in the BaselineCBF series, but with the imaging volume set as a single slice
approximately 1 cm above the labeling plane location.  The peak control/tag 
intensity difference is estimated from this scan to get an estimate of labeling
efficiency.  The ``alpha.txt`` file contains the estimated efficiency.  

TODO: more detail of how the selection of voxels is done during the estimation
stage

.. Note:: This is a relatively crude estimate of labeling efficiency, but in our
    experience using it does reduce the variance in fitted CBF values across
    subjects.  It is imperfect in that there may be some partial-volume effects and 
    it is possible that in practice, the efficiency may vary from vessel to vessel, 
    while this routine returns a single efficiency number for the scan.

pipeline module: ``cmind_alpha.py``

Registration to Structural Space
--------------------------------

The mean BaselineCBF subtraction volume was used for registration to the 
T1-weighted structural volume.  For subjects without fieldmap data, this is an
affine boundary-based registration [Greve2009]_ as implemented in FLIRT 
[Jenkinson2002]_.  For those subjects with fieldmap data, epi unwarping is also
applied via FUGUE [Jenkinson2001]_.

pipeline module: ``cmind_fieldmap_regBBR.py``

CBF quantification
------------------

CBF is quantified using the approach of [Wang2002]_, Eq. 1.  Measured values for
inversion efficiency and tissue T1 were used.  The labeling duration was 2.0 
seconds and the post-labeling delay was 1.4 seconds.  The T1 decay rate of 
arterial blood was assumed to be 1.6 seconds, while the transit times to the 
arterial and tissue water compartments were assumed to be 1.3 and 1.5 seconds, 
respectively.

:math:`M0_{arterial}` was estimated from the white matter signal intensity in the 
BaselineCBF control images after correction for differences in proton density 
and T2* decay.  The TR of the BaselineCBF scan was 4 seconds, so the white matter 
(where T1<1 s) can be assumed fully relaxed.  The acquisition used a gradient echo 
readout with TE of 11.5 ms.   A small correction for the relative :math:`T2*` 
decay of blood vs WM was applied, based on literature values for the relative 
decay rates.

The CBF values output from this module are currently not corrected for partial
volume effects.  Partial volume correction is likely to be added in the near 
future.

TODO: add additional references here

pipeline module: ``cmind_baselineCBF_quantitative.py``

Normalization to Standard Space
-------------------------------

Quantitative CBF maps were registered to MNI space by concatenating the 
``low resolution CBF``->``high resolution structural`` transform with the 
transform from anatomical to MNI space.  The anatomical->MNI transformation 
was described in the section on :ref:`structural processing<Normalization>`.  

pipeline module: ``cmind_apply_normalization.py``




