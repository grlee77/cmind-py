#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def cmind_fMRI_norm(output_dir, reg_struct,regBBR_dir,prep_dir, stats_dir, ANTS_path=None,reg_flag='001', output_tar=True, generate_figures=True, ForceUpdate=False, verbose=False, logger=None, update_reg_struct=False):
    """Apply previously computed transforms to normalize fMRI results to MNI
    standard space
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    reg_struct : str or dict
        filename of the .csv containing the registration structure
    regBBR_dir : str
        pathname to the BBR registration results (rigid from functional to structural)
    prep_dir : str
        directory containing the preprocessed fMRI volumes from cmind_fMRI_preprocess2
    stats_dir : str
        directory containing the first-level stats results
    ANTS_path : str, optional
        path to the ANTs binaries
    reg_flag : str or int, optional
        string of 3 characters controlling whether FLIRT, FNIRT and/or ANTS registrations
        of T1 to standard are to be run. 
        doFLIRT = 0 or 1  (1 = do the regisration using FLIRT). 
        doFNIRT = 0 or 1  (1 = do the regisration using FNIRT) (NOT CURRENTLY IMPLEMENTED). 
        doANTS = 0, 1 or 2  (0 = skip, 1 = do using Exp, 2 = do using SyN)
    output_tar : bool, optional
        compress the registration folders into a .tar.gz archive
    generate_figures : bool,optional
        if true, generate overlay images summarizing the registration
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    
    Returns
    -------
    reg_tarfile : str
        filename of the .tar.gz filenames corresponding to the lowres->highres 
        registration results
    reg_standard_tarfile : dict or str
        dictionary containing the .tar.gz filenames corresponding to 'FLIRT', 
        'FNIRT', or 'ANTS' to MNI registration results.  Will just be a str if
        only a single case was run.
    reg_dir : str
        directory containing highres registration
    reg_standard_dir : str
        directory containing standard space registration
        
    Notes
    -----
    To run a second level analysis, the /reg folder must have at least:  highres.nii.gz, example_func2standard.mat
    If feat is being used for 2nd level analysis can remove tstat*.nii.gz from reg_standard/stats
    
    """

    import os
    import glob
    import shutil
    import warnings
    from os.path import join as pjoin
    
    import numpy as np
    
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist, csv2dict, split_multiple_ext, multiglob_list, export_tarfile #, rm_broken_links
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info
    from cmind.pipeline.cmind_apply_normalization import cmind_apply_normalization
    
    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose = input2bool([generate_figures, ForceUpdate, verbose])

    if generate_figures:
        from cmind.pipeline.cmind_calculate_slice_coverage import cmind_calculate_slice_coverage

    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting fMRI Normalization")      

    if not ANTS_path:
        from cmind.globals import cmind_ANTs_dir as ANTS_path
            
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
            
    if isinstance(reg_struct,str):
        if os.path.exists(reg_struct):
            reg_struct=csv2dict(reg_struct)
        else:
            raise IOError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
    elif not isinstance(reg_struct,dict):
        raise ValueError("reg_struct must either be the registration structure, reg_struct, or a path to the .csv file containing it")
    
    if update_reg_struct:
        from cmind.utils.cmind_utils import _convert_reg_struct
        reg_struct=_convert_reg_struct(reg_struct,target='New',rename_files='False')            
                                        
    if isinstance(reg_flag,str):
        if len(reg_flag)==1:
            reg_flag=int(reg_flag);
            reg_flag=bin(reg_flag)[2::].zfill(3);
    
    if len(reg_flag)==3:
        reg_flag_array=np.zeros((3,),int); 
        reg_flag_array[0]=int(reg_flag[0])
        reg_flag_array[1]=int(reg_flag[1])
        reg_flag_array[2]=int(reg_flag[2])
    else:
        raise ValueError("reg_flag should be a string of three integer values")

    do_flirt=reg_flag_array[0]
    do_fnirt=reg_flag_array[1]
    do_ANTS=reg_flag_array[2]    #0=none, 1= Exp, 2=SyN
    
    cases_to_run=[];
    if do_flirt:
        cases_to_run.append('FLIRT')
    if do_fnirt:
        cases_to_run.append('FNIRT')
    if do_ANTS:
        cases_to_run.append('ANTS')
        

    if not os.path.exists(stats_dir):
        raise IOError("stats directory was not found")
    
    reg_dir = pjoin(output_dir,'reg')
#    reg_dir = pjoin(output_dir,'reg_' + stats_settings_str)
    if output_tar: 
        #reg_tarfile=pjoin(output_dir,'reg_' + stats_settings_str + '.tar.gz')
        reg_tarfile=pjoin(output_dir,'reg.tar.gz')
    else:
        reg_tarfile=None
        
    module_logger.debug("reg_dir={}".format(reg_dir))  
     
    use_links_instead_of_copies=True  #if True, will use soft links instead of copies to save disk space
    if (not os.path.exists(pjoin(reg_dir,'example_func2standard.mat'))) or ForceUpdate:  #copy previously performed regBBR registration here
        if not os.path.exists(reg_dir):
            module_logger.debug("creating reg_dir directory")  
            os.makedirs(reg_dir)
        elif ForceUpdate: #remove old registration folder
            shutil.rmtree(reg_dir)
            os.makedirs(reg_dir)
            
        log_cmd('$FSLDIR/bin/imcp "%s"/* %s' % (regBBR_dir,reg_dir), verbose=verbose, logger=module_logger);
        log_cmd('cp "%s"/*.mat %s' % (regBBR_dir,reg_dir), verbose=verbose, logger=module_logger);
        #log_cmd('cp %s/*.png .' % (regBBR_dir), verbose=verbose, logger=module_logger)  #example_func2highres.png
        log_cmd('$FSLDIR/bin/imrm %s' % pjoin(reg_dir,'EPIvol'), verbose=verbose, logger=module_logger)
        
        #some renaming from "lowres" to "example_func" to match FSL's feat conventions
        lr_files = glob.glob(pjoin(reg_dir,'*lowres*'))
        for fname in lr_files:
            log_cmd('mv "%s" "%s"' % (fname,fname.replace('lowres','example_func')), verbose=verbose, logger=module_logger)
        
        str1='example_func2highres_inv.mat'
        str1_root, str1_ext=split_multiple_ext(str1)
        
        #change names such as XXX2YYY_inv.* to YYY2XXX.* to match FSL's feat conventions (e.g. rename example_func2highres_inv.mat to highres2example_func.mat)
        filelist=multiglob_list((pjoin(reg_dir,'*_inv.mat'),pjoin(reg_dir,'*_warp_inv*')))
        for f in filelist:   
            if f.find('_inv')!=-1: 
                f_root, f_ext=split_multiple_ext(f)
                f_root = os.path.basename(f_root) #remove reg_dir and then add it back later
                f2=f_root.split('_inv')[0]
                f_new='2'.join(f2.split('2')[::-1])+f_ext
                f_new=pjoin(reg_dir,f_new)  
                log_cmd('mv %s %s' % (f, f_new))
                
        module_logger.debug("reg_struct = {}".format(reg_struct))
        module_logger.debug("reg_struct['HR_img'] = {}".format(reg_struct['HR_img']))

        if use_links_instead_of_copies:
            link_or_copy_str='ln -s -f'
        else:
            link_or_copy_str='cp'
        
        try:
            log_cmd('%s "%s" "%s"' % (link_or_copy_str, reg_struct['HR_img'],pjoin(reg_dir,'highres_brain.nii.gz')), verbose=verbose, logger=module_logger)
        except OSError as e:
            if use_links_instead_of_copies:
                warnings.warn("Unable to generate symbolic links.  copying files instead...")
                log_cmd('%s "%s" "%s"' % (link_or_copy_str, reg_struct['HR_img'],pjoin(reg_dir,'highres_brain.nii.gz')), verbose=verbose, logger=module_logger)
                use_links_instead_of_copies=False
                link_or_copy_str='cp'
            else:
                print("OSError({0}): {1}".format(e.errno, e.strerror))
            
        log_cmd('%s "%s" "%s"' % (link_or_copy_str, reg_struct['MNI_standard_brain'],pjoin(reg_dir,'standard.nii.gz')), verbose=verbose, logger=module_logger)
        log_cmd('%s "%s" "%s"' % (link_or_copy_str, reg_struct['HR_img'].replace('_brain',''),pjoin(reg_dir,'highres.nii.gz')), verbose=verbose, logger=module_logger)
        
        if 'FLIRT_StudyTemplate_to_MNI_img' in reg_struct:
            log_cmd('%s "%s" "%s"' % (link_or_copy_str, reg_struct['FLIRT_StudyTemplate_to_MNI_img'],pjoin(reg_dir,'highres2standard.nii.gz')), verbose=verbose, logger=module_logger)
        if 'FLIRT_StudyTemplate_to_MNI_Aff' in reg_struct:
            log_cmd('%s "%s" "%s"' % (link_or_copy_str, reg_struct['FLIRT_StudyTemplate_to_MNI_Aff'],pjoin(reg_dir,'highres2standard.mat')), verbose=verbose, logger=module_logger)
        omat=pjoin(reg_dir,'example_func2standard.mat')
        aff1=pjoin(reg_dir,'highres2standard.mat')
        aff2=pjoin(reg_dir,'example_func2highres.mat')
        log_cmd("$FSLDIR/bin/convert_xfm -omat %s -concat %s %s" % (omat,aff1,aff2), verbose=verbose, logger=module_logger)  #example_func2highres.mat must exist to run second level processing
        #TODO: FNIRT case
        
    if reg_tarfile:
        #reg_tarfile = pjoin(output_dir,'reg.tar.gz')
        if (not os.path.exists(reg_tarfile)) or ForceUpdate:
            export_tarfile(reg_tarfile, reg_dir, arcnames='reg') #tar poststats_dir into output_tar

    reg_standard_tarfile={} #will be a dictionary of up to 3 tar files corresponding to FLIRT, FNIRT, or ANTS
    for reg_IDX in range(len(cases_to_run)):
        current_reg_case=cases_to_run[reg_IDX]
        
        
        if current_reg_case=='FLIRT':
            #reg_standard_dir=pjoin(output_dir,'reg_standard_FLIRT_' + stats_settings_str)
            reg_standard_dir=pjoin(output_dir,'reg_standard_FLIRT')
            current_reg_flag=[1, 0, 0] #'100' 
        elif current_reg_case=='FNIRT':
            #reg_standard_dir=pjoin(output_dir,'reg_standard_FNIRT_' + stats_settings_str)
            reg_standard_dir=pjoin(output_dir,'reg_standard_FNIRT')
            current_reg_flag=[0, 1, 0] #'010' 
        elif current_reg_case=='ANTS':
            #reg_standard_dir=pjoin(output_dir,'reg_standard_ANTS_' + stats_settings_str)
            reg_standard_dir=pjoin(output_dir,'reg_standard_ANTS')
            current_reg_flag=[0, 0, 1] #'001'
        
        if output_tar: 
            reg_standard_tarfile[current_reg_case]=reg_standard_dir + '.tar.gz'
        else:
            reg_standard_tarfile[current_reg_case]=None      
        
        if (not imexist(pjoin(reg_standard_dir,'stats','cope1'),'nifti-1')[0]) or ForceUpdate:  #check if at least one cope file already exists
        
            if ForceUpdate and os.path.exists(reg_standard_dir):
                shutil.rmtree(reg_standard_dir);
                #log_cmd('rm -rf "%s"' % (reg_standard_dir), verbose=verbose, logger=module_logger)
            
            if not os.path.exists(reg_standard_dir):
                os.makedirs(reg_standard_dir)
                os.makedirs(pjoin(reg_standard_dir,'stats'))
                os.makedirs(pjoin(reg_standard_dir,'reg'))
            
            copelist=glob.glob(pjoin(stats_dir,'cope*.nii*'));
            Ncope = len(copelist)
            #from IPython.core.debugger import Tracer
            #debug_here = Tracer()
            #debug_here()
            input_names=[]
            output_names=[]
            LRtoHR_affines=[]
            LRtoHR_warps=[]
            interp_types=[]
            #output_dirs=[]
            input_names.append(pjoin(prep_dir,'example_func.nii.gz'));
            output_names.append(pjoin(reg_standard_dir,'example_func.nii.gz'));
            interp_types.append('trilinear');
            input_names.append(pjoin(prep_dir,'mean_func.nii.gz'));
            output_names.append(pjoin(reg_standard_dir,'mean_func.nii.gz'));
            interp_types.append('trilinear');
            input_names.append(pjoin(prep_dir,'mask.nii.gz'));
            output_names.append(pjoin(reg_standard_dir,'mask.nii.gz'));
            interp_types.append('nn');  #have to use nearest-neighbor on the mask image
            
            for copeIDX in range(Ncope): #register all cope and varcopes to standard space
                input_names.append(pjoin(stats_dir,'cope%d.nii.gz' % (copeIDX+1)))
                output_names.append(pjoin(reg_standard_dir,'stats','cope%d.nii.gz' % (copeIDX+1)))
                interp_types.append('trilinear')
                input_names.append(pjoin(stats_dir,'varcope%d.nii.gz' % (copeIDX+1)))
                output_names.append(pjoin(reg_standard_dir,'stats','varcope%d.nii.gz' % (copeIDX+1)))
                interp_types.append('trilinear')
                
            for nameIDX in range(len(input_names)):
                warpfile=pjoin(reg_dir,'example_func2highres_warp.nii.gz')
                if os.path.exists(warpfile):
                    LRtoHR_warps.append(warpfile)
                else:
                    aff_file=pjoin(reg_dir,'example_func2highres.mat')
                    if os.path.exists(aff_file):
                        LRtoHR_affines.append(aff_file)
    
                #output_dirs.append(pjoin(output_dir,'reg_standard'))
            input_names.append(pjoin(reg_dir,'highres.nii.gz'))
            output_names.append(pjoin(reg_standard_dir,'reg','highres.nii.gz'))
            interp_types.append('trilinear')
            if os.path.exists(warpfile):  #append empty LRtoHR transform since the structural doesn't need a lowres2highres transform
                LRtoHR_warps.append('') 
            else:
                LRtoHR_affines.append('')
                  
            cmind_apply_normalization(output_dir, reg_struct, input_names, 
                                      output_names, 
                                      LRtoHR_affine=LRtoHR_affines, 
                                      LRtoHR_warp=LRtoHR_warps, 
                                      ANTS_path=ANTS_path, 
                                      reg_flag=current_reg_flag, 
                                      interp_type=interp_types, 
                                      generate_coverage=False, 
                                      generate_figures=generate_figures, 
                                      ForceUpdate=ForceUpdate, 
                                      verbose=verbose, logger=logger)
            
            if reg_standard_tarfile[current_reg_case]:  #TODO: use reg_standard_dir+'.nii.gz' instead or reg_standard.tar.gz?
                #reg_standard_tarfile = pjoin(output_dir,'reg_standard.tar.gz')
                if (not os.path.exists(reg_standard_tarfile[current_reg_case])) or ForceUpdate:
                    export_tarfile(reg_standard_tarfile[current_reg_case], reg_standard_dir, arcnames='reg_standard') #tar poststats_dir into output_tar
        
    #if only a single item in the dictionary, convert to (str) value
    if len(reg_standard_tarfile)==1:
        reg_standard_tarfile=reg_standard_tarfile.values()[0]
    
    if generate_figures: 
        try:
            #generate slice coverage summary figures from the standard space mask
            coverage_vol = pjoin(reg_standard_dir,'mask.nii.gz')
            cmind_calculate_slice_coverage(coverage_vol,
                                           bg_thresh=0.05,
                                           fill_holes=True,
                                           verbose=verbose,
                                           logger=logger)
        except:
            warnings.warn("Failed to generate slice coverage images")
        
    #print any file or directory name outputs for capture by LONI
    print("reg_tarfile:{}".format(reg_tarfile))
    print("reg_standard_tarfile:{}".format(reg_standard_tarfile))
    print("reg_dir:{}".format(reg_dir))
    print("reg_standard_dir:{}".format(reg_standard_dir))
    return (reg_tarfile, 
            reg_standard_tarfile,
            reg_dir,
            reg_standard_dir)

        
def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir, cmind_ANTs_dir
    
    T1_dir=pjoin(cmind_example_output_dir,'F','T1_proc')
    reg_struct=pjoin(T1_dir,'reg_struct.csv')
    ANTS_path=cmind_ANTs_dir
    reg_flag='001'
    generate_figures=True
    ForceUpdate=True
    verbose=True
    output_tar=True
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',
                        file_mode='w')  
        
    if verbose:
        func=cmind_timer(cmind_fMRI_norm,logger=logger)
    else:
        func=cmind_fMRI_norm
    
    paradigms=['Sentences','Stories']
    acq_types=['ASL','BOLD']
    update_reg_struct=False
    for paradigm in paradigms:
        for acq_type in acq_types:
            output_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s_reg' % acq_type)
            prep_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s' % acq_type)
            stats_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'Feat_%s_stats' % acq_type,'stats')
            regBBR_dir=pjoin(cmind_example_output_dir,'F','ASLBOLD_%s' % paradigm,'regBBR')
            func(output_dir, 
                 reg_struct,
                 regBBR_dir,
                 prep_dir=prep_dir, 
                 stats_dir=stats_dir,
                 ANTS_path=ANTS_path,
                 reg_flag=reg_flag,
                 generate_figures=generate_figures, 
                 ForceUpdate=ForceUpdate, 
                 verbose=verbose, 
                 logger=logger, 
                 output_tar=output_tar,
                 update_reg_struct=update_reg_struct)
        
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        _testme()    
    else: 
        parser = argparse.ArgumentParser(description="Apply previously computed transforms to normalize fMRI results to MNI standard space", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-rstr","--reg_struct", required=True, help="filename of the .csv containing the registration structure")
        parser.add_argument("-regbbr","--regBBR_dir", required=True, type=str, help="pathname to the BBR registration results (rigid from functional to structural)")
        parser.add_argument("--prep_dir", required=False, type=str, default="None", help="specify location where cmind_fMRI_preprocess2.py output is located")
        parser.add_argument("--stats_dir", required=False, type=str, default="None", help="specify location where stats output should go (or is already located)")
        parser.add_argument("-ants","--ANTS_path", type=str, help="path to the ANTs binaries")
        parser.add_argument("-rflg","--reg_flag", type=str, help="string of 3 characters controlling whether FLIRT, FNIRT and/or ANTS registrations of T1 to standard are to be run. doFLIRT = 0 or 1  (1 = do the regisration using FLIRT). doFNIRT = 0 or 1  (1 = do the regisration using FNIRT) (NOT CURRENTLY IMPLEMENTED). doANTS = 0, 1 or 2  (0 = skip, 1 = do using Exp, 2 = do using SyN)")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate overlay images summarizing the registration")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        parser.add_argument("-tar","--output_tar", type=str, default="false", help="compress the registration folders into a .tar.gz archive")
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        output_dir=args.output_dir;
        reg_struct=args.reg_struct;
        regBBR_dir=args.regBBR_dir;
        prep_dir=str2none(args.prep_dir)
        stats_dir=str2none(args.stats_dir)
        ANTS_path=args.ANTS_path;
        reg_flag=args.reg_flag;
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger;
        output_tar=args.output_tar;
    
        if verbose:
            cmind_fMRI_norm=cmind_timer(cmind_fMRI_norm,logger=logger)
    
        cmind_fMRI_norm(output_dir, 
                        reg_struct,
                        regBBR_dir,
                        prep_dir=prep_dir, 
                        stats_dir=stats_dir,
                        ANTS_path=ANTS_path,
                        reg_flag=reg_flag,
                        generate_figures=generate_figures, 
                        ForceUpdate=ForceUpdate, 
                        verbose=verbose, 
                        logger=logger, 
                        output_tar=output_tar)
                        
