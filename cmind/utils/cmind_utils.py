"""
**CMIND Support Utilities**: 
A series of utilities for working with C-MIND data

run_mcflirt
run_mcflirt_parallel
cmind_reg_report_img
cmind_lowres_to_T1
find_optimal_FSL_refvol
cmind_scaleimg
read_fsl_design
oblique_bool
AFNI_3dCM
cmind_outlier_remove
_transformlist_from_case
_convert_reg_struct
"""
from __future__ import division, print_function, absolute_import
import os
import warnings
import shutil
import glob
import tempfile

import numpy as np

from cmind.utils.utils import input2bool
from cmind.utils.logging_utils import log_cmd
from cmind.utils.file_utils import imexist, split_multiple_ext
from cmind.globals import has_ImageMagick
            

def _transformlist_from_case(reg_struct,ANTS_reg_case, Norm_Target='MNI'):  
    """This function only for backwards compatibility purposes.
       the transform_lists should already be stored in reg_struct.csv
    """
    transform_list=[]
    if Norm_Target=='MNI':
        if 'ANTS_HR_to_MNI_Warp' in reg_struct and os.path.exists(reg_struct['ANTS_HR_to_MNI_Warp']):
            if ANTS_reg_case.lower()=='aff': #direct affine to MNI
                    transform_list[0]=reg_struct['ANTS_StudyTemplate_to_MNI_Aff']
            elif ANTS_reg_case.lower()=='syn': #direct Nonlin to MNI
                    transform_list[0]=reg_struct['ANTS_StudyTemplate_to_MNI_Aff']
                    transform_list[1]=reg_struct['ANTS_StudyTemplate_to_MNI_Warp']
            else:
                raise ValueError("Invalid ANTS_reg_case")
        else:  #Nonlin to pstd then nonlin to MNI
            if ANTS_reg_case.lower()=='syn': #Nonlin to pstd then nonlin to MNI
                    transform_list[0]=reg_struct['ANTS_HR_to_StudyTemplate_Aff'];
                    transform_list[1]=reg_struct['ANTS_HR_to_StudyTemplate_Warp'];
                    transform_list[2]=reg_struct['ANTS_StudyTemplate_to_MNI_Aff']
                    transform_list[3]=reg_struct['ANTS_StudyTemplate_to_MNI_Warp']
            elif ANTS_reg_case.lower()=='synaff': #Nonlin to pstd then affine to MNI
                    transform_list[0]=reg_struct['ANTS_HR_to_StudyTemplate_Aff']
                    transform_list[1]=reg_struct['ANTS_HR_to_StudyTemplate_Warp']
                    transform_list[2]=reg_struct['ANTS_StudyTemplate_to_MNI_Aff']
            elif ANTS_reg_case.lower()=='affsyn': #affine to pstd then nonlin to MNI
                    transform_list[0]=reg_struct['ANTS_HR_to_StudyTemplate_Aff']
                    transform_list[1]=reg_struct['ANTS_StudyTemplate_to_MNI_Aff']
                    transform_list[2]=reg_struct['ANTS_StudyTemplate_to_MNI_Warp']
            elif ANTS_reg_case.lower()=='aff': #affine to pstd then affine to MNI
                    transform_list[0]=reg_struct['ANTS_HR_to_StudyTemplate_Aff']
                    transform_list[1]=reg_struct['ANTS_StudyTemplate_to_MNI_Aff']
            else:
                raise ValueError("Invalid ANTS_reg_case")
    elif Norm_Target=='StudyTemplate':
        if (ANTS_reg_case.lower()=='syn' or ANTS_reg_case.lower()=='synaff'): #Nonlin to pstd then nonlin to MNI
                transform_list[0]=reg_struct['ANTS_HR_to_StudyTemplate_Aff'];
                transform_list[1]=reg_struct['ANTS_HR_to_StudyTemplate_Warp'];
        elif (ANTS_reg_case.lower()=='affsyn' or ANTS_reg_case.lower()=='aff'): #affine to pstd then nonlin to MNI
                transform_list[0]=reg_struct['ANTS_HR_to_StudyTemplate_Aff']
        else:
            raise ValueError("Invalid ANTS_reg_case")
    else:
        raise ValueError("_transformlist_from_case: Norm_Target should be either MNI or StudyTemplate")
        
    return transform_list

def update_reg_struct_file(reg_struct_file):
    from cmind.utils.file_utils import csv2dict, dict2csv
    if os.path.exists(reg_struct_file):
        reg_struct=csv2dict(reg_struct_file)  
    reg_struct=_convert_reg_struct(reg_struct,target='New',rename_files='True')
    dict2csv(reg_struct,reg_struct_file)

def _convert_reg_struct(reg_struct,target='New',rename_files='False'):
    """
    For backwards compatibility with old naming conventions.
    If rename_files=True, any files pointed to will also be renamed, if possible
    If rename_files=False, only the key names are changed, but not the values
    """
    #WARNING: ordering of these lists is very important.  do not change it!
    
    #convert keys/values old to new naming convention
    old_keystr=['ped_standard','_pstd','lin_to_','ANTS_to_','lin_HR','lin_StudyTemplate','unregistered_img']
    new_keystr=['StudyTemplate','_StudyTemplate','lin_HR_to_','ANTS_HR_to_','FLIRT_HR','FLIRT_StudyTemplate','HR_img']
    old_valstr='ped_std'
    new_valstr='StudyTemplate'

    if target.lower()=='old':  #convert keys/values new to old naming convention
        tmp_old=old_keystr;
        old_keystr=list(reversed(new_keystr))
        new_keystr=list(reversed(tmp_old))
        old_valstr='StudyTemplate'
        new_valstr='ped_std'
    elif target.lower()!='new':
        raise ValueError("Invalid target. Must be 'New' or 'Old'")
        
    new_reg_struct={}
    for key,val in reg_struct.items():
        
        #update all key names
        for idx,v in enumerate(old_keystr):  
            if idx==0:
                new_key=key.replace(old_keystr[idx],new_keystr[idx]);
            else:
                new_key=new_key.replace(old_keystr[idx],new_keystr[idx]);
                
        if rename_files:
            def _mv_val(old_val,new_val):  #move the file
                old_val=old_val.replace('"','').replace("'","")  #remove any quotes from filename
                if os.path.isfile(old_val):
                    shutil.move(old_val,new_val)
                    #print('moving {} to: {}'.format(old_val,new_val))
                else:  #often same file more than once in reg_struct, so don't error if it isn't there
                    pass
                    #print('old file, {}, not found'.format(old_val))
                
            #update all value strings
            if isinstance(val,str):
                old_val=val;
                new_val=val.replace(old_valstr,new_valstr);
                if rename_files and (new_val!=old_val):
                    _mv_val(old_val,new_val)
                    
            elif isinstance(val,(list,tuple)):
                new_val=[]
                for old_val in val:
                    tmp_val=old_val.replace(old_valstr,new_valstr)
                    if rename_files and (tmp_val!=old_val):
                        _mv_val(old_val,tmp_val)
                    new_val.append(tmp_val)
        else:
            new_val = val
            
        new_reg_struct[new_key]=new_val
        
    return new_reg_struct
                                
def update_reg_struct_paths(reg_struct,T1_proc_dir,template_dir=None, MNI_dir=None):
    reg_struct=reg_struct.copy()  #work with a copy
    if not isinstance(reg_struct,dict):
        raise ValueError("reg_struct must be a dictionary")
    old_template_dir=os.path.sep.join(reg_struct['StudyTemplate_brain_mask'].split('RIGID')[0].split(os.path.sep)[:-2])
    old_MNI_dir=os.path.dirname(reg_struct['MNI_standard_brain'])
    old_T1_dir=reg_struct['T1proc_dir']
    if not template_dir:
        from cmind.globals import cmind_template_dir
        template_dir=cmind_template_dir
    if not MNI_dir:
        from cmind.globals import cmind_MNI_dir
        MNI_dir=cmind_MNI_dir
        
    def _renamer(f): #update paths
        f=f.replace(old_template_dir,template_dir)
        f=f.replace(old_MNI_dir,MNI_dir)
        f=f.replace(old_T1_dir,T1_proc_dir)
        return f
        
    for key,val in reg_struct.items():
        if isinstance(val,(list,tuple)):
            reg_struct[key]=[_renamer(v) for v in val]
            for v in reg_struct[key]:
                if os.path.splitext(v)[1] and (not os.path.exists(v)):
                        raise Exception("Unable to find: {}".format(v))
        else:
            reg_struct[key]=_renamer(val)
            v=reg_struct[key]
            if os.path.splitext(v)[1] and (not os.path.exists(v)):
                print("old_template_dir={}".format(old_template_dir))
                print("old_MNI_dir={}".format(old_MNI_dir))
                print("old_T1_dir={}".format(old_T1_dir))
                raise Exception("Unable to find: {}".format(v))
    return reg_struct
                                   
def run_mcflirt(input_name,output_name=None,cost_type='normcorr',refvol=0,extra_args="", verbose=False, logger=None):
    """Run FSL mcflirt motion correction
       
    Parameters
    ----------     
    input_name : str
        4D volume to coregister with mcflirt
    output_name : str or None, optional
        output filename
    cost_type : {'normcorr','mutualinfo','woods','corratio','normmi','leastsquares'}, optional
        cost function used by mcflirt (default = 'normcorr')
    refvol : int or str, optional
        reference volume to register the rest to. 
        If int register to timepoint refvol of the 4D series.
        If str, register to a specified 3D volume
    extra_args : str, optional
        this string will be appended to the mcflirt command line
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
            
            
    Returns
    -------   
    stdo : str
        standard output of the mcflirt call
        
    Notes
    ----------
       [1] M. Jenkinson and P. Bannister and M. Brady and S. Smith. Improved optimisation for 
       the robust and accurate linear registration and motion correction of brain images. 
       NeuroImage 17:2(825-841) 2002.
    """
    
    if not extra_args:
        extra_args = '-plots -rmsrel -rmsabs'
        
    if not output_name:  #replace file extension of input_name with .par
        (input_basename, input_ext)=split_multiple_ext(input_name)
        output_name = input_basename + '_mcf' + input_ext
        
    if isinstance(refvol,int):
        #stdo=log_cmd('$FSLDIR/bin/mcflirt -in "%s" -out "%s_mcf" -refvol %d -cost %s %s' % (input_name,output_name,refvol,cost_type,extra_args))
        stdo=log_cmd('$FSLDIR/bin/mcflirt -in "%s" -out "%s" -refvol %d -cost %s %s' % (input_name,output_name,refvol,cost_type,extra_args), verbose=verbose, logger=logger)
    else:  #ref_vol is a reference filename instead of a volume number
        exists=imexist(refvol,'nifti-1')[0]
        if exists:
            #stdo=log_cmd('$FSLDIR/bin/mcflirt -in "%s" -out "%s_mcf" -reffile "%s" -cost %s %s' % (input_name,output_name,refvol,cost_type,extra_args))
            stdo=log_cmd('$FSLDIR/bin/mcflirt -in "%s" -out "%s" -reffile "%s" -cost %s %s' % (input_name,output_name,refvol,cost_type,extra_args), verbose=verbose, logger=logger)
        else:
            raise IOError("specified reference volume doesn't exist!")
    return stdo

def run_mcflirt_parallel(mcflirt_arg_list):
    """Utility to call multiple instances of run_mcflirt in parallel
       
    Parameters
    ----------     
    mcflirt_arg_list : list
        list of tuples where each tuple is a set of arguments to `utils.run_mcflirt`
    """
    import multiprocessing
    
    if True:
        procs=[]
        for current_args in mcflirt_arg_list:
            p = multiprocessing.Process(target=run_mcflirt,args=current_args)
            procs.append(p)
            p.start()
        for p in procs:
            p.join()
    else: #broken
        pool = multiprocessing.Pool(processes=len(mcflirt_arg_list))
        pool.map(run_mcflirt,mcflirt_arg_list)
        
def cmind_reg_report_img(input_vol1,input_vol2, output_file, working_dir=None,fewer_flag=0, istr='', istr2=''):
    """utility for overlaying two input volumes
    
    Parameters
    ----------     
    input_vol1 : str
        filename of the first 3D input volume
    input_vol2 : str
        filename of the second 3D input volume
    output_file : str
        filename of overlay image to be generated
    working_dir : str, optional
        intermediate working directory to use.  by default generate a random one
    fewer_flag : {0,1,2,3,4,5}, optional
        various different cases for the overlay (need to document)
    istr : str, optional
        optional argument string to pass to $FSLDIR/bin/slicer when processing 
        input_vol1 (e.g. istr='-i -0.5 2' to specify intensity range [-0.5, 2])
    istr2 : str, optional
        optional argument string to pass to $FSLDIR/bin/slicer when processing 
        input_vol2  (e.g. istr2='-i -0.5 2' to specify intensity range [-0.5, 2])
        
    Notes
    -----
       
    """
    
    if not working_dir:
        working_dir = tempfile.mkdtemp()  #createRandomDirectory()
        rm_working_dir=True
    else:
        rm_working_dir=False

    ## This section checks existence of input filenames and expands all filenames to full path name
    (input1_exists, input1_full)=imexist(input_vol1,'nifti-1')[0:2]    
    if input1_exists:
        input_vol1=input1_full
    else: 
        raise ValueError('input_vol1, %s, doesn''t exist!' % (input_vol1))

    if input_vol2:  #if 2nd input volume was specified, check that it exists too
        (input2_exists, input2_full)=imexist(input_vol2,'nifti-1')[0:2];    
        if input2_exists:
            input_vol2=input2_full
        else: 
            raise ValueError('input_vol2, %s, doesn''t exist!' % (input_vol2))
            
   
    p_out=os.path.dirname(output_file)
    f_out,ext_out = os.path.splitext(os.path.basename(output_file))
    
    if not p_out:
        pwd_old=os.getcwd();
        output_file=os.path.join(pwd_old,f_out+ext_out)

    if not os.path.isdir(working_dir):
        os.makedirs(working_dir)

    w=working_dir 
    if input_vol2:
        if fewer_flag==1:
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" "%s" -e 0.12 -s 2 -x 0.4 "%s/sla.png" -x 0.6 "%s/sld.png" -y 0.4 "%s/sle.png" -y 0.6 "%s/slh.png" -z 0.4 "%s/sli.png" -z 0.6 "%s/sll.png"' % (input_vol1,input_vol2,istr,w,w,w,w,w,w))
            log_cmd('$FSLDIR/bin/pngappend "%s/sla.png" + "%s/sld.png" + "%s/sle.png" + "%s/slh.png" + "%s/sli.png" + "%s/sll.png" "%s/tmp_reg1.png"' % (w,w,w,w,w,w,w))
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" "%s" -e 0.12 -s 2 -x 0.4 "%s/sla.png" -x 0.6 "%s/sld.png" -y 0.4 "%s/sle.png" -y 0.6 "%s/slh.png" -z 0.4 "%s/sli.png" -z 0.6 "%s/sll.png"' % (input_vol2,input_vol1,istr2,w,w,w,w,w,w))
            log_cmd('$FSLDIR/bin/pngappend "%s/sla.png" + "%s/sld.png" + "%s/sle.png" + "%s/slh.png" + "%s/sli.png" + "%s/sll.png" "%s/tmp_reg2.png"' % (w,w,w,w,w,w,w))
        elif fewer_flag==2:
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" "%s" -e 0.12 -s 4 -x 0.45 "%s/sla.png" -y 0.45 "%s/sle.png" -z 0.45 "%s/sli.png"' % (input_vol2,input_vol1,istr2,w,w,w))
            log_cmd('$FSLDIR/bin/pngappend "%s/sla.png" + "%s/sle.png" + "%s/sli.png" "%s/tmp_reg2.png"' % (w,w,w,w))
        elif fewer_flag==3:
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" "%s" -e 0.12 -s 2 -x 0.45 "%s/sla.png" -y 0.45 "%s/sle.png" -z 0.45 "%s/sli.png"' % (input_vol1,input_vol2,istr,w,w,w))
            log_cmd('$FSLDIR/bin/pngappend "%s/sla.png" + "%s/sle.png" + "%s/sli.png" "%s/tmp_reg2.png"' % (w,w,w,w))
        elif fewer_flag==4: #all axial slices overlay with zoom 2
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" "%s" -e 0.12 -s 2 -A 800 "%s/tmp_reg2.png"' % (input_vol1,input_vol2,istr,w))
        elif fewer_flag==5:
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" "%s" -e 0.12 -s 1 -z 0.55 "%s/tmp_reg2.png"' % (input_vol1,input_vol2,istr,w))
        else:
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" "%s" -e 0.12 -s 2 -x 0.35 "%s/sla.png" -x 0.45 "%s/slb.png" -x 0.55 "%s/slc.png" -x 0.65 "%s/sld.png" -y 0.35 "%s/sle.png" -y 0.45 "%s/slf.png" -y 0.55 "%s/slg.png" -y 0.65 "%s/slh.png" -z 0.35 "%s/sli.png" -z 0.45 "%s/slj.png" -z 0.55 "%s/slk.png" -z 0.65 "%s/sll.png"' % (input_vol1,input_vol2,istr,w,w,w,w,w,w,w,w,w,w,w,w))
            log_cmd('$FSLDIR/bin/pngappend "%s/sla.png" + "%s/slb.png" + "%s/slc.png" + "%s/sld.png" + "%s/sle.png" + "%s/slf.png" + "%s/slg.png" + "%s/slh.png" + "%s/sli.png" + "%s/slj.png" + "%s/slk.png" + "%s/sll.png" "%s/tmp_reg1.png"' % (w,w,w,w,w,w,w,w,w,w,w,w,w))
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" "%s" -e 0.12 -s 2 -x 0.35 "%s/sla.png" -x 0.45 "%s/slb.png" -x 0.55 "%s/slc.png" -x 0.65 "%s/sld.png" -y 0.35 "%s/sle.png" -y 0.45 "%s/slf.png" -y 0.55 "%s/slg.png" -y 0.65 "%s/slh.png" -z 0.35 "%s/sli.png" -z 0.45 "%s/slj.png" -z 0.55 "%s/slk.png" -z 0.65 "%s/sll.png"' % (input_vol2,input_vol1,istr2,w,w,w,w,w,w,w,w,w,w,w,w))
            log_cmd('$FSLDIR/bin/pngappend "%s/sla.png" + "%s/slb.png" + "%s/slc.png" + "%s/sld.png" + "%s/sle.png" + "%s/slf.png" + "%s/slg.png" + "%s/slh.png" + "%s/sli.png" + "%s/slj.png" + "%s/slk.png" + "%s/sll.png" "%s/tmp_reg2.png"' % (w,w,w,w,w,w,w,w,w,w,w,w,w))
        if fewer_flag<2:
            log_cmd('$FSLDIR/bin/pngappend "%s/tmp_reg1.png" - "%s/tmp_reg2.png" "%s"' % (w,w,output_file))
        else:
            shutil.copy("%s/tmp_reg2.png" % w,output_file)
    else:
        if fewer_flag==1:
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" -s 2 -x 0.4 "%s/sla.png" -x 0.6 "%s/sld.png" -y 0.4 "%s/sle.png" -y 0.6 "%s/slh.png" -z 0.4 "%s/sli.png" -z 0.6 "%s/sll.png"' % (input_vol1,istr,w,w,w,w,w,w))
            log_cmd('$FSLDIR/bin/pngappend "%s/sla.png" + "%s/sld.png" + "%s/sle.png" + "%s/slh.png" + "%s/sli.png" + "%s/sll.png" "%s" ' % (output_file,w,w,w,w,w,w))
        else:
            log_cmd('$FSLDIR/bin/slicer "%s" "%s" -s 2 -x 0.35 "%s/sla.png" -x 0.45 "%s/slb.png" -x 0.55 "%s/slc.png" -x 0.65 "%s/sld.png" -y 0.35 "%s/sle.png" -y 0.45 "%s/slf.png" -y 0.55 "%s/slg.png" -y 0.65 "%s/slh.png" -z 0.35 "%s/sli.png" -z 0.45 "%s/slj.png" -z 0.55 "%s/slk.png" -z 0.65 "%s/sll.png"' % (input_vol1,istr,w,w,w,w,w,w,w,w,w,w,w,w))
            log_cmd('$FSLDIR/bin/pngappend "%s/sla.png" + "%s/slb.png" + "%s/slc.png" + "%s/sld.png" + "%s/sle.png" + "%s/slf.png" + "%s/slg.png" + "%s/slh.png" + "%s/sli.png" + "%s/slj.png" + "%s/slk.png" + "%s/sll.png" "%s" ' % (output_file,w,w,w,w,w,w,w,w,w,w,w,w))
    
    if rm_working_dir:
        shutil.rmtree(working_dir)
    else:
        log_cmd('rm -f "%s/sl?.png" "%s/tmp_reg1.png" "%s/tmp_reg2.png"' % (w,w,w))
    
    #!convert junk.png -background Khaki  label:'18M001' +swap -gravity Center  -append anno_label.jpg
    #!montage -label "18M001" junk.png -pointsize 90 -frame 5  -geometry +0+0 anno_montage2.jpg

def cmind_lowres_to_T1(reg_struct,func_vol,T1_vol,T1brain_vol, T1wmseg_vol,output_dir,bbr_schedule_path="default",standard_vol=None,warp_highres2std=None,use_bbr=True,tight_angle_range_flag=False,fieldmap_dict=None, verbose=False, logger=None):
    """coregister a low-resolution functional image to a structural image
    
    Parameters
    ----------     
    reg_struct : str
        filename of the .csv file containing the registration structure
    func_vol : str
        filename of the "moving" image
    T1_vol : str
        filename of the "fixed" head image  (before brain extraction)
    T1brain_vol : str
        filename of the "fixed" brain image
    T1wmseg_vol : str
        filename of the white matter partial volume estimate image
    output_dir : str
        directory in which to store output
    bbr_schedule_path : str, optional
        filename of flirt schedule to use  (e.g. $FSLDIR/etc/flirtsch/bbr.sch)
    standard_vol : str, optional
        if specified, also apply precomputed FLIRT linear transformation to
        standard space
    warp_highres2std : str, optional
        if specified along with standard_vol, also apply the precomputed FNIRT
        transform to standard space
    use_bbr : bool, optional
        use BBR registration for FLIRT
    tight_angle_range_flag : bool or str or int or float or tuple
        optional parameter to specify a limited angular range to be used during
        BBR-based registration
    fieldmap_dict : dict
        dictionary containing parameters for a fieldmap-corrected BBR
        registration
        
    Notes
    -----
    This is largely based off of the epi_reg script from FSL
    
    FLIRT is described in:
    [1] M. Jenkinson and S.M. Smith. A global optimisation method for robust affine registration of brain images. Medical Image Analysis, 5(2):143-156, 2001.
    [2] M. Jenkinson, P.R. Bannister, J.M. Brady, and S.M. Smith. Improved optimisation for the robust and accurate linear registration and motion correction of brain images. NeuroImage, 17(2):825-841, 2002.  

    Boundary based registration (BBR) is described in:
    [3] Greve, D.N. and Fischl, B. Accurate and robust brain image alignment using boundary-based registration. NeuroImage, 48(1):63-72, 2009
    """
    from cmind.globals import cmind_AFNI_dir

    use_bbr, tight_angle_range_flag, verbose = input2bool([use_bbr, tight_angle_range_flag, verbose])   
    
    if verbose:
        import inspect
        frame = inspect.currentframe()
        args, _, _, values = inspect.getargvalues(frame)
        func_name=inspect.getframeinfo(frame)[2]
        print('%s:\n   arguments:' % func_name)
        for i in args:
            print("      %s = %s" % (i, values[i]))
    
    #FLIRT pre-alignment
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    
    if not bbr_schedule_path:
        bbr_schedule_path="$[FSLDIR]/etc/flirtsch/bbr.sch"
    if not os.path.isfile(bbr_schedule_path):
        raise Exception("specified BBR schedule file, %s, not found" % bbr_schedule_path)
            
    wopt_args=""
    
    if fieldmap_dict:
        fieldmap_args = '-echospacing %f -pedir %d -fieldmap "%s"' % (fieldmap_dict['dwell'],fieldmap_dict['pe_dir'],fieldmap_dict['Fieldmap_vol_processed'])
    else:
        fieldmap_args = ""

    vout=os.path.join(output_dir,'lowres2highres')
    
    #check existence of input volumes and convert to full paths
    (imexists,func_vol)=imexist(func_vol,'nifti-1')[0:2]
    if not imexists:
        raise Exception("func_vol not found!")
    
    (imexists,T1_vol)=imexist(T1_vol,'nifti-1')[0:2]
    if not imexists:
        raise Exception("T1_vol not found!")
    
    (imexists,T1brain_vol)=imexist(T1brain_vol,'nifti-1')[0:2]
    if not imexists:
        raise Exception("T1brain_vol not found!")
    
    (imexists,T1wmseg_vol)=imexist(T1wmseg_vol,'nifti-1')[0:2]
    if not imexists:
        raise Exception("T1wmseg_vol not found!")
        
    log_cmd('$FSLDIR/bin/fslmaths "%s" -Tmean %s/EPIvol' % (func_vol, output_dir), verbose=verbose, logger=logger)

    if tight_angle_range_flag:
        if isinstance(tight_angle_range_flag,bool): #default to 20 degree range  
            angle_range_args="-searchrx -20 20 -searchry -20 20 -searchrz -20 20"
        elif isinstance(tight_angle_range_flag,(str,int,float)):
            angle_range_args="-searchrx -{} {} -searchry -{} {} -searchrz -{} {}".format(tight_angle_range_flag,tight_angle_range_flag,tight_angle_range_flag,tight_angle_range_flag,tight_angle_range_flag,tight_angle_range_flag)
        elif isinstance(tight_angle_range_flag,tuple):
            angle_range_args="-searchrx -%f %f -searchry -%f %f -searchrz -%f %f" % tight_angle_range_flag
    else:
        angle_range_args=""

    initialize_with_COM_align = False
    if initialize_with_COM_align and cmind_AFNI_dir:
        from cmind.utils.cmind_utils import AFNI_3dCM
        from os.path import join as pjoin
        raise ValueError("Not Implemented")
#        brain_COM = np.asarray(AFNI_3dCM(T1brain_vol, reset_coords=False))
#        lowres_COM = np.asarray(AFNI_3dCM(pjoin(output_dir,'EPIvol.nii.gz'), reset_coords=False))
#        COM_shift = brain_COM - lowres_COM
#        COM_aff = np.eye(4)
#        COM_aff[:3, -1] = COM_shift
#        np.savetxt(pjoin(output_dir, 'COM_align.mat'), COM_aff)
#        log_cmd('$FSLDIR/bin/flirt -ref "%s" -in %s/EPIvol -dof 6 -init %s/COM_align.mat -omat %s/T1Reg_init.mat -o %s/T1Reg_init %s' % (T1brain_vol, output_dir, output_dir, output_dir, output_dir, angle_range_args), verbose=verbose, logger=logger)
    else:
        if use_bbr:
            log_cmd('$FSLDIR/bin/flirt -ref "%s" -in %s/EPIvol -dof 6 -omat %s/T1Reg_init.mat -o %s/T1Reg_init %s' % (T1brain_vol, output_dir, output_dir, output_dir, angle_range_args), verbose=verbose, logger=logger)
        else:
            log_cmd('$FSLDIR/bin/flirt -ref "%s" -in %s/EPIvol -dof 6  -omat "%s.mat" -out "%s" %s' % (T1brain_vol, output_dir, vout, vout, angle_range_args), verbose=verbose, logger=logger)
    
    if use_bbr:
        #echo "Running BBR"
        log_cmd('$FSLDIR/bin/flirt -ref "%s" -in %s/EPIvol -dof 6 -cost bbr -wmseg "%s" -init %s/T1Reg_init.mat -omat "%s.mat" -out "%s" -schedule "%s" %s %s' % (T1_vol, output_dir,T1wmseg_vol,output_dir,vout,vout,bbr_schedule_path,fieldmap_args,wopt_args), verbose=verbose, logger=logger)
        #Don't apply warp to full timeseries!
        log_cmd('$FSLDIR/bin/applywarp -i %s/EPIvol -r "%s" -o "%s" --premat="%s.mat" --interp=spline' % (output_dir, T1_vol, vout, vout), verbose=verbose, logger=logger)

        mcf_fnames=glob.glob(os.path.join(output_dir,'T1Reg_init.*'));
        for f in mcf_fnames:
            os.remove(f)

    if imexist(standard_vol,'nifti-1')[0]:
        log_cmd('$FSLDIR/bin/convert_xfm -omat %s/lowres2standard.mat -concat "%s" %s.mat' % (output_dir, reg_struct['FLIRT_HR_to_MNI_Aff'], vout), verbose=verbose, logger=logger)
        log_cmd('$FSLDIR/bin/applywarp -i %s/EPIvol -r "%s" -o %s/lowres2standard --premat=%s/lowres2standard.mat --interp=spline' % (output_dir, reg_struct['MNI_standard'], output_dir, output_dir), verbose=verbose, logger=logger)
        if imexist(warp_highres2std,'nifti-1')[0]:
            log_cmd('$FSLDIR/bin/applywarp -i %s/EPIvol -r "%s" -o %s/lowres2standard_nonlin --premat=%s.mat -w "%s" --interp=spline' % (output_dir, standard_vol, output_dir,vout,warp_highres2std), verbose=verbose, logger=logger)

def find_optimal_FSL_refvol(motionparam_filename):
    """based on mcflirt motion parameters, find the volume within a 4D 
       timeseries that is closest to the mean position
       
    Parameters
    ----------     
    motionparam_filename : str
        filename containing the .par output from mcflirt
        
    Returns
    -------   
    ref_vol : int
        the optimal reference volume
    dist : ndarray
        array, dist = tdist + rdist
    rdist : ndarray
        array of translation distances
    tdist : ndarray
        array of rotation distances
    motpar : ndarray
        the motion parameters read from `motionparam_filename`
        
    See Also
    --------
    fsl_mcflirt_opt
    fsl_mcflirt_opt_ASLBOLD
       
    """
    motpar=np.loadtxt(motionparam_filename)
    rot=motpar[:,0:3]*180/np.pi; #rotations in degrees
    #rnet=np.sqrt(np.sum(rot**2,axis=1));  #RMS rotation
    trans=motpar[:,3:6]; #translation in mm
    #tnet=np.sqrt(np.sum(trans**2,axis=1));  #RMS translation
    ##clear motpar; motpar.rot=rot; motpar.trans=trans;
    
    vec1=np.ones([trans.shape[0],1]);
    tdist=np.sum(np.abs(trans-vec1*np.mean(trans,axis=0)),axis=1);  #find the volume closest to the mean translation
    
    object_radius=10; #mm
    rdist=np.sum(object_radius*np.sin(np.abs(rot-vec1*np.mean(rot,axis=0))/180.0*np.pi),axis=1);  #find the volume closest to the mean rotation  (scale by 10mm offset * sind(rot) to get an approximate translation at edge of volume)
    
    dist=tdist+rdist;
    #m, mi=dist.min(0),dist.argmin(0);
    ref_vol=dist.argmin(0);  #mi-1;
    return ref_vol, dist, rdist, tdist, motpar


def cmind_scaleimg(img,pct=99.9,nii_type=16):
    """truncate image to percentile, pct and rescale into a range dependent on
        nii_type
        
    Parameters
    ----------     
    img : ndarray
        data array containing the image values to scale
    pct : float
        percentile in range [0, 100] to truncate values at
    nii_type : {4,16}
        4 = int16, 16=float32.  if 4, scale between [0 16384].  If 16 scale
        between [0 32766].
        
    Notes
    -----
    img data type will not be changed.  `nii_type` is purely used to determine
    the range for rescaling
    """
    max_thr=np.percentile(img.flatten(),q=pct);
    img[img>max_thr]=max_thr
    if nii_type==4:  #rescale to ~2^16/4
        #Dmax = max(abs(DW_Data.img(:)))
        img = np.abs(img)/float(max_thr)*16384
    if nii_type==16:
        maxi=np.max(np.abs(img))
        img = img/float(maxi)*32766
    return img
        



def read_fsl_design(design_fname):    
    """read in a design matrix (design.mat) created by FSL
    
    Parameters
    ----------
    design_fname : str
        filename of the design matrix
    
    Returns
    -------
    des_mat : ndarray
        ndarray of size [Ntimepoints x Nregressors]   
    
    """
    desmat=np.genfromtxt(design_fname,delimiter='\t',skip_header=5)  #use genfromtxt() instead of loadtxt() to allow empty column as nan
    
    #find any nan columns and remove them
    nan_cols=np.where(np.sum(np.isnan(desmat),axis=0)>0)[0]  
    if nan_cols:  #remove any columns containing NaNs
        desmat = np.delete(desmat,nan_cols,axis=1);    
    return desmat
    
def oblique_bool(parfile):
    """Read angulation from Philips .PAR file and return True if oblique
    
    Parameters
    ----------
    parfile : str
        filename of the parfile to check
        
    isOblique : bool       
        True if the scan was oblique
    """
    PARFILE=open(parfile)
    isOblique=None
    for line in PARFILE:
        if line.find("Angulation midslice")!=-1:
            angle_array=np.genfromtxt(line.split(':')[1].strip().split())
            if np.sum(angle_array)==0:
                isOblique=False
            else:
                isOblique=True
    if isOblique==None:
        print("Warning:  Angulation not found in .PAR file!")
        
    return isOblique    
        
def AFNI_3dCM(input_vol,reset_coords=False,extra_opts=None):
    """return the center of mass of an image volume (NIFTI_GZ, etc) using 
    AFNI's 3dCM
    
    Parameters
    ----------
    input_vol : str
        image volume to process  (.nii, .nii.gz, etc)
    reset_coords : str
        update the image affine to be 0,0,0 at the center of mass
            
    Returns
    -------
    COM_coords : list
        list of the center-of-mass coordinates
    
    """
    if not extra_opts:
        if reset_coords:
            extra_opts="-set 0 0 0"
        else:
            extra_opts=""
    COM_coords=log_cmd("3dCM %s %s" % (extra_opts,input_vol))
    COM_coords=[float(item) for item in COM_coords.split()]

    return COM_coords                        

#TODO: probably want to use fsl_motion_outliers_v2.sh instead
def cmind_outlier_remove(InputImageSeries,thresh=1,mask_vol=None,verbose=False):
    """Returns true if input_file exists, but output_file doesn't exist
      or if input_file is newer than output_file
      
    Parameters
    ----------     
    InputImageSeries : str
        filename to a 4D image volume
    thresh : float
        number of standard deviations to use as outlier threshold
    mask_vol : str, optional
        optional mask over which to restrict the voxels considered in the 
        outlier calculation
    verbose : bool, optional
        more verbose output
                
    Returns
    -------   
    timepoints_keep : ndarray
        array of timepoints to keep
    AL : ndarray
        array of containing the "artifact level" computed at each timepoint
        
    See Also
    --------
    fsl_motion_outliers
    fsl_motion_outliers_v2
    
    Notes
    -----
    Generally recommend using fsl_motion_outliers_v2 instead
      
    """
    time_dim = len(InputImageSeries.shape)-1 #ndims(InputImageSeries);
    mean_img = np.mean(InputImageSeries,time_dim)
    std_img = np.std(InputImageSeries,time_dim,ddof=1)
 
    if time_dim==2:
        tmp=InputImageSeries-mean_img[:,:,None]
        if not mask_vol:
            tmp_mask=np.abs(tmp)>np.abs(std_img[:,:,None])
        else:
            tmp_mask=(np.abs(tmp)*mask_vol[:,:,None])>0  #replacate to 3D
        tmp2=tmp*(tmp_mask);
    elif time_dim==3:
        tmp=InputImageSeries-mean_img[:,:,:,None]
        if not mask_vol:
            tmp_mask=np.abs(tmp)>np.abs(std_img[:,:,:,None])
        else:
            tmp_mask=(np.abs(tmp)*mask_vol[:,:,:,None])>0  #replacate to 4D
        tmp2=tmp*(tmp_mask);
    AL=np.sum(np.abs(tmp2),tuple(range(time_dim))); #artifact level
    
    #thresh=input('Rejection Threshold \n (Strong = 0, Weak = 1, Med = 0.8) = ');
    
    if False:
        dyn_thresh = np.mean(AL) + thresh*np.std(AL,ddof=1);
    else:
        dyn_thresh = np.mean(AL) + thresh*np.std(AL,ddof=1);
        
    dyn_test=AL<=dyn_thresh
    timepoints_keep = np.where(dyn_test)[0] #only keep timepoints below the threshold
    if verbose:
        ntime=AL.shape[0] 
        print('Rejected %d of %d timepoints' % (np.sum(~dyn_test),ntime))
        
        #t=np.arange(ntime)
        #pyplot.plot(t[dyn_test],AL[dyn_test],'b.',t[~dyn_test],AL[~dyn_test],'r.')
    
    return (timepoints_keep, AL)            

    
def ASL_sub(ts,axis=0,asl_outliers=None):
    """
    ASL_ts_sub=ASL_sub(ASL_ts.copy().reshape((-1,ASL_ts.shape[-1])).T,axis=0,asl_outliers=None).T.reshape(ASL_ts.shape)
    """
    
    ts_out=np.zeros_like(ts)
    t_all=np.arange(ts.shape[0])
    if asl_outliers is None:
        idx_even=np.arange(0,ts.shape[0],2)
        idx_odd=np.arange(1,ts.shape[0],2)
        t_even=t_all[idx_even]
        t_odd=t_all[idx_odd]
    else:
        t_good = np.array([t for t in list(t_all) if t not in asl_outliers])
        ts=ts[t_good,...]                
        mod2=np.mod(t_good,2)
        idx_even=np.where(mod2==0)[0]
        idx_odd=np.where(mod2==1)[0]
        t_even=t_good[idx_even]
        t_odd=t_good[idx_odd]

    if ts.ndim>2:
        raise ValueError("only works on 1D and 2D input")

    if axis==0:
        if ts.ndim==1:
            ts=ts[:,np.newaxis]

        for i in range(ts.shape[1]):
            xe=np.interp(t_all,t_even,ts[idx_even,i])
            xo=np.interp(t_all,t_odd,ts[idx_odd,i])
            ts_out[:,i]=xe-xo
    else:
        raise ValueError("only axis=0 implemented")
    return ts_out    
