#!/usr/bin/env python

from traits.etsconfig.api import ETSConfig
"""
Set the GUI toolkit to use
     must be set before traits imports below
"""
ETSConfig.toolkit='qt4'  
#ETSConfig.toolkit='wx'

from traits.api import CStr, CBool, File, Enum, Directory, HasTraits, Button
from traitsui.api import *
from cmind.globals import cmind_ANTs_N4, cmind_outliers_cmd
from cmind.pipeline import cmind_fMRI_preprocess


class fMRI_Preprocess(HasTraits): 
    """ fMRI preprocessing object """ 
    output_dir = Directory('/tmp/test', desc="", label="output directory")
    #output_dir.help=output_dir.label
    BOLD_vol = File('',desc="", label="BOLD volume")  #exists=True,
    ASL_vol = File('', desc="", label="ASL volume")
    paradigm_name = CStr('', desc="", label="paradigm name")
    #paradigm_name = Enum('Stories','Sentences','', desc="", label="paradigm name")
    ANTS_bias_cmd=File(cmind_ANTs_N4, desc="", label="ANTS_bias_cmd")
    UCLA_flag=CBool(False, desc="", label="Tag Before Control?")
    fsl_motion_outliers_cmd=File(cmind_outliers_cmd, desc="", label="ANTS_bias_cmd")
    generate_figures=CBool(True, desc="", label="generate figures?")    
    verbose=CBool(True, desc="", label="verbose")    
    ForceUpdate=CBool(False, desc="", label="ForceUpdate")    
    logger=File('', desc="", label="log file")    
    Run = Button('') #, orientation='vertical')
    
    def _Run_fired(self):
        print("Running now!")
        try:
            self.run()
            print("Processing completed")
        except Exception as e:
            print("\n*****************\nProcessing failed\n*****************\n")
            raise e
                
    def run(self): 
        """ Run cmind_fMRI_preprocess """ 
        cmind_fMRI_preprocess(self.output_dir, 
             self.BOLD_vol,
             ASL_vol=self.ASL_vol, 
             paradigm_name=self.paradigm_name,
             ANTS_bias_cmd=self.ANTS_bias_cmd, 
             UCLA_flag=self.UCLA_flag, 
             fsl_motion_outliers_cmd=self.fsl_motion_outliers_cmd, 
             generate_figures=self.generate_figures, 
             ForceUpdate=self.ForceUpdate, 
             verbose=self.verbose, 
             logger=self.logger)
#  
#class fMRI_Preprocessv2(fMRI_Preprocess):
#     view = View(Group(Group(Item(name = 'output_dir'),
#                            Item(name = 'BOLD_vol'),
#                            Item(name = 'ANTS_bias_cmd',visible_when='ASL_vol'),
#                            Item(name = 'UCLA_flag',visible_when='ASL_vol'),
#                            Item(name = 'fsl_motion_outliers_cmd',visible_when='ASL_vol'),
#                            label = 'Required Inputs',
#                            #layout = 'tabbed',
#                            show_border = True,
#                            show_labels = True),
#                       Group(Item(name = 'ASL_vol'),
#                            Item(name = 'paradigm_name'),
#                            Item(name = 'generate_figures'),
#                            Item(name = 'verbose'),
#                            Item(name = 'ForceUpdate'),
#                            Item(name = 'logger'),
#                            label = 'Optional Inputs',
#                            #layout = 'split',
#                            show_border = True),
#                        Item(name = 'Run'),
#                        layout='normal'  #control how subgroups are layed out
#                          #layout options:  normal, split, tabbed, flow
#                        ),
#                    
#                    width=640,
#                    resizable=True,
#                    handler=Handler(),
#                 buttons = [OKButton,CancelButton]) #[OKButton,CancelButton])  #OKButton
#             
if __name__ == "__main__": 
    app = fMRI_Preprocess() 
    
    view1 = View(Group(Group(Item(name = 'output_dir'),
                            Item(name = 'BOLD_vol',editor=FileEditor(entries=10,filter=['*.nii.gz','*.nii','*.img'])),  #,style='custom'),  #filters=['*.nii.gz;*.nii;*.img']
                            Item(name = 'ANTS_bias_cmd',visible_when='ASL_vol',editor=FileEditor(entries=10,filter=['*.nii.gz','*.nii','*.img'])),
                            Item(name = 'UCLA_flag',visible_when='ASL_vol'),
                            Item(name = 'fsl_motion_outliers_cmd',visible_when='ASL_vol'),
                            label = 'Required Inputs',
                            #layout = 'tabbed',
                            show_border = True,
                            show_labels = True),
                       Group(Item(name = 'ASL_vol'),
                            Item(name = 'paradigm_name'),
                            Item(name = 'generate_figures'),
                            Item(name = 'verbose'),
                            Item(name = 'ForceUpdate'),
                            Item(name = 'logger'),
                            label = 'Optional Inputs',
                            #layout = 'split',
                            show_border = True),
                        Group(Item(name = 'Run'),
                              show_border=True),                        
                        layout='normal'  #control how subgroups are layed out
                          #layout options:  normal, split, tabbed, flow
                        ),

                    width=640,
                    resizable=True,
                    close_result=False,  #Treat window close as "Cancel"
                 
                 buttons = [CancelButton]) #[OKButton,CancelButton])  #OKButton
             
    result=app.configure_traits(view=view1) 
    if result:  #will be True if user clicked OK, False if user clicked Cancel
        app.run()
    else:
        print("processing cancelled")