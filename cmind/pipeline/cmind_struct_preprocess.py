#!/usr/bin/env python
from __future__ import division, print_function, absolute_import

def _move_bias_iter(fname_brain,bias_field_vol,N4_img,N4_brain_img,output_dir,iter_no,verbose=False,logger=None):
    """copy/rename files from a previous bias correction iteration to prepare for the next
    """

    import os
    import warnings
    
    from cmind.utils.file_utils import split_multiple_ext
    from cmind.utils.logging_utils import log_cmd
    
    iter_no=int(iter_no)
    #move the prior image volumes and summary images to *_iter1
    fname_brain_previous_iter=split_multiple_ext(fname_brain)[0]+'_iter%04d' % iter_no
    log_cmd('$FSLDIR/bin/immv %s %s' % (fname_brain,fname_brain_previous_iter), verbose=verbose, logger=logger)
    if bias_field_vol:
        N4Bias_vol=split_multiple_ext(bias_field_vol)[0];
        if iter_no==1: #if first iteration, store the original bias field as N4BiasT1_head
            log_cmd('$FSLDIR/bin/imcp %s %s' % (N4Bias_vol,N4Bias_vol+'_head'), verbose=verbose, logger=logger)
        else:
            log_cmd('$FSLDIR/bin/immv %s %s' % (N4Bias_vol,N4Bias_vol+'_iter%04d' % iter_no), verbose=verbose, logger=logger)
    try:
        (N4_img_base,N4_img_ext)=split_multiple_ext(N4_img)
        if os.path.exists(N4_img):
            if iter_no==1: #if first iteration, copy the original bias field image to N4BiasT1_head
                log_cmd('cp %s %s' % (N4_img,N4_img_base+'_head'+N4_img_ext), verbose=verbose, logger=logger)
            else:
                log_cmd('mv %s %s' % (N4_img,N4_img_base+'_iter%04d' % iter_no + N4_img_ext), verbose=verbose, logger=logger)
        (N4_brain_img_base,N4_brain_img_ext)=split_multiple_ext(N4_brain_img)
        if os.path.exists(N4_brain_img):
            log_cmd('cp %s %s' % (N4_brain_img,N4_brain_img_base+'_iter%04d' % iter_no + N4_brain_img_ext), verbose=verbose, logger=logger)
    except:
        warnings.warn("failed to move summary images from previous iteration")
        
    return (fname_brain_previous_iter)
            
def cmind_struct_preprocess(output_dir, struct_nii, T2_struct_nii=None, age_months=252, bet_thresh=0.4, bet_use_T2=False, oname_root='T1W', reupdate_bias_cor=True, resegment_using_N4mask=False, ANTS_bias_cmd=None, omit_segmentation=False, generate_figures=True, ForceUpdate=False, verbose=False, logger=None):  #, iterate_segmentation=True, Nsegmentation_iters=1
    """Structural volume preprocessing pipeline
    
    Parameters
    ----------
    output_dir : directory
        directory in which to store the output
    struct_nii : str
        filename of T1-weighted NIFTI or NIFTIGZ volume to process
    T2_struct_nii : str, optional
        if T2_struct_nii is specified it will be rigidly registered to
        struct_nii. Brain extraction will be run on T2_struct_nii instead
    age_months : float, optional
        subject age in months  (used during cropping)
    bet_thresh : float, optional
        bet brain extraction threshold
    bet_use_T2 : bool, optional
        if True, brain extract via T2_struct_nii instead of struct_nii
    oname_root : str, optional
        output filename prefix
    reupdate_bias_cor : bool, optional
        rerun bias correction after brain extraction
    resegment_using_N4mask : bool, optional
        if True, after initial segmentation rerun N4 bias correction, weighted to
        the WM mask.  Repeat segmentation on the newly bias corrected image.
    ANTS_bias_cmd : str, optional
        location of N4BiasFieldCorrection binary
    omit_segmentation : bool, optional
        if True, omit segmentation
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool,optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
    
    Returns
    -------
    fname_head : str
        filename of Bias corrected head
    fname_brain : str
        filename of Bias corrected after brain extraction
    fname_brain_mask : str
        filename of brain mask
    WM_vol : str or None
        filename of WM partial volume estimate
    GM_vol : str or None
        filename of GM partial volume estimate
    CSF_vol : str or None
        filename of CSF partial volume estimate
    bias_field_vol : str
        filename of the N4 bias correction field
    fname2_head : str or None
        if T2_struct_nii is input, this is the corresponding bias-corrected head
    fname2_brain : str or None
        if T2_struct_nii is input, this is the corresponding bias-corrected brain
        
    See Also
    --------
    cmind_crop_robust.py, cmind_bias_correct.py, cmind_brain_extract.py, cmind_structural.py
    
    Notes
    -----
    
    This function is a wrapper that calls other structural processing routines in the following order:
        - cmind_crop_robust.py
        - cmind_bias_correct.py
        - cmind_brain_extract.py
        - cmind_structural.py (optional)
        
    """

    import os
    from os.path import join as pjoin
    from cmind.pipeline.cmind_crop_robust import cmind_crop_robust
    from cmind.pipeline.cmind_brain_extract import cmind_brain_extract
    from cmind.pipeline.cmind_bias_correct import cmind_bias_correct
    from cmind.pipeline.cmind_rigid_coregister import cmind_rigid_coregister
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    from cmind.pipeline.cmind_struct_preprocess import _move_bias_iter

    #convert various strings to boolean
    (generate_figures, ForceUpdate, verbose, 
    omit_segmentation, reupdate_bias_cor, 
    resegment_using_N4mask,
    bet_use_T2 ) = input2bool([generate_figures, ForceUpdate, verbose, 
                                omit_segmentation, reupdate_bias_cor, 
                                resegment_using_N4mask, 
                                bet_use_T2])
    
    if not omit_segmentation:
        from cmind.pipeline.cmind_segment import cmind_segment  
    
    if not ANTS_bias_cmd:  #assume it is already on the path.
        from cmind.globals import cmind_ANTs_dir
        ANTS_bias_cmd=pjoin(cmind_ANTs_dir,'N4BiasFieldCorrection')
        
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting Structural Preprocessing")      
    
    
    #convert strings to appropriate type
    if isinstance(age_months,str):
        age_months=float(age_months)
    if isinstance(bet_thresh,str):
        bet_thresh=float(bet_thresh)
          
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
        
    
#        try:
#            ANTS_bias_cmd=log_cmd('command -v N4BiasFieldCorrection', verbose=False, logger=None)
#            ANTS_bias_cmd=ANTS_bias_cmd.strip()
#        except:
#            raise IOError("N4BiasFieldCorrection not found in path, specify manually via --ANTS_bias_cmd" % ANTS_bias_cmd)

    
    
    fname_crop=pjoin(output_dir,oname_root+'_Structural_Crop')
    if T2_struct_nii:
        oname2_root='T2W'  #add as input variable?
        output_dir2=None  #add as input variable?
        if not output_dir2:
            output_dir2=output_dir;
        fname2_crop=pjoin(output_dir,oname2_root+'_Structural_Crop')        
    else:
        oname2_root=None
        fname2_crop=None
    
    fname_bias=pjoin(output_dir,oname_root+'_Structural_N4')
    if T2_struct_nii:
        fname2_bias=pjoin(output_dir,oname2_root+'_Structural_N4')        
    else:
        fname2_bias=None
    
    #if T2_struct_nii is present, the crop will be identical for both inputs
    cmind_crop_robust(struct_nii, 
                      fname_crop, 
                      age_months, 
                      oname_root=oname_root, 
                      fname2=T2_struct_nii, 
                      fname2_crop=fname2_crop, 
                      oname2_root=oname2_root, 
                      generate_figures=generate_figures, 
                      ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
    
    
    #Need this check here prior to first cmind_bias_correct call to ensure
    # correct behavior when reupdate_bias_cor=True & ForceUpdate=False on 
    # repeated runs
    if not imexist(fname_bias,'nifti-1')[0] or ForceUpdate:
        bias_skipped=False
    else:
        bias_skipped=True

    if T2_struct_nii:   #rigid registration of the T2-weighted volume to the T1-weighted volume       
        cmind_rigid_coregister(fname2_crop,fname_crop,fname2_crop)
    
    #run even if bias result already exists to make sure fname_head is set correctly       
    fname_head,bias_field_vol=cmind_bias_correct(fname_crop, fname_bias, ANTS_bias_cmd, oname_root=oname_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)

    if T2_struct_nii:
        if False:  #run independent bias-correction on the T2-weighted data
            fname2_head,bias_field_vol2=cmind_bias_correct(fname2_crop, fname2_bias, ANTS_bias_cmd, oname_root=oname2_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
        else:  #reuse the bias field from the T1-weighted data
            fname2_head=fname2_bias
            log_cmd('$FSLDIR/bin/fslmaths %s -div %s %s' % (fname2_crop, bias_field_vol, fname2_head), verbose=verbose, logger=module_logger) 
        if generate_figures and T2_struct_nii:
            log_cmd('$FSLDIR/bin/slicer %s -a %s' % (fname2_head,pjoin(output_dir,'T2W_N4.png')), verbose=verbose, logger=module_logger) 
            
        #make sure fname2_head has the full file extension
        imbool,fname2_head = imexist(fname2_head)[0:2]
    else:
        fname2_head = None
        
    if T2_struct_nii and bet_use_T2:  
        #brain extract via T2_struct_nii
        fname_head_in=fname2_head
        (fname2_brain,fname2_brain_mask)=cmind_brain_extract(fname_head_in, bet_thresh=bet_thresh, oname_root=oname2_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)

         #Now apply the same brain mask to the T1-weighted volume   
        fname_brain_mask=fname2_brain_mask.replace(oname2_root,oname_root)
        fname_brain=fname2_brain.replace(oname2_root,oname_root)
        log_cmd('$FSLDIR/bin/imcp %s %s' % (fname2_brain_mask, fname_brain_mask), verbose=verbose, logger=module_logger) 
        log_cmd('$FSLDIR/bin/fslmaths %s -mul %s %s' % (fname_head, fname_brain_mask, fname_brain), verbose=verbose, logger=module_logger) 

    else:
        #brain extract via struct_nii
        fname_head_in=fname_head
        (fname_brain,fname_brain_mask)=cmind_brain_extract(fname_head_in, bet_thresh=bet_thresh, oname_root=oname_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
        
        if T2_struct_nii:
            #Now apply the same brain mask to the T2-weighted volume   
            fname2_brain_mask=fname_brain_mask.replace(oname_root,oname2_root)
            fname2_brain=fname_brain.replace(oname_root,oname2_root)
            log_cmd('$FSLDIR/bin/imcp %s %s' % (fname_brain_mask, fname2_brain_mask), verbose=verbose, logger=module_logger) 
            log_cmd('$FSLDIR/bin/fslmaths %s -mul %s %s' % (fname2_head, fname2_brain_mask, fname2_brain), verbose=verbose, logger=module_logger) 
        else:
            fname2_brain=''

    #bias correction may work better after brain extraction, so repeat it here if requested
    #TODO: use T2 here as well?
    iter_no=1
    N4_img=pjoin(output_dir,oname_root+'_N4.png')
    N4_brain_img=pjoin(output_dir,oname_root+'_N4_brain.png')
        
    if reupdate_bias_cor and (not bias_skipped) :
        fname_brain_noN4=pjoin(output_dir,oname_root+'_brain.nii.gz')
        log_cmd('$FSLDIR/bin/fslmaths %s -mul %s %s' % (fname_crop, fname_brain_mask, fname_brain_noN4), verbose=verbose, logger=module_logger) 
        
        _move_bias_iter(fname_brain,bias_field_vol,N4_img,N4_brain_img,output_dir,iter_no=iter_no, verbose=verbose, logger=module_logger)
        #rerun bias correction, but with brain-extracted input
        fname_brain,bias_field_vol=cmind_bias_correct(fname_brain_noN4, fname_brain, ANTS_bias_cmd, oname_root=oname_root, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger) #current brain must be overwritten so ForceUpdate=True
        log_cmd('mv %s %s' % (N4_img,N4_brain_img), verbose=verbose, logger=module_logger)
        if T2_struct_nii:
            fname2_brain_noN4=pjoin(output_dir,oname2_root+'_brain.nii.gz')
            N4_img2=pjoin(output_dir,oname2_root+'_N4.png')
            N4_brain_img2=pjoin(output_dir,oname2_root+'_N4_brain.png')
            #_move_bias_iter(fname2_brain,bias_field_vol2,N4_img2,N4_brain_img2,output_dir,iter_no=iter_no, verbose=verbose, logger=module_logger)
            _move_bias_iter(fname2_brain,None,N4_img2,N4_brain_img2,output_dir,iter_no=iter_no, verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/fslmaths %s -mul %s %s' % (fname2_crop, fname2_brain_mask, fname2_brain_noN4), verbose=verbose, logger=module_logger) 
            log_cmd('$FSLDIR/bin/fslmaths %s -div %s %s' % (fname2_brain_noN4, bias_field_vol, fname2_brain), verbose=verbose, logger=module_logger) 
        iter_no+=1
  
    #normalize T2 volume to have the same median intensity as the T1 volume
    if T2_struct_nii:
        p50_T1=log_cmd('$FSLDIR/bin/fslstats "%s" -k "%s" -p 50' % (fname_brain,fname_brain_mask), verbose=verbose, logger=logger)
        p50_T1=float(p50_T1.strip())
        p50_T2=log_cmd('$FSLDIR/bin/fslstats "%s" -k "%s" -p 50' % (fname2_brain,fname2_brain_mask), verbose=verbose, logger=logger)
        p50_T2=float(p50_T2.strip())
        mult_fact=p50_T1/p50_T2
        log_cmd('$FSLDIR/bin/fslmaths "%s" -mul %f "%s"' % (fname2_brain,mult_fact,fname2_brain), verbose=verbose, logger=logger)
        #log_cmd('$FSLDIR/bin/fslmaths "%s" -mul %f "%s"' % (fname2_crop,mult_fact,fname2_crop), verbose=verbose, logger=logger)
        
    #generate the brain extraction images for the other volume      
    if generate_figures and T2_struct_nii:
        from cmind.pipeline.cmind_brain_extract import _gen_brain_overlay_img
        if bet_use_T2:
            _gen_brain_overlay_img(output_dir,fname_head,fname_brain,fname_brain_mask, verbose=verbose, logger=module_logger)
        else:
            _gen_brain_overlay_img(output_dir,fname2_head,fname2_brain,fname2_brain_mask, verbose=verbose, logger=module_logger)
    
    writeHTML=True
    if writeHTML:
        from cmind.utils.cmind_HTML_report_gen import cmind_HTML_reports

    if omit_segmentation:
        #output_tuple=(fname_head,fname_brain,fname_brain_mask,"","","",bias_field_vol)
        WM_vol=None
        GM_vol=None
        CSF_vol=None
        if writeHTML:
            cmind_HTML_reports(oname_root+'_Crop_Extract_BiasCor',output_dir,ForceUpdate=ForceUpdate)
    else:
        #TODO: incorporate T2-weighted volume into the segmentation 
        
        if (oname_root=='T2W') or (oname_root=='T2'):
            seg_name='T2Structural'
        else:
            seg_name=None  #will use default
        (WM_vol, GM_vol, CSF_vol)=cmind_segment(fname_brain, seg_name=seg_name, resegment_using_N4mask=resegment_using_N4mask, ANTS_bias_cmd=ANTS_bias_cmd, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
        
        #log_cmd('$FSLDIR/bin/fast -v --nobias -n 4 -o %s %s %s' % (pjoin(output_dir,'T1segment_4chan'),'',fname_brain), verbose=verbose, logger=module_logger)
        #log_cmd('$FSLDIR/bin/fast -v --nobias -n 3 -S 2 -o %s %s %s %s' % (pjoin(output_dir,'T1T2segment_3chan'),'',fname_brain, fname2_brain), verbose=verbose, logger=module_logger)
        #log_cmd('$FSLDIR/bin/fast -v --nobias -n 4 -S 2 -o %s %s %s %s' % (pjoin(output_dir,'T1T2segment_4chan'),'',fname_brain, fname2_brain), verbose=verbose, logger=module_logger)
        #log_cmd('$FSLDIR/bin/fast -v --nobias -n 5 -S 2 -o %s %s %s %s' % (pjoin(output_dir,'T1T2segment_5chan'),'',fname_brain, fname2_brain), verbose=verbose, logger=module_logger)
        #log_cmd('$FSLDIR/bin/fast -v --nobias -n 6 -S 2 -o %s %s %s %s' % (pjoin(output_dir,'T1T2segment_6chan'),'',fname_brain, fname2_brain), verbose=verbose, logger=module_logger)        
        #Atropos_cmd='/media/Data1/BRAINSTools/Scripts/antsAtroposN4.sh'
        #log_cmd('bash %s -d 3 -a %s -c 3 -o %s' % (Atropos_cmd,fname_brain,pjoin(output_dir,'Atropos_T1_3chan')),verbose=True, logger=module_logger)
        #log_cmd('bash %s -d 3 -a %s -c 4 -o %s' % (Atropos_cmd,fname_brain,pjoin(output_dir,'Atropos_T1_4chan')),verbose=True, logger=module_logger)
        #log_cmd('bash %s -d 3 -a %s -c 5 -o %s' % (Atropos_cmd,fname_brain,pjoin(output_dir,'Atropos_T1_5chan')),verbose=True, logger=module_logger)
        #log_cmd('bash %s -d 3 -a %s -a %s -c 3 -o %s' % (Atropos_cmd,fname_brain,fname2_brain,pjoin(output_dir,'Atropos_T1T2_3chan')),verbose=True, logger=module_logger)
        #log_cmd('bash %s -d 3 -a %s -a %s -c 4 -o %s' % (Atropos_cmd,fname_brain,fname2_brain,pjoin(output_dir,'Atropos_T1T2_4chan')),verbose=True, logger=module_logger)
        #~/src_repositories/svn_stuff/cmind-matlab/trunk/lib/Python/cmind/cmind/data/P/T1_proc$ /home/lee8rx/src_repositories/git_stuff/antsbin/bin/Atropos -d 3 -x combined_mask.nii.gz -c [5,0.0] -a T1.nii.gz -a T2.nii.gz -a FA.nii.gz -a MD.nii.gz -i kmeans[4] -k Gaussian -m [0.1,1x1x1] -o [Atrops_T1T2FAMD_Segmentation.nii.gz,Atrops_T1T2FAMD_SegmentationPosteriors%d.nii.gz] -p Socrates[0]
        #optionally rerun bias correction using a weighting for the weight-matter mask from the first iteration.
        #then redo segmentation on the newly bias corrected volume
        
        #Atropos -d 3 -v -x combined_mask.nii.gz -a T1.nii.gz -a T2.nii.gz -a FA.nii.gz -a MD.nii.gz -i KMeans[4] -o Atrops_T1T2FAMD_
        
        #if iterate_segmentation:  #TODO: move old outputs before rerunning so that ForceUpdate=True flag can be removed
        #    for segment_iter_no in range(1,Nsegmentation_iters+1):
        #        fname_brain_previous_iter = _move_bias_iter(fname_brain,bias_field_vol,N4_img,N4_brain_img,output_dir,iter_no=iter_no)
        #        iter_no+=1
        #        #restart N4 on the uncorrected brain volume, but weighting white matter more heavily
        #        weight_mask=pjoin(output_dir,'N4_weight_mask.nii.gz')
        #        log_cmd('$FSLDIR/bin/fslmaths %s -mul 0.5 -add %s -div 1.5 %s' % (fname_brain_mask,WM_vol,weight_mask),verbose=verbose,logger=logger)
        #        
        #        fname_brain=cmind_bias_correct(fname_brain_noN4, fname_brain, ANTS_bias_cmd, oname_root=oname_root, weight_mask=weight_mask, generate_figures=generate_figures, ForceUpdate=True, verbose=verbose, logger=logger) #current brain must be overwritten so ForceUpdate=True
        #        log_cmd('mv %s %s' % (N4_img,N4_brain_img), verbose=verbose, logger=module_logger)
        #        _move_segment_iter(WM_vol, GM_vol, CSF_vol,output_dir,iter_no=segment_iter_no,image_only=True)
        #        (WM_vol, GM_vol, CSF_vol)=cmind_segment(fname_brain, seg_name=seg_name, generate_figures=generate_figures, ForceUpdate=ForceUpdate, verbose=verbose, logger=logger)
        #    

        if writeHTML:
            cmind_HTML_reports(oname_root+'_Crop_Extract_BiasCor_Segment',output_dir,ForceUpdate=ForceUpdate)
            
            
    #print any filename outputs for capture by LONI
    print("fname_head:{}".format(fname_head))
    print("fname_brain:{}".format(fname_brain))
    print("fname_brain_mask:{}".format(fname_brain_mask))
    print("WM_vol:{}".format(WM_vol))
    print("GM_vol:{}".format(GM_vol))
    print("CSF_vol:{}".format(CSF_vol))
    print("bias_field_vol:{}".format(bias_field_vol))
    print("fname2_head:{}".format(fname2_head))
    print("fname2_brain:{}".format(fname2_brain))
    
    
    return (fname_head,fname_brain,fname_brain_mask,WM_vol,GM_vol,CSF_vol,bias_field_vol,fname2_head, fname2_brain)
    
    
    
def _testme():
    import tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_dir, cmind_example_output_dir, cmind_ANTs_dir
       
    age_months=84.7
    bet_thresh=0.3
    bet_use_T2=False
    reupdate_bias_cor=True
    resegment_using_N4mask=True
    ANTS_bias_cmd=pjoin(cmind_ANTs_dir,'N4BiasFieldCorrection')
    omit_segmentation=False
    oname_root='T1W'
    generate_figures=True
    ForceUpdate=True
    verbose=True
    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',
                        file_mode='w')        
    if verbose:
        func=cmind_timer(cmind_struct_preprocess,logger=logger)
    else:
        func=cmind_struct_preprocess

    testcases=['P','F']
    for testcase in testcases:    
        if testcase=='F':
            struct_nii=pjoin(cmind_example_dir,'IRC04H_06M008_F_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1_defaced.nii.gz')
            output_dir=pjoin(cmind_example_output_dir,'F','T1_proc')
            T2_struct_nii=None
        elif testcase=='P':
            struct_nii=pjoin(cmind_example_dir,'IRC04H_06M008_P_1_WIP_T1W_3D_IRCstandard32_SENSE_4_1_defaced.nii.gz')
            T2_struct_nii=pjoin(cmind_example_dir,'IRC04H_06M008_P_1_WIP_T2W_3D_FLAIR_1x1x1_SENSE_19_1_defaced.nii.gz')
            output_dir=pjoin(cmind_example_output_dir,'P','T1_proc')
     
        func(output_dir,
             struct_nii,
             T2_struct_nii=T2_struct_nii,
             age_months=age_months, 
             bet_thresh=bet_thresh, 
             bet_use_T2=bet_use_T2,
             reupdate_bias_cor=reupdate_bias_cor, 
             resegment_using_N4mask=resegment_using_N4mask,
             ANTS_bias_cmd=ANTS_bias_cmd, 
             omit_segmentation=omit_segmentation,
             oname_root=oname_root, 
             generate_figures=generate_figures, 
             ForceUpdate=ForceUpdate, 
             verbose=verbose, 
             logger=logger)   
    
  
     
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import str2none, _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and (sys.argv[1]=='test' or sys.argv[1]=='testF'):
        _testme()
    else:    
        parser = argparse.ArgumentParser(description="Structural volume preprocessing pipeline", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-i","-t1","-fname","--struct_nii",required=True, help="filename of NIFTI or NIFTIGZ volume to process")
        parser.add_argument("-t2","--T2_struct_nii", type=str, help="if T2_struct_nii is specified it will be rigidly registered to struct_nii. Brain extraction will be run on T2_struct_nii instead")
        parser.add_argument("-age","--age_months",type=float, help="subject age in months  (used during cropping)")
        parser.add_argument("-bet","--bet_thresh",type=float, help="bet brain extraction threshold")
        parser.add_argument("-betT2","--bet_use_T2",type=float, help="if True, brain extract via T2_struct_nii instead of struct_nii")
        parser.add_argument("-rebias","--reupdate_bias_cor",type=str,default="False", help="rerun bias correction after brain extraction")
        parser.add_argument("-reseg","--resegment_using_N4mask",type=str,default="False", help="if True, after initial segmentation rerun N4 bias correction, weighted to the WM mask.  Repeat segmentation on the newly bias corrected image.")
        parser.add_argument("-a","-ants","--ANTS_bias_cmd", help="location of N4BiasFieldCorrection binary")
        parser.add_argument("-noseg","--omit_segmentation",type=str,default="True", help="if True, omit segmentation")
        parser.add_argument("-oroot","--oname_root",type=str,default="T1W", help="output filename prefix")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
        
        
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        struct_nii=args.struct_nii;
        output_dir=args.output_dir;
        T2_struct_nii=args.T2_struct_nii;
        age_months=args.age_months;
        bet_thresh=args.bet_thresh;
        bet_use_T2=args.bet_use_T2;
        reupdate_bias_cor=args.reupdate_bias_cor;
        resegment_using_N4mask=args.resegment_using_N4mask;
        omit_segmentation=args.omit_segmentation;
        ANTS_bias_cmd=str2none(args.ANTS_bias_cmd,none_strings=['none','default']);
        oname_root=args.oname_root
        generate_figures=args.generate_figures;
        #UCLA_flag=args.UCLA_flag;
        #generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        verbose=args.verbose;
        logger=args.logger
        
        if verbose:
            cmind_struct_preprocess=cmind_timer(cmind_struct_preprocess,logger=logger)
    
        cmind_struct_preprocess(output_dir,
                                struct_nii,
                                T2_struct_nii=T2_struct_nii,
                                age_months=age_months, 
                                bet_thresh=bet_thresh, 
                                bet_use_T2=bet_use_T2,
                                reupdate_bias_cor=reupdate_bias_cor, 
                                resegment_using_N4mask=resegment_using_N4mask,
                                ANTS_bias_cmd=ANTS_bias_cmd, 
                                omit_segmentation=omit_segmentation,
                                oname_root=oname_root, 
                                generate_figures=generate_figures, 
                                ForceUpdate=ForceUpdate, 
                                verbose=verbose, 
                                logger=logger)
        
                        