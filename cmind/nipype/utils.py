# -*- coding: utf-8 -*-
""" misc nipype related utilities """ 
import os
from os.path import join as pjoin
    
    
def cmind_list_base_dirs(workflow):
    """ Set the base_dir of each node to match the base_dir of the overall 
    workflow
    
    Parameters
    ----------
    workflow : nipype.pipeline.engine.Workflow
        input workflow
        
    Returns
    -------
    workflow : nipype.pipeline.engine.Workflow
        output workflow
        
    """
    import nipype.pipeline.engine
    
    if not isinstance(workflow,nipype.pipeline.engine.Workflow):
        raise ValueError("workflow must be of type nipype.pipeline.engine.Workflow")
        
    #iterate through each node in the workflow, setting base_dir
    for node_name in workflow.list_node_names():
        node = workflow.get_node(node_name)
        print("{}.base_dir = {}".format(node_name,node.base_dir))
    print("\n\n")
    for node_name in workflow.list_node_names():
        node = workflow.get_node(node_name)
        print("{}.output_dir = {}".format(node_name,node.output_dir()))

        
def print_all_output_dirs(workflow):
    """ Print the output directories for all nodes in a nipype workflow.
    
    Parameters
    ----------
    workflow : nipype.pipeline.engine.Workflow
        input workflow
        
    Returns
    -------
    outlist : list of tuples
        list of (node_name, output_dir) tuples
    """
    import nipype.pipeline.engine
    
    if not isinstance(workflow,nipype.pipeline.engine.Workflow):
        raise ValueError("workflow must be of type nipype.pipeline.engine.Workflow")
        
    #iterate through each node in the workflow, setting the output_dir
    #appropriately
    outlist=[]
    for node_name in workflow.list_node_names():
        node = workflow.get_node(node_name)
        print("{}\n\t{}".format(node_name,node.output_dir()))
        outlist.append((node_name,node.output_dir()))
        
    return outlist
        
        

def load_nipype_resultfile(output_dir):
    import glob
    from nipype.utils.filemanip import loadpkl
    
    #out_dir='/media/Data1/temp/nipype_processed/IRC04H_06M008/F/CMIND_ASLBOLD_Stories/cmind_fMRI_preprocess2_BOLD'
    resultfile=glob.glob(pjoin(output_dir,'result*'))[0]
    results = loadpkl(resultfile)
    return results   

def load_nipype_inputs(output_dir):
    import glob
    from nipype.utils.filemanip import loadpkl
    
    #out_dir='/media/Data1/temp/nipype_processed/IRC04H_06M008/F/CMIND_ASLBOLD_Stories/cmind_fMRI_preprocess2_BOLD'
    inputsfile=glob.glob(pjoin(output_dir,'_inputs.pklz'))[0]
    inputs = loadpkl(inputsfile)
    return inputs        

def load_nipype_resultfile_standalone(output_dir):  #this version doesn't require nipype to be installed
    import cPickle, gzip, glob
    #out_dir='/media/Data1/temp/nipype_processed/IRC04H_06M008/F/CMIND_ASLBOLD_Stories/cmind_fMRI_preprocess2_BOLD'
    resultfile=glob.glob(pjoin(output_dir,'result*'))[0]
    results = cPickle.load(gzip.open(resultfile))
    return results        
    
def build_results_dict(basedir):
    from nipype.utils.filemanip import loadpkl
    
    #the following is similar to:  
    if False: #use find in shell
        import subprocess
        stdout=subprocess.check_output('find {} -name "result_*.pklz"'.format(basedir),shell=True)
        result_files=[f for f in stdout.split('\n') if f is not '']
        result_file_keynames = result_files
    else:  #pure python 
        result_files = [] #glob.glob(pjoin(output_dir,'result*.pklz'))
        result_file_keynames = []
        for (root, subFolders, filenames) in os.walk(basedir):  #recursively walk the full path
            if '.svn' in subFolders: #skip any .svn subfolders that may exist
                subFolders.remove('.svn')
            for f in filenames:
                #print(f)
                if os.path.splitext(f)[1]=='.pklz' and os.path.basename(f)[:7]=='result_':
                    fullfile=pjoin(root,f)
                    relative_filename=fullfile.replace(basedir,'')
                    if relative_filename[0]==os.path.sep: #strip any leading path separator
                        relative_filename=relative_filename[1:]
                    result_files.append(fullfile)
                    result_file_keynames.append(os.path.dirname(relative_filename))
    
    result_dict={}
    for f, k in zip(result_files,result_file_keynames):
        result_dict[k]=loadpkl(f)
    return result_dict   



if False:
    import glob
    basedir='/media/Data2/CMIND_NIPYPE/'
    dirlist = glob.glob(pjoin(basedir,'IRC*')) + glob.glob(pjoin(basedir,'UCLA*'))
    dirlist = sorted([d for d in dirlist if os.path.isdir(d)] )
    
    #subject_basedir='/media/Data2/CMIND_NIPYPE/IRC04H_00M034_P_1'   
#    subject_basedir='/media/Data2/CMIND_NIPYPE/IRC04H_00F001_P_1'   
 
    all_dupe_lines={}
    
    
    
    for subject_basedir in dirlist[250::]:
        print(subj_str)
        subj_str = os.path.basename(subject_basedir)
        
        outfile = pjoin('/media/Data2/CMIND_NIPYPE_REPORTS/HTML_Reports','cmind_report_{}.html'.format(subj_str))
        build_cmind_report(subject_basedir,outfile)
        with open(outfile,'r') as f:
            for line in f.readlines():
                if 'IRC04H_' in line and (not os.path.basename(subj_str) in line):
                    if not subj_str in all_dupe_lines:
                        all_dupe_lines[subj_str]=[]
                    all_dupe_lines[subj_str].append(line)

    for k in sorted(all_dupe_lines.keys()):
        v = all_dupe_lines[k]
        print(k)
        print(v)
        print("\n\n")

        
def build_cmind_report(basedir,outfile):
    import os, time
    import numpy as np
    import cmind
    from collections import OrderedDict

    from os.path import join as pjoin
    from cmind.nipype.utils import build_results_dict
    from cmind.utils.utils import dprint, lprint

    
    from jinja2 import Environment, PackageLoader

    
    
    result_dict = build_results_dict(basedir)

    env = Environment(loader=PackageLoader('cmind', pjoin('sandbox','templates')))
    #template = env.get_template('dict_table.html')
    #template.render(result=result_dict['Functional/Functional/CMIND_ASLBOLD_Hypercapnia/_paradigm_hp_104_smooth_8/cmind_fMRI_preprocess2_ASL'].inputs)   
    
    template = env.get_template('all_dict_table.html')
    
#    r=result_dict['Functional/Functional/Structural_ASLBOLD/cmind_segment']
#    output_dict={}
#    for k,v in r.outputs.trait_get().items():
#        #if v is not None:
#        output_dict[k]=v
#
#    result_list = [r.inputs, output_dict]
#    name = r.interface.__name__
#    title_list = [name + ' Inputs',name + ' Outputs']
#    IO_table = template.render(result_list=zip(result_list,title_list),title=r.interface.__name__)      
#    
    #h2 = env.get_template('h2_line.html')
    #h2.render(title='H2 Line')


            
    version_info={}
    version_info['Versions']=cmind.print_environment_info(extended_info=True)
    version_info['Versions']=version_info['Versions'].replace('\n','<br>')


    with open(outfile,'wb') as f:
        
        for k,r in result_dict.items():
            runtime_info=OrderedDict()
            runtime_info['Name']="{}".format(r.interface.__name__)
            runtime_info['Node (Full)']="{}".format(k)
            runtime_info['Run on']="{}".format(r.runtime.hostname)
            runtime_info['Platform']="{}".format(r.runtime.platform)
    #        print("Name: {}".format(r.interface.__name__)
    #        print("Node (Full): {}".format(k))
    #        print("Run on: {}".format(r.runtime.hostname))
    #        print("Platform: {}".format(r.runtime.platform))
            nseconds_per_day = 24*60*60
            if r.runtime.duration < nseconds_per_day:
                runtime_info['Run Time']="{}".format(time.strftime('%H:%M:%S', time.gmtime(r.runtime.duration)))
                #print("Run Time: {}".format(time.strftime('%H:%M:%S', time.gmtime(r.runtime.duration))))
            else:
                ndays = int(np.floor(r.runtime.duration/nseconds_per_day))
                s_rem = r.runtime.duration - ndays*nseconds_per_day
                runtime_info['Run Time']="{} days and {}".format(ndays,time.strftime('%H:%M:%S', time.gmtime(s_rem)))
                #print("Run Time: {} days and {}".format(ndays,time.strftime('%H:%M:%S', time.gmtime(s_rem))))
                
            runtime_info['Completed']="{} at {}".format(*r.runtime.endTime.split('T'))
            runtime_info['Result Path']="{}".format(pjoin(basedir,k))
            #print("Completed: {} at {}".format(*r.runtime.endTime.split('T')))
            #print("Results in: {}".format(pjoin(basedir,k)))
            output_dict={}
            for ok,ov in r.outputs.trait_get().items():
                #if v is not None:
                if isinstance(ov,list):
                    ov=lprint(ov,HTML_markup=True, silent=True)
                elif isinstance(ov,dict):
                    ov=dprint(ov,HTML_markup=True, silent=True)
                
                output_dict[ok]=ov
    
            #Trim some extraneous variables from the environment variables table
            environ_subset={}
            for k,v in r.runtime.environ.items():
                if ('FSL' in k) or (k=='PATH')  or (k=='LD_LIBRARY_PATH') or (k=='SHELL') or ('CMIND' in k) or ('ANTS' in k) or ('FSF' in k):
                    environ_subset[k]=v
            environ_subset=OrderedDict(sorted(environ_subset.items()))
            
            detailed_env_info = False
            dict_list=[runtime_info,r.inputs,output_dict]
            name = r.interface.__name__
            title_list = [name + 'Runtime Information',name + ' Inputs',name + ' Outputs']
            if detailed_env_info == True:
                dict_list += [version_info, environ_subset]
                title_list += ['Software Version Information','Environment']
                
            
            IO_table = template.render(result_list=zip(dict_list,title_list),title=r.interface.__name__)      
            #with open('/tmp/report_{}.html'.format(name),'wb') as f_local:
            #    f_local.write(IO_table)
            f.write(IO_table)


                
        #print("Inputs: {}".format(r.inputs))
        #print("Outputs: {}".format(output_dict))
    
        
    