#!/usr/bin/env python
from __future__ import division, print_function, absolute_import
        
def T1recovery_func(p,t,TR=None, absflag=True):
    """Generate a T1 recovery curve corresponding to the parameters in p

    Parameters
    ----------
    p : list or tuple
        fitted parameters (M0, T1, alpha)
    t : ndarray
        array of TI values
    TR : float, optional
        repetition time, TR (seconds)    
    absflag : bool, optional
        True if fitting absolute magnitude T1 recovery data
        
    Returns
    -------
    y : ndarray
        T1 recovery curve corresponding to the parameters in `p`
        
    """
    import numpy as np
    
    M0,T1,alpha = p
    if (TR is None) or TR==np.Inf: #TR>>T1 case, where full recovery is assumed
        y=M0*(1-(2*alpha)*np.exp(-t/T1))
    else:
        Mz_ss=1-np.exp(-TR/T1); #FLASH steady state for 90 degree excitation
        y=M0*(1+(-alpha*(Mz_ss)-1)*np.exp(-t/T1)) #%case with with finite TR where Spoiled GRE steady state at excitation flip=90 is reached before application of the Inversion pulse
    if absflag:  #if fitting magnitude data
        y=np.abs(y);
    return y
    
def T1fit_err(p,y,x,TR=None,absflag=True):
    """Return error in the T1 data fit

    Parameters
    ----------
    p : list or tuple
        fitted parameters (M0, T1, alpha)
    y : ndarray
        true data
    x : ndarray
        array of TI values
        
    absflag : bool
        True if fitting absolute magnitude T1 recovery data
        
    Returns
    -------
    fit_error : ndarray
        error between the data, y, and the fitted T1 recovery curve
        
        
    """
    
    fit_error = y-T1recovery_func(p,x,TR,absflag)
    return fit_error

def cmind_T1map_fit(output_dir,T1concat_file,TIvals_file,mask_file=None, auto_discard=True, nmax_discards=3, show_plots=False,generate_figures=True,ForceUpdate=False, verbose=False, logger=None):
    """Fit T1 values to multi TI T1EST data
    
    Parameters
    ----------
    output_dir : str
        directory in which to store the output
    T1concat_file : str
        file containing the 4D concatenated T1EST files (from cmind_T1map_preprocess.py)
    TIvals_file : str
        text file listing the TI times corresponding to the images in T1concat_file
    mask_file : str, optional
        logical ndarray of location over which to perform the fit
    auto_discard : bool, optional
        if True, first fit a small subset of voxels and detect any TI points that
        are outliers
    nmax_discards : int, optional
        maximum number of outliers that will be allowed to be discarded via
        auto_discard.  
    show_plots : bool, optional
        if False, suppress onscreen display of the plotted results
    generate_figures : bool, optional
        if true, generate additional summary images
    ForceUpdate : bool, optional
        if True, rerun and overwrite any previously existing results
    verbose : bool, optional
        print additional output (to terminal and log)
    logger : logging.Logger or str, optional
        logging.Logger object (or string of a filename to log to)
        
    Returns
    -------
    T1_map_fname : str
        filename of the T1 map image
    I0_map_fname : str
        filename of the I0 map (M0) image
    T1_NRMSE_map_fname : str
        filename of the normalized root mean squared error of the fits image
        
    Notes
    -----
    Performs T1 fits
   
    .. figure::  ../img/IRC04H_06M008_P_1_T1_map_0pt5_to_3.png
       :align:   center
       :scale:   200%
       
       T1 map
       
    .. figure::  ../img/IRC04H_06M008_P_1_T1_NRMSE_map_0_to_0pt1.png
       :align:   center
       :scale:   200%
       
       Normalized Root Mean Square Error (NRMSE) image
     
    """
    import os
    from os.path import join as pjoin
    
    import nibabel as nib
    import numpy as np
    
    from cmind.extern import leastsqbound  #leastsqbound adds bounded parmeter fits to scipy.optimize.leastsq.  See: https://github.com/jjhelmus/leastsqbound-scipy 
    from cmind.utils.utils import input2bool
    from cmind.utils.file_utils import imexist
    from cmind.utils.logging_utils import cmind_init_logging, log_cmd, cmind_func_info

    from cmind.pipeline.cmind_T1map_fit import T1fit_err, T1recovery_func
    
    
    #convert various strings to boolean
    generate_figures, ForceUpdate, verbose, show_plots = input2bool([generate_figures, ForceUpdate, verbose, show_plots])
    
    if verbose:
        cmind_func_info(logger=logger)     
    
    module_logger=cmind_init_logging(verbose=verbose, logger=logger)
    module_logger.info("Starting T1 Fitting")      
    if show_plots:
        import matplotlib.pyplot as plt
        import matplotlib.cm as cm
        from cmind.utils.montager import montager
             
    #if not mask_file: #assume default mask filename exists in output_dir
    #    mask_file=pjoin(output_dir,'T1concat_mean_N4_brain_mask.nii.gz')
        
    if mask_file and (not imexist(mask_file,'nifti-1')[0]):
        raise IOError("specified mask_file: %s not found" % (mask_file))
    if not imexist(T1concat_file,'nifti-1')[0]:
        raise IOError("specified T1concat_file: %s not found" % (T1concat_file))
    if not os.path.exists(TIvals_file):
        raise IOError("specified TIvals_file: %s not found" % (TIvals_file))
    
    ForceUpdate=input2bool(ForceUpdate)
    show_plots=input2bool(show_plots)
    generate_figures=input2bool(generate_figures)
    
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir);
    
    T1_map_fname=pjoin(output_dir,'T1_map.nii.gz')
    I0_map_fname=pjoin(output_dir,'I0_map.nii.gz')
    T1_NRMSE_map_fname=pjoin(output_dir,'T1_NRMSE_map.nii.gz')

    if (not imexist(T1_map_fname,'nifti-1')[0]) or ForceUpdate:
        T1concat_nii=nib.load(T1concat_file);
        T1series=T1concat_nii.get_data();
        if show_plots:
            plt.figure()
            plt.imshow(montager(T1series[:,:,:,0]),cmap=plt.cm.gray)
            plt.show()
        
        if not mask_file: #default mask
            #mask=np.ones(T1series.shape[0:3])>0; 
             
            T1mean_img=np.mean(np.abs(T1series),axis=3)
            minval=0.03*np.max(T1mean_img)
            mask = T1mean_img>minval;
            
            hdr=T1concat_nii.get_header();
            affine=T1concat_nii.get_affine();
            #hdr.set_data_shape(hdr_tmp.get_data_shape()[:3])
        else:
            mask_nii=nib.load(mask_file);
            hdr=mask_nii.get_header();
            affine=mask_nii.get_affine();
            mask=np.greater(mask_nii.get_data(),0) #convert to boolean
        
        if show_plots:
            plt.figure()
            plt.imshow(montager(mask),cmap=cm.gray)
            plt.show()
        
        TIvals = np.loadtxt(TIvals_file)
        
        if (T1series.shape[3] != TIvals.shape[0]):
            raise ValueError('Specified TIvals vector is a different length than the number of T1EST images in %s' % (T1concat_nii))
            #return
            
        if(TIvals.shape[0]<3):  #can't perform fit with fewer equations than unknowns
            raise ValueError('Cannot attempt T1 fit because <3 TI values exist')
            
        TR=None  #10 #TR in seconds.  T1 fit model will take finite TR into account    
        
        im1=T1series[:,:,:,0]
        p0 = [np.mean(im1[mask]), 1.0, 0.98]
        
        #p, cov_x, infodic, mesg, ier = leastsq(err, p0, args=(y, x), full_output=True)
        bounds = [(0.0, 50*p0[0]), (0.1, 6.0), (0.3, 1.0)] 
        #bounds = [(0.0, 10*p0[0]), (0.05, 6.0), (0.978, 0.982)] 
        #p, cov_x, infodic, mesg, ier = leastsqbound(T1fit_err, p0, args=(np.squeeze(T1series[x,y,z,:]), TIvals), bounds=bounds, full_output=True)
        
        (xx,yy,zz)=np.where(mask)
        npix=xx.shape[0];
        T1map=np.zeros(T1series.shape[:3],dtype='float32')
        M0map=np.zeros(T1series.shape[:3],dtype='float32')
        alphamap=np.zeros(T1series.shape[:3],dtype='float32')
        #msemap=np.zeros(T1series.shape[:3],dtype='float32')
        nrmsemap=np.zeros(T1series.shape[:3],dtype='float32')
        
        absflag=True; #True if using absolute magnitude data for the T1 fits

        def calc_initial_params(y, p0, bounds=None):
            """determine an approximate initial guess"""
            from copy import copy
            p_init = copy(p0)
            y = y.copy()
            
            #find which TI volume has the minimum intensity
            y_argsort = np.argsort(y)
            if np.abs(y_argsort[0]-y_argsort[1])>1:
                #if oscillating, average TI at the two minimum y values
                TI_min = 0.5*(TIvals[y_argsort[0]] + TIvals[y_argsort[1]])
            else:  #TIval giving y closest to zero
                #if minimum y are within 50% of each other, average the TIs
                if (1-np.abs(y[y_argsort[0]]/y[y_argsort[1]])) < 1.5:
                    TI_min = 0.5*(TIvals[y_argsort[0]] + TIvals[y_argsort[1]])
                TI_min = TIvals[y_argsort[0]]

            # for robustness to slice dropout, etc:  clip initial guess to 
            # plausable range
            TI_min = np.clip(TI_min,0.3,3.0) 
            
            # rough initial T1 guess based on where TI~=0
            T1_init = TI_min / np.log(2)
            

            if bounds is not None:
                p_init[1] = np.clip(T1_init,bounds[1][0],bounds[1][1])            
            
            # set M0 initial guess a bit higher than the largest magnetization
            # across all TI values
            y_argmax = np.argmax(y)
            p_init[0] = y[y_argmax]/np.abs(1-2*np.exp(-TIvals[y_argmax]/T1_init))
            if bounds is not None:
                p_init[0] = np.clip(p_init[0],bounds[0][0],bounds[0][1])

            return p_init
                    
        if auto_discard:
            for discard_iter in range(0,min(nmax_discards,TIvals.shape[0]-4)):  #discard up to nmax_discards outlier TI timepoints
                pixel_subset=list(range(50,npix,50));
                err_byTI_map=np.zeros((len(pixel_subset),TIvals.shape[0]),dtype='float32')
                
                for index,pix in enumerate(pixel_subset):
                
                    idx=(xx[pix],yy[pix],zz[pix])
                    y=np.squeeze(T1series[idx])
                    if verbose and ((pix % 500)==0):
                        module_logger.info("Fitting voxel %d of %d" % (pix, npix))

                    p_init = calc_initial_params(y,p0,bounds=bounds) 
                    
                    p, cov_x = leastsqbound.leastsqbound(T1fit_err, p_init, args=(y, TIvals, TR, absflag), bounds=bounds, full_output=False, maxfev=2000)
                    M0map[idx], T1map[idx], alphamap[idx] = p
                    yfit_error=T1fit_err(p,y,TIvals, TR, absflag)
                    mseval = np.sqrt(np.sum(yfit_error**2))
                    nrmsemap[idx] = mseval/np.sqrt(np.sum(y**2))
                    err_byTI_map[index,:]=np.abs(yfit_error)/np.mean(np.abs(y));
                    
                errsum = np.mean(err_byTI_map,0)

                (p25, p75) = np.percentile(errsum, q=(25.0, 75.0))
                num_IQR = 2.0  # larger -> less likely to discard a TI
                errsum_thresh = p75 + num_IQR*(p75-p25)
                
                #only throw out up to one bad point per discard_iter iteration
                if any(errsum>errsum_thresh):
                    idx_discard=np.argmax(errsum)  
                    tmp_idx = np.arange(errsum.shape[0])
                    idx_keep = tmp_idx[tmp_idx!=idx_discard]
                else:
                    idx_discard=None

                if idx_discard is not None:
                    if discard_iter==0:
                        fo=open(pjoin(output_dir,'T1map_TIdiscards.txt'),'w');
                    else:
                        fo=open(pjoin(output_dir,'T1map_TIdiscards.txt'),'a');
            
                    #for dd in range(0,idx_discard.shape[0]):
                    #    fo.write("TI=%d discarded\n" % np.round(1000*TIvals[idx_discard[dd]]));
                    fo.write("TI=%d discarded\n" % np.round(1000*TIvals[idx_discard]));
                    fo.close()
            
                    TIvals=TIvals[idx_keep];
                    T1series=T1series[:,:,:,idx_keep];
                else:  #no TI values to discard, so quit iterating
                    break
        
        
        show_all_fits=False
        Nev=400*(TIvals.shape[0]);
        for nn in np.arange(0,xx.shape[0]):
            if verbose and np.mod(nn+1,500)==0:
                module_logger.info('fitting voxel %d of %d' % (nn+1, xx.shape[0]))
            idx=(xx[nn],yy[nn],zz[nn])
            y=np.squeeze(T1series[idx])
            p_init = calc_initial_params(y,p0,bounds=bounds) 
            #p, cov_x, infodic, mesg, ier = leastsqbound(T1fit_err, p0, args=(y, TIvals), bounds=bounds, full_output=True)
            p, cov_x = leastsqbound.leastsqbound(T1fit_err, p_init, args=(y, TIvals, TR, absflag), bounds=bounds, full_output=False, ftol=1.49012e-8, xtol=1.49012e-8, maxfev=Nev) 
            M0map[idx], T1map[idx], alphamap[idx] = p
            mseval = np.sqrt(np.sum(T1fit_err(p,y,TIvals,TR,absflag)**2))
            nrmsemap[idx] = mseval/np.sqrt(np.sum(y**2))
            if show_all_fits:
                print(nn)
                plt.hold(False)
                plt.plot(TIvals,y,TIvals,T1recovery_func(p,TIvals, TR, absflag))
                plt.draw()
       	        
        if show_plots:
            plt.figure(); plt.imshow(montager(T1map),vmin=0.5,vmax=3,cmap=plt.cm.gray)
        
        map_hdr=hdr.copy()
        map_hdr.set_data_shape(T1series.shape[:3])
        map_hdr.set_data_dtype(np.dtype(np.float32)) #save the outputs as float32!
        M0map_nii=nib.Nifti1Image(M0map,affine,map_hdr);
                
        T1_hdr=map_hdr.copy(); T1_hdr['cal_min']=0; T1_hdr['cal_max']=6;
        T1map_nii=nib.Nifti1Image(T1map,affine,T1_hdr);

        alpha_hdr=map_hdr.copy(); alpha_hdr['cal_min']=0; alpha_hdr['cal_max']=1;
        alphamap_nii=nib.Nifti1Image(alphamap,affine,alpha_hdr);
        nrmsemap_nii=nib.Nifti1Image(nrmsemap,affine,map_hdr);
        T1map_nii.to_filename(pjoin(output_dir,'T1_map.nii.gz'))
        M0map_nii.to_filename(pjoin(output_dir,'I0_map.nii.gz'))
        alphamap_nii.to_filename(pjoin(output_dir,'T1_alpha_map.nii.gz'))
        nrmsemap_nii.to_filename(pjoin(output_dir,'T1_NRMSE_map.nii.gz'))
        
        if generate_figures:
            
            try: 
                """ Fit a 3-component Guassian Mixture Model """
                from sklearn.mixture import GMM 
                import matplotlib as mpl
                import matplotlib.pyplot as plt
                mpl.use('agg') #don't plot to screen
                
                mask2 = mask.copy()
                mask2 = mask2 & (T1map>0.1) & (T1map<5.0)
                
                covariance_type='diag' 
                gmm = GMM(n_components=3, covariance_type=covariance_type) 
                gmm.fit(T1map[mask2])
                #gmm.bic(T1map[mask2])
                #gmm.aic(T1map[mask2])
                gmm.weights_
                gmm.means_
                gmm.covars_
                
                asort = np.argsort(gmm.means_[:,0])
                means = gmm.means_[asort]
                weights = gmm.weights_[asort]
                if covariance_type in ['diag','sphere']:
                    variances = gmm.covars_[asort]
                elif covariance_type in ['tied']:
                    variances = np.asarray(list(gmm.covars_[0])*gmm.n_components)
     
                
                x_plot = np.linspace(0,6,1000)
                logprob,responsibilities = gmm.score_samples(x_plot)
                #plt.figure(); plt.plot(x_plot,np.exp(logprob))
                pdf = np.exp(logprob)
                pdf_individual = responsibilities * pdf[:, np.newaxis]
                plt.figure(figsize=[8,5],dpi=80)
                plt.hist(T1map[mask2], 1000, normed=True, histtype='stepfilled', alpha=0.4)
                plt.plot(x_plot, pdf, 'k-')            
                plt.plot(x_plot, pdf_individual, 'k--') 
                plt.xlabel('TI (s)')
                plt.ylabel('p(x)')
                plt.text(2.3,0.8,'Gaussian 1: ($\mu$=%0.3g, $\sigma$=%0.3g, $w$=%0.3g)' % (means[0],np.sqrt(variances[0]),weights[0]))
                plt.text(2.3,0.6,'Gaussian 2: ($\mu$=%0.3g, $\sigma$=%0.3g, $w$=%0.3g)' % (means[1],np.sqrt(variances[1]),weights[1]))
                plt.text(2.3,0.4,'Gaussian 3: ($\mu$=%0.3g, $\sigma$=%0.3g, $w$=%0.3g)' % (means[2],np.sqrt(variances[2]),weights[2]))
                plt.savefig(pjoin(output_dir,'GMM_fit.png'))
                np.savetxt(pjoin(output_dir,'GMM_means.txt'),means)
                np.savetxt(pjoin(output_dir,'GMM_stds.txt'),np.sqrt(variances))
                np.savetxt(pjoin(output_dir,'GMM_weights.txt'),weights)

            except:
                pass
                
                
       
            
        if generate_figures:
            log_cmd('$FSLDIR/bin/slicer "%s/T1_map" -i 0.5 3 -a "%s/T1_map_0pt5_to_3.png"' % (output_dir,output_dir), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/I0_map" -a "%s/I0_map.png"' % (output_dir,output_dir), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/T1_alpha_map" -i 0.5 1 -a "%s/T1_alpha_map_0pt5_to_1.png"' % (output_dir,output_dir), verbose=verbose, logger=module_logger)
            log_cmd('$FSLDIR/bin/slicer "%s/T1_NRMSE_map" -i 0 .1 -a "%s/T1_NRMSE_map_0_to_0pt1.png"' % (output_dir,output_dir), verbose=verbose, logger=module_logger)
            writeHTML=True
            if writeHTML:
                from cmind.utils.cmind_HTML_report_gen import cmind_HTML_reports
                cmind_HTML_reports('T1map',output_dir,report_dir=None,ForceUpdate=ForceUpdate);
    
    
    #print any filename outputs for capture by LONI
    print("T1_map_fname:{}".format(T1_map_fname))
    print("I0_map_fname:{}".format(I0_map_fname))
    print("T1_NRMSE_map_fname:{}".format(T1_NRMSE_map_fname))
    return (T1_map_fname, I0_map_fname, T1_NRMSE_map_fname)
    
    
def _testme():
    import os, tempfile
    from os.path import join as pjoin
    from cmind.utils.logging_utils import cmind_logger
    from cmind.globals import cmind_example_output_dir
    output_dir=pjoin(cmind_example_output_dir,'P','T1map');
    T1concat_file=pjoin(output_dir,'T1concat.nii.gz')
    TIvals_file=pjoin(output_dir,'TIvals.txt')
    generate_figures=True
    ForceUpdate=True;
    mask_file=pjoin(output_dir,'T1concat_mean_N4_brain_mask.nii.gz')
    verbose=True
    show_plots=False
    auto_discard=True 
    nmax_discards=3

    logger=cmind_logger(log_level_console='INFO',
                        logfile=pjoin(tempfile.gettempdir(),'cmind_log.txt'),
                        log_level_file='DEBUG',file_mode='w')  
    if verbose:
        func=cmind_timer(cmind_T1map_fit,logger=logger)
    else:
        func=cmind_T1map_fit

    func(output_dir, T1concat_file, TIvals_file, mask_file=mask_file, 
         auto_discard=auto_discard, nmax_discards=nmax_discards, 
         show_plots=show_plots, generate_figures=generate_figures,
         ForceUpdate=ForceUpdate,verbose=verbose, logger=logger)
            
if __name__ == '__main__':
    import sys, argparse
    from cmind.utils.utils import _parser_to_loni
    from cmind.utils.decorators import cmind_timer

    if len(sys.argv) == 2 and sys.argv[1]=='test':
        _testme()
    else:
        #function cmind_T1map_fit(output_dir,T1concat_nii,TIvals,generate_figures,ForceUpdate)
        parser = argparse.ArgumentParser(description="Fit T1 values to multi TI T1EST data", epilog="")  #-h, --help exist by default
        #example of a positional argument
        parser.add_argument("-o","--output_dir",required=True, help="directory in which to store the output")
        parser.add_argument("-i","--T1concat_nii",required=True, help="file containing the 4D concatenated T1EST files (from cmind_T1map_preprocess.py)")
        parser.add_argument("-tivals","--TIvals",required=True, help="text file listing the TI times corresponding to the images in T1concat_file")
        parser.add_argument("-mask","--mask_file", type=str, default="",  help="logical ndarray of location over which to perform the fit")
        parser.add_argument("--auto_discard", type=str, default="True",  help="if True, first fit a small subset of voxels and detect any TI points that are outliers")
        parser.add_argument("--nmax_discards", type=int, default=3,  help="maximum number of outliers that will be allowed to be discarded via auto_discard")
        parser.add_argument("-gf","--generate_figures",type=str,default="True", help="if true, generate additional summary images")
        parser.add_argument("-u","--ForceUpdate",type=str,default="False", help="if True, rerun and overwrite any previously existing results")
        parser.add_argument("-v","-debug","--verbose", help="print additional output (to terminal and log)", action="store_true")
        parser.add_argument("-log","--logfile", dest='logger', type=str, default="", help="logging.Logger object (or string of a filename to log to)")
         
        #if first command line argument is 'loni_bash', print bash template instead of proceeding
        _parser_to_loni(parser, sys.argv, __file__)  #kludge to auto-generate case/esac and usage statements for the corresponding LONI shell scripts
        
        args=parser.parse_args()
        
        output_dir=args.output_dir; #'/media/Data1/CMIND_Other/Matlab_Output/06M008/P/T1map'
        T1concat_file=args.T1concat_nii; #'/media/Data1/CMIND_Other/Matlab_Output/06M008/P/T1map/T1concat.nii.gz';
        TIvals_file=args.TIvals; #'/media/Data1/CMIND_Other/Matlab_Output/06M008/P/T1map/TIvals.txt'
        generate_figures=args.generate_figures;
        ForceUpdate=args.ForceUpdate;
        mask_file=args.mask_file
        auto_discard=args.auto_discard
        nmax_discards=args.nmax_discards
        verbose=args.verbose
        logger=args.logger
        
        if (not mask_file) or mask_file.lower()=="none":
            mask_file=None
            
        #/media/Data2/CMIND/Matlab_Output/06M008/P/T1map_test
        show_plots=False
           
        if verbose:
            cmind_T1map_fit=cmind_timer(cmind_T1map_fit,logger=logger)
    
        cmind_T1map_fit(output_dir, T1concat_file, TIvals_file, mask_file=mask_file, 
                      auto_discard=auto_discard, nmax_discards=nmax_discards, 
                      show_plots=show_plots, generate_figures=generate_figures,
                      ForceUpdate=ForceUpdate,verbose=verbose, logger=logger)
